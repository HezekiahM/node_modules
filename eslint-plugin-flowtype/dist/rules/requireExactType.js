'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var schema = [{
  enum: ['always', 'never'],
  type: 'string'
}];

var meta = {
  fixable: 'code'
};

var create = function create(context) {
  var always = (context.options[0] || 'always') === 'always';
  var sourceCode = context.getSourceCode();

  return {
    ObjectTypeAnnotation(node) {
      var exact = node.exact,
          indexers = node.indexers;


      if (always && !exact && indexers.length === 0) {
        context.report({
          fix: function fix(fixer) {
            return [fixer.replaceText(sourceCode.getFirstToken(node), '{|'), fixer.replaceText(sourceCode.getLastToken(node), '|}')];
          },
          message: 'Object type must be exact.',
          node
        });
      }

      if (!always && exact) {
        context.report({
          fix: function fix(fixer) {
            return [fixer.replaceText(sourceCode.getFirstToken(node), '{'), fixer.replaceText(sourceCode.getLastToken(node), '}')];
          },
          message: 'Object type must not be exact.',
          node
        });
      }
    }
  };
};

exports.default = {
  create,
  meta,
  schema
};
module.exports = exports.default;
"use strict";
exports.__esModule = true;
exports.c_nav__link_OutlineOffset = {
  "name": "--pf-c-nav__link--OutlineOffset",
  "value": "calc(0.25rem * -1)",
  "var": "var(--pf-c-nav__link--OutlineOffset)"
};
exports["default"] = exports.c_nav__link_OutlineOffset;
"use strict";
exports.__esModule = true;
exports.c_empty_state_m_xl__icon_MarginBottom = {
  "name": "--pf-c-empty-state--m-xl__icon--MarginBottom",
  "value": "2rem",
  "var": "var(--pf-c-empty-state--m-xl__icon--MarginBottom)"
};
exports["default"] = exports.c_empty_state_m_xl__icon_MarginBottom;
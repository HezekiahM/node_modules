"use strict";
exports.__esModule = true;
exports.c_chip_group__label_MarginRight = {
  "name": "--pf-c-chip-group__label--MarginRight",
  "value": "0.5rem",
  "var": "var(--pf-c-chip-group__label--MarginRight)"
};
exports["default"] = exports.c_chip_group__label_MarginRight;
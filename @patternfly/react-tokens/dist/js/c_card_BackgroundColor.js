"use strict";
exports.__esModule = true;
exports.c_card_BackgroundColor = {
  "name": "--pf-c-card--BackgroundColor",
  "value": "rgba(#030303, .32)",
  "var": "var(--pf-c-card--BackgroundColor)"
};
exports["default"] = exports.c_card_BackgroundColor;
"use strict";
exports.__esModule = true;
exports.c_modal_box__body_PaddingRight = {
  "name": "--pf-c-modal-box__body--PaddingRight",
  "value": "1.5rem",
  "var": "var(--pf-c-modal-box__body--PaddingRight)"
};
exports["default"] = exports.c_modal_box__body_PaddingRight;
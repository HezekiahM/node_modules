"use strict";
exports.__esModule = true;
exports.c_about_modal_box__close_PaddingBottom = {
  "name": "--pf-c-about-modal-box__close--PaddingBottom",
  "value": "2rem",
  "var": "var(--pf-c-about-modal-box__close--PaddingBottom)"
};
exports["default"] = exports.c_about_modal_box__close_PaddingBottom;
"use strict";
exports.__esModule = true;
exports.chart_boxplot_max_stroke_Width = {
  "name": "--pf-chart-boxplot--max--stroke--Width",
  "value": 1,
  "var": "var(--pf-chart-boxplot--max--stroke--Width)"
};
exports["default"] = exports.chart_boxplot_max_stroke_Width;
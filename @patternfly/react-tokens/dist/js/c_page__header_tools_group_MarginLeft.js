"use strict";
exports.__esModule = true;
exports.c_page__header_tools_group_MarginLeft = {
  "name": "--pf-c-page__header-tools-group--MarginLeft",
  "value": "2rem",
  "var": "var(--pf-c-page__header-tools-group--MarginLeft)"
};
exports["default"] = exports.c_page__header_tools_group_MarginLeft;
"use strict";
exports.__esModule = true;
exports.c_notification_drawer__header_PaddingLeft = {
  "name": "--pf-c-notification-drawer__header--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-notification-drawer__header--PaddingLeft)"
};
exports["default"] = exports.c_notification_drawer__header_PaddingLeft;
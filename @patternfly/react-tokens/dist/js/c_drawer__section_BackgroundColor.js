"use strict";
exports.__esModule = true;
exports.c_drawer__section_BackgroundColor = {
  "name": "--pf-c-drawer__section--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-drawer__section--BackgroundColor)"
};
exports["default"] = exports.c_drawer__section_BackgroundColor;
"use strict";
exports.__esModule = true;
exports.c_table_td_responsive_GridColumnGap = {
  "name": "--pf-c-table-td--responsive--GridColumnGap",
  "value": "1rem",
  "var": "var(--pf-c-table-td--responsive--GridColumnGap)"
};
exports["default"] = exports.c_table_td_responsive_GridColumnGap;
export const c_nav_m_tertiary__link_BackgroundColor: {
  "name": "--pf-c-nav--m-tertiary__link--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-nav--m-tertiary__link--BackgroundColor)"
};
export default c_nav_m_tertiary__link_BackgroundColor;
"use strict";
exports.__esModule = true;
exports.c_tile__icon_FontSize = {
  "name": "--pf-c-tile__icon--FontSize",
  "value": "1.5rem",
  "var": "var(--pf-c-tile__icon--FontSize)"
};
exports["default"] = exports.c_tile__icon_FontSize;
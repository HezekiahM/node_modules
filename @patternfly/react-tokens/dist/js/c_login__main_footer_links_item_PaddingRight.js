"use strict";
exports.__esModule = true;
exports.c_login__main_footer_links_item_PaddingRight = {
  "name": "--pf-c-login__main-footer-links-item--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-login__main-footer-links-item--PaddingRight)"
};
exports["default"] = exports.c_login__main_footer_links_item_PaddingRight;
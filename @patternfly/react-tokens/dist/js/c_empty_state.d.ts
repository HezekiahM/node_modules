export const c_empty_state: {
  ".pf-c-empty-state": {
    "c_empty_state_PaddingTop": {
      "name": "--pf-c-empty-state--PaddingTop",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_empty_state_PaddingRight": {
      "name": "--pf-c-empty-state--PaddingRight",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_empty_state_PaddingBottom": {
      "name": "--pf-c-empty-state--PaddingBottom",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_empty_state_PaddingLeft": {
      "name": "--pf-c-empty-state--PaddingLeft",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_empty_state__content_MaxWidth": {
      "name": "--pf-c-empty-state__content--MaxWidth",
      "value": "none"
    },
    "c_empty_state__icon_MarginBottom": {
      "name": "--pf-c-empty-state__icon--MarginBottom",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_empty_state__icon_FontSize": {
      "name": "--pf-c-empty-state__icon--FontSize",
      "value": "3.375rem",
      "values": [
        "--pf-global--icon--FontSize--xl",
        "$pf-global--icon--FontSize--xl",
        "pf-font-prem(54px)",
        "3.375rem"
      ]
    },
    "c_empty_state__icon_Color": {
      "name": "--pf-c-empty-state__icon--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--icon--Color--light",
        "$pf-global--icon--Color--light",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_empty_state__content_c_title_m_lg_FontSize": {
      "name": "--pf-c-empty-state__content--c-title--m-lg--FontSize",
      "value": "1.25rem",
      "values": [
        "--pf-global--FontSize--xl",
        "$pf-global--FontSize--xl",
        "pf-font-prem(20px)",
        "1.25rem"
      ]
    },
    "c_empty_state__body_MarginTop": {
      "name": "--pf-c-empty-state__body--MarginTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_empty_state__body_Color": {
      "name": "--pf-c-empty-state__body--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--200",
        "$pf-global--Color--200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_empty_state__primary_MarginTop": {
      "name": "--pf-c-empty-state__primary--MarginTop",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_empty_state__primary_secondary_MarginTop": {
      "name": "--pf-c-empty-state__primary--secondary--MarginTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_empty_state__secondary_MarginTop": {
      "name": "--pf-c-empty-state__secondary--MarginTop",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_empty_state__secondary_MarginBottom": {
      "name": "--pf-c-empty-state__secondary--MarginBottom",
      "value": "calc(0.25rem * -1)",
      "values": [
        "calc(--pf-global--spacer--xs * -1)",
        "calc($pf-global--spacer--xs * -1)",
        "calc(pf-size-prem(4px) * -1)",
        "calc(0.25rem * -1)"
      ]
    },
    "c_empty_state__secondary_child_MarginRight": {
      "name": "--pf-c-empty-state__secondary--child--MarginRight",
      "value": "calc(0.25rem / 2)",
      "values": [
        "calc(--pf-global--spacer--xs / 2)",
        "calc($pf-global--spacer--xs / 2)",
        "calc(pf-size-prem(4px) / 2)",
        "calc(0.25rem / 2)"
      ]
    },
    "c_empty_state__secondary_child_MarginBottom": {
      "name": "--pf-c-empty-state__secondary--child--MarginBottom",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_empty_state__secondary_child_MarginLeft": {
      "name": "--pf-c-empty-state__secondary--child--MarginLeft",
      "value": "calc(0.25rem / 2)",
      "values": [
        "calc(--pf-global--spacer--xs / 2)",
        "calc($pf-global--spacer--xs / 2)",
        "calc(pf-size-prem(4px) / 2)",
        "calc(0.25rem / 2)"
      ]
    },
    "c_empty_state_m_sm__content_MaxWidth": {
      "name": "--pf-c-empty-state--m-sm__content--MaxWidth",
      "value": "25rem"
    },
    "c_empty_state_m_lg__content_MaxWidth": {
      "name": "--pf-c-empty-state--m-lg__content--MaxWidth",
      "value": "37.5rem"
    },
    "c_empty_state_m_xl__body_FontSize": {
      "name": "--pf-c-empty-state--m-xl__body--FontSize",
      "value": "1.25rem",
      "values": [
        "--pf-global--FontSize--xl",
        "$pf-global--FontSize--xl",
        "pf-font-prem(20px)",
        "1.25rem"
      ]
    },
    "c_empty_state_m_xl__body_MarginTop": {
      "name": "--pf-c-empty-state--m-xl__body--MarginTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_empty_state_m_xl__icon_MarginBottom": {
      "name": "--pf-c-empty-state--m-xl__icon--MarginBottom",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_empty_state_m_xl__icon_FontSize": {
      "name": "--pf-c-empty-state--m-xl__icon--FontSize",
      "value": "6.25rem"
    },
    "c_empty_state_m_xl_c_button__secondary_MarginTop": {
      "name": "--pf-c-empty-state--m-xl--c-button__secondary--MarginTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    }
  },
  ".pf-c-empty-state.pf-m-sm": {
    "c_empty_state__content_MaxWidth": {
      "name": "--pf-c-empty-state__content--MaxWidth",
      "value": "25rem",
      "values": [
        "--pf-c-empty-state--m-sm__content--MaxWidth",
        "25rem"
      ]
    }
  },
  ".pf-c-empty-state.pf-m-lg": {
    "c_empty_state__content_MaxWidth": {
      "name": "--pf-c-empty-state__content--MaxWidth",
      "value": "37.5rem",
      "values": [
        "--pf-c-empty-state--m-lg__content--MaxWidth",
        "37.5rem"
      ]
    }
  },
  ".pf-c-empty-state.pf-m-xl": {
    "c_empty_state__body_MarginTop": {
      "name": "--pf-c-empty-state__body--MarginTop",
      "value": "1.5rem",
      "values": [
        "--pf-c-empty-state--m-xl__body--MarginTop",
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_empty_state__icon_MarginBottom": {
      "name": "--pf-c-empty-state__icon--MarginBottom",
      "value": "2rem",
      "values": [
        "--pf-c-empty-state--m-xl__icon--MarginBottom",
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_empty_state__icon_FontSize": {
      "name": "--pf-c-empty-state__icon--FontSize",
      "value": "6.25rem",
      "values": [
        "--pf-c-empty-state--m-xl__icon--FontSize",
        "6.25rem"
      ]
    },
    "c_empty_state_c_button__secondary_MarginTop": {
      "name": "--pf-c-empty-state--c-button__secondary--MarginTop",
      "value": "1rem",
      "values": [
        "--pf-c-empty-state--m-xl--c-button__secondary--MarginTop",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    }
  }
};
export default c_empty_state;
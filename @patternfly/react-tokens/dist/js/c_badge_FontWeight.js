"use strict";
exports.__esModule = true;
exports.c_badge_FontWeight = {
  "name": "--pf-c-badge--FontWeight",
  "value": "700",
  "var": "var(--pf-c-badge--FontWeight)"
};
exports["default"] = exports.c_badge_FontWeight;
"use strict";
exports.__esModule = true;
exports.chart_tooltip_flyoutStyle_stroke_Color = {
  "name": "--pf-chart-tooltip--flyoutStyle--stroke--Color",
  "value": "#151515",
  "var": "var(--pf-chart-tooltip--flyoutStyle--stroke--Color)"
};
exports["default"] = exports.chart_tooltip_flyoutStyle_stroke_Color;
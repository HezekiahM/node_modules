"use strict";
exports.__esModule = true;
exports.c_button_m_tertiary_focus_BackgroundColor = {
  "name": "--pf-c-button--m-tertiary--focus--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-button--m-tertiary--focus--BackgroundColor)"
};
exports["default"] = exports.c_button_m_tertiary_focus_BackgroundColor;
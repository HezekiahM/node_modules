"use strict";
exports.__esModule = true;
exports.c_chip_group__label_FontSize = {
  "name": "--pf-c-chip-group__label--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-chip-group__label--FontSize)"
};
exports["default"] = exports.c_chip_group__label_FontSize;
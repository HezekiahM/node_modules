"use strict";
exports.__esModule = true;
exports.c_about_modal_box__strapline_PaddingTop = {
  "name": "--pf-c-about-modal-box__strapline--PaddingTop",
  "value": "2rem",
  "var": "var(--pf-c-about-modal-box__strapline--PaddingTop)"
};
exports["default"] = exports.c_about_modal_box__strapline_PaddingTop;
"use strict";
exports.__esModule = true;
exports.c_tabs__link_before_BorderBottomWidth = {
  "name": "--pf-c-tabs__link--before--BorderBottomWidth",
  "value": "1px",
  "var": "var(--pf-c-tabs__link--before--BorderBottomWidth)"
};
exports["default"] = exports.c_tabs__link_before_BorderBottomWidth;
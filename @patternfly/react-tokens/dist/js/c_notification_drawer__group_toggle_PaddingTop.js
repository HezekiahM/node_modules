"use strict";
exports.__esModule = true;
exports.c_notification_drawer__group_toggle_PaddingTop = {
  "name": "--pf-c-notification-drawer__group-toggle--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-notification-drawer__group-toggle--PaddingTop)"
};
exports["default"] = exports.c_notification_drawer__group_toggle_PaddingTop;
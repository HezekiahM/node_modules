export const c_modal_box__body_PaddingRight: {
  "name": "--pf-c-modal-box__body--PaddingRight",
  "value": "1.5rem",
  "var": "var(--pf-c-modal-box__body--PaddingRight)"
};
export default c_modal_box__body_PaddingRight;
export const c_chip_before_BorderWidth: {
  "name": "--pf-c-chip--before--BorderWidth",
  "value": "1px",
  "var": "var(--pf-c-chip--before--BorderWidth)"
};
export default c_chip_before_BorderWidth;
"use strict";
exports.__esModule = true;
exports.c_description_list__group_RowGap = {
  "name": "--pf-c-description-list__group--RowGap",
  "value": "0.5rem",
  "var": "var(--pf-c-description-list__group--RowGap)"
};
exports["default"] = exports.c_description_list__group_RowGap;
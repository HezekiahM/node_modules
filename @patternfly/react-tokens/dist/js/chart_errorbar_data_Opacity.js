"use strict";
exports.__esModule = true;
exports.chart_errorbar_data_Opacity = {
  "name": "--pf-chart-errorbar--data--Opacity",
  "value": 1,
  "var": "var(--pf-chart-errorbar--data--Opacity)"
};
exports["default"] = exports.chart_errorbar_data_Opacity;
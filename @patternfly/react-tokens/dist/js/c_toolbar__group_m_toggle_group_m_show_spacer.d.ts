export const c_toolbar__group_m_toggle_group_m_show_spacer: {
  "name": "--pf-c-toolbar__group--m-toggle-group--m-show--spacer",
  "value": "1rem",
  "var": "var(--pf-c-toolbar__group--m-toggle-group--m-show--spacer)"
};
export default c_toolbar__group_m_toggle_group_m_show_spacer;
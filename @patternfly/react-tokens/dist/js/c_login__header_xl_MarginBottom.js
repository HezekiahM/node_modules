"use strict";
exports.__esModule = true;
exports.c_login__header_xl_MarginBottom = {
  "name": "--pf-c-login__header--xl--MarginBottom",
  "value": "3rem",
  "var": "var(--pf-c-login__header--xl--MarginBottom)"
};
exports["default"] = exports.c_login__header_xl_MarginBottom;
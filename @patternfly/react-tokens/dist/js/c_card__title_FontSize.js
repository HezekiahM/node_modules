"use strict";
exports.__esModule = true;
exports.c_card__title_FontSize = {
  "name": "--pf-c-card__title--FontSize",
  "value": "1rem",
  "var": "var(--pf-c-card__title--FontSize)"
};
exports["default"] = exports.c_card__title_FontSize;
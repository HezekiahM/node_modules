"use strict";
exports.__esModule = true;
exports.c_options_menu__toggle_BorderWidth = {
  "name": "--pf-c-options-menu__toggle--BorderWidth",
  "value": "1px",
  "var": "var(--pf-c-options-menu__toggle--BorderWidth)"
};
exports["default"] = exports.c_options_menu__toggle_BorderWidth;
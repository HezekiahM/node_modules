export const c_tile_m_disabled__body_Color: {
  "name": "--pf-c-tile--m-disabled__body--Color",
  "value": "#6a6e73",
  "var": "var(--pf-c-tile--m-disabled__body--Color)"
};
export default c_tile_m_disabled__body_Color;
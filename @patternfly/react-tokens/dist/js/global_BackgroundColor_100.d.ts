export const global_BackgroundColor_100: {
  "name": "--pf-global--BackgroundColor--100",
  "value": "#151515",
  "var": "var(--pf-global--BackgroundColor--100)"
};
export default global_BackgroundColor_100;
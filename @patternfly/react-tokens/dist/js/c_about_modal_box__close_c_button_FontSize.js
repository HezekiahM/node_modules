"use strict";
exports.__esModule = true;
exports.c_about_modal_box__close_c_button_FontSize = {
  "name": "--pf-c-about-modal-box__close--c-button--FontSize",
  "value": "1.25rem",
  "var": "var(--pf-c-about-modal-box__close--c-button--FontSize)"
};
exports["default"] = exports.c_about_modal_box__close_c_button_FontSize;
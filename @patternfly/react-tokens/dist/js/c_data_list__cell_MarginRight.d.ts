export const c_data_list__cell_MarginRight: {
  "name": "--pf-c-data-list__cell--MarginRight",
  "value": "1rem",
  "var": "var(--pf-c-data-list__cell--MarginRight)"
};
export default c_data_list__cell_MarginRight;
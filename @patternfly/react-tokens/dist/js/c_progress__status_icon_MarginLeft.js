"use strict";
exports.__esModule = true;
exports.c_progress__status_icon_MarginLeft = {
  "name": "--pf-c-progress__status-icon--MarginLeft",
  "value": "0.5rem",
  "var": "var(--pf-c-progress__status-icon--MarginLeft)"
};
exports["default"] = exports.c_progress__status_icon_MarginLeft;
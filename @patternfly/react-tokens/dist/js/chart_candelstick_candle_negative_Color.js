"use strict";
exports.__esModule = true;
exports.chart_candelstick_candle_negative_Color = {
  "name": "--pf-chart-candelstick--candle--negative--Color",
  "value": "#151515",
  "var": "var(--pf-chart-candelstick--candle--negative--Color)"
};
exports["default"] = exports.chart_candelstick_candle_negative_Color;
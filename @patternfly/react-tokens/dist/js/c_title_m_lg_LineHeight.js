"use strict";
exports.__esModule = true;
exports.c_title_m_lg_LineHeight = {
  "name": "--pf-c-title--m-lg--LineHeight",
  "value": "1.5",
  "var": "var(--pf-c-title--m-lg--LineHeight)"
};
exports["default"] = exports.c_title_m_lg_LineHeight;
"use strict";
exports.__esModule = true;
exports.c_switch__toggle_icon_Offset = {
  "name": "--pf-c-switch__toggle-icon--Offset",
  "value": "0.125rem",
  "var": "var(--pf-c-switch__toggle-icon--Offset)"
};
exports["default"] = exports.c_switch__toggle_icon_Offset;
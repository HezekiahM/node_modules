"use strict";
exports.__esModule = true;
exports.c_label_m_purple_BackgroundColor = {
  "name": "--pf-c-label--m-purple--BackgroundColor",
  "value": "#f2f0fc",
  "var": "var(--pf-c-label--m-purple--BackgroundColor)"
};
exports["default"] = exports.c_label_m_purple_BackgroundColor;
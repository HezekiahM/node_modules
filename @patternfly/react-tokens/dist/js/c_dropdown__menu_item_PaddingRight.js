"use strict";
exports.__esModule = true;
exports.c_dropdown__menu_item_PaddingRight = {
  "name": "--pf-c-dropdown__menu-item--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-dropdown__menu-item--PaddingRight)"
};
exports["default"] = exports.c_dropdown__menu_item_PaddingRight;
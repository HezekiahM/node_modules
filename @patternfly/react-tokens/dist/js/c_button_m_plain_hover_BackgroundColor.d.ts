export const c_button_m_plain_hover_BackgroundColor: {
  "name": "--pf-c-button--m-plain--hover--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-button--m-plain--hover--BackgroundColor)"
};
export default c_button_m_plain_hover_BackgroundColor;
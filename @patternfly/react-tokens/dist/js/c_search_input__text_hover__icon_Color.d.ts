export const c_search_input__text_hover__icon_Color: {
  "name": "--pf-c-search-input__text--hover__icon--Color",
  "value": "#151515",
  "var": "var(--pf-c-search-input__text--hover__icon--Color)"
};
export default c_search_input__text_hover__icon_Color;
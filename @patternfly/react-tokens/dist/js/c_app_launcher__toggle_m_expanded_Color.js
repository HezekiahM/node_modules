"use strict";
exports.__esModule = true;
exports.c_app_launcher__toggle_m_expanded_Color = {
  "name": "--pf-c-app-launcher__toggle--m-expanded--Color",
  "value": "#151515",
  "var": "var(--pf-c-app-launcher__toggle--m-expanded--Color)"
};
exports["default"] = exports.c_app_launcher__toggle_m_expanded_Color;
export const c_table__compound_expansion_toggle__button_after_BorderTopWidth: {
  "name": "--pf-c-table__compound-expansion-toggle__button--after--BorderTopWidth",
  "value": "3px",
  "var": "var(--pf-c-table__compound-expansion-toggle__button--after--BorderTopWidth)"
};
export default c_table__compound_expansion_toggle__button_after_BorderTopWidth;
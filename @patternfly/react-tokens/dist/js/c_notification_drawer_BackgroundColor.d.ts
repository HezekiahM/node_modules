export const c_notification_drawer_BackgroundColor: {
  "name": "--pf-c-notification-drawer--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-notification-drawer--BackgroundColor)"
};
export default c_notification_drawer_BackgroundColor;
"use strict";
exports.__esModule = true;
exports.c_background_image_BackgroundImage = {
  "name": "--pf-c-background-image--BackgroundImage",
  "value": "url(\"../../assets/images/pfbg_576.jpg\")",
  "var": "var(--pf-c-background-image--BackgroundImage)"
};
exports["default"] = exports.c_background_image_BackgroundImage;
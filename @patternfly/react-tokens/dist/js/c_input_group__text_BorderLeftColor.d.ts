export const c_input_group__text_BorderLeftColor: {
  "name": "--pf-c-input-group__text--BorderLeftColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-input-group__text--BorderLeftColor)"
};
export default c_input_group__text_BorderLeftColor;
export const c_banner_m_info_BackgroundColor: {
  "name": "--pf-c-banner--m-info--BackgroundColor",
  "value": "#73bcf7",
  "var": "var(--pf-c-banner--m-info--BackgroundColor)"
};
export default c_banner_m_info_BackgroundColor;
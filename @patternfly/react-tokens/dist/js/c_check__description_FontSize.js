"use strict";
exports.__esModule = true;
exports.c_check__description_FontSize = {
  "name": "--pf-c-check__description--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-check__description--FontSize)"
};
exports["default"] = exports.c_check__description_FontSize;
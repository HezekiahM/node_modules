"use strict";
exports.__esModule = true;
exports.chart_color_green_200 = {
  "name": "--pf-chart-color-green-200",
  "value": "#7cc674",
  "var": "var(--pf-chart-color-green-200)"
};
exports["default"] = exports.chart_color_green_200;
"use strict";
exports.__esModule = true;
exports.c_switch__input_disabled__toggle_BackgroundColor = {
  "name": "--pf-c-switch__input--disabled__toggle--BackgroundColor",
  "value": "#6a6e73",
  "var": "var(--pf-c-switch__input--disabled__toggle--BackgroundColor)"
};
exports["default"] = exports.c_switch__input_disabled__toggle_BackgroundColor;
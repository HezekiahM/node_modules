"use strict";
exports.__esModule = true;
exports.global_Color_light_100 = {
  "name": "--pf-global--Color--light-100",
  "value": "#fff",
  "var": "var(--pf-global--Color--light-100)"
};
exports["default"] = exports.global_Color_light_100;
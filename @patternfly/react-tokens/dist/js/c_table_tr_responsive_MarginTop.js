"use strict";
exports.__esModule = true;
exports.c_table_tr_responsive_MarginTop = {
  "name": "--pf-c-table-tr--responsive--MarginTop",
  "value": "0.5rem",
  "var": "var(--pf-c-table-tr--responsive--MarginTop)"
};
exports["default"] = exports.c_table_tr_responsive_MarginTop;
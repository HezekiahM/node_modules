export const l_level: {
  ".pf-l-level": {
    "l_level_m_gutter_MarginRight": {
      "name": "--pf-l-level--m-gutter--MarginRight",
      "value": "1rem",
      "values": [
        "--pf-global--gutter",
        "$pf-global--gutter",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    }
  }
};
export default l_level;
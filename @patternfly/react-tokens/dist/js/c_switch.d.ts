export const c_switch: {
  ".pf-c-switch": {
    "c_switch_FontSize": {
      "name": "--pf-c-switch--FontSize",
      "value": "1rem",
      "values": [
        "--pf-global--FontSize--md",
        "$pf-global--FontSize--md",
        "pf-font-prem(16px)",
        "1rem"
      ]
    },
    "c_switch__toggle_icon_FontSize": {
      "name": "--pf-c-switch__toggle-icon--FontSize",
      "value": "calc(1rem * .625)",
      "values": [
        "calc(--pf-c-switch--FontSize * .625)",
        "calc(--pf-global--FontSize--md * .625)",
        "calc($pf-global--FontSize--md * .625)",
        "calc(pf-font-prem(16px) * .625)",
        "calc(1rem * .625)"
      ]
    },
    "c_switch__toggle_icon_Color": {
      "name": "--pf-c-switch__toggle-icon--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_switch__toggle_icon_Left": {
      "name": "--pf-c-switch__toggle-icon--Left",
      "value": "calc(1rem * .4)",
      "values": [
        "calc(--pf-c-switch--FontSize * .4)",
        "calc(--pf-global--FontSize--md * .4)",
        "calc($pf-global--FontSize--md * .4)",
        "calc(pf-font-prem(16px) * .4)",
        "calc(1rem * .4)"
      ]
    },
    "c_switch__toggle_icon_Offset": {
      "name": "--pf-c-switch__toggle-icon--Offset",
      "value": "0.125rem"
    },
    "c_switch_LineHeight": {
      "name": "--pf-c-switch--LineHeight",
      "value": "1.5",
      "values": [
        "--pf-global--LineHeight--md",
        "$pf-global--LineHeight--md",
        "1.5"
      ]
    },
    "c_switch_Height": {
      "name": "--pf-c-switch--Height",
      "value": "calc(1rem * 1.5)",
      "values": [
        "calc(--pf-c-switch--FontSize * --pf-c-switch--LineHeight)",
        "calc(--pf-global--FontSize--md * --pf-global--LineHeight--md)",
        "calc($pf-global--FontSize--md * $pf-global--LineHeight--md)",
        "calc(pf-font-prem(16px) * 1.5)",
        "calc(1rem * 1.5)"
      ]
    },
    "c_switch__input_checked__toggle_BackgroundColor": {
      "name": "--pf-c-switch__input--checked__toggle--BackgroundColor",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_switch__input_checked__toggle_before_TranslateX": {
      "name": "--pf-c-switch__input--checked__toggle--before--TranslateX",
      "value": "calc(100% + 0.125rem)",
      "values": [
        "calc(100% + --pf-c-switch__toggle-icon--Offset)",
        "calc(100% + 0.125rem)"
      ]
    },
    "c_switch__input_checked__label_Color": {
      "name": "--pf-c-switch__input--checked__label--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_switch__input_not_checked__label_Color": {
      "name": "--pf-c-switch__input--not-checked__label--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--disabled-color--100",
        "$pf-global--disabled-color--100",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_switch__input_disabled__label_Color": {
      "name": "--pf-c-switch__input--disabled__label--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--disabled-color--100",
        "$pf-global--disabled-color--100",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_switch__input_disabled__toggle_BackgroundColor": {
      "name": "--pf-c-switch__input--disabled__toggle--BackgroundColor",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--dark-200",
        "$pf-global--Color--dark-200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_switch__input_disabled__toggle_before_BackgroundColor": {
      "name": "--pf-c-switch__input--disabled__toggle--before--BackgroundColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--disabled-color--200",
        "$pf-global--disabled-color--200",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_switch__input_focus__toggle_OutlineWidth": {
      "name": "--pf-c-switch__input--focus__toggle--OutlineWidth",
      "value": "2px",
      "values": [
        "--pf-global--BorderWidth--md",
        "$pf-global--BorderWidth--md",
        "2px"
      ]
    },
    "c_switch__input_focus__toggle_OutlineOffset": {
      "name": "--pf-c-switch__input--focus__toggle--OutlineOffset",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_switch__input_focus__toggle_OutlineColor": {
      "name": "--pf-c-switch__input--focus__toggle--OutlineColor",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_switch__toggle_Height": {
      "name": "--pf-c-switch__toggle--Height",
      "value": "calc(1rem * 1.5)",
      "values": [
        "calc(--pf-c-switch--FontSize * --pf-c-switch--LineHeight)",
        "calc(--pf-global--FontSize--md * --pf-global--LineHeight--md)",
        "calc($pf-global--FontSize--md * $pf-global--LineHeight--md)",
        "calc(pf-font-prem(16px) * 1.5)",
        "calc(1rem * 1.5)"
      ]
    },
    "c_switch__toggle_BackgroundColor": {
      "name": "--pf-c-switch__toggle--BackgroundColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--disabled-color--200",
        "$pf-global--disabled-color--200",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_switch__toggle_BorderRadius": {
      "name": "--pf-c-switch__toggle--BorderRadius",
      "value": "calc(1rem * 1.5)",
      "values": [
        "--pf-c-switch--Height",
        "calc(--pf-c-switch--FontSize * --pf-c-switch--LineHeight)",
        "calc(--pf-global--FontSize--md * --pf-global--LineHeight--md)",
        "calc($pf-global--FontSize--md * $pf-global--LineHeight--md)",
        "calc(pf-font-prem(16px) * 1.5)",
        "calc(1rem * 1.5)"
      ]
    },
    "c_switch__toggle_before_Width": {
      "name": "--pf-c-switch__toggle--before--Width",
      "value": "calc(1rem - 0.125rem)",
      "values": [
        "calc(--pf-c-switch--FontSize - --pf-c-switch__toggle-icon--Offset)",
        "calc(--pf-global--FontSize--md - 0.125rem)",
        "calc($pf-global--FontSize--md - 0.125rem)",
        "calc(pf-font-prem(16px) - 0.125rem)",
        "calc(1rem - 0.125rem)"
      ]
    },
    "c_switch__toggle_before_Height": {
      "name": "--pf-c-switch__toggle--before--Height",
      "value": "calc(1rem - 0.125rem)",
      "values": [
        "--pf-c-switch__toggle--before--Width",
        "calc(--pf-c-switch--FontSize - --pf-c-switch__toggle-icon--Offset)",
        "calc(--pf-global--FontSize--md - 0.125rem)",
        "calc($pf-global--FontSize--md - 0.125rem)",
        "calc(pf-font-prem(16px) - 0.125rem)",
        "calc(1rem - 0.125rem)"
      ]
    },
    "c_switch__toggle_before_Top": {
      "name": "--pf-c-switch__toggle--before--Top",
      "value": "calc((calc(1rem * 1.5) - calc(1rem - 0.125rem)) / 2)",
      "values": [
        "calc((--pf-c-switch--Height - --pf-c-switch__toggle--before--Height) / 2)",
        "calc((calc(--pf-c-switch--FontSize * --pf-c-switch--LineHeight) - --pf-c-switch__toggle--before--Width) / 2)",
        "calc((calc(--pf-global--FontSize--md * --pf-global--LineHeight--md) - calc(--pf-c-switch--FontSize - --pf-c-switch__toggle-icon--Offset)) / 2)",
        "calc((calc($pf-global--FontSize--md * $pf-global--LineHeight--md) - calc(--pf-global--FontSize--md - 0.125rem)) / 2)",
        "calc((calc($pf-global--FontSize--md * $pf-global--LineHeight--md) - calc($pf-global--FontSize--md - 0.125rem)) / 2)",
        "calc((calc(pf-font-prem(16px) * 1.5) - calc(pf-font-prem(16px) - 0.125rem)) / 2)",
        "calc((calc(1rem * 1.5) - calc(1rem - 0.125rem)) / 2)"
      ]
    },
    "c_switch__toggle_before_Left": {
      "name": "--pf-c-switch__toggle--before--Left",
      "value": "calc((calc(1rem * 1.5) - calc(1rem - 0.125rem)) / 2)",
      "values": [
        "--pf-c-switch__toggle--before--Top",
        "calc((--pf-c-switch--Height - --pf-c-switch__toggle--before--Height) / 2)",
        "calc((calc(--pf-c-switch--FontSize * --pf-c-switch--LineHeight) - --pf-c-switch__toggle--before--Width) / 2)",
        "calc((calc(--pf-global--FontSize--md * --pf-global--LineHeight--md) - calc(--pf-c-switch--FontSize - --pf-c-switch__toggle-icon--Offset)) / 2)",
        "calc((calc($pf-global--FontSize--md * $pf-global--LineHeight--md) - calc(--pf-global--FontSize--md - 0.125rem)) / 2)",
        "calc((calc($pf-global--FontSize--md * $pf-global--LineHeight--md) - calc($pf-global--FontSize--md - 0.125rem)) / 2)",
        "calc((calc(pf-font-prem(16px) * 1.5) - calc(pf-font-prem(16px) - 0.125rem)) / 2)",
        "calc((calc(1rem * 1.5) - calc(1rem - 0.125rem)) / 2)"
      ]
    },
    "c_switch__toggle_before_BackgroundColor": {
      "name": "--pf-c-switch__toggle--before--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_switch__toggle_before_BorderRadius": {
      "name": "--pf-c-switch__toggle--before--BorderRadius",
      "value": "30em",
      "values": [
        "--pf-global--BorderRadius--lg",
        "$pf-global--BorderRadius--lg",
        "30em"
      ]
    },
    "c_switch__toggle_before_BoxShadow": {
      "name": "--pf-c-switch__toggle--before--BoxShadow",
      "value": "0 0.25rem 0.5rem 0rem rgba(3, 3, 3, 0.12), 0 0 0.25rem 0 rgba(3, 3, 3, 0.06)",
      "values": [
        "--pf-global--BoxShadow--md",
        "$pf-global--BoxShadow--md",
        "0 pf-size-prem(4px) pf-size-prem(8px) pf-size-prem(0) rgba($pf-color-black-1000, .12), 0 0 pf-size-prem(4px) 0 rgba($pf-color-black-1000, .06)",
        "0 pf-size-prem(4px) pf-size-prem(8px) pf-size-prem(0) rgba(#030303, .12), 0 0 pf-size-prem(4px) 0 rgba(#030303, .06)",
        "0 0.25rem 0.5rem 0rem rgba(3, 3, 3, 0.12), 0 0 0.25rem 0 rgba(3, 3, 3, 0.06)"
      ]
    },
    "c_switch__toggle_before_Transition": {
      "name": "--pf-c-switch__toggle--before--Transition",
      "value": "transform .25s ease 0s"
    },
    "c_switch__toggle_Width": {
      "name": "--pf-c-switch__toggle--Width",
      "value": "calc(calc(1rem * 1.5) + 0.125rem + calc(1rem - 0.125rem))",
      "values": [
        "calc(--pf-c-switch--Height + --pf-c-switch__toggle-icon--Offset + --pf-c-switch__toggle--before--Width)",
        "calc(calc(--pf-c-switch--FontSize * --pf-c-switch--LineHeight) + 0.125rem + calc(--pf-c-switch--FontSize - --pf-c-switch__toggle-icon--Offset))",
        "calc(calc(--pf-global--FontSize--md * --pf-global--LineHeight--md) + 0.125rem + calc(--pf-global--FontSize--md - 0.125rem))",
        "calc(calc($pf-global--FontSize--md * $pf-global--LineHeight--md) + 0.125rem + calc($pf-global--FontSize--md - 0.125rem))",
        "calc(calc(pf-font-prem(16px) * 1.5) + 0.125rem + calc(pf-font-prem(16px) - 0.125rem))",
        "calc(calc(1rem * 1.5) + 0.125rem + calc(1rem - 0.125rem))"
      ]
    },
    "c_switch__label_PaddingLeft": {
      "name": "--pf-c-switch__label--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_switch__label_Color": {
      "name": "--pf-c-switch__label--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    }
  }
};
export default c_switch;
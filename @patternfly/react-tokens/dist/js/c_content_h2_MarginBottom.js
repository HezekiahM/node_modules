"use strict";
exports.__esModule = true;
exports.c_content_h2_MarginBottom = {
  "name": "--pf-c-content--h2--MarginBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-content--h2--MarginBottom)"
};
exports["default"] = exports.c_content_h2_MarginBottom;
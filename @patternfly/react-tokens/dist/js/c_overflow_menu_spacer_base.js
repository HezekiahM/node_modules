"use strict";
exports.__esModule = true;
exports.c_overflow_menu_spacer_base = {
  "name": "--pf-c-overflow-menu--spacer--base",
  "value": "1rem",
  "var": "var(--pf-c-overflow-menu--spacer--base)"
};
exports["default"] = exports.c_overflow_menu_spacer_base;
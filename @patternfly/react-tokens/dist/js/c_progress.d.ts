export const c_progress: {
  ".pf-c-progress": {
    "c_progress_GridGap": {
      "name": "--pf-c-progress--GridGap",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_progress__bar_before_BackgroundColor": {
      "name": "--pf-c-progress__bar--before--BackgroundColor",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_progress__bar_Height": {
      "name": "--pf-c-progress__bar--Height",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_progress__bar_BackgroundColor": {
      "name": "--pf-c-progress__bar--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--light-100",
        "$pf-global--BackgroundColor--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_progress__status_icon_Color": {
      "name": "--pf-c-progress__status-icon--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_progress__status_icon_MarginLeft": {
      "name": "--pf-c-progress__status-icon--MarginLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_progress__bar_before_Opacity": {
      "name": "--pf-c-progress__bar--before--Opacity",
      "value": ".2"
    },
    "c_progress__indicator_Height": {
      "name": "--pf-c-progress__indicator--Height",
      "value": "1rem",
      "values": [
        "--pf-c-progress__bar--Height",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_progress__indicator_BackgroundColor": {
      "name": "--pf-c-progress__indicator--BackgroundColor",
      "value": "#06c",
      "values": [
        "--pf-c-progress__bar--before--BackgroundColor",
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_progress_m_danger__bar_BackgroundColor": {
      "name": "--pf-c-progress--m-danger__bar--BackgroundColor",
      "value": "#c9190b",
      "values": [
        "--pf-global--danger-color--100",
        "$pf-global--danger-color--100",
        "$pf-color-red-100",
        "#c9190b"
      ]
    },
    "c_progress_m_success__bar_BackgroundColor": {
      "name": "--pf-c-progress--m-success__bar--BackgroundColor",
      "value": "#3e8635",
      "values": [
        "--pf-global--success-color--100",
        "$pf-global--success-color--100",
        "$pf-color-green-500",
        "#3e8635"
      ]
    },
    "c_progress_m_danger__status_icon_Color": {
      "name": "--pf-c-progress--m-danger__status-icon--Color",
      "value": "#c9190b",
      "values": [
        "--pf-global--danger-color--100",
        "$pf-global--danger-color--100",
        "$pf-color-red-100",
        "#c9190b"
      ]
    },
    "c_progress_m_success__status_icon_Color": {
      "name": "--pf-c-progress--m-success__status-icon--Color",
      "value": "#3e8635",
      "values": [
        "--pf-global--success-color--100",
        "$pf-global--success-color--100",
        "$pf-color-green-500",
        "#3e8635"
      ]
    },
    "c_progress_m_inside__indicator_MinWidth": {
      "name": "--pf-c-progress--m-inside__indicator--MinWidth",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_progress_m_inside__measure_Color": {
      "name": "--pf-c-progress--m-inside__measure--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_progress_m_success_m_inside__measure_Color": {
      "name": "--pf-c-progress--m-success--m-inside__measure--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_progress_m_inside__measure_FontSize": {
      "name": "--pf-c-progress--m-inside__measure--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_progress_m_outside__measure_FontSize": {
      "name": "--pf-c-progress--m-outside__measure--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_progress_m_sm__bar_Height": {
      "name": "--pf-c-progress--m-sm__bar--Height",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_progress_m_sm__description_FontSize": {
      "name": "--pf-c-progress--m-sm__description--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_progress_m_sm__measure_FontSize": {
      "name": "--pf-c-progress--m-sm__measure--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_progress_m_lg__bar_Height": {
      "name": "--pf-c-progress--m-lg__bar--Height",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    }
  },
  ".pf-c-progress.pf-m-sm": {
    "c_progress__bar_Height": {
      "name": "--pf-c-progress__bar--Height",
      "value": "0.5rem",
      "values": [
        "--pf-c-progress--m-sm__bar--Height",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-c-progress.pf-m-lg": {
    "c_progress__bar_Height": {
      "name": "--pf-c-progress__bar--Height",
      "value": "1.5rem",
      "values": [
        "--pf-c-progress--m-lg__bar--Height",
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    }
  },
  ".pf-c-progress.pf-m-success": {
    "c_progress__bar_before_BackgroundColor": {
      "name": "--pf-c-progress__bar--before--BackgroundColor",
      "value": "#3e8635",
      "values": [
        "--pf-c-progress--m-success__bar--BackgroundColor",
        "--pf-global--success-color--100",
        "$pf-global--success-color--100",
        "$pf-color-green-500",
        "#3e8635"
      ]
    },
    "c_progress__status_icon_Color": {
      "name": "--pf-c-progress__status-icon--Color",
      "value": "#3e8635",
      "values": [
        "--pf-c-progress--m-success__status-icon--Color",
        "--pf-global--success-color--100",
        "$pf-global--success-color--100",
        "$pf-color-green-500",
        "#3e8635"
      ]
    },
    "c_progress_m_inside__measure_Color": {
      "name": "--pf-c-progress--m-inside__measure--Color",
      "value": "#151515",
      "values": [
        "--pf-c-progress--m-success--m-inside__measure--Color",
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    }
  },
  ".pf-c-progress.pf-m-danger": {
    "c_progress__bar_before_BackgroundColor": {
      "name": "--pf-c-progress__bar--before--BackgroundColor",
      "value": "#c9190b",
      "values": [
        "--pf-c-progress--m-danger__bar--BackgroundColor",
        "--pf-global--danger-color--100",
        "$pf-global--danger-color--100",
        "$pf-color-red-100",
        "#c9190b"
      ]
    },
    "c_progress__status_icon_Color": {
      "name": "--pf-c-progress__status-icon--Color",
      "value": "#c9190b",
      "values": [
        "--pf-c-progress--m-danger__status-icon--Color",
        "--pf-global--danger-color--100",
        "$pf-global--danger-color--100",
        "$pf-color-red-100",
        "#c9190b"
      ]
    }
  }
};
export default c_progress;
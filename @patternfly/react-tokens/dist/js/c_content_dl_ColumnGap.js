"use strict";
exports.__esModule = true;
exports.c_content_dl_ColumnGap = {
  "name": "--pf-c-content--dl--ColumnGap",
  "value": "3rem",
  "var": "var(--pf-c-content--dl--ColumnGap)"
};
exports["default"] = exports.c_content_dl_ColumnGap;
"use strict";
exports.__esModule = true;
exports.c_dropdown__toggle_before_BorderWidth = {
  "name": "--pf-c-dropdown__toggle--before--BorderWidth",
  "value": "1px",
  "var": "var(--pf-c-dropdown__toggle--before--BorderWidth)"
};
exports["default"] = exports.c_dropdown__toggle_before_BorderWidth;
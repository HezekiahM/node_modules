"use strict";
exports.__esModule = true;
exports.c_toolbar__item_m_chip_group_spacer = {
  "name": "--pf-c-toolbar__item--m-chip-group--spacer",
  "value": "0.5rem",
  "var": "var(--pf-c-toolbar__item--m-chip-group--spacer)"
};
exports["default"] = exports.c_toolbar__item_m_chip_group_spacer;
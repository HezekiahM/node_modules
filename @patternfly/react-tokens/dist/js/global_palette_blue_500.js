"use strict";
exports.__esModule = true;
exports.global_palette_blue_500 = {
  "name": "--pf-global--palette--blue-500",
  "value": "#004080",
  "var": "var(--pf-global--palette--blue-500)"
};
exports["default"] = exports.global_palette_blue_500;
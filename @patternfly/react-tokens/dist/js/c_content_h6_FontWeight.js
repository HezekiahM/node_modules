"use strict";
exports.__esModule = true;
exports.c_content_h6_FontWeight = {
  "name": "--pf-c-content--h6--FontWeight",
  "value": "700",
  "var": "var(--pf-c-content--h6--FontWeight)"
};
exports["default"] = exports.c_content_h6_FontWeight;
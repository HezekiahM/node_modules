"use strict";
exports.__esModule = true;
exports.chart_global_FontSize_2xl = {
  "name": "--pf-chart-global--FontSize--2xl",
  "value": 24,
  "var": "var(--pf-chart-global--FontSize--2xl)"
};
exports["default"] = exports.chart_global_FontSize_2xl;
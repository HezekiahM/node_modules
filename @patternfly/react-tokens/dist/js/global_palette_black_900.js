"use strict";
exports.__esModule = true;
exports.global_palette_black_900 = {
  "name": "--pf-global--palette--black-900",
  "value": "#151515",
  "var": "var(--pf-global--palette--black-900)"
};
exports["default"] = exports.global_palette_black_900;
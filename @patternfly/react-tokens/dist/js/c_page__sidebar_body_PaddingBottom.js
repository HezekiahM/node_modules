"use strict";
exports.__esModule = true;
exports.c_page__sidebar_body_PaddingBottom = {
  "name": "--pf-c-page__sidebar-body--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-page__sidebar-body--PaddingBottom)"
};
exports["default"] = exports.c_page__sidebar_body_PaddingBottom;
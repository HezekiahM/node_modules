"use strict";
exports.__esModule = true;
exports.chart_color_red_400 = {
  "name": "--pf-chart-color-red-400",
  "value": "#470000",
  "var": "var(--pf-chart-color-red-400)"
};
exports["default"] = exports.chart_color_red_400;
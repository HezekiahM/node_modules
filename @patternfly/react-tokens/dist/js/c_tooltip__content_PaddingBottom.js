"use strict";
exports.__esModule = true;
exports.c_tooltip__content_PaddingBottom = {
  "name": "--pf-c-tooltip__content--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-tooltip__content--PaddingBottom)"
};
exports["default"] = exports.c_tooltip__content_PaddingBottom;
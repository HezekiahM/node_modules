"use strict";
exports.__esModule = true;
exports.c_dropdown__toggle_PaddingBottom = {
  "name": "--pf-c-dropdown__toggle--PaddingBottom",
  "value": "0.375rem",
  "var": "var(--pf-c-dropdown__toggle--PaddingBottom)"
};
exports["default"] = exports.c_dropdown__toggle_PaddingBottom;
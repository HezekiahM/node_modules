"use strict";
exports.__esModule = true;
exports.c_tabs__scroll_button_xl_Width = {
  "name": "--pf-c-tabs__scroll-button--xl--Width",
  "value": "4rem",
  "var": "var(--pf-c-tabs__scroll-button--xl--Width)"
};
exports["default"] = exports.c_tabs__scroll_button_xl_Width;
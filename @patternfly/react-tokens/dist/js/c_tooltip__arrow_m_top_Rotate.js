"use strict";
exports.__esModule = true;
exports.c_tooltip__arrow_m_top_Rotate = {
  "name": "--pf-c-tooltip__arrow--m-top--Rotate",
  "value": "45deg",
  "var": "var(--pf-c-tooltip__arrow--m-top--Rotate)"
};
exports["default"] = exports.c_tooltip__arrow_m_top_Rotate;
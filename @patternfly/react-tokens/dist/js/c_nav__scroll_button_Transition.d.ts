export const c_nav__scroll_button_Transition: {
  "name": "--pf-c-nav__scroll-button--Transition",
  "value": "margin .125s, transform .125s, opacity .125s",
  "var": "var(--pf-c-nav__scroll-button--Transition)"
};
export default c_nav__scroll_button_Transition;
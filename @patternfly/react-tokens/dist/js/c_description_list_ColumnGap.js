"use strict";
exports.__esModule = true;
exports.c_description_list_ColumnGap = {
  "name": "--pf-c-description-list--ColumnGap",
  "value": "1rem",
  "var": "var(--pf-c-description-list--ColumnGap)"
};
exports["default"] = exports.c_description_list_ColumnGap;
"use strict";
exports.__esModule = true;
exports.c_dropdown__menu_item_FontWeight = {
  "name": "--pf-c-dropdown__menu-item--FontWeight",
  "value": "400",
  "var": "var(--pf-c-dropdown__menu-item--FontWeight)"
};
exports["default"] = exports.c_dropdown__menu_item_FontWeight;
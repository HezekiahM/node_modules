export const c_context_selector__menu_search_PaddingRight: {
  "name": "--pf-c-context-selector__menu-search--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-context-selector__menu-search--PaddingRight)"
};
export default c_context_selector__menu_search_PaddingRight;
"use strict";
exports.__esModule = true;
exports.c_page__sidebar_body_PaddingTop = {
  "name": "--pf-c-page__sidebar-body--PaddingTop",
  "value": "0.5rem",
  "var": "var(--pf-c-page__sidebar-body--PaddingTop)"
};
exports["default"] = exports.c_page__sidebar_body_PaddingTop;
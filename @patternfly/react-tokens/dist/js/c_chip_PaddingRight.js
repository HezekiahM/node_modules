"use strict";
exports.__esModule = true;
exports.c_chip_PaddingRight = {
  "name": "--pf-c-chip--PaddingRight",
  "value": "0.5rem",
  "var": "var(--pf-c-chip--PaddingRight)"
};
exports["default"] = exports.c_chip_PaddingRight;
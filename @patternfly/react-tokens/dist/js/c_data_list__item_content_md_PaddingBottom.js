"use strict";
exports.__esModule = true;
exports.c_data_list__item_content_md_PaddingBottom = {
  "name": "--pf-c-data-list__item-content--md--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-data-list__item-content--md--PaddingBottom)"
};
exports["default"] = exports.c_data_list__item_content_md_PaddingBottom;
"use strict";
exports.__esModule = true;
exports.c_pagination_m_bottom_md_PaddingBottom = {
  "name": "--pf-c-pagination--m-bottom--md--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-pagination--m-bottom--md--PaddingBottom)"
};
exports["default"] = exports.c_pagination_m_bottom_md_PaddingBottom;
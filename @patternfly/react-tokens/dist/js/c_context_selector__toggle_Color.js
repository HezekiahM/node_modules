"use strict";
exports.__esModule = true;
exports.c_context_selector__toggle_Color = {
  "name": "--pf-c-context-selector__toggle--Color",
  "value": "#151515",
  "var": "var(--pf-c-context-selector__toggle--Color)"
};
exports["default"] = exports.c_context_selector__toggle_Color;
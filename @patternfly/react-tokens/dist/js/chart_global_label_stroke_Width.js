"use strict";
exports.__esModule = true;
exports.chart_global_label_stroke_Width = {
  "name": "--pf-chart-global--label--stroke--Width",
  "value": 0,
  "var": "var(--pf-chart-global--label--stroke--Width)"
};
exports["default"] = exports.chart_global_label_stroke_Width;
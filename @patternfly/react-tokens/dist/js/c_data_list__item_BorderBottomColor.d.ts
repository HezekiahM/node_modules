export const c_data_list__item_BorderBottomColor: {
  "name": "--pf-c-data-list__item--BorderBottomColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-data-list__item--BorderBottomColor)"
};
export default c_data_list__item_BorderBottomColor;
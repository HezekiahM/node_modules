"use strict";
exports.__esModule = true;
exports.c_table_cell_first_last_child_PaddingLeft = {
  "name": "--pf-c-table--cell--first-last-child--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-table--cell--first-last-child--PaddingLeft)"
};
exports["default"] = exports.c_table_cell_first_last_child_PaddingLeft;
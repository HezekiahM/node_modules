"use strict";
exports.__esModule = true;
exports.c_about_modal_box_PaddingLeft = {
  "name": "--pf-c-about-modal-box--PaddingLeft",
  "value": "2rem",
  "var": "var(--pf-c-about-modal-box--PaddingLeft)"
};
exports["default"] = exports.c_about_modal_box_PaddingLeft;
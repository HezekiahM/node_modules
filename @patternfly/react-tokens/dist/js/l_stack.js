"use strict";
exports.__esModule = true;
exports.l_stack = {
  ".pf-l-stack": {
    "l_stack_m_gutter_MarginBottom": {
      "name": "--pf-l-stack--m-gutter--MarginBottom",
      "value": "1rem",
      "values": [
        "--pf-global--gutter",
        "$pf-global--gutter",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    }
  }
};
exports["default"] = exports.l_stack;
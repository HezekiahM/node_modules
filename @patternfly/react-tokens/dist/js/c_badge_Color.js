"use strict";
exports.__esModule = true;
exports.c_badge_Color = {
  "name": "--pf-c-badge--Color",
  "value": "#fff",
  "var": "var(--pf-c-badge--Color)"
};
exports["default"] = exports.c_badge_Color;
"use strict";
exports.__esModule = true;
exports.c_about_modal_box_lg_grid_template_columns = {
  "name": "--pf-c-about-modal-box--lg--grid-template-columns",
  "value": "1fr .6fr",
  "var": "var(--pf-c-about-modal-box--lg--grid-template-columns)"
};
exports["default"] = exports.c_about_modal_box_lg_grid_template_columns;
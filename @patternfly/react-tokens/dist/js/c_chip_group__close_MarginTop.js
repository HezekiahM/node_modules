"use strict";
exports.__esModule = true;
exports.c_chip_group__close_MarginTop = {
  "name": "--pf-c-chip-group__close--MarginTop",
  "value": "calc(0.25rem * -1)",
  "var": "var(--pf-c-chip-group__close--MarginTop)"
};
exports["default"] = exports.c_chip_group__close_MarginTop;
"use strict";
exports.__esModule = true;
exports.c_button_m_link_m_display_lg_FontSize = {
  "name": "--pf-c-button--m-link--m-display-lg--FontSize",
  "value": "1.125rem",
  "var": "var(--pf-c-button--m-link--m-display-lg--FontSize)"
};
exports["default"] = exports.c_button_m_link_m_display_lg_FontSize;
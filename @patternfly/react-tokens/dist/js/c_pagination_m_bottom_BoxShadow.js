"use strict";
exports.__esModule = true;
exports.c_pagination_m_bottom_BoxShadow = {
  "name": "--pf-c-pagination--m-bottom--BoxShadow",
  "value": "0 -0.125rem 0.25rem -0.0625rem rgba(3, 3, 3, 0.16)",
  "var": "var(--pf-c-pagination--m-bottom--BoxShadow)"
};
exports["default"] = exports.c_pagination_m_bottom_BoxShadow;
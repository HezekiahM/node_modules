"use strict";
exports.__esModule = true;
exports.c_button_m_secondary_BackgroundColor = {
  "name": "--pf-c-button--m-secondary--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-button--m-secondary--BackgroundColor)"
};
exports["default"] = exports.c_button_m_secondary_BackgroundColor;
"use strict";
exports.__esModule = true;
exports.c_table_tr_responsive_PaddingBottom = {
  "name": "--pf-c-table-tr--responsive--PaddingBottom",
  "value": "2rem",
  "var": "var(--pf-c-table-tr--responsive--PaddingBottom)"
};
exports["default"] = exports.c_table_tr_responsive_PaddingBottom;
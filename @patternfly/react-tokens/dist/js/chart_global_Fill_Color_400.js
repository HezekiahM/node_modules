"use strict";
exports.__esModule = true;
exports.chart_global_Fill_Color_400 = {
  "name": "--pf-chart-global--Fill--Color--400",
  "value": "#b8bbbe",
  "var": "var(--pf-chart-global--Fill--Color--400)"
};
exports["default"] = exports.chart_global_Fill_Color_400;
"use strict";
exports.__esModule = true;
exports.global_primary_color_light_100 = {
  "name": "--pf-global--primary-color--light-100",
  "value": "#73bcf7",
  "var": "var(--pf-global--primary-color--light-100)"
};
exports["default"] = exports.global_primary_color_light_100;
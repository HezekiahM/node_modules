export const c_alert__icon_MarginRight: {
  "name": "--pf-c-alert__icon--MarginRight",
  "value": "0.5rem",
  "var": "var(--pf-c-alert__icon--MarginRight)"
};
export default c_alert__icon_MarginRight;
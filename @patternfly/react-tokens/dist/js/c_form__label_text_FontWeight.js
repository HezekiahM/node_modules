"use strict";
exports.__esModule = true;
exports.c_form__label_text_FontWeight = {
  "name": "--pf-c-form__label-text--FontWeight",
  "value": "700",
  "var": "var(--pf-c-form__label-text--FontWeight)"
};
exports["default"] = exports.c_form__label_text_FontWeight;
"use strict";
exports.__esModule = true;
exports.c_label_BorderRadius = {
  "name": "--pf-c-label--BorderRadius",
  "value": "30em",
  "var": "var(--pf-c-label--BorderRadius)"
};
exports["default"] = exports.c_label_BorderRadius;
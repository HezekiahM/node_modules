"use strict";
exports.__esModule = true;
exports.c_tabs__link_hover_after_BorderWidth = {
  "name": "--pf-c-tabs__link--hover--after--BorderWidth",
  "value": "3px",
  "var": "var(--pf-c-tabs__link--hover--after--BorderWidth)"
};
exports["default"] = exports.c_tabs__link_hover_after_BorderWidth;
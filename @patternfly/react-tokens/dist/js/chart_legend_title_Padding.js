"use strict";
exports.__esModule = true;
exports.chart_legend_title_Padding = {
  "name": "--pf-chart-legend--title--Padding",
  "value": 2,
  "var": "var(--pf-chart-legend--title--Padding)"
};
exports["default"] = exports.chart_legend_title_Padding;
"use strict";
exports.__esModule = true;
exports.chart_global_Fill_Color_300 = {
  "name": "--pf-chart-global--Fill--Color--300",
  "value": "#d2d2d2",
  "var": "var(--pf-chart-global--Fill--Color--300)"
};
exports["default"] = exports.chart_global_Fill_Color_300;
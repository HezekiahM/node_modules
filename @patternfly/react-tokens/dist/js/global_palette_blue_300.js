"use strict";
exports.__esModule = true;
exports.global_palette_blue_300 = {
  "name": "--pf-global--palette--blue-300",
  "value": "#2b9af3",
  "var": "var(--pf-global--palette--blue-300)"
};
exports["default"] = exports.global_palette_blue_300;
"use strict";
exports.__esModule = true;
exports.c_banner_m_info_BackgroundColor = {
  "name": "--pf-c-banner--m-info--BackgroundColor",
  "value": "#73bcf7",
  "var": "var(--pf-c-banner--m-info--BackgroundColor)"
};
exports["default"] = exports.c_banner_m_info_BackgroundColor;
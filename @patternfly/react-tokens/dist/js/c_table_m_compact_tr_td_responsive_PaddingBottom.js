"use strict";
exports.__esModule = true;
exports.c_table_m_compact_tr_td_responsive_PaddingBottom = {
  "name": "--pf-c-table--m-compact-tr-td--responsive--PaddingBottom",
  "value": "0.25rem",
  "var": "var(--pf-c-table--m-compact-tr-td--responsive--PaddingBottom)"
};
exports["default"] = exports.c_table_m_compact_tr_td_responsive_PaddingBottom;
export const c_table__sort__button_focus__sort_indicator_Color: {
  "name": "--pf-c-table__sort__button--focus__sort-indicator--Color",
  "value": "#151515",
  "var": "var(--pf-c-table__sort__button--focus__sort-indicator--Color)"
};
export default c_table__sort__button_focus__sort_indicator_Color;
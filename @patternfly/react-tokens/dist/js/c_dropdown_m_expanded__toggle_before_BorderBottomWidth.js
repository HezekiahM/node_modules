"use strict";
exports.__esModule = true;
exports.c_dropdown_m_expanded__toggle_before_BorderBottomWidth = {
  "name": "--pf-c-dropdown--m-expanded__toggle--before--BorderBottomWidth",
  "value": "2px",
  "var": "var(--pf-c-dropdown--m-expanded__toggle--before--BorderBottomWidth)"
};
exports["default"] = exports.c_dropdown_m_expanded__toggle_before_BorderBottomWidth;
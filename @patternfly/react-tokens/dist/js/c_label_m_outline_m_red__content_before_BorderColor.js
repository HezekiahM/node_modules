"use strict";
exports.__esModule = true;
exports.c_label_m_outline_m_red__content_before_BorderColor = {
  "name": "--pf-c-label--m-outline--m-red__content--before--BorderColor",
  "value": "#c9190b",
  "var": "var(--pf-c-label--m-outline--m-red__content--before--BorderColor)"
};
exports["default"] = exports.c_label_m_outline_m_red__content_before_BorderColor;
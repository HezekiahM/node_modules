"use strict";
exports.__esModule = true;
exports.chart_global_stroke_line_cap = {
  "name": "--pf-chart-global--stroke-line-cap",
  "value": "round",
  "var": "var(--pf-chart-global--stroke-line-cap)"
};
exports["default"] = exports.chart_global_stroke_line_cap;
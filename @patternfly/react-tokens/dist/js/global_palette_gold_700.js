"use strict";
exports.__esModule = true;
exports.global_palette_gold_700 = {
  "name": "--pf-global--palette--gold-700",
  "value": "#3d2c00",
  "var": "var(--pf-global--palette--gold-700)"
};
exports["default"] = exports.global_palette_gold_700;
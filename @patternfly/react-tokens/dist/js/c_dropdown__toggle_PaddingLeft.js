"use strict";
exports.__esModule = true;
exports.c_dropdown__toggle_PaddingLeft = {
  "name": "--pf-c-dropdown__toggle--PaddingLeft",
  "value": "0.5rem",
  "var": "var(--pf-c-dropdown__toggle--PaddingLeft)"
};
exports["default"] = exports.c_dropdown__toggle_PaddingLeft;
"use strict";
exports.__esModule = true;
exports.c_notification_badge__i_Height = {
  "name": "--pf-c-notification-badge__i--Height",
  "value": "1rem",
  "var": "var(--pf-c-notification-badge__i--Height)"
};
exports["default"] = exports.c_notification_badge__i_Height;
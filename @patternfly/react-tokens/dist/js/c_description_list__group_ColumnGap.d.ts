export const c_description_list__group_ColumnGap: {
  "name": "--pf-c-description-list__group--ColumnGap",
  "value": "1rem",
  "var": "var(--pf-c-description-list__group--ColumnGap)"
};
export default c_description_list__group_ColumnGap;
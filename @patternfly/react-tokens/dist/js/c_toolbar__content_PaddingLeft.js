"use strict";
exports.__esModule = true;
exports.c_toolbar__content_PaddingLeft = {
  "name": "--pf-c-toolbar__content--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-toolbar__content--PaddingLeft)"
};
exports["default"] = exports.c_toolbar__content_PaddingLeft;
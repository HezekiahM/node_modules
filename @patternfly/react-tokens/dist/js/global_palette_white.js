"use strict";
exports.__esModule = true;
exports.global_palette_white = {
  "name": "--pf-global--palette--white",
  "value": "#fff",
  "var": "var(--pf-global--palette--white)"
};
exports["default"] = exports.global_palette_white;
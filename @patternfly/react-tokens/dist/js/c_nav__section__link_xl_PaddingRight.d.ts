export const c_nav__section__link_xl_PaddingRight: {
  "name": "--pf-c-nav__section__link--xl--PaddingRight",
  "value": "1.5rem",
  "var": "var(--pf-c-nav__section__link--xl--PaddingRight)"
};
export default c_nav__section__link_xl_PaddingRight;
"use strict";
exports.__esModule = true;
exports.c_button_PaddingRight = {
  "name": "--pf-c-button--PaddingRight",
  "value": "0.5rem",
  "var": "var(--pf-c-button--PaddingRight)"
};
exports["default"] = exports.c_button_PaddingRight;
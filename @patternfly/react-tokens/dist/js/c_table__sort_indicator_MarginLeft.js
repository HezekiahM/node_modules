"use strict";
exports.__esModule = true;
exports.c_table__sort_indicator_MarginLeft = {
  "name": "--pf-c-table__sort-indicator--MarginLeft",
  "value": "1rem",
  "var": "var(--pf-c-table__sort-indicator--MarginLeft)"
};
exports["default"] = exports.c_table__sort_indicator_MarginLeft;
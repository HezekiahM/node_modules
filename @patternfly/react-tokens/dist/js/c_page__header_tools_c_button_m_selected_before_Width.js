"use strict";
exports.__esModule = true;
exports.c_page__header_tools_c_button_m_selected_before_Width = {
  "name": "--pf-c-page__header-tools--c-button--m-selected--before--Width",
  "value": "2.25rem",
  "var": "var(--pf-c-page__header-tools--c-button--m-selected--before--Width)"
};
exports["default"] = exports.c_page__header_tools_c_button_m_selected_before_Width;
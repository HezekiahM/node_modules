"use strict";
exports.__esModule = true;
exports.c_button_m_small_FontSize = {
  "name": "--pf-c-button--m-small--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-button--m-small--FontSize)"
};
exports["default"] = exports.c_button_m_small_FontSize;
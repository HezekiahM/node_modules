export const c_page__main_breadcrumb_PaddingTop: {
  "name": "--pf-c-page__main-breadcrumb--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-page__main-breadcrumb--PaddingTop)"
};
export default c_page__main_breadcrumb_PaddingTop;
"use strict";
exports.__esModule = true;
exports.c_nav__link_focus_before_BorderBottomWidth = {
  "name": "--pf-c-nav__link--focus--before--BorderBottomWidth",
  "value": "3px",
  "var": "var(--pf-c-nav__link--focus--before--BorderBottomWidth)"
};
exports["default"] = exports.c_nav__link_focus_before_BorderBottomWidth;
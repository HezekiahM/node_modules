"use strict";
exports.__esModule = true;
exports.chart_legend_gutter_Width = {
  "name": "--pf-chart-legend--gutter--Width",
  "value": 20,
  "var": "var(--pf-chart-legend--gutter--Width)"
};
exports["default"] = exports.chart_legend_gutter_Width;
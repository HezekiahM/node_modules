export const c_hint_BackgroundColor: {
  "name": "--pf-c-hint--BackgroundColor",
  "value": "#e7f1fa",
  "var": "var(--pf-c-hint--BackgroundColor)"
};
export default c_hint_BackgroundColor;
export const c_tooltip__arrow_m_right_TranslateX: {
  "name": "--pf-c-tooltip__arrow--m-right--TranslateX",
  "value": "-50%",
  "var": "var(--pf-c-tooltip__arrow--m-right--TranslateX)"
};
export default c_tooltip__arrow_m_right_TranslateX;
export const c_dropdown_c_divider_MarginTop: {
  "name": "--pf-c-dropdown--c-divider--MarginTop",
  "value": "0.5rem",
  "var": "var(--pf-c-dropdown--c-divider--MarginTop)"
};
export default c_dropdown_c_divider_MarginTop;
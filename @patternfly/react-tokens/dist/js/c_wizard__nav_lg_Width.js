"use strict";
exports.__esModule = true;
exports.c_wizard__nav_lg_Width = {
  "name": "--pf-c-wizard__nav--lg--Width",
  "value": "15.625rem",
  "var": "var(--pf-c-wizard__nav--lg--Width)"
};
exports["default"] = exports.c_wizard__nav_lg_Width;
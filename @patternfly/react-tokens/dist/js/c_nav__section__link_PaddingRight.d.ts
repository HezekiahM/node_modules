export const c_nav__section__link_PaddingRight: {
  "name": "--pf-c-nav__section__link--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-nav__section__link--PaddingRight)"
};
export default c_nav__section__link_PaddingRight;
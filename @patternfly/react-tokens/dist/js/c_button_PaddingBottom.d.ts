export const c_button_PaddingBottom: {
  "name": "--pf-c-button--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-button--PaddingBottom)"
};
export default c_button_PaddingBottom;
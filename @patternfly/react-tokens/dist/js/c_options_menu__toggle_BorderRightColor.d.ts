export const c_options_menu__toggle_BorderRightColor: {
  "name": "--pf-c-options-menu__toggle--BorderRightColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-options-menu__toggle--BorderRightColor)"
};
export default c_options_menu__toggle_BorderRightColor;
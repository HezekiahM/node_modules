"use strict";
exports.__esModule = true;
exports.c_form_control_invalid_PaddingBottom = {
  "name": "--pf-c-form-control--invalid--PaddingBottom",
  "value": "calc(0.375rem - 2px)",
  "var": "var(--pf-c-form-control--invalid--PaddingBottom)"
};
exports["default"] = exports.c_form_control_invalid_PaddingBottom;
"use strict";
exports.__esModule = true;
exports.global_TransitionDuration = {
  "name": "--pf-global--TransitionDuration",
  "value": "250ms",
  "var": "var(--pf-global--TransitionDuration)"
};
exports["default"] = exports.global_TransitionDuration;
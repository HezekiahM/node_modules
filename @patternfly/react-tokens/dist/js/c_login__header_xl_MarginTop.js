"use strict";
exports.__esModule = true;
exports.c_login__header_xl_MarginTop = {
  "name": "--pf-c-login__header--xl--MarginTop",
  "value": "4rem",
  "var": "var(--pf-c-login__header--xl--MarginTop)"
};
exports["default"] = exports.c_login__header_xl_MarginTop;
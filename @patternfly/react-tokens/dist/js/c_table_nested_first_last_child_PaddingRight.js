"use strict";
exports.__esModule = true;
exports.c_table_nested_first_last_child_PaddingRight = {
  "name": "--pf-c-table--nested--first-last-child--PaddingRight",
  "value": "4rem",
  "var": "var(--pf-c-table--nested--first-last-child--PaddingRight)"
};
exports["default"] = exports.c_table_nested_first_last_child_PaddingRight;
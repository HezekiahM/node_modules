export const c_chip_before_BorderRadius: {
  "name": "--pf-c-chip--before--BorderRadius",
  "value": "3px",
  "var": "var(--pf-c-chip--before--BorderRadius)"
};
export default c_chip_before_BorderRadius;
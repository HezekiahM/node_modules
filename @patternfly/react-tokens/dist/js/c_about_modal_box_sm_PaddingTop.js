"use strict";
exports.__esModule = true;
exports.c_about_modal_box_sm_PaddingTop = {
  "name": "--pf-c-about-modal-box--sm--PaddingTop",
  "value": "4rem",
  "var": "var(--pf-c-about-modal-box--sm--PaddingTop)"
};
exports["default"] = exports.c_about_modal_box_sm_PaddingTop;
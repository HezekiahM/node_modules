"use strict";
exports.__esModule = true;
exports.chart_color_gold_100 = {
  "name": "--pf-chart-color-gold-100",
  "value": "#f9e0a2",
  "var": "var(--pf-chart-color-gold-100)"
};
exports["default"] = exports.chart_color_gold_100;
"use strict";
exports.__esModule = true;
exports.c_overflow_menu__item_spacer = {
  "name": "--pf-c-overflow-menu__item--spacer",
  "value": "1rem",
  "var": "var(--pf-c-overflow-menu__item--spacer)"
};
exports["default"] = exports.c_overflow_menu__item_spacer;
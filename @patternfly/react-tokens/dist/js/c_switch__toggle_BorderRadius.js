"use strict";
exports.__esModule = true;
exports.c_switch__toggle_BorderRadius = {
  "name": "--pf-c-switch__toggle--BorderRadius",
  "value": "calc(1rem * 1.5)",
  "var": "var(--pf-c-switch__toggle--BorderRadius)"
};
exports["default"] = exports.c_switch__toggle_BorderRadius;
"use strict";
exports.__esModule = true;
exports.c_table_cell_MaxWidth = {
  "name": "--pf-c-table--cell--MaxWidth",
  "value": "100%",
  "var": "var(--pf-c-table--cell--MaxWidth)"
};
exports["default"] = exports.c_table_cell_MaxWidth;
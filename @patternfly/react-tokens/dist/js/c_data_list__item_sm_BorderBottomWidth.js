"use strict";
exports.__esModule = true;
exports.c_data_list__item_sm_BorderBottomWidth = {
  "name": "--pf-c-data-list__item--sm--BorderBottomWidth",
  "value": "1px",
  "var": "var(--pf-c-data-list__item--sm--BorderBottomWidth)"
};
exports["default"] = exports.c_data_list__item_sm_BorderBottomWidth;
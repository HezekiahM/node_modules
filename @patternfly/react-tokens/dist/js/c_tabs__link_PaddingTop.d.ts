export const c_tabs__link_PaddingTop: {
  "name": "--pf-c-tabs__link--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-tabs__link--PaddingTop)"
};
export default c_tabs__link_PaddingTop;
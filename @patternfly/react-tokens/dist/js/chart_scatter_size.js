"use strict";
exports.__esModule = true;
exports.chart_scatter_size = {
  "name": "--pf-chart-scatter--size",
  "value": 3,
  "var": "var(--pf-chart-scatter--size)"
};
exports["default"] = exports.chart_scatter_size;
export const c_options_menu_m_top__menu_TranslateY: {
  "name": "--pf-c-options-menu--m-top__menu--TranslateY",
  "value": "calc(-100% - 0.25rem)",
  "var": "var(--pf-c-options-menu--m-top__menu--TranslateY)"
};
export default c_options_menu_m_top__menu_TranslateY;
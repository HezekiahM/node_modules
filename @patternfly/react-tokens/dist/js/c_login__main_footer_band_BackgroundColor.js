"use strict";
exports.__esModule = true;
exports.c_login__main_footer_band_BackgroundColor = {
  "name": "--pf-c-login__main-footer-band--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-login__main-footer-band--BackgroundColor)"
};
exports["default"] = exports.c_login__main_footer_band_BackgroundColor;
"use strict";
exports.__esModule = true;
exports.c_label_m_outline_m_orange__content_before_BorderColor = {
  "name": "--pf-c-label--m-outline--m-orange__content--before--BorderColor",
  "value": "#f9e0a2",
  "var": "var(--pf-c-label--m-outline--m-orange__content--before--BorderColor)"
};
exports["default"] = exports.c_label_m_outline_m_orange__content_before_BorderColor;
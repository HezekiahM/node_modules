"use strict";
exports.__esModule = true;
exports.c_nav_m_light__item_before_BorderColor = {
  "name": "--pf-c-nav--m-light__item--before--BorderColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-nav--m-light__item--before--BorderColor)"
};
exports["default"] = exports.c_nav_m_light__item_before_BorderColor;
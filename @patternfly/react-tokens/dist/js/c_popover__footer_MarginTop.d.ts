export const c_popover__footer_MarginTop: {
  "name": "--pf-c-popover__footer--MarginTop",
  "value": "1rem",
  "var": "var(--pf-c-popover__footer--MarginTop)"
};
export default c_popover__footer_MarginTop;
"use strict";
exports.__esModule = true;
exports.c_button__icon_m_end_MarginLeft = {
  "name": "--pf-c-button__icon--m-end--MarginLeft",
  "value": "0.25rem",
  "var": "var(--pf-c-button__icon--m-end--MarginLeft)"
};
exports["default"] = exports.c_button__icon_m_end_MarginLeft;
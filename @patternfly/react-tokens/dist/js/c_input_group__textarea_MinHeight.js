"use strict";
exports.__esModule = true;
exports.c_input_group__textarea_MinHeight = {
  "name": "--pf-c-input-group__textarea--MinHeight",
  "value": "2rem",
  "var": "var(--pf-c-input-group__textarea--MinHeight)"
};
exports["default"] = exports.c_input_group__textarea_MinHeight;
"use strict";
exports.__esModule = true;
exports.c_card_child_PaddingLeft = {
  "name": "--pf-c-card--child--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-card--child--PaddingLeft)"
};
exports["default"] = exports.c_card_child_PaddingLeft;
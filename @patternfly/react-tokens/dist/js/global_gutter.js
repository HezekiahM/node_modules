"use strict";
exports.__esModule = true;
exports.global_gutter = {
  "name": "--pf-global--gutter",
  "value": "1rem",
  "var": "var(--pf-global--gutter)"
};
exports["default"] = exports.global_gutter;
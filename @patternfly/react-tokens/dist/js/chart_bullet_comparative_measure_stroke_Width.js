"use strict";
exports.__esModule = true;
exports.chart_bullet_comparative_measure_stroke_Width = {
  "name": "--pf-chart-bullet--comparative-measure--stroke--Width",
  "value": 1,
  "var": "var(--pf-chart-bullet--comparative-measure--stroke--Width)"
};
exports["default"] = exports.chart_bullet_comparative_measure_stroke_Width;
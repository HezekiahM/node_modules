"use strict";
exports.__esModule = true;
exports.chart_bullet_comparative_measure_error_stroke_Width = {
  "name": "--pf-chart-bullet--comparative-measure--error--stroke--Width",
  "value": 2,
  "var": "var(--pf-chart-bullet--comparative-measure--error--stroke--Width)"
};
exports["default"] = exports.chart_bullet_comparative_measure_error_stroke_Width;
"use strict";
exports.__esModule = true;
exports.c_button_FontWeight = {
  "name": "--pf-c-button--FontWeight",
  "value": "700",
  "var": "var(--pf-c-button--FontWeight)"
};
exports["default"] = exports.c_button_FontWeight;
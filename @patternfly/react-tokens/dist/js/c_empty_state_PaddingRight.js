"use strict";
exports.__esModule = true;
exports.c_empty_state_PaddingRight = {
  "name": "--pf-c-empty-state--PaddingRight",
  "value": "2rem",
  "var": "var(--pf-c-empty-state--PaddingRight)"
};
exports["default"] = exports.c_empty_state_PaddingRight;
"use strict";
exports.__esModule = true;
exports.c_alert_GridTemplateColumns = {
  "name": "--pf-c-alert--GridTemplateColumns",
  "value": "max-content 1fr max-content",
  "var": "var(--pf-c-alert--GridTemplateColumns)"
};
exports["default"] = exports.c_alert_GridTemplateColumns;
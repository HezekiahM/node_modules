"use strict";
exports.__esModule = true;
exports.c_button_m_display_lg_PaddingTop = {
  "name": "--pf-c-button--m-display-lg--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-button--m-display-lg--PaddingTop)"
};
exports["default"] = exports.c_button_m_display_lg_PaddingTop;
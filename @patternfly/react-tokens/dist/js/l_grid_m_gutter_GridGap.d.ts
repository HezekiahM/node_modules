export const l_grid_m_gutter_GridGap: {
  "name": "--pf-l-grid--m-gutter--GridGap",
  "value": "1rem",
  "var": "var(--pf-l-grid--m-gutter--GridGap)"
};
export default l_grid_m_gutter_GridGap;
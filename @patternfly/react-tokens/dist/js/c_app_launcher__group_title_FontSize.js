"use strict";
exports.__esModule = true;
exports.c_app_launcher__group_title_FontSize = {
  "name": "--pf-c-app-launcher__group-title--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-app-launcher__group-title--FontSize)"
};
exports["default"] = exports.c_app_launcher__group_title_FontSize;
"use strict";
exports.__esModule = true;
exports.c_card_m_compact_first_child_PaddingTop = {
  "name": "--pf-c-card--m-compact--first-child--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-card--m-compact--first-child--PaddingTop)"
};
exports["default"] = exports.c_card_m_compact_first_child_PaddingTop;
"use strict";
exports.__esModule = true;
exports.c_button_m_plain_focus_Color = {
  "name": "--pf-c-button--m-plain--focus--Color",
  "value": "#151515",
  "var": "var(--pf-c-button--m-plain--focus--Color)"
};
exports["default"] = exports.c_button_m_plain_focus_Color;
"use strict";
exports.__esModule = true;
exports.c_empty_state__primary_MarginTop = {
  "name": "--pf-c-empty-state__primary--MarginTop",
  "value": "2rem",
  "var": "var(--pf-c-empty-state__primary--MarginTop)"
};
exports["default"] = exports.c_empty_state__primary_MarginTop;
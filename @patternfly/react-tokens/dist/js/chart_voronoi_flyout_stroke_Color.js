"use strict";
exports.__esModule = true;
exports.chart_voronoi_flyout_stroke_Color = {
  "name": "--pf-chart-voronoi--flyout--stroke--Color",
  "value": "#151515",
  "var": "var(--pf-chart-voronoi--flyout--stroke--Color)"
};
exports["default"] = exports.chart_voronoi_flyout_stroke_Color;
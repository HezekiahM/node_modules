export const c_nav__link_hover_after_BorderColor: {
  "name": "--pf-c-nav__link--hover--after--BorderColor",
  "value": "transparent",
  "var": "var(--pf-c-nav__link--hover--after--BorderColor)"
};
export default c_nav__link_hover_after_BorderColor;
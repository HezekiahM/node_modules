"use strict";
exports.__esModule = true;
exports.global_BackgroundColor_200 = {
  "name": "--pf-global--BackgroundColor--200",
  "value": "#f0f0f0",
  "var": "var(--pf-global--BackgroundColor--200)"
};
exports["default"] = exports.global_BackgroundColor_200;
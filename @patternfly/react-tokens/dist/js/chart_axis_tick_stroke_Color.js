"use strict";
exports.__esModule = true;
exports.chart_axis_tick_stroke_Color = {
  "name": "--pf-chart-axis--tick--stroke--Color",
  "value": "#d2d2d2",
  "var": "var(--pf-chart-axis--tick--stroke--Color)"
};
exports["default"] = exports.chart_axis_tick_stroke_Color;
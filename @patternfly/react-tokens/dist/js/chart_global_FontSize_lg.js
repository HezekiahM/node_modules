"use strict";
exports.__esModule = true;
exports.chart_global_FontSize_lg = {
  "name": "--pf-chart-global--FontSize--lg",
  "value": 18,
  "var": "var(--pf-chart-global--FontSize--lg)"
};
exports["default"] = exports.chart_global_FontSize_lg;
"use strict";
exports.__esModule = true;
exports.global_FontWeight_light = {
  "name": "--pf-global--FontWeight--light",
  "value": "300",
  "var": "var(--pf-global--FontWeight--light)"
};
exports["default"] = exports.global_FontWeight_light;
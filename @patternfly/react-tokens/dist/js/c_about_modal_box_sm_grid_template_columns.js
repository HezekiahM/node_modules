"use strict";
exports.__esModule = true;
exports.c_about_modal_box_sm_grid_template_columns = {
  "name": "--pf-c-about-modal-box--sm--grid-template-columns",
  "value": "5fr 1fr",
  "var": "var(--pf-c-about-modal-box--sm--grid-template-columns)"
};
exports["default"] = exports.c_about_modal_box_sm_grid_template_columns;
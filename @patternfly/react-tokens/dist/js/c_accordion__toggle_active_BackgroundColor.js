"use strict";
exports.__esModule = true;
exports.c_accordion__toggle_active_BackgroundColor = {
  "name": "--pf-c-accordion__toggle--active--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-accordion__toggle--active--BackgroundColor)"
};
exports["default"] = exports.c_accordion__toggle_active_BackgroundColor;
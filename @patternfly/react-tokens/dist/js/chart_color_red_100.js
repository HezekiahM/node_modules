"use strict";
exports.__esModule = true;
exports.chart_color_red_100 = {
  "name": "--pf-chart-color-red-100",
  "value": "#c9190b",
  "var": "var(--pf-chart-color-red-100)"
};
exports["default"] = exports.chart_color_red_100;
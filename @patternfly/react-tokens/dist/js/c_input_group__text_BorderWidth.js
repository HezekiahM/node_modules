"use strict";
exports.__esModule = true;
exports.c_input_group__text_BorderWidth = {
  "name": "--pf-c-input-group__text--BorderWidth",
  "value": "1px",
  "var": "var(--pf-c-input-group__text--BorderWidth)"
};
exports["default"] = exports.c_input_group__text_BorderWidth;
"use strict";
exports.__esModule = true;
exports.c_select__menu_Top = {
  "name": "--pf-c-select__menu--Top",
  "value": "calc(100% + 0.25rem)",
  "var": "var(--pf-c-select__menu--Top)"
};
exports["default"] = exports.c_select__menu_Top;
"use strict";
exports.__esModule = true;
exports.c_spinner_AnimationTimingFunction = {
  "name": "--pf-c-spinner--AnimationTimingFunction",
  "value": "cubic-bezier(.77, .005, .315, 1)",
  "var": "var(--pf-c-spinner--AnimationTimingFunction)"
};
exports["default"] = exports.c_spinner_AnimationTimingFunction;
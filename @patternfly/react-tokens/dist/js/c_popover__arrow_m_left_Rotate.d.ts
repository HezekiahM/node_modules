export const c_popover__arrow_m_left_Rotate: {
  "name": "--pf-c-popover__arrow--m-left--Rotate",
  "value": "45deg",
  "var": "var(--pf-c-popover__arrow--m-left--Rotate)"
};
export default c_popover__arrow_m_left_Rotate;
"use strict";
exports.__esModule = true;
exports.c_table_BackgroundColor = {
  "name": "--pf-c-table--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-table--BackgroundColor)"
};
exports["default"] = exports.c_table_BackgroundColor;
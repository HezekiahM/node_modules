export const c_modal_box__description_PaddingTop: {
  "name": "--pf-c-modal-box__description--PaddingTop",
  "value": "0.25rem",
  "var": "var(--pf-c-modal-box__description--PaddingTop)"
};
export default c_modal_box__description_PaddingTop;
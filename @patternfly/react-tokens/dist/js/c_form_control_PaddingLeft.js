"use strict";
exports.__esModule = true;
exports.c_form_control_PaddingLeft = {
  "name": "--pf-c-form-control--PaddingLeft",
  "value": "2rem",
  "var": "var(--pf-c-form-control--PaddingLeft)"
};
exports["default"] = exports.c_form_control_PaddingLeft;
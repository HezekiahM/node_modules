"use strict";
exports.__esModule = true;
exports.c_simple_list__title_PaddingRight = {
  "name": "--pf-c-simple-list__title--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-simple-list__title--PaddingRight)"
};
exports["default"] = exports.c_simple_list__title_PaddingRight;
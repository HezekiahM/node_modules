"use strict";
exports.__esModule = true;
exports.chart_global_Fill_Color_white = {
  "name": "--pf-chart-global--Fill--Color--white",
  "value": "#fff",
  "var": "var(--pf-chart-global--Fill--Color--white)"
};
exports["default"] = exports.chart_global_Fill_Color_white;
export const c_form__group_label_help_PaddingLeft: {
  "name": "--pf-c-form__group-label-help--PaddingLeft",
  "value": "0.25rem",
  "var": "var(--pf-c-form__group-label-help--PaddingLeft)"
};
export default c_form__group_label_help_PaddingLeft;
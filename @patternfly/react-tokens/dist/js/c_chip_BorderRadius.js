"use strict";
exports.__esModule = true;
exports.c_chip_BorderRadius = {
  "name": "--pf-c-chip--BorderRadius",
  "value": "3px",
  "var": "var(--pf-c-chip--BorderRadius)"
};
exports["default"] = exports.c_chip_BorderRadius;
"use strict";
exports.__esModule = true;
exports.c_alert_PaddingTop = {
  "name": "--pf-c-alert--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-alert--PaddingTop)"
};
exports["default"] = exports.c_alert_PaddingTop;
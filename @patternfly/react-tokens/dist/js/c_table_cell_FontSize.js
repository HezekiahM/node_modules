"use strict";
exports.__esModule = true;
exports.c_table_cell_FontSize = {
  "name": "--pf-c-table--cell--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-table--cell--FontSize)"
};
exports["default"] = exports.c_table_cell_FontSize;
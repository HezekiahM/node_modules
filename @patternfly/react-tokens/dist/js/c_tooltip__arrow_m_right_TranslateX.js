"use strict";
exports.__esModule = true;
exports.c_tooltip__arrow_m_right_TranslateX = {
  "name": "--pf-c-tooltip__arrow--m-right--TranslateX",
  "value": "-50%",
  "var": "var(--pf-c-tooltip__arrow--m-right--TranslateX)"
};
exports["default"] = exports.c_tooltip__arrow_m_right_TranslateX;
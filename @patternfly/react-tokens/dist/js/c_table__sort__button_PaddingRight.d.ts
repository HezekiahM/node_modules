export const c_table__sort__button_PaddingRight: {
  "name": "--pf-c-table__sort__button--PaddingRight",
  "value": "0.5rem",
  "var": "var(--pf-c-table__sort__button--PaddingRight)"
};
export default c_table__sort__button_PaddingRight;
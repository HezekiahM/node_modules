"use strict";
exports.__esModule = true;
exports.global_danger_color_200 = {
  "name": "--pf-global--danger-color--200",
  "value": "#a30000",
  "var": "var(--pf-global--danger-color--200)"
};
exports["default"] = exports.global_danger_color_200;
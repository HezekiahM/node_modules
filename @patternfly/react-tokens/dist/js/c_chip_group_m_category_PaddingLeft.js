"use strict";
exports.__esModule = true;
exports.c_chip_group_m_category_PaddingLeft = {
  "name": "--pf-c-chip-group--m-category--PaddingLeft",
  "value": "0.5rem",
  "var": "var(--pf-c-chip-group--m-category--PaddingLeft)"
};
exports["default"] = exports.c_chip_group_m_category_PaddingLeft;
"use strict";
exports.__esModule = true;
exports.c_page__main_section_PaddingTop = {
  "name": "--pf-c-page__main-section--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-page__main-section--PaddingTop)"
};
exports["default"] = exports.c_page__main_section_PaddingTop;
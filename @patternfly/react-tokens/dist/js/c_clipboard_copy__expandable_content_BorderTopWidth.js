"use strict";
exports.__esModule = true;
exports.c_clipboard_copy__expandable_content_BorderTopWidth = {
  "name": "--pf-c-clipboard-copy__expandable-content--BorderTopWidth",
  "value": "0",
  "var": "var(--pf-c-clipboard-copy__expandable-content--BorderTopWidth)"
};
exports["default"] = exports.c_clipboard_copy__expandable_content_BorderTopWidth;
"use strict";
exports.__esModule = true;
exports.c_input_group_c_form_control_MarginRight = {
  "name": "--pf-c-input-group--c-form-control--MarginRight",
  "value": "1px",
  "var": "var(--pf-c-input-group--c-form-control--MarginRight)"
};
exports["default"] = exports.c_input_group_c_form_control_MarginRight;
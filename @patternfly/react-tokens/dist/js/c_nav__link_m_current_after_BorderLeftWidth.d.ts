export const c_nav__link_m_current_after_BorderLeftWidth: {
  "name": "--pf-c-nav__link--m-current--after--BorderLeftWidth",
  "value": "4px",
  "var": "var(--pf-c-nav__link--m-current--after--BorderLeftWidth)"
};
export default c_nav__link_m_current_after_BorderLeftWidth;
"use strict";
exports.__esModule = true;
exports.c_title_m_md_FontWeight = {
  "name": "--pf-c-title--m-md--FontWeight",
  "value": "700",
  "var": "var(--pf-c-title--m-md--FontWeight)"
};
exports["default"] = exports.c_title_m_md_FontWeight;
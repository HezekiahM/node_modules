"use strict";
exports.__esModule = true;
exports.c_tile_PaddingLeft = {
  "name": "--pf-c-tile--PaddingLeft",
  "value": "1.5rem",
  "var": "var(--pf-c-tile--PaddingLeft)"
};
exports["default"] = exports.c_tile_PaddingLeft;
"use strict";
exports.__esModule = true;
exports.l_flex_spacer_xl = {
  "name": "--pf-l-flex--spacer--xl",
  "value": "2rem",
  "var": "var(--pf-l-flex--spacer--xl)"
};
exports["default"] = exports.l_flex_spacer_xl;
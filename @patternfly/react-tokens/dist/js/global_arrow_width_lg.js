"use strict";
exports.__esModule = true;
exports.global_arrow_width_lg = {
  "name": "--pf-global--arrow--width-lg",
  "value": "1.5625rem",
  "var": "var(--pf-global--arrow--width-lg)"
};
exports["default"] = exports.global_arrow_width_lg;
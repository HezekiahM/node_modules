"use strict";
exports.__esModule = true;
exports.c_banner_PaddingRight = {
  "name": "--pf-c-banner--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-banner--PaddingRight)"
};
exports["default"] = exports.c_banner_PaddingRight;
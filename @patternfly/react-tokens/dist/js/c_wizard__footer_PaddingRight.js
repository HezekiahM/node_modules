"use strict";
exports.__esModule = true;
exports.c_wizard__footer_PaddingRight = {
  "name": "--pf-c-wizard__footer--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-wizard__footer--PaddingRight)"
};
exports["default"] = exports.c_wizard__footer_PaddingRight;
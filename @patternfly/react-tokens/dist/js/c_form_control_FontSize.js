"use strict";
exports.__esModule = true;
exports.c_form_control_FontSize = {
  "name": "--pf-c-form-control--FontSize",
  "value": "1rem",
  "var": "var(--pf-c-form-control--FontSize)"
};
exports["default"] = exports.c_form_control_FontSize;
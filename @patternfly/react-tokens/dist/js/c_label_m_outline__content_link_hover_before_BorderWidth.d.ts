export const c_label_m_outline__content_link_hover_before_BorderWidth: {
  "name": "--pf-c-label--m-outline__content--link--hover--before--BorderWidth",
  "value": "2px",
  "var": "var(--pf-c-label--m-outline__content--link--hover--before--BorderWidth)"
};
export default c_label_m_outline__content_link_hover_before_BorderWidth;
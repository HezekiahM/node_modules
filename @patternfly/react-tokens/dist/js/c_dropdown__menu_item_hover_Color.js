"use strict";
exports.__esModule = true;
exports.c_dropdown__menu_item_hover_Color = {
  "name": "--pf-c-dropdown__menu-item--hover--Color",
  "value": "#151515",
  "var": "var(--pf-c-dropdown__menu-item--hover--Color)"
};
exports["default"] = exports.c_dropdown__menu_item_hover_Color;
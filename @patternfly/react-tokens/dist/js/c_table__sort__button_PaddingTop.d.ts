export const c_table__sort__button_PaddingTop: {
  "name": "--pf-c-table__sort__button--PaddingTop",
  "value": "0.375rem",
  "var": "var(--pf-c-table__sort__button--PaddingTop)"
};
export default c_table__sort__button_PaddingTop;
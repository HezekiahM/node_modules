"use strict";
exports.__esModule = true;
exports.c_button_m_tertiary_active_after_BorderColor = {
  "name": "--pf-c-button--m-tertiary--active--after--BorderColor",
  "value": "#151515",
  "var": "var(--pf-c-button--m-tertiary--active--after--BorderColor)"
};
exports["default"] = exports.c_button_m_tertiary_active_after_BorderColor;
"use strict";
exports.__esModule = true;
exports.c_nav__toggle_icon_Transition = {
  "name": "--pf-c-nav__toggle-icon--Transition",
  "value": "250ms",
  "var": "var(--pf-c-nav__toggle-icon--Transition)"
};
exports["default"] = exports.c_nav__toggle_icon_Transition;
"use strict";
exports.__esModule = true;
exports.c_page__header_nav_xl_PaddingLeft = {
  "name": "--pf-c-page__header-nav--xl--PaddingLeft",
  "value": "2rem",
  "var": "var(--pf-c-page__header-nav--xl--PaddingLeft)"
};
exports["default"] = exports.c_page__header_nav_xl_PaddingLeft;
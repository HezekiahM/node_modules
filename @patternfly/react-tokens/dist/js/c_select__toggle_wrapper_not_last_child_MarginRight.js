"use strict";
exports.__esModule = true;
exports.c_select__toggle_wrapper_not_last_child_MarginRight = {
  "name": "--pf-c-select__toggle-wrapper--not-last-child--MarginRight",
  "value": "0.25rem",
  "var": "var(--pf-c-select__toggle-wrapper--not-last-child--MarginRight)"
};
exports["default"] = exports.c_select__toggle_wrapper_not_last_child_MarginRight;
"use strict";
exports.__esModule = true;
exports.c_wizard__header_BackgroundColor = {
  "name": "--pf-c-wizard__header--BackgroundColor",
  "value": "#151515",
  "var": "var(--pf-c-wizard__header--BackgroundColor)"
};
exports["default"] = exports.c_wizard__header_BackgroundColor;
"use strict";
exports.__esModule = true;
exports.c_form_control_readonly_hover_BorderBottomColor = {
  "name": "--pf-c-form-control--readonly--hover--BorderBottomColor",
  "value": "#8a8d90",
  "var": "var(--pf-c-form-control--readonly--hover--BorderBottomColor)"
};
exports["default"] = exports.c_form_control_readonly_hover_BorderBottomColor;
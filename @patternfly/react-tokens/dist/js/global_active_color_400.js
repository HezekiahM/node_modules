"use strict";
exports.__esModule = true;
exports.global_active_color_400 = {
  "name": "--pf-global--active-color--400",
  "value": "#2b9af3",
  "var": "var(--pf-global--active-color--400)"
};
exports["default"] = exports.global_active_color_400;
"use strict";
exports.__esModule = true;
exports.c_clipboard_copy__expandable_content_BorderColor = {
  "name": "--pf-c-clipboard-copy__expandable-content--BorderColor",
  "value": "#d2d2d2",
  "var": "var(--pf-c-clipboard-copy__expandable-content--BorderColor)"
};
exports["default"] = exports.c_clipboard_copy__expandable_content_BorderColor;
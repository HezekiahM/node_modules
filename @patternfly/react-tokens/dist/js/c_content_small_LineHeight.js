"use strict";
exports.__esModule = true;
exports.c_content_small_LineHeight = {
  "name": "--pf-c-content--small--LineHeight",
  "value": "1.5",
  "var": "var(--pf-c-content--small--LineHeight)"
};
exports["default"] = exports.c_content_small_LineHeight;
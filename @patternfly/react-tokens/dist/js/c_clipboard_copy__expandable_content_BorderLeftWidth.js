"use strict";
exports.__esModule = true;
exports.c_clipboard_copy__expandable_content_BorderLeftWidth = {
  "name": "--pf-c-clipboard-copy__expandable-content--BorderLeftWidth",
  "value": "1px",
  "var": "var(--pf-c-clipboard-copy__expandable-content--BorderLeftWidth)"
};
exports["default"] = exports.c_clipboard_copy__expandable_content_BorderLeftWidth;
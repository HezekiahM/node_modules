"use strict";
exports.__esModule = true;
exports.chart_boxplot_min_stroke_Color = {
  "name": "--pf-chart-boxplot--min--stroke--Color",
  "value": "#151515",
  "var": "var(--pf-chart-boxplot--min--stroke--Color)"
};
exports["default"] = exports.chart_boxplot_min_stroke_Color;
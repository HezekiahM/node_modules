"use strict";
exports.__esModule = true;
exports.global_palette_light_blue_400 = {
  "name": "--pf-global--palette--light-blue-400",
  "value": "#00b9e4",
  "var": "var(--pf-global--palette--light-blue-400)"
};
exports["default"] = exports.global_palette_light_blue_400;
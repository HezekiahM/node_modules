"use strict";
exports.__esModule = true;
exports.c_nav__link_hover_BackgroundColor = {
  "name": "--pf-c-nav__link--hover--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-nav__link--hover--BackgroundColor)"
};
exports["default"] = exports.c_nav__link_hover_BackgroundColor;
"use strict";
exports.__esModule = true;
exports.c_options_menu__toggle_button_BackgroundColor = {
  "name": "--pf-c-options-menu__toggle-button--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-options-menu__toggle-button--BackgroundColor)"
};
exports["default"] = exports.c_options_menu__toggle_button_BackgroundColor;
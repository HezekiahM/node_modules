"use strict";
exports.__esModule = true;
exports.c_table__compound_expansion_toggle__button_before_BorderRightWidth = {
  "name": "--pf-c-table__compound-expansion-toggle__button--before--BorderRightWidth",
  "value": "1px",
  "var": "var(--pf-c-table__compound-expansion-toggle__button--before--BorderRightWidth)"
};
exports["default"] = exports.c_table__compound_expansion_toggle__button_before_BorderRightWidth;
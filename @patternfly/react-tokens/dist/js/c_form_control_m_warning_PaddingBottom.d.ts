export const c_form_control_m_warning_PaddingBottom: {
  "name": "--pf-c-form-control--m-warning--PaddingBottom",
  "value": "calc(0.375rem - 2px)",
  "var": "var(--pf-c-form-control--m-warning--PaddingBottom)"
};
export default c_form_control_m_warning_PaddingBottom;
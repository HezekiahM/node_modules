"use strict";
exports.__esModule = true;
exports.c_button__icon_m_start_MarginRight = {
  "name": "--pf-c-button__icon--m-start--MarginRight",
  "value": "0.25rem",
  "var": "var(--pf-c-button__icon--m-start--MarginRight)"
};
exports["default"] = exports.c_button__icon_m_start_MarginRight;
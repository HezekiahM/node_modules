"use strict";
exports.__esModule = true;
exports.c_button_m_plain_disabled_BackgroundColor = {
  "name": "--pf-c-button--m-plain--disabled--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-button--m-plain--disabled--BackgroundColor)"
};
exports["default"] = exports.c_button_m_plain_disabled_BackgroundColor;
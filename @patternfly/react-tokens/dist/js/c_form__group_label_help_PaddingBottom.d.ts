export const c_form__group_label_help_PaddingBottom: {
  "name": "--pf-c-form__group-label-help--PaddingBottom",
  "value": "0.25rem",
  "var": "var(--pf-c-form__group-label-help--PaddingBottom)"
};
export default c_form__group_label_help_PaddingBottom;
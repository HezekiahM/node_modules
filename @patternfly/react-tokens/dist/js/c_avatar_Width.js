"use strict";
exports.__esModule = true;
exports.c_avatar_Width = {
  "name": "--pf-c-avatar--Width",
  "value": "2.25rem",
  "var": "var(--pf-c-avatar--Width)"
};
exports["default"] = exports.c_avatar_Width;
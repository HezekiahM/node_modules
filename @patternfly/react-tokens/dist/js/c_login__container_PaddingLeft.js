"use strict";
exports.__esModule = true;
exports.c_login__container_PaddingLeft = {
  "name": "--pf-c-login__container--PaddingLeft",
  "value": "6.125rem",
  "var": "var(--pf-c-login__container--PaddingLeft)"
};
exports["default"] = exports.c_login__container_PaddingLeft;
"use strict";
exports.__esModule = true;
exports.c_dropdown__group_title_PaddingBottom = {
  "name": "--pf-c-dropdown__group-title--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-dropdown__group-title--PaddingBottom)"
};
exports["default"] = exports.c_dropdown__group_title_PaddingBottom;
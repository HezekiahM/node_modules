export const c_dropdown__toggle_active_before_BorderBottomWidth: {
  "name": "--pf-c-dropdown__toggle--active--before--BorderBottomWidth",
  "value": "2px",
  "var": "var(--pf-c-dropdown__toggle--active--before--BorderBottomWidth)"
};
export default c_dropdown__toggle_active_before_BorderBottomWidth;
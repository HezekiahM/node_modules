"use strict";
exports.__esModule = true;
exports.c_wizard__toggle_PaddingLeft = {
  "name": "--pf-c-wizard__toggle--PaddingLeft",
  "value": "calc(1rem + 1.5rem + 0.5rem)",
  "var": "var(--pf-c-wizard__toggle--PaddingLeft)"
};
exports["default"] = exports.c_wizard__toggle_PaddingLeft;
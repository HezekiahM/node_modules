"use strict";
exports.__esModule = true;
exports.chart_donut_label_subtitle_position = {
  "name": "--pf-chart-donut--label--subtitle--position",
  "value": "center",
  "var": "var(--pf-chart-donut--label--subtitle--position)"
};
exports["default"] = exports.chart_donut_label_subtitle_position;
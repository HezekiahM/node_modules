export const l_gallery: {
  ".pf-l-gallery": {
    "l_gallery_m_gutter_GridGap": {
      "name": "--pf-l-gallery--m-gutter--GridGap",
      "value": "1rem",
      "values": [
        "--pf-global--gutter",
        "$pf-global--gutter",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "l_gallery_GridTemplateColumns": {
      "name": "--pf-l-gallery--GridTemplateColumns",
      "value": "repeat(auto-fill, minmax(250px, 1fr))"
    },
    "l_gallery_GridTemplateRows": {
      "name": "--pf-l-gallery--GridTemplateRows",
      "value": "auto"
    }
  }
};
export default l_gallery;
export const c_nav_m_tertiary__link_hover_BackgroundColor: {
  "name": "--pf-c-nav--m-tertiary__link--hover--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-nav--m-tertiary__link--hover--BackgroundColor)"
};
export default c_nav_m_tertiary__link_hover_BackgroundColor;
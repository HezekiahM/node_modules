"use strict";
exports.__esModule = true;
exports.c_chip_PaddingTop = {
  "name": "--pf-c-chip--PaddingTop",
  "value": "0.25rem",
  "var": "var(--pf-c-chip--PaddingTop)"
};
exports["default"] = exports.c_chip_PaddingTop;
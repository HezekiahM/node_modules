"use strict";
exports.__esModule = true;
exports.c_tile_PaddingBottom = {
  "name": "--pf-c-tile--PaddingBottom",
  "value": "1.5rem",
  "var": "var(--pf-c-tile--PaddingBottom)"
};
exports["default"] = exports.c_tile_PaddingBottom;
"use strict";
exports.__esModule = true;
exports.global_FontWeight_semi_bold = {
  "name": "--pf-global--FontWeight--semi-bold",
  "value": "500",
  "var": "var(--pf-global--FontWeight--semi-bold)"
};
exports["default"] = exports.global_FontWeight_semi_bold;
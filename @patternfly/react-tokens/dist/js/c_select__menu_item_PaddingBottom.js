"use strict";
exports.__esModule = true;
exports.c_select__menu_item_PaddingBottom = {
  "name": "--pf-c-select__menu-item--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-select__menu-item--PaddingBottom)"
};
exports["default"] = exports.c_select__menu_item_PaddingBottom;
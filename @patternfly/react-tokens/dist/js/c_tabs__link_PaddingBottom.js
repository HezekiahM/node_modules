"use strict";
exports.__esModule = true;
exports.c_tabs__link_PaddingBottom = {
  "name": "--pf-c-tabs__link--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-tabs__link--PaddingBottom)"
};
exports["default"] = exports.c_tabs__link_PaddingBottom;
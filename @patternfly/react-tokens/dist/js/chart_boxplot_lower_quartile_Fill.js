"use strict";
exports.__esModule = true;
exports.chart_boxplot_lower_quartile_Fill = {
  "name": "--pf-chart-boxplot--lower-quartile--Fill",
  "value": "#8a8d90",
  "var": "var(--pf-chart-boxplot--lower-quartile--Fill)"
};
exports["default"] = exports.chart_boxplot_lower_quartile_Fill;
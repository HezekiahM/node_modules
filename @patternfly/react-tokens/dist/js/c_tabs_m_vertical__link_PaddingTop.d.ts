export const c_tabs_m_vertical__link_PaddingTop: {
  "name": "--pf-c-tabs--m-vertical__link--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-tabs--m-vertical__link--PaddingTop)"
};
export default c_tabs_m_vertical__link_PaddingTop;
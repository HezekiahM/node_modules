"use strict";
exports.__esModule = true;
exports.c_tabs_m_box__link_BackgroundColor = {
  "name": "--pf-c-tabs--m-box__link--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-tabs--m-box__link--BackgroundColor)"
};
exports["default"] = exports.c_tabs_m_box__link_BackgroundColor;
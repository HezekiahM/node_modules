"use strict";
exports.__esModule = true;
exports.c_form_control__select_success_BackgroundPosition = {
  "name": "--pf-c-form-control__select--success--BackgroundPosition",
  "value": "calc(100% - 0.5rem - 1.5rem)",
  "var": "var(--pf-c-form-control__select--success--BackgroundPosition)"
};
exports["default"] = exports.c_form_control__select_success_BackgroundPosition;
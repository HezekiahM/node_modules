"use strict";
exports.__esModule = true;
exports.c_notification_drawer__group_toggle_PaddingRight = {
  "name": "--pf-c-notification-drawer__group-toggle--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-notification-drawer__group-toggle--PaddingRight)"
};
exports["default"] = exports.c_notification_drawer__group_toggle_PaddingRight;
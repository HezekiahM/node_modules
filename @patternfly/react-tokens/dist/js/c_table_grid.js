"use strict";
exports.__esModule = true;
exports.c_table_grid = {
  ".pf-c-table[class*=\"pf-m-grid\"]": {
    "c_table_responsive_BorderColor": {
      "name": "--pf-c-table--responsive--BorderColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_table_tbody_responsive_border_width_base": {
      "name": "--pf-c-table--tbody--responsive--border-width--base",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_table_tbody_after_border_width_base": {
      "name": "--pf-c-table--tbody--after--border-width--base",
      "value": "3px",
      "values": [
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    },
    "c_table_tbody_after_BorderLeftWidth": {
      "name": "--pf-c-table--tbody--after--BorderLeftWidth",
      "value": "0"
    },
    "c_table_tbody_after_BorderColor": {
      "name": "--pf-c-table--tbody--after--BorderColor",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_table_tr_responsive_border_width_base": {
      "name": "--pf-c-table-tr--responsive--border-width--base",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_table_tr_responsive_last_child_BorderBottomWidth": {
      "name": "--pf-c-table-tr--responsive--last-child--BorderBottomWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_table_tr_responsive_GridColumnGap": {
      "name": "--pf-c-table-tr--responsive--GridColumnGap",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_table_tr_responsive_MarginTop": {
      "name": "--pf-c-table-tr--responsive--MarginTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_table_tr_responsive_PaddingTop": {
      "name": "--pf-c-table-tr--responsive--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_table_tr_responsive_PaddingRight": {
      "name": "--pf-c-table-tr--responsive--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_table_tr_responsive_xl_PaddingRight": {
      "name": "--pf-c-table-tr--responsive--xl--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_table_tr_responsive_PaddingBottom": {
      "name": "--pf-c-table-tr--responsive--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_table_tr_responsive_PaddingLeft": {
      "name": "--pf-c-table-tr--responsive--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_table_tr_responsive_xl_PaddingLeft": {
      "name": "--pf-c-table-tr--responsive--xl--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_table_tr_responsive_nested_table_PaddingTop": {
      "name": "--pf-c-table-tr--responsive--nested-table--PaddingTop",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_table_tr_responsive_nested_table_PaddingRight": {
      "name": "--pf-c-table-tr--responsive--nested-table--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_table_tr_responsive_nested_table_PaddingBottom": {
      "name": "--pf-c-table-tr--responsive--nested-table--PaddingBottom",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_table_tr_responsive_nested_table_PaddingLeft": {
      "name": "--pf-c-table-tr--responsive--nested-table--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_table_m_grid_cell_hidden_visible_Display": {
      "name": "--pf-c-table--m-grid--cell--hidden-visible--Display",
      "value": "grid"
    },
    "c_table_m_grid_cell_PaddingTop": {
      "name": "--pf-c-table--m-grid--cell--PaddingTop",
      "value": "0"
    },
    "c_table_m_grid_cell_PaddingRight": {
      "name": "--pf-c-table--m-grid--cell--PaddingRight",
      "value": "0"
    },
    "c_table_m_grid_cell_PaddingBottom": {
      "name": "--pf-c-table--m-grid--cell--PaddingBottom",
      "value": "0"
    },
    "c_table_m_grid_cell_PaddingLeft": {
      "name": "--pf-c-table--m-grid--cell--PaddingLeft",
      "value": "0"
    },
    "c_table_td_responsive_GridColumnGap": {
      "name": "--pf-c-table-td--responsive--GridColumnGap",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_table_cell_responsive_PaddingTop": {
      "name": "--pf-c-table--cell--responsive--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_table_cell_responsive_PaddingBottom": {
      "name": "--pf-c-table--cell--responsive--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_table_cell_first_child_responsive_PaddingTop": {
      "name": "--pf-c-table--cell--first-child--responsive--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_table_cell_responsive_PaddingRight": {
      "name": "--pf-c-table--cell--responsive--PaddingRight",
      "value": "0"
    },
    "c_table_cell_responsive_PaddingLeft": {
      "name": "--pf-c-table--cell--responsive--PaddingLeft",
      "value": "0"
    },
    "c_table_m_compact_tr_responsive_PaddingTop": {
      "name": "--pf-c-table--m-compact-tr--responsive--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_table_m_compact_tr_responsive_PaddingBottom": {
      "name": "--pf-c-table--m-compact-tr--responsive--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_table_m_compact_tr_td_responsive_PaddingTop": {
      "name": "--pf-c-table--m-compact-tr-td--responsive--PaddingTop",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_table_m_compact_tr_td_responsive_PaddingBottom": {
      "name": "--pf-c-table--m-compact-tr-td--responsive--PaddingBottom",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_table_m_compact__action_responsive_MarginTop": {
      "name": "--pf-c-table--m-compact__action--responsive--MarginTop",
      "value": "calc(0.25rem * -1)",
      "values": [
        "calc(--pf-global--spacer--xs * -1)",
        "calc($pf-global--spacer--xs * -1)",
        "calc(pf-size-prem(4px) * -1)",
        "calc(0.25rem * -1)"
      ]
    },
    "c_table_m_compact__action_responsive_MarginBottom": {
      "name": "--pf-c-table--m-compact__action--responsive--MarginBottom",
      "value": "calc(0.25rem * -1)",
      "values": [
        "calc(--pf-global--spacer--xs * -1)",
        "calc($pf-global--spacer--xs * -1)",
        "calc(pf-size-prem(4px) * -1)",
        "calc(0.25rem * -1)"
      ]
    },
    "c_table_m_compact__toggle_c_button_responsive_MarginBottom": {
      "name": "--pf-c-table--m-compact__toggle--c-button--responsive--MarginBottom",
      "value": "calc(0.375rem * -1)"
    },
    "c_table__expandable_row_content_responsive_PaddingRight": {
      "name": "--pf-c-table__expandable-row-content--responsive--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_table__expandable_row_content_responsive_PaddingLeft": {
      "name": "--pf-c-table__expandable-row-content--responsive--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_table__expandable_row_content_responsive_xl_PaddingRight": {
      "name": "--pf-c-table__expandable-row-content--responsive--xl--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_table__expandable_row_content_responsive_xl_PaddingLeft": {
      "name": "--pf-c-table__expandable-row-content--responsive--xl--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_table__expandable_row_content_BackgroundColor": {
      "name": "--pf-c-table__expandable-row-content--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_table__check_responsive_MarginLeft": {
      "name": "--pf-c-table__check--responsive--MarginLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_table__check_responsive_MarginTop": {
      "name": "--pf-c-table__check--responsive--MarginTop",
      "value": "0.375rem"
    },
    "c_table__action_responsive_MarginLeft": {
      "name": "--pf-c-table__action--responsive--MarginLeft",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_table__toggle__icon_Transition": {
      "name": "--pf-c-table__toggle__icon--Transition",
      "value": ".2s ease-in 0s"
    },
    "c_table__toggle_m_expanded__icon_Rotate": {
      "name": "--pf-c-table__toggle--m-expanded__icon--Rotate",
      "value": "180deg"
    }
  },
  ".pf-m-grid.pf-c-table": {
    "c_table_cell_PaddingTop": {
      "name": "--pf-c-table--cell--PaddingTop",
      "value": "0",
      "values": [
        "--pf-c-table--m-grid--cell--PaddingTop",
        "0"
      ]
    },
    "c_table_cell_PaddingRight": {
      "name": "--pf-c-table--cell--PaddingRight",
      "value": "0",
      "values": [
        "--pf-c-table--m-grid--cell--PaddingRight",
        "0"
      ]
    },
    "c_table_cell_PaddingBottom": {
      "name": "--pf-c-table--cell--PaddingBottom",
      "value": "0",
      "values": [
        "--pf-c-table--m-grid--cell--PaddingBottom",
        "0"
      ]
    },
    "c_table_cell_PaddingLeft": {
      "name": "--pf-c-table--cell--PaddingLeft",
      "value": "0",
      "values": [
        "--pf-c-table--m-grid--cell--PaddingLeft",
        "0"
      ]
    }
  },
  ".pf-m-grid.pf-c-table tr:not(.pf-c-table__expandable-row) > *:first-child": {
    "c_table_cell_responsive_PaddingTop": {
      "name": "--pf-c-table--cell--responsive--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-c-table--cell--first-child--responsive--PaddingTop",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-m-grid.pf-c-table.pf-m-compact": {
    "c_table_tr_responsive_PaddingTop": {
      "name": "--pf-c-table-tr--responsive--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-c-table--m-compact-tr--responsive--PaddingTop",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_table_tr_responsive_PaddingBottom": {
      "name": "--pf-c-table-tr--responsive--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-c-table--m-compact-tr--responsive--PaddingBottom",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_table_cell_responsive_PaddingTop": {
      "name": "--pf-c-table--cell--responsive--PaddingTop",
      "value": "0.25rem",
      "values": [
        "--pf-c-table--m-compact-tr-td--responsive--PaddingTop",
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_table_cell_responsive_PaddingBottom": {
      "name": "--pf-c-table--cell--responsive--PaddingBottom",
      "value": "0.25rem",
      "values": [
        "--pf-c-table--m-compact-tr-td--responsive--PaddingBottom",
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_table__check_input_MarginTop": {
      "name": "--pf-c-table__check--input--MarginTop",
      "value": "0"
    }
  },
  ".pf-m-grid.pf-c-table [data-label]": {
    "c_table_cell_hidden_visible_Display": {
      "name": "--pf-c-table--cell--hidden-visible--Display",
      "value": "grid",
      "values": [
        "--pf-c-table--m-grid--cell--hidden-visible--Display",
        "grid"
      ]
    }
  },
  ".pf-m-grid.pf-c-table tr > *:first-child": {
    "c_table_cell_PaddingLeft": {
      "name": "--pf-c-table--cell--PaddingLeft",
      "value": "0"
    }
  },
  ".pf-m-grid.pf-c-table tr > *:last-child": {
    "c_table_cell_PaddingRight": {
      "name": "--pf-c-table--cell--PaddingRight",
      "value": "0"
    }
  },
  ".pf-m-grid.pf-c-table .pf-c-table": {
    "c_table_tr_responsive_PaddingTop": {
      "name": "--pf-c-table-tr--responsive--PaddingTop",
      "value": "2rem",
      "values": [
        "--pf-c-table-tr--responsive--nested-table--PaddingTop",
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_table_tr_responsive_PaddingRight": {
      "name": "--pf-c-table-tr--responsive--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-c-table-tr--responsive--nested-table--PaddingRight",
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_table_tr_responsive_PaddingBottom": {
      "name": "--pf-c-table-tr--responsive--PaddingBottom",
      "value": "2rem",
      "values": [
        "--pf-c-table-tr--responsive--nested-table--PaddingBottom",
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_table_tr_responsive_PaddingLeft": {
      "name": "--pf-c-table-tr--responsive--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-c-table-tr--responsive--nested-table--PaddingLeft",
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    }
  },
  ".pf-m-grid.pf-c-table .pf-c-table tr:not(.pf-c-table__expandable-row) + tr:not(.pf-c-table__expandable-row)": {
    "c_table_tr_responsive_PaddingTop": {
      "name": "--pf-c-table-tr--responsive--PaddingTop",
      "value": "0"
    }
  },
  ".pf-m-grid.pf-c-table .pf-c-table__compound-expansion-toggle": {
    "c_table__compound_expansion_toggle__button_before_BorderRightWidth": {
      "name": "--pf-c-table__compound-expansion-toggle__button--before--BorderRightWidth",
      "value": "0"
    },
    "c_table__compound_expansion_toggle__button_before_BorderLeftWidth": {
      "name": "--pf-c-table__compound-expansion-toggle__button--before--BorderLeftWidth",
      "value": "0"
    },
    "c_table__compound_expansion_toggle__button_after_Top": {
      "name": "--pf-c-table__compound-expansion-toggle__button--after--Top",
      "value": "100%"
    }
  },
  ".pf-m-grid.pf-c-table tbody.pf-m-expanded": {
    "c_table_tbody_after_BorderLeftWidth": {
      "name": "--pf-c-table--tbody--after--BorderLeftWidth",
      "value": "3px",
      "values": [
        "--pf-c-table--tbody--after--border-width--base",
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    }
  },
  ".pf-m-grid.pf-c-table tbody.pf-m-expanded tbody": {
    "c_table_tbody_after_BorderLeftWidth": {
      "name": "--pf-c-table--tbody--after--BorderLeftWidth",
      "value": "0"
    }
  },
  ".pf-m-grid.pf-c-table tbody > tr > :first-child:not(.pf-c-table__check)::after": {
    "c_table__expandable_row_after_BorderLeftWidth": {
      "name": "--pf-c-table__expandable-row--after--BorderLeftWidth",
      "value": "0"
    }
  },
  ".pf-m-grid.pf-c-table .pf-c-table__expandable-row": {
    "c_table_cell_responsive_PaddingTop": {
      "name": "--pf-c-table--cell--responsive--PaddingTop",
      "value": "0"
    },
    "c_table_cell_responsive_PaddingRight": {
      "name": "--pf-c-table--cell--responsive--PaddingRight",
      "value": "0"
    },
    "c_table_cell_responsive_PaddingBottom": {
      "name": "--pf-c-table--cell--responsive--PaddingBottom",
      "value": "0"
    },
    "c_table_cell_responsive_PaddingLeft": {
      "name": "--pf-c-table--cell--responsive--PaddingLeft",
      "value": "0"
    },
    "c_table_cell_PaddingRight": {
      "name": "--pf-c-table--cell--PaddingRight",
      "value": "0"
    },
    "c_table_cell_PaddingLeft": {
      "name": "--pf-c-table--cell--PaddingLeft",
      "value": "0"
    }
  },
  ".pf-m-grid.pf-c-table .pf-c-table__button": {
    "c_table_cell_PaddingTop": {
      "name": "--pf-c-table--cell--PaddingTop",
      "value": "0",
      "values": [
        "--pf-c-table--m-grid--cell--PaddingTop",
        "0"
      ]
    },
    "c_table_cell_PaddingRight": {
      "name": "--pf-c-table--cell--PaddingRight",
      "value": "0",
      "values": [
        "--pf-c-table--m-grid--cell--PaddingRight",
        "0"
      ]
    },
    "c_table_cell_PaddingBottom": {
      "name": "--pf-c-table--cell--PaddingBottom",
      "value": "0",
      "values": [
        "--pf-c-table--m-grid--cell--PaddingBottom",
        "0"
      ]
    },
    "c_table_cell_PaddingLeft": {
      "name": "--pf-c-table--cell--PaddingLeft",
      "value": "0",
      "values": [
        "--pf-c-table--m-grid--cell--PaddingLeft",
        "0"
      ]
    }
  },
  ".pf-m-grid.pf-c-table .pf-m-nowrap": {
    "c_table_cell_Overflow": {
      "name": "--pf-c-table--cell--Overflow",
      "value": "auto"
    }
  },
  ".pf-m-grid.pf-c-table .pf-m-truncate": {
    "c_table_cell_MaxWidth": {
      "name": "--pf-c-table--cell--MaxWidth",
      "value": "100%"
    }
  },
  ".pf-m-grid.pf-c-table [class*=\"pf-m-width\"]": {
    "c_table_cell_Width": {
      "name": "--pf-c-table--cell--Width",
      "value": "auto"
    }
  }
};
exports["default"] = exports.c_table_grid;
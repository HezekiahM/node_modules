"use strict";
exports.__esModule = true;
exports.c_label__content_link_focus_before_BorderWidth = {
  "name": "--pf-c-label__content--link--focus--before--BorderWidth",
  "value": "1px",
  "var": "var(--pf-c-label__content--link--focus--before--BorderWidth)"
};
exports["default"] = exports.c_label__content_link_focus_before_BorderWidth;
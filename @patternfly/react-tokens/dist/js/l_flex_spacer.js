"use strict";
exports.__esModule = true;
exports.l_flex_spacer = {
  "name": "--pf-l-flex--spacer",
  "value": "5rem",
  "var": "var(--pf-l-flex--spacer)"
};
exports["default"] = exports.l_flex_spacer;
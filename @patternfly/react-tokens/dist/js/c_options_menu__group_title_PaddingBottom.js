"use strict";
exports.__esModule = true;
exports.c_options_menu__group_title_PaddingBottom = {
  "name": "--pf-c-options-menu__group-title--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-options-menu__group-title--PaddingBottom)"
};
exports["default"] = exports.c_options_menu__group_title_PaddingBottom;
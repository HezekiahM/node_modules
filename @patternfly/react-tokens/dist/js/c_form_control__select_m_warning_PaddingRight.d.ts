export const c_form_control__select_m_warning_PaddingRight: {
  "name": "--pf-c-form-control__select--m-warning--PaddingRight",
  "value": "calc(0.5rem + 3rem)",
  "var": "var(--pf-c-form-control__select--m-warning--PaddingRight)"
};
export default c_form_control__select_m_warning_PaddingRight;
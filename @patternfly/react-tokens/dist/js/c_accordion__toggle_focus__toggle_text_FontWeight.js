"use strict";
exports.__esModule = true;
exports.c_accordion__toggle_focus__toggle_text_FontWeight = {
  "name": "--pf-c-accordion__toggle--focus__toggle-text--FontWeight",
  "value": "700",
  "var": "var(--pf-c-accordion__toggle--focus__toggle-text--FontWeight)"
};
exports["default"] = exports.c_accordion__toggle_focus__toggle_text_FontWeight;
"use strict";
exports.__esModule = true;
exports.c_dropdown__group_title_PaddingRight = {
  "name": "--pf-c-dropdown__group-title--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-dropdown__group-title--PaddingRight)"
};
exports["default"] = exports.c_dropdown__group_title_PaddingRight;
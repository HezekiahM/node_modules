"use strict";
exports.__esModule = true;
exports.l_flex_m_row_AlignItems = {
  "name": "--pf-l-flex--m-row--AlignItems",
  "value": "baseline",
  "var": "var(--pf-l-flex--m-row--AlignItems)"
};
exports["default"] = exports.l_flex_m_row_AlignItems;
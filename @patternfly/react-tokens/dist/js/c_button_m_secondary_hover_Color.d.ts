export const c_button_m_secondary_hover_Color: {
  "name": "--pf-c-button--m-secondary--hover--Color",
  "value": "#fff",
  "var": "var(--pf-c-button--m-secondary--hover--Color)"
};
export default c_button_m_secondary_hover_Color;
"use strict";
exports.__esModule = true;
exports.c_dropdown__menu_PaddingBottom = {
  "name": "--pf-c-dropdown__menu--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-dropdown__menu--PaddingBottom)"
};
exports["default"] = exports.c_dropdown__menu_PaddingBottom;
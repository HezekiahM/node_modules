export const c_label_FontSize: {
  "name": "--pf-c-label--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-label--FontSize)"
};
export default c_label_FontSize;
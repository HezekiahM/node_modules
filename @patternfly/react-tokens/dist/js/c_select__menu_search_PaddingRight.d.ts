export const c_select__menu_search_PaddingRight: {
  "name": "--pf-c-select__menu-search--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-select__menu-search--PaddingRight)"
};
export default c_select__menu_search_PaddingRight;
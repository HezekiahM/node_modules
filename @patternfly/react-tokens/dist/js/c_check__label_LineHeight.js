"use strict";
exports.__esModule = true;
exports.c_check__label_LineHeight = {
  "name": "--pf-c-check__label--LineHeight",
  "value": "1.3",
  "var": "var(--pf-c-check__label--LineHeight)"
};
exports["default"] = exports.c_check__label_LineHeight;
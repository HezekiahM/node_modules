"use strict";
exports.__esModule = true;
exports.c_banner_m_danger_BackgroundColor = {
  "name": "--pf-c-banner--m-danger--BackgroundColor",
  "value": "#c9190b",
  "var": "var(--pf-c-banner--m-danger--BackgroundColor)"
};
exports["default"] = exports.c_banner_m_danger_BackgroundColor;
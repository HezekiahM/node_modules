"use strict";
exports.__esModule = true;
exports.c_table_m_compact__expandable_row_content_PaddingTop = {
  "name": "--pf-c-table--m-compact__expandable-row-content--PaddingTop",
  "value": "1.5rem",
  "var": "var(--pf-c-table--m-compact__expandable-row-content--PaddingTop)"
};
exports["default"] = exports.c_table_m_compact__expandable_row_content_PaddingTop;
"use strict";
exports.__esModule = true;
exports.c_wizard__nav_lg_BorderRightColor = {
  "name": "--pf-c-wizard__nav--lg--BorderRightColor",
  "value": "#d2d2d2",
  "var": "var(--pf-c-wizard__nav--lg--BorderRightColor)"
};
exports["default"] = exports.c_wizard__nav_lg_BorderRightColor;
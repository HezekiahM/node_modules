"use strict";
exports.__esModule = true;
exports.c_wizard__toggle_BackgroundColor = {
  "name": "--pf-c-wizard__toggle--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-wizard__toggle--BackgroundColor)"
};
exports["default"] = exports.c_wizard__toggle_BackgroundColor;
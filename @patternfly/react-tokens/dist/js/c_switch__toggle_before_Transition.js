"use strict";
exports.__esModule = true;
exports.c_switch__toggle_before_Transition = {
  "name": "--pf-c-switch__toggle--before--Transition",
  "value": "transform .25s ease 0s",
  "var": "var(--pf-c-switch__toggle--before--Transition)"
};
exports["default"] = exports.c_switch__toggle_before_Transition;
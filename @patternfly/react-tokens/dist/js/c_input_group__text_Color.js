"use strict";
exports.__esModule = true;
exports.c_input_group__text_Color = {
  "name": "--pf-c-input-group__text--Color",
  "value": "#6a6e73",
  "var": "var(--pf-c-input-group__text--Color)"
};
exports["default"] = exports.c_input_group__text_Color;
"use strict";
exports.__esModule = true;
exports.c_switch_FontSize = {
  "name": "--pf-c-switch--FontSize",
  "value": "1rem",
  "var": "var(--pf-c-switch--FontSize)"
};
exports["default"] = exports.c_switch_FontSize;
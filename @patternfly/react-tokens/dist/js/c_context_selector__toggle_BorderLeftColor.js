"use strict";
exports.__esModule = true;
exports.c_context_selector__toggle_BorderLeftColor = {
  "name": "--pf-c-context-selector__toggle--BorderLeftColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-context-selector__toggle--BorderLeftColor)"
};
exports["default"] = exports.c_context_selector__toggle_BorderLeftColor;
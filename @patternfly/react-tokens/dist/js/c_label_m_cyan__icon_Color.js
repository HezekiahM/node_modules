"use strict";
exports.__esModule = true;
exports.c_label_m_cyan__icon_Color = {
  "name": "--pf-c-label--m-cyan__icon--Color",
  "value": "#009596",
  "var": "var(--pf-c-label--m-cyan__icon--Color)"
};
exports["default"] = exports.c_label_m_cyan__icon_Color;
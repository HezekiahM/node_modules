"use strict";
exports.__esModule = true;
exports.c_avatar_BorderRadius = {
  "name": "--pf-c-avatar--BorderRadius",
  "value": "30em",
  "var": "var(--pf-c-avatar--BorderRadius)"
};
exports["default"] = exports.c_avatar_BorderRadius;
"use strict";
exports.__esModule = true;
exports.c_button_m_danger_Color = {
  "name": "--pf-c-button--m-danger--Color",
  "value": "#fff",
  "var": "var(--pf-c-button--m-danger--Color)"
};
exports["default"] = exports.c_button_m_danger_Color;
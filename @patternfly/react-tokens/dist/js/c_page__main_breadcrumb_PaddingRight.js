"use strict";
exports.__esModule = true;
exports.c_page__main_breadcrumb_PaddingRight = {
  "name": "--pf-c-page__main-breadcrumb--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-page__main-breadcrumb--PaddingRight)"
};
exports["default"] = exports.c_page__main_breadcrumb_PaddingRight;
"use strict";
exports.__esModule = true;
exports.c_popover__arrow_m_right_Rotate = {
  "name": "--pf-c-popover__arrow--m-right--Rotate",
  "value": "45deg",
  "var": "var(--pf-c-popover__arrow--m-right--Rotate)"
};
exports["default"] = exports.c_popover__arrow_m_right_Rotate;
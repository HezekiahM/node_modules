"use strict";
exports.__esModule = true;
exports.c_login__container_xl_GridColumnGap = {
  "name": "--pf-c-login__container--xl--GridColumnGap",
  "value": "4rem",
  "var": "var(--pf-c-login__container--xl--GridColumnGap)"
};
exports["default"] = exports.c_login__container_xl_GridColumnGap;
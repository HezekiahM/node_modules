export const c_button_m_secondary_Color: {
  "name": "--pf-c-button--m-secondary--Color",
  "value": "#fff",
  "var": "var(--pf-c-button--m-secondary--Color)"
};
export default c_button_m_secondary_Color;
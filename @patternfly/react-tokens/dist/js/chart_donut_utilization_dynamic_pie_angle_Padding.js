"use strict";
exports.__esModule = true;
exports.chart_donut_utilization_dynamic_pie_angle_Padding = {
  "name": "--pf-chart-donut--utilization--dynamic--pie--angle--Padding",
  "value": 1,
  "var": "var(--pf-chart-donut--utilization--dynamic--pie--angle--Padding)"
};
exports["default"] = exports.chart_donut_utilization_dynamic_pie_angle_Padding;
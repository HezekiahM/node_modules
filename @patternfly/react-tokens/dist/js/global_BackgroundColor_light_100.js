"use strict";
exports.__esModule = true;
exports.global_BackgroundColor_light_100 = {
  "name": "--pf-global--BackgroundColor--light-100",
  "value": "#fff",
  "var": "var(--pf-global--BackgroundColor--light-100)"
};
exports["default"] = exports.global_BackgroundColor_light_100;
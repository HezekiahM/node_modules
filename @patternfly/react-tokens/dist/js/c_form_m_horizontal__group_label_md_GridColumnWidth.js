"use strict";
exports.__esModule = true;
exports.c_form_m_horizontal__group_label_md_GridColumnWidth = {
  "name": "--pf-c-form--m-horizontal__group-label--md--GridColumnWidth",
  "value": "9.375rem",
  "var": "var(--pf-c-form--m-horizontal__group-label--md--GridColumnWidth)"
};
exports["default"] = exports.c_form_m_horizontal__group_label_md_GridColumnWidth;
"use strict";
exports.__esModule = true;
exports.c_button_m_secondary_active_BorderColor = {
  "name": "--pf-c-button--m-secondary--active--BorderColor",
  "value": "#fff",
  "var": "var(--pf-c-button--m-secondary--active--BorderColor)"
};
exports["default"] = exports.c_button_m_secondary_active_BorderColor;
"use strict";
exports.__esModule = true;
exports.c_content_blockquote_Color = {
  "name": "--pf-c-content--blockquote--Color",
  "value": "#6a6e73",
  "var": "var(--pf-c-content--blockquote--Color)"
};
exports["default"] = exports.c_content_blockquote_Color;
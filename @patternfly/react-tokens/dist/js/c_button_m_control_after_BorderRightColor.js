"use strict";
exports.__esModule = true;
exports.c_button_m_control_after_BorderRightColor = {
  "name": "--pf-c-button--m-control--after--BorderRightColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-button--m-control--after--BorderRightColor)"
};
exports["default"] = exports.c_button_m_control_after_BorderRightColor;
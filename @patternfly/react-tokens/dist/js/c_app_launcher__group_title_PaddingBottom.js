"use strict";
exports.__esModule = true;
exports.c_app_launcher__group_title_PaddingBottom = {
  "name": "--pf-c-app-launcher__group-title--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-app-launcher__group-title--PaddingBottom)"
};
exports["default"] = exports.c_app_launcher__group_title_PaddingBottom;
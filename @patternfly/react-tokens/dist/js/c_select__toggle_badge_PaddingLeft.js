"use strict";
exports.__esModule = true;
exports.c_select__toggle_badge_PaddingLeft = {
  "name": "--pf-c-select__toggle-badge--PaddingLeft",
  "value": "0.5rem",
  "var": "var(--pf-c-select__toggle-badge--PaddingLeft)"
};
exports["default"] = exports.c_select__toggle_badge_PaddingLeft;
"use strict";
exports.__esModule = true;
exports.c_input_group__text_BackgroundColor = {
  "name": "--pf-c-input-group__text--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-input-group__text--BackgroundColor)"
};
exports["default"] = exports.c_input_group__text_BackgroundColor;
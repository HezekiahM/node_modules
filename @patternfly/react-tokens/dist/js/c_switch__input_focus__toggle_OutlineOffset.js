"use strict";
exports.__esModule = true;
exports.c_switch__input_focus__toggle_OutlineOffset = {
  "name": "--pf-c-switch__input--focus__toggle--OutlineOffset",
  "value": "0.5rem",
  "var": "var(--pf-c-switch__input--focus__toggle--OutlineOffset)"
};
exports["default"] = exports.c_switch__input_focus__toggle_OutlineOffset;
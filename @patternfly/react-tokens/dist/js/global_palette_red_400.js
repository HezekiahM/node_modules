"use strict";
exports.__esModule = true;
exports.global_palette_red_400 = {
  "name": "--pf-global--palette--red-400",
  "value": "#470000",
  "var": "var(--pf-global--palette--red-400)"
};
exports["default"] = exports.global_palette_red_400;
"use strict";
exports.__esModule = true;
exports.c_simple_list__item_link_active_FontWeight = {
  "name": "--pf-c-simple-list__item-link--active--FontWeight",
  "value": "700",
  "var": "var(--pf-c-simple-list__item-link--active--FontWeight)"
};
exports["default"] = exports.c_simple_list__item_link_active_FontWeight;
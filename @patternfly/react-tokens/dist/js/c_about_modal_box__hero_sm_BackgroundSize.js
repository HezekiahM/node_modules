"use strict";
exports.__esModule = true;
exports.c_about_modal_box__hero_sm_BackgroundSize = {
  "name": "--pf-c-about-modal-box__hero--sm--BackgroundSize",
  "value": "cover",
  "var": "var(--pf-c-about-modal-box__hero--sm--BackgroundSize)"
};
exports["default"] = exports.c_about_modal_box__hero_sm_BackgroundSize;
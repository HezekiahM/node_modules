"use strict";
exports.__esModule = true;
exports.c_popover__arrow_BackgroundColor = {
  "name": "--pf-c-popover__arrow--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-popover__arrow--BackgroundColor)"
};
exports["default"] = exports.c_popover__arrow_BackgroundColor;
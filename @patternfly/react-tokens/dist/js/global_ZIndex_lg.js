"use strict";
exports.__esModule = true;
exports.global_ZIndex_lg = {
  "name": "--pf-global--ZIndex--lg",
  "value": "400",
  "var": "var(--pf-global--ZIndex--lg)"
};
exports["default"] = exports.global_ZIndex_lg;
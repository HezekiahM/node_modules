"use strict";
exports.__esModule = true;
exports.c_check_GridGap = {
  "name": "--pf-c-check--GridGap",
  "value": "0.25rem 0.5rem",
  "var": "var(--pf-c-check--GridGap)"
};
exports["default"] = exports.c_check_GridGap;
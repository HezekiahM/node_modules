export const c_select__menu_item_m_link_hover_BackgroundColor: {
  "name": "--pf-c-select__menu-item--m-link--hover--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-select__menu-item--m-link--hover--BackgroundColor)"
};
export default c_select__menu_item_m_link_hover_BackgroundColor;
export const l_split: {
  ".pf-l-split": {
    "l_split_m_gutter_MarginRight": {
      "name": "--pf-l-split--m-gutter--MarginRight",
      "value": "1rem",
      "values": [
        "--pf-global--gutter",
        "$pf-global--gutter",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    }
  }
};
export default l_split;
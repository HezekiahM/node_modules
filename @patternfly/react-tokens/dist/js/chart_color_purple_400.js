"use strict";
exports.__esModule = true;
exports.chart_color_purple_400 = {
  "name": "--pf-chart-color-purple-400",
  "value": "#3c3d99",
  "var": "var(--pf-chart-color-purple-400)"
};
exports["default"] = exports.chart_color_purple_400;
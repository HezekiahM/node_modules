"use strict";
exports.__esModule = true;
exports.c_nav_m_light__subnav__link_m_current_after_BorderColor = {
  "name": "--pf-c-nav--m-light__subnav__link--m-current--after--BorderColor",
  "value": "#06c",
  "var": "var(--pf-c-nav--m-light__subnav__link--m-current--after--BorderColor)"
};
exports["default"] = exports.c_nav_m_light__subnav__link_m_current_after_BorderColor;
"use strict";
exports.__esModule = true;
exports.global_palette_gold_500 = {
  "name": "--pf-global--palette--gold-500",
  "value": "#c58c00",
  "var": "var(--pf-global--palette--gold-500)"
};
exports["default"] = exports.global_palette_gold_500;
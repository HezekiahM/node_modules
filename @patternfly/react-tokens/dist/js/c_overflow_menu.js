"use strict";
exports.__esModule = true;
exports.c_overflow_menu = {
  ".pf-c-overflow-menu": {
    "c_overflow_menu_spacer_base": {
      "name": "--pf-c-overflow-menu--spacer--base",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_overflow_menu_spacer": {
      "name": "--pf-c-overflow-menu--spacer",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_overflow_menu__group_spacer": {
      "name": "--pf-c-overflow-menu__group--spacer",
      "value": "1rem",
      "values": [
        "--pf-c-overflow-menu--spacer--base",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_overflow_menu__item_spacer": {
      "name": "--pf-c-overflow-menu__item--spacer",
      "value": "1rem",
      "values": [
        "--pf-c-overflow-menu--spacer--base",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_overflow_menu_c_divider_m_vertical_spacer": {
      "name": "--pf-c-overflow-menu--c-divider--m-vertical--spacer",
      "value": "1rem",
      "values": [
        "--pf-c-overflow-menu--spacer--base",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_overflow_menu__group_m_button_group_spacer": {
      "name": "--pf-c-overflow-menu__group--m-button-group--spacer",
      "value": "1rem",
      "values": [
        "--pf-c-overflow-menu--spacer--base",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_overflow_menu__group_m_button_group_space_items": {
      "name": "--pf-c-overflow-menu__group--m-button-group--space-items",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_overflow_menu__group_m_icon_button_group_spacer": {
      "name": "--pf-c-overflow-menu__group--m-icon-button-group--spacer",
      "value": "1rem",
      "values": [
        "--pf-c-overflow-menu--spacer--base",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_overflow_menu__group_m_icon_button_group_space_items": {
      "name": "--pf-c-overflow-menu__group--m-icon-button-group--space-items",
      "value": "0"
    }
  },
  ".pf-c-overflow-menu__group": {
    "c_overflow_menu_spacer": {
      "name": "--pf-c-overflow-menu--spacer",
      "value": "1rem",
      "values": [
        "--pf-c-overflow-menu__group--spacer",
        "--pf-c-overflow-menu--spacer--base",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    }
  },
  ".pf-c-overflow-menu__group.pf-m-button-group": {
    "c_overflow_menu_spacer": {
      "name": "--pf-c-overflow-menu--spacer",
      "value": "1rem",
      "values": [
        "--pf-c-overflow-menu__group--m-button-group--spacer",
        "--pf-c-overflow-menu--spacer--base",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    }
  },
  ".pf-c-overflow-menu__group.pf-m-button-group > *": {
    "c_overflow_menu_spacer": {
      "name": "--pf-c-overflow-menu--spacer",
      "value": "0.5rem",
      "values": [
        "--pf-c-overflow-menu__group--m-button-group--space-items",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-c-overflow-menu__group.pf-m-icon-button-group": {
    "c_overflow_menu_spacer": {
      "name": "--pf-c-overflow-menu--spacer",
      "value": "1rem",
      "values": [
        "--pf-c-overflow-menu__group--m-icon-button-group--spacer",
        "--pf-c-overflow-menu--spacer--base",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    }
  },
  ".pf-c-overflow-menu__group.pf-m-icon-button-group > *": {
    "c_overflow_menu_spacer": {
      "name": "--pf-c-overflow-menu--spacer",
      "value": "0",
      "values": [
        "--pf-c-overflow-menu__group--m-icon-button-group--space-items",
        "0"
      ]
    }
  },
  ".pf-c-overflow-menu__item": {
    "c_overflow_menu_spacer": {
      "name": "--pf-c-overflow-menu--spacer",
      "value": "1rem",
      "values": [
        "--pf-c-overflow-menu__item--spacer",
        "--pf-c-overflow-menu--spacer--base",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    }
  },
  ".pf-c-overflow-menu__content:last-child": {
    "c_overflow_menu_spacer": {
      "name": "--pf-c-overflow-menu--spacer",
      "value": "0"
    }
  },
  ".pf-c-overflow-menu > .pf-c-divider": {
    "c_overflow_menu_spacer": {
      "name": "--pf-c-overflow-menu--spacer",
      "value": "1rem",
      "values": [
        "--pf-c-overflow-menu--c-divider--m-vertical--spacer",
        "--pf-c-overflow-menu--spacer--base",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    }
  },
  ".pf-c-overflow-menu > .pf-c-divider.pf-m-vertical:last-child": {
    "c_overflow_menu_spacer": {
      "name": "--pf-c-overflow-menu--spacer",
      "value": "0"
    }
  }
};
exports["default"] = exports.c_overflow_menu;
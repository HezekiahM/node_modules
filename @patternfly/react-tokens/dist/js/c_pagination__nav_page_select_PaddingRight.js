"use strict";
exports.__esModule = true;
exports.c_pagination__nav_page_select_PaddingRight = {
  "name": "--pf-c-pagination__nav-page-select--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-pagination__nav-page-select--PaddingRight)"
};
exports["default"] = exports.c_pagination__nav_page_select_PaddingRight;
"use strict";
exports.__esModule = true;
exports.chart_bullet_label_subtitle_Fill = {
  "name": "--pf-chart-bullet--label--subtitle--Fill",
  "value": "#b8bbbe",
  "var": "var(--pf-chart-bullet--label--subtitle--Fill)"
};
exports["default"] = exports.chart_bullet_label_subtitle_Fill;
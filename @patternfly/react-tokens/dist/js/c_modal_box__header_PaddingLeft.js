"use strict";
exports.__esModule = true;
exports.c_modal_box__header_PaddingLeft = {
  "name": "--pf-c-modal-box__header--PaddingLeft",
  "value": "1.5rem",
  "var": "var(--pf-c-modal-box__header--PaddingLeft)"
};
exports["default"] = exports.c_modal_box__header_PaddingLeft;
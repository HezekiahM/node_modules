"use strict";
exports.__esModule = true;
exports.c_tile__header_m_stacked__icon_Color = {
  "name": "--pf-c-tile__header--m-stacked__icon--Color",
  "value": "#6a6e73",
  "var": "var(--pf-c-tile__header--m-stacked__icon--Color)"
};
exports["default"] = exports.c_tile__header_m_stacked__icon_Color;
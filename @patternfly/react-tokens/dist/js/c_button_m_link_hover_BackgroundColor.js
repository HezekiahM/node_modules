"use strict";
exports.__esModule = true;
exports.c_button_m_link_hover_BackgroundColor = {
  "name": "--pf-c-button--m-link--hover--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-button--m-link--hover--BackgroundColor)"
};
exports["default"] = exports.c_button_m_link_hover_BackgroundColor;
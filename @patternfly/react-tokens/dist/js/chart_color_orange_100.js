"use strict";
exports.__esModule = true;
exports.chart_color_orange_100 = {
  "name": "--pf-chart-color-orange-100",
  "value": "#f4b678",
  "var": "var(--pf-chart-color-orange-100)"
};
exports["default"] = exports.chart_color_orange_100;
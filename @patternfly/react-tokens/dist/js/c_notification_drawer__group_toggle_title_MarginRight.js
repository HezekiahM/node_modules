"use strict";
exports.__esModule = true;
exports.c_notification_drawer__group_toggle_title_MarginRight = {
  "name": "--pf-c-notification-drawer__group-toggle-title--MarginRight",
  "value": "1rem",
  "var": "var(--pf-c-notification-drawer__group-toggle-title--MarginRight)"
};
exports["default"] = exports.c_notification_drawer__group_toggle_title_MarginRight;
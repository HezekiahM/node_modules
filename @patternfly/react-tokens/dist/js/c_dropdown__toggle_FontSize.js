"use strict";
exports.__esModule = true;
exports.c_dropdown__toggle_FontSize = {
  "name": "--pf-c-dropdown__toggle--FontSize",
  "value": "1rem",
  "var": "var(--pf-c-dropdown__toggle--FontSize)"
};
exports["default"] = exports.c_dropdown__toggle_FontSize;
"use strict";
exports.__esModule = true;
exports.c_content_blockquote_PaddingTop = {
  "name": "--pf-c-content--blockquote--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-content--blockquote--PaddingTop)"
};
exports["default"] = exports.c_content_blockquote_PaddingTop;
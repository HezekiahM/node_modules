"use strict";
exports.__esModule = true;
exports.c_spinner_diameter = {
  "name": "--pf-c-spinner--diameter",
  "value": "3.375rem",
  "var": "var(--pf-c-spinner--diameter)"
};
exports["default"] = exports.c_spinner_diameter;
"use strict";
exports.__esModule = true;
exports.c_context_selector__menu_list_item_disabled_Color = {
  "name": "--pf-c-context-selector__menu-list-item--disabled--Color",
  "value": "#6a6e73",
  "var": "var(--pf-c-context-selector__menu-list-item--disabled--Color)"
};
exports["default"] = exports.c_context_selector__menu_list_item_disabled_Color;
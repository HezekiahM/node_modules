"use strict";
exports.__esModule = true;
exports.c_table__check_responsive_MarginLeft = {
  "name": "--pf-c-table__check--responsive--MarginLeft",
  "value": "0.5rem",
  "var": "var(--pf-c-table__check--responsive--MarginLeft)"
};
exports["default"] = exports.c_table__check_responsive_MarginLeft;
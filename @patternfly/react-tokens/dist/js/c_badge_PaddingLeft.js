"use strict";
exports.__esModule = true;
exports.c_badge_PaddingLeft = {
  "name": "--pf-c-badge--PaddingLeft",
  "value": "0.5rem",
  "var": "var(--pf-c-badge--PaddingLeft)"
};
exports["default"] = exports.c_badge_PaddingLeft;
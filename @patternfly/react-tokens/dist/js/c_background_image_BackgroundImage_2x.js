"use strict";
exports.__esModule = true;
exports.c_background_image_BackgroundImage_2x = {
  "name": "--pf-c-background-image--BackgroundImage-2x",
  "value": "url(\"../../assets/images/pfbg_576@2x.jpg\")",
  "var": "var(--pf-c-background-image--BackgroundImage-2x)"
};
exports["default"] = exports.c_background_image_BackgroundImage_2x;
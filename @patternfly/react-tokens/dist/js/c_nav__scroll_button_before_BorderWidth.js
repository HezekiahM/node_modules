"use strict";
exports.__esModule = true;
exports.c_nav__scroll_button_before_BorderWidth = {
  "name": "--pf-c-nav__scroll-button--before--BorderWidth",
  "value": "1px",
  "var": "var(--pf-c-nav__scroll-button--before--BorderWidth)"
};
exports["default"] = exports.c_nav__scroll_button_before_BorderWidth;
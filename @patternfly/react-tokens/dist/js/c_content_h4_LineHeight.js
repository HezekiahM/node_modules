"use strict";
exports.__esModule = true;
exports.c_content_h4_LineHeight = {
  "name": "--pf-c-content--h4--LineHeight",
  "value": "1.5",
  "var": "var(--pf-c-content--h4--LineHeight)"
};
exports["default"] = exports.c_content_h4_LineHeight;
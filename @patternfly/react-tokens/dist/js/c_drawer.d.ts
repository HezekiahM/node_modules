export const c_drawer: {
  ".pf-c-drawer": {
    "c_drawer__section_BackgroundColor": {
      "name": "--pf-c-drawer__section--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_drawer__content_FlexBasis": {
      "name": "--pf-c-drawer__content--FlexBasis",
      "value": "100%"
    },
    "c_drawer__content_BackgroundColor": {
      "name": "--pf-c-drawer__content--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_drawer__content_ZIndex": {
      "name": "--pf-c-drawer__content--ZIndex",
      "value": "100",
      "values": [
        "--pf-global--ZIndex--xs",
        "$pf-global--ZIndex--xs",
        "100"
      ]
    },
    "c_drawer__panel_FlexBasis": {
      "name": "--pf-c-drawer__panel--FlexBasis",
      "value": "100%"
    },
    "c_drawer__panel_md_FlexBasis": {
      "name": "--pf-c-drawer__panel--md--FlexBasis",
      "value": "50%"
    },
    "c_drawer__panel_MinWidth": {
      "name": "--pf-c-drawer__panel--MinWidth",
      "value": "50%"
    },
    "c_drawer__panel_xl_MinWidth": {
      "name": "--pf-c-drawer__panel--xl--MinWidth",
      "value": "28.125rem"
    },
    "c_drawer__panel_xl_FlexBasis": {
      "name": "--pf-c-drawer__panel--xl--FlexBasis",
      "value": "28.125rem"
    },
    "c_drawer__panel_ZIndex": {
      "name": "--pf-c-drawer__panel--ZIndex",
      "value": "200",
      "values": [
        "--pf-global--ZIndex--sm",
        "$pf-global--ZIndex--sm",
        "200"
      ]
    },
    "c_drawer__panel_BackgroundColor": {
      "name": "--pf-c-drawer__panel--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_drawer__panel_TransitionDuration": {
      "name": "--pf-c-drawer__panel--TransitionDuration",
      "value": "250ms",
      "values": [
        "--pf-global--TransitionDuration",
        "$pf-global--TransitionDuration",
        "250ms"
      ]
    },
    "c_drawer__panel_TransitionProperty": {
      "name": "--pf-c-drawer__panel--TransitionProperty",
      "value": "margin, transform, box-shadow"
    },
    "c_drawer_child_PaddingTop": {
      "name": "--pf-c-drawer--child--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_drawer_child_PaddingRight": {
      "name": "--pf-c-drawer--child--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_drawer_child_PaddingBottom": {
      "name": "--pf-c-drawer--child--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_drawer_child_PaddingLeft": {
      "name": "--pf-c-drawer--child--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_drawer_child_md_PaddingTop": {
      "name": "--pf-c-drawer--child--md--PaddingTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_drawer_child_md_PaddingRight": {
      "name": "--pf-c-drawer--child--md--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_drawer_child_md_PaddingBottom": {
      "name": "--pf-c-drawer--child--md--PaddingBottom",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_drawer_child_md_PaddingLeft": {
      "name": "--pf-c-drawer--child--md--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_drawer_child_m_padding_PaddingTop": {
      "name": "--pf-c-drawer--child--m-padding--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_drawer_child_m_padding_PaddingRight": {
      "name": "--pf-c-drawer--child--m-padding--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_drawer_child_m_padding_PaddingBottom": {
      "name": "--pf-c-drawer--child--m-padding--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_drawer_child_m_padding_PaddingLeft": {
      "name": "--pf-c-drawer--child--m-padding--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_drawer_child_m_padding_md_PaddingTop": {
      "name": "--pf-c-drawer--child--m-padding--md--PaddingTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_drawer_child_m_padding_md_PaddingRight": {
      "name": "--pf-c-drawer--child--m-padding--md--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_drawer_child_m_padding_md_PaddingBottom": {
      "name": "--pf-c-drawer--child--m-padding--md--PaddingBottom",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_drawer_child_m_padding_md_PaddingLeft": {
      "name": "--pf-c-drawer--child--m-padding--md--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_drawer__content_child_PaddingTop": {
      "name": "--pf-c-drawer__content--child--PaddingTop",
      "value": "0"
    },
    "c_drawer__content_child_PaddingRight": {
      "name": "--pf-c-drawer__content--child--PaddingRight",
      "value": "0"
    },
    "c_drawer__content_child_PaddingBottom": {
      "name": "--pf-c-drawer__content--child--PaddingBottom",
      "value": "0"
    },
    "c_drawer__content_child_PaddingLeft": {
      "name": "--pf-c-drawer__content--child--PaddingLeft",
      "value": "0"
    },
    "c_drawer__actions_MarginTop": {
      "name": "--pf-c-drawer__actions--MarginTop",
      "value": "calc(0.375rem * -1)"
    },
    "c_drawer__actions_MarginRight": {
      "name": "--pf-c-drawer__actions--MarginRight",
      "value": "calc(0.375rem * -1)"
    },
    "c_drawer__panel_BoxShadow": {
      "name": "--pf-c-drawer__panel--BoxShadow",
      "value": "none"
    },
    "c_drawer_m_expanded__panel_BoxShadow": {
      "name": "--pf-c-drawer--m-expanded__panel--BoxShadow",
      "value": "-0.75rem 0 0.75rem -0.5rem rgba(3, 3, 3, 0.18)",
      "values": [
        "--pf-global--BoxShadow--lg-left",
        "$pf-global--BoxShadow--lg-left",
        "pf-size-prem(-12px) 0 pf-size-prem(12px) pf-size-prem(-8px) rgba($pf-color-black-1000, .18)",
        "pf-size-prem(-12px) 0 pf-size-prem(12px) pf-size-prem(-8px) rgba(#030303, .18)",
        "-0.75rem 0 0.75rem -0.5rem rgba(3, 3, 3, 0.18)"
      ]
    },
    "c_drawer_m_expanded_m_panel_left__panel_BoxShadow": {
      "name": "--pf-c-drawer--m-expanded--m-panel-left__panel--BoxShadow",
      "value": "0.75rem 0 0.75rem -0.5rem rgba(3, 3, 3, 0.18)",
      "values": [
        "--pf-global--BoxShadow--lg-right",
        "$pf-global--BoxShadow--lg-right",
        "pf-size-prem(12px) 0 pf-size-prem(12px) pf-size-prem(-8px) rgba($pf-color-black-1000, .18)",
        "pf-size-prem(12px) 0 pf-size-prem(12px) pf-size-prem(-8px) rgba(#030303, .18)",
        "0.75rem 0 0.75rem -0.5rem rgba(3, 3, 3, 0.18)"
      ]
    },
    "c_drawer__panel_after_Width": {
      "name": "--pf-c-drawer__panel--after--Width",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_drawer__panel_after_BackgroundColor": {
      "name": "--pf-c-drawer__panel--after--BackgroundColor",
      "value": "transparent"
    },
    "c_drawer_m_inline_m_expanded__panel_after_BackgroundColor": {
      "name": "--pf-c-drawer--m-inline--m-expanded__panel--after--BackgroundColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_drawer_m_inline__panel_PaddingLeft": {
      "name": "--pf-c-drawer--m-inline__panel--PaddingLeft",
      "value": "1px",
      "values": [
        "--pf-c-drawer__panel--after--Width",
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_drawer_m_panel_left_m_inline__panel_PaddingRight": {
      "name": "--pf-c-drawer--m-panel-left--m-inline__panel--PaddingRight",
      "value": "1px",
      "values": [
        "--pf-c-drawer__panel--after--Width",
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    }
  },
  ".pf-c-drawer__section.pf-m-no-background": {
    "c_drawer__section_BackgroundColor": {
      "name": "--pf-c-drawer__section--BackgroundColor",
      "value": "transparent"
    }
  },
  ".pf-c-drawer__content": {
    "c_drawer_child_PaddingTop": {
      "name": "--pf-c-drawer--child--PaddingTop",
      "value": "0",
      "values": [
        "--pf-c-drawer__content--child--PaddingTop",
        "0"
      ]
    },
    "c_drawer_child_PaddingRight": {
      "name": "--pf-c-drawer--child--PaddingRight",
      "value": "0",
      "values": [
        "--pf-c-drawer__content--child--PaddingRight",
        "0"
      ]
    },
    "c_drawer_child_PaddingBottom": {
      "name": "--pf-c-drawer--child--PaddingBottom",
      "value": "0",
      "values": [
        "--pf-c-drawer__content--child--PaddingBottom",
        "0"
      ]
    },
    "c_drawer_child_PaddingLeft": {
      "name": "--pf-c-drawer--child--PaddingLeft",
      "value": "0",
      "values": [
        "--pf-c-drawer__content--child--PaddingLeft",
        "0"
      ]
    }
  },
  ".pf-c-drawer__content.pf-m-no-background": {
    "c_drawer__content_BackgroundColor": {
      "name": "--pf-c-drawer__content--BackgroundColor",
      "value": "transparent"
    }
  },
  ".pf-c-drawer__panel.pf-m-no-background": {
    "c_drawer__content_BackgroundColor": {
      "name": "--pf-c-drawer__content--BackgroundColor",
      "value": "transparent"
    }
  },
  ".pf-c-drawer__body.pf-m-no-padding": {
    "c_drawer__actions_MarginTop": {
      "name": "--pf-c-drawer__actions--MarginTop",
      "value": "0"
    },
    "c_drawer__actions_MarginRight": {
      "name": "--pf-c-drawer__actions--MarginRight",
      "value": "0"
    },
    "c_drawer_child_PaddingTop": {
      "name": "--pf-c-drawer--child--PaddingTop",
      "value": "0"
    },
    "c_drawer_child_PaddingRight": {
      "name": "--pf-c-drawer--child--PaddingRight",
      "value": "0"
    },
    "c_drawer_child_PaddingBottom": {
      "name": "--pf-c-drawer--child--PaddingBottom",
      "value": "0"
    },
    "c_drawer_child_PaddingLeft": {
      "name": "--pf-c-drawer--child--PaddingLeft",
      "value": "0"
    }
  },
  ".pf-c-drawer__body.pf-m-padding": {
    "c_drawer_child_PaddingTop": {
      "name": "--pf-c-drawer--child--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-c-drawer--child--m-padding--PaddingTop",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_drawer_child_PaddingRight": {
      "name": "--pf-c-drawer--child--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-c-drawer--child--m-padding--PaddingRight",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_drawer_child_PaddingBottom": {
      "name": "--pf-c-drawer--child--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-c-drawer--child--m-padding--PaddingBottom",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_drawer_child_PaddingLeft": {
      "name": "--pf-c-drawer--child--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-c-drawer--child--m-padding--PaddingLeft",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    }
  },
  ".pf-c-drawer__body:not(.pf-m-no-padding) + *": {
    "c_drawer_child_PaddingTop": {
      "name": "--pf-c-drawer--child--PaddingTop",
      "value": "0"
    }
  }
};
export default c_drawer;
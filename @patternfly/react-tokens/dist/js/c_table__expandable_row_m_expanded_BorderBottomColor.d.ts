export const c_table__expandable_row_m_expanded_BorderBottomColor: {
  "name": "--pf-c-table__expandable-row--m-expanded--BorderBottomColor",
  "value": "#d2d2d2",
  "var": "var(--pf-c-table__expandable-row--m-expanded--BorderBottomColor)"
};
export default c_table__expandable_row_m_expanded_BorderBottomColor;
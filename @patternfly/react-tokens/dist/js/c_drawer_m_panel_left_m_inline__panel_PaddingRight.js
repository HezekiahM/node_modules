"use strict";
exports.__esModule = true;
exports.c_drawer_m_panel_left_m_inline__panel_PaddingRight = {
  "name": "--pf-c-drawer--m-panel-left--m-inline__panel--PaddingRight",
  "value": "1px",
  "var": "var(--pf-c-drawer--m-panel-left--m-inline__panel--PaddingRight)"
};
exports["default"] = exports.c_drawer_m_panel_left_m_inline__panel_PaddingRight;
"use strict";
exports.__esModule = true;
exports.c_form_control_invalid_BackgroundSize = {
  "name": "--pf-c-form-control--invalid--BackgroundSize",
  "value": "1rem 1rem",
  "var": "var(--pf-c-form-control--invalid--BackgroundSize)"
};
exports["default"] = exports.c_form_control_invalid_BackgroundSize;
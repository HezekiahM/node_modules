"use strict";
exports.__esModule = true;
exports.c_form__helper_text_icon_FontSize = {
  "name": "--pf-c-form__helper-text-icon--FontSize",
  "value": "1rem",
  "var": "var(--pf-c-form__helper-text-icon--FontSize)"
};
exports["default"] = exports.c_form__helper_text_icon_FontSize;
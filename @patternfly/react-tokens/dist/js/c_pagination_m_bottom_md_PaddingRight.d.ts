export const c_pagination_m_bottom_md_PaddingRight: {
  "name": "--pf-c-pagination--m-bottom--md--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-pagination--m-bottom--md--PaddingRight)"
};
export default c_pagination_m_bottom_md_PaddingRight;
"use strict";
exports.__esModule = true;
exports.c_content_h6_FontSize = {
  "name": "--pf-c-content--h6--FontSize",
  "value": "1rem",
  "var": "var(--pf-c-content--h6--FontSize)"
};
exports["default"] = exports.c_content_h6_FontSize;
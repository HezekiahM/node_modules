"use strict";
exports.__esModule = true;
exports.c_wizard__nav_list__nav_list__nav_link_m_current_FontWeight = {
  "name": "--pf-c-wizard__nav-list__nav-list__nav-link--m-current--FontWeight",
  "value": "700",
  "var": "var(--pf-c-wizard__nav-list__nav-list__nav-link--m-current--FontWeight)"
};
exports["default"] = exports.c_wizard__nav_list__nav_list__nav_link_m_current_FontWeight;
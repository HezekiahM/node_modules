"use strict";
exports.__esModule = true;
exports.c_form_control__select_PaddingRight = {
  "name": "--pf-c-form-control__select--PaddingRight",
  "value": "1.5rem",
  "var": "var(--pf-c-form-control__select--PaddingRight)"
};
exports["default"] = exports.c_form_control__select_PaddingRight;
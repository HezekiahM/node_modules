"use strict";
exports.__esModule = true;
exports.c_notification_badge_m_read_after_BackgroundColor = {
  "name": "--pf-c-notification-badge--m-read--after--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-notification-badge--m-read--after--BackgroundColor)"
};
exports["default"] = exports.c_notification_badge_m_read_after_BackgroundColor;
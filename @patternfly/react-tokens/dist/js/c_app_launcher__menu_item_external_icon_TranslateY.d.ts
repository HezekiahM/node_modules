export const c_app_launcher__menu_item_external_icon_TranslateY: {
  "name": "--pf-c-app-launcher__menu-item-external-icon--TranslateY",
  "value": "-0.0625rem",
  "var": "var(--pf-c-app-launcher__menu-item-external-icon--TranslateY)"
};
export default c_app_launcher__menu_item_external_icon_TranslateY;
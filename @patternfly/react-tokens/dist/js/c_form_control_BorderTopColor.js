"use strict";
exports.__esModule = true;
exports.c_form_control_BorderTopColor = {
  "name": "--pf-c-form-control--BorderTopColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-form-control--BorderTopColor)"
};
exports["default"] = exports.c_form_control_BorderTopColor;
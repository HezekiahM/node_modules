"use strict";
exports.__esModule = true;
exports.global_palette_purple_200 = {
  "name": "--pf-global--palette--purple-200",
  "value": "#b2a3ff",
  "var": "var(--pf-global--palette--purple-200)"
};
exports["default"] = exports.global_palette_purple_200;
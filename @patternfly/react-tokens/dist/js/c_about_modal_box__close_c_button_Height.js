"use strict";
exports.__esModule = true;
exports.c_about_modal_box__close_c_button_Height = {
  "name": "--pf-c-about-modal-box__close--c-button--Height",
  "value": "calc(1.25rem * 2)",
  "var": "var(--pf-c-about-modal-box__close--c-button--Height)"
};
exports["default"] = exports.c_about_modal_box__close_c_button_Height;
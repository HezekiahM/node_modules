"use strict";
exports.__esModule = true;
exports.global_info_color_100 = {
  "name": "--pf-global--info-color--100",
  "value": "#2b9af3",
  "var": "var(--pf-global--info-color--100)"
};
exports["default"] = exports.global_info_color_100;
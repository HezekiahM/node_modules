"use strict";
exports.__esModule = true;
exports.chart_color_black_300 = {
  "name": "--pf-chart-color-black-300",
  "value": "#b8bbbe",
  "var": "var(--pf-chart-color-black-300)"
};
exports["default"] = exports.chart_color_black_300;
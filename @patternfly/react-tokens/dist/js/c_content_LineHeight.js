"use strict";
exports.__esModule = true;
exports.c_content_LineHeight = {
  "name": "--pf-c-content--LineHeight",
  "value": "1.5",
  "var": "var(--pf-c-content--LineHeight)"
};
exports["default"] = exports.c_content_LineHeight;
export const c_page__main_section_xl_PaddingRight: {
  "name": "--pf-c-page__main-section--xl--PaddingRight",
  "value": "1.5rem",
  "var": "var(--pf-c-page__main-section--xl--PaddingRight)"
};
export default c_page__main_section_xl_PaddingRight;
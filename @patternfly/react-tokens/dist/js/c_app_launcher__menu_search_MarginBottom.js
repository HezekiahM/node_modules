"use strict";
exports.__esModule = true;
exports.c_app_launcher__menu_search_MarginBottom = {
  "name": "--pf-c-app-launcher__menu-search--MarginBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-app-launcher__menu-search--MarginBottom)"
};
exports["default"] = exports.c_app_launcher__menu_search_MarginBottom;
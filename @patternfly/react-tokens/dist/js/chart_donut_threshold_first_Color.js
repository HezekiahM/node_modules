"use strict";
exports.__esModule = true;
exports.chart_donut_threshold_first_Color = {
  "name": "--pf-chart-donut--threshold--first--Color",
  "value": "#f0f0f0",
  "var": "var(--pf-chart-donut--threshold--first--Color)"
};
exports["default"] = exports.chart_donut_threshold_first_Color;
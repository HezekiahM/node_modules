"use strict";
exports.__esModule = true;
exports.c_form_control__select_invalid_PaddingRight = {
  "name": "--pf-c-form-control__select--invalid--PaddingRight",
  "value": "calc(0.5rem + 3rem)",
  "var": "var(--pf-c-form-control__select--invalid--PaddingRight)"
};
exports["default"] = exports.c_form_control__select_invalid_PaddingRight;
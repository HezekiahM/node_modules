"use strict";
exports.__esModule = true;
exports.c_drawer__panel_xl_FlexBasis = {
  "name": "--pf-c-drawer__panel--xl--FlexBasis",
  "value": "28.125rem",
  "var": "var(--pf-c-drawer__panel--xl--FlexBasis)"
};
exports["default"] = exports.c_drawer__panel_xl_FlexBasis;
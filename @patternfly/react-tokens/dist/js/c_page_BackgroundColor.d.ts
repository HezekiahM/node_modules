export const c_page_BackgroundColor: {
  "name": "--pf-c-page--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-page--BackgroundColor)"
};
export default c_page_BackgroundColor;
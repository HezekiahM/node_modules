"use strict";
exports.__esModule = true;
exports.c_simple_list__item_link_hover_BackgroundColor = {
  "name": "--pf-c-simple-list__item-link--hover--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-simple-list__item-link--hover--BackgroundColor)"
};
exports["default"] = exports.c_simple_list__item_link_hover_BackgroundColor;
export const c_dropdown: {
  ".pf-c-dropdown": {
    "c_dropdown__toggle_PaddingTop": {
      "name": "--pf-c-dropdown__toggle--PaddingTop",
      "value": "0.375rem",
      "values": [
        "--pf-global--spacer--form-element",
        "$pf-global--spacer--form-element",
        "pf-size-prem(6px)",
        "0.375rem"
      ]
    },
    "c_dropdown__toggle_PaddingRight": {
      "name": "--pf-c-dropdown__toggle--PaddingRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_dropdown__toggle_PaddingBottom": {
      "name": "--pf-c-dropdown__toggle--PaddingBottom",
      "value": "0.375rem",
      "values": [
        "--pf-global--spacer--form-element",
        "$pf-global--spacer--form-element",
        "pf-size-prem(6px)",
        "0.375rem"
      ]
    },
    "c_dropdown__toggle_PaddingLeft": {
      "name": "--pf-c-dropdown__toggle--PaddingLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_dropdown__toggle_MinWidth": {
      "name": "--pf-c-dropdown__toggle--MinWidth",
      "value": "44px",
      "values": [
        "--pf-global--target-size--MinWidth",
        "$pf-global--target-size--MinWidth",
        "44px"
      ]
    },
    "c_dropdown__toggle_FontSize": {
      "name": "--pf-c-dropdown__toggle--FontSize",
      "value": "1rem",
      "values": [
        "--pf-global--FontSize--md",
        "$pf-global--FontSize--md",
        "pf-font-prem(16px)",
        "1rem"
      ]
    },
    "c_dropdown__toggle_FontWeight": {
      "name": "--pf-c-dropdown__toggle--FontWeight",
      "value": "400",
      "values": [
        "--pf-global--FontWeight--normal",
        "$pf-global--FontWeight--normal",
        "400"
      ]
    },
    "c_dropdown__toggle_Color": {
      "name": "--pf-c-dropdown__toggle--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_dropdown__toggle_LineHeight": {
      "name": "--pf-c-dropdown__toggle--LineHeight",
      "value": "1.5",
      "values": [
        "--pf-global--LineHeight--md",
        "$pf-global--LineHeight--md",
        "1.5"
      ]
    },
    "c_dropdown__toggle_BackgroundColor": {
      "name": "--pf-c-dropdown__toggle--BackgroundColor",
      "value": "transparent"
    },
    "c_dropdown__toggle_before_BorderWidth": {
      "name": "--pf-c-dropdown__toggle--before--BorderWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_dropdown__toggle_before_BorderTopColor": {
      "name": "--pf-c-dropdown__toggle--before--BorderTopColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_dropdown__toggle_before_BorderRightColor": {
      "name": "--pf-c-dropdown__toggle--before--BorderRightColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_dropdown__toggle_before_BorderBottomColor": {
      "name": "--pf-c-dropdown__toggle--before--BorderBottomColor",
      "value": "#8a8d90",
      "values": [
        "--pf-global--BorderColor--200",
        "$pf-global--BorderColor--200",
        "$pf-color-black-500",
        "#8a8d90"
      ]
    },
    "c_dropdown__toggle_before_BorderLeftColor": {
      "name": "--pf-c-dropdown__toggle--before--BorderLeftColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_dropdown__toggle_hover_before_BorderBottomColor": {
      "name": "--pf-c-dropdown__toggle--hover--before--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_dropdown__toggle_active_before_BorderBottomWidth": {
      "name": "--pf-c-dropdown__toggle--active--before--BorderBottomWidth",
      "value": "2px",
      "values": [
        "--pf-global--BorderWidth--md",
        "$pf-global--BorderWidth--md",
        "2px"
      ]
    },
    "c_dropdown__toggle_active_before_BorderBottomColor": {
      "name": "--pf-c-dropdown__toggle--active--before--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_dropdown__toggle_focus_before_BorderBottomWidth": {
      "name": "--pf-c-dropdown__toggle--focus--before--BorderBottomWidth",
      "value": "2px",
      "values": [
        "--pf-global--BorderWidth--md",
        "$pf-global--BorderWidth--md",
        "2px"
      ]
    },
    "c_dropdown__toggle_focus_before_BorderBottomColor": {
      "name": "--pf-c-dropdown__toggle--focus--before--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_dropdown_m_expanded__toggle_before_BorderBottomWidth": {
      "name": "--pf-c-dropdown--m-expanded__toggle--before--BorderBottomWidth",
      "value": "2px",
      "values": [
        "--pf-global--BorderWidth--md",
        "$pf-global--BorderWidth--md",
        "2px"
      ]
    },
    "c_dropdown_m_expanded__toggle_before_BorderBottomColor": {
      "name": "--pf-c-dropdown--m-expanded__toggle--before--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_dropdown__toggle_disabled_BackgroundColor": {
      "name": "--pf-c-dropdown__toggle--disabled--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--disabled-color--300",
        "$pf-global--disabled-color--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_dropdown__toggle_m_plain_Color": {
      "name": "--pf-c-dropdown__toggle--m-plain--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--200",
        "$pf-global--Color--200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_dropdown__toggle_m_plain_hover_Color": {
      "name": "--pf-c-dropdown__toggle--m-plain--hover--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_dropdown__toggle_m_plain_disabled_Color": {
      "name": "--pf-c-dropdown__toggle--m-plain--disabled--Color",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--disabled-color--200",
        "$pf-global--disabled-color--200",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_dropdown__toggle_m_plain_child_LineHeight": {
      "name": "--pf-c-dropdown__toggle--m-plain--child--LineHeight",
      "value": "1.5",
      "values": [
        "--pf-global--LineHeight--md",
        "$pf-global--LineHeight--md",
        "1.5"
      ]
    },
    "c_dropdown__toggle_m_primary_Color": {
      "name": "--pf-c-dropdown__toggle--m-primary--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_dropdown__toggle_m_primary_BackgroundColor": {
      "name": "--pf-c-dropdown__toggle--m-primary--BackgroundColor",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_dropdown__toggle_m_primary_hover_BackgroundColor": {
      "name": "--pf-c-dropdown__toggle--m-primary--hover--BackgroundColor",
      "value": "#004080",
      "values": [
        "--pf-global--primary-color--200",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "c_dropdown__toggle_m_primary_active_BackgroundColor": {
      "name": "--pf-c-dropdown__toggle--m-primary--active--BackgroundColor",
      "value": "#004080",
      "values": [
        "--pf-global--primary-color--200",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "c_dropdown__toggle_m_primary_focus_BackgroundColor": {
      "name": "--pf-c-dropdown__toggle--m-primary--focus--BackgroundColor",
      "value": "#004080",
      "values": [
        "--pf-global--primary-color--200",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "c_dropdown_m_expanded__toggle_m_primary_BackgroundColor": {
      "name": "--pf-c-dropdown--m-expanded__toggle--m-primary--BackgroundColor",
      "value": "#004080",
      "values": [
        "--pf-global--primary-color--200",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "c_dropdown__toggle_button_Color": {
      "name": "--pf-c-dropdown__toggle-button--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_dropdown__toggle_m_split_button_child_PaddingTop": {
      "name": "--pf-c-dropdown__toggle--m-split-button--child--PaddingTop",
      "value": "0.375rem",
      "values": [
        "--pf-global--spacer--form-element",
        "$pf-global--spacer--form-element",
        "pf-size-prem(6px)",
        "0.375rem"
      ]
    },
    "c_dropdown__toggle_m_split_button_child_PaddingRight": {
      "name": "--pf-c-dropdown__toggle--m-split-button--child--PaddingRight",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_dropdown__toggle_m_split_button_child_PaddingBottom": {
      "name": "--pf-c-dropdown__toggle--m-split-button--child--PaddingBottom",
      "value": "0.375rem",
      "values": [
        "--pf-global--spacer--form-element",
        "$pf-global--spacer--form-element",
        "pf-size-prem(6px)",
        "0.375rem"
      ]
    },
    "c_dropdown__toggle_m_split_button_child_PaddingLeft": {
      "name": "--pf-c-dropdown__toggle--m-split-button--child--PaddingLeft",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_dropdown__toggle_m_split_button_child_BackgroundColor": {
      "name": "--pf-c-dropdown__toggle--m-split-button--child--BackgroundColor",
      "value": "transparent"
    },
    "c_dropdown__toggle_m_split_button_first_child_PaddingLeft": {
      "name": "--pf-c-dropdown__toggle--m-split-button--first-child--PaddingLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_dropdown__toggle_m_split_button_last_child_PaddingRight": {
      "name": "--pf-c-dropdown__toggle--m-split-button--last-child--PaddingRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_dropdown__toggle_m_split_button_m_action_child_PaddingLeft": {
      "name": "--pf-c-dropdown__toggle--m-split-button--m-action--child--PaddingLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_dropdown__toggle_m_split_button_m_action_child_PaddingRight": {
      "name": "--pf-c-dropdown__toggle--m-split-button--m-action--child--PaddingRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_dropdown__toggle_m_split_button_m_action__toggle_button_MarginRight": {
      "name": "--pf-c-dropdown__toggle--m-split-button--m-action__toggle-button--MarginRight",
      "value": "calc(-1 * 1px)",
      "values": [
        "calc(-1 * --pf-global--BorderWidth--sm)",
        "calc(-1 * $pf-global--BorderWidth--sm)",
        "calc(-1 * 1px)"
      ]
    },
    "c_dropdown__toggle_m_split_button__toggle_check__input_TranslateY": {
      "name": "--pf-c-dropdown__toggle--m-split-button__toggle-check__input--TranslateY",
      "value": "-0.0625rem"
    },
    "c_dropdown__toggle_m_split_button__toggle_text_MarginLeft": {
      "name": "--pf-c-dropdown__toggle--m-split-button__toggle-text--MarginLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_dropdown__toggle_icon_LineHeight": {
      "name": "--pf-c-dropdown__toggle-icon--LineHeight",
      "value": "1.5",
      "values": [
        "--pf-global--LineHeight--md",
        "$pf-global--LineHeight--md",
        "1.5"
      ]
    },
    "c_dropdown__toggle_icon_MarginRight": {
      "name": "--pf-c-dropdown__toggle-icon--MarginRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_dropdown__toggle_icon_MarginLeft": {
      "name": "--pf-c-dropdown__toggle-icon--MarginLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_dropdown_m_top_m_expanded__toggle_icon_Rotate": {
      "name": "--pf-c-dropdown--m-top--m-expanded__toggle-icon--Rotate",
      "value": "180deg"
    },
    "c_dropdown__menu_BackgroundColor": {
      "name": "--pf-c-dropdown__menu--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--light-100",
        "$pf-global--BackgroundColor--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_dropdown__menu_BoxShadow": {
      "name": "--pf-c-dropdown__menu--BoxShadow",
      "value": "0 0.25rem 0.5rem 0rem rgba(3, 3, 3, 0.12), 0 0 0.25rem 0 rgba(3, 3, 3, 0.06)",
      "values": [
        "--pf-global--BoxShadow--md",
        "$pf-global--BoxShadow--md",
        "0 pf-size-prem(4px) pf-size-prem(8px) pf-size-prem(0) rgba($pf-color-black-1000, .12), 0 0 pf-size-prem(4px) 0 rgba($pf-color-black-1000, .06)",
        "0 pf-size-prem(4px) pf-size-prem(8px) pf-size-prem(0) rgba(#030303, .12), 0 0 pf-size-prem(4px) 0 rgba(#030303, .06)",
        "0 0.25rem 0.5rem 0rem rgba(3, 3, 3, 0.12), 0 0 0.25rem 0 rgba(3, 3, 3, 0.06)"
      ]
    },
    "c_dropdown__menu_PaddingTop": {
      "name": "--pf-c-dropdown__menu--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_dropdown__menu_PaddingBottom": {
      "name": "--pf-c-dropdown__menu--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_dropdown__menu_Top": {
      "name": "--pf-c-dropdown__menu--Top",
      "value": "calc(100% + 0.25rem)",
      "values": [
        "calc(100% + --pf-global--spacer--xs)",
        "calc(100% + $pf-global--spacer--xs)",
        "calc(100% + pf-size-prem(4px))",
        "calc(100% + 0.25rem)"
      ]
    },
    "c_dropdown__menu_ZIndex": {
      "name": "--pf-c-dropdown__menu--ZIndex",
      "value": "200",
      "values": [
        "--pf-global--ZIndex--sm",
        "$pf-global--ZIndex--sm",
        "200"
      ]
    },
    "c_dropdown_m_top__menu_Top": {
      "name": "--pf-c-dropdown--m-top__menu--Top",
      "value": "0"
    },
    "c_dropdown_m_top__menu_TranslateY": {
      "name": "--pf-c-dropdown--m-top__menu--TranslateY",
      "value": "calc(-100% - 0.25rem)",
      "values": [
        "calc(-100% - --pf-global--spacer--xs)",
        "calc(-100% - $pf-global--spacer--xs)",
        "calc(-100% - pf-size-prem(4px))",
        "calc(-100% - 0.25rem)"
      ]
    },
    "c_dropdown__menu_item_BackgroundColor": {
      "name": "--pf-c-dropdown__menu-item--BackgroundColor",
      "value": "transparent"
    },
    "c_dropdown__menu_item_PaddingTop": {
      "name": "--pf-c-dropdown__menu-item--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_dropdown__menu_item_PaddingRight": {
      "name": "--pf-c-dropdown__menu-item--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_dropdown__menu_item_PaddingBottom": {
      "name": "--pf-c-dropdown__menu-item--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_dropdown__menu_item_PaddingLeft": {
      "name": "--pf-c-dropdown__menu-item--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_dropdown__menu_item_FontSize": {
      "name": "--pf-c-dropdown__menu-item--FontSize",
      "value": "1rem",
      "values": [
        "--pf-global--FontSize--md",
        "$pf-global--FontSize--md",
        "pf-font-prem(16px)",
        "1rem"
      ]
    },
    "c_dropdown__menu_item_FontWeight": {
      "name": "--pf-c-dropdown__menu-item--FontWeight",
      "value": "400",
      "values": [
        "--pf-global--FontWeight--normal",
        "$pf-global--FontWeight--normal",
        "400"
      ]
    },
    "c_dropdown__menu_item_LineHeight": {
      "name": "--pf-c-dropdown__menu-item--LineHeight",
      "value": "1.5",
      "values": [
        "--pf-global--LineHeight--md",
        "$pf-global--LineHeight--md",
        "1.5"
      ]
    },
    "c_dropdown__menu_item_Color": {
      "name": "--pf-c-dropdown__menu-item--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_dropdown__menu_item_hover_Color": {
      "name": "--pf-c-dropdown__menu-item--hover--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_dropdown__menu_item_disabled_Color": {
      "name": "--pf-c-dropdown__menu-item--disabled--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--dark-200",
        "$pf-global--Color--dark-200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_dropdown__menu_item_hover_BackgroundColor": {
      "name": "--pf-c-dropdown__menu-item--hover--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_dropdown__menu_item_disabled_BackgroundColor": {
      "name": "--pf-c-dropdown__menu-item--disabled--BackgroundColor",
      "value": "transparent"
    },
    "c_dropdown__menu_item_m_text_Color": {
      "name": "--pf-c-dropdown__menu-item--m-text--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--200",
        "$pf-global--Color--200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_dropdown__menu_item_icon_MarginRight": {
      "name": "--pf-c-dropdown__menu-item-icon--MarginRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_dropdown__menu_item_icon_Width": {
      "name": "--pf-c-dropdown__menu-item-icon--Width",
      "value": "1.5rem",
      "values": [
        "--pf-global--icon--FontSize--lg",
        "$pf-global--icon--FontSize--lg",
        "pf-font-prem(24px)",
        "1.5rem"
      ]
    },
    "c_dropdown__menu_item_icon_Height": {
      "name": "--pf-c-dropdown__menu-item-icon--Height",
      "value": "1.5rem",
      "values": [
        "--pf-global--icon--FontSize--lg",
        "$pf-global--icon--FontSize--lg",
        "pf-font-prem(24px)",
        "1.5rem"
      ]
    },
    "c_dropdown__menu_item_description_FontSize": {
      "name": "--pf-c-dropdown__menu-item-description--FontSize",
      "value": "0.75rem",
      "values": [
        "--pf-global--FontSize--xs",
        "$pf-global--FontSize--xs",
        "pf-font-prem(12px)",
        "0.75rem"
      ]
    },
    "c_dropdown__menu_item_description_Color": {
      "name": "--pf-c-dropdown__menu-item-description--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--200",
        "$pf-global--Color--200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_dropdown__group_group_PaddingTop": {
      "name": "--pf-c-dropdown__group--group--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_dropdown__group_title_PaddingTop": {
      "name": "--pf-c-dropdown__group-title--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_dropdown__group_title_PaddingRight": {
      "name": "--pf-c-dropdown__group-title--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-c-dropdown__menu-item--PaddingRight",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_dropdown__group_title_PaddingBottom": {
      "name": "--pf-c-dropdown__group-title--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-c-dropdown__menu-item--PaddingBottom",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_dropdown__group_title_PaddingLeft": {
      "name": "--pf-c-dropdown__group-title--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-c-dropdown__menu-item--PaddingLeft",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_dropdown__group_title_FontSize": {
      "name": "--pf-c-dropdown__group-title--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_dropdown__group_title_FontWeight": {
      "name": "--pf-c-dropdown__group-title--FontWeight",
      "value": "700",
      "values": [
        "--pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    },
    "c_dropdown__group_title_Color": {
      "name": "--pf-c-dropdown__group-title--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--dark-200",
        "$pf-global--Color--dark-200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_dropdown__toggle_image_MarginTop": {
      "name": "--pf-c-dropdown__toggle-image--MarginTop",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_dropdown__toggle_image_MarginBottom": {
      "name": "--pf-c-dropdown__toggle-image--MarginBottom",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_dropdown__toggle_image_MarginRight": {
      "name": "--pf-c-dropdown__toggle-image--MarginRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_dropdown_c_divider_MarginTop": {
      "name": "--pf-c-dropdown--c-divider--MarginTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_dropdown_c_divider_MarginBottom": {
      "name": "--pf-c-dropdown--c-divider--MarginBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-c-dropdown .pf-c-divider:last-child": {
    "c_dropdown_c_divider_MarginBottom": {
      "name": "--pf-c-dropdown--c-divider--MarginBottom",
      "value": "0"
    }
  },
  ".pf-c-dropdown__toggle.pf-m-disabled:not(.pf-m-plain)": {
    "c_dropdown__toggle_BackgroundColor": {
      "name": "--pf-c-dropdown__toggle--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-c-dropdown__toggle--disabled--BackgroundColor",
        "--pf-global--disabled-color--300",
        "$pf-global--disabled-color--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    }
  },
  ".pf-c-dropdown__toggle.pf-m-split-button > *:first-child": {
    "c_dropdown__toggle_m_split_button_child_PaddingLeft": {
      "name": "--pf-c-dropdown__toggle--m-split-button--child--PaddingLeft",
      "value": "0.5rem",
      "values": [
        "--pf-c-dropdown__toggle--m-split-button--first-child--PaddingLeft",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-c-dropdown__toggle.pf-m-split-button > *:last-child": {
    "c_dropdown__toggle_m_split_button_child_PaddingRight": {
      "name": "--pf-c-dropdown__toggle--m-split-button--child--PaddingRight",
      "value": "0.5rem",
      "values": [
        "--pf-c-dropdown__toggle--m-split-button--last-child--PaddingRight",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-c-dropdown__toggle.pf-m-split-button.pf-m-action": {
    "c_dropdown__toggle_m_split_button_child_PaddingRight": {
      "name": "--pf-c-dropdown__toggle--m-split-button--child--PaddingRight",
      "value": "0.5rem",
      "values": [
        "--pf-c-dropdown__toggle--m-split-button--m-action--child--PaddingRight",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_dropdown__toggle_m_split_button_child_PaddingLeft": {
      "name": "--pf-c-dropdown__toggle--m-split-button--child--PaddingLeft",
      "value": "0.5rem",
      "values": [
        "--pf-c-dropdown__toggle--m-split-button--m-action--child--PaddingLeft",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-c-dropdown__toggle.pf-m-split-button.pf-m-action .pf-c-dropdown__toggle-button:last-child": {
    "c_dropdown__toggle_m_split_button_m_action__toggle_button_MarginRight": {
      "name": "--pf-c-dropdown__toggle--m-split-button--m-action__toggle-button--MarginRight",
      "value": "0"
    }
  },
  ".pf-c-dropdown__toggle:not(.pf-m-action):hover::before": {
    "c_dropdown__toggle_before_BorderBottomColor": {
      "name": "--pf-c-dropdown__toggle--before--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-c-dropdown__toggle--hover--before--BorderBottomColor",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-dropdown__toggle:not(.pf-m-action):active::before": {
    "c_dropdown__toggle_before_BorderBottomColor": {
      "name": "--pf-c-dropdown__toggle--before--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-c-dropdown__toggle--active--before--BorderBottomColor",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-dropdown__toggle:not(.pf-m-action):focus::before": {
    "c_dropdown__toggle_before_BorderBottomColor": {
      "name": "--pf-c-dropdown__toggle--before--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-c-dropdown__toggle--focus--before--BorderBottomColor",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-m-expanded > .pf-c-dropdown__toggle:not(.pf-m-action)::before": {
    "c_dropdown__toggle_before_BorderBottomColor": {
      "name": "--pf-c-dropdown__toggle--before--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-c-dropdown--m-expanded__toggle--before--BorderBottomColor",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-dropdown__toggle.pf-m-plain:hover": {
    "c_dropdown__toggle_m_plain_Color": {
      "name": "--pf-c-dropdown__toggle--m-plain--Color",
      "value": "#151515",
      "values": [
        "--pf-c-dropdown__toggle--m-plain--hover--Color",
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    }
  },
  ".pf-c-dropdown__toggle.pf-m-plain.pf-m-disabled": {
    "c_dropdown__toggle_m_plain_Color": {
      "name": "--pf-c-dropdown__toggle--m-plain--Color",
      "value": "#d2d2d2",
      "values": [
        "--pf-c-dropdown__toggle--m-plain--disabled--Color",
        "--pf-global--disabled-color--200",
        "$pf-global--disabled-color--200",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    }
  },
  ".pf-c-dropdown__toggle.pf-m-primary": {
    "c_dropdown__toggle_Color": {
      "name": "--pf-c-dropdown__toggle--Color",
      "value": "#fff",
      "values": [
        "--pf-c-dropdown__toggle--m-primary--Color",
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_dropdown__toggle_BackgroundColor": {
      "name": "--pf-c-dropdown__toggle--BackgroundColor",
      "value": "#06c",
      "values": [
        "--pf-c-dropdown__toggle--m-primary--BackgroundColor",
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-dropdown__toggle.pf-m-primary:hover": {
    "c_dropdown__toggle_BackgroundColor": {
      "name": "--pf-c-dropdown__toggle--BackgroundColor",
      "value": "#004080",
      "values": [
        "--pf-c-dropdown__toggle--m-primary--hover--BackgroundColor",
        "--pf-global--primary-color--200",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    }
  },
  ".pf-c-dropdown__toggle.pf-m-primary:active": {
    "c_dropdown__toggle_BackgroundColor": {
      "name": "--pf-c-dropdown__toggle--BackgroundColor",
      "value": "#004080",
      "values": [
        "--pf-c-dropdown__toggle--m-primary--active--BackgroundColor",
        "--pf-global--primary-color--200",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    }
  },
  ".pf-c-dropdown__toggle.pf-m-primary:focus": {
    "c_dropdown__toggle_BackgroundColor": {
      "name": "--pf-c-dropdown__toggle--BackgroundColor",
      "value": "#004080",
      "values": [
        "--pf-c-dropdown__toggle--m-primary--focus--BackgroundColor",
        "--pf-global--primary-color--200",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    }
  },
  ".pf-m-expanded > .pf-c-dropdown__toggle.pf-m-primary": {
    "c_dropdown__toggle_BackgroundColor": {
      "name": "--pf-c-dropdown__toggle--BackgroundColor",
      "value": "#004080",
      "values": [
        "--pf-c-dropdown--m-expanded__toggle--m-primary--BackgroundColor",
        "--pf-global--primary-color--200",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    }
  },
  ".pf-c-dropdown__toggle-image:last-child": {
    "c_dropdown__toggle_image_MarginRight": {
      "name": "--pf-c-dropdown__toggle-image--MarginRight",
      "value": "0"
    }
  },
  ".pf-c-dropdown.pf-m-top .pf-c-dropdown__menu": {
    "c_dropdown__menu_Top": {
      "name": "--pf-c-dropdown__menu--Top",
      "value": "0",
      "values": [
        "--pf-c-dropdown--m-top__menu--Top",
        "0"
      ]
    }
  },
  ".pf-c-dropdown__menu-item:hover": {
    "c_dropdown__menu_item_Color": {
      "name": "--pf-c-dropdown__menu-item--Color",
      "value": "#151515",
      "values": [
        "--pf-c-dropdown__menu-item--hover--Color",
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_dropdown__menu_item_BackgroundColor": {
      "name": "--pf-c-dropdown__menu-item--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-c-dropdown__menu-item--hover--BackgroundColor",
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    }
  },
  ".pf-c-dropdown__menu-item:disabled": {
    "c_dropdown__menu_item_Color": {
      "name": "--pf-c-dropdown__menu-item--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-c-dropdown__menu-item--disabled--Color",
        "--pf-global--Color--dark-200",
        "$pf-global--Color--dark-200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_dropdown__menu_item_BackgroundColor": {
      "name": "--pf-c-dropdown__menu-item--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-dropdown__menu-item--disabled--BackgroundColor",
        "transparent"
      ]
    }
  },
  ".pf-c-dropdown__menu-item.pf-m-text": {
    "c_dropdown__menu_item_Color": {
      "name": "--pf-c-dropdown__menu-item--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-c-dropdown__menu-item--m-text--Color",
        "--pf-global--Color--200",
        "$pf-global--Color--200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    }
  },
  ".pf-c-dropdown__menu-item.pf-m-text:hover": {
    "c_dropdown__menu_item_BackgroundColor": {
      "name": "--pf-c-dropdown__menu-item--BackgroundColor",
      "value": "transparent"
    }
  }
};
export default c_dropdown;
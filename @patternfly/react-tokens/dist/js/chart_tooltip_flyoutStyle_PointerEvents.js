"use strict";
exports.__esModule = true;
exports.chart_tooltip_flyoutStyle_PointerEvents = {
  "name": "--pf-chart-tooltip--flyoutStyle--PointerEvents",
  "value": "none",
  "var": "var(--pf-chart-tooltip--flyoutStyle--PointerEvents)"
};
exports["default"] = exports.chart_tooltip_flyoutStyle_PointerEvents;
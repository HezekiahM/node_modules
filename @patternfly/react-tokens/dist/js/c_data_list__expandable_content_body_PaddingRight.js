"use strict";
exports.__esModule = true;
exports.c_data_list__expandable_content_body_PaddingRight = {
  "name": "--pf-c-data-list__expandable-content-body--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-data-list__expandable-content-body--PaddingRight)"
};
exports["default"] = exports.c_data_list__expandable_content_body_PaddingRight;
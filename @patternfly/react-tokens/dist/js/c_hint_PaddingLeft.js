"use strict";
exports.__esModule = true;
exports.c_hint_PaddingLeft = {
  "name": "--pf-c-hint--PaddingLeft",
  "value": "1.5rem",
  "var": "var(--pf-c-hint--PaddingLeft)"
};
exports["default"] = exports.c_hint_PaddingLeft;
"use strict";
exports.__esModule = true;
exports.c_spinner_Height = {
  "name": "--pf-c-spinner--Height",
  "value": "3.375rem",
  "var": "var(--pf-c-spinner--Height)"
};
exports["default"] = exports.c_spinner_Height;
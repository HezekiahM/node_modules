"use strict";
exports.__esModule = true;
exports.c_wizard__toggle_m_expanded_BorderBottomColor = {
  "name": "--pf-c-wizard__toggle--m-expanded--BorderBottomColor",
  "value": "#d2d2d2",
  "var": "var(--pf-c-wizard__toggle--m-expanded--BorderBottomColor)"
};
exports["default"] = exports.c_wizard__toggle_m_expanded_BorderBottomColor;
"use strict";
exports.__esModule = true;
exports.c_form_control_readonly_BackgroundColor = {
  "name": "--pf-c-form-control--readonly--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-form-control--readonly--BackgroundColor)"
};
exports["default"] = exports.c_form_control_readonly_BackgroundColor;
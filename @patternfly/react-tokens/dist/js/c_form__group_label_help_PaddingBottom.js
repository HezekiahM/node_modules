"use strict";
exports.__esModule = true;
exports.c_form__group_label_help_PaddingBottom = {
  "name": "--pf-c-form__group-label-help--PaddingBottom",
  "value": "0.25rem",
  "var": "var(--pf-c-form__group-label-help--PaddingBottom)"
};
exports["default"] = exports.c_form__group_label_help_PaddingBottom;
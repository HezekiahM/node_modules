"use strict";
exports.__esModule = true;
exports.c_login__main_header_desc_MarginBottom = {
  "name": "--pf-c-login__main-header-desc--MarginBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-login__main-header-desc--MarginBottom)"
};
exports["default"] = exports.c_login__main_header_desc_MarginBottom;
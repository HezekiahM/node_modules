"use strict";
exports.__esModule = true;
exports.c_about_modal_box__close_lg_PaddingRight = {
  "name": "--pf-c-about-modal-box__close--lg--PaddingRight",
  "value": "4rem",
  "var": "var(--pf-c-about-modal-box__close--lg--PaddingRight)"
};
exports["default"] = exports.c_about_modal_box__close_lg_PaddingRight;
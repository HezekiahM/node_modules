"use strict";
exports.__esModule = true;
exports.c_nav_m_horizontal__link_PaddingBottom = {
  "name": "--pf-c-nav--m-horizontal__link--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-nav--m-horizontal__link--PaddingBottom)"
};
exports["default"] = exports.c_nav_m_horizontal__link_PaddingBottom;
"use strict";
exports.__esModule = true;
exports.c_options_menu__toggle_focus_BorderBottomWidth = {
  "name": "--pf-c-options-menu__toggle--focus--BorderBottomWidth",
  "value": "2px",
  "var": "var(--pf-c-options-menu__toggle--focus--BorderBottomWidth)"
};
exports["default"] = exports.c_options_menu__toggle_focus_BorderBottomWidth;
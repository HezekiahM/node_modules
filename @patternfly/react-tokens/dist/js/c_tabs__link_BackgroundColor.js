"use strict";
exports.__esModule = true;
exports.c_tabs__link_BackgroundColor = {
  "name": "--pf-c-tabs__link--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-tabs__link--BackgroundColor)"
};
exports["default"] = exports.c_tabs__link_BackgroundColor;
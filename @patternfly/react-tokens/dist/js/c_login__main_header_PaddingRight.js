"use strict";
exports.__esModule = true;
exports.c_login__main_header_PaddingRight = {
  "name": "--pf-c-login__main-header--PaddingRight",
  "value": "2rem",
  "var": "var(--pf-c-login__main-header--PaddingRight)"
};
exports["default"] = exports.c_login__main_header_PaddingRight;
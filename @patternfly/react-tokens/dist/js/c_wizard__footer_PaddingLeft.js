"use strict";
exports.__esModule = true;
exports.c_wizard__footer_PaddingLeft = {
  "name": "--pf-c-wizard__footer--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-wizard__footer--PaddingLeft)"
};
exports["default"] = exports.c_wizard__footer_PaddingLeft;
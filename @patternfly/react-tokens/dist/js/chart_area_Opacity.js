"use strict";
exports.__esModule = true;
exports.chart_area_Opacity = {
  "name": "--pf-chart-area--Opacity",
  "value": 0.3,
  "var": "var(--pf-chart-area--Opacity)"
};
exports["default"] = exports.chart_area_Opacity;
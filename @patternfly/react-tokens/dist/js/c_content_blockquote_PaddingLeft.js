"use strict";
exports.__esModule = true;
exports.c_content_blockquote_PaddingLeft = {
  "name": "--pf-c-content--blockquote--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-content--blockquote--PaddingLeft)"
};
exports["default"] = exports.c_content_blockquote_PaddingLeft;
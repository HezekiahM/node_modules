"use strict";
exports.__esModule = true;
exports.c_wizard__nav_list_xl_PaddingTop = {
  "name": "--pf-c-wizard__nav-list--xl--PaddingTop",
  "value": "1.5rem",
  "var": "var(--pf-c-wizard__nav-list--xl--PaddingTop)"
};
exports["default"] = exports.c_wizard__nav_list_xl_PaddingTop;
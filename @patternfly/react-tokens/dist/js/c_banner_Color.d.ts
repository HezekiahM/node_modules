export const c_banner_Color: {
  "name": "--pf-c-banner--Color",
  "value": "#151515",
  "var": "var(--pf-c-banner--Color)"
};
export default c_banner_Color;
"use strict";
exports.__esModule = true;
exports.c_select__menu_BackgroundColor = {
  "name": "--pf-c-select__menu--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-select__menu--BackgroundColor)"
};
exports["default"] = exports.c_select__menu_BackgroundColor;
"use strict";
exports.__esModule = true;
exports.c_data_list__item_action_MarginLeft = {
  "name": "--pf-c-data-list__item-action--MarginLeft",
  "value": "1rem",
  "var": "var(--pf-c-data-list__item-action--MarginLeft)"
};
exports["default"] = exports.c_data_list__item_action_MarginLeft;
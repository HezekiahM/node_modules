"use strict";
exports.__esModule = true;
exports.global_disabled_color_100 = {
  "name": "--pf-global--disabled-color--100",
  "value": "#6a6e73",
  "var": "var(--pf-global--disabled-color--100)"
};
exports["default"] = exports.global_disabled_color_100;
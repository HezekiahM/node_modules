"use strict";
exports.__esModule = true;
exports.global_BorderWidth_sm = {
  "name": "--pf-global--BorderWidth--sm",
  "value": "1px",
  "var": "var(--pf-global--BorderWidth--sm)"
};
exports["default"] = exports.global_BorderWidth_sm;
"use strict";
exports.__esModule = true;
exports.c_options_menu__menu_item_PaddingRight = {
  "name": "--pf-c-options-menu__menu-item--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-options-menu__menu-item--PaddingRight)"
};
exports["default"] = exports.c_options_menu__menu_item_PaddingRight;
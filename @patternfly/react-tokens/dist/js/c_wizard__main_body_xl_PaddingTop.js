"use strict";
exports.__esModule = true;
exports.c_wizard__main_body_xl_PaddingTop = {
  "name": "--pf-c-wizard__main-body--xl--PaddingTop",
  "value": "1.5rem",
  "var": "var(--pf-c-wizard__main-body--xl--PaddingTop)"
};
exports["default"] = exports.c_wizard__main_body_xl_PaddingTop;
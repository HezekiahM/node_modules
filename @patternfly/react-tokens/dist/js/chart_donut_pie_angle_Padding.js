"use strict";
exports.__esModule = true;
exports.chart_donut_pie_angle_Padding = {
  "name": "--pf-chart-donut--pie--angle--Padding",
  "value": 1,
  "var": "var(--pf-chart-donut--pie--angle--Padding)"
};
exports["default"] = exports.chart_donut_pie_angle_Padding;
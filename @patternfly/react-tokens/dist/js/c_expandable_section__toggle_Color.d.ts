export const c_expandable_section__toggle_Color: {
  "name": "--pf-c-expandable-section__toggle--Color",
  "value": "#004080",
  "var": "var(--pf-c-expandable-section__toggle--Color)"
};
export default c_expandable_section__toggle_Color;
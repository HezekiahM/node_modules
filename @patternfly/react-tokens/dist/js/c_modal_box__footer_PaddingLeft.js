"use strict";
exports.__esModule = true;
exports.c_modal_box__footer_PaddingLeft = {
  "name": "--pf-c-modal-box__footer--PaddingLeft",
  "value": "1.5rem",
  "var": "var(--pf-c-modal-box__footer--PaddingLeft)"
};
exports["default"] = exports.c_modal_box__footer_PaddingLeft;
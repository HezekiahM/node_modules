"use strict";
exports.__esModule = true;
exports.c_skip_to_content_focus_Left = {
  "name": "--pf-c-skip-to-content--focus--Left",
  "value": "1rem",
  "var": "var(--pf-c-skip-to-content--focus--Left)"
};
exports["default"] = exports.c_skip_to_content_focus_Left;
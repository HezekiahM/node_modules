"use strict";
exports.__esModule = true;
exports.c_options_menu__toggle_button_PaddingBottom = {
  "name": "--pf-c-options-menu__toggle-button--PaddingBottom",
  "value": "0.375rem",
  "var": "var(--pf-c-options-menu__toggle-button--PaddingBottom)"
};
exports["default"] = exports.c_options_menu__toggle_button_PaddingBottom;
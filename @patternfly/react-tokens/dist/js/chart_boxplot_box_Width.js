"use strict";
exports.__esModule = true;
exports.chart_boxplot_box_Width = {
  "name": "--pf-chart-boxplot--box--Width",
  "value": 20,
  "var": "var(--pf-chart-boxplot--box--Width)"
};
exports["default"] = exports.chart_boxplot_box_Width;
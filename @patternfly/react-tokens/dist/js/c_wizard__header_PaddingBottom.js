"use strict";
exports.__esModule = true;
exports.c_wizard__header_PaddingBottom = {
  "name": "--pf-c-wizard__header--PaddingBottom",
  "value": "1.5rem",
  "var": "var(--pf-c-wizard__header--PaddingBottom)"
};
exports["default"] = exports.c_wizard__header_PaddingBottom;
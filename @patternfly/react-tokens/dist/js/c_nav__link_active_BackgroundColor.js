"use strict";
exports.__esModule = true;
exports.c_nav__link_active_BackgroundColor = {
  "name": "--pf-c-nav__link--active--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-nav__link--active--BackgroundColor)"
};
exports["default"] = exports.c_nav__link_active_BackgroundColor;
"use strict";
exports.__esModule = true;
exports.c_alert_m_success__title_Color = {
  "name": "--pf-c-alert--m-success__title--Color",
  "value": "#0f280d",
  "var": "var(--pf-c-alert--m-success__title--Color)"
};
exports["default"] = exports.c_alert_m_success__title_Color;
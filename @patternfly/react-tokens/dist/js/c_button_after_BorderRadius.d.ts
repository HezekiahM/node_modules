export const c_button_after_BorderRadius: {
  "name": "--pf-c-button--after--BorderRadius",
  "value": "3px",
  "var": "var(--pf-c-button--after--BorderRadius)"
};
export default c_button_after_BorderRadius;
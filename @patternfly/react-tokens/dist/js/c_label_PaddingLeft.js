"use strict";
exports.__esModule = true;
exports.c_label_PaddingLeft = {
  "name": "--pf-c-label--PaddingLeft",
  "value": "0.5rem",
  "var": "var(--pf-c-label--PaddingLeft)"
};
exports["default"] = exports.c_label_PaddingLeft;
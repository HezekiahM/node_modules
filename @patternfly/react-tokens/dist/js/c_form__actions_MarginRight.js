"use strict";
exports.__esModule = true;
exports.c_form__actions_MarginRight = {
  "name": "--pf-c-form__actions--MarginRight",
  "value": "calc(0.5rem * -1)",
  "var": "var(--pf-c-form__actions--MarginRight)"
};
exports["default"] = exports.c_form__actions_MarginRight;
"use strict";
exports.__esModule = true;
exports.c_nav__link_m_current_after_BorderColor = {
  "name": "--pf-c-nav__link--m-current--after--BorderColor",
  "value": "#2b9af3",
  "var": "var(--pf-c-nav__link--m-current--after--BorderColor)"
};
exports["default"] = exports.c_nav__link_m_current_after_BorderColor;
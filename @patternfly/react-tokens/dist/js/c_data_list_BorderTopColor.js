"use strict";
exports.__esModule = true;
exports.c_data_list_BorderTopColor = {
  "name": "--pf-c-data-list--BorderTopColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-data-list--BorderTopColor)"
};
exports["default"] = exports.c_data_list_BorderTopColor;
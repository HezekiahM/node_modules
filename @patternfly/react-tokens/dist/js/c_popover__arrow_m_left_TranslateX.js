"use strict";
exports.__esModule = true;
exports.c_popover__arrow_m_left_TranslateX = {
  "name": "--pf-c-popover__arrow--m-left--TranslateX",
  "value": "50%",
  "var": "var(--pf-c-popover__arrow--m-left--TranslateX)"
};
exports["default"] = exports.c_popover__arrow_m_left_TranslateX;
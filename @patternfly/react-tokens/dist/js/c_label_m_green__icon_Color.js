"use strict";
exports.__esModule = true;
exports.c_label_m_green__icon_Color = {
  "name": "--pf-c-label--m-green__icon--Color",
  "value": "#3e8635",
  "var": "var(--pf-c-label--m-green__icon--Color)"
};
exports["default"] = exports.c_label_m_green__icon_Color;
"use strict";
exports.__esModule = true;
exports.c_wizard__close_Top = {
  "name": "--pf-c-wizard__close--Top",
  "value": "calc(1.5rem - 0.375rem)",
  "var": "var(--pf-c-wizard__close--Top)"
};
exports["default"] = exports.c_wizard__close_Top;
"use strict";
exports.__esModule = true;
exports.c_select__toggle_LineHeight = {
  "name": "--pf-c-select__toggle--LineHeight",
  "value": "1.5",
  "var": "var(--pf-c-select__toggle--LineHeight)"
};
exports["default"] = exports.c_select__toggle_LineHeight;
"use strict";
exports.__esModule = true;
exports.c_nav__subnav_MaxHeight = {
  "name": "--pf-c-nav__subnav--MaxHeight",
  "value": "100%",
  "var": "var(--pf-c-nav__subnav--MaxHeight)"
};
exports["default"] = exports.c_nav__subnav_MaxHeight;
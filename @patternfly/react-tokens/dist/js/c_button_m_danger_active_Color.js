"use strict";
exports.__esModule = true;
exports.c_button_m_danger_active_Color = {
  "name": "--pf-c-button--m-danger--active--Color",
  "value": "#fff",
  "var": "var(--pf-c-button--m-danger--active--Color)"
};
exports["default"] = exports.c_button_m_danger_active_Color;
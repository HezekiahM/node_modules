"use strict";
exports.__esModule = true;
exports.global_FontSize_4xl = {
  "name": "--pf-global--FontSize--4xl",
  "value": "2.25rem",
  "var": "var(--pf-global--FontSize--4xl)"
};
exports["default"] = exports.global_FontSize_4xl;
"use strict";
exports.__esModule = true;
exports.c_simple_list__item_link_focus_BackgroundColor = {
  "name": "--pf-c-simple-list__item-link--focus--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-simple-list__item-link--focus--BackgroundColor)"
};
exports["default"] = exports.c_simple_list__item_link_focus_BackgroundColor;
"use strict";
exports.__esModule = true;
exports.c_chip_group_m_category_BorderRadius = {
  "name": "--pf-c-chip-group--m-category--BorderRadius",
  "value": "3px",
  "var": "var(--pf-c-chip-group--m-category--BorderRadius)"
};
exports["default"] = exports.c_chip_group_m_category_BorderRadius;
"use strict";
exports.__esModule = true;
exports.chart_global_BorderWidth_lg = {
  "name": "--pf-chart-global--BorderWidth--lg",
  "value": 8,
  "var": "var(--pf-chart-global--BorderWidth--lg)"
};
exports["default"] = exports.chart_global_BorderWidth_lg;
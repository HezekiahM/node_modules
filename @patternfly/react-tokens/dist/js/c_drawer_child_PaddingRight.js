"use strict";
exports.__esModule = true;
exports.c_drawer_child_PaddingRight = {
  "name": "--pf-c-drawer--child--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-drawer--child--PaddingRight)"
};
exports["default"] = exports.c_drawer_child_PaddingRight;
"use strict";
exports.__esModule = true;
exports.c_nav__section_title_PaddingBottom = {
  "name": "--pf-c-nav__section-title--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-nav__section-title--PaddingBottom)"
};
exports["default"] = exports.c_nav__section_title_PaddingBottom;
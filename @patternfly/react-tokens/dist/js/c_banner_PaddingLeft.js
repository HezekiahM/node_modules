"use strict";
exports.__esModule = true;
exports.c_banner_PaddingLeft = {
  "name": "--pf-c-banner--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-banner--PaddingLeft)"
};
exports["default"] = exports.c_banner_PaddingLeft;
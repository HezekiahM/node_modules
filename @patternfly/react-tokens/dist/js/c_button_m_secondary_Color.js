"use strict";
exports.__esModule = true;
exports.c_button_m_secondary_Color = {
  "name": "--pf-c-button--m-secondary--Color",
  "value": "#fff",
  "var": "var(--pf-c-button--m-secondary--Color)"
};
exports["default"] = exports.c_button_m_secondary_Color;
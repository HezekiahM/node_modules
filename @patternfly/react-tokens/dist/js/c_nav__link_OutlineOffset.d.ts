export const c_nav__link_OutlineOffset: {
  "name": "--pf-c-nav__link--OutlineOffset",
  "value": "calc(0.25rem * -1)",
  "var": "var(--pf-c-nav__link--OutlineOffset)"
};
export default c_nav__link_OutlineOffset;
"use strict";
exports.__esModule = true;
exports.chart_color_purple_500 = {
  "name": "--pf-chart-color-purple-500",
  "value": "#2a265f",
  "var": "var(--pf-chart-color-purple-500)"
};
exports["default"] = exports.chart_color_purple_500;
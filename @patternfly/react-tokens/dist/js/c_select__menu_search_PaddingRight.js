"use strict";
exports.__esModule = true;
exports.c_select__menu_search_PaddingRight = {
  "name": "--pf-c-select__menu-search--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-select__menu-search--PaddingRight)"
};
exports["default"] = exports.c_select__menu_search_PaddingRight;
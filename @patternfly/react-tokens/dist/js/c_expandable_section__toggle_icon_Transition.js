"use strict";
exports.__esModule = true;
exports.c_expandable_section__toggle_icon_Transition = {
  "name": "--pf-c-expandable-section__toggle-icon--Transition",
  "value": ".2s ease-in 0s",
  "var": "var(--pf-c-expandable-section__toggle-icon--Transition)"
};
exports["default"] = exports.c_expandable_section__toggle_icon_Transition;
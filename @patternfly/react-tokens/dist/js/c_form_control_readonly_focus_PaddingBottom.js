"use strict";
exports.__esModule = true;
exports.c_form_control_readonly_focus_PaddingBottom = {
  "name": "--pf-c-form-control--readonly--focus--PaddingBottom",
  "value": "calc(0.375rem - 1px)",
  "var": "var(--pf-c-form-control--readonly--focus--PaddingBottom)"
};
exports["default"] = exports.c_form_control_readonly_focus_PaddingBottom;
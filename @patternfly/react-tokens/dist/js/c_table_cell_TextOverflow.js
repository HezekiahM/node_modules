"use strict";
exports.__esModule = true;
exports.c_table_cell_TextOverflow = {
  "name": "--pf-c-table--cell--TextOverflow",
  "value": "clip",
  "var": "var(--pf-c-table--cell--TextOverflow)"
};
exports["default"] = exports.c_table_cell_TextOverflow;
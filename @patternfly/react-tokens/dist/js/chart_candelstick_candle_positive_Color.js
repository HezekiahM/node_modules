"use strict";
exports.__esModule = true;
exports.chart_candelstick_candle_positive_Color = {
  "name": "--pf-chart-candelstick--candle--positive--Color",
  "value": "#fff",
  "var": "var(--pf-chart-candelstick--candle--positive--Color)"
};
exports["default"] = exports.chart_candelstick_candle_positive_Color;
"use strict";
exports.__esModule = true;
exports.c_modal_box__footer_PaddingRight = {
  "name": "--pf-c-modal-box__footer--PaddingRight",
  "value": "1.5rem",
  "var": "var(--pf-c-modal-box__footer--PaddingRight)"
};
exports["default"] = exports.c_modal_box__footer_PaddingRight;
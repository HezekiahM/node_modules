"use strict";
exports.__esModule = true;
exports.global_palette_orange_400 = {
  "name": "--pf-global--palette--orange-400",
  "value": "#c46100",
  "var": "var(--pf-global--palette--orange-400)"
};
exports["default"] = exports.global_palette_orange_400;
"use strict";
exports.__esModule = true;
exports.c_wizard__description_Color = {
  "name": "--pf-c-wizard__description--Color",
  "value": "#f0f0f0",
  "var": "var(--pf-c-wizard__description--Color)"
};
exports["default"] = exports.c_wizard__description_Color;
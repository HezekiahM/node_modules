"use strict";
exports.__esModule = true;
exports.c_label_m_green__content_link_hover_before_BorderColor = {
  "name": "--pf-c-label--m-green__content--link--hover--before--BorderColor",
  "value": "#3e8635",
  "var": "var(--pf-c-label--m-green__content--link--hover--before--BorderColor)"
};
exports["default"] = exports.c_label_m_green__content_link_hover_before_BorderColor;
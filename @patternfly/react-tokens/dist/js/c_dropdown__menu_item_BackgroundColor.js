"use strict";
exports.__esModule = true;
exports.c_dropdown__menu_item_BackgroundColor = {
  "name": "--pf-c-dropdown__menu-item--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-dropdown__menu-item--BackgroundColor)"
};
exports["default"] = exports.c_dropdown__menu_item_BackgroundColor;
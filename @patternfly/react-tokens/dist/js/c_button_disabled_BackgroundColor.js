"use strict";
exports.__esModule = true;
exports.c_button_disabled_BackgroundColor = {
  "name": "--pf-c-button--disabled--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-button--disabled--BackgroundColor)"
};
exports["default"] = exports.c_button_disabled_BackgroundColor;
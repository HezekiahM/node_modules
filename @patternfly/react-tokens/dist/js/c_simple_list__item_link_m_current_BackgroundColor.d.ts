export const c_simple_list__item_link_m_current_BackgroundColor: {
  "name": "--pf-c-simple-list__item-link--m-current--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-simple-list__item-link--m-current--BackgroundColor)"
};
export default c_simple_list__item_link_m_current_BackgroundColor;
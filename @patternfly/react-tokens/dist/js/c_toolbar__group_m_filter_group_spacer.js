"use strict";
exports.__esModule = true;
exports.c_toolbar__group_m_filter_group_spacer = {
  "name": "--pf-c-toolbar__group--m-filter-group--spacer",
  "value": "1rem",
  "var": "var(--pf-c-toolbar__group--m-filter-group--spacer)"
};
exports["default"] = exports.c_toolbar__group_m_filter_group_spacer;
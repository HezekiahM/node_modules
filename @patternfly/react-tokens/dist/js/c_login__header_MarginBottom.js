"use strict";
exports.__esModule = true;
exports.c_login__header_MarginBottom = {
  "name": "--pf-c-login__header--MarginBottom",
  "value": "1rem",
  "var": "var(--pf-c-login__header--MarginBottom)"
};
exports["default"] = exports.c_login__header_MarginBottom;
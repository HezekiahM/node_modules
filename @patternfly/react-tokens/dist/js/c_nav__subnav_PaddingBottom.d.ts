export const c_nav__subnav_PaddingBottom: {
  "name": "--pf-c-nav__subnav--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-nav__subnav--PaddingBottom)"
};
export default c_nav__subnav_PaddingBottom;
"use strict";
exports.__esModule = true;
exports.c_alert_m_danger_BorderTopColor = {
  "name": "--pf-c-alert--m-danger--BorderTopColor",
  "value": "#c9190b",
  "var": "var(--pf-c-alert--m-danger--BorderTopColor)"
};
exports["default"] = exports.c_alert_m_danger_BorderTopColor;
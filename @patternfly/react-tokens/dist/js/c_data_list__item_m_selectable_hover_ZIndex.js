"use strict";
exports.__esModule = true;
exports.c_data_list__item_m_selectable_hover_ZIndex = {
  "name": "--pf-c-data-list__item--m-selectable--hover--ZIndex",
  "value": "calc(100 + 1)",
  "var": "var(--pf-c-data-list__item--m-selectable--hover--ZIndex)"
};
exports["default"] = exports.c_data_list__item_m_selectable_hover_ZIndex;
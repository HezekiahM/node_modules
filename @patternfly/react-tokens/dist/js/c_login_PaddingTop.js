"use strict";
exports.__esModule = true;
exports.c_login_PaddingTop = {
  "name": "--pf-c-login--PaddingTop",
  "value": "1.5rem",
  "var": "var(--pf-c-login--PaddingTop)"
};
exports["default"] = exports.c_login_PaddingTop;
"use strict";
exports.__esModule = true;
exports.c_tabs__link_PaddingRight = {
  "name": "--pf-c-tabs__link--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-tabs__link--PaddingRight)"
};
exports["default"] = exports.c_tabs__link_PaddingRight;
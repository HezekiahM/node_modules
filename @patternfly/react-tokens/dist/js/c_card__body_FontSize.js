"use strict";
exports.__esModule = true;
exports.c_card__body_FontSize = {
  "name": "--pf-c-card__body--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-card__body--FontSize)"
};
exports["default"] = exports.c_card__body_FontSize;
"use strict";
exports.__esModule = true;
exports.c_content_blockquote_PaddingBottom = {
  "name": "--pf-c-content--blockquote--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-content--blockquote--PaddingBottom)"
};
exports["default"] = exports.c_content_blockquote_PaddingBottom;
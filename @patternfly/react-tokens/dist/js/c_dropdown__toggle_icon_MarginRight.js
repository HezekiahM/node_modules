"use strict";
exports.__esModule = true;
exports.c_dropdown__toggle_icon_MarginRight = {
  "name": "--pf-c-dropdown__toggle-icon--MarginRight",
  "value": "0.5rem",
  "var": "var(--pf-c-dropdown__toggle-icon--MarginRight)"
};
exports["default"] = exports.c_dropdown__toggle_icon_MarginRight;
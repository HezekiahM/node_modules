"use strict";
exports.__esModule = true;
exports.chart_donut_pie_Padding = {
  "name": "--pf-chart-donut--pie--Padding",
  "value": 20,
  "var": "var(--pf-chart-donut--pie--Padding)"
};
exports["default"] = exports.chart_donut_pie_Padding;
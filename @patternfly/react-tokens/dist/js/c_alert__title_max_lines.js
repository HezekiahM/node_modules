"use strict";
exports.__esModule = true;
exports.c_alert__title_max_lines = {
  "name": "--pf-c-alert__title--max-lines",
  "value": "1",
  "var": "var(--pf-c-alert__title--max-lines)"
};
exports["default"] = exports.c_alert__title_max_lines;
export const c_data_list__item_row_xl_PaddingRight: {
  "name": "--pf-c-data-list__item-row--xl--PaddingRight",
  "value": "1.5rem",
  "var": "var(--pf-c-data-list__item-row--xl--PaddingRight)"
};
export default c_data_list__item_row_xl_PaddingRight;
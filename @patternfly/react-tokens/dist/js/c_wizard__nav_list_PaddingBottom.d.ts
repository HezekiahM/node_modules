export const c_wizard__nav_list_PaddingBottom: {
  "name": "--pf-c-wizard__nav-list--PaddingBottom",
  "value": "1.5rem",
  "var": "var(--pf-c-wizard__nav-list--PaddingBottom)"
};
export default c_wizard__nav_list_PaddingBottom;
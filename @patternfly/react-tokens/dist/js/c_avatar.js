"use strict";
exports.__esModule = true;
exports.c_avatar = {
  ".pf-c-avatar": {
    "c_avatar_BorderRadius": {
      "name": "--pf-c-avatar--BorderRadius",
      "value": "30em",
      "values": [
        "--pf-global--BorderRadius--lg",
        "$pf-global--BorderRadius--lg",
        "30em"
      ]
    },
    "c_avatar_Width": {
      "name": "--pf-c-avatar--Width",
      "value": "2.25rem"
    },
    "c_avatar_Height": {
      "name": "--pf-c-avatar--Height",
      "value": "2.25rem"
    }
  }
};
exports["default"] = exports.c_avatar;
"use strict";
exports.__esModule = true;
exports.c_tabs_m_vertical__link_PaddingBottom = {
  "name": "--pf-c-tabs--m-vertical__link--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-tabs--m-vertical__link--PaddingBottom)"
};
exports["default"] = exports.c_tabs_m_vertical__link_PaddingBottom;
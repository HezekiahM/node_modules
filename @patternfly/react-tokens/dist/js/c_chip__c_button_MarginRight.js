"use strict";
exports.__esModule = true;
exports.c_chip__c_button_MarginRight = {
  "name": "--pf-c-chip__c-button--MarginRight",
  "value": "calc(0.5rem / 2 * -1)",
  "var": "var(--pf-c-chip__c-button--MarginRight)"
};
exports["default"] = exports.c_chip__c_button_MarginRight;
"use strict";
exports.__esModule = true;
exports.c_spinner__ball_after_Height = {
  "name": "--pf-c-spinner__ball--after--Height",
  "value": "calc(3.375rem * .1)",
  "var": "var(--pf-c-spinner__ball--after--Height)"
};
exports["default"] = exports.c_spinner__ball_after_Height;
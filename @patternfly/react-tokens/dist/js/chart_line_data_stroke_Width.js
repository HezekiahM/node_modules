"use strict";
exports.__esModule = true;
exports.chart_line_data_stroke_Width = {
  "name": "--pf-chart-line--data--stroke--Width",
  "value": 2,
  "var": "var(--pf-chart-line--data--stroke--Width)"
};
exports["default"] = exports.chart_line_data_stroke_Width;
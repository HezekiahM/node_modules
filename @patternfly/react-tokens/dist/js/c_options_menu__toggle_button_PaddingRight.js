"use strict";
exports.__esModule = true;
exports.c_options_menu__toggle_button_PaddingRight = {
  "name": "--pf-c-options-menu__toggle-button--PaddingRight",
  "value": "0.5rem",
  "var": "var(--pf-c-options-menu__toggle-button--PaddingRight)"
};
exports["default"] = exports.c_options_menu__toggle_button_PaddingRight;
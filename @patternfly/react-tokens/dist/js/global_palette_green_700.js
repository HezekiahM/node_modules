"use strict";
exports.__esModule = true;
exports.global_palette_green_700 = {
  "name": "--pf-global--palette--green-700",
  "value": "#0f280d",
  "var": "var(--pf-global--palette--green-700)"
};
exports["default"] = exports.global_palette_green_700;
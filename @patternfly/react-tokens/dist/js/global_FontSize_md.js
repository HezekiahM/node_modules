"use strict";
exports.__esModule = true;
exports.global_FontSize_md = {
  "name": "--pf-global--FontSize--md",
  "value": "1rem",
  "var": "var(--pf-global--FontSize--md)"
};
exports["default"] = exports.global_FontSize_md;
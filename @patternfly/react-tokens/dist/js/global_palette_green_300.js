"use strict";
exports.__esModule = true;
exports.global_palette_green_300 = {
  "name": "--pf-global--palette--green-300",
  "value": "#6ec664",
  "var": "var(--pf-global--palette--green-300)"
};
exports["default"] = exports.global_palette_green_300;
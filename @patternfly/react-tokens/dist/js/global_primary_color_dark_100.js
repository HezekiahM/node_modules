"use strict";
exports.__esModule = true;
exports.global_primary_color_dark_100 = {
  "name": "--pf-global--primary-color--dark-100",
  "value": "#06c",
  "var": "var(--pf-global--primary-color--dark-100)"
};
exports["default"] = exports.global_primary_color_dark_100;
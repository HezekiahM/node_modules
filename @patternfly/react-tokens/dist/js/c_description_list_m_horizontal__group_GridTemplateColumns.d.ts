export const c_description_list_m_horizontal__group_GridTemplateColumns: {
  "name": "--pf-c-description-list--m-horizontal__group--GridTemplateColumns",
  "value": "14ch minmax(10ch, auto)",
  "var": "var(--pf-c-description-list--m-horizontal__group--GridTemplateColumns)"
};
export default c_description_list_m_horizontal__group_GridTemplateColumns;
"use strict";
exports.__esModule = true;
exports.c_about_modal_box_sm_PaddingLeft = {
  "name": "--pf-c-about-modal-box--sm--PaddingLeft",
  "value": "4rem",
  "var": "var(--pf-c-about-modal-box--sm--PaddingLeft)"
};
exports["default"] = exports.c_about_modal_box_sm_PaddingLeft;
"use strict";
exports.__esModule = true;
exports.c_data_list__expandable_content_BorderTopWidth = {
  "name": "--pf-c-data-list__expandable-content--BorderTopWidth",
  "value": "1px",
  "var": "var(--pf-c-data-list__expandable-content--BorderTopWidth)"
};
exports["default"] = exports.c_data_list__expandable_content_BorderTopWidth;
"use strict";
exports.__esModule = true;
exports.c_select__menu_item_description_FontSize = {
  "name": "--pf-c-select__menu-item-description--FontSize",
  "value": "0.75rem",
  "var": "var(--pf-c-select__menu-item-description--FontSize)"
};
exports["default"] = exports.c_select__menu_item_description_FontSize;
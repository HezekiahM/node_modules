"use strict";
exports.__esModule = true;
exports.c_nav_m_horizontal__link_focus_BackgroundColor = {
  "name": "--pf-c-nav--m-horizontal__link--focus--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-nav--m-horizontal__link--focus--BackgroundColor)"
};
exports["default"] = exports.c_nav_m_horizontal__link_focus_BackgroundColor;
"use strict";
exports.__esModule = true;
exports.c_options_menu__toggle_expanded_BorderBottomWidth = {
  "name": "--pf-c-options-menu__toggle--expanded--BorderBottomWidth",
  "value": "2px",
  "var": "var(--pf-c-options-menu__toggle--expanded--BorderBottomWidth)"
};
exports["default"] = exports.c_options_menu__toggle_expanded_BorderBottomWidth;
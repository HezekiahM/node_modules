export const c_form__helper_text_m_success_Color: {
  "name": "--pf-c-form__helper-text--m-success--Color",
  "value": "#0f280d",
  "var": "var(--pf-c-form__helper-text--m-success--Color)"
};
export default c_form__helper_text_m_success_Color;
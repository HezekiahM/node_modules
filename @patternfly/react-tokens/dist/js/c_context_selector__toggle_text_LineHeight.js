"use strict";
exports.__esModule = true;
exports.c_context_selector__toggle_text_LineHeight = {
  "name": "--pf-c-context-selector__toggle-text--LineHeight",
  "value": "1.5",
  "var": "var(--pf-c-context-selector__toggle-text--LineHeight)"
};
exports["default"] = exports.c_context_selector__toggle_text_LineHeight;
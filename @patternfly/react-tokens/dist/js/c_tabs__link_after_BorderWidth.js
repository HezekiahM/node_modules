"use strict";
exports.__esModule = true;
exports.c_tabs__link_after_BorderWidth = {
  "name": "--pf-c-tabs__link--after--BorderWidth",
  "value": "3px",
  "var": "var(--pf-c-tabs__link--after--BorderWidth)"
};
exports["default"] = exports.c_tabs__link_after_BorderWidth;
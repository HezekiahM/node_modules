"use strict";
exports.__esModule = true;
exports.c_about_modal_box_BoxShadow = {
  "name": "--pf-c-about-modal-box--BoxShadow",
  "value": "0 0 100px 0 rgba(255, 255, 255, .05)",
  "var": "var(--pf-c-about-modal-box--BoxShadow)"
};
exports["default"] = exports.c_about_modal_box_BoxShadow;
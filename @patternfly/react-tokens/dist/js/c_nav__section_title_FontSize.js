"use strict";
exports.__esModule = true;
exports.c_nav__section_title_FontSize = {
  "name": "--pf-c-nav__section-title--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-nav__section-title--FontSize)"
};
exports["default"] = exports.c_nav__section_title_FontSize;
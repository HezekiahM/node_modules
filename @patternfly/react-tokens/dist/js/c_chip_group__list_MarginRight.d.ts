export const c_chip_group__list_MarginRight: {
  "name": "--pf-c-chip-group__list--MarginRight",
  "value": "calc(0.25rem * -1)",
  "var": "var(--pf-c-chip-group__list--MarginRight)"
};
export default c_chip_group__list_MarginRight;
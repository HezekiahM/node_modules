"use strict";
exports.__esModule = true;
exports.c_dropdown_m_top_m_expanded__toggle_icon_Rotate = {
  "name": "--pf-c-dropdown--m-top--m-expanded__toggle-icon--Rotate",
  "value": "180deg",
  "var": "var(--pf-c-dropdown--m-top--m-expanded__toggle-icon--Rotate)"
};
exports["default"] = exports.c_dropdown_m_top_m_expanded__toggle_icon_Rotate;
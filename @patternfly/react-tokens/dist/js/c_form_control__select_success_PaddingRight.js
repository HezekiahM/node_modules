"use strict";
exports.__esModule = true;
exports.c_form_control__select_success_PaddingRight = {
  "name": "--pf-c-form-control__select--success--PaddingRight",
  "value": "calc(0.5rem + 3rem)",
  "var": "var(--pf-c-form-control__select--success--PaddingRight)"
};
exports["default"] = exports.c_form_control__select_success_PaddingRight;
"use strict";
exports.__esModule = true;
exports.c_nav__scroll_button_before_BorderRightWidth = {
  "name": "--pf-c-nav__scroll-button--before--BorderRightWidth",
  "value": "1px",
  "var": "var(--pf-c-nav__scroll-button--before--BorderRightWidth)"
};
exports["default"] = exports.c_nav__scroll_button_before_BorderRightWidth;
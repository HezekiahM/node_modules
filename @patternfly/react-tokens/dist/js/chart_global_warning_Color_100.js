"use strict";
exports.__esModule = true;
exports.chart_global_warning_Color_100 = {
  "name": "--pf-chart-global--warning--Color--100",
  "value": "#ec7a08",
  "var": "var(--pf-chart-global--warning--Color--100)"
};
exports["default"] = exports.chart_global_warning_Color_100;
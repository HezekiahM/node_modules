"use strict";
exports.__esModule = true;
exports.chart_boxplot_max_Padding = {
  "name": "--pf-chart-boxplot--max--Padding",
  "value": 8,
  "var": "var(--pf-chart-boxplot--max--Padding)"
};
exports["default"] = exports.chart_boxplot_max_Padding;
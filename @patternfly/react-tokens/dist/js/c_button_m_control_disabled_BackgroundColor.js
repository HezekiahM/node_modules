"use strict";
exports.__esModule = true;
exports.c_button_m_control_disabled_BackgroundColor = {
  "name": "--pf-c-button--m-control--disabled--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-button--m-control--disabled--BackgroundColor)"
};
exports["default"] = exports.c_button_m_control_disabled_BackgroundColor;
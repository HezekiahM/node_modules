"use strict";
exports.__esModule = true;
exports.c_data_list_sm_BorderTopWidth = {
  "name": "--pf-c-data-list--sm--BorderTopWidth",
  "value": "1px",
  "var": "var(--pf-c-data-list--sm--BorderTopWidth)"
};
exports["default"] = exports.c_data_list_sm_BorderTopWidth;
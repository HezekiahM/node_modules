"use strict";
exports.__esModule = true;
exports.c_search_input__text_focus_within_after_BorderBottomWidth = {
  "name": "--pf-c-search-input__text--focus-within--after--BorderBottomWidth",
  "value": "2px",
  "var": "var(--pf-c-search-input__text--focus-within--after--BorderBottomWidth)"
};
exports["default"] = exports.c_search_input__text_focus_within_after_BorderBottomWidth;
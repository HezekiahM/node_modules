export const global_FontWeight_bold: {
  "name": "--pf-global--FontWeight--bold",
  "value": "600",
  "var": "var(--pf-global--FontWeight--bold)"
};
export default global_FontWeight_bold;
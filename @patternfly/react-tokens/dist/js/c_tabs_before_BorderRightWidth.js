"use strict";
exports.__esModule = true;
exports.c_tabs_before_BorderRightWidth = {
  "name": "--pf-c-tabs--before--BorderRightWidth",
  "value": "1px",
  "var": "var(--pf-c-tabs--before--BorderRightWidth)"
};
exports["default"] = exports.c_tabs_before_BorderRightWidth;
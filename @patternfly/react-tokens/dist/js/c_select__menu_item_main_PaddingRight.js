"use strict";
exports.__esModule = true;
exports.c_select__menu_item_main_PaddingRight = {
  "name": "--pf-c-select__menu-item-main--PaddingRight",
  "value": "3rem",
  "var": "var(--pf-c-select__menu-item-main--PaddingRight)"
};
exports["default"] = exports.c_select__menu_item_main_PaddingRight;
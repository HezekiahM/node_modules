"use strict";
exports.__esModule = true;
exports.c_about_modal_box__brand_sm_PaddingLeft = {
  "name": "--pf-c-about-modal-box__brand--sm--PaddingLeft",
  "value": "4rem",
  "var": "var(--pf-c-about-modal-box__brand--sm--PaddingLeft)"
};
exports["default"] = exports.c_about_modal_box__brand_sm_PaddingLeft;
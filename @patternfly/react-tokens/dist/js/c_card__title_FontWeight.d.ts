export const c_card__title_FontWeight: {
  "name": "--pf-c-card__title--FontWeight",
  "value": "700",
  "var": "var(--pf-c-card__title--FontWeight)"
};
export default c_card__title_FontWeight;
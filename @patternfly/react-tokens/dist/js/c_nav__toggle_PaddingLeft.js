"use strict";
exports.__esModule = true;
exports.c_nav__toggle_PaddingLeft = {
  "name": "--pf-c-nav__toggle--PaddingLeft",
  "value": "0.5rem",
  "var": "var(--pf-c-nav__toggle--PaddingLeft)"
};
exports["default"] = exports.c_nav__toggle_PaddingLeft;
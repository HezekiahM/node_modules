"use strict";
exports.__esModule = true;
exports.c_nav__section_MarginTop = {
  "name": "--pf-c-nav__section--MarginTop",
  "value": "2rem",
  "var": "var(--pf-c-nav__section--MarginTop)"
};
exports["default"] = exports.c_nav__section_MarginTop;
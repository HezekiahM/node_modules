"use strict";
exports.__esModule = true;
exports.c_context_selector__menu_list_item_PaddingRight = {
  "name": "--pf-c-context-selector__menu-list-item--PaddingRight",
  "value": "1.5rem",
  "var": "var(--pf-c-context-selector__menu-list-item--PaddingRight)"
};
exports["default"] = exports.c_context_selector__menu_list_item_PaddingRight;
"use strict";
exports.__esModule = true;
exports.c_table_m_compact_cell_PaddingBottom = {
  "name": "--pf-c-table--m-compact--cell--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-table--m-compact--cell--PaddingBottom)"
};
exports["default"] = exports.c_table_m_compact_cell_PaddingBottom;
"use strict";
exports.__esModule = true;
exports.chart_color_gold_300 = {
  "name": "--pf-chart-color-gold-300",
  "value": "#f4c145",
  "var": "var(--pf-chart-color-gold-300)"
};
exports["default"] = exports.chart_color_gold_300;
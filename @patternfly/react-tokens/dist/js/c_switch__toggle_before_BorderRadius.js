"use strict";
exports.__esModule = true;
exports.c_switch__toggle_before_BorderRadius = {
  "name": "--pf-c-switch__toggle--before--BorderRadius",
  "value": "30em",
  "var": "var(--pf-c-switch__toggle--before--BorderRadius)"
};
exports["default"] = exports.c_switch__toggle_before_BorderRadius;
"use strict";
exports.__esModule = true;
exports.c_wizard__main_body_PaddingLeft = {
  "name": "--pf-c-wizard__main-body--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-wizard__main-body--PaddingLeft)"
};
exports["default"] = exports.c_wizard__main_body_PaddingLeft;
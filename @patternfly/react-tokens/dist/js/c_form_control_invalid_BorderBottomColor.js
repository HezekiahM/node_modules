"use strict";
exports.__esModule = true;
exports.c_form_control_invalid_BorderBottomColor = {
  "name": "--pf-c-form-control--invalid--BorderBottomColor",
  "value": "#c9190b",
  "var": "var(--pf-c-form-control--invalid--BorderBottomColor)"
};
exports["default"] = exports.c_form_control_invalid_BorderBottomColor;
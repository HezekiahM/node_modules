export const c_modal_box__title_LineHeight: {
  "name": "--pf-c-modal-box__title--LineHeight",
  "value": "1.3",
  "var": "var(--pf-c-modal-box__title--LineHeight)"
};
export default c_modal_box__title_LineHeight;
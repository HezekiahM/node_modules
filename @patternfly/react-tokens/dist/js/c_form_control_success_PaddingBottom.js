"use strict";
exports.__esModule = true;
exports.c_form_control_success_PaddingBottom = {
  "name": "--pf-c-form-control--success--PaddingBottom",
  "value": "calc(0.375rem - 2px)",
  "var": "var(--pf-c-form-control--success--PaddingBottom)"
};
exports["default"] = exports.c_form_control_success_PaddingBottom;
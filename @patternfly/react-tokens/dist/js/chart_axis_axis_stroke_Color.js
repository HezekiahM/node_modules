"use strict";
exports.__esModule = true;
exports.chart_axis_axis_stroke_Color = {
  "name": "--pf-chart-axis--axis--stroke--Color",
  "value": "#d2d2d2",
  "var": "var(--pf-chart-axis--axis--stroke--Color)"
};
exports["default"] = exports.chart_axis_axis_stroke_Color;
"use strict";
exports.__esModule = true;
exports.global_breakpoint_sm = {
  "name": "--pf-global--breakpoint--sm",
  "value": "576px",
  "var": "var(--pf-global--breakpoint--sm)"
};
exports["default"] = exports.global_breakpoint_sm;
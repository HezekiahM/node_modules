"use strict";
exports.__esModule = true;
exports.c_login__main_body_PaddingBottom = {
  "name": "--pf-c-login__main-body--PaddingBottom",
  "value": "2rem",
  "var": "var(--pf-c-login__main-body--PaddingBottom)"
};
exports["default"] = exports.c_login__main_body_PaddingBottom;
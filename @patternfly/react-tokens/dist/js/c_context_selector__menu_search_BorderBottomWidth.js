"use strict";
exports.__esModule = true;
exports.c_context_selector__menu_search_BorderBottomWidth = {
  "name": "--pf-c-context-selector__menu-search--BorderBottomWidth",
  "value": "1px",
  "var": "var(--pf-c-context-selector__menu-search--BorderBottomWidth)"
};
exports["default"] = exports.c_context_selector__menu_search_BorderBottomWidth;
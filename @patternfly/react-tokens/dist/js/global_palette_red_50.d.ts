export const global_palette_red_50: {
  "name": "--pf-global--palette--red-50",
  "value": "#faeae8",
  "var": "var(--pf-global--palette--red-50)"
};
export default global_palette_red_50;
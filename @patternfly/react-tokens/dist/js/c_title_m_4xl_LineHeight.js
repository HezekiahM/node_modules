"use strict";
exports.__esModule = true;
exports.c_title_m_4xl_LineHeight = {
  "name": "--pf-c-title--m-4xl--LineHeight",
  "value": "1.3",
  "var": "var(--pf-c-title--m-4xl--LineHeight)"
};
exports["default"] = exports.c_title_m_4xl_LineHeight;
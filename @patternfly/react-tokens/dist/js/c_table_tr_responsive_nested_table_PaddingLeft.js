"use strict";
exports.__esModule = true;
exports.c_table_tr_responsive_nested_table_PaddingLeft = {
  "name": "--pf-c-table-tr--responsive--nested-table--PaddingLeft",
  "value": "1.5rem",
  "var": "var(--pf-c-table-tr--responsive--nested-table--PaddingLeft)"
};
exports["default"] = exports.c_table_tr_responsive_nested_table_PaddingLeft;
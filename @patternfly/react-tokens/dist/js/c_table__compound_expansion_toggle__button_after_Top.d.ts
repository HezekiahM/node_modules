export const c_table__compound_expansion_toggle__button_after_Top: {
  "name": "--pf-c-table__compound-expansion-toggle__button--after--Top",
  "value": "calc(1px * -1)",
  "var": "var(--pf-c-table__compound-expansion-toggle__button--after--Top)"
};
export default c_table__compound_expansion_toggle__button_after_Top;
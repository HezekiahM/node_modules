export const c_tabs_before_BorderColor: {
  "name": "--pf-c-tabs--before--BorderColor",
  "value": "#d2d2d2",
  "var": "var(--pf-c-tabs--before--BorderColor)"
};
export default c_tabs_before_BorderColor;
"use strict";
exports.__esModule = true;
exports.c_empty_state__secondary_child_MarginBottom = {
  "name": "--pf-c-empty-state__secondary--child--MarginBottom",
  "value": "0.25rem",
  "var": "var(--pf-c-empty-state__secondary--child--MarginBottom)"
};
exports["default"] = exports.c_empty_state__secondary_child_MarginBottom;
"use strict";
exports.__esModule = true;
exports.global_palette_orange_200 = {
  "name": "--pf-global--palette--orange-200",
  "value": "#ef9234",
  "var": "var(--pf-global--palette--orange-200)"
};
exports["default"] = exports.global_palette_orange_200;
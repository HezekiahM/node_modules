"use strict";
exports.__esModule = true;
exports.c_app_launcher__toggle_Color = {
  "name": "--pf-c-app-launcher__toggle--Color",
  "value": "#d2d2d2",
  "var": "var(--pf-c-app-launcher__toggle--Color)"
};
exports["default"] = exports.c_app_launcher__toggle_Color;
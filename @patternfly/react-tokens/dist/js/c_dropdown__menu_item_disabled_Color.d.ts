export const c_dropdown__menu_item_disabled_Color: {
  "name": "--pf-c-dropdown__menu-item--disabled--Color",
  "value": "#6a6e73",
  "var": "var(--pf-c-dropdown__menu-item--disabled--Color)"
};
export default c_dropdown__menu_item_disabled_Color;
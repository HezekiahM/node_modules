"use strict";
exports.__esModule = true;
exports.global_palette_light_green_200 = {
  "name": "--pf-global--palette--light-green-200",
  "value": "#c8eb79",
  "var": "var(--pf-global--palette--light-green-200)"
};
exports["default"] = exports.global_palette_light_green_200;
"use strict";
exports.__esModule = true;
exports.c_button_PaddingLeft = {
  "name": "--pf-c-button--PaddingLeft",
  "value": "0.5rem",
  "var": "var(--pf-c-button--PaddingLeft)"
};
exports["default"] = exports.c_button_PaddingLeft;
"use strict";
exports.__esModule = true;
exports.c_notification_drawer__header_PaddingRight = {
  "name": "--pf-c-notification-drawer__header--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-notification-drawer__header--PaddingRight)"
};
exports["default"] = exports.c_notification_drawer__header_PaddingRight;
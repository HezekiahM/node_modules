"use strict";
exports.__esModule = true;
exports.c_notification_drawer__list_item_m_danger__list_item_before_BackgroundColor = {
  "name": "--pf-c-notification-drawer__list-item--m-danger__list-item--before--BackgroundColor",
  "value": "#c9190b",
  "var": "var(--pf-c-notification-drawer__list-item--m-danger__list-item--before--BackgroundColor)"
};
exports["default"] = exports.c_notification_drawer__list_item_m_danger__list_item_before_BackgroundColor;
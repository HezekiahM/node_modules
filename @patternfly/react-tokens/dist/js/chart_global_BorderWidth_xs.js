"use strict";
exports.__esModule = true;
exports.chart_global_BorderWidth_xs = {
  "name": "--pf-chart-global--BorderWidth--xs",
  "value": 1,
  "var": "var(--pf-chart-global--BorderWidth--xs)"
};
exports["default"] = exports.chart_global_BorderWidth_xs;
"use strict";
exports.__esModule = true;
exports.c_drawer_child_PaddingLeft = {
  "name": "--pf-c-drawer--child--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-drawer--child--PaddingLeft)"
};
exports["default"] = exports.c_drawer_child_PaddingLeft;
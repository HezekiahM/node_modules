"use strict";
exports.__esModule = true;
exports.global_info_color_200 = {
  "name": "--pf-global--info-color--200",
  "value": "#002952",
  "var": "var(--pf-global--info-color--200)"
};
exports["default"] = exports.global_info_color_200;
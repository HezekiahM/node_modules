"use strict";
exports.__esModule = true;
exports.c_label_m_outline_m_cyan__content_link_focus_before_BorderColor = {
  "name": "--pf-c-label--m-outline--m-cyan__content--link--focus--before--BorderColor",
  "value": "#a2d9d9",
  "var": "var(--pf-c-label--m-outline--m-cyan__content--link--focus--before--BorderColor)"
};
exports["default"] = exports.c_label_m_outline_m_cyan__content_link_focus_before_BorderColor;
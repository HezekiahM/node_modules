"use strict";
exports.__esModule = true;
exports.c_skip_to_content_Top = {
  "name": "--pf-c-skip-to-content--Top",
  "value": "1rem",
  "var": "var(--pf-c-skip-to-content--Top)"
};
exports["default"] = exports.c_skip_to_content_Top;
"use strict";
exports.__esModule = true;
exports.chart_global_layout_Padding = {
  "name": "--pf-chart-global--layout--Padding",
  "value": 50,
  "var": "var(--pf-chart-global--layout--Padding)"
};
exports["default"] = exports.chart_global_layout_Padding;
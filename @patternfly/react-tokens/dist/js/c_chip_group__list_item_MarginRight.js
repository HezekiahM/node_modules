"use strict";
exports.__esModule = true;
exports.c_chip_group__list_item_MarginRight = {
  "name": "--pf-c-chip-group__list-item--MarginRight",
  "value": "0.25rem",
  "var": "var(--pf-c-chip-group__list-item--MarginRight)"
};
exports["default"] = exports.c_chip_group__list_item_MarginRight;
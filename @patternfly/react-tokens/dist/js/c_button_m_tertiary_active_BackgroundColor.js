"use strict";
exports.__esModule = true;
exports.c_button_m_tertiary_active_BackgroundColor = {
  "name": "--pf-c-button--m-tertiary--active--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-button--m-tertiary--active--BackgroundColor)"
};
exports["default"] = exports.c_button_m_tertiary_active_BackgroundColor;
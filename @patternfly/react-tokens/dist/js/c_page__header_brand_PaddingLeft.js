"use strict";
exports.__esModule = true;
exports.c_page__header_brand_PaddingLeft = {
  "name": "--pf-c-page__header-brand--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-page__header-brand--PaddingLeft)"
};
exports["default"] = exports.c_page__header_brand_PaddingLeft;
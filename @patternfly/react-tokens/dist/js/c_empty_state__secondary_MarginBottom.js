"use strict";
exports.__esModule = true;
exports.c_empty_state__secondary_MarginBottom = {
  "name": "--pf-c-empty-state__secondary--MarginBottom",
  "value": "calc(0.25rem * -1)",
  "var": "var(--pf-c-empty-state__secondary--MarginBottom)"
};
exports["default"] = exports.c_empty_state__secondary_MarginBottom;
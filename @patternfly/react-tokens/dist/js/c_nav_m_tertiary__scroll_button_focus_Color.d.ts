export const c_nav_m_tertiary__scroll_button_focus_Color: {
  "name": "--pf-c-nav--m-tertiary__scroll-button--focus--Color",
  "value": "#06c",
  "var": "var(--pf-c-nav--m-tertiary__scroll-button--focus--Color)"
};
export default c_nav_m_tertiary__scroll_button_focus_Color;
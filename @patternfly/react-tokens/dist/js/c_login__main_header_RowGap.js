"use strict";
exports.__esModule = true;
exports.c_login__main_header_RowGap = {
  "name": "--pf-c-login__main-header--RowGap",
  "value": "1rem",
  "var": "var(--pf-c-login__main-header--RowGap)"
};
exports["default"] = exports.c_login__main_header_RowGap;
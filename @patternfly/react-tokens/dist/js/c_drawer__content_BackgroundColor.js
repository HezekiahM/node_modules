"use strict";
exports.__esModule = true;
exports.c_drawer__content_BackgroundColor = {
  "name": "--pf-c-drawer__content--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-drawer__content--BackgroundColor)"
};
exports["default"] = exports.c_drawer__content_BackgroundColor;
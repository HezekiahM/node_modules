"use strict";
exports.__esModule = true;
exports.c_backdrop_ZIndex = {
  "name": "--pf-c-backdrop--ZIndex",
  "value": "400",
  "var": "var(--pf-c-backdrop--ZIndex)"
};
exports["default"] = exports.c_backdrop_ZIndex;
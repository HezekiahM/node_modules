export const c_alert_m_success__title_Color: {
  "name": "--pf-c-alert--m-success__title--Color",
  "value": "#0f280d",
  "var": "var(--pf-c-alert--m-success__title--Color)"
};
export default c_alert_m_success__title_Color;
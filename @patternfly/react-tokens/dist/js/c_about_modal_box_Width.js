"use strict";
exports.__esModule = true;
exports.c_about_modal_box_Width = {
  "name": "--pf-c-about-modal-box--Width",
  "value": "100vw",
  "var": "var(--pf-c-about-modal-box--Width)"
};
exports["default"] = exports.c_about_modal_box_Width;
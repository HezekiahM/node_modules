"use strict";
exports.__esModule = true;
exports.chart_bullet_qualitative_range_Width = {
  "name": "--pf-chart-bullet--qualitative-range--Width",
  "value": 30,
  "var": "var(--pf-chart-bullet--qualitative-range--Width)"
};
exports["default"] = exports.chart_bullet_qualitative_range_Width;
"use strict";
exports.__esModule = true;
exports.c_data_list__item_row_PaddingLeft = {
  "name": "--pf-c-data-list__item-row--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-data-list__item-row--PaddingLeft)"
};
exports["default"] = exports.c_data_list__item_row_PaddingLeft;
"use strict";
exports.__esModule = true;
exports.chart_donut_threshold_static_pie_angle_Padding = {
  "name": "--pf-chart-donut--threshold--static--pie--angle--Padding",
  "value": 1,
  "var": "var(--pf-chart-donut--threshold--static--pie--angle--Padding)"
};
exports["default"] = exports.chart_donut_threshold_static_pie_angle_Padding;
"use strict";
exports.__esModule = true;
exports.c_alert_BackgroundColor = {
  "name": "--pf-c-alert--BackgroundColor",
  "value": "#f2f9f9",
  "var": "var(--pf-c-alert--BackgroundColor)"
};
exports["default"] = exports.c_alert_BackgroundColor;
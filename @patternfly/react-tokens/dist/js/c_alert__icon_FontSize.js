"use strict";
exports.__esModule = true;
exports.c_alert__icon_FontSize = {
  "name": "--pf-c-alert__icon--FontSize",
  "value": "1.125rem",
  "var": "var(--pf-c-alert__icon--FontSize)"
};
exports["default"] = exports.c_alert__icon_FontSize;
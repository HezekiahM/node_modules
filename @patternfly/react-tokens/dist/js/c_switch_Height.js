"use strict";
exports.__esModule = true;
exports.c_switch_Height = {
  "name": "--pf-c-switch--Height",
  "value": "calc(1rem * 1.5)",
  "var": "var(--pf-c-switch--Height)"
};
exports["default"] = exports.c_switch_Height;
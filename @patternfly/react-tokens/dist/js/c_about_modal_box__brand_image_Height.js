"use strict";
exports.__esModule = true;
exports.c_about_modal_box__brand_image_Height = {
  "name": "--pf-c-about-modal-box__brand-image--Height",
  "value": "2.5rem",
  "var": "var(--pf-c-about-modal-box__brand-image--Height)"
};
exports["default"] = exports.c_about_modal_box__brand_image_Height;
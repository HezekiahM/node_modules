"use strict";
exports.__esModule = true;
exports.c_about_modal_box__brand_PaddingTop = {
  "name": "--pf-c-about-modal-box__brand--PaddingTop",
  "value": "3rem",
  "var": "var(--pf-c-about-modal-box__brand--PaddingTop)"
};
exports["default"] = exports.c_about_modal_box__brand_PaddingTop;
"use strict";
exports.__esModule = true;
exports.c_content_h4_MarginBottom = {
  "name": "--pf-c-content--h4--MarginBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-content--h4--MarginBottom)"
};
exports["default"] = exports.c_content_h4_MarginBottom;
"use strict";
exports.__esModule = true;
exports.c_tabs__scroll_button_before_BorderLeftWidth = {
  "name": "--pf-c-tabs__scroll-button--before--BorderLeftWidth",
  "value": "1px",
  "var": "var(--pf-c-tabs__scroll-button--before--BorderLeftWidth)"
};
exports["default"] = exports.c_tabs__scroll_button_before_BorderLeftWidth;
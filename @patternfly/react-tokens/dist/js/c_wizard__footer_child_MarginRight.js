"use strict";
exports.__esModule = true;
exports.c_wizard__footer_child_MarginRight = {
  "name": "--pf-c-wizard__footer--child--MarginRight",
  "value": "1rem",
  "var": "var(--pf-c-wizard__footer--child--MarginRight)"
};
exports["default"] = exports.c_wizard__footer_child_MarginRight;
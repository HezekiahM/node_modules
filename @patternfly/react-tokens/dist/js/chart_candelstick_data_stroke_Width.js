"use strict";
exports.__esModule = true;
exports.chart_candelstick_data_stroke_Width = {
  "name": "--pf-chart-candelstick--data--stroke--Width",
  "value": 1,
  "var": "var(--pf-chart-candelstick--data--stroke--Width)"
};
exports["default"] = exports.chart_candelstick_data_stroke_Width;
"use strict";
exports.__esModule = true;
exports.c_button_m_secondary_active_BackgroundColor = {
  "name": "--pf-c-button--m-secondary--active--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-button--m-secondary--active--BackgroundColor)"
};
exports["default"] = exports.c_button_m_secondary_active_BackgroundColor;
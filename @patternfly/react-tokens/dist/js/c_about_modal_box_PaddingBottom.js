"use strict";
exports.__esModule = true;
exports.c_about_modal_box_PaddingBottom = {
  "name": "--pf-c-about-modal-box--PaddingBottom",
  "value": "2rem",
  "var": "var(--pf-c-about-modal-box--PaddingBottom)"
};
exports["default"] = exports.c_about_modal_box_PaddingBottom;
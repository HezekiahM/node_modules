"use strict";
exports.__esModule = true;
exports.c_notification_drawer__list_item_m_read_before_Top = {
  "name": "--pf-c-notification-drawer__list-item--m-read--before--Top",
  "value": "calc(1px * -1)",
  "var": "var(--pf-c-notification-drawer__list-item--m-read--before--Top)"
};
exports["default"] = exports.c_notification_drawer__list_item_m_read_before_Top;
"use strict";
exports.__esModule = true;
exports.c_data_list__toggle_MarginTop = {
  "name": "--pf-c-data-list__toggle--MarginTop",
  "value": "calc(0.375rem * -1)",
  "var": "var(--pf-c-data-list__toggle--MarginTop)"
};
exports["default"] = exports.c_data_list__toggle_MarginTop;
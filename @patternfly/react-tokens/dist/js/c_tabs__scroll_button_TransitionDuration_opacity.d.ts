export const c_tabs__scroll_button_TransitionDuration_opacity: {
  "name": "--pf-c-tabs__scroll-button--TransitionDuration--opacity",
  "value": ".125s",
  "var": "var(--pf-c-tabs__scroll-button--TransitionDuration--opacity)"
};
export default c_tabs__scroll_button_TransitionDuration_opacity;
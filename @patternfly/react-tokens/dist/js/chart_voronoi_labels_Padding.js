"use strict";
exports.__esModule = true;
exports.chart_voronoi_labels_Padding = {
  "name": "--pf-chart-voronoi--labels--Padding",
  "value": 8,
  "var": "var(--pf-chart-voronoi--labels--Padding)"
};
exports["default"] = exports.chart_voronoi_labels_Padding;
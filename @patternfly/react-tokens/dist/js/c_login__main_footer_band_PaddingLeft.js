"use strict";
exports.__esModule = true;
exports.c_login__main_footer_band_PaddingLeft = {
  "name": "--pf-c-login__main-footer-band--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-login__main-footer-band--PaddingLeft)"
};
exports["default"] = exports.c_login__main_footer_band_PaddingLeft;
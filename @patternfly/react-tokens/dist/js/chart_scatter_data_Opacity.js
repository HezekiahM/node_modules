"use strict";
exports.__esModule = true;
exports.chart_scatter_data_Opacity = {
  "name": "--pf-chart-scatter--data--Opacity",
  "value": 1,
  "var": "var(--pf-chart-scatter--data--Opacity)"
};
exports["default"] = exports.chart_scatter_data_Opacity;
"use strict";
exports.__esModule = true;
exports.c_table_caption_FontSize = {
  "name": "--pf-c-table-caption--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-table-caption--FontSize)"
};
exports["default"] = exports.c_table_caption_FontSize;
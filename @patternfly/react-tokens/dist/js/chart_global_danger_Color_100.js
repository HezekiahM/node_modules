"use strict";
exports.__esModule = true;
exports.chart_global_danger_Color_100 = {
  "name": "--pf-chart-global--danger--Color--100",
  "value": "#c9190b",
  "var": "var(--pf-chart-global--danger--Color--100)"
};
exports["default"] = exports.chart_global_danger_Color_100;
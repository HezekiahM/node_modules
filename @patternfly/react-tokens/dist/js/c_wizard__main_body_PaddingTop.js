"use strict";
exports.__esModule = true;
exports.c_wizard__main_body_PaddingTop = {
  "name": "--pf-c-wizard__main-body--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-wizard__main-body--PaddingTop)"
};
exports["default"] = exports.c_wizard__main_body_PaddingTop;
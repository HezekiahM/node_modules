"use strict";
exports.__esModule = true;
exports.chart_donut_label_subtitle_Fill = {
  "name": "--pf-chart-donut--label--subtitle--Fill",
  "value": "#b8bbbe",
  "var": "var(--pf-chart-donut--label--subtitle--Fill)"
};
exports["default"] = exports.chart_donut_label_subtitle_Fill;
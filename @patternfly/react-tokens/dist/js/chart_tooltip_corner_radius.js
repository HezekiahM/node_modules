"use strict";
exports.__esModule = true;
exports.chart_tooltip_corner_radius = {
  "name": "--pf-chart-tooltip--corner-radius",
  "value": 0,
  "var": "var(--pf-chart-tooltip--corner-radius)"
};
exports["default"] = exports.chart_tooltip_corner_radius;
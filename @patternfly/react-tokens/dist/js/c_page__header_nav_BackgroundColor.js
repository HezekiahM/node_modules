"use strict";
exports.__esModule = true;
exports.c_page__header_nav_BackgroundColor = {
  "name": "--pf-c-page__header-nav--BackgroundColor",
  "value": "#212427",
  "var": "var(--pf-c-page__header-nav--BackgroundColor)"
};
exports["default"] = exports.c_page__header_nav_BackgroundColor;
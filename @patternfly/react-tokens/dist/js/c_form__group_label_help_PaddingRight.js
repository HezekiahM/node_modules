"use strict";
exports.__esModule = true;
exports.c_form__group_label_help_PaddingRight = {
  "name": "--pf-c-form__group-label-help--PaddingRight",
  "value": "0.25rem",
  "var": "var(--pf-c-form__group-label-help--PaddingRight)"
};
exports["default"] = exports.c_form__group_label_help_PaddingRight;
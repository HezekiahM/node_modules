"use strict";
exports.__esModule = true;
exports.c_spinner = {
  ".pf-c-spinner": {
    "c_spinner_AnimationDuration": {
      "name": "--pf-c-spinner--AnimationDuration",
      "value": "1.5s"
    },
    "c_spinner_AnimationTimingFunction": {
      "name": "--pf-c-spinner--AnimationTimingFunction",
      "value": "cubic-bezier(.77, .005, .315, 1)"
    },
    "c_spinner_diameter": {
      "name": "--pf-c-spinner--diameter",
      "value": "3.375rem",
      "values": [
        "--pf-global--icon--FontSize--xl",
        "$pf-global--icon--FontSize--xl",
        "pf-font-prem(54px)",
        "3.375rem"
      ]
    },
    "c_spinner_stroke_width_multiplier": {
      "name": "--pf-c-spinner--stroke-width-multiplier",
      "value": ".1"
    },
    "c_spinner_stroke_width": {
      "name": "--pf-c-spinner--stroke-width",
      "value": "calc(3.375rem * .1)",
      "values": [
        "calc(--pf-c-spinner--diameter * --pf-c-spinner--stroke-width-multiplier)",
        "calc(--pf-global--icon--FontSize--xl * .1)",
        "calc($pf-global--icon--FontSize--xl * .1)",
        "calc(pf-font-prem(54px) * .1)",
        "calc(3.375rem * .1)"
      ]
    },
    "c_spinner_Width": {
      "name": "--pf-c-spinner--Width",
      "value": "3.375rem",
      "values": [
        "--pf-c-spinner--diameter",
        "--pf-global--icon--FontSize--xl",
        "$pf-global--icon--FontSize--xl",
        "pf-font-prem(54px)",
        "3.375rem"
      ]
    },
    "c_spinner_Height": {
      "name": "--pf-c-spinner--Height",
      "value": "3.375rem",
      "values": [
        "--pf-c-spinner--diameter",
        "--pf-global--icon--FontSize--xl",
        "$pf-global--icon--FontSize--xl",
        "pf-font-prem(54px)",
        "3.375rem"
      ]
    },
    "c_spinner_Color": {
      "name": "--pf-c-spinner--Color",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_spinner_m_sm_diameter": {
      "name": "--pf-c-spinner--m-sm--diameter",
      "value": "0.625rem",
      "values": [
        "--pf-global--icon--FontSize--sm",
        "$pf-global--icon--FontSize--sm",
        "pf-font-prem(10px)",
        "0.625rem"
      ]
    },
    "c_spinner_m_md_diameter": {
      "name": "--pf-c-spinner--m-md--diameter",
      "value": "1.125rem",
      "values": [
        "--pf-global--icon--FontSize--md",
        "$pf-global--icon--FontSize--md",
        "pf-font-prem(18px)",
        "1.125rem"
      ]
    },
    "c_spinner_m_lg_diameter": {
      "name": "--pf-c-spinner--m-lg--diameter",
      "value": "1.5rem",
      "values": [
        "--pf-global--icon--FontSize--lg",
        "$pf-global--icon--FontSize--lg",
        "pf-font-prem(24px)",
        "1.5rem"
      ]
    },
    "c_spinner_m_xl_diameter": {
      "name": "--pf-c-spinner--m-xl--diameter",
      "value": "3.375rem",
      "values": [
        "--pf-global--icon--FontSize--xl",
        "$pf-global--icon--FontSize--xl",
        "pf-font-prem(54px)",
        "3.375rem"
      ]
    },
    "c_spinner__clipper_Width": {
      "name": "--pf-c-spinner__clipper--Width",
      "value": "3.375rem",
      "values": [
        "--pf-c-spinner--diameter",
        "--pf-global--icon--FontSize--xl",
        "$pf-global--icon--FontSize--xl",
        "pf-font-prem(54px)",
        "3.375rem"
      ]
    },
    "c_spinner__clipper_Height": {
      "name": "--pf-c-spinner__clipper--Height",
      "value": "3.375rem",
      "values": [
        "--pf-c-spinner--diameter",
        "--pf-global--icon--FontSize--xl",
        "$pf-global--icon--FontSize--xl",
        "pf-font-prem(54px)",
        "3.375rem"
      ]
    },
    "c_spinner__clipper_after_BoxShadowColor": {
      "name": "--pf-c-spinner__clipper--after--BoxShadowColor",
      "value": "#06c",
      "values": [
        "--pf-c-spinner--Color",
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_spinner__clipper_after_Width": {
      "name": "--pf-c-spinner__clipper--after--Width",
      "value": "3.375rem",
      "values": [
        "--pf-c-spinner--diameter",
        "--pf-global--icon--FontSize--xl",
        "$pf-global--icon--FontSize--xl",
        "pf-font-prem(54px)",
        "3.375rem"
      ]
    },
    "c_spinner__clipper_after_Height": {
      "name": "--pf-c-spinner__clipper--after--Height",
      "value": "3.375rem",
      "values": [
        "--pf-c-spinner--diameter",
        "--pf-global--icon--FontSize--xl",
        "$pf-global--icon--FontSize--xl",
        "pf-font-prem(54px)",
        "3.375rem"
      ]
    },
    "c_spinner__clipper_after_BoxShadowSpreadRadius": {
      "name": "--pf-c-spinner__clipper--after--BoxShadowSpreadRadius",
      "value": "calc(3.375rem * .1)",
      "values": [
        "--pf-c-spinner--stroke-width",
        "calc(--pf-c-spinner--diameter * --pf-c-spinner--stroke-width-multiplier)",
        "calc(--pf-global--icon--FontSize--xl * .1)",
        "calc($pf-global--icon--FontSize--xl * .1)",
        "calc(pf-font-prem(54px) * .1)",
        "calc(3.375rem * .1)"
      ]
    },
    "c_spinner__lead_ball_after_BackgroundColor": {
      "name": "--pf-c-spinner__lead-ball--after--BackgroundColor",
      "value": "#06c",
      "values": [
        "--pf-c-spinner--Color",
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_spinner__ball_after_Width": {
      "name": "--pf-c-spinner__ball--after--Width",
      "value": "calc(3.375rem * .1)",
      "values": [
        "--pf-c-spinner--stroke-width",
        "calc(--pf-c-spinner--diameter * --pf-c-spinner--stroke-width-multiplier)",
        "calc(--pf-global--icon--FontSize--xl * .1)",
        "calc($pf-global--icon--FontSize--xl * .1)",
        "calc(pf-font-prem(54px) * .1)",
        "calc(3.375rem * .1)"
      ]
    },
    "c_spinner__ball_after_Height": {
      "name": "--pf-c-spinner__ball--after--Height",
      "value": "calc(3.375rem * .1)",
      "values": [
        "--pf-c-spinner--stroke-width",
        "calc(--pf-c-spinner--diameter * --pf-c-spinner--stroke-width-multiplier)",
        "calc(--pf-global--icon--FontSize--xl * .1)",
        "calc($pf-global--icon--FontSize--xl * .1)",
        "calc(pf-font-prem(54px) * .1)",
        "calc(3.375rem * .1)"
      ]
    },
    "c_spinner__tail_ball_after_BackgroundColor": {
      "name": "--pf-c-spinner__tail-ball--after--BackgroundColor",
      "value": "#06c",
      "values": [
        "--pf-c-spinner--Color",
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-spinner.pf-m-sm": {
    "c_spinner_diameter": {
      "name": "--pf-c-spinner--diameter",
      "value": "0.625rem",
      "values": [
        "--pf-c-spinner--m-sm--diameter",
        "--pf-global--icon--FontSize--sm",
        "$pf-global--icon--FontSize--sm",
        "pf-font-prem(10px)",
        "0.625rem"
      ]
    }
  },
  ".pf-c-spinner.pf-m-md": {
    "c_spinner_diameter": {
      "name": "--pf-c-spinner--diameter",
      "value": "1.125rem",
      "values": [
        "--pf-c-spinner--m-md--diameter",
        "--pf-global--icon--FontSize--md",
        "$pf-global--icon--FontSize--md",
        "pf-font-prem(18px)",
        "1.125rem"
      ]
    }
  },
  ".pf-c-spinner.pf-m-lg": {
    "c_spinner_diameter": {
      "name": "--pf-c-spinner--diameter",
      "value": "1.5rem",
      "values": [
        "--pf-c-spinner--m-lg--diameter",
        "--pf-global--icon--FontSize--lg",
        "$pf-global--icon--FontSize--lg",
        "pf-font-prem(24px)",
        "1.5rem"
      ]
    }
  },
  ".pf-c-spinner.pf-m-xl": {
    "c_spinner_diameter": {
      "name": "--pf-c-spinner--diameter",
      "value": "3.375rem",
      "values": [
        "--pf-c-spinner--m-xl--diameter",
        "--pf-global--icon--FontSize--xl",
        "$pf-global--icon--FontSize--xl",
        "pf-font-prem(54px)",
        "3.375rem"
      ]
    }
  }
};
exports["default"] = exports.c_spinner;
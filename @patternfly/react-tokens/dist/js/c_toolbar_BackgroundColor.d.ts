export const c_toolbar_BackgroundColor: {
  "name": "--pf-c-toolbar--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-toolbar--BackgroundColor)"
};
export default c_toolbar_BackgroundColor;
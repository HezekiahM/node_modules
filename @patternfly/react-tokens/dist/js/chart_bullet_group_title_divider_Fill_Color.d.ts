export const chart_bullet_group_title_divider_Fill_Color: {
  "name": "--pf-chart-bullet--group-title--divider--Fill--Color",
  "value": "#f0f0f0",
  "var": "var(--pf-chart-bullet--group-title--divider--Fill--Color)"
};
export default chart_bullet_group_title_divider_Fill_Color;
"use strict";
exports.__esModule = true;
exports.c_context_selector = {
  ".pf-c-context-selector__menu": {
    "global_Color_100": {
      "name": "--pf-global--Color--100",
      "value": "#151515",
      "values": [
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "global_Color_200": {
      "name": "--pf-global--Color--200",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--dark-200",
        "$pf-global--Color--dark-200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "global_BorderColor_100": {
      "name": "--pf-global--BorderColor--100",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--dark-100",
        "$pf-global--BorderColor--dark-100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "global_primary_color_100": {
      "name": "--pf-global--primary-color--100",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--dark-100",
        "$pf-global--primary-color--dark-100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "global_link_Color": {
      "name": "--pf-global--link--Color",
      "value": "#06c",
      "values": [
        "--pf-global--link--Color--dark",
        "$pf-global--link--Color--dark",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "global_link_Color_hover": {
      "name": "--pf-global--link--Color--hover",
      "value": "#004080",
      "values": [
        "--pf-global--link--Color--dark--hover",
        "$pf-global--link--Color--dark--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "global_BackgroundColor_100": {
      "name": "--pf-global--BackgroundColor--100",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--light-100",
        "$pf-global--BackgroundColor--light-100",
        "$pf-color-white",
        "#fff"
      ]
    }
  },
  ".pf-c-context-selector": {
    "c_context_selector_Width": {
      "name": "--pf-c-context-selector--Width",
      "value": "15.625rem"
    },
    "c_context_selector__toggle_PaddingTop": {
      "name": "--pf-c-context-selector__toggle--PaddingTop",
      "value": "0.375rem",
      "values": [
        "--pf-global--spacer--form-element",
        "$pf-global--spacer--form-element",
        "pf-size-prem(6px)",
        "0.375rem"
      ]
    },
    "c_context_selector__toggle_PaddingRight": {
      "name": "--pf-c-context-selector__toggle--PaddingRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_context_selector__toggle_PaddingBottom": {
      "name": "--pf-c-context-selector__toggle--PaddingBottom",
      "value": "0.375rem",
      "values": [
        "--pf-global--spacer--form-element",
        "$pf-global--spacer--form-element",
        "pf-size-prem(6px)",
        "0.375rem"
      ]
    },
    "c_context_selector__toggle_PaddingLeft": {
      "name": "--pf-c-context-selector__toggle--PaddingLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_context_selector__toggle_BorderWidth": {
      "name": "--pf-c-context-selector__toggle--BorderWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_context_selector__toggle_BorderTopColor": {
      "name": "--pf-c-context-selector__toggle--BorderTopColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_context_selector__toggle_BorderRightColor": {
      "name": "--pf-c-context-selector__toggle--BorderRightColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_context_selector__toggle_BorderBottomColor": {
      "name": "--pf-c-context-selector__toggle--BorderBottomColor",
      "value": "#8a8d90",
      "values": [
        "--pf-global--BorderColor--200",
        "$pf-global--BorderColor--200",
        "$pf-color-black-500",
        "#8a8d90"
      ]
    },
    "c_context_selector__toggle_BorderLeftColor": {
      "name": "--pf-c-context-selector__toggle--BorderLeftColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_context_selector__toggle_Color": {
      "name": "--pf-c-context-selector__toggle--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_context_selector__toggle_hover_BorderBottomColor": {
      "name": "--pf-c-context-selector__toggle--hover--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_context_selector__toggle_active_BorderBottomWidth": {
      "name": "--pf-c-context-selector__toggle--active--BorderBottomWidth",
      "value": "2px",
      "values": [
        "--pf-global--BorderWidth--md",
        "$pf-global--BorderWidth--md",
        "2px"
      ]
    },
    "c_context_selector__toggle_active_BorderBottomColor": {
      "name": "--pf-c-context-selector__toggle--active--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_context_selector__toggle_expanded_BorderBottomWidth": {
      "name": "--pf-c-context-selector__toggle--expanded--BorderBottomWidth",
      "value": "2px",
      "values": [
        "--pf-global--BorderWidth--md",
        "$pf-global--BorderWidth--md",
        "2px"
      ]
    },
    "c_context_selector__toggle_expanded_BorderBottomColor": {
      "name": "--pf-c-context-selector__toggle--expanded--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_context_selector__toggle_text_FontSize": {
      "name": "--pf-c-context-selector__toggle-text--FontSize",
      "value": "1rem",
      "values": [
        "--pf-global--FontSize--md",
        "$pf-global--FontSize--md",
        "pf-font-prem(16px)",
        "1rem"
      ]
    },
    "c_context_selector__toggle_text_FontWeight": {
      "name": "--pf-c-context-selector__toggle-text--FontWeight",
      "value": "400",
      "values": [
        "--pf-global--FontWeight--normal",
        "$pf-global--FontWeight--normal",
        "400"
      ]
    },
    "c_context_selector__toggle_text_LineHeight": {
      "name": "--pf-c-context-selector__toggle-text--LineHeight",
      "value": "1.5",
      "values": [
        "--pf-global--LineHeight--md",
        "$pf-global--LineHeight--md",
        "1.5"
      ]
    },
    "c_context_selector__toggle_icon_MarginRight": {
      "name": "--pf-c-context-selector__toggle-icon--MarginRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_context_selector__toggle_icon_MarginLeft": {
      "name": "--pf-c-context-selector__toggle-icon--MarginLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_context_selector__menu_Top": {
      "name": "--pf-c-context-selector__menu--Top",
      "value": "calc(100% + 0.25rem)",
      "values": [
        "calc(100% + --pf-global--spacer--xs)",
        "calc(100% + $pf-global--spacer--xs)",
        "calc(100% + pf-size-prem(4px))",
        "calc(100% + 0.25rem)"
      ]
    },
    "c_context_selector__menu_ZIndex": {
      "name": "--pf-c-context-selector__menu--ZIndex",
      "value": "200",
      "values": [
        "--pf-global--ZIndex--sm",
        "$pf-global--ZIndex--sm",
        "200"
      ]
    },
    "c_context_selector__menu_PaddingTop": {
      "name": "--pf-c-context-selector__menu--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_context_selector__menu_BackgroundColor": {
      "name": "--pf-c-context-selector__menu--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--light-100",
        "$pf-global--BackgroundColor--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_context_selector__menu_BoxShadow": {
      "name": "--pf-c-context-selector__menu--BoxShadow",
      "value": "0 0.25rem 0.5rem 0rem rgba(3, 3, 3, 0.12), 0 0 0.25rem 0 rgba(3, 3, 3, 0.06)",
      "values": [
        "--pf-global--BoxShadow--md",
        "$pf-global--BoxShadow--md",
        "0 pf-size-prem(4px) pf-size-prem(8px) pf-size-prem(0) rgba($pf-color-black-1000, .12), 0 0 pf-size-prem(4px) 0 rgba($pf-color-black-1000, .06)",
        "0 pf-size-prem(4px) pf-size-prem(8px) pf-size-prem(0) rgba(#030303, .12), 0 0 pf-size-prem(4px) 0 rgba(#030303, .06)",
        "0 0.25rem 0.5rem 0rem rgba(3, 3, 3, 0.12), 0 0 0.25rem 0 rgba(3, 3, 3, 0.06)"
      ]
    },
    "c_context_selector__menu_search_PaddingTop": {
      "name": "--pf-c-context-selector__menu-search--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_context_selector__menu_search_PaddingRight": {
      "name": "--pf-c-context-selector__menu-search--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_context_selector__menu_search_PaddingBottom": {
      "name": "--pf-c-context-selector__menu-search--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_context_selector__menu_search_PaddingLeft": {
      "name": "--pf-c-context-selector__menu-search--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_context_selector__menu_search_BorderBottomColor": {
      "name": "--pf-c-context-selector__menu-search--BorderBottomColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_context_selector__menu_search_BorderBottomWidth": {
      "name": "--pf-c-context-selector__menu-search--BorderBottomWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_context_selector__menu_list_MaxHeight": {
      "name": "--pf-c-context-selector__menu-list--MaxHeight",
      "value": "12.5rem"
    },
    "c_context_selector__menu_list_item_PaddingTop": {
      "name": "--pf-c-context-selector__menu-list-item--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_context_selector__menu_list_item_PaddingRight": {
      "name": "--pf-c-context-selector__menu-list-item--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_context_selector__menu_list_item_PaddingBottom": {
      "name": "--pf-c-context-selector__menu-list-item--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_context_selector__menu_list_item_PaddingLeft": {
      "name": "--pf-c-context-selector__menu-list-item--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_context_selector__menu_list_item_hover_BackgroundColor": {
      "name": "--pf-c-context-selector__menu-list-item--hover--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_context_selector__menu_list_item_disabled_Color": {
      "name": "--pf-c-context-selector__menu-list-item--disabled--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--dark-200",
        "$pf-global--Color--dark-200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    }
  },
  ".pf-c-context-selector__toggle:hover::before": {
    "c_context_selector__toggle_BorderBottomColor": {
      "name": "--pf-c-context-selector__toggle--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-c-context-selector__toggle--hover--BorderBottomColor",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-context-selector__toggle:active::before": {
    "c_context_selector__toggle_BorderBottomColor": {
      "name": "--pf-c-context-selector__toggle--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-c-context-selector__toggle--active--BorderBottomColor",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-m-expanded > .pf-c-context-selector__toggle::before": {
    "c_context_selector__toggle_BorderBottomColor": {
      "name": "--pf-c-context-selector__toggle--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-c-context-selector__toggle--expanded--BorderBottomColor",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  }
};
exports["default"] = exports.c_context_selector;
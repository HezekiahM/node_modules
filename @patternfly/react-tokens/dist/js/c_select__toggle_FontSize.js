"use strict";
exports.__esModule = true;
exports.c_select__toggle_FontSize = {
  "name": "--pf-c-select__toggle--FontSize",
  "value": "1rem",
  "var": "var(--pf-c-select__toggle--FontSize)"
};
exports["default"] = exports.c_select__toggle_FontSize;
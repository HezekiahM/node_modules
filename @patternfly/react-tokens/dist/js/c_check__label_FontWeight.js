"use strict";
exports.__esModule = true;
exports.c_check__label_FontWeight = {
  "name": "--pf-c-check__label--FontWeight",
  "value": "400",
  "var": "var(--pf-c-check__label--FontWeight)"
};
exports["default"] = exports.c_check__label_FontWeight;
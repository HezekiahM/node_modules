"use strict";
exports.__esModule = true;
exports.c_data_list_m_compact__check_FontSize = {
  "name": "--pf-c-data-list--m-compact__check--FontSize",
  "value": "1rem",
  "var": "var(--pf-c-data-list--m-compact__check--FontSize)"
};
exports["default"] = exports.c_data_list_m_compact__check_FontSize;
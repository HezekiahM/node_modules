export const c_button_m_small_FontSize: {
  "name": "--pf-c-button--m-small--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-button--m-small--FontSize)"
};
export default c_button_m_small_FontSize;
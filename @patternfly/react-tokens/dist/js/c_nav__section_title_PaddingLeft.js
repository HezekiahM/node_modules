"use strict";
exports.__esModule = true;
exports.c_nav__section_title_PaddingLeft = {
  "name": "--pf-c-nav__section-title--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-nav__section-title--PaddingLeft)"
};
exports["default"] = exports.c_nav__section_title_PaddingLeft;
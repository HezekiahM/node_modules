"use strict";
exports.__esModule = true;
exports.c_page__main_section_PaddingRight = {
  "name": "--pf-c-page__main-section--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-page__main-section--PaddingRight)"
};
exports["default"] = exports.c_page__main_section_PaddingRight;
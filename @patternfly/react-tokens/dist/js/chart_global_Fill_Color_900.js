"use strict";
exports.__esModule = true;
exports.chart_global_Fill_Color_900 = {
  "name": "--pf-chart-global--Fill--Color--900",
  "value": "#151515",
  "var": "var(--pf-chart-global--Fill--Color--900)"
};
exports["default"] = exports.chart_global_Fill_Color_900;
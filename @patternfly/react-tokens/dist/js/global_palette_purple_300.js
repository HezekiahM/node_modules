"use strict";
exports.__esModule = true;
exports.global_palette_purple_300 = {
  "name": "--pf-global--palette--purple-300",
  "value": "#a18fff",
  "var": "var(--pf-global--palette--purple-300)"
};
exports["default"] = exports.global_palette_purple_300;
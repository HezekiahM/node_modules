"use strict";
exports.__esModule = true;
exports.c_breadcrumb__item_divider_Color = {
  "name": "--pf-c-breadcrumb__item-divider--Color",
  "value": "#8a8d90",
  "var": "var(--pf-c-breadcrumb__item-divider--Color)"
};
exports["default"] = exports.c_breadcrumb__item_divider_Color;
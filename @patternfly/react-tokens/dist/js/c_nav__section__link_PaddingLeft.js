"use strict";
exports.__esModule = true;
exports.c_nav__section__link_PaddingLeft = {
  "name": "--pf-c-nav__section__link--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-nav__section__link--PaddingLeft)"
};
exports["default"] = exports.c_nav__section__link_PaddingLeft;
"use strict";
exports.__esModule = true;
exports.c_wizard__toggle_separator_Color = {
  "name": "--pf-c-wizard__toggle-separator--Color",
  "value": "#8a8d90",
  "var": "var(--pf-c-wizard__toggle-separator--Color)"
};
exports["default"] = exports.c_wizard__toggle_separator_Color;
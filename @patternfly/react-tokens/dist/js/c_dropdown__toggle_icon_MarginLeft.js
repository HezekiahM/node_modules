"use strict";
exports.__esModule = true;
exports.c_dropdown__toggle_icon_MarginLeft = {
  "name": "--pf-c-dropdown__toggle-icon--MarginLeft",
  "value": "1rem",
  "var": "var(--pf-c-dropdown__toggle-icon--MarginLeft)"
};
exports["default"] = exports.c_dropdown__toggle_icon_MarginLeft;
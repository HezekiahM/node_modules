export const c_options_menu__toggle_BackgroundColor: {
  "name": "--pf-c-options-menu__toggle--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-options-menu__toggle--BackgroundColor)"
};
export default c_options_menu__toggle_BackgroundColor;
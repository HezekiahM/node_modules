"use strict";
exports.__esModule = true;
exports.chart_voronoi_data_stroke_Width = {
  "name": "--pf-chart-voronoi--data--stroke--Width",
  "value": 0,
  "var": "var(--pf-chart-voronoi--data--stroke--Width)"
};
exports["default"] = exports.chart_voronoi_data_stroke_Width;
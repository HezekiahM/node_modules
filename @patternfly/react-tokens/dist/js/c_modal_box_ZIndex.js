"use strict";
exports.__esModule = true;
exports.c_modal_box_ZIndex = {
  "name": "--pf-c-modal-box--ZIndex",
  "value": "500",
  "var": "var(--pf-c-modal-box--ZIndex)"
};
exports["default"] = exports.c_modal_box_ZIndex;
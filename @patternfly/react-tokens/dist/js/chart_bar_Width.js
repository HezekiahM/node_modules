"use strict";
exports.__esModule = true;
exports.chart_bar_Width = {
  "name": "--pf-chart-bar--Width",
  "value": 10,
  "var": "var(--pf-chart-bar--Width)"
};
exports["default"] = exports.chart_bar_Width;
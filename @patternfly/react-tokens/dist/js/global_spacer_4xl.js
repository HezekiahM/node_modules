"use strict";
exports.__esModule = true;
exports.global_spacer_4xl = {
  "name": "--pf-global--spacer--4xl",
  "value": "5rem",
  "var": "var(--pf-global--spacer--4xl)"
};
exports["default"] = exports.global_spacer_4xl;
export const c_label_m_outline_m_red__content_link_hover_before_BorderColor: {
  "name": "--pf-c-label--m-outline--m-red__content--link--hover--before--BorderColor",
  "value": "#c9190b",
  "var": "var(--pf-c-label--m-outline--m-red__content--link--hover--before--BorderColor)"
};
export default c_label_m_outline_m_red__content_link_hover_before_BorderColor;
"use strict";
exports.__esModule = true;
exports.c_spinner_Width = {
  "name": "--pf-c-spinner--Width",
  "value": "3.375rem",
  "var": "var(--pf-c-spinner--Width)"
};
exports["default"] = exports.c_spinner_Width;
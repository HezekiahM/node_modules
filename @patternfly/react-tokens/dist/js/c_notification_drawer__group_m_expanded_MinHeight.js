"use strict";
exports.__esModule = true;
exports.c_notification_drawer__group_m_expanded_MinHeight = {
  "name": "--pf-c-notification-drawer__group--m-expanded--MinHeight",
  "value": "18.75rem",
  "var": "var(--pf-c-notification-drawer__group--m-expanded--MinHeight)"
};
exports["default"] = exports.c_notification_drawer__group_m_expanded_MinHeight;
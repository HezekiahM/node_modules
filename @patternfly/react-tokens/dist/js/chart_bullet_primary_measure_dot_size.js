"use strict";
exports.__esModule = true;
exports.chart_bullet_primary_measure_dot_size = {
  "name": "--pf-chart-bullet--primary-measure--dot--size",
  "value": 6,
  "var": "var(--pf-chart-bullet--primary-measure--dot--size)"
};
exports["default"] = exports.chart_bullet_primary_measure_dot_size;
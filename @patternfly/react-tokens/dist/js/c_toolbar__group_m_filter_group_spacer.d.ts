export const c_toolbar__group_m_filter_group_spacer: {
  "name": "--pf-c-toolbar__group--m-filter-group--spacer",
  "value": "1rem",
  "var": "var(--pf-c-toolbar__group--m-filter-group--spacer)"
};
export default c_toolbar__group_m_filter_group_spacer;
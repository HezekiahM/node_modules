"use strict";
exports.__esModule = true;
exports.c_app_launcher__toggle_PaddingLeft = {
  "name": "--pf-c-app-launcher__toggle--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-app-launcher__toggle--PaddingLeft)"
};
exports["default"] = exports.c_app_launcher__toggle_PaddingLeft;
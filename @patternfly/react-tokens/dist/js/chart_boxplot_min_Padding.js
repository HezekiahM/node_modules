"use strict";
exports.__esModule = true;
exports.chart_boxplot_min_Padding = {
  "name": "--pf-chart-boxplot--min--Padding",
  "value": 8,
  "var": "var(--pf-chart-boxplot--min--Padding)"
};
exports["default"] = exports.chart_boxplot_min_Padding;
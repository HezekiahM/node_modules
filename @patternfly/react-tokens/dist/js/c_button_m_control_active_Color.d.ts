export const c_button_m_control_active_Color: {
  "name": "--pf-c-button--m-control--active--Color",
  "value": "#151515",
  "var": "var(--pf-c-button--m-control--active--Color)"
};
export default c_button_m_control_active_Color;
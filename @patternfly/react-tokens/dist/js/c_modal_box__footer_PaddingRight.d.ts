export const c_modal_box__footer_PaddingRight: {
  "name": "--pf-c-modal-box__footer--PaddingRight",
  "value": "1.5rem",
  "var": "var(--pf-c-modal-box__footer--PaddingRight)"
};
export default c_modal_box__footer_PaddingRight;
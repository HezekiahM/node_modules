"use strict";
exports.__esModule = true;
exports.global_BorderRadius_lg = {
  "name": "--pf-global--BorderRadius--lg",
  "value": "30em",
  "var": "var(--pf-global--BorderRadius--lg)"
};
exports["default"] = exports.global_BorderRadius_lg;
"use strict";
exports.__esModule = true;
exports.c_table_m_truncate_cell_MaxWidth = {
  "name": "--pf-c-table--m-truncate--cell--MaxWidth",
  "value": "1px",
  "var": "var(--pf-c-table--m-truncate--cell--MaxWidth)"
};
exports["default"] = exports.c_table_m_truncate_cell_MaxWidth;
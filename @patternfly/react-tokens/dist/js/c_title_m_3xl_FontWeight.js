"use strict";
exports.__esModule = true;
exports.c_title_m_3xl_FontWeight = {
  "name": "--pf-c-title--m-3xl--FontWeight",
  "value": "400",
  "var": "var(--pf-c-title--m-3xl--FontWeight)"
};
exports["default"] = exports.c_title_m_3xl_FontWeight;
"use strict";
exports.__esModule = true;
exports.c_notification_drawer__header_status_MarginLeft = {
  "name": "--pf-c-notification-drawer__header-status--MarginLeft",
  "value": "1rem",
  "var": "var(--pf-c-notification-drawer__header-status--MarginLeft)"
};
exports["default"] = exports.c_notification_drawer__header_status_MarginLeft;
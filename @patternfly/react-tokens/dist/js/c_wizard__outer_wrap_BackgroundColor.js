"use strict";
exports.__esModule = true;
exports.c_wizard__outer_wrap_BackgroundColor = {
  "name": "--pf-c-wizard__outer-wrap--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-wizard__outer-wrap--BackgroundColor)"
};
exports["default"] = exports.c_wizard__outer_wrap_BackgroundColor;
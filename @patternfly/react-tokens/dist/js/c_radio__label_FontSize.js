"use strict";
exports.__esModule = true;
exports.c_radio__label_FontSize = {
  "name": "--pf-c-radio__label--FontSize",
  "value": "1rem",
  "var": "var(--pf-c-radio__label--FontSize)"
};
exports["default"] = exports.c_radio__label_FontSize;
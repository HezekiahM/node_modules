"use strict";
exports.__esModule = true;
exports.c_tabs__scroll_button_OutlineOffset = {
  "name": "--pf-c-tabs__scroll-button--OutlineOffset",
  "value": "calc(-1 * 0.25rem)",
  "var": "var(--pf-c-tabs__scroll-button--OutlineOffset)"
};
exports["default"] = exports.c_tabs__scroll_button_OutlineOffset;
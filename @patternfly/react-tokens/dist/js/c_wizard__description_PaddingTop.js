"use strict";
exports.__esModule = true;
exports.c_wizard__description_PaddingTop = {
  "name": "--pf-c-wizard__description--PaddingTop",
  "value": "0.5rem",
  "var": "var(--pf-c-wizard__description--PaddingTop)"
};
exports["default"] = exports.c_wizard__description_PaddingTop;
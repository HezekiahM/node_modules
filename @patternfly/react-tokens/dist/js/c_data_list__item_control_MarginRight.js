"use strict";
exports.__esModule = true;
exports.c_data_list__item_control_MarginRight = {
  "name": "--pf-c-data-list__item-control--MarginRight",
  "value": "1rem",
  "var": "var(--pf-c-data-list__item-control--MarginRight)"
};
exports["default"] = exports.c_data_list__item_control_MarginRight;
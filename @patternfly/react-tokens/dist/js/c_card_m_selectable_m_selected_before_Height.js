"use strict";
exports.__esModule = true;
exports.c_card_m_selectable_m_selected_before_Height = {
  "name": "--pf-c-card--m-selectable--m-selected--before--Height",
  "value": "3px",
  "var": "var(--pf-c-card--m-selectable--m-selected--before--Height)"
};
exports["default"] = exports.c_card_m_selectable_m_selected_before_Height;
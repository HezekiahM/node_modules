export const c_button_m_display_lg_PaddingRight: {
  "name": "--pf-c-button--m-display-lg--PaddingRight",
  "value": "2rem",
  "var": "var(--pf-c-button--m-display-lg--PaddingRight)"
};
export default c_button_m_display_lg_PaddingRight;
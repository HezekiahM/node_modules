"use strict";
exports.__esModule = true;
exports.l_split_m_gutter_MarginRight = {
  "name": "--pf-l-split--m-gutter--MarginRight",
  "value": "1rem",
  "var": "var(--pf-l-split--m-gutter--MarginRight)"
};
exports["default"] = exports.l_split_m_gutter_MarginRight;
"use strict";
exports.__esModule = true;
exports.c_context_selector__toggle_BorderTopColor = {
  "name": "--pf-c-context-selector__toggle--BorderTopColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-context-selector__toggle--BorderTopColor)"
};
exports["default"] = exports.c_context_selector__toggle_BorderTopColor;
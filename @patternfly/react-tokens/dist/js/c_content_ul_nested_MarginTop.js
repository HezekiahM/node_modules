"use strict";
exports.__esModule = true;
exports.c_content_ul_nested_MarginTop = {
  "name": "--pf-c-content--ul--nested--MarginTop",
  "value": "0.5rem",
  "var": "var(--pf-c-content--ul--nested--MarginTop)"
};
exports["default"] = exports.c_content_ul_nested_MarginTop;
"use strict";
exports.__esModule = true;
exports.c_wizard__header_lg_PaddingLeft = {
  "name": "--pf-c-wizard__header--lg--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-wizard__header--lg--PaddingLeft)"
};
exports["default"] = exports.c_wizard__header_lg_PaddingLeft;
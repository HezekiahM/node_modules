"use strict";
exports.__esModule = true;
exports.c_accordion__expanded_content_body_PaddingBottom = {
  "name": "--pf-c-accordion__expanded-content-body--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-accordion__expanded-content-body--PaddingBottom)"
};
exports["default"] = exports.c_accordion__expanded_content_body_PaddingBottom;
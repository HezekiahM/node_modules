"use strict";
exports.__esModule = true;
exports.c_form_control_m_warning_PaddingBottom = {
  "name": "--pf-c-form-control--m-warning--PaddingBottom",
  "value": "calc(0.375rem - 2px)",
  "var": "var(--pf-c-form-control--m-warning--PaddingBottom)"
};
exports["default"] = exports.c_form_control_m_warning_PaddingBottom;
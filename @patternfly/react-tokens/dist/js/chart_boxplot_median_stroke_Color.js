"use strict";
exports.__esModule = true;
exports.chart_boxplot_median_stroke_Color = {
  "name": "--pf-chart-boxplot--median--stroke--Color",
  "value": "#151515",
  "var": "var(--pf-chart-boxplot--median--stroke--Color)"
};
exports["default"] = exports.chart_boxplot_median_stroke_Color;
"use strict";
exports.__esModule = true;
exports.c_nav__subnav_c_divider_PaddingRight = {
  "name": "--pf-c-nav__subnav--c-divider--PaddingRight",
  "value": "1.5rem",
  "var": "var(--pf-c-nav__subnav--c-divider--PaddingRight)"
};
exports["default"] = exports.c_nav__subnav_c_divider_PaddingRight;
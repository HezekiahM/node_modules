"use strict";
exports.__esModule = true;
exports.c_select__menu_group_title_PaddingRight = {
  "name": "--pf-c-select__menu-group-title--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-select__menu-group-title--PaddingRight)"
};
exports["default"] = exports.c_select__menu_group_title_PaddingRight;
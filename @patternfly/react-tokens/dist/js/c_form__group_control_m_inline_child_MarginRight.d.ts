export const c_form__group_control_m_inline_child_MarginRight: {
  "name": "--pf-c-form__group-control--m-inline--child--MarginRight",
  "value": "1.5rem",
  "var": "var(--pf-c-form__group-control--m-inline--child--MarginRight)"
};
export default c_form__group_control_m_inline_child_MarginRight;
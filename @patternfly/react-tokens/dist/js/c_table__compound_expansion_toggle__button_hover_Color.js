"use strict";
exports.__esModule = true;
exports.c_table__compound_expansion_toggle__button_hover_Color = {
  "name": "--pf-c-table__compound-expansion-toggle__button--hover--Color",
  "value": "#004080",
  "var": "var(--pf-c-table__compound-expansion-toggle__button--hover--Color)"
};
exports["default"] = exports.c_table__compound_expansion_toggle__button_hover_Color;
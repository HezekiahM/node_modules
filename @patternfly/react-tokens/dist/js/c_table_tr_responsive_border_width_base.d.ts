export const c_table_tr_responsive_border_width_base: {
  "name": "--pf-c-table-tr--responsive--border-width--base",
  "value": "0.5rem",
  "var": "var(--pf-c-table-tr--responsive--border-width--base)"
};
export default c_table_tr_responsive_border_width_base;
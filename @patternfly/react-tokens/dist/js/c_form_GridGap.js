"use strict";
exports.__esModule = true;
exports.c_form_GridGap = {
  "name": "--pf-c-form--GridGap",
  "value": "1.5rem",
  "var": "var(--pf-c-form--GridGap)"
};
exports["default"] = exports.c_form_GridGap;
"use strict";
exports.__esModule = true;
exports.c_label_PaddingTop = {
  "name": "--pf-c-label--PaddingTop",
  "value": "0.25rem",
  "var": "var(--pf-c-label--PaddingTop)"
};
exports["default"] = exports.c_label_PaddingTop;
"use strict";
exports.__esModule = true;
exports.global_palette_cyan_500 = {
  "name": "--pf-global--palette--cyan-500",
  "value": "#003737",
  "var": "var(--pf-global--palette--cyan-500)"
};
exports["default"] = exports.global_palette_cyan_500;
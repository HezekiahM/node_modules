export const c_form_control__select_m_warning_BackgroundPosition: {
  "name": "--pf-c-form-control__select--m-warning--BackgroundPosition",
  "value": "calc(100% - 0.5rem - 1.5rem + 0.0625rem)",
  "var": "var(--pf-c-form-control__select--m-warning--BackgroundPosition)"
};
export default c_form_control__select_m_warning_BackgroundPosition;
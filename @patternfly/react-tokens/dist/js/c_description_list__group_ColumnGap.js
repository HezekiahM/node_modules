"use strict";
exports.__esModule = true;
exports.c_description_list__group_ColumnGap = {
  "name": "--pf-c-description-list__group--ColumnGap",
  "value": "1rem",
  "var": "var(--pf-c-description-list__group--ColumnGap)"
};
exports["default"] = exports.c_description_list__group_ColumnGap;
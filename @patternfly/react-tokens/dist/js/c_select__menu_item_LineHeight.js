"use strict";
exports.__esModule = true;
exports.c_select__menu_item_LineHeight = {
  "name": "--pf-c-select__menu-item--LineHeight",
  "value": "1.5",
  "var": "var(--pf-c-select__menu-item--LineHeight)"
};
exports["default"] = exports.c_select__menu_item_LineHeight;
"use strict";
exports.__esModule = true;
exports.c_select__menu_item_m_selected_PaddingRight = {
  "name": "--pf-c-select__menu-item--m-selected--PaddingRight",
  "value": "3rem",
  "var": "var(--pf-c-select__menu-item--m-selected--PaddingRight)"
};
exports["default"] = exports.c_select__menu_item_m_selected_PaddingRight;
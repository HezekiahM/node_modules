"use strict";
exports.__esModule = true;
exports.c_drawer_child_m_padding_PaddingBottom = {
  "name": "--pf-c-drawer--child--m-padding--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-drawer--child--m-padding--PaddingBottom)"
};
exports["default"] = exports.c_drawer_child_m_padding_PaddingBottom;
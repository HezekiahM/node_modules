"use strict";
exports.__esModule = true;
exports.chart_scatter_data_stroke_Width = {
  "name": "--pf-chart-scatter--data--stroke--Width",
  "value": 0,
  "var": "var(--pf-chart-scatter--data--stroke--Width)"
};
exports["default"] = exports.chart_scatter_data_stroke_Width;
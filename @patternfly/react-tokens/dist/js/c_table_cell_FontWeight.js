"use strict";
exports.__esModule = true;
exports.c_table_cell_FontWeight = {
  "name": "--pf-c-table--cell--FontWeight",
  "value": "700",
  "var": "var(--pf-c-table--cell--FontWeight)"
};
exports["default"] = exports.c_table_cell_FontWeight;
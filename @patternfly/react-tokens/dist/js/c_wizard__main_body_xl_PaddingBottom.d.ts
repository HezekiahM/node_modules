export const c_wizard__main_body_xl_PaddingBottom: {
  "name": "--pf-c-wizard__main-body--xl--PaddingBottom",
  "value": "1.5rem",
  "var": "var(--pf-c-wizard__main-body--xl--PaddingBottom)"
};
export default c_wizard__main_body_xl_PaddingBottom;
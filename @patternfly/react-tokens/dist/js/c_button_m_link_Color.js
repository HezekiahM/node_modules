"use strict";
exports.__esModule = true;
exports.c_button_m_link_Color = {
  "name": "--pf-c-button--m-link--Color",
  "value": "#004080",
  "var": "var(--pf-c-button--m-link--Color)"
};
exports["default"] = exports.c_button_m_link_Color;
export const c_hint_BorderColor: {
  "name": "--pf-c-hint--BorderColor",
  "value": "#bee1f4",
  "var": "var(--pf-c-hint--BorderColor)"
};
export default c_hint_BorderColor;
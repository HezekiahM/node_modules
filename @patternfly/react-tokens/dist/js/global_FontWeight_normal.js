"use strict";
exports.__esModule = true;
exports.global_FontWeight_normal = {
  "name": "--pf-global--FontWeight--normal",
  "value": "400",
  "var": "var(--pf-global--FontWeight--normal)"
};
exports["default"] = exports.global_FontWeight_normal;
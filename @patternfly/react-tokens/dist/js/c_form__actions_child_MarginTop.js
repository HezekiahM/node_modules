"use strict";
exports.__esModule = true;
exports.c_form__actions_child_MarginTop = {
  "name": "--pf-c-form__actions--child--MarginTop",
  "value": "0.5rem",
  "var": "var(--pf-c-form__actions--child--MarginTop)"
};
exports["default"] = exports.c_form__actions_child_MarginTop;
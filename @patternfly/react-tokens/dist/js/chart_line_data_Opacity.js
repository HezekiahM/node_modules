"use strict";
exports.__esModule = true;
exports.chart_line_data_Opacity = {
  "name": "--pf-chart-line--data--Opacity",
  "value": 1,
  "var": "var(--pf-chart-line--data--Opacity)"
};
exports["default"] = exports.chart_line_data_Opacity;
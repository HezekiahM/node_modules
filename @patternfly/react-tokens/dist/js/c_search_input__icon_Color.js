"use strict";
exports.__esModule = true;
exports.c_search_input__icon_Color = {
  "name": "--pf-c-search-input__icon--Color",
  "value": "#151515",
  "var": "var(--pf-c-search-input__icon--Color)"
};
exports["default"] = exports.c_search_input__icon_Color;
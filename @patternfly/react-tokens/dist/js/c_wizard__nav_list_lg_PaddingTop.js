"use strict";
exports.__esModule = true;
exports.c_wizard__nav_list_lg_PaddingTop = {
  "name": "--pf-c-wizard__nav-list--lg--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-wizard__nav-list--lg--PaddingTop)"
};
exports["default"] = exports.c_wizard__nav_list_lg_PaddingTop;
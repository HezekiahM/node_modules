export const c_nav_m_horizontal__link_lg_PaddingBottom: {
  "name": "--pf-c-nav--m-horizontal__link--lg--PaddingBottom",
  "value": "1.5rem",
  "var": "var(--pf-c-nav--m-horizontal__link--lg--PaddingBottom)"
};
export default c_nav_m_horizontal__link_lg_PaddingBottom;
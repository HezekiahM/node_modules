export const c_expandable_section_m_expanded__toggle_icon_Rotate: {
  "name": "--pf-c-expandable-section--m-expanded__toggle-icon--Rotate",
  "value": "90deg",
  "var": "var(--pf-c-expandable-section--m-expanded__toggle-icon--Rotate)"
};
export default c_expandable_section_m_expanded__toggle_icon_Rotate;
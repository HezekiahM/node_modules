"use strict";
exports.__esModule = true;
exports.c_tooltip = {
  ".pf-c-tooltip": {
    "c_tooltip_MaxWidth": {
      "name": "--pf-c-tooltip--MaxWidth",
      "value": "18.75rem"
    },
    "c_tooltip_BoxShadow": {
      "name": "--pf-c-tooltip--BoxShadow",
      "value": "0 0.25rem 0.5rem 0rem rgba(3, 3, 3, 0.12), 0 0 0.25rem 0 rgba(3, 3, 3, 0.06)",
      "values": [
        "--pf-global--BoxShadow--md",
        "$pf-global--BoxShadow--md",
        "0 pf-size-prem(4px) pf-size-prem(8px) pf-size-prem(0) rgba($pf-color-black-1000, .12), 0 0 pf-size-prem(4px) 0 rgba($pf-color-black-1000, .06)",
        "0 pf-size-prem(4px) pf-size-prem(8px) pf-size-prem(0) rgba(#030303, .12), 0 0 pf-size-prem(4px) 0 rgba(#030303, .06)",
        "0 0.25rem 0.5rem 0rem rgba(3, 3, 3, 0.12), 0 0 0.25rem 0 rgba(3, 3, 3, 0.06)"
      ]
    },
    "c_tooltip__content_PaddingTop": {
      "name": "--pf-c-tooltip__content--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_tooltip__content_PaddingRight": {
      "name": "--pf-c-tooltip__content--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_tooltip__content_PaddingBottom": {
      "name": "--pf-c-tooltip__content--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_tooltip__content_PaddingLeft": {
      "name": "--pf-c-tooltip__content--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_tooltip__content_Color": {
      "name": "--pf-c-tooltip__content--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_tooltip__content_BackgroundColor": {
      "name": "--pf-c-tooltip__content--BackgroundColor",
      "value": "#151515",
      "values": [
        "--pf-global--BackgroundColor--dark-100",
        "$pf-global--BackgroundColor--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_tooltip__content_FontSize": {
      "name": "--pf-c-tooltip__content--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_tooltip__arrow_Width": {
      "name": "--pf-c-tooltip__arrow--Width",
      "value": "0.9375rem",
      "values": [
        "--pf-global--arrow--width",
        "$pf-global--arrow--width",
        "pf-font-prem(15px)",
        "0.9375rem"
      ]
    },
    "c_tooltip__arrow_Height": {
      "name": "--pf-c-tooltip__arrow--Height",
      "value": "0.9375rem",
      "values": [
        "--pf-global--arrow--width",
        "$pf-global--arrow--width",
        "pf-font-prem(15px)",
        "0.9375rem"
      ]
    },
    "c_tooltip__arrow_m_top_TranslateX": {
      "name": "--pf-c-tooltip__arrow--m-top--TranslateX",
      "value": "-50%"
    },
    "c_tooltip__arrow_m_top_TranslateY": {
      "name": "--pf-c-tooltip__arrow--m-top--TranslateY",
      "value": "50%"
    },
    "c_tooltip__arrow_m_top_Rotate": {
      "name": "--pf-c-tooltip__arrow--m-top--Rotate",
      "value": "45deg"
    },
    "c_tooltip__arrow_m_right_TranslateX": {
      "name": "--pf-c-tooltip__arrow--m-right--TranslateX",
      "value": "-50%"
    },
    "c_tooltip__arrow_m_right_TranslateY": {
      "name": "--pf-c-tooltip__arrow--m-right--TranslateY",
      "value": "-50%"
    },
    "c_tooltip__arrow_m_right_Rotate": {
      "name": "--pf-c-tooltip__arrow--m-right--Rotate",
      "value": "45deg"
    },
    "c_tooltip__arrow_m_bottom_TranslateX": {
      "name": "--pf-c-tooltip__arrow--m-bottom--TranslateX",
      "value": "-50%"
    },
    "c_tooltip__arrow_m_bottom_TranslateY": {
      "name": "--pf-c-tooltip__arrow--m-bottom--TranslateY",
      "value": "-50%"
    },
    "c_tooltip__arrow_m_bottom_Rotate": {
      "name": "--pf-c-tooltip__arrow--m-bottom--Rotate",
      "value": "45deg"
    },
    "c_tooltip__arrow_m_left_TranslateX": {
      "name": "--pf-c-tooltip__arrow--m-left--TranslateX",
      "value": "50%"
    },
    "c_tooltip__arrow_m_left_TranslateY": {
      "name": "--pf-c-tooltip__arrow--m-left--TranslateY",
      "value": "-50%"
    },
    "c_tooltip__arrow_m_left_Rotate": {
      "name": "--pf-c-tooltip__arrow--m-left--Rotate",
      "value": "45deg"
    }
  }
};
exports["default"] = exports.c_tooltip;
export const c_tabs__link_before_BorderBottomColor: {
  "name": "--pf-c-tabs__link--before--BorderBottomColor",
  "value": "#d2d2d2",
  "var": "var(--pf-c-tabs__link--before--BorderBottomColor)"
};
export default c_tabs__link_before_BorderBottomColor;
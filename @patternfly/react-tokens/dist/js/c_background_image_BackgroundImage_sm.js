"use strict";
exports.__esModule = true;
exports.c_background_image_BackgroundImage_sm = {
  "name": "--pf-c-background-image--BackgroundImage--sm",
  "value": "url(\"../../assets/images/pfbg_768.jpg\")",
  "var": "var(--pf-c-background-image--BackgroundImage--sm)"
};
exports["default"] = exports.c_background_image_BackgroundImage_sm;
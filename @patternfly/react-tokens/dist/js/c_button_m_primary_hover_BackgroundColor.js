"use strict";
exports.__esModule = true;
exports.c_button_m_primary_hover_BackgroundColor = {
  "name": "--pf-c-button--m-primary--hover--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-button--m-primary--hover--BackgroundColor)"
};
exports["default"] = exports.c_button_m_primary_hover_BackgroundColor;
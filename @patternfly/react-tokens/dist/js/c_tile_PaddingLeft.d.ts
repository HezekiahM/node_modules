export const c_tile_PaddingLeft: {
  "name": "--pf-c-tile--PaddingLeft",
  "value": "1.5rem",
  "var": "var(--pf-c-tile--PaddingLeft)"
};
export default c_tile_PaddingLeft;
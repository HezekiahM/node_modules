"use strict";
exports.__esModule = true;
exports.chart_color_red_500 = {
  "name": "--pf-chart-color-red-500",
  "value": "#2c0000",
  "var": "var(--pf-chart-color-red-500)"
};
exports["default"] = exports.chart_color_red_500;
export const global_BorderWidth_xl: {
  "name": "--pf-global--BorderWidth--xl",
  "value": "4px",
  "var": "var(--pf-global--BorderWidth--xl)"
};
export default global_BorderWidth_xl;
"use strict";
exports.__esModule = true;
exports.c_card_child_PaddingBottom = {
  "name": "--pf-c-card--child--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-card--child--PaddingBottom)"
};
exports["default"] = exports.c_card_child_PaddingBottom;
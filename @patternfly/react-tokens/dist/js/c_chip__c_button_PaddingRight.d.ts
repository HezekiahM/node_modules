export const c_chip__c_button_PaddingRight: {
  "name": "--pf-c-chip__c-button--PaddingRight",
  "value": "0.5rem",
  "var": "var(--pf-c-chip__c-button--PaddingRight)"
};
export default c_chip__c_button_PaddingRight;
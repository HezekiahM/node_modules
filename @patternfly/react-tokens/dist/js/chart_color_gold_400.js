"use strict";
exports.__esModule = true;
exports.chart_color_gold_400 = {
  "name": "--pf-chart-color-gold-400",
  "value": "#f0ab00",
  "var": "var(--pf-chart-color-gold-400)"
};
exports["default"] = exports.chart_color_gold_400;
export const c_table__column_help_c_button_PaddingLeft: {
  "name": "--pf-c-table__column-help--c-button--PaddingLeft",
  "value": "0.5rem",
  "var": "var(--pf-c-table__column-help--c-button--PaddingLeft)"
};
export default c_table__column_help_c_button_PaddingLeft;
"use strict";
exports.__esModule = true;
exports.global_palette_black_400 = {
  "name": "--pf-global--palette--black-400",
  "value": "#b8bbbe",
  "var": "var(--pf-global--palette--black-400)"
};
exports["default"] = exports.global_palette_black_400;
"use strict";
exports.__esModule = true;
exports.c_empty_state__secondary_MarginTop = {
  "name": "--pf-c-empty-state__secondary--MarginTop",
  "value": "2rem",
  "var": "var(--pf-c-empty-state__secondary--MarginTop)"
};
exports["default"] = exports.c_empty_state__secondary_MarginTop;
"use strict";
exports.__esModule = true;
exports.c_toolbar_spacer_base = {
  "name": "--pf-c-toolbar--spacer--base",
  "value": "1rem",
  "var": "var(--pf-c-toolbar--spacer--base)"
};
exports["default"] = exports.c_toolbar_spacer_base;
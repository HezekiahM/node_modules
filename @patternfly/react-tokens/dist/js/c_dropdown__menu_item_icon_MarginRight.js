"use strict";
exports.__esModule = true;
exports.c_dropdown__menu_item_icon_MarginRight = {
  "name": "--pf-c-dropdown__menu-item-icon--MarginRight",
  "value": "0.5rem",
  "var": "var(--pf-c-dropdown__menu-item-icon--MarginRight)"
};
exports["default"] = exports.c_dropdown__menu_item_icon_MarginRight;
"use strict";
exports.__esModule = true;
exports.chart_axis_tick_Width = {
  "name": "--pf-chart-axis--tick--Width",
  "value": 1,
  "var": "var(--pf-chart-axis--tick--Width)"
};
exports["default"] = exports.chart_axis_tick_Width;
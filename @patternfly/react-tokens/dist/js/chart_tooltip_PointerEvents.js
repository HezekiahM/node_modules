"use strict";
exports.__esModule = true;
exports.chart_tooltip_PointerEvents = {
  "name": "--pf-chart-tooltip--PointerEvents",
  "value": "none",
  "var": "var(--pf-chart-tooltip--PointerEvents)"
};
exports["default"] = exports.chart_tooltip_PointerEvents;
"use strict";
exports.__esModule = true;
exports.c_form__helper_text_m_error_Color = {
  "name": "--pf-c-form__helper-text--m-error--Color",
  "value": "#c9190b",
  "var": "var(--pf-c-form__helper-text--m-error--Color)"
};
exports["default"] = exports.c_form__helper_text_m_error_Color;
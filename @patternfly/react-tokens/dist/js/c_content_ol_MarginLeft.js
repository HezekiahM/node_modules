"use strict";
exports.__esModule = true;
exports.c_content_ol_MarginLeft = {
  "name": "--pf-c-content--ol--MarginLeft",
  "value": "0.5rem",
  "var": "var(--pf-c-content--ol--MarginLeft)"
};
exports["default"] = exports.c_content_ol_MarginLeft;
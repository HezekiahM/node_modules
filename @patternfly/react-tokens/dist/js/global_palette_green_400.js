"use strict";
exports.__esModule = true;
exports.global_palette_green_400 = {
  "name": "--pf-global--palette--green-400",
  "value": "#5ba352",
  "var": "var(--pf-global--palette--green-400)"
};
exports["default"] = exports.global_palette_green_400;
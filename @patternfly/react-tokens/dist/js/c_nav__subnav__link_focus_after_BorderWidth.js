"use strict";
exports.__esModule = true;
exports.c_nav__subnav__link_focus_after_BorderWidth = {
  "name": "--pf-c-nav__subnav__link--focus--after--BorderWidth",
  "value": "1px",
  "var": "var(--pf-c-nav__subnav__link--focus--after--BorderWidth)"
};
exports["default"] = exports.c_nav__subnav__link_focus_after_BorderWidth;
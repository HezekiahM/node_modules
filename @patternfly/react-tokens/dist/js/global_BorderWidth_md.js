"use strict";
exports.__esModule = true;
exports.global_BorderWidth_md = {
  "name": "--pf-global--BorderWidth--md",
  "value": "2px",
  "var": "var(--pf-global--BorderWidth--md)"
};
exports["default"] = exports.global_BorderWidth_md;
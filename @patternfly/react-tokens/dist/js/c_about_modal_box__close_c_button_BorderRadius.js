"use strict";
exports.__esModule = true;
exports.c_about_modal_box__close_c_button_BorderRadius = {
  "name": "--pf-c-about-modal-box__close--c-button--BorderRadius",
  "value": "30em",
  "var": "var(--pf-c-about-modal-box__close--c-button--BorderRadius)"
};
exports["default"] = exports.c_about_modal_box__close_c_button_BorderRadius;
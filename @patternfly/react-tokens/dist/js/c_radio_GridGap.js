"use strict";
exports.__esModule = true;
exports.c_radio_GridGap = {
  "name": "--pf-c-radio--GridGap",
  "value": "0.25rem 0.5rem",
  "var": "var(--pf-c-radio--GridGap)"
};
exports["default"] = exports.c_radio_GridGap;
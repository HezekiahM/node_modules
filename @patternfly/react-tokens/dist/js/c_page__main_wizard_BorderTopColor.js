"use strict";
exports.__esModule = true;
exports.c_page__main_wizard_BorderTopColor = {
  "name": "--pf-c-page__main-wizard--BorderTopColor",
  "value": "#d2d2d2",
  "var": "var(--pf-c-page__main-wizard--BorderTopColor)"
};
exports["default"] = exports.c_page__main_wizard_BorderTopColor;
"use strict";
exports.__esModule = true;
exports.c_page__header_BackgroundColor = {
  "name": "--pf-c-page__header--BackgroundColor",
  "value": "#151515",
  "var": "var(--pf-c-page__header--BackgroundColor)"
};
exports["default"] = exports.c_page__header_BackgroundColor;
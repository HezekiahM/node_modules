"use strict";
exports.__esModule = true;
exports.c_spinner__clipper_Width = {
  "name": "--pf-c-spinner__clipper--Width",
  "value": "3.375rem",
  "var": "var(--pf-c-spinner__clipper--Width)"
};
exports["default"] = exports.c_spinner__clipper_Width;
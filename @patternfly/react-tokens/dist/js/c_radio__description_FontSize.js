"use strict";
exports.__esModule = true;
exports.c_radio__description_FontSize = {
  "name": "--pf-c-radio__description--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-radio__description--FontSize)"
};
exports["default"] = exports.c_radio__description_FontSize;
"use strict";
exports.__esModule = true;
exports.global_palette_red_200 = {
  "name": "--pf-global--palette--red-200",
  "value": "#a30000",
  "var": "var(--pf-global--palette--red-200)"
};
exports["default"] = exports.global_palette_red_200;
export const c_app_launcher: {
  ".pf-c-app-launcher": {
    "c_app_launcher__menu_BackgroundColor": {
      "name": "--pf-c-app-launcher__menu--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--light-100",
        "$pf-global--BackgroundColor--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_app_launcher__menu_BoxShadow": {
      "name": "--pf-c-app-launcher__menu--BoxShadow",
      "value": "0 0.25rem 0.5rem 0rem rgba(3, 3, 3, 0.12), 0 0 0.25rem 0 rgba(3, 3, 3, 0.06)",
      "values": [
        "--pf-global--BoxShadow--md",
        "$pf-global--BoxShadow--md",
        "0 pf-size-prem(4px) pf-size-prem(8px) pf-size-prem(0) rgba($pf-color-black-1000, .12), 0 0 pf-size-prem(4px) 0 rgba($pf-color-black-1000, .06)",
        "0 pf-size-prem(4px) pf-size-prem(8px) pf-size-prem(0) rgba(#030303, .12), 0 0 pf-size-prem(4px) 0 rgba(#030303, .06)",
        "0 0.25rem 0.5rem 0rem rgba(3, 3, 3, 0.12), 0 0 0.25rem 0 rgba(3, 3, 3, 0.06)"
      ]
    },
    "c_app_launcher__menu_PaddingTop": {
      "name": "--pf-c-app-launcher__menu--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_app_launcher__menu_PaddingBottom": {
      "name": "--pf-c-app-launcher__menu--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_app_launcher__menu_Top": {
      "name": "--pf-c-app-launcher__menu--Top",
      "value": "calc(100% + 0.25rem)",
      "values": [
        "calc(100% + --pf-global--spacer--xs)",
        "calc(100% + $pf-global--spacer--xs)",
        "calc(100% + pf-size-prem(4px))",
        "calc(100% + 0.25rem)"
      ]
    },
    "c_app_launcher__menu_ZIndex": {
      "name": "--pf-c-app-launcher__menu--ZIndex",
      "value": "200",
      "values": [
        "--pf-global--ZIndex--sm",
        "$pf-global--ZIndex--sm",
        "200"
      ]
    },
    "c_app_launcher_m_top__menu_Top": {
      "name": "--pf-c-app-launcher--m-top__menu--Top",
      "value": "0"
    },
    "c_app_launcher_m_top__menu_TranslateY": {
      "name": "--pf-c-app-launcher--m-top__menu--TranslateY",
      "value": "calc(-100% - 0.25rem)",
      "values": [
        "calc(-100% - --pf-global--spacer--xs)",
        "calc(-100% - $pf-global--spacer--xs)",
        "calc(-100% - pf-size-prem(4px))",
        "calc(-100% - 0.25rem)"
      ]
    },
    "c_app_launcher__toggle_PaddingTop": {
      "name": "--pf-c-app-launcher__toggle--PaddingTop",
      "value": "0.375rem",
      "values": [
        "--pf-global--spacer--form-element",
        "$pf-global--spacer--form-element",
        "pf-size-prem(6px)",
        "0.375rem"
      ]
    },
    "c_app_launcher__toggle_PaddingRight": {
      "name": "--pf-c-app-launcher__toggle--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_app_launcher__toggle_PaddingBottom": {
      "name": "--pf-c-app-launcher__toggle--PaddingBottom",
      "value": "0.375rem",
      "values": [
        "--pf-global--spacer--form-element",
        "$pf-global--spacer--form-element",
        "pf-size-prem(6px)",
        "0.375rem"
      ]
    },
    "c_app_launcher__toggle_PaddingLeft": {
      "name": "--pf-c-app-launcher__toggle--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_app_launcher__toggle_Color": {
      "name": "--pf-c-app-launcher__toggle--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--200",
        "$pf-global--Color--200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_app_launcher__toggle_hover_Color": {
      "name": "--pf-c-app-launcher__toggle--hover--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_app_launcher__toggle_active_Color": {
      "name": "--pf-c-app-launcher__toggle--active--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_app_launcher__toggle_focus_Color": {
      "name": "--pf-c-app-launcher__toggle--focus--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_app_launcher__toggle_disabled_Color": {
      "name": "--pf-c-app-launcher__toggle--disabled--Color",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--disabled-color--200",
        "$pf-global--disabled-color--200",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_app_launcher__toggle_m_expanded_Color": {
      "name": "--pf-c-app-launcher__toggle--m-expanded--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_app_launcher__menu_search_PaddingTop": {
      "name": "--pf-c-app-launcher__menu-search--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_app_launcher__menu_search_PaddingRight": {
      "name": "--pf-c-app-launcher__menu-search--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_app_launcher__menu_search_PaddingBottom": {
      "name": "--pf-c-app-launcher__menu-search--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_app_launcher__menu_search_PaddingLeft": {
      "name": "--pf-c-app-launcher__menu-search--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_app_launcher__menu_search_BottomBorderColor": {
      "name": "--pf-c-app-launcher__menu-search--BottomBorderColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_app_launcher__menu_search_BottomBorderWidth": {
      "name": "--pf-c-app-launcher__menu-search--BottomBorderWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_app_launcher__menu_search_MarginBottom": {
      "name": "--pf-c-app-launcher__menu-search--MarginBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_app_launcher__menu_item_PaddingTop": {
      "name": "--pf-c-app-launcher__menu-item--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_app_launcher__menu_item_PaddingRight": {
      "name": "--pf-c-app-launcher__menu-item--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_app_launcher__menu_item_PaddingBottom": {
      "name": "--pf-c-app-launcher__menu-item--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_app_launcher__menu_item_PaddingLeft": {
      "name": "--pf-c-app-launcher__menu-item--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_app_launcher__menu_item_Color": {
      "name": "--pf-c-app-launcher__menu-item--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_app_launcher__menu_item_FontWeight": {
      "name": "--pf-c-app-launcher__menu-item--FontWeight",
      "value": "400",
      "values": [
        "--pf-global--FontWeight--normal",
        "$pf-global--FontWeight--normal",
        "400"
      ]
    },
    "c_app_launcher__menu_item_Width": {
      "name": "--pf-c-app-launcher__menu-item--Width",
      "value": "100%"
    },
    "c_app_launcher__menu_item_disabled_Color": {
      "name": "--pf-c-app-launcher__menu-item--disabled--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--dark-200",
        "$pf-global--Color--dark-200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_app_launcher__menu_item_hover_BackgroundColor": {
      "name": "--pf-c-app-launcher__menu-item--hover--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_app_launcher__menu_item_m_link_PaddingRight": {
      "name": "--pf-c-app-launcher__menu-item--m-link--PaddingRight",
      "value": "0"
    },
    "c_app_launcher__menu_item_m_link_hover_BackgroundColor": {
      "name": "--pf-c-app-launcher__menu-item--m-link--hover--BackgroundColor",
      "value": "transparent"
    },
    "c_app_launcher__menu_item_m_action_hover_BackgroundColor": {
      "name": "--pf-c-app-launcher__menu-item--m-action--hover--BackgroundColor",
      "value": "transparent"
    },
    "c_app_launcher__menu_item_m_action_Color": {
      "name": "--pf-c-app-launcher__menu-item--m-action--Color",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--disabled-color--200",
        "$pf-global--disabled-color--200",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_app_launcher__menu_item_m_action_Width": {
      "name": "--pf-c-app-launcher__menu-item--m-action--Width",
      "value": "auto"
    },
    "c_app_launcher__menu_item_m_action_FontSize": {
      "name": "--pf-c-app-launcher__menu-item--m-action--FontSize",
      "value": "0.625rem",
      "values": [
        "--pf-global--icon--FontSize--sm",
        "$pf-global--icon--FontSize--sm",
        "pf-font-prem(10px)",
        "0.625rem"
      ]
    },
    "c_app_launcher__menu_item_hover__menu_item_m_action_Color": {
      "name": "--pf-c-app-launcher__menu-item--hover__menu-item--m-action--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--200",
        "$pf-global--Color--200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_app_launcher__menu_item_m_action_hover_Color": {
      "name": "--pf-c-app-launcher__menu-item--m-action--hover--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_app_launcher__menu_item_m_favorite__menu_item_m_action_Color": {
      "name": "--pf-c-app-launcher__menu-item--m-favorite__menu-item--m-action--Color",
      "value": "#f0ab00",
      "values": [
        "--pf-global--palette--gold-400",
        "$pf-color-gold-400",
        "#f0ab00"
      ]
    },
    "c_app_launcher__menu_item_icon_MarginRight": {
      "name": "--pf-c-app-launcher__menu-item-icon--MarginRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_app_launcher__menu_item_icon_Width": {
      "name": "--pf-c-app-launcher__menu-item-icon--Width",
      "value": "1.5rem",
      "values": [
        "--pf-global--icon--FontSize--lg",
        "$pf-global--icon--FontSize--lg",
        "pf-font-prem(24px)",
        "1.5rem"
      ]
    },
    "c_app_launcher__menu_item_icon_Height": {
      "name": "--pf-c-app-launcher__menu-item-icon--Height",
      "value": "1.5rem",
      "values": [
        "--pf-global--icon--FontSize--lg",
        "$pf-global--icon--FontSize--lg",
        "pf-font-prem(24px)",
        "1.5rem"
      ]
    },
    "c_app_launcher__menu_item_external_icon_Color": {
      "name": "--pf-c-app-launcher__menu-item-external-icon--Color",
      "value": "#06c",
      "values": [
        "--pf-global--link--Color",
        "$pf-global--link--Color",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_app_launcher__menu_item_external_icon_PaddingLeft": {
      "name": "--pf-c-app-launcher__menu-item-external-icon--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_app_launcher__menu_item_external_icon_TranslateY": {
      "name": "--pf-c-app-launcher__menu-item-external-icon--TranslateY",
      "value": "-0.0625rem"
    },
    "c_app_launcher__menu_item_external_icon_FontSize": {
      "name": "--pf-c-app-launcher__menu-item-external-icon--FontSize",
      "value": "0.625rem",
      "values": [
        "--pf-global--icon--FontSize--sm",
        "$pf-global--icon--FontSize--sm",
        "pf-font-prem(10px)",
        "0.625rem"
      ]
    },
    "c_app_launcher__group_group_PaddingTop": {
      "name": "--pf-c-app-launcher__group--group--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_app_launcher__group_title_PaddingTop": {
      "name": "--pf-c-app-launcher__group-title--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_app_launcher__group_title_PaddingRight": {
      "name": "--pf-c-app-launcher__group-title--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-c-app-launcher__menu-item--PaddingRight",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_app_launcher__group_title_PaddingBottom": {
      "name": "--pf-c-app-launcher__group-title--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-c-app-launcher__menu-item--PaddingBottom",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_app_launcher__group_title_PaddingLeft": {
      "name": "--pf-c-app-launcher__group-title--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-c-app-launcher__menu-item--PaddingLeft",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_app_launcher__group_title_FontSize": {
      "name": "--pf-c-app-launcher__group-title--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_app_launcher__group_title_FontWeight": {
      "name": "--pf-c-app-launcher__group-title--FontWeight",
      "value": "700",
      "values": [
        "--pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    },
    "c_app_launcher__group_title_Color": {
      "name": "--pf-c-app-launcher__group-title--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--dark-200",
        "$pf-global--Color--dark-200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_app_launcher_c_divider_MarginTop": {
      "name": "--pf-c-app-launcher--c-divider--MarginTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_app_launcher_c_divider_MarginBottom": {
      "name": "--pf-c-app-launcher--c-divider--MarginBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-c-app-launcher .pf-c-divider:last-child": {
    "c_app_launcher_c_divider_MarginBottom": {
      "name": "--pf-c-app-launcher--c-divider--MarginBottom",
      "value": "0"
    }
  },
  ".pf-c-app-launcher__toggle:hover": {
    "c_app_launcher__toggle_Color": {
      "name": "--pf-c-app-launcher__toggle--Color",
      "value": "#151515",
      "values": [
        "--pf-c-app-launcher__toggle--hover--Color",
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    }
  },
  ".pf-c-app-launcher__toggle:active": {
    "c_app_launcher__toggle_Color": {
      "name": "--pf-c-app-launcher__toggle--Color",
      "value": "#151515",
      "values": [
        "--pf-c-app-launcher__toggle--active--Color",
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    }
  },
  ".pf-c-app-launcher__toggle:focus": {
    "c_app_launcher__toggle_Color": {
      "name": "--pf-c-app-launcher__toggle--Color",
      "value": "#151515",
      "values": [
        "--pf-c-app-launcher__toggle--focus--Color",
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    }
  },
  ".pf-c-app-launcher__toggle:disabled": {
    "c_app_launcher__toggle_Color": {
      "name": "--pf-c-app-launcher__toggle--Color",
      "value": "#d2d2d2",
      "values": [
        "--pf-c-app-launcher__toggle--disabled--Color",
        "--pf-global--disabled-color--200",
        "$pf-global--disabled-color--200",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    }
  },
  ".pf-c-app-launcher.pf-m-top .pf-c-app-launcher__menu": {
    "c_app_launcher__menu_Top": {
      "name": "--pf-c-app-launcher__menu--Top",
      "value": "0",
      "values": [
        "--pf-c-app-launcher--m-top__menu--Top",
        "0"
      ]
    }
  },
  ".pf-c-app-launcher__menu-wrapper.pf-m-favorite": {
    "c_app_launcher__menu_item_m_action_Color": {
      "name": "--pf-c-app-launcher__menu-item--m-action--Color",
      "value": "#f0ab00",
      "values": [
        "--pf-c-app-launcher__menu-item--m-favorite__menu-item--m-action--Color",
        "--pf-global--palette--gold-400",
        "$pf-color-gold-400",
        "#f0ab00"
      ]
    }
  },
  ".pf-c-app-launcher__menu-item:hover": {
    "c_app_launcher__menu_item_m_action_Color": {
      "name": "--pf-c-app-launcher__menu-item--m-action--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-c-app-launcher__menu-item--hover__menu-item--m-action--Color",
        "--pf-global--Color--200",
        "$pf-global--Color--200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    }
  },
  ".pf-c-app-launcher__menu-item:disabled": {
    "c_app_launcher__menu_item_Color": {
      "name": "--pf-c-app-launcher__menu-item--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-c-app-launcher__menu-item--disabled--Color",
        "--pf-global--Color--dark-200",
        "$pf-global--Color--dark-200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    }
  },
  ".pf-c-app-launcher__menu-item.pf-m-link": {
    "c_app_launcher__menu_item_PaddingRight": {
      "name": "--pf-c-app-launcher__menu-item--PaddingRight",
      "value": "0",
      "values": [
        "--pf-c-app-launcher__menu-item--m-link--PaddingRight",
        "0"
      ]
    },
    "c_app_launcher__menu_item_hover_BackgroundColor": {
      "name": "--pf-c-app-launcher__menu-item--hover--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-app-launcher__menu-item--m-link--hover--BackgroundColor",
        "transparent"
      ]
    }
  },
  ".pf-c-app-launcher__menu-item.pf-m-action": {
    "c_app_launcher__menu_item_hover_BackgroundColor": {
      "name": "--pf-c-app-launcher__menu-item--hover--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-app-launcher__menu-item--m-action--hover--BackgroundColor",
        "transparent"
      ]
    },
    "c_app_launcher__menu_item_Color": {
      "name": "--pf-c-app-launcher__menu-item--Color",
      "value": "#d2d2d2",
      "values": [
        "--pf-c-app-launcher__menu-item--m-action--Color",
        "--pf-global--disabled-color--200",
        "$pf-global--disabled-color--200",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_app_launcher__menu_item_Width": {
      "name": "--pf-c-app-launcher__menu-item--Width",
      "value": "auto",
      "values": [
        "--pf-c-app-launcher__menu-item--m-action--Width",
        "auto"
      ]
    }
  },
  ".pf-c-app-launcher__menu-item.pf-m-action:hover": {
    "c_app_launcher__menu_item_m_action_Color": {
      "name": "--pf-c-app-launcher__menu-item--m-action--Color",
      "value": "#151515",
      "values": [
        "--pf-c-app-launcher__menu-item--m-action--hover--Color",
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    }
  }
};
export default c_app_launcher;
"use strict";
exports.__esModule = true;
exports.c_breadcrumb__item_MarginRight = {
  "name": "--pf-c-breadcrumb__item--MarginRight",
  "value": "0.5rem",
  "var": "var(--pf-c-breadcrumb__item--MarginRight)"
};
exports["default"] = exports.c_breadcrumb__item_MarginRight;
"use strict";
exports.__esModule = true;
exports.c_table_m_compact_cell_first_last_child_xl_PaddingRight = {
  "name": "--pf-c-table--m-compact--cell--first-last-child--xl--PaddingRight",
  "value": "1.5rem",
  "var": "var(--pf-c-table--m-compact--cell--first-last-child--xl--PaddingRight)"
};
exports["default"] = exports.c_table_m_compact_cell_first_last_child_xl_PaddingRight;
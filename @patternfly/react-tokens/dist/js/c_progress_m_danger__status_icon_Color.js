"use strict";
exports.__esModule = true;
exports.c_progress_m_danger__status_icon_Color = {
  "name": "--pf-c-progress--m-danger__status-icon--Color",
  "value": "#c9190b",
  "var": "var(--pf-c-progress--m-danger__status-icon--Color)"
};
exports["default"] = exports.c_progress_m_danger__status_icon_Color;
"use strict";
exports.__esModule = true;
exports.c_card_child_PaddingRight = {
  "name": "--pf-c-card--child--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-card--child--PaddingRight)"
};
exports["default"] = exports.c_card_child_PaddingRight;
"use strict";
exports.__esModule = true;
exports.global_warning_color_200 = {
  "name": "--pf-global--warning-color--200",
  "value": "#795600",
  "var": "var(--pf-global--warning-color--200)"
};
exports["default"] = exports.global_warning_color_200;
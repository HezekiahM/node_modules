export const c_label_m_cyan_BackgroundColor: {
  "name": "--pf-c-label--m-cyan--BackgroundColor",
  "value": "#f2f9f9",
  "var": "var(--pf-c-label--m-cyan--BackgroundColor)"
};
export default c_label_m_cyan_BackgroundColor;
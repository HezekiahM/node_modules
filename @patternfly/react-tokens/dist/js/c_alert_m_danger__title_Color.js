"use strict";
exports.__esModule = true;
exports.c_alert_m_danger__title_Color = {
  "name": "--pf-c-alert--m-danger__title--Color",
  "value": "#a30000",
  "var": "var(--pf-c-alert--m-danger__title--Color)"
};
exports["default"] = exports.c_alert_m_danger__title_Color;
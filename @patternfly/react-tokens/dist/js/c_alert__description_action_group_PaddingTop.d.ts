export const c_alert__description_action_group_PaddingTop: {
  "name": "--pf-c-alert__description--action-group--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-alert__description--action-group--PaddingTop)"
};
export default c_alert__description_action_group_PaddingTop;
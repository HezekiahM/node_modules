"use strict";
exports.__esModule = true;
exports.c_table__expandable_row_MaxHeight = {
  "name": "--pf-c-table__expandable-row--MaxHeight",
  "value": "28.125rem",
  "var": "var(--pf-c-table__expandable-row--MaxHeight)"
};
exports["default"] = exports.c_table__expandable_row_MaxHeight;
"use strict";
exports.__esModule = true;
exports.c_app_launcher__toggle_PaddingRight = {
  "name": "--pf-c-app-launcher__toggle--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-app-launcher__toggle--PaddingRight)"
};
exports["default"] = exports.c_app_launcher__toggle_PaddingRight;
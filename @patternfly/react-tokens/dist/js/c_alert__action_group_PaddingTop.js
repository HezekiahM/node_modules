"use strict";
exports.__esModule = true;
exports.c_alert__action_group_PaddingTop = {
  "name": "--pf-c-alert__action-group--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-alert__action-group--PaddingTop)"
};
exports["default"] = exports.c_alert__action_group_PaddingTop;
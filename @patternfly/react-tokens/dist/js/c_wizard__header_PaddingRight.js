"use strict";
exports.__esModule = true;
exports.c_wizard__header_PaddingRight = {
  "name": "--pf-c-wizard__header--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-wizard__header--PaddingRight)"
};
exports["default"] = exports.c_wizard__header_PaddingRight;
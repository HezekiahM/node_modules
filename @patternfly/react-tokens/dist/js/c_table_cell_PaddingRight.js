"use strict";
exports.__esModule = true;
exports.c_table_cell_PaddingRight = {
  "name": "--pf-c-table--cell--PaddingRight",
  "value": "4rem",
  "var": "var(--pf-c-table--cell--PaddingRight)"
};
exports["default"] = exports.c_table_cell_PaddingRight;
"use strict";
exports.__esModule = true;
exports.c_login__footer_PaddingLeft = {
  "name": "--pf-c-login__footer--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-login__footer--PaddingLeft)"
};
exports["default"] = exports.c_login__footer_PaddingLeft;
"use strict";
exports.__esModule = true;
exports.c_hint__title_FontSize = {
  "name": "--pf-c-hint__title--FontSize",
  "value": "1.125rem",
  "var": "var(--pf-c-hint__title--FontSize)"
};
exports["default"] = exports.c_hint__title_FontSize;
"use strict";
exports.__esModule = true;
exports.c_page__header_tools_c_avatar_MarginLeft = {
  "name": "--pf-c-page__header-tools--c-avatar--MarginLeft",
  "value": "1rem",
  "var": "var(--pf-c-page__header-tools--c-avatar--MarginLeft)"
};
exports["default"] = exports.c_page__header_tools_c_avatar_MarginLeft;
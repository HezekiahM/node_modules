export const chart_global_Fill_Color_200: {
  "name": "--pf-chart-global--Fill--Color--200",
  "value": "#f0f0f0",
  "var": "var(--pf-chart-global--Fill--Color--200)"
};
export default chart_global_Fill_Color_200;
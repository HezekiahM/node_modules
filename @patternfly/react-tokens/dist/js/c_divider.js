"use strict";
exports.__esModule = true;
exports.c_divider = {
  ".pf-c-divider": {
    "c_divider_Height": {
      "name": "--pf-c-divider--Height",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_divider_BackgroundColor": {
      "name": "--pf-c-divider--BackgroundColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_divider_after_Height": {
      "name": "--pf-c-divider--after--Height",
      "value": "1px",
      "values": [
        "--pf-c-divider--Height",
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_divider_after_BackgroundColor": {
      "name": "--pf-c-divider--after--BackgroundColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-c-divider--BackgroundColor",
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_divider_after_FlexBasis": {
      "name": "--pf-c-divider--after--FlexBasis",
      "value": "100%"
    },
    "c_divider_after_Inset": {
      "name": "--pf-c-divider--after--Inset",
      "value": "0%"
    },
    "c_divider_m_vertical_after_FlexBasis": {
      "name": "--pf-c-divider--m-vertical--after--FlexBasis",
      "value": "100%"
    },
    "c_divider_m_vertical_after_Width": {
      "name": "--pf-c-divider--m-vertical--after--Width",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    }
  },
  ".pf-c-divider.pf-m-inset-none": {
    "c_divider_after_Inset": {
      "name": "--pf-c-divider--after--Inset",
      "value": "0%"
    }
  },
  ".pf-c-divider.pf-m-inset-xs": {
    "c_divider_after_Inset": {
      "name": "--pf-c-divider--after--Inset",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    }
  },
  ".pf-c-divider.pf-m-inset-sm": {
    "c_divider_after_Inset": {
      "name": "--pf-c-divider--after--Inset",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-c-divider.pf-m-inset-md": {
    "c_divider_after_Inset": {
      "name": "--pf-c-divider--after--Inset",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    }
  },
  ".pf-c-divider.pf-m-inset-lg": {
    "c_divider_after_Inset": {
      "name": "--pf-c-divider--after--Inset",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    }
  },
  ".pf-c-divider.pf-m-inset-xl": {
    "c_divider_after_Inset": {
      "name": "--pf-c-divider--after--Inset",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    }
  },
  ".pf-c-divider.pf-m-inset-2xl": {
    "c_divider_after_Inset": {
      "name": "--pf-c-divider--after--Inset",
      "value": "3rem",
      "values": [
        "--pf-global--spacer--2xl",
        "$pf-global--spacer--2xl",
        "pf-size-prem(48px)",
        "3rem"
      ]
    }
  },
  ".pf-c-divider.pf-m-inset-3xl": {
    "c_divider_after_Inset": {
      "name": "--pf-c-divider--after--Inset",
      "value": "4rem",
      "values": [
        "--pf-global--spacer--3xl",
        "$pf-global--spacer--3xl",
        "pf-size-prem(64px)",
        "4rem"
      ]
    }
  }
};
exports["default"] = exports.c_divider;
"use strict";
exports.__esModule = true;
exports.c_label_PaddingRight = {
  "name": "--pf-c-label--PaddingRight",
  "value": "0.5rem",
  "var": "var(--pf-c-label--PaddingRight)"
};
exports["default"] = exports.c_label_PaddingRight;
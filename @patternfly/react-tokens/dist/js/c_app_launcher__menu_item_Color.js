"use strict";
exports.__esModule = true;
exports.c_app_launcher__menu_item_Color = {
  "name": "--pf-c-app-launcher__menu-item--Color",
  "value": "#d2d2d2",
  "var": "var(--pf-c-app-launcher__menu-item--Color)"
};
exports["default"] = exports.c_app_launcher__menu_item_Color;
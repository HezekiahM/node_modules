export const c_label_m_green__icon_Color: {
  "name": "--pf-c-label--m-green__icon--Color",
  "value": "#3e8635",
  "var": "var(--pf-c-label--m-green__icon--Color)"
};
export default c_label_m_green__icon_Color;
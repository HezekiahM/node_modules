"use strict";
exports.__esModule = true;
exports.c_alert_PaddingLeft = {
  "name": "--pf-c-alert--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-alert--PaddingLeft)"
};
exports["default"] = exports.c_alert_PaddingLeft;
"use strict";
exports.__esModule = true;
exports.c_wizard__nav_list_xl_PaddingBottom = {
  "name": "--pf-c-wizard__nav-list--xl--PaddingBottom",
  "value": "1.5rem",
  "var": "var(--pf-c-wizard__nav-list--xl--PaddingBottom)"
};
exports["default"] = exports.c_wizard__nav_list_xl_PaddingBottom;
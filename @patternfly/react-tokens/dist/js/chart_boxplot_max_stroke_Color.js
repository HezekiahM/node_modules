"use strict";
exports.__esModule = true;
exports.chart_boxplot_max_stroke_Color = {
  "name": "--pf-chart-boxplot--max--stroke--Color",
  "value": "#151515",
  "var": "var(--pf-chart-boxplot--max--stroke--Color)"
};
exports["default"] = exports.chart_boxplot_max_stroke_Color;
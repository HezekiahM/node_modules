export const c_tile_m_disabled__icon_Color: {
  "name": "--pf-c-tile--m-disabled__icon--Color",
  "value": "#6a6e73",
  "var": "var(--pf-c-tile--m-disabled__icon--Color)"
};
export default c_tile_m_disabled__icon_Color;
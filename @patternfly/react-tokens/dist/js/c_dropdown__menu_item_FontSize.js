"use strict";
exports.__esModule = true;
exports.c_dropdown__menu_item_FontSize = {
  "name": "--pf-c-dropdown__menu-item--FontSize",
  "value": "1rem",
  "var": "var(--pf-c-dropdown__menu-item--FontSize)"
};
exports["default"] = exports.c_dropdown__menu_item_FontSize;
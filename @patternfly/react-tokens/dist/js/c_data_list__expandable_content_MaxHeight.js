"use strict";
exports.__esModule = true;
exports.c_data_list__expandable_content_MaxHeight = {
  "name": "--pf-c-data-list__expandable-content--MaxHeight",
  "value": "37.5rem",
  "var": "var(--pf-c-data-list__expandable-content--MaxHeight)"
};
exports["default"] = exports.c_data_list__expandable_content_MaxHeight;
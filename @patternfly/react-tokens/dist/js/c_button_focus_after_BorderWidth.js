"use strict";
exports.__esModule = true;
exports.c_button_focus_after_BorderWidth = {
  "name": "--pf-c-button--focus--after--BorderWidth",
  "value": "2px",
  "var": "var(--pf-c-button--focus--after--BorderWidth)"
};
exports["default"] = exports.c_button_focus_after_BorderWidth;
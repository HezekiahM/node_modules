export const c_chip_PaddingTop: {
  "name": "--pf-c-chip--PaddingTop",
  "value": "0.25rem",
  "var": "var(--pf-c-chip--PaddingTop)"
};
export default c_chip_PaddingTop;
"use strict";
exports.__esModule = true;
exports.c_login__main_MarginBottom = {
  "name": "--pf-c-login__main--MarginBottom",
  "value": "1.5rem",
  "var": "var(--pf-c-login__main--MarginBottom)"
};
exports["default"] = exports.c_login__main_MarginBottom;
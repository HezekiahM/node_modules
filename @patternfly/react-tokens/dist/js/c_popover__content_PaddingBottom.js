"use strict";
exports.__esModule = true;
exports.c_popover__content_PaddingBottom = {
  "name": "--pf-c-popover__content--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-popover__content--PaddingBottom)"
};
exports["default"] = exports.c_popover__content_PaddingBottom;
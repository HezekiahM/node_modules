"use strict";
exports.__esModule = true;
exports.c_switch__toggle_before_Width = {
  "name": "--pf-c-switch__toggle--before--Width",
  "value": "calc(1rem - 0.125rem)",
  "var": "var(--pf-c-switch__toggle--before--Width)"
};
exports["default"] = exports.c_switch__toggle_before_Width;
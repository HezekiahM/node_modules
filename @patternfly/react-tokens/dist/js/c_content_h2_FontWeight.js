"use strict";
exports.__esModule = true;
exports.c_content_h2_FontWeight = {
  "name": "--pf-c-content--h2--FontWeight",
  "value": "400",
  "var": "var(--pf-c-content--h2--FontWeight)"
};
exports["default"] = exports.c_content_h2_FontWeight;
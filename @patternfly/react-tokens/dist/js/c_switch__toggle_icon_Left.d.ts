export const c_switch__toggle_icon_Left: {
  "name": "--pf-c-switch__toggle-icon--Left",
  "value": "calc(1rem * .4)",
  "var": "var(--pf-c-switch__toggle-icon--Left)"
};
export default c_switch__toggle_icon_Left;
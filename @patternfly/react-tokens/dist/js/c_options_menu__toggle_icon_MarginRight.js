"use strict";
exports.__esModule = true;
exports.c_options_menu__toggle_icon_MarginRight = {
  "name": "--pf-c-options-menu__toggle-icon--MarginRight",
  "value": "0.5rem",
  "var": "var(--pf-c-options-menu__toggle-icon--MarginRight)"
};
exports["default"] = exports.c_options_menu__toggle_icon_MarginRight;
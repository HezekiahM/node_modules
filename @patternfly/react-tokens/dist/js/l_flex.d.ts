export const l_flex: {
  ".pf-l-flex": {
    "l_flex_Display": {
      "name": "--pf-l-flex--Display",
      "value": "flex"
    },
    "l_flex_FlexWrap": {
      "name": "--pf-l-flex--FlexWrap",
      "value": "wrap"
    },
    "l_flex_AlignItems": {
      "name": "--pf-l-flex--AlignItems",
      "value": "baseline"
    },
    "l_flex_m_row_AlignItems": {
      "name": "--pf-l-flex--m-row--AlignItems",
      "value": "baseline"
    },
    "l_flex_m_row_reverse_AlignItems": {
      "name": "--pf-l-flex--m-row-reverse--AlignItems",
      "value": "baseline"
    },
    "l_flex_spacer_base": {
      "name": "--pf-l-flex--spacer-base",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "1rem",
      "values": [
        "--pf-l-flex--spacer-base",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "l_flex_spacer_none": {
      "name": "--pf-l-flex--spacer--none",
      "value": "0"
    },
    "l_flex_spacer_xs": {
      "name": "--pf-l-flex--spacer--xs",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "l_flex_spacer_sm": {
      "name": "--pf-l-flex--spacer--sm",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "l_flex_spacer_md": {
      "name": "--pf-l-flex--spacer--md",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "l_flex_spacer_lg": {
      "name": "--pf-l-flex--spacer--lg",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "l_flex_spacer_xl": {
      "name": "--pf-l-flex--spacer--xl",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "l_flex_spacer_2xl": {
      "name": "--pf-l-flex--spacer--2xl",
      "value": "3rem",
      "values": [
        "--pf-global--spacer--2xl",
        "$pf-global--spacer--2xl",
        "pf-size-prem(48px)",
        "3rem"
      ]
    },
    "l_flex_spacer_3xl": {
      "name": "--pf-l-flex--spacer--3xl",
      "value": "4rem",
      "values": [
        "--pf-global--spacer--3xl",
        "$pf-global--spacer--3xl",
        "pf-size-prem(64px)",
        "4rem"
      ]
    },
    "l_flex_spacer_4xl": {
      "name": "--pf-l-flex--spacer--4xl",
      "value": "5rem",
      "values": [
        "--pf-global--spacer--4xl",
        "$pf-global--spacer--4xl",
        "pf-size-prem(80px)",
        "5rem"
      ]
    }
  },
  ".pf-l-flex:last-child": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "0"
    }
  },
  ".pf-l-flex > *": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "1rem",
      "values": [
        "--pf-l-flex--spacer-base",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    }
  },
  ".pf-l-flex > *:last-child": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "0"
    }
  },
  ".pf-l-flex.pf-m-inline-flex": {
    "l_flex_Display": {
      "name": "--pf-l-flex--Display",
      "value": "inline-flex"
    }
  },
  ".pf-l-flex.pf-m-space-items-none > *": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "0",
      "values": [
        "--pf-l-flex--spacer--none",
        "0"
      ]
    }
  },
  ".pf-l-flex.pf-m-space-items-none > :last-child": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "0"
    }
  },
  ".pf-l-flex.pf-m-space-items-xs > *": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "0.25rem",
      "values": [
        "--pf-l-flex--spacer--xs",
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    }
  },
  ".pf-l-flex.pf-m-space-items-xs > :last-child": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "0"
    }
  },
  ".pf-l-flex.pf-m-space-items-sm > *": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "0.5rem",
      "values": [
        "--pf-l-flex--spacer--sm",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-l-flex.pf-m-space-items-sm > :last-child": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "0"
    }
  },
  ".pf-l-flex.pf-m-space-items-md > *": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "1rem",
      "values": [
        "--pf-l-flex--spacer--md",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    }
  },
  ".pf-l-flex.pf-m-space-items-md > :last-child": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "0"
    }
  },
  ".pf-l-flex.pf-m-space-items-lg > *": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "1.5rem",
      "values": [
        "--pf-l-flex--spacer--lg",
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    }
  },
  ".pf-l-flex.pf-m-space-items-lg > :last-child": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "0"
    }
  },
  ".pf-l-flex.pf-m-space-items-xl > *": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "2rem",
      "values": [
        "--pf-l-flex--spacer--xl",
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    }
  },
  ".pf-l-flex.pf-m-space-items-xl > :last-child": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "0"
    }
  },
  ".pf-l-flex.pf-m-space-items-2xl > *": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "3rem",
      "values": [
        "--pf-l-flex--spacer--2xl",
        "--pf-global--spacer--2xl",
        "$pf-global--spacer--2xl",
        "pf-size-prem(48px)",
        "3rem"
      ]
    }
  },
  ".pf-l-flex.pf-m-space-items-2xl > :last-child": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "0"
    }
  },
  ".pf-l-flex.pf-m-space-items-3xl > *": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "4rem",
      "values": [
        "--pf-l-flex--spacer--3xl",
        "--pf-global--spacer--3xl",
        "$pf-global--spacer--3xl",
        "pf-size-prem(64px)",
        "4rem"
      ]
    }
  },
  ".pf-l-flex.pf-m-space-items-3xl > :last-child": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "0"
    }
  },
  ".pf-l-flex.pf-m-space-items-4xl > *": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "5rem",
      "values": [
        "--pf-l-flex--spacer--4xl",
        "--pf-global--spacer--4xl",
        "$pf-global--spacer--4xl",
        "pf-size-prem(80px)",
        "5rem"
      ]
    }
  },
  ".pf-l-flex.pf-m-space-items-4xl > :last-child": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "0"
    }
  },
  ".pf-l-flex .pf-m-spacer-none": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "0",
      "values": [
        "--pf-l-flex--spacer--none",
        "0"
      ]
    }
  },
  ".pf-l-flex .pf-m-spacer-none:last-child": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "0",
      "values": [
        "--pf-l-flex--spacer--none",
        "0"
      ]
    }
  },
  ".pf-l-flex .pf-m-spacer-xs": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "0.25rem",
      "values": [
        "--pf-l-flex--spacer--xs",
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    }
  },
  ".pf-l-flex .pf-m-spacer-xs:last-child": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "0.25rem",
      "values": [
        "--pf-l-flex--spacer--xs",
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    }
  },
  ".pf-l-flex .pf-m-spacer-sm": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "0.5rem",
      "values": [
        "--pf-l-flex--spacer--sm",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-l-flex .pf-m-spacer-sm:last-child": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "0.5rem",
      "values": [
        "--pf-l-flex--spacer--sm",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-l-flex .pf-m-spacer-md": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "1rem",
      "values": [
        "--pf-l-flex--spacer--md",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    }
  },
  ".pf-l-flex .pf-m-spacer-md:last-child": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "1rem",
      "values": [
        "--pf-l-flex--spacer--md",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    }
  },
  ".pf-l-flex .pf-m-spacer-lg": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "1.5rem",
      "values": [
        "--pf-l-flex--spacer--lg",
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    }
  },
  ".pf-l-flex .pf-m-spacer-lg:last-child": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "1.5rem",
      "values": [
        "--pf-l-flex--spacer--lg",
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    }
  },
  ".pf-l-flex .pf-m-spacer-xl": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "2rem",
      "values": [
        "--pf-l-flex--spacer--xl",
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    }
  },
  ".pf-l-flex .pf-m-spacer-xl:last-child": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "2rem",
      "values": [
        "--pf-l-flex--spacer--xl",
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    }
  },
  ".pf-l-flex .pf-m-spacer-2xl": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "3rem",
      "values": [
        "--pf-l-flex--spacer--2xl",
        "--pf-global--spacer--2xl",
        "$pf-global--spacer--2xl",
        "pf-size-prem(48px)",
        "3rem"
      ]
    }
  },
  ".pf-l-flex .pf-m-spacer-2xl:last-child": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "3rem",
      "values": [
        "--pf-l-flex--spacer--2xl",
        "--pf-global--spacer--2xl",
        "$pf-global--spacer--2xl",
        "pf-size-prem(48px)",
        "3rem"
      ]
    }
  },
  ".pf-l-flex .pf-m-spacer-3xl": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "4rem",
      "values": [
        "--pf-l-flex--spacer--3xl",
        "--pf-global--spacer--3xl",
        "$pf-global--spacer--3xl",
        "pf-size-prem(64px)",
        "4rem"
      ]
    }
  },
  ".pf-l-flex .pf-m-spacer-3xl:last-child": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "4rem",
      "values": [
        "--pf-l-flex--spacer--3xl",
        "--pf-global--spacer--3xl",
        "$pf-global--spacer--3xl",
        "pf-size-prem(64px)",
        "4rem"
      ]
    }
  },
  ".pf-l-flex .pf-m-spacer-4xl": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "5rem",
      "values": [
        "--pf-l-flex--spacer--4xl",
        "--pf-global--spacer--4xl",
        "$pf-global--spacer--4xl",
        "pf-size-prem(80px)",
        "5rem"
      ]
    }
  },
  ".pf-l-flex .pf-m-spacer-4xl:last-child": {
    "l_flex_spacer": {
      "name": "--pf-l-flex--spacer",
      "value": "5rem",
      "values": [
        "--pf-l-flex--spacer--4xl",
        "--pf-global--spacer--4xl",
        "$pf-global--spacer--4xl",
        "pf-size-prem(80px)",
        "5rem"
      ]
    }
  }
};
export default l_flex;
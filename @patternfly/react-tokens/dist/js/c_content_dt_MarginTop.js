"use strict";
exports.__esModule = true;
exports.c_content_dt_MarginTop = {
  "name": "--pf-c-content--dt--MarginTop",
  "value": "1rem",
  "var": "var(--pf-c-content--dt--MarginTop)"
};
exports["default"] = exports.c_content_dt_MarginTop;
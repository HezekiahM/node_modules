"use strict";
exports.__esModule = true;
exports.c_empty_state__secondary_child_MarginLeft = {
  "name": "--pf-c-empty-state__secondary--child--MarginLeft",
  "value": "calc(0.25rem / 2)",
  "var": "var(--pf-c-empty-state__secondary--child--MarginLeft)"
};
exports["default"] = exports.c_empty_state__secondary_child_MarginLeft;
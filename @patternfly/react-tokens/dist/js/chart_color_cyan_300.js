"use strict";
exports.__esModule = true;
exports.chart_color_cyan_300 = {
  "name": "--pf-chart-color-cyan-300",
  "value": "#009596",
  "var": "var(--pf-chart-color-cyan-300)"
};
exports["default"] = exports.chart_color_cyan_300;
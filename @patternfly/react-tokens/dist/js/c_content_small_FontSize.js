"use strict";
exports.__esModule = true;
exports.c_content_small_FontSize = {
  "name": "--pf-c-content--small--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-content--small--FontSize)"
};
exports["default"] = exports.c_content_small_FontSize;
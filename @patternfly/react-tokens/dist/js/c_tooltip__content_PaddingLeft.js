"use strict";
exports.__esModule = true;
exports.c_tooltip__content_PaddingLeft = {
  "name": "--pf-c-tooltip__content--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-tooltip__content--PaddingLeft)"
};
exports["default"] = exports.c_tooltip__content_PaddingLeft;
"use strict";
exports.__esModule = true;
exports.c_about_modal_box_PaddingRight = {
  "name": "--pf-c-about-modal-box--PaddingRight",
  "value": "2rem",
  "var": "var(--pf-c-about-modal-box--PaddingRight)"
};
exports["default"] = exports.c_about_modal_box_PaddingRight;
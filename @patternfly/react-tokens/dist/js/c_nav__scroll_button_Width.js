"use strict";
exports.__esModule = true;
exports.c_nav__scroll_button_Width = {
  "name": "--pf-c-nav__scroll-button--Width",
  "value": "44px",
  "var": "var(--pf-c-nav__scroll-button--Width)"
};
exports["default"] = exports.c_nav__scroll_button_Width;
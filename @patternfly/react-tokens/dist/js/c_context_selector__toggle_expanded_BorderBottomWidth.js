"use strict";
exports.__esModule = true;
exports.c_context_selector__toggle_expanded_BorderBottomWidth = {
  "name": "--pf-c-context-selector__toggle--expanded--BorderBottomWidth",
  "value": "2px",
  "var": "var(--pf-c-context-selector__toggle--expanded--BorderBottomWidth)"
};
exports["default"] = exports.c_context_selector__toggle_expanded_BorderBottomWidth;
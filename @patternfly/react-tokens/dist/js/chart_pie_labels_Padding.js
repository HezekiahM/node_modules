"use strict";
exports.__esModule = true;
exports.chart_pie_labels_Padding = {
  "name": "--pf-chart-pie--labels--Padding",
  "value": 8,
  "var": "var(--pf-chart-pie--labels--Padding)"
};
exports["default"] = exports.chart_pie_labels_Padding;
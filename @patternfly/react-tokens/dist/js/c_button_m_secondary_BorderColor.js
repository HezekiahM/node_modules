"use strict";
exports.__esModule = true;
exports.c_button_m_secondary_BorderColor = {
  "name": "--pf-c-button--m-secondary--BorderColor",
  "value": "#fff",
  "var": "var(--pf-c-button--m-secondary--BorderColor)"
};
exports["default"] = exports.c_button_m_secondary_BorderColor;
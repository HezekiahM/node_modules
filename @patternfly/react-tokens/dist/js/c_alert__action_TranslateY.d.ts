export const c_alert__action_TranslateY: {
  "name": "--pf-c-alert__action--TranslateY",
  "value": "0.125rem",
  "var": "var(--pf-c-alert__action--TranslateY)"
};
export default c_alert__action_TranslateY;
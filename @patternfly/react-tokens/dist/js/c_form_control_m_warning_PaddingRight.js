"use strict";
exports.__esModule = true;
exports.c_form_control_m_warning_PaddingRight = {
  "name": "--pf-c-form-control--m-warning--PaddingRight",
  "value": "2rem",
  "var": "var(--pf-c-form-control--m-warning--PaddingRight)"
};
exports["default"] = exports.c_form_control_m_warning_PaddingRight;
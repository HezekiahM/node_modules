export const global_palette_green_50: {
  "name": "--pf-global--palette--green-50",
  "value": "#f3faf2",
  "var": "var(--pf-global--palette--green-50)"
};
export default global_palette_green_50;
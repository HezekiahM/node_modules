"use strict";
exports.__esModule = true;
exports.c_tabs__link_before_BorderRightColor = {
  "name": "--pf-c-tabs__link--before--BorderRightColor",
  "value": "#fff",
  "var": "var(--pf-c-tabs__link--before--BorderRightColor)"
};
exports["default"] = exports.c_tabs__link_before_BorderRightColor;
"use strict";
exports.__esModule = true;
exports.c_page__sidebar_m_light_BackgroundColor = {
  "name": "--pf-c-page__sidebar--m-light--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-page__sidebar--m-light--BackgroundColor)"
};
exports["default"] = exports.c_page__sidebar_m_light_BackgroundColor;
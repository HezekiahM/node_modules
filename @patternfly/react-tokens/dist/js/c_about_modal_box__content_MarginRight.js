"use strict";
exports.__esModule = true;
exports.c_about_modal_box__content_MarginRight = {
  "name": "--pf-c-about-modal-box__content--MarginRight",
  "value": "2rem",
  "var": "var(--pf-c-about-modal-box__content--MarginRight)"
};
exports["default"] = exports.c_about_modal_box__content_MarginRight;
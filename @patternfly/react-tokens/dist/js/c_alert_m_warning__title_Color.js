"use strict";
exports.__esModule = true;
exports.c_alert_m_warning__title_Color = {
  "name": "--pf-c-alert--m-warning__title--Color",
  "value": "#795600",
  "var": "var(--pf-c-alert--m-warning__title--Color)"
};
exports["default"] = exports.c_alert_m_warning__title_Color;
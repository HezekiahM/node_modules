export const c_tabs_inset: {
  "name": "--pf-c-tabs--inset",
  "value": "3rem",
  "var": "var(--pf-c-tabs--inset)"
};
export default c_tabs_inset;
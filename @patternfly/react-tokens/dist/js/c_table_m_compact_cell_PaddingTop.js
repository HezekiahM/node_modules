"use strict";
exports.__esModule = true;
exports.c_table_m_compact_cell_PaddingTop = {
  "name": "--pf-c-table--m-compact--cell--PaddingTop",
  "value": "0.5rem",
  "var": "var(--pf-c-table--m-compact--cell--PaddingTop)"
};
exports["default"] = exports.c_table_m_compact_cell_PaddingTop;
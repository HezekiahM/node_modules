"use strict";
exports.__esModule = true;
exports.c_card__actions_PaddingLeft = {
  "name": "--pf-c-card__actions--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-card__actions--PaddingLeft)"
};
exports["default"] = exports.c_card__actions_PaddingLeft;
"use strict";
exports.__esModule = true;
exports.chart_bullet_group_title_divider_stroke_Width = {
  "name": "--pf-chart-bullet--group-title--divider--stroke--Width",
  "value": 2,
  "var": "var(--pf-chart-bullet--group-title--divider--stroke--Width)"
};
exports["default"] = exports.chart_bullet_group_title_divider_stroke_Width;
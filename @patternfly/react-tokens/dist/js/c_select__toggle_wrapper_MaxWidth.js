"use strict";
exports.__esModule = true;
exports.c_select__toggle_wrapper_MaxWidth = {
  "name": "--pf-c-select__toggle-wrapper--MaxWidth",
  "value": "calc(100% - 1.5rem)",
  "var": "var(--pf-c-select__toggle-wrapper--MaxWidth)"
};
exports["default"] = exports.c_select__toggle_wrapper_MaxWidth;
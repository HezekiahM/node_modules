export const c_notification_drawer: {
  ".pf-c-notification-drawer": {
    "c_notification_drawer_BackgroundColor": {
      "name": "--pf-c-notification-drawer--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--200",
        "$pf-global--BackgroundColor--200",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_notification_drawer__header_PaddingTop": {
      "name": "--pf-c-notification-drawer__header--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_notification_drawer__header_PaddingRight": {
      "name": "--pf-c-notification-drawer__header--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_notification_drawer__header_PaddingBottom": {
      "name": "--pf-c-notification-drawer__header--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_notification_drawer__header_PaddingLeft": {
      "name": "--pf-c-notification-drawer__header--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_notification_drawer__header_BackgroundColor": {
      "name": "--pf-c-notification-drawer__header--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_notification_drawer__header_BoxShadow": {
      "name": "--pf-c-notification-drawer__header--BoxShadow",
      "value": "0 0.125rem 0.25rem -0.0625rem rgba(3, 3, 3, 0.16)",
      "values": [
        "--pf-global--BoxShadow--sm-bottom",
        "$pf-global--BoxShadow--sm-bottom",
        "0 pf-size-prem(2px) pf-size-prem(4px) pf-size-prem(-1px) rgba($pf-color-black-1000, .16)",
        "0 pf-size-prem(2px) pf-size-prem(4px) pf-size-prem(-1px) rgba(#030303, .16)",
        "0 0.125rem 0.25rem -0.0625rem rgba(3, 3, 3, 0.16)"
      ]
    },
    "c_notification_drawer__header_ZIndex": {
      "name": "--pf-c-notification-drawer__header--ZIndex",
      "value": "200",
      "values": [
        "--pf-global--ZIndex--sm",
        "$pf-global--ZIndex--sm",
        "200"
      ]
    },
    "c_notification_drawer__header_title_FontSize": {
      "name": "--pf-c-notification-drawer__header-title--FontSize",
      "value": "1.25rem",
      "values": [
        "--pf-global--FontSize--xl",
        "$pf-global--FontSize--xl",
        "pf-font-prem(20px)",
        "1.25rem"
      ]
    },
    "c_notification_drawer__header_status_MarginLeft": {
      "name": "--pf-c-notification-drawer__header-status--MarginLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_notification_drawer__body_ZIndex": {
      "name": "--pf-c-notification-drawer__body--ZIndex",
      "value": "100",
      "values": [
        "--pf-global--ZIndex--xs",
        "$pf-global--ZIndex--xs",
        "100"
      ]
    },
    "c_notification_drawer__list_item_PaddingTop": {
      "name": "--pf-c-notification-drawer__list-item--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_notification_drawer__list_item_PaddingRight": {
      "name": "--pf-c-notification-drawer__list-item--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_notification_drawer__list_item_PaddingBottom": {
      "name": "--pf-c-notification-drawer__list-item--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_notification_drawer__list_item_PaddingLeft": {
      "name": "--pf-c-notification-drawer__list-item--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_notification_drawer__list_item_BackgroundColor": {
      "name": "--pf-c-notification-drawer__list-item--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_notification_drawer__list_item_BoxShadow": {
      "name": "--pf-c-notification-drawer__list-item--BoxShadow",
      "value": "inset 0 0.125rem 0.25rem -0.0625rem rgba(3, 3, 3, 0.16)",
      "values": [
        "inset --pf-global--BoxShadow--sm-bottom",
        "inset $pf-global--BoxShadow--sm-bottom",
        "inset 0 pf-size-prem(2px) pf-size-prem(4px) pf-size-prem(-1px) rgba($pf-color-black-1000, .16)",
        "inset 0 pf-size-prem(2px) pf-size-prem(4px) pf-size-prem(-1px) rgba(#030303, .16)",
        "inset 0 0.125rem 0.25rem -0.0625rem rgba(3, 3, 3, 0.16)"
      ]
    },
    "c_notification_drawer__list_item_BorderBottomWidth": {
      "name": "--pf-c-notification-drawer__list-item--BorderBottomWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_notification_drawer__list_item_BorderBottomColor": {
      "name": "--pf-c-notification-drawer__list-item--BorderBottomColor",
      "value": "transparent"
    },
    "c_notification_drawer__list_item_OutlineOffset": {
      "name": "--pf-c-notification-drawer__list-item--OutlineOffset",
      "value": "-0.25rem"
    },
    "c_notification_drawer__list_item_before_Width": {
      "name": "--pf-c-notification-drawer__list-item--before--Width",
      "value": "3px",
      "values": [
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    },
    "c_notification_drawer__list_item_before_Top": {
      "name": "--pf-c-notification-drawer__list-item--before--Top",
      "value": "0"
    },
    "c_notification_drawer__list_item_before_Bottom": {
      "name": "--pf-c-notification-drawer__list-item--before--Bottom",
      "value": "calc(1px * -1)",
      "values": [
        "calc(--pf-c-notification-drawer__list-item--BorderBottomWidth * -1)",
        "calc(--pf-global--BorderWidth--sm * -1)",
        "calc($pf-global--BorderWidth--sm * -1)",
        "calc(1px * -1)"
      ]
    },
    "c_notification_drawer__list_item_m_info__list_item_header_icon_Color": {
      "name": "--pf-c-notification-drawer__list-item--m-info__list-item-header-icon--Color",
      "value": "#2b9af3",
      "values": [
        "--pf-global--info-color--100",
        "$pf-global--info-color--100",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    },
    "c_notification_drawer__list_item_m_info__list_item_before_BackgroundColor": {
      "name": "--pf-c-notification-drawer__list-item--m-info__list-item--before--BackgroundColor",
      "value": "#2b9af3",
      "values": [
        "--pf-global--info-color--100",
        "$pf-global--info-color--100",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    },
    "c_notification_drawer__list_item_m_warning__list_item_header_icon_Color": {
      "name": "--pf-c-notification-drawer__list-item--m-warning__list-item-header-icon--Color",
      "value": "#f0ab00",
      "values": [
        "--pf-global--warning-color--100",
        "$pf-global--warning-color--100",
        "$pf-color-gold-400",
        "#f0ab00"
      ]
    },
    "c_notification_drawer__list_item_m_warning__list_item_before_BackgroundColor": {
      "name": "--pf-c-notification-drawer__list-item--m-warning__list-item--before--BackgroundColor",
      "value": "#f0ab00",
      "values": [
        "--pf-global--warning-color--100",
        "$pf-global--warning-color--100",
        "$pf-color-gold-400",
        "#f0ab00"
      ]
    },
    "c_notification_drawer__list_item_m_danger__list_item_header_icon_Color": {
      "name": "--pf-c-notification-drawer__list-item--m-danger__list-item-header-icon--Color",
      "value": "#c9190b",
      "values": [
        "--pf-global--danger-color--100",
        "$pf-global--danger-color--100",
        "$pf-color-red-100",
        "#c9190b"
      ]
    },
    "c_notification_drawer__list_item_m_danger__list_item_before_BackgroundColor": {
      "name": "--pf-c-notification-drawer__list-item--m-danger__list-item--before--BackgroundColor",
      "value": "#c9190b",
      "values": [
        "--pf-global--danger-color--100",
        "$pf-global--danger-color--100",
        "$pf-color-red-100",
        "#c9190b"
      ]
    },
    "c_notification_drawer__list_item_m_success__list_item_header_icon_Color": {
      "name": "--pf-c-notification-drawer__list-item--m-success__list-item-header-icon--Color",
      "value": "#3e8635",
      "values": [
        "--pf-global--success-color--100",
        "$pf-global--success-color--100",
        "$pf-color-green-500",
        "#3e8635"
      ]
    },
    "c_notification_drawer__list_item_m_success__list_item_before_BackgroundColor": {
      "name": "--pf-c-notification-drawer__list-item--m-success__list-item--before--BackgroundColor",
      "value": "#3e8635",
      "values": [
        "--pf-global--success-color--100",
        "$pf-global--success-color--100",
        "$pf-color-green-500",
        "#3e8635"
      ]
    },
    "c_notification_drawer__list_item_m_read_BackgroundColor": {
      "name": "--pf-c-notification-drawer__list-item--m-read--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--200",
        "$pf-global--BackgroundColor--200",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_notification_drawer__list_item_m_read_BorderBottomColor": {
      "name": "--pf-c-notification-drawer__list-item--m-read--BorderBottomColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_notification_drawer__list_item_m_read_before_Top": {
      "name": "--pf-c-notification-drawer__list-item--m-read--before--Top",
      "value": "calc(1px * -1)",
      "values": [
        "calc(--pf-c-notification-drawer__list-item--BorderBottomWidth * -1)",
        "calc(--pf-global--BorderWidth--sm * -1)",
        "calc($pf-global--BorderWidth--sm * -1)",
        "calc(1px * -1)"
      ]
    },
    "c_notification_drawer__list_item_m_read_before_Bottom": {
      "name": "--pf-c-notification-drawer__list-item--m-read--before--Bottom",
      "value": "0"
    },
    "c_notification_drawer__list_item_m_read_before_BackgroundColor": {
      "name": "--pf-c-notification-drawer__list-item--m-read--before--BackgroundColor",
      "value": "transparent"
    },
    "c_notification_drawer__list_item_list_item_m_read_before_Top": {
      "name": "--pf-c-notification-drawer__list-item--list-item--m-read--before--Top",
      "value": "0"
    },
    "c_notification_drawer__list_item_list_item_m_read_BoxShadow": {
      "name": "--pf-c-notification-drawer__list-item--list-item--m-read--BoxShadow",
      "value": "inset 0 0.125rem 0.25rem -0.0625rem rgba(3, 3, 3, 0.16)",
      "values": [
        "inset --pf-global--BoxShadow--sm-bottom",
        "inset $pf-global--BoxShadow--sm-bottom",
        "inset 0 pf-size-prem(2px) pf-size-prem(4px) pf-size-prem(-1px) rgba($pf-color-black-1000, .16)",
        "inset 0 pf-size-prem(2px) pf-size-prem(4px) pf-size-prem(-1px) rgba(#030303, .16)",
        "inset 0 0.125rem 0.25rem -0.0625rem rgba(3, 3, 3, 0.16)"
      ]
    },
    "c_notification_drawer__list_item_m_hoverable_hover_ZIndex": {
      "name": "--pf-c-notification-drawer__list-item--m-hoverable--hover--ZIndex",
      "value": "100",
      "values": [
        "--pf-global--ZIndex--xs",
        "$pf-global--ZIndex--xs",
        "100"
      ]
    },
    "c_notification_drawer__list_item_m_hoverable_hover_BoxShadow": {
      "name": "--pf-c-notification-drawer__list-item--m-hoverable--hover--BoxShadow",
      "value": "0 -0.5rem 0.5rem -0.375rem rgba(3, 3, 3, 0.18), 0 0.5rem 0.5rem -0.375rem rgba(3, 3, 3, 0.18)",
      "values": [
        "--pf-global--BoxShadow--md-top, --pf-global--BoxShadow--md-bottom",
        "$pf-global--BoxShadow--md-top, $pf-global--BoxShadow--md-bottom",
        "0 pf-size-prem(-8px) pf-size-prem(8px) pf-size-prem(-6px) rgba($pf-color-black-1000, .18), 0 pf-size-prem(8px) pf-size-prem(8px) pf-size-prem(-6px) rgba($pf-color-black-1000, .18)",
        "0 pf-size-prem(-8px) pf-size-prem(8px) pf-size-prem(-6px) rgba(#030303, .18), 0 pf-size-prem(8px) pf-size-prem(8px) pf-size-prem(-6px) rgba(#030303, .18)",
        "0 -0.5rem 0.5rem -0.375rem rgba(3, 3, 3, 0.18), 0 0.5rem 0.5rem -0.375rem rgba(3, 3, 3, 0.18)"
      ]
    },
    "c_notification_drawer__list_item_header_MarginBottom": {
      "name": "--pf-c-notification-drawer__list-item-header--MarginBottom",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_notification_drawer__list_item_header_icon_Color": {
      "name": "--pf-c-notification-drawer__list-item-header-icon--Color",
      "value": "inherit"
    },
    "c_notification_drawer__list_item_header_icon_MarginRight": {
      "name": "--pf-c-notification-drawer__list-item-header-icon--MarginRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_notification_drawer__list_item_header_title_FontWeight": {
      "name": "--pf-c-notification-drawer__list-item-header-title--FontWeight",
      "value": "700",
      "values": [
        "--pf-global--FontWeight--bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    },
    "c_notification_drawer__list_item_header_title_max_lines": {
      "name": "--pf-c-notification-drawer__list-item-header-title--max-lines",
      "value": "1"
    },
    "c_notification_drawer__list_item_m_read__list_item_header_title_FontWeight": {
      "name": "--pf-c-notification-drawer__list-item--m-read__list-item-header-title--FontWeight",
      "value": "400",
      "values": [
        "--pf-global--FontWeight--normal",
        "$pf-global--FontWeight--normal",
        "400"
      ]
    },
    "c_notification_drawer__list_item_description_MarginBottom": {
      "name": "--pf-c-notification-drawer__list-item-description--MarginBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_notification_drawer__list_item_timestamp_Color": {
      "name": "--pf-c-notification-drawer__list-item-timestamp--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--200",
        "$pf-global--Color--200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_notification_drawer__list_item_timestamp_FontSize": {
      "name": "--pf-c-notification-drawer__list-item-timestamp--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_notification_drawer__group_m_expanded_group_BorderTopWidth": {
      "name": "--pf-c-notification-drawer__group--m-expanded--group--BorderTopWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_notification_drawer__group_m_expanded_group_BorderTopColor": {
      "name": "--pf-c-notification-drawer__group--m-expanded--group--BorderTopColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_notification_drawer__group_m_expanded_MinHeight": {
      "name": "--pf-c-notification-drawer__group--m-expanded--MinHeight",
      "value": "18.75rem"
    },
    "c_notification_drawer__group_toggle_PaddingTop": {
      "name": "--pf-c-notification-drawer__group-toggle--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_notification_drawer__group_toggle_PaddingRight": {
      "name": "--pf-c-notification-drawer__group-toggle--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_notification_drawer__group_toggle_PaddingBottom": {
      "name": "--pf-c-notification-drawer__group-toggle--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_notification_drawer__group_toggle_PaddingLeft": {
      "name": "--pf-c-notification-drawer__group-toggle--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_notification_drawer__group_toggle_BackgroundColor": {
      "name": "--pf-c-notification-drawer__group-toggle--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_notification_drawer__group_toggle_BorderColor": {
      "name": "--pf-c-notification-drawer__group-toggle--BorderColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_notification_drawer__group_toggle_BorderBottomWidth": {
      "name": "--pf-c-notification-drawer__group-toggle--BorderBottomWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_notification_drawer__group_toggle_OutlineOffset": {
      "name": "--pf-c-notification-drawer__group-toggle--OutlineOffset",
      "value": "-0.25rem"
    },
    "c_notification_drawer__group_toggle_title_MarginRight": {
      "name": "--pf-c-notification-drawer__group-toggle-title--MarginRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_notification_drawer__group_toggle_title_max_lines": {
      "name": "--pf-c-notification-drawer__group-toggle-title--max-lines",
      "value": "1"
    },
    "c_notification_drawer__group_toggle_count_MarginRight": {
      "name": "--pf-c-notification-drawer__group-toggle-count--MarginRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_notification_drawer__group_toggle_icon_MarginRight": {
      "name": "--pf-c-notification-drawer__group-toggle-icon--MarginRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_notification_drawer__group_toggle_icon_Color": {
      "name": "--pf-c-notification-drawer__group-toggle-icon--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--200",
        "$pf-global--Color--200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_notification_drawer__group_toggle_icon_Transition": {
      "name": "--pf-c-notification-drawer__group-toggle-icon--Transition",
      "value": ".2s ease-in 0s"
    },
    "c_notification_drawer__group_m_expanded__group_toggle_icon_Rotate": {
      "name": "--pf-c-notification-drawer__group--m-expanded__group-toggle-icon--Rotate",
      "value": "90deg"
    }
  },
  ".pf-c-notification-drawer__list-item.pf-m-read": {
    "c_notification_drawer__list_item_BoxShadow": {
      "name": "--pf-c-notification-drawer__list-item--BoxShadow",
      "value": "none"
    },
    "c_notification_drawer__list_item_BorderBottomColor": {
      "name": "--pf-c-notification-drawer__list-item--BorderBottomColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-c-notification-drawer__list-item--m-read--BorderBottomColor",
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_notification_drawer__list_item_BackgroundColor": {
      "name": "--pf-c-notification-drawer__list-item--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-c-notification-drawer__list-item--m-read--BackgroundColor",
        "--pf-global--BackgroundColor--200",
        "$pf-global--BackgroundColor--200",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_notification_drawer__list_item_before_Top": {
      "name": "--pf-c-notification-drawer__list-item--before--Top",
      "value": "calc(1px * -1)",
      "values": [
        "--pf-c-notification-drawer__list-item--m-read--before--Top",
        "calc(--pf-c-notification-drawer__list-item--BorderBottomWidth * -1)",
        "calc(--pf-global--BorderWidth--sm * -1)",
        "calc($pf-global--BorderWidth--sm * -1)",
        "calc(1px * -1)"
      ]
    },
    "c_notification_drawer__list_item_before_Bottom": {
      "name": "--pf-c-notification-drawer__list-item--before--Bottom",
      "value": "0",
      "values": [
        "--pf-c-notification-drawer__list-item--m-read--before--Bottom",
        "0"
      ]
    },
    "c_notification_drawer__list_item_before_BackgroundColor": {
      "name": "--pf-c-notification-drawer__list-item--before--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-notification-drawer__list-item--m-read--before--BackgroundColor",
        "transparent"
      ]
    },
    "c_notification_drawer__list_item_header_title_FontWeight": {
      "name": "--pf-c-notification-drawer__list-item-header-title--FontWeight",
      "value": "400",
      "values": [
        "--pf-c-notification-drawer__list-item--m-read__list-item-header-title--FontWeight",
        "--pf-global--FontWeight--normal",
        "$pf-global--FontWeight--normal",
        "400"
      ]
    }
  },
  ".pf-c-notification-drawer__list-item:not(.pf-m-read) + .pf-c-notification-drawer__list-item.pf-m-read": {
    "c_notification_drawer__list_item_BoxShadow": {
      "name": "--pf-c-notification-drawer__list-item--BoxShadow",
      "value": "inset 0 0.125rem 0.25rem -0.0625rem rgba(3, 3, 3, 0.16)",
      "values": [
        "--pf-c-notification-drawer__list-item--list-item--m-read--BoxShadow",
        "inset --pf-global--BoxShadow--sm-bottom",
        "inset $pf-global--BoxShadow--sm-bottom",
        "inset 0 pf-size-prem(2px) pf-size-prem(4px) pf-size-prem(-1px) rgba($pf-color-black-1000, .16)",
        "inset 0 pf-size-prem(2px) pf-size-prem(4px) pf-size-prem(-1px) rgba(#030303, .16)",
        "inset 0 0.125rem 0.25rem -0.0625rem rgba(3, 3, 3, 0.16)"
      ]
    },
    "c_notification_drawer__list_item_before_Top": {
      "name": "--pf-c-notification-drawer__list-item--before--Top",
      "value": "0",
      "values": [
        "--pf-c-notification-drawer__list-item--list-item--m-read--before--Top",
        "0"
      ]
    }
  },
  ".pf-c-notification-drawer__list-item.pf-m-info": {
    "c_notification_drawer__list_item_before_BackgroundColor": {
      "name": "--pf-c-notification-drawer__list-item--before--BackgroundColor",
      "value": "#2b9af3",
      "values": [
        "--pf-c-notification-drawer__list-item--m-info__list-item--before--BackgroundColor",
        "--pf-global--info-color--100",
        "$pf-global--info-color--100",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    },
    "c_notification_drawer__list_item_header_icon_Color": {
      "name": "--pf-c-notification-drawer__list-item-header-icon--Color",
      "value": "#2b9af3",
      "values": [
        "--pf-c-notification-drawer__list-item--m-info__list-item-header-icon--Color",
        "--pf-global--info-color--100",
        "$pf-global--info-color--100",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    }
  },
  ".pf-c-notification-drawer__list-item.pf-m-warning": {
    "c_notification_drawer__list_item_before_BackgroundColor": {
      "name": "--pf-c-notification-drawer__list-item--before--BackgroundColor",
      "value": "#f0ab00",
      "values": [
        "--pf-c-notification-drawer__list-item--m-warning__list-item--before--BackgroundColor",
        "--pf-global--warning-color--100",
        "$pf-global--warning-color--100",
        "$pf-color-gold-400",
        "#f0ab00"
      ]
    },
    "c_notification_drawer__list_item_header_icon_Color": {
      "name": "--pf-c-notification-drawer__list-item-header-icon--Color",
      "value": "#f0ab00",
      "values": [
        "--pf-c-notification-drawer__list-item--m-warning__list-item-header-icon--Color",
        "--pf-global--warning-color--100",
        "$pf-global--warning-color--100",
        "$pf-color-gold-400",
        "#f0ab00"
      ]
    }
  },
  ".pf-c-notification-drawer__list-item.pf-m-danger": {
    "c_notification_drawer__list_item_before_BackgroundColor": {
      "name": "--pf-c-notification-drawer__list-item--before--BackgroundColor",
      "value": "#c9190b",
      "values": [
        "--pf-c-notification-drawer__list-item--m-danger__list-item--before--BackgroundColor",
        "--pf-global--danger-color--100",
        "$pf-global--danger-color--100",
        "$pf-color-red-100",
        "#c9190b"
      ]
    },
    "c_notification_drawer__list_item_header_icon_Color": {
      "name": "--pf-c-notification-drawer__list-item-header-icon--Color",
      "value": "#c9190b",
      "values": [
        "--pf-c-notification-drawer__list-item--m-danger__list-item-header-icon--Color",
        "--pf-global--danger-color--100",
        "$pf-global--danger-color--100",
        "$pf-color-red-100",
        "#c9190b"
      ]
    }
  },
  ".pf-c-notification-drawer__list-item.pf-m-success": {
    "c_notification_drawer__list_item_before_BackgroundColor": {
      "name": "--pf-c-notification-drawer__list-item--before--BackgroundColor",
      "value": "#3e8635",
      "values": [
        "--pf-c-notification-drawer__list-item--m-success__list-item--before--BackgroundColor",
        "--pf-global--success-color--100",
        "$pf-global--success-color--100",
        "$pf-color-green-500",
        "#3e8635"
      ]
    },
    "c_notification_drawer__list_item_header_icon_Color": {
      "name": "--pf-c-notification-drawer__list-item-header-icon--Color",
      "value": "#3e8635",
      "values": [
        "--pf-c-notification-drawer__list-item--m-success__list-item-header-icon--Color",
        "--pf-global--success-color--100",
        "$pf-global--success-color--100",
        "$pf-color-green-500",
        "#3e8635"
      ]
    }
  },
  ".pf-c-notification-drawer__group .pf-c-notification-drawer__list-item:last-child": {
    "c_notification_drawer__list_item_BorderBottomWidth": {
      "name": "--pf-c-notification-drawer__list-item--BorderBottomWidth",
      "value": "0"
    },
    "c_notification_drawer__list_item_before_Bottom": {
      "name": "--pf-c-notification-drawer__list-item--before--Bottom",
      "value": "0"
    }
  }
};
export default c_notification_drawer;
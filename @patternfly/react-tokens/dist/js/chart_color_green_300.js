"use strict";
exports.__esModule = true;
exports.chart_color_green_300 = {
  "name": "--pf-chart-color-green-300",
  "value": "#4cb140",
  "var": "var(--pf-chart-color-green-300)"
};
exports["default"] = exports.chart_color_green_300;
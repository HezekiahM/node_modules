"use strict";
exports.__esModule = true;
exports.c_label_m_blue_BackgroundColor = {
  "name": "--pf-c-label--m-blue--BackgroundColor",
  "value": "#e7f1fa",
  "var": "var(--pf-c-label--m-blue--BackgroundColor)"
};
exports["default"] = exports.c_label_m_blue_BackgroundColor;
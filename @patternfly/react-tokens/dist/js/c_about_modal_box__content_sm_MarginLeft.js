"use strict";
exports.__esModule = true;
exports.c_about_modal_box__content_sm_MarginLeft = {
  "name": "--pf-c-about-modal-box__content--sm--MarginLeft",
  "value": "4rem",
  "var": "var(--pf-c-about-modal-box__content--sm--MarginLeft)"
};
exports["default"] = exports.c_about_modal_box__content_sm_MarginLeft;
export const c_nav__subnav__link_PaddingTop: {
  "name": "--pf-c-nav__subnav__link--PaddingTop",
  "value": "0.5rem",
  "var": "var(--pf-c-nav__subnav__link--PaddingTop)"
};
export default c_nav__subnav__link_PaddingTop;
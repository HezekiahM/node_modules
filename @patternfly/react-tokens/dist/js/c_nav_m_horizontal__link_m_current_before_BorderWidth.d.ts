export const c_nav_m_horizontal__link_m_current_before_BorderWidth: {
  "name": "--pf-c-nav--m-horizontal__link--m-current--before--BorderWidth",
  "value": "3px",
  "var": "var(--pf-c-nav--m-horizontal__link--m-current--before--BorderWidth)"
};
export default c_nav_m_horizontal__link_m_current_before_BorderWidth;
"use strict";
exports.__esModule = true;
exports.c_notification_drawer__list_item_PaddingBottom = {
  "name": "--pf-c-notification-drawer__list-item--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-notification-drawer__list-item--PaddingBottom)"
};
exports["default"] = exports.c_notification_drawer__list_item_PaddingBottom;
"use strict";
exports.__esModule = true;
exports.c_tabs__link_before_BorderTopWidth = {
  "name": "--pf-c-tabs__link--before--BorderTopWidth",
  "value": "1px",
  "var": "var(--pf-c-tabs__link--before--BorderTopWidth)"
};
exports["default"] = exports.c_tabs__link_before_BorderTopWidth;
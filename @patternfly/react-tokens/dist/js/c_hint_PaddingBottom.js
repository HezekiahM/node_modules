"use strict";
exports.__esModule = true;
exports.c_hint_PaddingBottom = {
  "name": "--pf-c-hint--PaddingBottom",
  "value": "1.5rem",
  "var": "var(--pf-c-hint--PaddingBottom)"
};
exports["default"] = exports.c_hint_PaddingBottom;
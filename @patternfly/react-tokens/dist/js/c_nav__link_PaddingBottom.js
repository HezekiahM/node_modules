"use strict";
exports.__esModule = true;
exports.c_nav__link_PaddingBottom = {
  "name": "--pf-c-nav__link--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-nav__link--PaddingBottom)"
};
exports["default"] = exports.c_nav__link_PaddingBottom;
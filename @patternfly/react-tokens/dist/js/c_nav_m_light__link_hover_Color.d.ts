export const c_nav_m_light__link_hover_Color: {
  "name": "--pf-c-nav--m-light__link--hover--Color",
  "value": "#151515",
  "var": "var(--pf-c-nav--m-light__link--hover--Color)"
};
export default c_nav_m_light__link_hover_Color;
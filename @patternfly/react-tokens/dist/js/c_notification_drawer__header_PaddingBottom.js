"use strict";
exports.__esModule = true;
exports.c_notification_drawer__header_PaddingBottom = {
  "name": "--pf-c-notification-drawer__header--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-notification-drawer__header--PaddingBottom)"
};
exports["default"] = exports.c_notification_drawer__header_PaddingBottom;
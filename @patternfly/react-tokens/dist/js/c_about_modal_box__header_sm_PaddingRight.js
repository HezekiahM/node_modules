"use strict";
exports.__esModule = true;
exports.c_about_modal_box__header_sm_PaddingRight = {
  "name": "--pf-c-about-modal-box__header--sm--PaddingRight",
  "value": "4rem",
  "var": "var(--pf-c-about-modal-box__header--sm--PaddingRight)"
};
exports["default"] = exports.c_about_modal_box__header_sm_PaddingRight;
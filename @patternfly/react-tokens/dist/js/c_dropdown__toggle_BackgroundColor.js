"use strict";
exports.__esModule = true;
exports.c_dropdown__toggle_BackgroundColor = {
  "name": "--pf-c-dropdown__toggle--BackgroundColor",
  "value": "#004080",
  "var": "var(--pf-c-dropdown__toggle--BackgroundColor)"
};
exports["default"] = exports.c_dropdown__toggle_BackgroundColor;
"use strict";
exports.__esModule = true;
exports.c_badge_MinWidth = {
  "name": "--pf-c-badge--MinWidth",
  "value": "2rem",
  "var": "var(--pf-c-badge--MinWidth)"
};
exports["default"] = exports.c_badge_MinWidth;
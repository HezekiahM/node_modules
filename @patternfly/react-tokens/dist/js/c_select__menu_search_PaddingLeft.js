"use strict";
exports.__esModule = true;
exports.c_select__menu_search_PaddingLeft = {
  "name": "--pf-c-select__menu-search--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-select__menu-search--PaddingLeft)"
};
exports["default"] = exports.c_select__menu_search_PaddingLeft;
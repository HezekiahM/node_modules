"use strict";
exports.__esModule = true;
exports.c_popover__content_BackgroundColor = {
  "name": "--pf-c-popover__content--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-popover__content--BackgroundColor)"
};
exports["default"] = exports.c_popover__content_BackgroundColor;
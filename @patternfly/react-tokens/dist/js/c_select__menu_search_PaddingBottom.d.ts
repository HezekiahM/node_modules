export const c_select__menu_search_PaddingBottom: {
  "name": "--pf-c-select__menu-search--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-select__menu-search--PaddingBottom)"
};
export default c_select__menu_search_PaddingBottom;
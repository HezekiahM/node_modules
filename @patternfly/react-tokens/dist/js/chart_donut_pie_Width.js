"use strict";
exports.__esModule = true;
exports.chart_donut_pie_Width = {
  "name": "--pf-chart-donut--pie--Width",
  "value": 230,
  "var": "var(--pf-chart-donut--pie--Width)"
};
exports["default"] = exports.chart_donut_pie_Width;
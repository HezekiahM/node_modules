"use strict";
exports.__esModule = true;
exports.global_BorderWidth_lg = {
  "name": "--pf-global--BorderWidth--lg",
  "value": "3px",
  "var": "var(--pf-global--BorderWidth--lg)"
};
exports["default"] = exports.global_BorderWidth_lg;
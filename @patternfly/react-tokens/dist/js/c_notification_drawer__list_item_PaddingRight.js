"use strict";
exports.__esModule = true;
exports.c_notification_drawer__list_item_PaddingRight = {
  "name": "--pf-c-notification-drawer__list-item--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-notification-drawer__list-item--PaddingRight)"
};
exports["default"] = exports.c_notification_drawer__list_item_PaddingRight;
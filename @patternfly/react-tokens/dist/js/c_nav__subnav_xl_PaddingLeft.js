"use strict";
exports.__esModule = true;
exports.c_nav__subnav_xl_PaddingLeft = {
  "name": "--pf-c-nav__subnav--xl--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-nav__subnav--xl--PaddingLeft)"
};
exports["default"] = exports.c_nav__subnav_xl_PaddingLeft;
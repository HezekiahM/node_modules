"use strict";
exports.__esModule = true;
exports.l_grid__item_GridColumnEnd = {
  "name": "--pf-l-grid__item--GridColumnEnd",
  "value": "span 12",
  "var": "var(--pf-l-grid__item--GridColumnEnd)"
};
exports["default"] = exports.l_grid__item_GridColumnEnd;
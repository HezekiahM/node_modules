"use strict";
exports.__esModule = true;
exports.c_dropdown__group_group_PaddingTop = {
  "name": "--pf-c-dropdown__group--group--PaddingTop",
  "value": "0.5rem",
  "var": "var(--pf-c-dropdown__group--group--PaddingTop)"
};
exports["default"] = exports.c_dropdown__group_group_PaddingTop;
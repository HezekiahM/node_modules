export const c_table__sort__button_MarginTop: {
  "name": "--pf-c-table__sort__button--MarginTop",
  "value": "calc(0.375rem * -1)",
  "var": "var(--pf-c-table__sort__button--MarginTop)"
};
export default c_table__sort__button_MarginTop;
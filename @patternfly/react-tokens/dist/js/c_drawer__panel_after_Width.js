"use strict";
exports.__esModule = true;
exports.c_drawer__panel_after_Width = {
  "name": "--pf-c-drawer__panel--after--Width",
  "value": "1px",
  "var": "var(--pf-c-drawer__panel--after--Width)"
};
exports["default"] = exports.c_drawer__panel_after_Width;
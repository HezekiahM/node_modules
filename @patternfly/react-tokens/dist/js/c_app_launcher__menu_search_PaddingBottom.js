"use strict";
exports.__esModule = true;
exports.c_app_launcher__menu_search_PaddingBottom = {
  "name": "--pf-c-app-launcher__menu-search--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-app-launcher__menu-search--PaddingBottom)"
};
exports["default"] = exports.c_app_launcher__menu_search_PaddingBottom;
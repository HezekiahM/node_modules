"use strict";
exports.__esModule = true;
exports.c_app_launcher__toggle_PaddingTop = {
  "name": "--pf-c-app-launcher__toggle--PaddingTop",
  "value": "0.375rem",
  "var": "var(--pf-c-app-launcher__toggle--PaddingTop)"
};
exports["default"] = exports.c_app_launcher__toggle_PaddingTop;
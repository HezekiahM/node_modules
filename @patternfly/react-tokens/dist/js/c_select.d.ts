export const c_select: {
  ".pf-c-select": {
    "global_Color_100": {
      "name": "--pf-global--Color--100",
      "value": "#151515",
      "values": [
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "global_Color_200": {
      "name": "--pf-global--Color--200",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--dark-200",
        "$pf-global--Color--dark-200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "global_BorderColor_100": {
      "name": "--pf-global--BorderColor--100",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--dark-100",
        "$pf-global--BorderColor--dark-100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "global_primary_color_100": {
      "name": "--pf-global--primary-color--100",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--dark-100",
        "$pf-global--primary-color--dark-100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "global_link_Color": {
      "name": "--pf-global--link--Color",
      "value": "#06c",
      "values": [
        "--pf-global--link--Color--dark",
        "$pf-global--link--Color--dark",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "global_link_Color_hover": {
      "name": "--pf-global--link--Color--hover",
      "value": "#004080",
      "values": [
        "--pf-global--link--Color--dark--hover",
        "$pf-global--link--Color--dark--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "global_BackgroundColor_100": {
      "name": "--pf-global--BackgroundColor--100",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--light-100",
        "$pf-global--BackgroundColor--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_select__toggle_PaddingTop": {
      "name": "--pf-c-select__toggle--PaddingTop",
      "value": "0.375rem",
      "values": [
        "--pf-global--spacer--form-element",
        "$pf-global--spacer--form-element",
        "pf-size-prem(6px)",
        "0.375rem"
      ]
    },
    "c_select__toggle_PaddingRight": {
      "name": "--pf-c-select__toggle--PaddingRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_select__toggle_PaddingBottom": {
      "name": "--pf-c-select__toggle--PaddingBottom",
      "value": "0.375rem",
      "values": [
        "--pf-global--spacer--form-element",
        "$pf-global--spacer--form-element",
        "pf-size-prem(6px)",
        "0.375rem"
      ]
    },
    "c_select__toggle_PaddingLeft": {
      "name": "--pf-c-select__toggle--PaddingLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_select__toggle_MinWidth": {
      "name": "--pf-c-select__toggle--MinWidth",
      "value": "44px",
      "values": [
        "--pf-global--target-size--MinWidth",
        "$pf-global--target-size--MinWidth",
        "44px"
      ]
    },
    "c_select__toggle_FontSize": {
      "name": "--pf-c-select__toggle--FontSize",
      "value": "1rem",
      "values": [
        "--pf-global--FontSize--md",
        "$pf-global--FontSize--md",
        "pf-font-prem(16px)",
        "1rem"
      ]
    },
    "c_select__toggle_FontWeight": {
      "name": "--pf-c-select__toggle--FontWeight",
      "value": "400",
      "values": [
        "--pf-global--FontWeight--normal",
        "$pf-global--FontWeight--normal",
        "400"
      ]
    },
    "c_select__toggle_LineHeight": {
      "name": "--pf-c-select__toggle--LineHeight",
      "value": "1.5",
      "values": [
        "--pf-global--LineHeight--md",
        "$pf-global--LineHeight--md",
        "1.5"
      ]
    },
    "c_select__toggle_BackgroundColor": {
      "name": "--pf-c-select__toggle--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_select__toggle_before_BorderWidth": {
      "name": "--pf-c-select__toggle--before--BorderWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_select__toggle_before_BorderTopColor": {
      "name": "--pf-c-select__toggle--before--BorderTopColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_select__toggle_before_BorderRightColor": {
      "name": "--pf-c-select__toggle--before--BorderRightColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_select__toggle_before_BorderBottomColor": {
      "name": "--pf-c-select__toggle--before--BorderBottomColor",
      "value": "#8a8d90",
      "values": [
        "--pf-global--BorderColor--200",
        "$pf-global--BorderColor--200",
        "$pf-color-black-500",
        "#8a8d90"
      ]
    },
    "c_select__toggle_before_BorderLeftColor": {
      "name": "--pf-c-select__toggle--before--BorderLeftColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_select__toggle_Color": {
      "name": "--pf-c-select__toggle--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_select__toggle_hover_before_BorderBottomColor": {
      "name": "--pf-c-select__toggle--hover--before--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_select__toggle_focus_before_BorderBottomColor": {
      "name": "--pf-c-select__toggle--focus--before--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_select__toggle_active_before_BorderBottomColor": {
      "name": "--pf-c-select__toggle--active--before--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_select__toggle_m_expanded_before_BorderBottomColor": {
      "name": "--pf-c-select__toggle--m-expanded--before--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_select__toggle_focus_before_BorderBottomWidth": {
      "name": "--pf-c-select__toggle--focus--before--BorderBottomWidth",
      "value": "2px",
      "values": [
        "--pf-global--BorderWidth--md",
        "$pf-global--BorderWidth--md",
        "2px"
      ]
    },
    "c_select__toggle_active_before_BorderBottomWidth": {
      "name": "--pf-c-select__toggle--active--before--BorderBottomWidth",
      "value": "2px",
      "values": [
        "--pf-global--BorderWidth--md",
        "$pf-global--BorderWidth--md",
        "2px"
      ]
    },
    "c_select__toggle_m_expanded_before_BorderBottomWidth": {
      "name": "--pf-c-select__toggle--m-expanded--before--BorderBottomWidth",
      "value": "2px",
      "values": [
        "--pf-global--BorderWidth--md",
        "$pf-global--BorderWidth--md",
        "2px"
      ]
    },
    "c_select__toggle_disabled_BackgroundColor": {
      "name": "--pf-c-select__toggle--disabled--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--disabled-color--300",
        "$pf-global--disabled-color--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_select__toggle_m_plain_before_BorderColor": {
      "name": "--pf-c-select__toggle--m-plain--before--BorderColor",
      "value": "transparent"
    },
    "c_select__toggle_wrapper_not_last_child_MarginRight": {
      "name": "--pf-c-select__toggle-wrapper--not-last-child--MarginRight",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_select__toggle_wrapper_MaxWidth": {
      "name": "--pf-c-select__toggle-wrapper--MaxWidth",
      "value": "calc(100% - 1.5rem)",
      "values": [
        "calc(100% - --pf-global--spacer--lg)",
        "calc(100% - $pf-global--spacer--lg)",
        "calc(100% - pf-size-prem(24px))",
        "calc(100% - 1.5rem)"
      ]
    },
    "c_select__toggle_wrapper_c_chip_group_MarginTop": {
      "name": "--pf-c-select__toggle-wrapper--c-chip-group--MarginTop",
      "value": "0.3125rem"
    },
    "c_select__toggle_wrapper_c_chip_group_MarginBottom": {
      "name": "--pf-c-select__toggle-wrapper--c-chip-group--MarginBottom",
      "value": "0.3125rem"
    },
    "c_select__toggle_typeahead_FlexBasis": {
      "name": "--pf-c-select__toggle-typeahead--FlexBasis",
      "value": "10em"
    },
    "c_select__toggle_typeahead_BackgroundColor": {
      "name": "--pf-c-select__toggle-typeahead--BackgroundColor",
      "value": "transparent"
    },
    "c_select__toggle_typeahead_BorderTop": {
      "name": "--pf-c-select__toggle-typeahead--BorderTop",
      "value": "none"
    },
    "c_select__toggle_typeahead_BorderRight": {
      "name": "--pf-c-select__toggle-typeahead--BorderRight",
      "value": "none"
    },
    "c_select__toggle_typeahead_BorderLeft": {
      "name": "--pf-c-select__toggle-typeahead--BorderLeft",
      "value": "none"
    },
    "c_select__toggle_typeahead_MinWidth": {
      "name": "--pf-c-select__toggle-typeahead--MinWidth",
      "value": "7.5rem"
    },
    "c_select__toggle_typeahead_focus_PaddingBottom": {
      "name": "--pf-c-select__toggle-typeahead--focus--PaddingBottom",
      "value": "calc(0.375rem - 1px)",
      "values": [
        "calc(--pf-global--spacer--form-element - --pf-global--BorderWidth--sm)",
        "calc($pf-global--spacer--form-element - $pf-global--BorderWidth--sm)",
        "calc(pf-size-prem(6px) - 1px)",
        "calc(0.375rem - 1px)"
      ]
    },
    "c_select__toggle_icon_toggle_text_MarginLeft": {
      "name": "--pf-c-select__toggle-icon--toggle-text--MarginLeft",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_select__toggle_badge_PaddingLeft": {
      "name": "--pf-c-select__toggle-badge--PaddingLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_select__toggle_arrow_MarginLeft": {
      "name": "--pf-c-select__toggle-arrow--MarginLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_select__toggle_arrow_MarginRight": {
      "name": "--pf-c-select__toggle-arrow--MarginRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_select__toggle_arrow_with_clear_MarginLeft": {
      "name": "--pf-c-select__toggle-arrow--with-clear--MarginLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_select__toggle_arrow_m_top_m_expanded__toggle_arrow_Rotate": {
      "name": "--pf-c-select__toggle-arrow--m-top--m-expanded__toggle-arrow--Rotate",
      "value": "180deg"
    },
    "c_select__toggle_clear_PaddingRight": {
      "name": "--pf-c-select__toggle-clear--PaddingRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_select__toggle_clear_PaddingLeft": {
      "name": "--pf-c-select__toggle-clear--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_select__toggle_clear_toggle_button_PaddingLeft": {
      "name": "--pf-c-select__toggle-clear--toggle-button--PaddingLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_select__toggle_button_Color": {
      "name": "--pf-c-select__toggle-button--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_select__menu_BackgroundColor": {
      "name": "--pf-c-select__menu--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--light-100",
        "$pf-global--BackgroundColor--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_select__menu_BoxShadow": {
      "name": "--pf-c-select__menu--BoxShadow",
      "value": "0 0.25rem 0.5rem 0rem rgba(3, 3, 3, 0.12), 0 0 0.25rem 0 rgba(3, 3, 3, 0.06)",
      "values": [
        "--pf-global--BoxShadow--md",
        "$pf-global--BoxShadow--md",
        "0 pf-size-prem(4px) pf-size-prem(8px) pf-size-prem(0) rgba($pf-color-black-1000, .12), 0 0 pf-size-prem(4px) 0 rgba($pf-color-black-1000, .06)",
        "0 pf-size-prem(4px) pf-size-prem(8px) pf-size-prem(0) rgba(#030303, .12), 0 0 pf-size-prem(4px) 0 rgba(#030303, .06)",
        "0 0.25rem 0.5rem 0rem rgba(3, 3, 3, 0.12), 0 0 0.25rem 0 rgba(3, 3, 3, 0.06)"
      ]
    },
    "c_select__menu_PaddingTop": {
      "name": "--pf-c-select__menu--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_select__menu_PaddingBottom": {
      "name": "--pf-c-select__menu--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_select__menu_Top": {
      "name": "--pf-c-select__menu--Top",
      "value": "calc(100% + 0.25rem)",
      "values": [
        "calc(100% + --pf-global--spacer--xs)",
        "calc(100% + $pf-global--spacer--xs)",
        "calc(100% + pf-size-prem(4px))",
        "calc(100% + 0.25rem)"
      ]
    },
    "c_select__menu_ZIndex": {
      "name": "--pf-c-select__menu--ZIndex",
      "value": "200",
      "values": [
        "--pf-global--ZIndex--sm",
        "$pf-global--ZIndex--sm",
        "200"
      ]
    },
    "c_select__menu_m_top_TranslateY": {
      "name": "--pf-c-select__menu--m-top--TranslateY",
      "value": "calc(-100% - 0.25rem)",
      "values": [
        "calc(-100% - --pf-global--spacer--xs)",
        "calc(-100% - $pf-global--spacer--xs)",
        "calc(-100% - pf-size-prem(4px))",
        "calc(-100% - 0.25rem)"
      ]
    },
    "c_select__menu_item_PaddingTop": {
      "name": "--pf-c-select__menu-item--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_select__menu_item_PaddingRight": {
      "name": "--pf-c-select__menu-item--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_select__menu_item_m_selected_PaddingRight": {
      "name": "--pf-c-select__menu-item--m-selected--PaddingRight",
      "value": "3rem",
      "values": [
        "--pf-global--spacer--2xl",
        "$pf-global--spacer--2xl",
        "pf-size-prem(48px)",
        "3rem"
      ]
    },
    "c_select__menu_item_PaddingBottom": {
      "name": "--pf-c-select__menu-item--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_select__menu_item_PaddingLeft": {
      "name": "--pf-c-select__menu-item--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_select__menu_item_FontSize": {
      "name": "--pf-c-select__menu-item--FontSize",
      "value": "1rem",
      "values": [
        "--pf-global--FontSize--md",
        "$pf-global--FontSize--md",
        "pf-font-prem(16px)",
        "1rem"
      ]
    },
    "c_select__menu_item_FontWeight": {
      "name": "--pf-c-select__menu-item--FontWeight",
      "value": "400",
      "values": [
        "--pf-global--FontWeight--normal",
        "$pf-global--FontWeight--normal",
        "400"
      ]
    },
    "c_select__menu_item_LineHeight": {
      "name": "--pf-c-select__menu-item--LineHeight",
      "value": "1.5",
      "values": [
        "--pf-global--LineHeight--md",
        "$pf-global--LineHeight--md",
        "1.5"
      ]
    },
    "c_select__menu_item_Color": {
      "name": "--pf-c-select__menu-item--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_select__menu_item_Width": {
      "name": "--pf-c-select__menu-item--Width",
      "value": "100%"
    },
    "c_select__menu_item_disabled_Color": {
      "name": "--pf-c-select__menu-item--disabled--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--dark-200",
        "$pf-global--Color--dark-200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_select__menu_item_hover_BackgroundColor": {
      "name": "--pf-c-select__menu-item--hover--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_select__menu_item_focus_BackgroundColor": {
      "name": "--pf-c-select__menu-item--focus--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_select__menu_item_disabled_BackgroundColor": {
      "name": "--pf-c-select__menu-item--disabled--BackgroundColor",
      "value": "transparent"
    },
    "c_select__menu_item_m_link_Width": {
      "name": "--pf-c-select__menu-item--m-link--Width",
      "value": "auto"
    },
    "c_select__menu_item_m_link_hover_BackgroundColor": {
      "name": "--pf-c-select__menu-item--m-link--hover--BackgroundColor",
      "value": "transparent"
    },
    "c_select__menu_item_m_link_focus_BackgroundColor": {
      "name": "--pf-c-select__menu-item--m-link--focus--BackgroundColor",
      "value": "transparent"
    },
    "c_select__menu_item_m_action_hover_BackgroundColor": {
      "name": "--pf-c-select__menu-item--m-action--hover--BackgroundColor",
      "value": "transparent"
    },
    "c_select__menu_item_m_action_Color": {
      "name": "--pf-c-select__menu-item--m-action--Color",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--disabled-color--200",
        "$pf-global--disabled-color--200",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_select__menu_item_m_action_Width": {
      "name": "--pf-c-select__menu-item--m-action--Width",
      "value": "auto"
    },
    "c_select__menu_item_m_action_FontSize": {
      "name": "--pf-c-select__menu-item--m-action--FontSize",
      "value": "0.625rem",
      "values": [
        "--pf-global--icon--FontSize--sm",
        "$pf-global--icon--FontSize--sm",
        "pf-font-prem(10px)",
        "0.625rem"
      ]
    },
    "c_select__menu_item_hover__menu_item_m_action_Color": {
      "name": "--pf-c-select__menu-item--hover__menu-item--m-action--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--200",
        "$pf-global--Color--200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_select__menu_item_m_action_hover_Color": {
      "name": "--pf-c-select__menu-item--m-action--hover--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_select__menu_item_m_action_focus_Color": {
      "name": "--pf-c-select__menu-item--m-action--focus--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_select__menu_wrapper_m_favorite__menu_item_m_favorite_action_Color": {
      "name": "--pf-c-select__menu-wrapper--m-favorite__menu-item--m-favorite-action--Color",
      "value": "#f0ab00",
      "values": [
        "--pf-global--palette--gold-400",
        "$pf-color-gold-400",
        "#f0ab00"
      ]
    },
    "c_select__menu_item_icon_Color": {
      "name": "--pf-c-select__menu-item-icon--Color",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_select__menu_item_icon_FontSize": {
      "name": "--pf-c-select__menu-item-icon--FontSize",
      "value": "0.625rem",
      "values": [
        "--pf-global--icon--FontSize--sm",
        "$pf-global--icon--FontSize--sm",
        "pf-font-prem(10px)",
        "0.625rem"
      ]
    },
    "c_select__menu_item_icon_Right": {
      "name": "--pf-c-select__menu-item-icon--Right",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_select__menu_item_icon_Top": {
      "name": "--pf-c-select__menu-item-icon--Top",
      "value": "50%"
    },
    "c_select__menu_item_icon_TranslateY": {
      "name": "--pf-c-select__menu-item-icon--TranslateY",
      "value": "-50%"
    },
    "c_select__menu_item_action_icon_MinHeight": {
      "name": "--pf-c-select__menu-item-action-icon--MinHeight",
      "value": "calc(1rem * 1.5)",
      "values": [
        "calc(--pf-c-select__menu-item--FontSize * --pf-c-select__menu-item--LineHeight)",
        "calc(--pf-global--FontSize--md * --pf-global--LineHeight--md)",
        "calc($pf-global--FontSize--md * $pf-global--LineHeight--md)",
        "calc(pf-font-prem(16px) * 1.5)",
        "calc(1rem * 1.5)"
      ]
    },
    "c_select__menu_item_match_FontWeight": {
      "name": "--pf-c-select__menu-item--match--FontWeight",
      "value": "700",
      "values": [
        "--pf-global--FontWeight--bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    },
    "c_select__menu_search_PaddingTop": {
      "name": "--pf-c-select__menu-search--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_select__menu_search_PaddingRight": {
      "name": "--pf-c-select__menu-search--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-c-select__menu-item--PaddingRight",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_select__menu_search_PaddingBottom": {
      "name": "--pf-c-select__menu-search--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_select__menu_search_PaddingLeft": {
      "name": "--pf-c-select__menu-search--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-c-select__menu-item--PaddingLeft",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_select__menu_group_menu_group_PaddingTop": {
      "name": "--pf-c-select__menu-group--menu-group--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_select__menu_group_title_PaddingTop": {
      "name": "--pf-c-select__menu-group-title--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-c-select__menu-item--PaddingTop",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_select__menu_group_title_PaddingRight": {
      "name": "--pf-c-select__menu-group-title--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-c-select__menu-item--PaddingRight",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_select__menu_group_title_PaddingBottom": {
      "name": "--pf-c-select__menu-group-title--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-c-select__menu-item--PaddingBottom",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_select__menu_group_title_PaddingLeft": {
      "name": "--pf-c-select__menu-group-title--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-c-select__menu-item--PaddingLeft",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_select__menu_group_title_FontSize": {
      "name": "--pf-c-select__menu-group-title--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_select__menu_group_title_FontWeight": {
      "name": "--pf-c-select__menu-group-title--FontWeight",
      "value": "700",
      "values": [
        "--pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    },
    "c_select__menu_group_title_Color": {
      "name": "--pf-c-select__menu-group-title--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--dark-200",
        "$pf-global--Color--dark-200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_select__menu_item_description_FontSize": {
      "name": "--pf-c-select__menu-item-description--FontSize",
      "value": "0.75rem",
      "values": [
        "--pf-global--FontSize--xs",
        "$pf-global--FontSize--xs",
        "pf-font-prem(12px)",
        "0.75rem"
      ]
    },
    "c_select__menu_item_description_Color": {
      "name": "--pf-c-select__menu-item-description--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--200",
        "$pf-global--Color--200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_select__menu_item_description_PaddingRight": {
      "name": "--pf-c-select__menu-item-description--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-c-select__menu-item--PaddingRight",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_select__menu_item_main_PaddingRight": {
      "name": "--pf-c-select__menu-item-main--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-c-select__menu-item--PaddingRight",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_select__menu_item_m_selected__menu_item_main_PaddingRight": {
      "name": "--pf-c-select__menu-item--m-selected__menu-item-main--PaddingRight",
      "value": "3rem",
      "values": [
        "--pf-c-select__menu-item--m-selected--PaddingRight",
        "--pf-global--spacer--2xl",
        "$pf-global--spacer--2xl",
        "pf-size-prem(48px)",
        "3rem"
      ]
    },
    "c_select_menu_c_divider_MarginTop": {
      "name": "--pf-c-select-menu--c-divider--MarginTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_select_menu_c_divider_MarginBottom": {
      "name": "--pf-c-select-menu--c-divider--MarginBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-c-select .pf-c-divider:last-child": {
    "c_select_menu_c_divider_MarginBottom": {
      "name": "--pf-c-select-menu--c-divider--MarginBottom",
      "value": "0"
    }
  },
  ".pf-c-select__menu-search + .pf-c-divider": {
    "c_select_menu_c_divider_MarginTop": {
      "name": "--pf-c-select-menu--c-divider--MarginTop",
      "value": "0"
    }
  },
  ".pf-c-select__toggle.pf-m-disabled": {
    "c_select__toggle_BackgroundColor": {
      "name": "--pf-c-select__toggle--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-c-select__toggle--disabled--BackgroundColor",
        "--pf-global--disabled-color--300",
        "$pf-global--disabled-color--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    }
  },
  ".pf-c-select__toggle:hover::before": {
    "c_select__toggle_before_BorderBottomColor": {
      "name": "--pf-c-select__toggle--before--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-c-select__toggle--hover--before--BorderBottomColor",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-select__toggle:focus::before": {
    "c_select__toggle_before_BorderBottomColor": {
      "name": "--pf-c-select__toggle--before--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-c-select__toggle--focus--before--BorderBottomColor",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-select__toggle:active::before": {
    "c_select__toggle_before_BorderBottomColor": {
      "name": "--pf-c-select__toggle--before--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-c-select__toggle--active--before--BorderBottomColor",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-m-expanded > .pf-c-select__toggle::before": {
    "c_select__toggle_before_BorderBottomColor": {
      "name": "--pf-c-select__toggle--before--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-c-select__toggle--m-expanded--before--BorderBottomColor",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-select__toggle.pf-m-typeahead": {
    "c_select__toggle_PaddingTop": {
      "name": "--pf-c-select__toggle--PaddingTop",
      "value": "0"
    },
    "c_select__toggle_PaddingRight": {
      "name": "--pf-c-select__toggle--PaddingRight",
      "value": "0"
    },
    "c_select__toggle_PaddingBottom": {
      "name": "--pf-c-select__toggle--PaddingBottom",
      "value": "0"
    }
  },
  ".pf-c-select__menu-wrapper.pf-m-favorite .pf-c-select__menu-item.pf-m-favorite-action": {
    "c_select__menu_item_Color": {
      "name": "--pf-c-select__menu-item--Color",
      "value": "#f0ab00",
      "values": [
        "--pf-c-select__menu-wrapper--m-favorite__menu-item--m-favorite-action--Color",
        "--pf-global--palette--gold-400",
        "$pf-color-gold-400",
        "#f0ab00"
      ]
    }
  },
  ".pf-c-select__menu-item:hover": {
    "c_select__menu_item_m_action_Color": {
      "name": "--pf-c-select__menu-item--m-action--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-c-select__menu-item--hover__menu-item--m-action--Color",
        "--pf-global--Color--200",
        "$pf-global--Color--200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    }
  },
  ".pf-c-select__menu-item.pf-m-link": {
    "c_select__menu_item_PaddingRight": {
      "name": "--pf-c-select__menu-item--PaddingRight",
      "value": "0"
    },
    "c_select__menu_item_main_PaddingRight": {
      "name": "--pf-c-select__menu-item-main--PaddingRight",
      "value": "0"
    },
    "c_select__menu_item_description_PaddingRight": {
      "name": "--pf-c-select__menu-item-description--PaddingRight",
      "value": "0"
    },
    "c_select__menu_item_Width": {
      "name": "--pf-c-select__menu-item--Width",
      "value": "auto",
      "values": [
        "--pf-c-select__menu-item--m-link--Width",
        "auto"
      ]
    },
    "c_select__menu_item_hover_BackgroundColor": {
      "name": "--pf-c-select__menu-item--hover--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-select__menu-item--m-link--hover--BackgroundColor",
        "transparent"
      ]
    },
    "c_select__menu_item_focus_BackgroundColor": {
      "name": "--pf-c-select__menu-item--focus--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-select__menu-item--m-link--focus--BackgroundColor",
        "transparent"
      ]
    }
  },
  ".pf-c-select__menu-item.pf-m-action": {
    "c_select__menu_item_hover_BackgroundColor": {
      "name": "--pf-c-select__menu-item--hover--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-select__menu-item--m-action--hover--BackgroundColor",
        "transparent"
      ]
    },
    "c_select__menu_item_Color": {
      "name": "--pf-c-select__menu-item--Color",
      "value": "#d2d2d2",
      "values": [
        "--pf-c-select__menu-item--m-action--Color",
        "--pf-global--disabled-color--200",
        "$pf-global--disabled-color--200",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_select__menu_item_Width": {
      "name": "--pf-c-select__menu-item--Width",
      "value": "auto",
      "values": [
        "--pf-c-select__menu-item--m-action--Width",
        "auto"
      ]
    }
  },
  ".pf-c-select__menu-item.pf-m-action:hover": {
    "c_select__menu_item_m_action_Color": {
      "name": "--pf-c-select__menu-item--m-action--Color",
      "value": "#151515",
      "values": [
        "--pf-c-select__menu-item--m-action--hover--Color",
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    }
  },
  ".pf-c-select__menu-item.pf-m-action:focus": {
    "c_select__menu_item_m_action_Color": {
      "name": "--pf-c-select__menu-item--m-action--Color",
      "value": "#151515",
      "values": [
        "--pf-c-select__menu-item--m-action--focus--Color",
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    }
  },
  ".pf-c-select__menu-item.pf-m-selected": {
    "c_select__menu_item_PaddingRight": {
      "name": "--pf-c-select__menu-item--PaddingRight",
      "value": "3rem",
      "values": [
        "--pf-c-select__menu-item--m-selected--PaddingRight",
        "--pf-global--spacer--2xl",
        "$pf-global--spacer--2xl",
        "pf-size-prem(48px)",
        "3rem"
      ]
    },
    "c_select__menu_item_main_PaddingRight": {
      "name": "--pf-c-select__menu-item-main--PaddingRight",
      "value": "3rem",
      "values": [
        "--pf-c-select__menu-item--m-selected__menu-item-main--PaddingRight",
        "--pf-c-select__menu-item--m-selected--PaddingRight",
        "--pf-global--spacer--2xl",
        "$pf-global--spacer--2xl",
        "pf-size-prem(48px)",
        "3rem"
      ]
    }
  },
  ".pf-c-select__menu-item.pf-m-description:not(.pf-c-check)": {
    "c_select__menu_item_PaddingRight": {
      "name": "--pf-c-select__menu-item--PaddingRight",
      "value": "0"
    }
  }
};
export default c_select;
"use strict";
exports.__esModule = true;
exports.global_palette_gold_50 = {
  "name": "--pf-global--palette--gold-50",
  "value": "#fdf7e7",
  "var": "var(--pf-global--palette--gold-50)"
};
exports["default"] = exports.global_palette_gold_50;
"use strict";
exports.__esModule = true;
exports.c_label_m_green__content_Color = {
  "name": "--pf-c-label--m-green__content--Color",
  "value": "#0f280d",
  "var": "var(--pf-c-label--m-green__content--Color)"
};
exports["default"] = exports.c_label_m_green__content_Color;
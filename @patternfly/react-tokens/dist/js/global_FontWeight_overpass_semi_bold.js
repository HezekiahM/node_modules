"use strict";
exports.__esModule = true;
exports.global_FontWeight_overpass_semi_bold = {
  "name": "--pf-global--FontWeight--overpass--semi-bold",
  "value": "500",
  "var": "var(--pf-global--FontWeight--overpass--semi-bold)"
};
exports["default"] = exports.global_FontWeight_overpass_semi_bold;
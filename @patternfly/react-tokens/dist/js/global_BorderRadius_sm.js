"use strict";
exports.__esModule = true;
exports.global_BorderRadius_sm = {
  "name": "--pf-global--BorderRadius--sm",
  "value": "3px",
  "var": "var(--pf-global--BorderRadius--sm)"
};
exports["default"] = exports.global_BorderRadius_sm;
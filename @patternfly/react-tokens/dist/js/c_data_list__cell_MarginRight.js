"use strict";
exports.__esModule = true;
exports.c_data_list__cell_MarginRight = {
  "name": "--pf-c-data-list__cell--MarginRight",
  "value": "1rem",
  "var": "var(--pf-c-data-list__cell--MarginRight)"
};
exports["default"] = exports.c_data_list__cell_MarginRight;
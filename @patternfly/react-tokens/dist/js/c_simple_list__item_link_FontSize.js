"use strict";
exports.__esModule = true;
exports.c_simple_list__item_link_FontSize = {
  "name": "--pf-c-simple-list__item-link--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-simple-list__item-link--FontSize)"
};
exports["default"] = exports.c_simple_list__item_link_FontSize;
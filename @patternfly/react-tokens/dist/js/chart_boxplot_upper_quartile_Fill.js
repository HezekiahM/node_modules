"use strict";
exports.__esModule = true;
exports.chart_boxplot_upper_quartile_Fill = {
  "name": "--pf-chart-boxplot--upper-quartile--Fill",
  "value": "#8a8d90",
  "var": "var(--pf-chart-boxplot--upper-quartile--Fill)"
};
exports["default"] = exports.chart_boxplot_upper_quartile_Fill;
export const c_alert__action_group_PaddingTop: {
  "name": "--pf-c-alert__action-group--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-alert__action-group--PaddingTop)"
};
export default c_alert__action_group_PaddingTop;
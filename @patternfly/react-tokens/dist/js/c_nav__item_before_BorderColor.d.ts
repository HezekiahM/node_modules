export const c_nav__item_before_BorderColor: {
  "name": "--pf-c-nav__item--before--BorderColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-nav__item--before--BorderColor)"
};
export default c_nav__item_before_BorderColor;
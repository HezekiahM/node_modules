"use strict";
exports.__esModule = true;
exports.c_nav_m_tertiary__scroll_button_Color = {
  "name": "--pf-c-nav--m-tertiary__scroll-button--Color",
  "value": "#151515",
  "var": "var(--pf-c-nav--m-tertiary__scroll-button--Color)"
};
exports["default"] = exports.c_nav_m_tertiary__scroll_button_Color;
"use strict";
exports.__esModule = true;
exports.chart_voronoi_labels_PointerEvents = {
  "name": "--pf-chart-voronoi--labels--PointerEvents",
  "value": "none",
  "var": "var(--pf-chart-voronoi--labels--PointerEvents)"
};
exports["default"] = exports.chart_voronoi_labels_PointerEvents;
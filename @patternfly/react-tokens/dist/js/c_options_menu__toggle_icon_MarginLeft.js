"use strict";
exports.__esModule = true;
exports.c_options_menu__toggle_icon_MarginLeft = {
  "name": "--pf-c-options-menu__toggle-icon--MarginLeft",
  "value": "1rem",
  "var": "var(--pf-c-options-menu__toggle-icon--MarginLeft)"
};
exports["default"] = exports.c_options_menu__toggle_icon_MarginLeft;
"use strict";
exports.__esModule = true;
exports.c_tabs__scroll_button_before_BorderColor = {
  "name": "--pf-c-tabs__scroll-button--before--BorderColor",
  "value": "#d2d2d2",
  "var": "var(--pf-c-tabs__scroll-button--before--BorderColor)"
};
exports["default"] = exports.c_tabs__scroll_button_before_BorderColor;
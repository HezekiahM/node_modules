"use strict";
exports.__esModule = true;
exports.c_notification_drawer__list_item_BackgroundColor = {
  "name": "--pf-c-notification-drawer__list-item--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-notification-drawer__list-item--BackgroundColor)"
};
exports["default"] = exports.c_notification_drawer__list_item_BackgroundColor;
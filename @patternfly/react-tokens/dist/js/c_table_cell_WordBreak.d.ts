export const c_table_cell_WordBreak: {
  "name": "--pf-c-table--cell--WordBreak",
  "value": "break-word",
  "var": "var(--pf-c-table--cell--WordBreak)"
};
export default c_table_cell_WordBreak;
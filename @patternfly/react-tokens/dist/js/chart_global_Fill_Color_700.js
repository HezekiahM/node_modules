"use strict";
exports.__esModule = true;
exports.chart_global_Fill_Color_700 = {
  "name": "--pf-chart-global--Fill--Color--700",
  "value": "#4f5255",
  "var": "var(--pf-chart-global--Fill--Color--700)"
};
exports["default"] = exports.chart_global_Fill_Color_700;
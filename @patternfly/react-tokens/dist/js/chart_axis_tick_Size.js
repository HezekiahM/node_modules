"use strict";
exports.__esModule = true;
exports.chart_axis_tick_Size = {
  "name": "--pf-chart-axis--tick--Size",
  "value": 5,
  "var": "var(--pf-chart-axis--tick--Size)"
};
exports["default"] = exports.chart_axis_tick_Size;
export const c_select__menu_item_main_PaddingRight: {
  "name": "--pf-c-select__menu-item-main--PaddingRight",
  "value": "3rem",
  "var": "var(--pf-c-select__menu-item-main--PaddingRight)"
};
export default c_select__menu_item_main_PaddingRight;
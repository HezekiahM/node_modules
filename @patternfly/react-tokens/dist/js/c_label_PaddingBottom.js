"use strict";
exports.__esModule = true;
exports.c_label_PaddingBottom = {
  "name": "--pf-c-label--PaddingBottom",
  "value": "0.25rem",
  "var": "var(--pf-c-label--PaddingBottom)"
};
exports["default"] = exports.c_label_PaddingBottom;
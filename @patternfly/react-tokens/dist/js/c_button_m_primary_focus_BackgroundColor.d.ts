export const c_button_m_primary_focus_BackgroundColor: {
  "name": "--pf-c-button--m-primary--focus--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-button--m-primary--focus--BackgroundColor)"
};
export default c_button_m_primary_focus_BackgroundColor;
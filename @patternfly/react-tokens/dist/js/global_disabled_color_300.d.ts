export const global_disabled_color_300: {
  "name": "--pf-global--disabled-color--300",
  "value": "#f0f0f0",
  "var": "var(--pf-global--disabled-color--300)"
};
export default global_disabled_color_300;
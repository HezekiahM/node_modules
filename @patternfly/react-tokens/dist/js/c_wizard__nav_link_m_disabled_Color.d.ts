export const c_wizard__nav_link_m_disabled_Color: {
  "name": "--pf-c-wizard__nav-link--m-disabled--Color",
  "value": "#6a6e73",
  "var": "var(--pf-c-wizard__nav-link--m-disabled--Color)"
};
export default c_wizard__nav_link_m_disabled_Color;
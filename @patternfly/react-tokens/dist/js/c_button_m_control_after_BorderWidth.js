"use strict";
exports.__esModule = true;
exports.c_button_m_control_after_BorderWidth = {
  "name": "--pf-c-button--m-control--after--BorderWidth",
  "value": "1px",
  "var": "var(--pf-c-button--m-control--after--BorderWidth)"
};
exports["default"] = exports.c_button_m_control_after_BorderWidth;
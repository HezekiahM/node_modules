"use strict";
exports.__esModule = true;
exports.c_form_control_m_search_BackgroundPosition = {
  "name": "--pf-c-form-control--m-search--BackgroundPosition",
  "value": "0.5rem",
  "var": "var(--pf-c-form-control--m-search--BackgroundPosition)"
};
exports["default"] = exports.c_form_control_m_search_BackgroundPosition;
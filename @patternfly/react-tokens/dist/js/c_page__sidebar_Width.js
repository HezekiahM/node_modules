"use strict";
exports.__esModule = true;
exports.c_page__sidebar_Width = {
  "name": "--pf-c-page__sidebar--Width",
  "value": "18.125rem",
  "var": "var(--pf-c-page__sidebar--Width)"
};
exports["default"] = exports.c_page__sidebar_Width;
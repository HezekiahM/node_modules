"use strict";
exports.__esModule = true;
exports.c_popover__arrow_m_right_TranslateY = {
  "name": "--pf-c-popover__arrow--m-right--TranslateY",
  "value": "-50%",
  "var": "var(--pf-c-popover__arrow--m-right--TranslateY)"
};
exports["default"] = exports.c_popover__arrow_m_right_TranslateY;
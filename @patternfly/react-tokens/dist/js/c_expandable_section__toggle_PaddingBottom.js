"use strict";
exports.__esModule = true;
exports.c_expandable_section__toggle_PaddingBottom = {
  "name": "--pf-c-expandable-section__toggle--PaddingBottom",
  "value": "0.375rem",
  "var": "var(--pf-c-expandable-section__toggle--PaddingBottom)"
};
exports["default"] = exports.c_expandable_section__toggle_PaddingBottom;
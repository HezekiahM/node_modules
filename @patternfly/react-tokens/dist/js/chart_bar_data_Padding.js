"use strict";
exports.__esModule = true;
exports.chart_bar_data_Padding = {
  "name": "--pf-chart-bar--data--Padding",
  "value": 8,
  "var": "var(--pf-chart-bar--data--Padding)"
};
exports["default"] = exports.chart_bar_data_Padding;
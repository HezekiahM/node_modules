export const c_nav_m_tertiary__link_PaddingLeft: {
  "name": "--pf-c-nav--m-tertiary__link--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-nav--m-tertiary__link--PaddingLeft)"
};
export default c_nav_m_tertiary__link_PaddingLeft;
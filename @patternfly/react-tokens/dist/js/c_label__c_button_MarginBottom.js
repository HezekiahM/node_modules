"use strict";
exports.__esModule = true;
exports.c_label__c_button_MarginBottom = {
  "name": "--pf-c-label__c-button--MarginBottom",
  "value": "calc(0.375rem * -1)",
  "var": "var(--pf-c-label__c-button--MarginBottom)"
};
exports["default"] = exports.c_label__c_button_MarginBottom;
"use strict";
exports.__esModule = true;
exports.c_tooltip_MaxWidth = {
  "name": "--pf-c-tooltip--MaxWidth",
  "value": "18.75rem",
  "var": "var(--pf-c-tooltip--MaxWidth)"
};
exports["default"] = exports.c_tooltip_MaxWidth;
export const c_table__expandable_row_content_responsive_xl_PaddingRight: {
  "name": "--pf-c-table__expandable-row-content--responsive--xl--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-table__expandable-row-content--responsive--xl--PaddingRight)"
};
export default c_table__expandable_row_content_responsive_xl_PaddingRight;
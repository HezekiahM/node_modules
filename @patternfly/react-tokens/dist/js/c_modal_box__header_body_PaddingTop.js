"use strict";
exports.__esModule = true;
exports.c_modal_box__header_body_PaddingTop = {
  "name": "--pf-c-modal-box__header--body--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-modal-box__header--body--PaddingTop)"
};
exports["default"] = exports.c_modal_box__header_body_PaddingTop;
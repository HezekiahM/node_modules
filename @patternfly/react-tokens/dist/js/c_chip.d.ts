export const c_chip: {
  ".pf-c-chip": {
    "global_Color_100": {
      "name": "--pf-global--Color--100",
      "value": "#151515",
      "values": [
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "global_Color_200": {
      "name": "--pf-global--Color--200",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--dark-200",
        "$pf-global--Color--dark-200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "global_BorderColor_100": {
      "name": "--pf-global--BorderColor--100",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--dark-100",
        "$pf-global--BorderColor--dark-100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "global_primary_color_100": {
      "name": "--pf-global--primary-color--100",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--dark-100",
        "$pf-global--primary-color--dark-100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "global_link_Color": {
      "name": "--pf-global--link--Color",
      "value": "#06c",
      "values": [
        "--pf-global--link--Color--dark",
        "$pf-global--link--Color--dark",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "global_link_Color_hover": {
      "name": "--pf-global--link--Color--hover",
      "value": "#004080",
      "values": [
        "--pf-global--link--Color--dark--hover",
        "$pf-global--link--Color--dark--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "global_BackgroundColor_100": {
      "name": "--pf-global--BackgroundColor--100",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--light-100",
        "$pf-global--BackgroundColor--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_chip_PaddingTop": {
      "name": "--pf-c-chip--PaddingTop",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_chip_PaddingRight": {
      "name": "--pf-c-chip--PaddingRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_chip_PaddingBottom": {
      "name": "--pf-c-chip--PaddingBottom",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_chip_PaddingLeft": {
      "name": "--pf-c-chip--PaddingLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_chip_BackgroundColor": {
      "name": "--pf-c-chip--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_chip_BorderRadius": {
      "name": "--pf-c-chip--BorderRadius",
      "value": "3px",
      "values": [
        "--pf-global--BorderRadius--sm",
        "$pf-global--BorderRadius--sm",
        "3px"
      ]
    },
    "c_chip_before_BorderColor": {
      "name": "--pf-c-chip--before--BorderColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_chip_before_BorderWidth": {
      "name": "--pf-c-chip--before--BorderWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_chip_before_BorderRadius": {
      "name": "--pf-c-chip--before--BorderRadius",
      "value": "3px",
      "values": [
        "--pf-c-chip--BorderRadius",
        "--pf-global--BorderRadius--sm",
        "$pf-global--BorderRadius--sm",
        "3px"
      ]
    },
    "c_chip_m_overflow__text_Color": {
      "name": "--pf-c-chip--m-overflow__text--Color",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_chip__text_FontSize": {
      "name": "--pf-c-chip__text--FontSize",
      "value": "0.75rem",
      "values": [
        "--pf-global--FontSize--xs",
        "$pf-global--FontSize--xs",
        "pf-font-prem(12px)",
        "0.75rem"
      ]
    },
    "c_chip__text_Color": {
      "name": "--pf-c-chip__text--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_chip__text_MaxWidth": {
      "name": "--pf-c-chip__text--MaxWidth",
      "value": "16ch"
    },
    "c_chip__c_button_PaddingTop": {
      "name": "--pf-c-chip__c-button--PaddingTop",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_chip__c_button_PaddingRight": {
      "name": "--pf-c-chip__c-button--PaddingRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_chip__c_button_PaddingBottom": {
      "name": "--pf-c-chip__c-button--PaddingBottom",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_chip__c_button_PaddingLeft": {
      "name": "--pf-c-chip__c-button--PaddingLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_chip__c_button_MarginTop": {
      "name": "--pf-c-chip__c-button--MarginTop",
      "value": "calc(0.25rem * -1)",
      "values": [
        "calc(--pf-c-chip--PaddingTop * -1)",
        "calc(--pf-global--spacer--xs * -1)",
        "calc($pf-global--spacer--xs * -1)",
        "calc(pf-size-prem(4px) * -1)",
        "calc(0.25rem * -1)"
      ]
    },
    "c_chip__c_button_MarginRight": {
      "name": "--pf-c-chip__c-button--MarginRight",
      "value": "calc(0.5rem / 2 * -1)",
      "values": [
        "calc(--pf-c-chip--PaddingRight / 2 * -1)",
        "calc(--pf-global--spacer--sm / 2 * -1)",
        "calc($pf-global--spacer--sm / 2 * -1)",
        "calc(pf-size-prem(8px) / 2 * -1)",
        "calc(0.5rem / 2 * -1)"
      ]
    },
    "c_chip__c_button_MarginBottom": {
      "name": "--pf-c-chip__c-button--MarginBottom",
      "value": "calc(0.25rem * -1)",
      "values": [
        "calc(--pf-c-chip--PaddingBottom * -1)",
        "calc(--pf-global--spacer--xs * -1)",
        "calc($pf-global--spacer--xs * -1)",
        "calc(pf-size-prem(4px) * -1)",
        "calc(0.25rem * -1)"
      ]
    },
    "c_chip__c_button_FontSize": {
      "name": "--pf-c-chip__c-button--FontSize",
      "value": "0.75rem",
      "values": [
        "--pf-global--FontSize--xs",
        "$pf-global--FontSize--xs",
        "pf-font-prem(12px)",
        "0.75rem"
      ]
    },
    "c_chip__c_badge_MarginLeft": {
      "name": "--pf-c-chip__c-badge--MarginLeft",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    }
  },
  ".pf-c-chip .pf-c-button": {
    "c_button_PaddingTop": {
      "name": "--pf-c-button--PaddingTop",
      "value": "0.25rem",
      "values": [
        "--pf-c-chip__c-button--PaddingTop",
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_button_PaddingRight": {
      "name": "--pf-c-button--PaddingRight",
      "value": "0.5rem",
      "values": [
        "--pf-c-chip__c-button--PaddingRight",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_button_PaddingBottom": {
      "name": "--pf-c-button--PaddingBottom",
      "value": "0.25rem",
      "values": [
        "--pf-c-chip__c-button--PaddingBottom",
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_button_PaddingLeft": {
      "name": "--pf-c-button--PaddingLeft",
      "value": "0.5rem",
      "values": [
        "--pf-c-chip__c-button--PaddingLeft",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_button_FontSize": {
      "name": "--pf-c-button--FontSize",
      "value": "0.75rem",
      "values": [
        "--pf-c-chip__c-button--FontSize",
        "--pf-global--FontSize--xs",
        "$pf-global--FontSize--xs",
        "pf-font-prem(12px)",
        "0.75rem"
      ]
    }
  }
};
export default c_chip;
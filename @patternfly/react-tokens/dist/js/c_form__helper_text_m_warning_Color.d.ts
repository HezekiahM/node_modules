export const c_form__helper_text_m_warning_Color: {
  "name": "--pf-c-form__helper-text--m-warning--Color",
  "value": "#795600",
  "var": "var(--pf-c-form__helper-text--m-warning--Color)"
};
export default c_form__helper_text_m_warning_Color;
"use strict";
exports.__esModule = true;
exports.c_options_menu__group_title_PaddingLeft = {
  "name": "--pf-c-options-menu__group-title--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-options-menu__group-title--PaddingLeft)"
};
exports["default"] = exports.c_options_menu__group_title_PaddingLeft;
"use strict";
exports.__esModule = true;
exports.c_page__main_breadcrumb_BackgroundColor = {
  "name": "--pf-c-page__main-breadcrumb--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-page__main-breadcrumb--BackgroundColor)"
};
exports["default"] = exports.c_page__main_breadcrumb_BackgroundColor;
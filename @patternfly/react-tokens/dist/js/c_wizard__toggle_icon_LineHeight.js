"use strict";
exports.__esModule = true;
exports.c_wizard__toggle_icon_LineHeight = {
  "name": "--pf-c-wizard__toggle-icon--LineHeight",
  "value": "1.5",
  "var": "var(--pf-c-wizard__toggle-icon--LineHeight)"
};
exports["default"] = exports.c_wizard__toggle_icon_LineHeight;
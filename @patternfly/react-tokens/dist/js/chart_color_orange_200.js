"use strict";
exports.__esModule = true;
exports.chart_color_orange_200 = {
  "name": "--pf-chart-color-orange-200",
  "value": "#ef9234",
  "var": "var(--pf-chart-color-orange-200)"
};
exports["default"] = exports.chart_color_orange_200;
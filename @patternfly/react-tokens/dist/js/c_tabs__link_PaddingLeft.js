"use strict";
exports.__esModule = true;
exports.c_tabs__link_PaddingLeft = {
  "name": "--pf-c-tabs__link--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-tabs__link--PaddingLeft)"
};
exports["default"] = exports.c_tabs__link_PaddingLeft;
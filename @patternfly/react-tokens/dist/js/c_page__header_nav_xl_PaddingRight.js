"use strict";
exports.__esModule = true;
exports.c_page__header_nav_xl_PaddingRight = {
  "name": "--pf-c-page__header-nav--xl--PaddingRight",
  "value": "2rem",
  "var": "var(--pf-c-page__header-nav--xl--PaddingRight)"
};
exports["default"] = exports.c_page__header_nav_xl_PaddingRight;
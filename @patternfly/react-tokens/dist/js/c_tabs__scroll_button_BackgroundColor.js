"use strict";
exports.__esModule = true;
exports.c_tabs__scroll_button_BackgroundColor = {
  "name": "--pf-c-tabs__scroll-button--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-tabs__scroll-button--BackgroundColor)"
};
exports["default"] = exports.c_tabs__scroll_button_BackgroundColor;
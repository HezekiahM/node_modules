export const c_about_modal_box: {
  ".pf-c-about-modal-box": {
    "global_Color_100": {
      "name": "--pf-global--Color--100",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "global_Color_200": {
      "name": "--pf-global--Color--200",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--Color--light-200",
        "$pf-global--Color--light-200",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "global_BorderColor_100": {
      "name": "--pf-global--BorderColor--100",
      "value": "#b8bbbe",
      "values": [
        "--pf-global--BorderColor--light-100",
        "$pf-global--BorderColor--light-100",
        "$pf-color-black-400",
        "#b8bbbe"
      ]
    },
    "global_primary_color_100": {
      "name": "--pf-global--primary-color--100",
      "value": "#73bcf7",
      "values": [
        "--pf-global--primary-color--light-100",
        "$pf-global--primary-color--light-100",
        "$pf-color-blue-200",
        "#73bcf7"
      ]
    },
    "global_link_Color": {
      "name": "--pf-global--link--Color",
      "value": "#73bcf7",
      "values": [
        "--pf-global--link--Color--light",
        "$pf-global--link--Color--light",
        "$pf-global--active-color--300",
        "$pf-color-blue-200",
        "#73bcf7"
      ]
    },
    "global_link_Color_hover": {
      "name": "--pf-global--link--Color--hover",
      "value": "#73bcf7",
      "values": [
        "--pf-global--link--Color--light",
        "$pf-global--link--Color--light",
        "$pf-global--active-color--300",
        "$pf-color-blue-200",
        "#73bcf7"
      ]
    },
    "global_BackgroundColor_100": {
      "name": "--pf-global--BackgroundColor--100",
      "value": "#151515",
      "values": [
        "--pf-global--BackgroundColor--dark-100",
        "$pf-global--BackgroundColor--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_about_modal_box_BackgroundColor": {
      "name": "--pf-c-about-modal-box--BackgroundColor",
      "value": "#030303",
      "values": [
        "--pf-global--palette--black-1000",
        "$pf-color-black-1000",
        "#030303"
      ]
    },
    "c_about_modal_box_BoxShadow": {
      "name": "--pf-c-about-modal-box--BoxShadow",
      "value": "0 0 100px 0 rgba(255, 255, 255, .05)"
    },
    "c_about_modal_box_ZIndex": {
      "name": "--pf-c-about-modal-box--ZIndex",
      "value": "500",
      "values": [
        "--pf-global--ZIndex--xl",
        "$pf-global--ZIndex--xl",
        "500"
      ]
    },
    "c_about_modal_box_Height": {
      "name": "--pf-c-about-modal-box--Height",
      "value": "100%"
    },
    "c_about_modal_box_lg_Height": {
      "name": "--pf-c-about-modal-box--lg--Height",
      "value": "47.625rem"
    },
    "c_about_modal_box_lg_MaxHeight": {
      "name": "--pf-c-about-modal-box--lg--MaxHeight",
      "value": "calc(100% - 2rem)",
      "values": [
        "calc(100% - --pf-global--spacer--xl)",
        "calc(100% - $pf-global--spacer--xl)",
        "calc(100% - pf-size-prem(32px))",
        "calc(100% - 2rem)"
      ]
    },
    "c_about_modal_box_Width": {
      "name": "--pf-c-about-modal-box--Width",
      "value": "100vw"
    },
    "c_about_modal_box_lg_Width": {
      "name": "--pf-c-about-modal-box--lg--Width",
      "value": "calc(100% - (4rem * 2))",
      "values": [
        "calc(100% - (--pf-global--spacer--3xl * 2))",
        "calc(100% - ($pf-global--spacer--3xl * 2))",
        "calc(100% - (pf-size-prem(64px) * 2))",
        "calc(100% - (4rem * 2))"
      ]
    },
    "c_about_modal_box_lg_MaxWidth": {
      "name": "--pf-c-about-modal-box--lg--MaxWidth",
      "value": "77rem"
    },
    "c_about_modal_box_PaddingTop": {
      "name": "--pf-c-about-modal-box--PaddingTop",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_about_modal_box_PaddingRight": {
      "name": "--pf-c-about-modal-box--PaddingRight",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_about_modal_box_PaddingBottom": {
      "name": "--pf-c-about-modal-box--PaddingBottom",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_about_modal_box_PaddingLeft": {
      "name": "--pf-c-about-modal-box--PaddingLeft",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_about_modal_box_sm_PaddingTop": {
      "name": "--pf-c-about-modal-box--sm--PaddingTop",
      "value": "4rem",
      "values": [
        "--pf-global--spacer--3xl",
        "$pf-global--spacer--3xl",
        "pf-size-prem(64px)",
        "4rem"
      ]
    },
    "c_about_modal_box_sm_PaddingRight": {
      "name": "--pf-c-about-modal-box--sm--PaddingRight",
      "value": "4rem",
      "values": [
        "--pf-global--spacer--3xl",
        "$pf-global--spacer--3xl",
        "pf-size-prem(64px)",
        "4rem"
      ]
    },
    "c_about_modal_box_sm_PaddingBottom": {
      "name": "--pf-c-about-modal-box--sm--PaddingBottom",
      "value": "4rem",
      "values": [
        "--pf-global--spacer--3xl",
        "$pf-global--spacer--3xl",
        "pf-size-prem(64px)",
        "4rem"
      ]
    },
    "c_about_modal_box_sm_PaddingLeft": {
      "name": "--pf-c-about-modal-box--sm--PaddingLeft",
      "value": "4rem",
      "values": [
        "--pf-global--spacer--3xl",
        "$pf-global--spacer--3xl",
        "pf-size-prem(64px)",
        "4rem"
      ]
    },
    "c_about_modal_box_sm_grid_template_columns": {
      "name": "--pf-c-about-modal-box--sm--grid-template-columns",
      "value": "5fr 1fr"
    },
    "c_about_modal_box_lg_grid_template_columns": {
      "name": "--pf-c-about-modal-box--lg--grid-template-columns",
      "value": "1fr .6fr"
    },
    "c_about_modal_box__brand_PaddingTop": {
      "name": "--pf-c-about-modal-box__brand--PaddingTop",
      "value": "3rem",
      "values": [
        "--pf-global--spacer--2xl",
        "$pf-global--spacer--2xl",
        "pf-size-prem(48px)",
        "3rem"
      ]
    },
    "c_about_modal_box__brand_PaddingRight": {
      "name": "--pf-c-about-modal-box__brand--PaddingRight",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_about_modal_box__brand_PaddingLeft": {
      "name": "--pf-c-about-modal-box__brand--PaddingLeft",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_about_modal_box__brand_PaddingBottom": {
      "name": "--pf-c-about-modal-box__brand--PaddingBottom",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_about_modal_box__brand_sm_PaddingRight": {
      "name": "--pf-c-about-modal-box__brand--sm--PaddingRight",
      "value": "4rem",
      "values": [
        "--pf-global--spacer--3xl",
        "$pf-global--spacer--3xl",
        "pf-size-prem(64px)",
        "4rem"
      ]
    },
    "c_about_modal_box__brand_sm_PaddingLeft": {
      "name": "--pf-c-about-modal-box__brand--sm--PaddingLeft",
      "value": "4rem",
      "values": [
        "--pf-global--spacer--3xl",
        "$pf-global--spacer--3xl",
        "pf-size-prem(64px)",
        "4rem"
      ]
    },
    "c_about_modal_box__brand_sm_PaddingBottom": {
      "name": "--pf-c-about-modal-box__brand--sm--PaddingBottom",
      "value": "4rem",
      "values": [
        "--pf-global--spacer--3xl",
        "$pf-global--spacer--3xl",
        "pf-size-prem(64px)",
        "4rem"
      ]
    },
    "c_about_modal_box__close_ZIndex": {
      "name": "--pf-c-about-modal-box__close--ZIndex",
      "value": "600",
      "values": [
        "--pf-global--ZIndex--2xl",
        "$pf-global--ZIndex--2xl",
        "600"
      ]
    },
    "c_about_modal_box__close_PaddingTop": {
      "name": "--pf-c-about-modal-box__close--PaddingTop",
      "value": "3rem",
      "values": [
        "--pf-global--spacer--2xl",
        "$pf-global--spacer--2xl",
        "pf-size-prem(48px)",
        "3rem"
      ]
    },
    "c_about_modal_box__close_PaddingRight": {
      "name": "--pf-c-about-modal-box__close--PaddingRight",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_about_modal_box__close_PaddingBottom": {
      "name": "--pf-c-about-modal-box__close--PaddingBottom",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_about_modal_box__close_sm_PaddingBottom": {
      "name": "--pf-c-about-modal-box__close--sm--PaddingBottom",
      "value": "4rem",
      "values": [
        "--pf-global--spacer--3xl",
        "$pf-global--spacer--3xl",
        "pf-size-prem(64px)",
        "4rem"
      ]
    },
    "c_about_modal_box__close_sm_PaddingRight": {
      "name": "--pf-c-about-modal-box__close--sm--PaddingRight",
      "value": "0"
    },
    "c_about_modal_box__close_lg_PaddingRight": {
      "name": "--pf-c-about-modal-box__close--lg--PaddingRight",
      "value": "4rem",
      "values": [
        "--pf-global--spacer--3xl",
        "$pf-global--spacer--3xl",
        "pf-size-prem(64px)",
        "4rem"
      ]
    },
    "c_about_modal_box__close_c_button_Color": {
      "name": "--pf-c-about-modal-box__close--c-button--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_about_modal_box__close_c_button_FontSize": {
      "name": "--pf-c-about-modal-box__close--c-button--FontSize",
      "value": "1.25rem",
      "values": [
        "--pf-global--FontSize--xl",
        "$pf-global--FontSize--xl",
        "pf-font-prem(20px)",
        "1.25rem"
      ]
    },
    "c_about_modal_box__close_c_button_BorderRadius": {
      "name": "--pf-c-about-modal-box__close--c-button--BorderRadius",
      "value": "30em",
      "values": [
        "--pf-global--BorderRadius--lg",
        "$pf-global--BorderRadius--lg",
        "30em"
      ]
    },
    "c_about_modal_box__close_c_button_Width": {
      "name": "--pf-c-about-modal-box__close--c-button--Width",
      "value": "calc(1.25rem * 2)",
      "values": [
        "calc(--pf-c-about-modal-box__close--c-button--FontSize * 2)",
        "calc(--pf-global--FontSize--xl * 2)",
        "calc($pf-global--FontSize--xl * 2)",
        "calc(pf-font-prem(20px) * 2)",
        "calc(1.25rem * 2)"
      ]
    },
    "c_about_modal_box__close_c_button_Height": {
      "name": "--pf-c-about-modal-box__close--c-button--Height",
      "value": "calc(1.25rem * 2)",
      "values": [
        "calc(--pf-c-about-modal-box__close--c-button--FontSize * 2)",
        "calc(--pf-global--FontSize--xl * 2)",
        "calc($pf-global--FontSize--xl * 2)",
        "calc(pf-font-prem(20px) * 2)",
        "calc(1.25rem * 2)"
      ]
    },
    "c_about_modal_box__close_c_button_BackgroundColor": {
      "name": "--pf-c-about-modal-box__close--c-button--BackgroundColor",
      "value": "#030303",
      "values": [
        "--pf-global--palette--black-1000",
        "$pf-color-black-1000",
        "#030303"
      ]
    },
    "c_about_modal_box__close_c_button_hover_BackgroundColor": {
      "name": "--pf-c-about-modal-box__close--c-button--hover--BackgroundColor",
      "value": "rgba(3, 3, 3, 0.4)"
    },
    "c_about_modal_box__hero_sm_BackgroundImage": {
      "name": "--pf-c-about-modal-box__hero--sm--BackgroundImage",
      "value": "url(\"../../assets/images/pfbg_992@2x.jpg\")"
    },
    "c_about_modal_box__hero_sm_BackgroundPosition": {
      "name": "--pf-c-about-modal-box__hero--sm--BackgroundPosition",
      "value": "top left"
    },
    "c_about_modal_box__hero_sm_BackgroundSize": {
      "name": "--pf-c-about-modal-box__hero--sm--BackgroundSize",
      "value": "cover"
    },
    "c_about_modal_box__brand_image_Height": {
      "name": "--pf-c-about-modal-box__brand-image--Height",
      "value": "2.5rem"
    },
    "c_about_modal_box__header_PaddingRight": {
      "name": "--pf-c-about-modal-box__header--PaddingRight",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_about_modal_box__header_PaddingBottom": {
      "name": "--pf-c-about-modal-box__header--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_about_modal_box__header_PaddingLeft": {
      "name": "--pf-c-about-modal-box__header--PaddingLeft",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_about_modal_box__header_sm_PaddingRight": {
      "name": "--pf-c-about-modal-box__header--sm--PaddingRight",
      "value": "4rem",
      "values": [
        "--pf-global--spacer--3xl",
        "$pf-global--spacer--3xl",
        "pf-size-prem(64px)",
        "4rem"
      ]
    },
    "c_about_modal_box__header_sm_PaddingLeft": {
      "name": "--pf-c-about-modal-box__header--sm--PaddingLeft",
      "value": "4rem",
      "values": [
        "--pf-global--spacer--3xl",
        "$pf-global--spacer--3xl",
        "pf-size-prem(64px)",
        "4rem"
      ]
    },
    "c_about_modal_box__strapline_PaddingTop": {
      "name": "--pf-c-about-modal-box__strapline--PaddingTop",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_about_modal_box__strapline_FontSize": {
      "name": "--pf-c-about-modal-box__strapline--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_about_modal_box__strapline_sm_PaddingTop": {
      "name": "--pf-c-about-modal-box__strapline--sm--PaddingTop",
      "value": "3rem",
      "values": [
        "--pf-global--spacer--2xl",
        "$pf-global--spacer--2xl",
        "pf-size-prem(48px)",
        "3rem"
      ]
    },
    "c_about_modal_box__content_MarginTop": {
      "name": "--pf-c-about-modal-box__content--MarginTop",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_about_modal_box__content_MarginRight": {
      "name": "--pf-c-about-modal-box__content--MarginRight",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_about_modal_box__content_MarginBottom": {
      "name": "--pf-c-about-modal-box__content--MarginBottom",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_about_modal_box__content_MarginLeft": {
      "name": "--pf-c-about-modal-box__content--MarginLeft",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_about_modal_box__content_sm_MarginTop": {
      "name": "--pf-c-about-modal-box__content--sm--MarginTop",
      "value": "3rem",
      "values": [
        "--pf-global--spacer--2xl",
        "$pf-global--spacer--2xl",
        "pf-size-prem(48px)",
        "3rem"
      ]
    },
    "c_about_modal_box__content_sm_MarginRight": {
      "name": "--pf-c-about-modal-box__content--sm--MarginRight",
      "value": "4rem",
      "values": [
        "--pf-global--spacer--3xl",
        "$pf-global--spacer--3xl",
        "pf-size-prem(64px)",
        "4rem"
      ]
    },
    "c_about_modal_box__content_sm_MarginBottom": {
      "name": "--pf-c-about-modal-box__content--sm--MarginBottom",
      "value": "3rem",
      "values": [
        "--pf-global--spacer--2xl",
        "$pf-global--spacer--2xl",
        "pf-size-prem(48px)",
        "3rem"
      ]
    },
    "c_about_modal_box__content_sm_MarginLeft": {
      "name": "--pf-c-about-modal-box__content--sm--MarginLeft",
      "value": "4rem",
      "values": [
        "--pf-global--spacer--3xl",
        "$pf-global--spacer--3xl",
        "pf-size-prem(64px)",
        "4rem"
      ]
    }
  },
  ".pf-c-about-modal-box .pf-c-card": {
    "c_card_BackgroundColor": {
      "name": "--pf-c-card--BackgroundColor",
      "value": "rgba(#030303, .32)",
      "values": [
        "--pf-global--BackgroundColor--dark-transparent-200",
        "$pf-global--BackgroundColor--dark-transparent-200",
        "rgba($pf-color-black-1000, .32)",
        "rgba(#030303, .32)"
      ]
    }
  },
  ".pf-c-about-modal-box .pf-c-button": {
    "c_button_m_primary_Color": {
      "name": "--pf-c-button--m-primary--Color",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--dark-100",
        "$pf-global--primary-color--dark-100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_primary_hover_Color": {
      "name": "--pf-c-button--m-primary--hover--Color",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--dark-100",
        "$pf-global--primary-color--dark-100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_primary_focus_Color": {
      "name": "--pf-c-button--m-primary--focus--Color",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--dark-100",
        "$pf-global--primary-color--dark-100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_primary_active_Color": {
      "name": "--pf-c-button--m-primary--active--Color",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--dark-100",
        "$pf-global--primary-color--dark-100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_primary_BackgroundColor": {
      "name": "--pf-c-button--m-primary--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--light-100",
        "$pf-global--BackgroundColor--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_primary_hover_BackgroundColor": {
      "name": "--pf-c-button--m-primary--hover--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_button_m_primary_focus_BackgroundColor": {
      "name": "--pf-c-button--m-primary--focus--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_button_m_primary_active_BackgroundColor": {
      "name": "--pf-c-button--m-primary--active--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_button_m_secondary_Color": {
      "name": "--pf-c-button--m-secondary--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_hover_Color": {
      "name": "--pf-c-button--m-secondary--hover--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_focus_Color": {
      "name": "--pf-c-button--m-secondary--focus--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_active_Color": {
      "name": "--pf-c-button--m-secondary--active--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_BorderColor": {
      "name": "--pf-c-button--m-secondary--BorderColor",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_hover_BorderColor": {
      "name": "--pf-c-button--m-secondary--hover--BorderColor",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_focus_BorderColor": {
      "name": "--pf-c-button--m-secondary--focus--BorderColor",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_active_BorderColor": {
      "name": "--pf-c-button--m-secondary--active--BorderColor",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    }
  },
  ".pf-c-about-modal-box__close .pf-c-button.pf-m-plain:hover": {
    "c_about_modal_box__close_c_button_BackgroundColor": {
      "name": "--pf-c-about-modal-box__close--c-button--BackgroundColor",
      "value": "rgba(3, 3, 3, 0.4)",
      "values": [
        "--pf-c-about-modal-box__close--c-button--hover--BackgroundColor",
        "rgba(3, 3, 3, 0.4)"
      ]
    }
  }
};
export default c_about_modal_box;
"use strict";
exports.__esModule = true;
exports.c_page__main_breadcrumb_PaddingTop = {
  "name": "--pf-c-page__main-breadcrumb--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-page__main-breadcrumb--PaddingTop)"
};
exports["default"] = exports.c_page__main_breadcrumb_PaddingTop;
"use strict";
exports.__esModule = true;
exports.c_content_a_TextDecoration = {
  "name": "--pf-c-content--a--TextDecoration",
  "value": "underline",
  "var": "var(--pf-c-content--a--TextDecoration)"
};
exports["default"] = exports.c_content_a_TextDecoration;
"use strict";
exports.__esModule = true;
exports.c_notification_drawer__list_item_timestamp_FontSize = {
  "name": "--pf-c-notification-drawer__list-item-timestamp--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-notification-drawer__list-item-timestamp--FontSize)"
};
exports["default"] = exports.c_notification_drawer__list_item_timestamp_FontSize;
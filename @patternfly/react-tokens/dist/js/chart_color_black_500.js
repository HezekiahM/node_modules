"use strict";
exports.__esModule = true;
exports.chart_color_black_500 = {
  "name": "--pf-chart-color-black-500",
  "value": "#6a6e73",
  "var": "var(--pf-chart-color-black-500)"
};
exports["default"] = exports.chart_color_black_500;
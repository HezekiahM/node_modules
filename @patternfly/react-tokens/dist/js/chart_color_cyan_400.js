"use strict";
exports.__esModule = true;
exports.chart_color_cyan_400 = {
  "name": "--pf-chart-color-cyan-400",
  "value": "#005f60",
  "var": "var(--pf-chart-color-cyan-400)"
};
exports["default"] = exports.chart_color_cyan_400;
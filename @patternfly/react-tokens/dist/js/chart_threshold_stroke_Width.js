"use strict";
exports.__esModule = true;
exports.chart_threshold_stroke_Width = {
  "name": "--pf-chart-threshold--stroke--Width",
  "value": 1.5,
  "var": "var(--pf-chart-threshold--stroke--Width)"
};
exports["default"] = exports.chart_threshold_stroke_Width;
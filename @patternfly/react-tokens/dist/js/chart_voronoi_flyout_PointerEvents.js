"use strict";
exports.__esModule = true;
exports.chart_voronoi_flyout_PointerEvents = {
  "name": "--pf-chart-voronoi--flyout--PointerEvents",
  "value": "none",
  "var": "var(--pf-chart-voronoi--flyout--PointerEvents)"
};
exports["default"] = exports.chart_voronoi_flyout_PointerEvents;
"use strict";
exports.__esModule = true;
exports.c_search_input__utilities_MarginLeft = {
  "name": "--pf-c-search-input__utilities--MarginLeft",
  "value": "0.25rem",
  "var": "var(--pf-c-search-input__utilities--MarginLeft)"
};
exports["default"] = exports.c_search_input__utilities_MarginLeft;
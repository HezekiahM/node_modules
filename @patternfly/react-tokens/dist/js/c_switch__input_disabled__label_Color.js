"use strict";
exports.__esModule = true;
exports.c_switch__input_disabled__label_Color = {
  "name": "--pf-c-switch__input--disabled__label--Color",
  "value": "#6a6e73",
  "var": "var(--pf-c-switch__input--disabled__label--Color)"
};
exports["default"] = exports.c_switch__input_disabled__label_Color;
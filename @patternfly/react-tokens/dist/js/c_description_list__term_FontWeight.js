"use strict";
exports.__esModule = true;
exports.c_description_list__term_FontWeight = {
  "name": "--pf-c-description-list__term--FontWeight",
  "value": "700",
  "var": "var(--pf-c-description-list__term--FontWeight)"
};
exports["default"] = exports.c_description_list__term_FontWeight;
"use strict";
exports.__esModule = true;
exports.c_notification_drawer__group_toggle_PaddingLeft = {
  "name": "--pf-c-notification-drawer__group-toggle--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-notification-drawer__group-toggle--PaddingLeft)"
};
exports["default"] = exports.c_notification_drawer__group_toggle_PaddingLeft;
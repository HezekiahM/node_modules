"use strict";
exports.__esModule = true;
exports.c_button_m_display_lg_PaddingRight = {
  "name": "--pf-c-button--m-display-lg--PaddingRight",
  "value": "2rem",
  "var": "var(--pf-c-button--m-display-lg--PaddingRight)"
};
exports["default"] = exports.c_button_m_display_lg_PaddingRight;
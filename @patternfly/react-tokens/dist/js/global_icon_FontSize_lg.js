"use strict";
exports.__esModule = true;
exports.global_icon_FontSize_lg = {
  "name": "--pf-global--icon--FontSize--lg",
  "value": "1.5rem",
  "var": "var(--pf-global--icon--FontSize--lg)"
};
exports["default"] = exports.global_icon_FontSize_lg;
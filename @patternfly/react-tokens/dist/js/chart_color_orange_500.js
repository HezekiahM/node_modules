"use strict";
exports.__esModule = true;
exports.chart_color_orange_500 = {
  "name": "--pf-chart-color-orange-500",
  "value": "#8f4700",
  "var": "var(--pf-chart-color-orange-500)"
};
exports["default"] = exports.chart_color_orange_500;
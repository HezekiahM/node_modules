export const c_banner_md_PaddingRight: {
  "name": "--pf-c-banner--md--PaddingRight",
  "value": "1.5rem",
  "var": "var(--pf-c-banner--md--PaddingRight)"
};
export default c_banner_md_PaddingRight;
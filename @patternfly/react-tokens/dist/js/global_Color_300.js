"use strict";
exports.__esModule = true;
exports.global_Color_300 = {
  "name": "--pf-global--Color--300",
  "value": "#3c3f42",
  "var": "var(--pf-global--Color--300)"
};
exports["default"] = exports.global_Color_300;
"use strict";
exports.__esModule = true;
exports.global_palette_green_200 = {
  "name": "--pf-global--palette--green-200",
  "value": "#95d58e",
  "var": "var(--pf-global--palette--green-200)"
};
exports["default"] = exports.global_palette_green_200;
"use strict";
exports.__esModule = true;
exports.c_wizard__main_body_PaddingBottom = {
  "name": "--pf-c-wizard__main-body--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-wizard__main-body--PaddingBottom)"
};
exports["default"] = exports.c_wizard__main_body_PaddingBottom;
"use strict";
exports.__esModule = true;
exports.chart_tooltip_Padding = {
  "name": "--pf-chart-tooltip--Padding",
  "value": 16,
  "var": "var(--pf-chart-tooltip--Padding)"
};
exports["default"] = exports.chart_tooltip_Padding;
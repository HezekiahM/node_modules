"use strict";
exports.__esModule = true;
exports.c_wizard__footer_PaddingBottom = {
  "name": "--pf-c-wizard__footer--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-wizard__footer--PaddingBottom)"
};
exports["default"] = exports.c_wizard__footer_PaddingBottom;
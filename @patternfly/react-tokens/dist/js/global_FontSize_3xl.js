"use strict";
exports.__esModule = true;
exports.global_FontSize_3xl = {
  "name": "--pf-global--FontSize--3xl",
  "value": "1.75rem",
  "var": "var(--pf-global--FontSize--3xl)"
};
exports["default"] = exports.global_FontSize_3xl;
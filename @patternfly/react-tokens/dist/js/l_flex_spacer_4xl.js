"use strict";
exports.__esModule = true;
exports.l_flex_spacer_4xl = {
  "name": "--pf-l-flex--spacer--4xl",
  "value": "5rem",
  "var": "var(--pf-l-flex--spacer--4xl)"
};
exports["default"] = exports.l_flex_spacer_4xl;
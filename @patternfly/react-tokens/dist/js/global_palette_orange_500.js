"use strict";
exports.__esModule = true;
exports.global_palette_orange_500 = {
  "name": "--pf-global--palette--orange-500",
  "value": "#8f4700",
  "var": "var(--pf-global--palette--orange-500)"
};
exports["default"] = exports.global_palette_orange_500;
export const c_table_tr_responsive_last_child_BorderBottomWidth: {
  "name": "--pf-c-table-tr--responsive--last-child--BorderBottomWidth",
  "value": "1px",
  "var": "var(--pf-c-table-tr--responsive--last-child--BorderBottomWidth)"
};
export default c_table_tr_responsive_last_child_BorderBottomWidth;
"use strict";
exports.__esModule = true;
exports.chart_color_orange_400 = {
  "name": "--pf-chart-color-orange-400",
  "value": "#c46100",
  "var": "var(--pf-chart-color-orange-400)"
};
exports["default"] = exports.chart_color_orange_400;
"use strict";
exports.__esModule = true;
exports.chart_pie_data_stroke_Color = {
  "name": "--pf-chart-pie--data--stroke--Color",
  "value": "transparent",
  "var": "var(--pf-chart-pie--data--stroke--Color)"
};
exports["default"] = exports.chart_pie_data_stroke_Color;
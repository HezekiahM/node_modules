"use strict";
exports.__esModule = true;
exports.c_app_launcher__menu_item_external_icon_FontSize = {
  "name": "--pf-c-app-launcher__menu-item-external-icon--FontSize",
  "value": "0.625rem",
  "var": "var(--pf-c-app-launcher__menu-item-external-icon--FontSize)"
};
exports["default"] = exports.c_app_launcher__menu_item_external_icon_FontSize;
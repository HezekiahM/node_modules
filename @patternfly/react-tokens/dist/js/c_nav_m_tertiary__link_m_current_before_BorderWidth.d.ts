export const c_nav_m_tertiary__link_m_current_before_BorderWidth: {
  "name": "--pf-c-nav--m-tertiary__link--m-current--before--BorderWidth",
  "value": "3px",
  "var": "var(--pf-c-nav--m-tertiary__link--m-current--before--BorderWidth)"
};
export default c_nav_m_tertiary__link_m_current_before_BorderWidth;
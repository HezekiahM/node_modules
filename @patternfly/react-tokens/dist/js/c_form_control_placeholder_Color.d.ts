export const c_form_control_placeholder_Color: {
  "name": "--pf-c-form-control--placeholder--Color",
  "value": "#6a6e73",
  "var": "var(--pf-c-form-control--placeholder--Color)"
};
export default c_form_control_placeholder_Color;
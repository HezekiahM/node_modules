"use strict";
exports.__esModule = true;
exports.c_select__toggle_typeahead_BackgroundColor = {
  "name": "--pf-c-select__toggle-typeahead--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-select__toggle-typeahead--BackgroundColor)"
};
exports["default"] = exports.c_select__toggle_typeahead_BackgroundColor;
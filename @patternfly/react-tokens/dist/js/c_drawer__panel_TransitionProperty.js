"use strict";
exports.__esModule = true;
exports.c_drawer__panel_TransitionProperty = {
  "name": "--pf-c-drawer__panel--TransitionProperty",
  "value": "margin, transform, box-shadow",
  "var": "var(--pf-c-drawer__panel--TransitionProperty)"
};
exports["default"] = exports.c_drawer__panel_TransitionProperty;
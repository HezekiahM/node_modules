"use strict";
exports.__esModule = true;
exports.l_stack_m_gutter_MarginBottom = {
  "name": "--pf-l-stack--m-gutter--MarginBottom",
  "value": "1rem",
  "var": "var(--pf-l-stack--m-gutter--MarginBottom)"
};
exports["default"] = exports.l_stack_m_gutter_MarginBottom;
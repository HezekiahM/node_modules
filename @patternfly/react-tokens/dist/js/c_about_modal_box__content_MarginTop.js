"use strict";
exports.__esModule = true;
exports.c_about_modal_box__content_MarginTop = {
  "name": "--pf-c-about-modal-box__content--MarginTop",
  "value": "2rem",
  "var": "var(--pf-c-about-modal-box__content--MarginTop)"
};
exports["default"] = exports.c_about_modal_box__content_MarginTop;
"use strict";
exports.__esModule = true;
exports.c_popover__content_PaddingTop = {
  "name": "--pf-c-popover__content--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-popover__content--PaddingTop)"
};
exports["default"] = exports.c_popover__content_PaddingTop;
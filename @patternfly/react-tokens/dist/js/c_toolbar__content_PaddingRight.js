"use strict";
exports.__esModule = true;
exports.c_toolbar__content_PaddingRight = {
  "name": "--pf-c-toolbar__content--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-toolbar__content--PaddingRight)"
};
exports["default"] = exports.c_toolbar__content_PaddingRight;
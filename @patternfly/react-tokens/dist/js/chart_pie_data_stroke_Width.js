"use strict";
exports.__esModule = true;
exports.chart_pie_data_stroke_Width = {
  "name": "--pf-chart-pie--data--stroke--Width",
  "value": 1,
  "var": "var(--pf-chart-pie--data--stroke--Width)"
};
exports["default"] = exports.chart_pie_data_stroke_Width;
"use strict";
exports.__esModule = true;
exports.c_progress_m_success__bar_BackgroundColor = {
  "name": "--pf-c-progress--m-success__bar--BackgroundColor",
  "value": "#3e8635",
  "var": "var(--pf-c-progress--m-success__bar--BackgroundColor)"
};
exports["default"] = exports.c_progress_m_success__bar_BackgroundColor;
"use strict";
exports.__esModule = true;
exports.global_palette_purple_500 = {
  "name": "--pf-global--palette--purple-500",
  "value": "#6753ac",
  "var": "var(--pf-global--palette--purple-500)"
};
exports["default"] = exports.global_palette_purple_500;
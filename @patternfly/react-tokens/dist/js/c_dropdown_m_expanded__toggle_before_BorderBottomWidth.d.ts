export const c_dropdown_m_expanded__toggle_before_BorderBottomWidth: {
  "name": "--pf-c-dropdown--m-expanded__toggle--before--BorderBottomWidth",
  "value": "2px",
  "var": "var(--pf-c-dropdown--m-expanded__toggle--before--BorderBottomWidth)"
};
export default c_dropdown_m_expanded__toggle_before_BorderBottomWidth;
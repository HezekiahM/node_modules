export const c_tabs_m_vertical_inset: {
  "name": "--pf-c-tabs--m-vertical--inset",
  "value": "3rem",
  "var": "var(--pf-c-tabs--m-vertical--inset)"
};
export default c_tabs_m_vertical_inset;
"use strict";
exports.__esModule = true;
exports.c_content_ul_PaddingLeft = {
  "name": "--pf-c-content--ul--PaddingLeft",
  "value": "1.5rem",
  "var": "var(--pf-c-content--ul--PaddingLeft)"
};
exports["default"] = exports.c_content_ul_PaddingLeft;
"use strict";
exports.__esModule = true;
exports.c_wizard__toggle_separator_MarginLeft = {
  "name": "--pf-c-wizard__toggle-separator--MarginLeft",
  "value": "0.5rem",
  "var": "var(--pf-c-wizard__toggle-separator--MarginLeft)"
};
exports["default"] = exports.c_wizard__toggle_separator_MarginLeft;
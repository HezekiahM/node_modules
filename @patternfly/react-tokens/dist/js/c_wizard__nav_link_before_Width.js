"use strict";
exports.__esModule = true;
exports.c_wizard__nav_link_before_Width = {
  "name": "--pf-c-wizard__nav-link--before--Width",
  "value": "1.5rem",
  "var": "var(--pf-c-wizard__nav-link--before--Width)"
};
exports["default"] = exports.c_wizard__nav_link_before_Width;
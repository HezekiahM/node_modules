"use strict";
exports.__esModule = true;
exports.c_login__header_PaddingLeft = {
  "name": "--pf-c-login__header--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-login__header--PaddingLeft)"
};
exports["default"] = exports.c_login__header_PaddingLeft;
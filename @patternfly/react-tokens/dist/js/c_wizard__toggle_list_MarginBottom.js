"use strict";
exports.__esModule = true;
exports.c_wizard__toggle_list_MarginBottom = {
  "name": "--pf-c-wizard__toggle-list--MarginBottom",
  "value": "calc(0.25rem * -1)",
  "var": "var(--pf-c-wizard__toggle-list--MarginBottom)"
};
exports["default"] = exports.c_wizard__toggle_list_MarginBottom;
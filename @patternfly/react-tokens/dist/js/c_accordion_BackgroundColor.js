"use strict";
exports.__esModule = true;
exports.c_accordion_BackgroundColor = {
  "name": "--pf-c-accordion--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-accordion--BackgroundColor)"
};
exports["default"] = exports.c_accordion_BackgroundColor;
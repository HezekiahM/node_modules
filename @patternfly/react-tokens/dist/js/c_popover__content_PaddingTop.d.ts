export const c_popover__content_PaddingTop: {
  "name": "--pf-c-popover__content--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-popover__content--PaddingTop)"
};
export default c_popover__content_PaddingTop;
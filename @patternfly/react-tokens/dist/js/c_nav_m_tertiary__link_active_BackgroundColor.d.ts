export const c_nav_m_tertiary__link_active_BackgroundColor: {
  "name": "--pf-c-nav--m-tertiary__link--active--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-nav--m-tertiary__link--active--BackgroundColor)"
};
export default c_nav_m_tertiary__link_active_BackgroundColor;
"use strict";
exports.__esModule = true;
exports.c_page__header_MinHeight = {
  "name": "--pf-c-page__header--MinHeight",
  "value": "4.75rem",
  "var": "var(--pf-c-page__header--MinHeight)"
};
exports["default"] = exports.c_page__header_MinHeight;
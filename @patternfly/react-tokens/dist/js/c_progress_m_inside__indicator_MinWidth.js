"use strict";
exports.__esModule = true;
exports.c_progress_m_inside__indicator_MinWidth = {
  "name": "--pf-c-progress--m-inside__indicator--MinWidth",
  "value": "2rem",
  "var": "var(--pf-c-progress--m-inside__indicator--MinWidth)"
};
exports["default"] = exports.c_progress_m_inside__indicator_MinWidth;
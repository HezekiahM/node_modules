"use strict";
exports.__esModule = true;
exports.c_button_m_secondary_hover_Color = {
  "name": "--pf-c-button--m-secondary--hover--Color",
  "value": "#fff",
  "var": "var(--pf-c-button--m-secondary--hover--Color)"
};
exports["default"] = exports.c_button_m_secondary_hover_Color;
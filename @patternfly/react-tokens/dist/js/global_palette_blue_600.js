"use strict";
exports.__esModule = true;
exports.global_palette_blue_600 = {
  "name": "--pf-global--palette--blue-600",
  "value": "#002952",
  "var": "var(--pf-global--palette--blue-600)"
};
exports["default"] = exports.global_palette_blue_600;
"use strict";
exports.__esModule = true;
exports.c_dropdown__toggle_active_before_BorderBottomWidth = {
  "name": "--pf-c-dropdown__toggle--active--before--BorderBottomWidth",
  "value": "2px",
  "var": "var(--pf-c-dropdown__toggle--active--before--BorderBottomWidth)"
};
exports["default"] = exports.c_dropdown__toggle_active_before_BorderBottomWidth;
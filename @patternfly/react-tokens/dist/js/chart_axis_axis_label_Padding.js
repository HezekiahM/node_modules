"use strict";
exports.__esModule = true;
exports.chart_axis_axis_label_Padding = {
  "name": "--pf-chart-axis--axis-label--Padding",
  "value": 40,
  "var": "var(--pf-chart-axis--axis-label--Padding)"
};
exports["default"] = exports.chart_axis_axis_label_Padding;
"use strict";
exports.__esModule = true;
exports.c_breadcrumb__heading_FontSize = {
  "name": "--pf-c-breadcrumb__heading--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-breadcrumb__heading--FontSize)"
};
exports["default"] = exports.c_breadcrumb__heading_FontSize;
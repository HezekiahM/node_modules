export const c_expandable_section__toggle_hover_Color: {
  "name": "--pf-c-expandable-section__toggle--hover--Color",
  "value": "#004080",
  "var": "var(--pf-c-expandable-section__toggle--hover--Color)"
};
export default c_expandable_section__toggle_hover_Color;
"use strict";
exports.__esModule = true;
exports.chart_global_letter_spacing = {
  "name": "--pf-chart-global--letter-spacing",
  "value": "normal",
  "var": "var(--pf-chart-global--letter-spacing)"
};
exports["default"] = exports.chart_global_letter_spacing;
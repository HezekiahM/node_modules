"use strict";
exports.__esModule = true;
exports.c_badge = {
  ".pf-c-badge": {
    "c_badge_BorderRadius": {
      "name": "--pf-c-badge--BorderRadius",
      "value": "30em",
      "values": [
        "--pf-global--BorderRadius--lg",
        "$pf-global--BorderRadius--lg",
        "30em"
      ]
    },
    "c_badge_FontSize": {
      "name": "--pf-c-badge--FontSize",
      "value": "0.75rem",
      "values": [
        "--pf-global--FontSize--xs",
        "$pf-global--FontSize--xs",
        "pf-font-prem(12px)",
        "0.75rem"
      ]
    },
    "c_badge_FontWeight": {
      "name": "--pf-c-badge--FontWeight",
      "value": "700",
      "values": [
        "--pf-global--FontWeight--bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    },
    "c_badge_PaddingRight": {
      "name": "--pf-c-badge--PaddingRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_badge_PaddingLeft": {
      "name": "--pf-c-badge--PaddingLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_badge_Color": {
      "name": "--pf-c-badge--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_badge_MinWidth": {
      "name": "--pf-c-badge--MinWidth",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_badge_m_read_BackgroundColor": {
      "name": "--pf-c-badge--m-read--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--200",
        "$pf-global--BackgroundColor--200",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_badge_m_read_Color": {
      "name": "--pf-c-badge--m-read--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_badge_m_unread_BackgroundColor": {
      "name": "--pf-c-badge--m-unread--BackgroundColor",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_badge_m_unread_Color": {
      "name": "--pf-c-badge--m-unread--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    }
  },
  ".pf-c-badge.pf-m-read": {
    "c_badge_Color": {
      "name": "--pf-c-badge--Color",
      "value": "#151515",
      "values": [
        "--pf-c-badge--m-read--Color",
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_badge_BackgroundColor": {
      "name": "--pf-c-badge--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-c-badge--m-read--BackgroundColor",
        "--pf-global--BackgroundColor--200",
        "$pf-global--BackgroundColor--200",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    }
  },
  ".pf-c-badge.pf-m-unread": {
    "c_badge_Color": {
      "name": "--pf-c-badge--Color",
      "value": "#fff",
      "values": [
        "--pf-c-badge--m-unread--Color",
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_badge_BackgroundColor": {
      "name": "--pf-c-badge--BackgroundColor",
      "value": "#06c",
      "values": [
        "--pf-c-badge--m-unread--BackgroundColor",
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  }
};
exports["default"] = exports.c_badge;
"use strict";
exports.__esModule = true;
exports.global_icon_Color_light = {
  "name": "--pf-global--icon--Color--light",
  "value": "#6a6e73",
  "var": "var(--pf-global--icon--Color--light)"
};
exports["default"] = exports.global_icon_Color_light;
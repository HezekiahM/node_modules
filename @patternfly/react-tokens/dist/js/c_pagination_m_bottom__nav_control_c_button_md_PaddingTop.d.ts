export const c_pagination_m_bottom__nav_control_c_button_md_PaddingTop: {
  "name": "--pf-c-pagination--m-bottom__nav-control--c-button--md--PaddingTop",
  "value": "0.375rem",
  "var": "var(--pf-c-pagination--m-bottom__nav-control--c-button--md--PaddingTop)"
};
export default c_pagination_m_bottom__nav_control_c_button_md_PaddingTop;
"use strict";
exports.__esModule = true;
exports.c_notification_drawer__group_toggle_BorderColor = {
  "name": "--pf-c-notification-drawer__group-toggle--BorderColor",
  "value": "#d2d2d2",
  "var": "var(--pf-c-notification-drawer__group-toggle--BorderColor)"
};
exports["default"] = exports.c_notification_drawer__group_toggle_BorderColor;
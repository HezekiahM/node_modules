export const c_button_m_plain_BackgroundColor: {
  "name": "--pf-c-button--m-plain--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-button--m-plain--BackgroundColor)"
};
export default c_button_m_plain_BackgroundColor;
export const c_search_input__text_after_BorderBottomWidth: {
  "name": "--pf-c-search-input__text--after--BorderBottomWidth",
  "value": "2px",
  "var": "var(--pf-c-search-input__text--after--BorderBottomWidth)"
};
export default c_search_input__text_after_BorderBottomWidth;
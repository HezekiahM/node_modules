"use strict";
exports.__esModule = true;
exports.global_palette_purple_100 = {
  "name": "--pf-global--palette--purple-100",
  "value": "#cbc1ff",
  "var": "var(--pf-global--palette--purple-100)"
};
exports["default"] = exports.global_palette_purple_100;
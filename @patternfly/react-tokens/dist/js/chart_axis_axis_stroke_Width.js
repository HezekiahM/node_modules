"use strict";
exports.__esModule = true;
exports.chart_axis_axis_stroke_Width = {
  "name": "--pf-chart-axis--axis--stroke--Width",
  "value": 1,
  "var": "var(--pf-chart-axis--axis--stroke--Width)"
};
exports["default"] = exports.chart_axis_axis_stroke_Width;
"use strict";
exports.__esModule = true;
exports.global_spacer_lg = {
  "name": "--pf-global--spacer--lg",
  "value": "1.5rem",
  "var": "var(--pf-global--spacer--lg)"
};
exports["default"] = exports.global_spacer_lg;
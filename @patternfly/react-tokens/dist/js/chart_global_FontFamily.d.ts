export const chart_global_FontFamily: {
  "name": "--pf-chart-global--FontFamily",
  "value": "RedHatText, Overpass, overpass, helvetica, arial, sans-serif",
  "var": "var(--pf-chart-global--FontFamily)"
};
export default chart_global_FontFamily;
"use strict";
exports.__esModule = true;
exports.c_switch_LineHeight = {
  "name": "--pf-c-switch--LineHeight",
  "value": "1.5",
  "var": "var(--pf-c-switch--LineHeight)"
};
exports["default"] = exports.c_switch_LineHeight;
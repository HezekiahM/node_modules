"use strict";
exports.__esModule = true;
exports.c_button_m_plain_hover_BackgroundColor = {
  "name": "--pf-c-button--m-plain--hover--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-button--m-plain--hover--BackgroundColor)"
};
exports["default"] = exports.c_button_m_plain_hover_BackgroundColor;
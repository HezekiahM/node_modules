export const c_button_disabled_Color: {
  "name": "--pf-c-button--disabled--Color",
  "value": "#d2d2d2",
  "var": "var(--pf-c-button--disabled--Color)"
};
export default c_button_disabled_Color;
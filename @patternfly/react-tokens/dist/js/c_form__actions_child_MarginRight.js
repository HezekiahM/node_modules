"use strict";
exports.__esModule = true;
exports.c_form__actions_child_MarginRight = {
  "name": "--pf-c-form__actions--child--MarginRight",
  "value": "0.5rem",
  "var": "var(--pf-c-form__actions--child--MarginRight)"
};
exports["default"] = exports.c_form__actions_child_MarginRight;
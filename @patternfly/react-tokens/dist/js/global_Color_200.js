"use strict";
exports.__esModule = true;
exports.global_Color_200 = {
  "name": "--pf-global--Color--200",
  "value": "#f0f0f0",
  "var": "var(--pf-global--Color--200)"
};
exports["default"] = exports.global_Color_200;
"use strict";
exports.__esModule = true;
exports.chart_bullet_comparative_measure_error_Width = {
  "name": "--pf-chart-bullet--comparative-measure--error--Width",
  "value": 30,
  "var": "var(--pf-chart-bullet--comparative-measure--error--Width)"
};
exports["default"] = exports.chart_bullet_comparative_measure_error_Width;
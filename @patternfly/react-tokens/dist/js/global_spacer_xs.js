"use strict";
exports.__esModule = true;
exports.global_spacer_xs = {
  "name": "--pf-global--spacer--xs",
  "value": "0.25rem",
  "var": "var(--pf-global--spacer--xs)"
};
exports["default"] = exports.global_spacer_xs;
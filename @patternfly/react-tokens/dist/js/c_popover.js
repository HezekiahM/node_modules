"use strict";
exports.__esModule = true;
exports.c_popover = {
  ".pf-c-popover": {
    "c_popover_FontSize": {
      "name": "--pf-c-popover--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_popover_MinWidth": {
      "name": "--pf-c-popover--MinWidth",
      "value": "calc(1rem + 1rem + 18.75rem)",
      "values": [
        "calc(--pf-c-popover__content--PaddingLeft + --pf-c-popover__content--PaddingRight + 18.75rem)",
        "calc(--pf-global--spacer--md + --pf-global--spacer--md + 18.75rem)",
        "calc($pf-global--spacer--md + $pf-global--spacer--md + 18.75rem)",
        "calc(pf-size-prem(16px) + pf-size-prem(16px) + 18.75rem)",
        "calc(1rem + 1rem + 18.75rem)"
      ]
    },
    "c_popover_MaxWidth": {
      "name": "--pf-c-popover--MaxWidth",
      "value": "calc(1rem + 1rem + 18.75rem)",
      "values": [
        "calc(--pf-c-popover__content--PaddingLeft + --pf-c-popover__content--PaddingRight + 18.75rem)",
        "calc(--pf-global--spacer--md + --pf-global--spacer--md + 18.75rem)",
        "calc($pf-global--spacer--md + $pf-global--spacer--md + 18.75rem)",
        "calc(pf-size-prem(16px) + pf-size-prem(16px) + 18.75rem)",
        "calc(1rem + 1rem + 18.75rem)"
      ]
    },
    "c_popover_BoxShadow": {
      "name": "--pf-c-popover--BoxShadow",
      "value": "0 0.25rem 0.5rem 0rem rgba(3, 3, 3, 0.12), 0 0 0.25rem 0 rgba(3, 3, 3, 0.06)",
      "values": [
        "--pf-global--BoxShadow--md",
        "$pf-global--BoxShadow--md",
        "0 pf-size-prem(4px) pf-size-prem(8px) pf-size-prem(0) rgba($pf-color-black-1000, .12), 0 0 pf-size-prem(4px) 0 rgba($pf-color-black-1000, .06)",
        "0 pf-size-prem(4px) pf-size-prem(8px) pf-size-prem(0) rgba(#030303, .12), 0 0 pf-size-prem(4px) 0 rgba(#030303, .06)",
        "0 0.25rem 0.5rem 0rem rgba(3, 3, 3, 0.12), 0 0 0.25rem 0 rgba(3, 3, 3, 0.06)"
      ]
    },
    "c_popover__content_BackgroundColor": {
      "name": "--pf-c-popover__content--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_popover__content_PaddingTop": {
      "name": "--pf-c-popover__content--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_popover__content_PaddingRight": {
      "name": "--pf-c-popover__content--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_popover__content_PaddingBottom": {
      "name": "--pf-c-popover__content--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_popover__content_PaddingLeft": {
      "name": "--pf-c-popover__content--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_popover__arrow_Width": {
      "name": "--pf-c-popover__arrow--Width",
      "value": "1.5625rem",
      "values": [
        "--pf-global--arrow--width-lg",
        "$pf-global--arrow--width-lg",
        "pf-font-prem(25px)",
        "1.5625rem"
      ]
    },
    "c_popover__arrow_Height": {
      "name": "--pf-c-popover__arrow--Height",
      "value": "1.5625rem",
      "values": [
        "--pf-global--arrow--width-lg",
        "$pf-global--arrow--width-lg",
        "pf-font-prem(25px)",
        "1.5625rem"
      ]
    },
    "c_popover__arrow_BoxShadow": {
      "name": "--pf-c-popover__arrow--BoxShadow",
      "value": "0 0.25rem 0.5rem 0rem rgba(3, 3, 3, 0.12), 0 0 0.25rem 0 rgba(3, 3, 3, 0.06)",
      "values": [
        "--pf-global--BoxShadow--md",
        "$pf-global--BoxShadow--md",
        "0 pf-size-prem(4px) pf-size-prem(8px) pf-size-prem(0) rgba($pf-color-black-1000, .12), 0 0 pf-size-prem(4px) 0 rgba($pf-color-black-1000, .06)",
        "0 pf-size-prem(4px) pf-size-prem(8px) pf-size-prem(0) rgba(#030303, .12), 0 0 pf-size-prem(4px) 0 rgba(#030303, .06)",
        "0 0.25rem 0.5rem 0rem rgba(3, 3, 3, 0.12), 0 0 0.25rem 0 rgba(3, 3, 3, 0.06)"
      ]
    },
    "c_popover__arrow_BackgroundColor": {
      "name": "--pf-c-popover__arrow--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_popover__arrow_m_top_TranslateX": {
      "name": "--pf-c-popover__arrow--m-top--TranslateX",
      "value": "-50%"
    },
    "c_popover__arrow_m_top_TranslateY": {
      "name": "--pf-c-popover__arrow--m-top--TranslateY",
      "value": "50%"
    },
    "c_popover__arrow_m_top_Rotate": {
      "name": "--pf-c-popover__arrow--m-top--Rotate",
      "value": "45deg"
    },
    "c_popover__arrow_m_right_TranslateX": {
      "name": "--pf-c-popover__arrow--m-right--TranslateX",
      "value": "-50%"
    },
    "c_popover__arrow_m_right_TranslateY": {
      "name": "--pf-c-popover__arrow--m-right--TranslateY",
      "value": "-50%"
    },
    "c_popover__arrow_m_right_Rotate": {
      "name": "--pf-c-popover__arrow--m-right--Rotate",
      "value": "45deg"
    },
    "c_popover__arrow_m_bottom_TranslateX": {
      "name": "--pf-c-popover__arrow--m-bottom--TranslateX",
      "value": "-50%"
    },
    "c_popover__arrow_m_bottom_TranslateY": {
      "name": "--pf-c-popover__arrow--m-bottom--TranslateY",
      "value": "-50%"
    },
    "c_popover__arrow_m_bottom_Rotate": {
      "name": "--pf-c-popover__arrow--m-bottom--Rotate",
      "value": "45deg"
    },
    "c_popover__arrow_m_left_TranslateX": {
      "name": "--pf-c-popover__arrow--m-left--TranslateX",
      "value": "50%"
    },
    "c_popover__arrow_m_left_TranslateY": {
      "name": "--pf-c-popover__arrow--m-left--TranslateY",
      "value": "-50%"
    },
    "c_popover__arrow_m_left_Rotate": {
      "name": "--pf-c-popover__arrow--m-left--Rotate",
      "value": "45deg"
    },
    "c_popover_c_button_MarginLeft": {
      "name": "--pf-c-popover--c-button--MarginLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_popover_c_button_Top": {
      "name": "--pf-c-popover--c-button--Top",
      "value": "calc(1rem - 0.375rem)",
      "values": [
        "calc(--pf-c-popover__content--PaddingTop - --pf-global--spacer--form-element)",
        "calc(--pf-global--spacer--md - $pf-global--spacer--form-element)",
        "calc($pf-global--spacer--md - $pf-global--spacer--form-element)",
        "calc(pf-size-prem(16px) - pf-size-prem(6px))",
        "calc(1rem - 0.375rem)"
      ]
    },
    "c_popover_c_button_Right": {
      "name": "--pf-c-popover--c-button--Right",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_popover_c_button_sibling_PaddingRight": {
      "name": "--pf-c-popover--c-button--sibling--PaddingRight",
      "value": "3rem",
      "values": [
        "--pf-global--spacer--2xl",
        "$pf-global--spacer--2xl",
        "pf-size-prem(48px)",
        "3rem"
      ]
    },
    "c_popover_c_title_MarginBottom": {
      "name": "--pf-c-popover--c-title--MarginBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_popover__footer_MarginTop": {
      "name": "--pf-c-popover__footer--MarginTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    }
  }
};
exports["default"] = exports.c_popover;
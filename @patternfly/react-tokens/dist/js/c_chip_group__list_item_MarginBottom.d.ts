export const c_chip_group__list_item_MarginBottom: {
  "name": "--pf-c-chip-group__list-item--MarginBottom",
  "value": "0.25rem",
  "var": "var(--pf-c-chip-group__list-item--MarginBottom)"
};
export default c_chip_group__list_item_MarginBottom;
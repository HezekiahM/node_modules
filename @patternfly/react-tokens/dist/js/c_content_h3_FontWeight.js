"use strict";
exports.__esModule = true;
exports.c_content_h3_FontWeight = {
  "name": "--pf-c-content--h3--FontWeight",
  "value": "400",
  "var": "var(--pf-c-content--h3--FontWeight)"
};
exports["default"] = exports.c_content_h3_FontWeight;
export const c_data_list__item_m_selectable_hover_item_BorderTopColor: {
  "name": "--pf-c-data-list__item--m-selectable--hover--item--BorderTopColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-data-list__item--m-selectable--hover--item--BorderTopColor)"
};
export default c_data_list__item_m_selectable_hover_item_BorderTopColor;
"use strict";
exports.__esModule = true;
exports.chart_tooltip_flyoutStyle_corner_radius = {
  "name": "--pf-chart-tooltip--flyoutStyle--corner-radius",
  "value": 0,
  "var": "var(--pf-chart-tooltip--flyoutStyle--corner-radius)"
};
exports["default"] = exports.chart_tooltip_flyoutStyle_corner_radius;
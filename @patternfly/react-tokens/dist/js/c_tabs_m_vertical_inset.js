"use strict";
exports.__esModule = true;
exports.c_tabs_m_vertical_inset = {
  "name": "--pf-c-tabs--m-vertical--inset",
  "value": "3rem",
  "var": "var(--pf-c-tabs--m-vertical--inset)"
};
exports["default"] = exports.c_tabs_m_vertical_inset;
"use strict";
exports.__esModule = true;
exports.c_form_m_horizontal__group_control_md_GridColumnWidth = {
  "name": "--pf-c-form--m-horizontal__group-control--md--GridColumnWidth",
  "value": "1fr",
  "var": "var(--pf-c-form--m-horizontal__group-control--md--GridColumnWidth)"
};
exports["default"] = exports.c_form_m_horizontal__group_control_md_GridColumnWidth;
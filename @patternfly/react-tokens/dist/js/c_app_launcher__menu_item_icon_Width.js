"use strict";
exports.__esModule = true;
exports.c_app_launcher__menu_item_icon_Width = {
  "name": "--pf-c-app-launcher__menu-item-icon--Width",
  "value": "1.5rem",
  "var": "var(--pf-c-app-launcher__menu-item-icon--Width)"
};
exports["default"] = exports.c_app_launcher__menu_item_icon_Width;
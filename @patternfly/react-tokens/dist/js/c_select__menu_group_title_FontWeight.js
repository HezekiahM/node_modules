"use strict";
exports.__esModule = true;
exports.c_select__menu_group_title_FontWeight = {
  "name": "--pf-c-select__menu-group-title--FontWeight",
  "value": "700",
  "var": "var(--pf-c-select__menu-group-title--FontWeight)"
};
exports["default"] = exports.c_select__menu_group_title_FontWeight;
export const c_options_menu__menu_item_disabled_Color: {
  "name": "--pf-c-options-menu__menu-item--disabled--Color",
  "value": "#6a6e73",
  "var": "var(--pf-c-options-menu__menu-item--disabled--Color)"
};
export default c_options_menu__menu_item_disabled_Color;
"use strict";
exports.__esModule = true;
exports.c_switch__label_PaddingLeft = {
  "name": "--pf-c-switch__label--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-switch__label--PaddingLeft)"
};
exports["default"] = exports.c_switch__label_PaddingLeft;
export const c_tile_BackgroundColor: {
  "name": "--pf-c-tile--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-tile--BackgroundColor)"
};
export default c_tile_BackgroundColor;
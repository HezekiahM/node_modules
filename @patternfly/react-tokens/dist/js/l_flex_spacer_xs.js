"use strict";
exports.__esModule = true;
exports.l_flex_spacer_xs = {
  "name": "--pf-l-flex--spacer--xs",
  "value": "0.25rem",
  "var": "var(--pf-l-flex--spacer--xs)"
};
exports["default"] = exports.l_flex_spacer_xs;
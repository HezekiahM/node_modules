"use strict";
exports.__esModule = true;
exports.c_nav__toggle_FontSize = {
  "name": "--pf-c-nav__toggle--FontSize",
  "value": "1.125rem",
  "var": "var(--pf-c-nav__toggle--FontSize)"
};
exports["default"] = exports.c_nav__toggle_FontSize;
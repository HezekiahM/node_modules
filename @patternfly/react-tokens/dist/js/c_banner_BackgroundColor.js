"use strict";
exports.__esModule = true;
exports.c_banner_BackgroundColor = {
  "name": "--pf-c-banner--BackgroundColor",
  "value": "#f0ab00",
  "var": "var(--pf-c-banner--BackgroundColor)"
};
exports["default"] = exports.c_banner_BackgroundColor;
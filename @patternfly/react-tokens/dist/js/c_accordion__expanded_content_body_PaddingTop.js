"use strict";
exports.__esModule = true;
exports.c_accordion__expanded_content_body_PaddingTop = {
  "name": "--pf-c-accordion__expanded-content-body--PaddingTop",
  "value": "0.5rem",
  "var": "var(--pf-c-accordion__expanded-content-body--PaddingTop)"
};
exports["default"] = exports.c_accordion__expanded_content_body_PaddingTop;
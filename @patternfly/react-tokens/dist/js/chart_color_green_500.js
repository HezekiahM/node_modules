"use strict";
exports.__esModule = true;
exports.chart_color_green_500 = {
  "name": "--pf-chart-color-green-500",
  "value": "#23511e",
  "var": "var(--pf-chart-color-green-500)"
};
exports["default"] = exports.chart_color_green_500;
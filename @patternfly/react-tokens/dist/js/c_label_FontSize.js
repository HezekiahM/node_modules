"use strict";
exports.__esModule = true;
exports.c_label_FontSize = {
  "name": "--pf-c-label--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-label--FontSize)"
};
exports["default"] = exports.c_label_FontSize;
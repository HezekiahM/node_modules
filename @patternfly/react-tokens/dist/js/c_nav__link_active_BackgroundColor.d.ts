export const c_nav__link_active_BackgroundColor: {
  "name": "--pf-c-nav__link--active--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-nav__link--active--BackgroundColor)"
};
export default c_nav__link_active_BackgroundColor;
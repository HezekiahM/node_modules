"use strict";
exports.__esModule = true;
exports.c_about_modal_box__close_PaddingRight = {
  "name": "--pf-c-about-modal-box__close--PaddingRight",
  "value": "2rem",
  "var": "var(--pf-c-about-modal-box__close--PaddingRight)"
};
exports["default"] = exports.c_about_modal_box__close_PaddingRight;
"use strict";
exports.__esModule = true;
exports.chart_candelstick_data_stroke_Color = {
  "name": "--pf-chart-candelstick--data--stroke--Color",
  "value": "#151515",
  "var": "var(--pf-chart-candelstick--data--stroke--Color)"
};
exports["default"] = exports.chart_candelstick_data_stroke_Color;
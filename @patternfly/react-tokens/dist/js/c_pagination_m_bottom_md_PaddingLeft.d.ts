export const c_pagination_m_bottom_md_PaddingLeft: {
  "name": "--pf-c-pagination--m-bottom--md--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-pagination--m-bottom--md--PaddingLeft)"
};
export default c_pagination_m_bottom_md_PaddingLeft;
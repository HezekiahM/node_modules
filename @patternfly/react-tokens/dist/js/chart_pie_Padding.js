"use strict";
exports.__esModule = true;
exports.chart_pie_Padding = {
  "name": "--pf-chart-pie--Padding",
  "value": 20,
  "var": "var(--pf-chart-pie--Padding)"
};
exports["default"] = exports.chart_pie_Padding;
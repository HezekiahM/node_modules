"use strict";
exports.__esModule = true;
exports.chart_global_label_text_anchor = {
  "name": "--pf-chart-global--label--text-anchor",
  "value": "middle",
  "var": "var(--pf-chart-global--label--text-anchor)"
};
exports["default"] = exports.chart_global_label_text_anchor;
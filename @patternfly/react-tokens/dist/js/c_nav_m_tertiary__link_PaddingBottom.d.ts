export const c_nav_m_tertiary__link_PaddingBottom: {
  "name": "--pf-c-nav--m-tertiary__link--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-nav--m-tertiary__link--PaddingBottom)"
};
export default c_nav_m_tertiary__link_PaddingBottom;
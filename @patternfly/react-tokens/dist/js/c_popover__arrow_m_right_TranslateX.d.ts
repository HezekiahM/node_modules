export const c_popover__arrow_m_right_TranslateX: {
  "name": "--pf-c-popover__arrow--m-right--TranslateX",
  "value": "-50%",
  "var": "var(--pf-c-popover__arrow--m-right--TranslateX)"
};
export default c_popover__arrow_m_right_TranslateX;
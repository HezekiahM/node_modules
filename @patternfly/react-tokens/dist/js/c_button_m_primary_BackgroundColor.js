"use strict";
exports.__esModule = true;
exports.c_button_m_primary_BackgroundColor = {
  "name": "--pf-c-button--m-primary--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-button--m-primary--BackgroundColor)"
};
exports["default"] = exports.c_button_m_primary_BackgroundColor;
"use strict";
exports.__esModule = true;
exports.c_page__main_section_PaddingLeft = {
  "name": "--pf-c-page__main-section--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-page__main-section--PaddingLeft)"
};
exports["default"] = exports.c_page__main_section_PaddingLeft;
"use strict";
exports.__esModule = true;
exports.c_divider_after_Height = {
  "name": "--pf-c-divider--after--Height",
  "value": "1px",
  "var": "var(--pf-c-divider--after--Height)"
};
exports["default"] = exports.c_divider_after_Height;
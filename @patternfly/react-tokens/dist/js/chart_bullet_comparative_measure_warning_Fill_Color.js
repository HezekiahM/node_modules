"use strict";
exports.__esModule = true;
exports.chart_bullet_comparative_measure_warning_Fill_Color = {
  "name": "--pf-chart-bullet--comparative-measure--warning--Fill--Color",
  "value": "#ec7a08",
  "var": "var(--pf-chart-bullet--comparative-measure--warning--Fill--Color)"
};
exports["default"] = exports.chart_bullet_comparative_measure_warning_Fill_Color;
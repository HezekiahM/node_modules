"use strict";
exports.__esModule = true;
exports.global_palette_green_500 = {
  "name": "--pf-global--palette--green-500",
  "value": "#3e8635",
  "var": "var(--pf-global--palette--green-500)"
};
exports["default"] = exports.global_palette_green_500;
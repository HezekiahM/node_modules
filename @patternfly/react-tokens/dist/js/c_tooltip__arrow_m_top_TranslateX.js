"use strict";
exports.__esModule = true;
exports.c_tooltip__arrow_m_top_TranslateX = {
  "name": "--pf-c-tooltip__arrow--m-top--TranslateX",
  "value": "-50%",
  "var": "var(--pf-c-tooltip__arrow--m-top--TranslateX)"
};
exports["default"] = exports.c_tooltip__arrow_m_top_TranslateX;
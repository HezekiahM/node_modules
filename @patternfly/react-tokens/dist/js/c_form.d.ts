export const c_form: {
  ".pf-c-form": {
    "c_form_GridGap": {
      "name": "--pf-c-form--GridGap",
      "value": "1.5rem",
      "values": [
        "--pf-global--gutter--md",
        "$pf-global--gutter--md",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_form__group_m_action_MarginTop": {
      "name": "--pf-c-form__group--m-action--MarginTop",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_form_m_horizontal__group_label_md_GridColumnWidth": {
      "name": "--pf-c-form--m-horizontal__group-label--md--GridColumnWidth",
      "value": "9.375rem"
    },
    "c_form_m_horizontal__group_label_md_GridColumnGap": {
      "name": "--pf-c-form--m-horizontal__group-label--md--GridColumnGap",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_form_m_horizontal__group_control_md_GridColumnWidth": {
      "name": "--pf-c-form--m-horizontal__group-control--md--GridColumnWidth",
      "value": "1fr"
    },
    "c_form_m_horizontal__group_label_md_PaddingTop": {
      "name": "--pf-c-form--m-horizontal__group-label--md--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_form__group_label_PaddingBottom": {
      "name": "--pf-c-form__group-label--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_form__label_FontSize": {
      "name": "--pf-c-form__label--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_form__label_LineHeight": {
      "name": "--pf-c-form__label--LineHeight",
      "value": "1.3",
      "values": [
        "--pf-global--LineHeight--sm",
        "$pf-global--LineHeight--sm",
        "1.3"
      ]
    },
    "c_form__label_m_disabled_Color": {
      "name": "--pf-c-form__label--m-disabled--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--disabled-color--100",
        "$pf-global--disabled-color--100",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_form__label_text_FontWeight": {
      "name": "--pf-c-form__label-text--FontWeight",
      "value": "700",
      "values": [
        "--pf-global--FontWeight--bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    },
    "c_form__label_required_MarginLeft": {
      "name": "--pf-c-form__label-required--MarginLeft",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_form__label_required_FontSize": {
      "name": "--pf-c-form__label-required--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_form__label_required_Color": {
      "name": "--pf-c-form__label-required--Color",
      "value": "#c9190b",
      "values": [
        "--pf-global--danger-color--100",
        "$pf-global--danger-color--100",
        "$pf-color-red-100",
        "#c9190b"
      ]
    },
    "c_form__group_label_help_PaddingTop": {
      "name": "--pf-c-form__group-label-help--PaddingTop",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_form__group_label_help_PaddingRight": {
      "name": "--pf-c-form__group-label-help--PaddingRight",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_form__group_label_help_PaddingBottom": {
      "name": "--pf-c-form__group-label-help--PaddingBottom",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_form__group_label_help_PaddingLeft": {
      "name": "--pf-c-form__group-label-help--PaddingLeft",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_form__group_label_help_MarginTop": {
      "name": "--pf-c-form__group-label-help--MarginTop",
      "value": "calc(0.25rem * -1)",
      "values": [
        "calc(--pf-c-form__group-label-help--PaddingTop * -1)",
        "calc(--pf-global--spacer--xs * -1)",
        "calc($pf-global--spacer--xs * -1)",
        "calc(pf-size-prem(4px) * -1)",
        "calc(0.25rem * -1)"
      ]
    },
    "c_form__group_label_help_MarginRight": {
      "name": "--pf-c-form__group-label-help--MarginRight",
      "value": "calc(0.25rem * -1)",
      "values": [
        "calc(--pf-c-form__group-label-help--PaddingRight * -1)",
        "calc(--pf-global--spacer--xs * -1)",
        "calc($pf-global--spacer--xs * -1)",
        "calc(pf-size-prem(4px) * -1)",
        "calc(0.25rem * -1)"
      ]
    },
    "c_form__group_label_help_MarginBottom": {
      "name": "--pf-c-form__group-label-help--MarginBottom",
      "value": "calc(0.25rem * -1)",
      "values": [
        "calc(--pf-c-form__group-label-help--PaddingBottom * -1)",
        "calc(--pf-global--spacer--xs * -1)",
        "calc($pf-global--spacer--xs * -1)",
        "calc(pf-size-prem(4px) * -1)",
        "calc(0.25rem * -1)"
      ]
    },
    "c_form__group_label_help_MarginLeft": {
      "name": "--pf-c-form__group-label-help--MarginLeft",
      "value": "calc(0.25rem * -1 + 0.25rem)",
      "values": [
        "calc(--pf-c-form__group-label-help--PaddingLeft * -1 + --pf-global--spacer--xs)",
        "calc(--pf-global--spacer--xs * -1 + $pf-global--spacer--xs)",
        "calc($pf-global--spacer--xs * -1 + $pf-global--spacer--xs)",
        "calc(pf-size-prem(4px) * -1 + pf-size-prem(4px))",
        "calc(0.25rem * -1 + 0.25rem)"
      ]
    },
    "c_form__group_label_help_FontSize": {
      "name": "--pf-c-form__group-label-help--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_form__group_label_help_TranslateY": {
      "name": "--pf-c-form__group-label-help--TranslateY",
      "value": "0.125rem"
    },
    "c_form__group_control_m_inline_child_MarginRight": {
      "name": "--pf-c-form__group-control--m-inline--child--MarginRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_form__actions_child_MarginTop": {
      "name": "--pf-c-form__actions--child--MarginTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_form__actions_child_MarginRight": {
      "name": "--pf-c-form__actions--child--MarginRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_form__actions_child_MarginBottom": {
      "name": "--pf-c-form__actions--child--MarginBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_form__actions_child_MarginLeft": {
      "name": "--pf-c-form__actions--child--MarginLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_form__actions_MarginTop": {
      "name": "--pf-c-form__actions--MarginTop",
      "value": "calc(0.5rem * -1)",
      "values": [
        "calc(--pf-c-form__actions--child--MarginTop * -1)",
        "calc(--pf-global--spacer--sm * -1)",
        "calc($pf-global--spacer--sm * -1)",
        "calc(pf-size-prem(8px) * -1)",
        "calc(0.5rem * -1)"
      ]
    },
    "c_form__actions_MarginRight": {
      "name": "--pf-c-form__actions--MarginRight",
      "value": "calc(0.5rem * -1)",
      "values": [
        "calc(--pf-c-form__actions--child--MarginRight * -1)",
        "calc(--pf-global--spacer--sm * -1)",
        "calc($pf-global--spacer--sm * -1)",
        "calc(pf-size-prem(8px) * -1)",
        "calc(0.5rem * -1)"
      ]
    },
    "c_form__actions_MarginBottom": {
      "name": "--pf-c-form__actions--MarginBottom",
      "value": "calc(0.5rem * -1)",
      "values": [
        "calc(--pf-c-form__actions--child--MarginBottom * -1)",
        "calc(--pf-global--spacer--sm * -1)",
        "calc($pf-global--spacer--sm * -1)",
        "calc(pf-size-prem(8px) * -1)",
        "calc(0.5rem * -1)"
      ]
    },
    "c_form__actions_MarginLeft": {
      "name": "--pf-c-form__actions--MarginLeft",
      "value": "calc(0.5rem * -1)",
      "values": [
        "calc(--pf-c-form__actions--child--MarginLeft * -1)",
        "calc(--pf-global--spacer--sm * -1)",
        "calc($pf-global--spacer--sm * -1)",
        "calc(pf-size-prem(8px) * -1)",
        "calc(0.5rem * -1)"
      ]
    },
    "c_form__helper_text_MarginTop": {
      "name": "--pf-c-form__helper-text--MarginTop",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_form__helper_text_FontSize": {
      "name": "--pf-c-form__helper-text--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_form__helper_text_Color": {
      "name": "--pf-c-form__helper-text--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_form__helper_text_icon_FontSize": {
      "name": "--pf-c-form__helper-text-icon--FontSize",
      "value": "1rem",
      "values": [
        "--pf-global--FontSize--md",
        "$pf-global--FontSize--md",
        "pf-font-prem(16px)",
        "1rem"
      ]
    },
    "c_form__helper_text_icon_MarginRight": {
      "name": "--pf-c-form__helper-text-icon--MarginRight",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_form__helper_text_m_success_Color": {
      "name": "--pf-c-form__helper-text--m-success--Color",
      "value": "#0f280d",
      "values": [
        "--pf-global--success-color--200",
        "$pf-global--success-color--200",
        "$pf-color-green-700",
        "#0f280d"
      ]
    },
    "c_form__helper_text_m_warning_Color": {
      "name": "--pf-c-form__helper-text--m-warning--Color",
      "value": "#795600",
      "values": [
        "--pf-global--warning-color--200",
        "$pf-global--warning-color--200",
        "$pf-color-gold-600",
        "#795600"
      ]
    },
    "c_form__helper_text_m_error_Color": {
      "name": "--pf-c-form__helper-text--m-error--Color",
      "value": "#c9190b",
      "values": [
        "--pf-global--danger-color--100",
        "$pf-global--danger-color--100",
        "$pf-color-red-100",
        "#c9190b"
      ]
    }
  },
  ".pf-c-form.pf-m-horizontal": {
    "c_form__group_label_PaddingBottom": {
      "name": "--pf-c-form__group-label--PaddingBottom",
      "value": "0"
    }
  },
  ".pf-c-form__helper-text.pf-m-error": {
    "c_form__helper_text_Color": {
      "name": "--pf-c-form__helper-text--Color",
      "value": "#c9190b",
      "values": [
        "--pf-c-form__helper-text--m-error--Color",
        "--pf-global--danger-color--100",
        "$pf-global--danger-color--100",
        "$pf-color-red-100",
        "#c9190b"
      ]
    }
  },
  ".pf-c-form__helper-text.pf-m-success": {
    "c_form__helper_text_Color": {
      "name": "--pf-c-form__helper-text--Color",
      "value": "#0f280d",
      "values": [
        "--pf-c-form__helper-text--m-success--Color",
        "--pf-global--success-color--200",
        "$pf-global--success-color--200",
        "$pf-color-green-700",
        "#0f280d"
      ]
    }
  },
  ".pf-c-form__helper-text.pf-m-warning": {
    "c_form__helper_text_Color": {
      "name": "--pf-c-form__helper-text--Color",
      "value": "#795600",
      "values": [
        "--pf-c-form__helper-text--m-warning--Color",
        "--pf-global--warning-color--200",
        "$pf-global--warning-color--200",
        "$pf-color-gold-600",
        "#795600"
      ]
    }
  }
};
export default c_form;
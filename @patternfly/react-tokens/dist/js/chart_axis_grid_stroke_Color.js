"use strict";
exports.__esModule = true;
exports.chart_axis_grid_stroke_Color = {
  "name": "--pf-chart-axis--grid--stroke--Color",
  "value": "#d2d2d2",
  "var": "var(--pf-chart-axis--grid--stroke--Color)"
};
exports["default"] = exports.chart_axis_grid_stroke_Color;
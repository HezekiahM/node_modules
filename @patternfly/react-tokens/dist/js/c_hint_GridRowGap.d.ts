export const c_hint_GridRowGap: {
  "name": "--pf-c-hint--GridRowGap",
  "value": "1rem",
  "var": "var(--pf-c-hint--GridRowGap)"
};
export default c_hint_GridRowGap;
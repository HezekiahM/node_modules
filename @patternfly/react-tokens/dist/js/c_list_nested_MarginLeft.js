"use strict";
exports.__esModule = true;
exports.c_list_nested_MarginLeft = {
  "name": "--pf-c-list--nested--MarginLeft",
  "value": "0.5rem",
  "var": "var(--pf-c-list--nested--MarginLeft)"
};
exports["default"] = exports.c_list_nested_MarginLeft;
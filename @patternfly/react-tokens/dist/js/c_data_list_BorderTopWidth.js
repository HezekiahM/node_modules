"use strict";
exports.__esModule = true;
exports.c_data_list_BorderTopWidth = {
  "name": "--pf-c-data-list--BorderTopWidth",
  "value": "0.5rem",
  "var": "var(--pf-c-data-list--BorderTopWidth)"
};
exports["default"] = exports.c_data_list_BorderTopWidth;
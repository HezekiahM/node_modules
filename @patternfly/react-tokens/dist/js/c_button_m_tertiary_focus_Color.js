"use strict";
exports.__esModule = true;
exports.c_button_m_tertiary_focus_Color = {
  "name": "--pf-c-button--m-tertiary--focus--Color",
  "value": "#151515",
  "var": "var(--pf-c-button--m-tertiary--focus--Color)"
};
exports["default"] = exports.c_button_m_tertiary_focus_Color;
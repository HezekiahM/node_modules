"use strict";
exports.__esModule = true;
exports.c_page__main_breadcrumb_xl_PaddingRight = {
  "name": "--pf-c-page__main-breadcrumb--xl--PaddingRight",
  "value": "1.5rem",
  "var": "var(--pf-c-page__main-breadcrumb--xl--PaddingRight)"
};
exports["default"] = exports.c_page__main_breadcrumb_xl_PaddingRight;
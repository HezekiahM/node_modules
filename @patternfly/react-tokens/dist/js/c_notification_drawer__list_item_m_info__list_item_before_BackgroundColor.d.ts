export const c_notification_drawer__list_item_m_info__list_item_before_BackgroundColor: {
  "name": "--pf-c-notification-drawer__list-item--m-info__list-item--before--BackgroundColor",
  "value": "#2b9af3",
  "var": "var(--pf-c-notification-drawer__list-item--m-info__list-item--before--BackgroundColor)"
};
export default c_notification_drawer__list_item_m_info__list_item_before_BackgroundColor;
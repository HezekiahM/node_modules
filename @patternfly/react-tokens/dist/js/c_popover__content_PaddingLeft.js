"use strict";
exports.__esModule = true;
exports.c_popover__content_PaddingLeft = {
  "name": "--pf-c-popover__content--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-popover__content--PaddingLeft)"
};
exports["default"] = exports.c_popover__content_PaddingLeft;
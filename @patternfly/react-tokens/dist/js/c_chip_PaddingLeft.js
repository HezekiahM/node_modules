"use strict";
exports.__esModule = true;
exports.c_chip_PaddingLeft = {
  "name": "--pf-c-chip--PaddingLeft",
  "value": "0.5rem",
  "var": "var(--pf-c-chip--PaddingLeft)"
};
exports["default"] = exports.c_chip_PaddingLeft;
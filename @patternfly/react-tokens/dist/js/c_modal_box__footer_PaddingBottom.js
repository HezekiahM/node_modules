"use strict";
exports.__esModule = true;
exports.c_modal_box__footer_PaddingBottom = {
  "name": "--pf-c-modal-box__footer--PaddingBottom",
  "value": "1.5rem",
  "var": "var(--pf-c-modal-box__footer--PaddingBottom)"
};
exports["default"] = exports.c_modal_box__footer_PaddingBottom;
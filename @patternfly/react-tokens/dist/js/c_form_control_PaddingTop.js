"use strict";
exports.__esModule = true;
exports.c_form_control_PaddingTop = {
  "name": "--pf-c-form-control--PaddingTop",
  "value": "calc(0.375rem - 1px)",
  "var": "var(--pf-c-form-control--PaddingTop)"
};
exports["default"] = exports.c_form_control_PaddingTop;
"use strict";
exports.__esModule = true;
exports.c_form_control_invalid_BorderBottomWidth = {
  "name": "--pf-c-form-control--invalid--BorderBottomWidth",
  "value": "2px",
  "var": "var(--pf-c-form-control--invalid--BorderBottomWidth)"
};
exports["default"] = exports.c_form_control_invalid_BorderBottomWidth;
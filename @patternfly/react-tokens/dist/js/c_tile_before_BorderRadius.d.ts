export const c_tile_before_BorderRadius: {
  "name": "--pf-c-tile--before--BorderRadius",
  "value": "3px",
  "var": "var(--pf-c-tile--before--BorderRadius)"
};
export default c_tile_before_BorderRadius;
export const c_tabs__scroll_button_BackgroundColor: {
  "name": "--pf-c-tabs__scroll-button--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-tabs__scroll-button--BackgroundColor)"
};
export default c_tabs__scroll_button_BackgroundColor;
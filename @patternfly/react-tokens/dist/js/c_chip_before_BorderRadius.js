"use strict";
exports.__esModule = true;
exports.c_chip_before_BorderRadius = {
  "name": "--pf-c-chip--before--BorderRadius",
  "value": "3px",
  "var": "var(--pf-c-chip--before--BorderRadius)"
};
exports["default"] = exports.c_chip_before_BorderRadius;
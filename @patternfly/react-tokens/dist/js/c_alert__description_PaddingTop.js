"use strict";
exports.__esModule = true;
exports.c_alert__description_PaddingTop = {
  "name": "--pf-c-alert__description--PaddingTop",
  "value": "0.25rem",
  "var": "var(--pf-c-alert__description--PaddingTop)"
};
exports["default"] = exports.c_alert__description_PaddingTop;
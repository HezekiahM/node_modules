"use strict";
exports.__esModule = true;
exports.l_flex_AlignItems = {
  "name": "--pf-l-flex--AlignItems",
  "value": "baseline",
  "var": "var(--pf-l-flex--AlignItems)"
};
exports["default"] = exports.l_flex_AlignItems;
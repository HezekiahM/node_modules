"use strict";
exports.__esModule = true;
exports.chart_donut_threshold_static_pie_Height = {
  "name": "--pf-chart-donut--threshold--static--pie--Height",
  "value": 230,
  "var": "var(--pf-chart-donut--threshold--static--pie--Height)"
};
exports["default"] = exports.chart_donut_threshold_static_pie_Height;
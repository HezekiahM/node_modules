"use strict";
exports.__esModule = true;
exports.c_progress_GridGap = {
  "name": "--pf-c-progress--GridGap",
  "value": "1rem",
  "var": "var(--pf-c-progress--GridGap)"
};
exports["default"] = exports.c_progress_GridGap;
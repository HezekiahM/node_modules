"use strict";
exports.__esModule = true;
exports.global_palette_blue_50 = {
  "name": "--pf-global--palette--blue-50",
  "value": "#e7f1fa",
  "var": "var(--pf-global--palette--blue-50)"
};
exports["default"] = exports.global_palette_blue_50;
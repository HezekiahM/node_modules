"use strict";
exports.__esModule = true;
exports.c_button_m_control_BackgroundColor = {
  "name": "--pf-c-button--m-control--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-button--m-control--BackgroundColor)"
};
exports["default"] = exports.c_button_m_control_BackgroundColor;
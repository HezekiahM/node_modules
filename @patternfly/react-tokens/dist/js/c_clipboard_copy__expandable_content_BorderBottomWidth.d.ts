export const c_clipboard_copy__expandable_content_BorderBottomWidth: {
  "name": "--pf-c-clipboard-copy__expandable-content--BorderBottomWidth",
  "value": "1px",
  "var": "var(--pf-c-clipboard-copy__expandable-content--BorderBottomWidth)"
};
export default c_clipboard_copy__expandable_content_BorderBottomWidth;
"use strict";
exports.__esModule = true;
exports.c_popover_MinWidth = {
  "name": "--pf-c-popover--MinWidth",
  "value": "calc(1rem + 1rem + 18.75rem)",
  "var": "var(--pf-c-popover--MinWidth)"
};
exports["default"] = exports.c_popover_MinWidth;
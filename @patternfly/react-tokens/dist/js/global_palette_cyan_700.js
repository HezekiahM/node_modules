"use strict";
exports.__esModule = true;
exports.global_palette_cyan_700 = {
  "name": "--pf-global--palette--cyan-700",
  "value": "#000f0f",
  "var": "var(--pf-global--palette--cyan-700)"
};
exports["default"] = exports.global_palette_cyan_700;
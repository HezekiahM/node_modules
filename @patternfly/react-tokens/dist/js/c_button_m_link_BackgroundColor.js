"use strict";
exports.__esModule = true;
exports.c_button_m_link_BackgroundColor = {
  "name": "--pf-c-button--m-link--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-button--m-link--BackgroundColor)"
};
exports["default"] = exports.c_button_m_link_BackgroundColor;
"use strict";
exports.__esModule = true;
exports.c_switch__input_focus__toggle_OutlineWidth = {
  "name": "--pf-c-switch__input--focus__toggle--OutlineWidth",
  "value": "2px",
  "var": "var(--pf-c-switch__input--focus__toggle--OutlineWidth)"
};
exports["default"] = exports.c_switch__input_focus__toggle_OutlineWidth;
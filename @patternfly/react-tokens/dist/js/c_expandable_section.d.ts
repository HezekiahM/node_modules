export const c_expandable_section: {
  ".pf-c-expandable-section": {
    "c_expandable_section__toggle_PaddingTop": {
      "name": "--pf-c-expandable-section__toggle--PaddingTop",
      "value": "0.375rem",
      "values": [
        "--pf-global--spacer--form-element",
        "$pf-global--spacer--form-element",
        "pf-size-prem(6px)",
        "0.375rem"
      ]
    },
    "c_expandable_section__toggle_PaddingRight": {
      "name": "--pf-c-expandable-section__toggle--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_expandable_section__toggle_PaddingBottom": {
      "name": "--pf-c-expandable-section__toggle--PaddingBottom",
      "value": "0.375rem",
      "values": [
        "--pf-global--spacer--form-element",
        "$pf-global--spacer--form-element",
        "pf-size-prem(6px)",
        "0.375rem"
      ]
    },
    "c_expandable_section__toggle_PaddingLeft": {
      "name": "--pf-c-expandable-section__toggle--PaddingLeft",
      "value": "0"
    },
    "c_expandable_section__toggle_Color": {
      "name": "--pf-c-expandable-section__toggle--Color",
      "value": "#06c",
      "values": [
        "--pf-global--link--Color",
        "$pf-global--link--Color",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_expandable_section__toggle_hover_Color": {
      "name": "--pf-c-expandable-section__toggle--hover--Color",
      "value": "#004080",
      "values": [
        "--pf-global--link--Color--hover",
        "$pf-global--link--Color--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "c_expandable_section__toggle_active_Color": {
      "name": "--pf-c-expandable-section__toggle--active--Color",
      "value": "#004080",
      "values": [
        "--pf-global--link--Color--hover",
        "$pf-global--link--Color--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "c_expandable_section__toggle_focus_Color": {
      "name": "--pf-c-expandable-section__toggle--focus--Color",
      "value": "#004080",
      "values": [
        "--pf-global--link--Color--hover",
        "$pf-global--link--Color--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "c_expandable_section__toggle_m_expanded_Color": {
      "name": "--pf-c-expandable-section__toggle--m-expanded--Color",
      "value": "#004080",
      "values": [
        "--pf-global--link--Color--hover",
        "$pf-global--link--Color--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "c_expandable_section__toggle_icon_Color": {
      "name": "--pf-c-expandable-section__toggle-icon--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_expandable_section__toggle_icon_Transition": {
      "name": "--pf-c-expandable-section__toggle-icon--Transition",
      "value": ".2s ease-in 0s"
    },
    "c_expandable_section_m_expanded__toggle_icon_Rotate": {
      "name": "--pf-c-expandable-section--m-expanded__toggle-icon--Rotate",
      "value": "90deg"
    },
    "c_expandable_section__content_MarginTop": {
      "name": "--pf-c-expandable-section__content--MarginTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_expandable_section__toggle_text_MarginLeft": {
      "name": "--pf-c-expandable-section__toggle-text--MarginLeft",
      "value": "calc(0.25rem + 0.5rem)",
      "values": [
        "calc(--pf-global--spacer--xs + --pf-global--spacer--sm)",
        "calc($pf-global--spacer--xs + $pf-global--spacer--sm)",
        "calc(pf-size-prem(4px) + pf-size-prem(8px))",
        "calc(0.25rem + 0.5rem)"
      ]
    }
  },
  ".pf-c-expandable-section.pf-m-expanded": {
    "c_expandable_section__toggle_Color": {
      "name": "--pf-c-expandable-section__toggle--Color",
      "value": "#004080",
      "values": [
        "--pf-c-expandable-section__toggle--m-expanded--Color",
        "--pf-global--link--Color--hover",
        "$pf-global--link--Color--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    }
  },
  ".pf-c-expandable-section .pf-c-expandable-section__toggle:hover": {
    "c_expandable_section__toggle_Color": {
      "name": "--pf-c-expandable-section__toggle--Color",
      "value": "#004080",
      "values": [
        "--pf-c-expandable-section__toggle--hover--Color",
        "--pf-global--link--Color--hover",
        "$pf-global--link--Color--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    }
  },
  ".pf-c-expandable-section .pf-c-expandable-section__toggle:active": {
    "c_expandable_section__toggle_Color": {
      "name": "--pf-c-expandable-section__toggle--Color",
      "value": "#004080",
      "values": [
        "--pf-c-expandable-section__toggle--active--Color",
        "--pf-global--link--Color--hover",
        "$pf-global--link--Color--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    }
  },
  ".pf-c-expandable-section .pf-c-expandable-section__toggle:focus": {
    "c_expandable_section__toggle_Color": {
      "name": "--pf-c-expandable-section__toggle--Color",
      "value": "#004080",
      "values": [
        "--pf-c-expandable-section__toggle--focus--Color",
        "--pf-global--link--Color--hover",
        "$pf-global--link--Color--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    }
  }
};
export default c_expandable_section;
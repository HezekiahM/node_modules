"use strict";
exports.__esModule = true;
exports.l_grid__item_GridColumnStart = {
  "name": "--pf-l-grid__item--GridColumnStart",
  "value": "col-start calc(12 + 1)",
  "var": "var(--pf-l-grid__item--GridColumnStart)"
};
exports["default"] = exports.l_grid__item_GridColumnStart;
export const l_grid__item_GridColumnStart: {
  "name": "--pf-l-grid__item--GridColumnStart",
  "value": "col-start calc(12 + 1)",
  "var": "var(--pf-l-grid__item--GridColumnStart)"
};
export default l_grid__item_GridColumnStart;
"use strict";
exports.__esModule = true;
exports.c_description_list_RowGap = {
  "name": "--pf-c-description-list--RowGap",
  "value": "1.5rem",
  "var": "var(--pf-c-description-list--RowGap)"
};
exports["default"] = exports.c_description_list_RowGap;
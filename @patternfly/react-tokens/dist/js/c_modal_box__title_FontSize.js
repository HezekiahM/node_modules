"use strict";
exports.__esModule = true;
exports.c_modal_box__title_FontSize = {
  "name": "--pf-c-modal-box__title--FontSize",
  "value": "1.5rem",
  "var": "var(--pf-c-modal-box__title--FontSize)"
};
exports["default"] = exports.c_modal_box__title_FontSize;
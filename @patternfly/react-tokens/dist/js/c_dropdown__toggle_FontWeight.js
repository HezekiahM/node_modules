"use strict";
exports.__esModule = true;
exports.c_dropdown__toggle_FontWeight = {
  "name": "--pf-c-dropdown__toggle--FontWeight",
  "value": "400",
  "var": "var(--pf-c-dropdown__toggle--FontWeight)"
};
exports["default"] = exports.c_dropdown__toggle_FontWeight;
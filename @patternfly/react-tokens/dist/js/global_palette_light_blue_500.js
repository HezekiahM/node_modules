"use strict";
exports.__esModule = true;
exports.global_palette_light_blue_500 = {
  "name": "--pf-global--palette--light-blue-500",
  "value": "#008bad",
  "var": "var(--pf-global--palette--light-blue-500)"
};
exports["default"] = exports.global_palette_light_blue_500;
"use strict";
exports.__esModule = true;
exports.c_login__main_footer_links_item_MarginBottom = {
  "name": "--pf-c-login__main-footer-links-item--MarginBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-login__main-footer-links-item--MarginBottom)"
};
exports["default"] = exports.c_login__main_footer_links_item_MarginBottom;
"use strict";
exports.__esModule = true;
exports.c_radio__label_LineHeight = {
  "name": "--pf-c-radio__label--LineHeight",
  "value": "1.3",
  "var": "var(--pf-c-radio__label--LineHeight)"
};
exports["default"] = exports.c_radio__label_LineHeight;
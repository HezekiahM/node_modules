export const c_clipboard_copy: {
  ".pf-c-clipboard-copy": {
    "c_clipboard_copy__toggle_icon_Transition": {
      "name": "--pf-c-clipboard-copy__toggle-icon--Transition",
      "value": ".2s ease-in 0s"
    },
    "c_clipboard_copy_m_expanded__toggle_icon_Rotate": {
      "name": "--pf-c-clipboard-copy--m-expanded__toggle-icon--Rotate",
      "value": "90deg"
    },
    "c_clipboard_copy__expandable_content_PaddingTop": {
      "name": "--pf-c-clipboard-copy__expandable-content--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_clipboard_copy__expandable_content_PaddingRight": {
      "name": "--pf-c-clipboard-copy__expandable-content--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_clipboard_copy__expandable_content_PaddingBottom": {
      "name": "--pf-c-clipboard-copy__expandable-content--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_clipboard_copy__expandable_content_PaddingLeft": {
      "name": "--pf-c-clipboard-copy__expandable-content--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_clipboard_copy__expandable_content_BackgroundColor": {
      "name": "--pf-c-clipboard-copy__expandable-content--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--light-100",
        "$pf-global--BackgroundColor--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_clipboard_copy__expandable_content_BorderTopWidth": {
      "name": "--pf-c-clipboard-copy__expandable-content--BorderTopWidth",
      "value": "0"
    },
    "c_clipboard_copy__expandable_content_BorderRightWidth": {
      "name": "--pf-c-clipboard-copy__expandable-content--BorderRightWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_clipboard_copy__expandable_content_BorderBottomWidth": {
      "name": "--pf-c-clipboard-copy__expandable-content--BorderBottomWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_clipboard_copy__expandable_content_BorderLeftWidth": {
      "name": "--pf-c-clipboard-copy__expandable-content--BorderLeftWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_clipboard_copy__expandable_content_BorderColor": {
      "name": "--pf-c-clipboard-copy__expandable-content--BorderColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_clipboard_copy__expandable_content_OutlineOffset": {
      "name": "--pf-c-clipboard-copy__expandable-content--OutlineOffset",
      "value": "calc(-1 * 0.25rem)",
      "values": [
        "calc(-1 * --pf-global--spacer--xs)",
        "calc(-1 * $pf-global--spacer--xs)",
        "calc(-1 * pf-size-prem(4px))",
        "calc(-1 * 0.25rem)"
      ]
    }
  }
};
export default c_clipboard_copy;
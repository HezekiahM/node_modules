"use strict";
exports.__esModule = true;
exports.c_tile__icon_Color = {
  "name": "--pf-c-tile__icon--Color",
  "value": "#151515",
  "var": "var(--pf-c-tile__icon--Color)"
};
exports["default"] = exports.c_tile__icon_Color;
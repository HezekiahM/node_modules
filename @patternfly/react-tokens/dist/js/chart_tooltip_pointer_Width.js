"use strict";
exports.__esModule = true;
exports.chart_tooltip_pointer_Width = {
  "name": "--pf-chart-tooltip--pointer--Width",
  "value": 20,
  "var": "var(--pf-chart-tooltip--pointer--Width)"
};
exports["default"] = exports.chart_tooltip_pointer_Width;
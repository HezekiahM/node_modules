"use strict";
exports.__esModule = true;
exports.c_app_launcher__toggle_active_Color = {
  "name": "--pf-c-app-launcher__toggle--active--Color",
  "value": "#151515",
  "var": "var(--pf-c-app-launcher__toggle--active--Color)"
};
exports["default"] = exports.c_app_launcher__toggle_active_Color;
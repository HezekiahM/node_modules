"use strict";
exports.__esModule = true;
exports.chart_color_blue_100 = {
  "name": "--pf-chart-color-blue-100",
  "value": "#8bc1f7",
  "var": "var(--pf-chart-color-blue-100)"
};
exports["default"] = exports.chart_color_blue_100;
"use strict";
exports.__esModule = true;
exports.c_options_menu_m_top_m_expanded__toggle_icon_Rotate = {
  "name": "--pf-c-options-menu--m-top--m-expanded__toggle-icon--Rotate",
  "value": "180deg",
  "var": "var(--pf-c-options-menu--m-top--m-expanded__toggle-icon--Rotate)"
};
exports["default"] = exports.c_options_menu_m_top_m_expanded__toggle_icon_Rotate;
export const global_gutter_md: {
  "name": "--pf-global--gutter--md",
  "value": "1.5rem",
  "var": "var(--pf-global--gutter--md)"
};
export default global_gutter_md;
export const c_button_PaddingLeft: {
  "name": "--pf-c-button--PaddingLeft",
  "value": "0.5rem",
  "var": "var(--pf-c-button--PaddingLeft)"
};
export default c_button_PaddingLeft;
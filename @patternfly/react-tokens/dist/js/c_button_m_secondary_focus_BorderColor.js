"use strict";
exports.__esModule = true;
exports.c_button_m_secondary_focus_BorderColor = {
  "name": "--pf-c-button--m-secondary--focus--BorderColor",
  "value": "#fff",
  "var": "var(--pf-c-button--m-secondary--focus--BorderColor)"
};
exports["default"] = exports.c_button_m_secondary_focus_BorderColor;
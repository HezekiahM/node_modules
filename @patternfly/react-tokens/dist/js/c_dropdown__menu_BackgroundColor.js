"use strict";
exports.__esModule = true;
exports.c_dropdown__menu_BackgroundColor = {
  "name": "--pf-c-dropdown__menu--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-dropdown__menu--BackgroundColor)"
};
exports["default"] = exports.c_dropdown__menu_BackgroundColor;
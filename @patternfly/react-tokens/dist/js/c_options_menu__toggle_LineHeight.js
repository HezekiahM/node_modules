"use strict";
exports.__esModule = true;
exports.c_options_menu__toggle_LineHeight = {
  "name": "--pf-c-options-menu__toggle--LineHeight",
  "value": "1.5",
  "var": "var(--pf-c-options-menu__toggle--LineHeight)"
};
exports["default"] = exports.c_options_menu__toggle_LineHeight;
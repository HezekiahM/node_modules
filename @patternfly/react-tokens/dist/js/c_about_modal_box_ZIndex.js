"use strict";
exports.__esModule = true;
exports.c_about_modal_box_ZIndex = {
  "name": "--pf-c-about-modal-box--ZIndex",
  "value": "500",
  "var": "var(--pf-c-about-modal-box--ZIndex)"
};
exports["default"] = exports.c_about_modal_box_ZIndex;
export const c_options_menu__toggle_BorderLeftColor: {
  "name": "--pf-c-options-menu__toggle--BorderLeftColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-options-menu__toggle--BorderLeftColor)"
};
export default c_options_menu__toggle_BorderLeftColor;
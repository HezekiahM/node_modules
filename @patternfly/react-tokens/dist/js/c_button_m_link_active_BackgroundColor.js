"use strict";
exports.__esModule = true;
exports.c_button_m_link_active_BackgroundColor = {
  "name": "--pf-c-button--m-link--active--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-button--m-link--active--BackgroundColor)"
};
exports["default"] = exports.c_button_m_link_active_BackgroundColor;
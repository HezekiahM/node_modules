"use strict";
exports.__esModule = true;
exports.c_about_modal_box__content_MarginBottom = {
  "name": "--pf-c-about-modal-box__content--MarginBottom",
  "value": "2rem",
  "var": "var(--pf-c-about-modal-box__content--MarginBottom)"
};
exports["default"] = exports.c_about_modal_box__content_MarginBottom;
"use strict";
exports.__esModule = true;
exports.c_empty_state_PaddingBottom = {
  "name": "--pf-c-empty-state--PaddingBottom",
  "value": "2rem",
  "var": "var(--pf-c-empty-state--PaddingBottom)"
};
exports["default"] = exports.c_empty_state_PaddingBottom;
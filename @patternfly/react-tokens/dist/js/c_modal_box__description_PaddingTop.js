"use strict";
exports.__esModule = true;
exports.c_modal_box__description_PaddingTop = {
  "name": "--pf-c-modal-box__description--PaddingTop",
  "value": "0.25rem",
  "var": "var(--pf-c-modal-box__description--PaddingTop)"
};
exports["default"] = exports.c_modal_box__description_PaddingTop;
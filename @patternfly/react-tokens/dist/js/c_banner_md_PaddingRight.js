"use strict";
exports.__esModule = true;
exports.c_banner_md_PaddingRight = {
  "name": "--pf-c-banner--md--PaddingRight",
  "value": "1.5rem",
  "var": "var(--pf-c-banner--md--PaddingRight)"
};
exports["default"] = exports.c_banner_md_PaddingRight;
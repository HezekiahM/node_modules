export const c_expandable_section__toggle_active_Color: {
  "name": "--pf-c-expandable-section__toggle--active--Color",
  "value": "#004080",
  "var": "var(--pf-c-expandable-section__toggle--active--Color)"
};
export default c_expandable_section__toggle_active_Color;
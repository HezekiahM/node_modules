export const c_form_control_focus_BorderBottomWidth: {
  "name": "--pf-c-form-control--focus--BorderBottomWidth",
  "value": "1px",
  "var": "var(--pf-c-form-control--focus--BorderBottomWidth)"
};
export default c_form_control_focus_BorderBottomWidth;
"use strict";
exports.__esModule = true;
exports.c_context_selector__menu_search_BorderBottomColor = {
  "name": "--pf-c-context-selector__menu-search--BorderBottomColor",
  "value": "#d2d2d2",
  "var": "var(--pf-c-context-selector__menu-search--BorderBottomColor)"
};
exports["default"] = exports.c_context_selector__menu_search_BorderBottomColor;
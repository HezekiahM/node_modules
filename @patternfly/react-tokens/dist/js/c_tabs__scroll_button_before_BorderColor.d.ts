export const c_tabs__scroll_button_before_BorderColor: {
  "name": "--pf-c-tabs__scroll-button--before--BorderColor",
  "value": "#d2d2d2",
  "var": "var(--pf-c-tabs__scroll-button--before--BorderColor)"
};
export default c_tabs__scroll_button_before_BorderColor;
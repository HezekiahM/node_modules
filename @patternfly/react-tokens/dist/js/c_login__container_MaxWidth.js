"use strict";
exports.__esModule = true;
exports.c_login__container_MaxWidth = {
  "name": "--pf-c-login__container--MaxWidth",
  "value": "31.25rem",
  "var": "var(--pf-c-login__container--MaxWidth)"
};
exports["default"] = exports.c_login__container_MaxWidth;
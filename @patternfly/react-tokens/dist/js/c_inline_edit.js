"use strict";
exports.__esModule = true;
exports.c_inline_edit = {
  ".pf-c-inline-edit": {
    "c_inline_edit__group_item_MarginRight": {
      "name": "--pf-c-inline-edit__group--item--MarginRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_inline_edit__action_c_button_m_valid_m_plain_Color": {
      "name": "--pf-c-inline-edit__action--c-button--m-valid--m-plain--Color",
      "value": "#06c",
      "values": [
        "--pf-global--link--Color",
        "$pf-global--link--Color",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_inline_edit__action_c_button_m_valid_m_plain_hover_Color": {
      "name": "--pf-c-inline-edit__action--c-button--m-valid--m-plain--hover--Color",
      "value": "#004080",
      "values": [
        "--pf-global--link--Color--hover",
        "$pf-global--link--Color--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "c_inline_edit__action_m_icon_group_item_MarginRight": {
      "name": "--pf-c-inline-edit__action--m-icon-group--item--MarginRight",
      "value": "0"
    },
    "c_inline_edit__group_m_footer_MarginTop": {
      "name": "--pf-c-inline-edit__group--m-footer--MarginTop",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_inline_edit__label_m_bold_FontWeight": {
      "name": "--pf-c-inline-edit__label--m-bold--FontWeight",
      "value": "700",
      "values": [
        "--pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    }
  },
  ".pf-c-inline-edit__group.pf-m-icon-group": {
    "c_inline_edit__group_item_MarginRight": {
      "name": "--pf-c-inline-edit__group--item--MarginRight",
      "value": "0",
      "values": [
        "--pf-c-inline-edit__action--m-icon-group--item--MarginRight",
        "0"
      ]
    }
  },
  ".pf-c-inline-edit__group.pf-m-column": {
    "c_inline_edit__group_item_MarginRight": {
      "name": "--pf-c-inline-edit__group--item--MarginRight",
      "value": "0"
    }
  },
  ".pf-c-inline-edit__group > :last-child": {
    "c_inline_edit__group_item_MarginRight": {
      "name": "--pf-c-inline-edit__group--item--MarginRight",
      "value": "0"
    }
  },
  ".pf-c-inline-edit__action.pf-m-valid .pf-c-button.pf-m-plain": {
    "c_button_m_plain_Color": {
      "name": "--pf-c-button--m-plain--Color",
      "value": "#06c",
      "values": [
        "--pf-c-inline-edit__action--c-button--m-valid--m-plain--Color",
        "--pf-global--link--Color",
        "$pf-global--link--Color",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-inline-edit__action.pf-m-valid .pf-c-button.pf-m-plain:hover": {
    "c_button_m_plain_Color": {
      "name": "--pf-c-button--m-plain--Color",
      "value": "#004080",
      "values": [
        "--pf-c-inline-edit__action--c-button--m-valid--m-plain--hover--Color",
        "--pf-global--link--Color--hover",
        "$pf-global--link--Color--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    }
  }
};
exports["default"] = exports.c_inline_edit;
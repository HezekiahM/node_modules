export const global_disabled_color_100: {
  "name": "--pf-global--disabled-color--100",
  "value": "#6a6e73",
  "var": "var(--pf-global--disabled-color--100)"
};
export default global_disabled_color_100;
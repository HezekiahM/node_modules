export const c_login__main_footer_band_BackgroundColor: {
  "name": "--pf-c-login__main-footer-band--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-login__main-footer-band--BackgroundColor)"
};
export default c_login__main_footer_band_BackgroundColor;
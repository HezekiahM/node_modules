export const c_form_control_BorderTopColor: {
  "name": "--pf-c-form-control--BorderTopColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-form-control--BorderTopColor)"
};
export default c_form_control_BorderTopColor;
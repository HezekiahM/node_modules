"use strict";
exports.__esModule = true;
exports.c_label__c_button_FontSize = {
  "name": "--pf-c-label__c-button--FontSize",
  "value": "0.75rem",
  "var": "var(--pf-c-label__c-button--FontSize)"
};
exports["default"] = exports.c_label__c_button_FontSize;
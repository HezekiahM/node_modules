"use strict";
exports.__esModule = true;
exports.c_form_control_readonly_focus_BorderBottomWidth = {
  "name": "--pf-c-form-control--readonly--focus--BorderBottomWidth",
  "value": "1px",
  "var": "var(--pf-c-form-control--readonly--focus--BorderBottomWidth)"
};
exports["default"] = exports.c_form_control_readonly_focus_BorderBottomWidth;
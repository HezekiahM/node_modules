"use strict";
exports.__esModule = true;
exports.global_ZIndex_sm = {
  "name": "--pf-global--ZIndex--sm",
  "value": "200",
  "var": "var(--pf-global--ZIndex--sm)"
};
exports["default"] = exports.global_ZIndex_sm;
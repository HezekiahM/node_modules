export const c_alert__icon_MarginTop: {
  "name": "--pf-c-alert__icon--MarginTop",
  "value": "0.0625rem",
  "var": "var(--pf-c-alert__icon--MarginTop)"
};
export default c_alert__icon_MarginTop;
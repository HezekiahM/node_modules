"use strict";
exports.__esModule = true;
exports.l_flex_spacer_2xl = {
  "name": "--pf-l-flex--spacer--2xl",
  "value": "3rem",
  "var": "var(--pf-l-flex--spacer--2xl)"
};
exports["default"] = exports.l_flex_spacer_2xl;
"use strict";
exports.__esModule = true;
exports.c_about_modal_box__content_MarginLeft = {
  "name": "--pf-c-about-modal-box__content--MarginLeft",
  "value": "2rem",
  "var": "var(--pf-c-about-modal-box__content--MarginLeft)"
};
exports["default"] = exports.c_about_modal_box__content_MarginLeft;
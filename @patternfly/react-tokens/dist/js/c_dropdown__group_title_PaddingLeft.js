"use strict";
exports.__esModule = true;
exports.c_dropdown__group_title_PaddingLeft = {
  "name": "--pf-c-dropdown__group-title--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-dropdown__group-title--PaddingLeft)"
};
exports["default"] = exports.c_dropdown__group_title_PaddingLeft;
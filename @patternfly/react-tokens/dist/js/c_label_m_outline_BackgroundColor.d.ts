export const c_label_m_outline_BackgroundColor: {
  "name": "--pf-c-label--m-outline--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-label--m-outline--BackgroundColor)"
};
export default c_label_m_outline_BackgroundColor;
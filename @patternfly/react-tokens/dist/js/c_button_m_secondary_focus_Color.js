"use strict";
exports.__esModule = true;
exports.c_button_m_secondary_focus_Color = {
  "name": "--pf-c-button--m-secondary--focus--Color",
  "value": "#fff",
  "var": "var(--pf-c-button--m-secondary--focus--Color)"
};
exports["default"] = exports.c_button_m_secondary_focus_Color;
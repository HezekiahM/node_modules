"use strict";
exports.__esModule = true;
exports.global_link_TextDecoration_hover = {
  "name": "--pf-global--link--TextDecoration--hover",
  "value": "underline",
  "var": "var(--pf-global--link--TextDecoration--hover)"
};
exports["default"] = exports.global_link_TextDecoration_hover;
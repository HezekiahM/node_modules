"use strict";
exports.__esModule = true;
exports.c_wizard__title_PaddingRight = {
  "name": "--pf-c-wizard__title--PaddingRight",
  "value": "3rem",
  "var": "var(--pf-c-wizard__title--PaddingRight)"
};
exports["default"] = exports.c_wizard__title_PaddingRight;
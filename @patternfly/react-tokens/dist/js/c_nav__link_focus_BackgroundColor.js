"use strict";
exports.__esModule = true;
exports.c_nav__link_focus_BackgroundColor = {
  "name": "--pf-c-nav__link--focus--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-nav__link--focus--BackgroundColor)"
};
exports["default"] = exports.c_nav__link_focus_BackgroundColor;
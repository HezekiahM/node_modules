"use strict";
exports.__esModule = true;
exports.chart_boxplot_upper_quartile_Padding = {
  "name": "--pf-chart-boxplot--upper-quartile--Padding",
  "value": 8,
  "var": "var(--pf-chart-boxplot--upper-quartile--Padding)"
};
exports["default"] = exports.chart_boxplot_upper_quartile_Padding;
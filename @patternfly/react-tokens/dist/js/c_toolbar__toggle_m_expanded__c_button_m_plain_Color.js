"use strict";
exports.__esModule = true;
exports.c_toolbar__toggle_m_expanded__c_button_m_plain_Color = {
  "name": "--pf-c-toolbar__toggle--m-expanded__c-button--m-plain--Color",
  "value": "#151515",
  "var": "var(--pf-c-toolbar__toggle--m-expanded__c-button--m-plain--Color)"
};
exports["default"] = exports.c_toolbar__toggle_m_expanded__c_button_m_plain_Color;
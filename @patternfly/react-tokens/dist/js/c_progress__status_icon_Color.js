"use strict";
exports.__esModule = true;
exports.c_progress__status_icon_Color = {
  "name": "--pf-c-progress__status-icon--Color",
  "value": "#c9190b",
  "var": "var(--pf-c-progress__status-icon--Color)"
};
exports["default"] = exports.c_progress__status_icon_Color;
export const global_secondary_color_100: {
  "name": "--pf-global--secondary-color--100",
  "value": "#6a6e73",
  "var": "var(--pf-global--secondary-color--100)"
};
export default global_secondary_color_100;
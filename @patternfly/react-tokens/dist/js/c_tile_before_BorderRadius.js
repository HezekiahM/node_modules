"use strict";
exports.__esModule = true;
exports.c_tile_before_BorderRadius = {
  "name": "--pf-c-tile--before--BorderRadius",
  "value": "3px",
  "var": "var(--pf-c-tile--before--BorderRadius)"
};
exports["default"] = exports.c_tile_before_BorderRadius;
export const c_modal_box__header_PaddingTop: {
  "name": "--pf-c-modal-box__header--PaddingTop",
  "value": "1.5rem",
  "var": "var(--pf-c-modal-box__header--PaddingTop)"
};
export default c_modal_box__header_PaddingTop;
export const c_wizard__footer_xl_PaddingLeft: {
  "name": "--pf-c-wizard__footer--xl--PaddingLeft",
  "value": "1.5rem",
  "var": "var(--pf-c-wizard__footer--xl--PaddingLeft)"
};
export default c_wizard__footer_xl_PaddingLeft;
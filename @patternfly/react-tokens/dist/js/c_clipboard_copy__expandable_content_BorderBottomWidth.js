"use strict";
exports.__esModule = true;
exports.c_clipboard_copy__expandable_content_BorderBottomWidth = {
  "name": "--pf-c-clipboard-copy__expandable-content--BorderBottomWidth",
  "value": "1px",
  "var": "var(--pf-c-clipboard-copy__expandable-content--BorderBottomWidth)"
};
exports["default"] = exports.c_clipboard_copy__expandable_content_BorderBottomWidth;
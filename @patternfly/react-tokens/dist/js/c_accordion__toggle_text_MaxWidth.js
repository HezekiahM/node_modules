"use strict";
exports.__esModule = true;
exports.c_accordion__toggle_text_MaxWidth = {
  "name": "--pf-c-accordion__toggle-text--MaxWidth",
  "value": "calc(100% - 1.5rem)",
  "var": "var(--pf-c-accordion__toggle-text--MaxWidth)"
};
exports["default"] = exports.c_accordion__toggle_text_MaxWidth;
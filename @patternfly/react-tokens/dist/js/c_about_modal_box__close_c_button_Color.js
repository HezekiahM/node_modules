"use strict";
exports.__esModule = true;
exports.c_about_modal_box__close_c_button_Color = {
  "name": "--pf-c-about-modal-box__close--c-button--Color",
  "value": "#151515",
  "var": "var(--pf-c-about-modal-box__close--c-button--Color)"
};
exports["default"] = exports.c_about_modal_box__close_c_button_Color;
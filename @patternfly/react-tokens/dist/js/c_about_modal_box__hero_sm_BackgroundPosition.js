"use strict";
exports.__esModule = true;
exports.c_about_modal_box__hero_sm_BackgroundPosition = {
  "name": "--pf-c-about-modal-box__hero--sm--BackgroundPosition",
  "value": "top left",
  "var": "var(--pf-c-about-modal-box__hero--sm--BackgroundPosition)"
};
exports["default"] = exports.c_about_modal_box__hero_sm_BackgroundPosition;
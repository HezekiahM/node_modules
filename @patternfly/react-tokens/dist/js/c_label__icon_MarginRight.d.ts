export const c_label__icon_MarginRight: {
  "name": "--pf-c-label__icon--MarginRight",
  "value": "0.25rem",
  "var": "var(--pf-c-label__icon--MarginRight)"
};
export default c_label__icon_MarginRight;
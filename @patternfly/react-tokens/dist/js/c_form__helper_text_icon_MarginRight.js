"use strict";
exports.__esModule = true;
exports.c_form__helper_text_icon_MarginRight = {
  "name": "--pf-c-form__helper-text-icon--MarginRight",
  "value": "0.25rem",
  "var": "var(--pf-c-form__helper-text-icon--MarginRight)"
};
exports["default"] = exports.c_form__helper_text_icon_MarginRight;
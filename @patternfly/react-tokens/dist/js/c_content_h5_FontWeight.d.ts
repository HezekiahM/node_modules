export const c_content_h5_FontWeight: {
  "name": "--pf-c-content--h5--FontWeight",
  "value": "700",
  "var": "var(--pf-c-content--h5--FontWeight)"
};
export default c_content_h5_FontWeight;
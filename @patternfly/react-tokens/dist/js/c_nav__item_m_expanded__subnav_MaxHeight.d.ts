export const c_nav__item_m_expanded__subnav_MaxHeight: {
  "name": "--pf-c-nav__item--m-expanded__subnav--MaxHeight",
  "value": "100%",
  "var": "var(--pf-c-nav__item--m-expanded__subnav--MaxHeight)"
};
export default c_nav__item_m_expanded__subnav_MaxHeight;
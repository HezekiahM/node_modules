"use strict";
exports.__esModule = true;
exports.c_content_ol_nested_MarginTop = {
  "name": "--pf-c-content--ol--nested--MarginTop",
  "value": "0.5rem",
  "var": "var(--pf-c-content--ol--nested--MarginTop)"
};
exports["default"] = exports.c_content_ol_nested_MarginTop;
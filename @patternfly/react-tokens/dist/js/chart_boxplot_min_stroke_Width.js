"use strict";
exports.__esModule = true;
exports.chart_boxplot_min_stroke_Width = {
  "name": "--pf-chart-boxplot--min--stroke--Width",
  "value": 1,
  "var": "var(--pf-chart-boxplot--min--stroke--Width)"
};
exports["default"] = exports.chart_boxplot_min_stroke_Width;
"use strict";
exports.__esModule = true;
exports.c_tile__header_m_stacked__icon_FontSize = {
  "name": "--pf-c-tile__header--m-stacked__icon--FontSize",
  "value": "1.5rem",
  "var": "var(--pf-c-tile__header--m-stacked__icon--FontSize)"
};
exports["default"] = exports.c_tile__header_m_stacked__icon_FontSize;
export const c_nav__subnav__link_active_after_BorderColor: {
  "name": "--pf-c-nav__subnav__link--active--after--BorderColor",
  "value": "#d2d2d2",
  "var": "var(--pf-c-nav__subnav__link--active--after--BorderColor)"
};
export default c_nav__subnav__link_active_after_BorderColor;
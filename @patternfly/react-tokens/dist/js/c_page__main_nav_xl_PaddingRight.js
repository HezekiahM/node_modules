"use strict";
exports.__esModule = true;
exports.c_page__main_nav_xl_PaddingRight = {
  "name": "--pf-c-page__main-nav--xl--PaddingRight",
  "value": "0.5rem",
  "var": "var(--pf-c-page__main-nav--xl--PaddingRight)"
};
exports["default"] = exports.c_page__main_nav_xl_PaddingRight;
"use strict";
exports.__esModule = true;
exports.c_app_launcher__menu_item_m_action_FontSize = {
  "name": "--pf-c-app-launcher__menu-item--m-action--FontSize",
  "value": "0.625rem",
  "var": "var(--pf-c-app-launcher__menu-item--m-action--FontSize)"
};
exports["default"] = exports.c_app_launcher__menu_item_m_action_FontSize;
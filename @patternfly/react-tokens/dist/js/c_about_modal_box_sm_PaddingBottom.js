"use strict";
exports.__esModule = true;
exports.c_about_modal_box_sm_PaddingBottom = {
  "name": "--pf-c-about-modal-box--sm--PaddingBottom",
  "value": "4rem",
  "var": "var(--pf-c-about-modal-box--sm--PaddingBottom)"
};
exports["default"] = exports.c_about_modal_box_sm_PaddingBottom;
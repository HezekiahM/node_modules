"use strict";
exports.__esModule = true;
exports.chart_color_purple_200 = {
  "name": "--pf-chart-color-purple-200",
  "value": "#8481dd",
  "var": "var(--pf-chart-color-purple-200)"
};
exports["default"] = exports.chart_color_purple_200;
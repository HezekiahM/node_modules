"use strict";
exports.__esModule = true;
exports.c_options_menu__group_title_FontWeight = {
  "name": "--pf-c-options-menu__group-title--FontWeight",
  "value": "700",
  "var": "var(--pf-c-options-menu__group-title--FontWeight)"
};
exports["default"] = exports.c_options_menu__group_title_FontWeight;
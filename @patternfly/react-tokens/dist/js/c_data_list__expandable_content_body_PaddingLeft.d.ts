export const c_data_list__expandable_content_body_PaddingLeft: {
  "name": "--pf-c-data-list__expandable-content-body--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-data-list__expandable-content-body--PaddingLeft)"
};
export default c_data_list__expandable_content_body_PaddingLeft;
"use strict";
exports.__esModule = true;
exports.global_FontFamily_overpass_monospace = {
  "name": "--pf-global--FontFamily--overpass--monospace",
  "value": "overpass-mono, overpass-mono, SFMono-Regular, menlo, monaco, consolas, Liberation Mono, Courier New, monospace",
  "var": "var(--pf-global--FontFamily--overpass--monospace)"
};
exports["default"] = exports.global_FontFamily_overpass_monospace;
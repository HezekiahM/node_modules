export const c_button_active_after_BorderWidth: {
  "name": "--pf-c-button--active--after--BorderWidth",
  "value": "2px",
  "var": "var(--pf-c-button--active--after--BorderWidth)"
};
export default c_button_active_after_BorderWidth;
"use strict";
exports.__esModule = true;
exports.c_wizard__footer_PaddingTop = {
  "name": "--pf-c-wizard__footer--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-wizard__footer--PaddingTop)"
};
exports["default"] = exports.c_wizard__footer_PaddingTop;
"use strict";
exports.__esModule = true;
exports.c_tile_m_selected_before_BorderWidth = {
  "name": "--pf-c-tile--m-selected--before--BorderWidth",
  "value": "2px",
  "var": "var(--pf-c-tile--m-selected--before--BorderWidth)"
};
exports["default"] = exports.c_tile_m_selected_before_BorderWidth;
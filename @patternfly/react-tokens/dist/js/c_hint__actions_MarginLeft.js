"use strict";
exports.__esModule = true;
exports.c_hint__actions_MarginLeft = {
  "name": "--pf-c-hint__actions--MarginLeft",
  "value": "3rem",
  "var": "var(--pf-c-hint__actions--MarginLeft)"
};
exports["default"] = exports.c_hint__actions_MarginLeft;
"use strict";
exports.__esModule = true;
exports.chart_color_cyan_500 = {
  "name": "--pf-chart-color-cyan-500",
  "value": "#003737",
  "var": "var(--pf-chart-color-cyan-500)"
};
exports["default"] = exports.chart_color_cyan_500;
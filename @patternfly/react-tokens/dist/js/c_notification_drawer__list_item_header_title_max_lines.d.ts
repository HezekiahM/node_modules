export const c_notification_drawer__list_item_header_title_max_lines: {
  "name": "--pf-c-notification-drawer__list-item-header-title--max-lines",
  "value": "1",
  "var": "var(--pf-c-notification-drawer__list-item-header-title--max-lines)"
};
export default c_notification_drawer__list_item_header_title_max_lines;
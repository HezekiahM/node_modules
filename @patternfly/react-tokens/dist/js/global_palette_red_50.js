"use strict";
exports.__esModule = true;
exports.global_palette_red_50 = {
  "name": "--pf-global--palette--red-50",
  "value": "#faeae8",
  "var": "var(--pf-global--palette--red-50)"
};
exports["default"] = exports.global_palette_red_50;
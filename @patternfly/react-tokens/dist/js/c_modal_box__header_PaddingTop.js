"use strict";
exports.__esModule = true;
exports.c_modal_box__header_PaddingTop = {
  "name": "--pf-c-modal-box__header--PaddingTop",
  "value": "1.5rem",
  "var": "var(--pf-c-modal-box__header--PaddingTop)"
};
exports["default"] = exports.c_modal_box__header_PaddingTop;
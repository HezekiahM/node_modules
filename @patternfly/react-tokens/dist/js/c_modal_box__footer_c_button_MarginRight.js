"use strict";
exports.__esModule = true;
exports.c_modal_box__footer_c_button_MarginRight = {
  "name": "--pf-c-modal-box__footer--c-button--MarginRight",
  "value": "1rem",
  "var": "var(--pf-c-modal-box__footer--c-button--MarginRight)"
};
exports["default"] = exports.c_modal_box__footer_c_button_MarginRight;
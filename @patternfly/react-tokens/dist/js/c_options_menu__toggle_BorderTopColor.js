"use strict";
exports.__esModule = true;
exports.c_options_menu__toggle_BorderTopColor = {
  "name": "--pf-c-options-menu__toggle--BorderTopColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-options-menu__toggle--BorderTopColor)"
};
exports["default"] = exports.c_options_menu__toggle_BorderTopColor;
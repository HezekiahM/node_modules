"use strict";
exports.__esModule = true;
exports.c_select__menu_item_disabled_Color = {
  "name": "--pf-c-select__menu-item--disabled--Color",
  "value": "#6a6e73",
  "var": "var(--pf-c-select__menu-item--disabled--Color)"
};
exports["default"] = exports.c_select__menu_item_disabled_Color;
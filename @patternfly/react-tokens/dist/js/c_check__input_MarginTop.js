"use strict";
exports.__esModule = true;
exports.c_check__input_MarginTop = {
  "name": "--pf-c-check__input--MarginTop",
  "value": "-0.1875rem",
  "var": "var(--pf-c-check__input--MarginTop)"
};
exports["default"] = exports.c_check__input_MarginTop;
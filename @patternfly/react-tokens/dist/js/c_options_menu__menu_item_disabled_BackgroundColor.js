"use strict";
exports.__esModule = true;
exports.c_options_menu__menu_item_disabled_BackgroundColor = {
  "name": "--pf-c-options-menu__menu-item--disabled--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-options-menu__menu-item--disabled--BackgroundColor)"
};
exports["default"] = exports.c_options_menu__menu_item_disabled_BackgroundColor;
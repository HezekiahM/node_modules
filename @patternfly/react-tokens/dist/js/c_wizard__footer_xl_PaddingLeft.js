"use strict";
exports.__esModule = true;
exports.c_wizard__footer_xl_PaddingLeft = {
  "name": "--pf-c-wizard__footer--xl--PaddingLeft",
  "value": "1.5rem",
  "var": "var(--pf-c-wizard__footer--xl--PaddingLeft)"
};
exports["default"] = exports.c_wizard__footer_xl_PaddingLeft;
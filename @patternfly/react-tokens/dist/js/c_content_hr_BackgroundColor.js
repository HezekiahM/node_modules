"use strict";
exports.__esModule = true;
exports.c_content_hr_BackgroundColor = {
  "name": "--pf-c-content--hr--BackgroundColor",
  "value": "#d2d2d2",
  "var": "var(--pf-c-content--hr--BackgroundColor)"
};
exports["default"] = exports.c_content_hr_BackgroundColor;
"use strict";
exports.__esModule = true;
exports.c_hint_BorderWidth = {
  "name": "--pf-c-hint--BorderWidth",
  "value": "1px",
  "var": "var(--pf-c-hint--BorderWidth)"
};
exports["default"] = exports.c_hint_BorderWidth;
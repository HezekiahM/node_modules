"use strict";
exports.__esModule = true;
exports.c_empty_state__icon_MarginBottom = {
  "name": "--pf-c-empty-state__icon--MarginBottom",
  "value": "2rem",
  "var": "var(--pf-c-empty-state__icon--MarginBottom)"
};
exports["default"] = exports.c_empty_state__icon_MarginBottom;
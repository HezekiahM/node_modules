"use strict";
exports.__esModule = true;
exports.global_FontWeight_overpass_bold = {
  "name": "--pf-global--FontWeight--overpass--bold",
  "value": "600",
  "var": "var(--pf-global--FontWeight--overpass--bold)"
};
exports["default"] = exports.global_FontWeight_overpass_bold;
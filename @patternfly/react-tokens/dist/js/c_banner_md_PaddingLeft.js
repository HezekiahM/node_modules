"use strict";
exports.__esModule = true;
exports.c_banner_md_PaddingLeft = {
  "name": "--pf-c-banner--md--PaddingLeft",
  "value": "1.5rem",
  "var": "var(--pf-c-banner--md--PaddingLeft)"
};
exports["default"] = exports.c_banner_md_PaddingLeft;
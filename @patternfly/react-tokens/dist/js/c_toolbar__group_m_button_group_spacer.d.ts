export const c_toolbar__group_m_button_group_spacer: {
  "name": "--pf-c-toolbar__group--m-button-group--spacer",
  "value": "1rem",
  "var": "var(--pf-c-toolbar__group--m-button-group--spacer)"
};
export default c_toolbar__group_m_button_group_spacer;
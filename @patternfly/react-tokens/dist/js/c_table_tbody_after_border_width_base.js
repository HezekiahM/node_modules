"use strict";
exports.__esModule = true;
exports.c_table_tbody_after_border_width_base = {
  "name": "--pf-c-table--tbody--after--border-width--base",
  "value": "3px",
  "var": "var(--pf-c-table--tbody--after--border-width--base)"
};
exports["default"] = exports.c_table_tbody_after_border_width_base;
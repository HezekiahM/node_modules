"use strict";
exports.__esModule = true;
exports.c_form__helper_text_m_success_Color = {
  "name": "--pf-c-form__helper-text--m-success--Color",
  "value": "#0f280d",
  "var": "var(--pf-c-form__helper-text--m-success--Color)"
};
exports["default"] = exports.c_form__helper_text_m_success_Color;
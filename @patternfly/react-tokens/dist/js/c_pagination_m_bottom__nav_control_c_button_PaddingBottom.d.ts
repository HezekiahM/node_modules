export const c_pagination_m_bottom__nav_control_c_button_PaddingBottom: {
  "name": "--pf-c-pagination--m-bottom__nav-control--c-button--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-pagination--m-bottom__nav-control--c-button--PaddingBottom)"
};
export default c_pagination_m_bottom__nav_control_c_button_PaddingBottom;
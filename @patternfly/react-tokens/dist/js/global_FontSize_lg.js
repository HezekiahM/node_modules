"use strict";
exports.__esModule = true;
exports.global_FontSize_lg = {
  "name": "--pf-global--FontSize--lg",
  "value": "1.125rem",
  "var": "var(--pf-global--FontSize--lg)"
};
exports["default"] = exports.global_FontSize_lg;
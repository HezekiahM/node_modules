"use strict";
exports.__esModule = true;
exports.global_palette_red_500 = {
  "name": "--pf-global--palette--red-500",
  "value": "#2c0000",
  "var": "var(--pf-global--palette--red-500)"
};
exports["default"] = exports.global_palette_red_500;
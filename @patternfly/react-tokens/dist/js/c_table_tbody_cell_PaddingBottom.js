"use strict";
exports.__esModule = true;
exports.c_table_tbody_cell_PaddingBottom = {
  "name": "--pf-c-table--tbody--cell--PaddingBottom",
  "value": "1.5rem",
  "var": "var(--pf-c-table--tbody--cell--PaddingBottom)"
};
exports["default"] = exports.c_table_tbody_cell_PaddingBottom;
export const c_label_m_orange__content_link_focus_before_BorderColor: {
  "name": "--pf-c-label--m-orange__content--link--focus--before--BorderColor",
  "value": "#ec7a08",
  "var": "var(--pf-c-label--m-orange__content--link--focus--before--BorderColor)"
};
export default c_label_m_orange__content_link_focus_before_BorderColor;
"use strict";
exports.__esModule = true;
exports.chart_global_label_Margin = {
  "name": "--pf-chart-global--label--Margin",
  "value": 8,
  "var": "var(--pf-chart-global--label--Margin)"
};
exports["default"] = exports.chart_global_label_Margin;
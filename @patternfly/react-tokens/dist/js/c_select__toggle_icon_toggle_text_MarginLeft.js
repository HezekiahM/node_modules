"use strict";
exports.__esModule = true;
exports.c_select__toggle_icon_toggle_text_MarginLeft = {
  "name": "--pf-c-select__toggle-icon--toggle-text--MarginLeft",
  "value": "0.25rem",
  "var": "var(--pf-c-select__toggle-icon--toggle-text--MarginLeft)"
};
exports["default"] = exports.c_select__toggle_icon_toggle_text_MarginLeft;
"use strict";
exports.__esModule = true;
exports.chart_bullet_axis_tick_count = {
  "name": "--pf-chart-bullet--axis--tick--count",
  "value": 5,
  "var": "var(--pf-chart-bullet--axis--tick--count)"
};
exports["default"] = exports.chart_bullet_axis_tick_count;
"use strict";
exports.__esModule = true;
exports.chart_legend_title_orientation = {
  "name": "--pf-chart-legend--title--orientation",
  "value": "top",
  "var": "var(--pf-chart-legend--title--orientation)"
};
exports["default"] = exports.chart_legend_title_orientation;
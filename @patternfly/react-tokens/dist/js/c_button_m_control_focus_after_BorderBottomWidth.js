"use strict";
exports.__esModule = true;
exports.c_button_m_control_focus_after_BorderBottomWidth = {
  "name": "--pf-c-button--m-control--focus--after--BorderBottomWidth",
  "value": "2px",
  "var": "var(--pf-c-button--m-control--focus--after--BorderBottomWidth)"
};
exports["default"] = exports.c_button_m_control_focus_after_BorderBottomWidth;
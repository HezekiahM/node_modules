"use strict";
exports.__esModule = true;
exports.c_wizard__nav_link_m_current_before_Color = {
  "name": "--pf-c-wizard__nav-link--m-current--before--Color",
  "value": "#fff",
  "var": "var(--pf-c-wizard__nav-link--m-current--before--Color)"
};
exports["default"] = exports.c_wizard__nav_link_m_current_before_Color;
export const c_label_m_blue__content_Color: {
  "name": "--pf-c-label--m-blue__content--Color",
  "value": "#002952",
  "var": "var(--pf-c-label--m-blue__content--Color)"
};
export default c_label_m_blue__content_Color;
"use strict";
exports.__esModule = true;
exports.global_breakpoint_xs = {
  "name": "--pf-global--breakpoint--xs",
  "value": "0",
  "var": "var(--pf-global--breakpoint--xs)"
};
exports["default"] = exports.global_breakpoint_xs;
"use strict";
exports.__esModule = true;
exports.chart_tooltip_pointer_length = {
  "name": "--pf-chart-tooltip--pointer-length",
  "value": 10,
  "var": "var(--pf-chart-tooltip--pointer-length)"
};
exports["default"] = exports.chart_tooltip_pointer_length;
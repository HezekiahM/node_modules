"use strict";
exports.__esModule = true;
exports.c_label_m_orange__icon_Color = {
  "name": "--pf-c-label--m-orange__icon--Color",
  "value": "#ec7a08",
  "var": "var(--pf-c-label--m-orange__icon--Color)"
};
exports["default"] = exports.c_label_m_orange__icon_Color;
"use strict";
exports.__esModule = true;
exports.chart_donut_utilization_dynamic_pie_Padding = {
  "name": "--pf-chart-donut--utilization--dynamic--pie--Padding",
  "value": 20,
  "var": "var(--pf-chart-donut--utilization--dynamic--pie--Padding)"
};
exports["default"] = exports.chart_donut_utilization_dynamic_pie_Padding;
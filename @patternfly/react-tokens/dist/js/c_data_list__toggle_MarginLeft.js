"use strict";
exports.__esModule = true;
exports.c_data_list__toggle_MarginLeft = {
  "name": "--pf-c-data-list__toggle--MarginLeft",
  "value": "calc(0.5rem * -1)",
  "var": "var(--pf-c-data-list__toggle--MarginLeft)"
};
exports["default"] = exports.c_data_list__toggle_MarginLeft;
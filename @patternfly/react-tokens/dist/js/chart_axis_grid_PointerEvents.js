"use strict";
exports.__esModule = true;
exports.chart_axis_grid_PointerEvents = {
  "name": "--pf-chart-axis--grid--PointerEvents",
  "value": "painted",
  "var": "var(--pf-chart-axis--grid--PointerEvents)"
};
exports["default"] = exports.chart_axis_grid_PointerEvents;
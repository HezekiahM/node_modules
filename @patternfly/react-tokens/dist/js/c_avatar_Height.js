"use strict";
exports.__esModule = true;
exports.c_avatar_Height = {
  "name": "--pf-c-avatar--Height",
  "value": "2.25rem",
  "var": "var(--pf-c-avatar--Height)"
};
exports["default"] = exports.c_avatar_Height;
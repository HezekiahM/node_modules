"use strict";
exports.__esModule = true;
exports.chart_axis_axis_label_stroke_Color = {
  "name": "--pf-chart-axis--axis-label--stroke--Color",
  "value": "transparent",
  "var": "var(--pf-chart-axis--axis-label--stroke--Color)"
};
exports["default"] = exports.chart_axis_axis_label_stroke_Color;
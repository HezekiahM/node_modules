"use strict";
exports.__esModule = true;
exports.c_progress_m_success__status_icon_Color = {
  "name": "--pf-c-progress--m-success__status-icon--Color",
  "value": "#3e8635",
  "var": "var(--pf-c-progress--m-success__status-icon--Color)"
};
exports["default"] = exports.c_progress_m_success__status_icon_Color;
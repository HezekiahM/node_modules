"use strict";
exports.__esModule = true;
exports.c_progress__bar_Height = {
  "name": "--pf-c-progress__bar--Height",
  "value": "1.5rem",
  "var": "var(--pf-c-progress__bar--Height)"
};
exports["default"] = exports.c_progress__bar_Height;
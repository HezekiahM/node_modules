export const c_table_cell_TextOverflow: {
  "name": "--pf-c-table--cell--TextOverflow",
  "value": "clip",
  "var": "var(--pf-c-table--cell--TextOverflow)"
};
export default c_table_cell_TextOverflow;
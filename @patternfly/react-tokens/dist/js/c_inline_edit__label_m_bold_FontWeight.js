"use strict";
exports.__esModule = true;
exports.c_inline_edit__label_m_bold_FontWeight = {
  "name": "--pf-c-inline-edit__label--m-bold--FontWeight",
  "value": "700",
  "var": "var(--pf-c-inline-edit__label--m-bold--FontWeight)"
};
exports["default"] = exports.c_inline_edit__label_m_bold_FontWeight;
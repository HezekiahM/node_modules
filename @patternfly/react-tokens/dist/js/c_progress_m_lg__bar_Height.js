"use strict";
exports.__esModule = true;
exports.c_progress_m_lg__bar_Height = {
  "name": "--pf-c-progress--m-lg__bar--Height",
  "value": "1.5rem",
  "var": "var(--pf-c-progress--m-lg__bar--Height)"
};
exports["default"] = exports.c_progress_m_lg__bar_Height;
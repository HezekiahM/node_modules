"use strict";
exports.__esModule = true;
exports.chart_donut_threshold_danger_Color = {
  "name": "--pf-chart-donut--threshold--danger--Color",
  "value": "#c9190b",
  "var": "var(--pf-chart-donut--threshold--danger--Color)"
};
exports["default"] = exports.chart_donut_threshold_danger_Color;
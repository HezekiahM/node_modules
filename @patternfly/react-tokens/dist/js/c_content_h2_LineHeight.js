"use strict";
exports.__esModule = true;
exports.c_content_h2_LineHeight = {
  "name": "--pf-c-content--h2--LineHeight",
  "value": "1.3",
  "var": "var(--pf-c-content--h2--LineHeight)"
};
exports["default"] = exports.c_content_h2_LineHeight;
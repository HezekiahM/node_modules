export const c_label__c_button_PaddingLeft: {
  "name": "--pf-c-label__c-button--PaddingLeft",
  "value": "0.5rem",
  "var": "var(--pf-c-label__c-button--PaddingLeft)"
};
export default c_label__c_button_PaddingLeft;
"use strict";
exports.__esModule = true;
exports.c_table_caption_xl_PaddingRight = {
  "name": "--pf-c-table-caption--xl--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-table-caption--xl--PaddingRight)"
};
exports["default"] = exports.c_table_caption_xl_PaddingRight;
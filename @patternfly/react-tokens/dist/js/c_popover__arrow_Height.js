"use strict";
exports.__esModule = true;
exports.c_popover__arrow_Height = {
  "name": "--pf-c-popover__arrow--Height",
  "value": "1.5625rem",
  "var": "var(--pf-c-popover__arrow--Height)"
};
exports["default"] = exports.c_popover__arrow_Height;
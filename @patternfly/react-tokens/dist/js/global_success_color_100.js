"use strict";
exports.__esModule = true;
exports.global_success_color_100 = {
  "name": "--pf-global--success-color--100",
  "value": "#3e8635",
  "var": "var(--pf-global--success-color--100)"
};
exports["default"] = exports.global_success_color_100;
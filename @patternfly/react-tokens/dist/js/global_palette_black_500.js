"use strict";
exports.__esModule = true;
exports.global_palette_black_500 = {
  "name": "--pf-global--palette--black-500",
  "value": "#8a8d90",
  "var": "var(--pf-global--palette--black-500)"
};
exports["default"] = exports.global_palette_black_500;
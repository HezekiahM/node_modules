export const c_nav_m_tertiary__link_focus_BackgroundColor: {
  "name": "--pf-c-nav--m-tertiary__link--focus--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-nav--m-tertiary__link--focus--BackgroundColor)"
};
export default c_nav_m_tertiary__link_focus_BackgroundColor;
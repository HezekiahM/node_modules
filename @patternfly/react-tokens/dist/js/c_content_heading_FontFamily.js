"use strict";
exports.__esModule = true;
exports.c_content_heading_FontFamily = {
  "name": "--pf-c-content--heading--FontFamily",
  "value": "\"RedHatDisplay\", \"Overpass\", overpass, helvetica, arial, sans-serif",
  "var": "var(--pf-c-content--heading--FontFamily)"
};
exports["default"] = exports.c_content_heading_FontFamily;
"use strict";
exports.__esModule = true;
exports.c_accordion__expanded_content_FontSize = {
  "name": "--pf-c-accordion__expanded-content--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-accordion__expanded-content--FontSize)"
};
exports["default"] = exports.c_accordion__expanded_content_FontSize;
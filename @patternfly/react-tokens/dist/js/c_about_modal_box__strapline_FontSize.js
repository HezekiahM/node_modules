"use strict";
exports.__esModule = true;
exports.c_about_modal_box__strapline_FontSize = {
  "name": "--pf-c-about-modal-box__strapline--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-about-modal-box__strapline--FontSize)"
};
exports["default"] = exports.c_about_modal_box__strapline_FontSize;
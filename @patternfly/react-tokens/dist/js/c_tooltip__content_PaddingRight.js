"use strict";
exports.__esModule = true;
exports.c_tooltip__content_PaddingRight = {
  "name": "--pf-c-tooltip__content--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-tooltip__content--PaddingRight)"
};
exports["default"] = exports.c_tooltip__content_PaddingRight;
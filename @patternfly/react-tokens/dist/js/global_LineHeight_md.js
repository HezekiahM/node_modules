"use strict";
exports.__esModule = true;
exports.global_LineHeight_md = {
  "name": "--pf-global--LineHeight--md",
  "value": "1.5",
  "var": "var(--pf-global--LineHeight--md)"
};
exports["default"] = exports.global_LineHeight_md;
export const c_wizard__header_lg_PaddingRight: {
  "name": "--pf-c-wizard__header--lg--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-wizard__header--lg--PaddingRight)"
};
export default c_wizard__header_lg_PaddingRight;
export const c_label__content_link_hover_before_BorderColor: {
  "name": "--pf-c-label__content--link--hover--before--BorderColor",
  "value": "#009596",
  "var": "var(--pf-c-label__content--link--hover--before--BorderColor)"
};
export default c_label__content_link_hover_before_BorderColor;
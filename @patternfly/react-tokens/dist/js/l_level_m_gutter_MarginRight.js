"use strict";
exports.__esModule = true;
exports.l_level_m_gutter_MarginRight = {
  "name": "--pf-l-level--m-gutter--MarginRight",
  "value": "1rem",
  "var": "var(--pf-l-level--m-gutter--MarginRight)"
};
exports["default"] = exports.l_level_m_gutter_MarginRight;
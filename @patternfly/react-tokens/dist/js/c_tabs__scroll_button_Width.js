"use strict";
exports.__esModule = true;
exports.c_tabs__scroll_button_Width = {
  "name": "--pf-c-tabs__scroll-button--Width",
  "value": "3rem",
  "var": "var(--pf-c-tabs__scroll-button--Width)"
};
exports["default"] = exports.c_tabs__scroll_button_Width;
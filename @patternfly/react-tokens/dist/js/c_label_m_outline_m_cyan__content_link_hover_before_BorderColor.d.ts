export const c_label_m_outline_m_cyan__content_link_hover_before_BorderColor: {
  "name": "--pf-c-label--m-outline--m-cyan__content--link--hover--before--BorderColor",
  "value": "#a2d9d9",
  "var": "var(--pf-c-label--m-outline--m-cyan__content--link--hover--before--BorderColor)"
};
export default c_label_m_outline_m_cyan__content_link_hover_before_BorderColor;
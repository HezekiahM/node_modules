"use strict";
exports.__esModule = true;
exports.c_options_menu__group_group_PaddingTop = {
  "name": "--pf-c-options-menu__group--group--PaddingTop",
  "value": "0.5rem",
  "var": "var(--pf-c-options-menu__group--group--PaddingTop)"
};
exports["default"] = exports.c_options_menu__group_group_PaddingTop;
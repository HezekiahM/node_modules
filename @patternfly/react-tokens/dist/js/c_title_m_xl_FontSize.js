"use strict";
exports.__esModule = true;
exports.c_title_m_xl_FontSize = {
  "name": "--pf-c-title--m-xl--FontSize",
  "value": "1.25rem",
  "var": "var(--pf-c-title--m-xl--FontSize)"
};
exports["default"] = exports.c_title_m_xl_FontSize;
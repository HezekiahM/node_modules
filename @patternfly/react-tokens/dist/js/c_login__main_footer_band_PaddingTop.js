"use strict";
exports.__esModule = true;
exports.c_login__main_footer_band_PaddingTop = {
  "name": "--pf-c-login__main-footer-band--PaddingTop",
  "value": "1.5rem",
  "var": "var(--pf-c-login__main-footer-band--PaddingTop)"
};
exports["default"] = exports.c_login__main_footer_band_PaddingTop;
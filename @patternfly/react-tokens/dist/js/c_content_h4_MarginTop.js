"use strict";
exports.__esModule = true;
exports.c_content_h4_MarginTop = {
  "name": "--pf-c-content--h4--MarginTop",
  "value": "1.5rem",
  "var": "var(--pf-c-content--h4--MarginTop)"
};
exports["default"] = exports.c_content_h4_MarginTop;
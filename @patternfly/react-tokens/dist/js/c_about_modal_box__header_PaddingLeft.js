"use strict";
exports.__esModule = true;
exports.c_about_modal_box__header_PaddingLeft = {
  "name": "--pf-c-about-modal-box__header--PaddingLeft",
  "value": "2rem",
  "var": "var(--pf-c-about-modal-box__header--PaddingLeft)"
};
exports["default"] = exports.c_about_modal_box__header_PaddingLeft;
"use strict";
exports.__esModule = true;
exports.c_label_m_outline_m_blue__content_link_focus_before_BorderColor = {
  "name": "--pf-c-label--m-outline--m-blue__content--link--focus--before--BorderColor",
  "value": "#bee1f4",
  "var": "var(--pf-c-label--m-outline--m-blue__content--link--focus--before--BorderColor)"
};
exports["default"] = exports.c_label_m_outline_m_blue__content_link_focus_before_BorderColor;
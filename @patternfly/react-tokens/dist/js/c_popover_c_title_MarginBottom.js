"use strict";
exports.__esModule = true;
exports.c_popover_c_title_MarginBottom = {
  "name": "--pf-c-popover--c-title--MarginBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-popover--c-title--MarginBottom)"
};
exports["default"] = exports.c_popover_c_title_MarginBottom;
"use strict";
exports.__esModule = true;
exports.c_button_m_control_after_BorderTopColor = {
  "name": "--pf-c-button--m-control--after--BorderTopColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-button--m-control--after--BorderTopColor)"
};
exports["default"] = exports.c_button_m_control_after_BorderTopColor;
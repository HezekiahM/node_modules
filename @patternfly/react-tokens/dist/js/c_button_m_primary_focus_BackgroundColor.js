"use strict";
exports.__esModule = true;
exports.c_button_m_primary_focus_BackgroundColor = {
  "name": "--pf-c-button--m-primary--focus--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-button--m-primary--focus--BackgroundColor)"
};
exports["default"] = exports.c_button_m_primary_focus_BackgroundColor;
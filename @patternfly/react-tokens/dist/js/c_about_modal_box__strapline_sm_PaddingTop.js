"use strict";
exports.__esModule = true;
exports.c_about_modal_box__strapline_sm_PaddingTop = {
  "name": "--pf-c-about-modal-box__strapline--sm--PaddingTop",
  "value": "3rem",
  "var": "var(--pf-c-about-modal-box__strapline--sm--PaddingTop)"
};
exports["default"] = exports.c_about_modal_box__strapline_sm_PaddingTop;
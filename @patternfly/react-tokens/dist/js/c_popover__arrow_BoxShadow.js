"use strict";
exports.__esModule = true;
exports.c_popover__arrow_BoxShadow = {
  "name": "--pf-c-popover__arrow--BoxShadow",
  "value": "0 0.25rem 0.5rem 0rem rgba(3, 3, 3, 0.12), 0 0 0.25rem 0 rgba(3, 3, 3, 0.06)",
  "var": "var(--pf-c-popover__arrow--BoxShadow)"
};
exports["default"] = exports.c_popover__arrow_BoxShadow;
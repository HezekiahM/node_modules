"use strict";
exports.__esModule = true;
exports.c_empty_state_PaddingTop = {
  "name": "--pf-c-empty-state--PaddingTop",
  "value": "2rem",
  "var": "var(--pf-c-empty-state--PaddingTop)"
};
exports["default"] = exports.c_empty_state_PaddingTop;
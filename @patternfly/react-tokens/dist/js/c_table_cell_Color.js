"use strict";
exports.__esModule = true;
exports.c_table_cell_Color = {
  "name": "--pf-c-table--cell--Color",
  "value": "#151515",
  "var": "var(--pf-c-table--cell--Color)"
};
exports["default"] = exports.c_table_cell_Color;
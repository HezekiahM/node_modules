"use strict";
exports.__esModule = true;
exports.c_alert_m_info_BorderTopColor = {
  "name": "--pf-c-alert--m-info--BorderTopColor",
  "value": "#2b9af3",
  "var": "var(--pf-c-alert--m-info--BorderTopColor)"
};
exports["default"] = exports.c_alert_m_info_BorderTopColor;
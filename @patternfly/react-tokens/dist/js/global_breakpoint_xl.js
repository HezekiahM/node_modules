"use strict";
exports.__esModule = true;
exports.global_breakpoint_xl = {
  "name": "--pf-global--breakpoint--xl",
  "value": "1200px",
  "var": "var(--pf-global--breakpoint--xl)"
};
exports["default"] = exports.global_breakpoint_xl;
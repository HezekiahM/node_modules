export const chart_color_black_500: {
  "name": "--pf-chart-color-black-500",
  "value": "#6a6e73",
  "var": "var(--pf-chart-color-black-500)"
};
export default chart_color_black_500;
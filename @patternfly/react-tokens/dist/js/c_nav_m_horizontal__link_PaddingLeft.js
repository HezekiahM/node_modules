"use strict";
exports.__esModule = true;
exports.c_nav_m_horizontal__link_PaddingLeft = {
  "name": "--pf-c-nav--m-horizontal__link--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-nav--m-horizontal__link--PaddingLeft)"
};
exports["default"] = exports.c_nav_m_horizontal__link_PaddingLeft;
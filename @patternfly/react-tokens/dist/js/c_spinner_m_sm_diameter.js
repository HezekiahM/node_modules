"use strict";
exports.__esModule = true;
exports.c_spinner_m_sm_diameter = {
  "name": "--pf-c-spinner--m-sm--diameter",
  "value": "0.625rem",
  "var": "var(--pf-c-spinner--m-sm--diameter)"
};
exports["default"] = exports.c_spinner_m_sm_diameter;
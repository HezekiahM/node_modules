export const c_page__main_section_xl_PaddingLeft: {
  "name": "--pf-c-page__main-section--xl--PaddingLeft",
  "value": "1.5rem",
  "var": "var(--pf-c-page__main-section--xl--PaddingLeft)"
};
export default c_page__main_section_xl_PaddingLeft;
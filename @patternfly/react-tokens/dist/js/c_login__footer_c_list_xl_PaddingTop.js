"use strict";
exports.__esModule = true;
exports.c_login__footer_c_list_xl_PaddingTop = {
  "name": "--pf-c-login__footer--c-list--xl--PaddingTop",
  "value": "3rem",
  "var": "var(--pf-c-login__footer--c-list--xl--PaddingTop)"
};
exports["default"] = exports.c_login__footer_c_list_xl_PaddingTop;
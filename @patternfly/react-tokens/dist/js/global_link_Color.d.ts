export const global_link_Color: {
  "name": "--pf-global--link--Color",
  "value": "#73bcf7",
  "var": "var(--pf-global--link--Color)"
};
export default global_link_Color;
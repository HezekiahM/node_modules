"use strict";
exports.__esModule = true;
exports.c_table__check_input_FontSize = {
  "name": "--pf-c-table__check--input--FontSize",
  "value": "1rem",
  "var": "var(--pf-c-table__check--input--FontSize)"
};
exports["default"] = exports.c_table__check_input_FontSize;
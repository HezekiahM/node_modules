"use strict";
exports.__esModule = true;
exports.c_table_tr_responsive_nested_table_PaddingTop = {
  "name": "--pf-c-table-tr--responsive--nested-table--PaddingTop",
  "value": "2rem",
  "var": "var(--pf-c-table-tr--responsive--nested-table--PaddingTop)"
};
exports["default"] = exports.c_table_tr_responsive_nested_table_PaddingTop;
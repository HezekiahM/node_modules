"use strict";
exports.__esModule = true;
exports.c_form_control_success_BorderBottomWidth = {
  "name": "--pf-c-form-control--success--BorderBottomWidth",
  "value": "2px",
  "var": "var(--pf-c-form-control--success--BorderBottomWidth)"
};
exports["default"] = exports.c_form_control_success_BorderBottomWidth;
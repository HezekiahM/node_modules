"use strict";
exports.__esModule = true;
exports.c_accordion__expanded_content_body_PaddingLeft = {
  "name": "--pf-c-accordion__expanded-content-body--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-accordion__expanded-content-body--PaddingLeft)"
};
exports["default"] = exports.c_accordion__expanded_content_body_PaddingLeft;
"use strict";
exports.__esModule = true;
exports.c_accordion__toggle_PaddingRight = {
  "name": "--pf-c-accordion__toggle--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-accordion__toggle--PaddingRight)"
};
exports["default"] = exports.c_accordion__toggle_PaddingRight;
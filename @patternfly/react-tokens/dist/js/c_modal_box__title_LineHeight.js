"use strict";
exports.__esModule = true;
exports.c_modal_box__title_LineHeight = {
  "name": "--pf-c-modal-box__title--LineHeight",
  "value": "1.3",
  "var": "var(--pf-c-modal-box__title--LineHeight)"
};
exports["default"] = exports.c_modal_box__title_LineHeight;
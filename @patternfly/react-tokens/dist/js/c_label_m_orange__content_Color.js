"use strict";
exports.__esModule = true;
exports.c_label_m_orange__content_Color = {
  "name": "--pf-c-label--m-orange__content--Color",
  "value": "#3d2c00",
  "var": "var(--pf-c-label--m-orange__content--Color)"
};
exports["default"] = exports.c_label_m_orange__content_Color;
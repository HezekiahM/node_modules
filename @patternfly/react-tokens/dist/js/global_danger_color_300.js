"use strict";
exports.__esModule = true;
exports.global_danger_color_300 = {
  "name": "--pf-global--danger-color--300",
  "value": "#470000",
  "var": "var(--pf-global--danger-color--300)"
};
exports["default"] = exports.global_danger_color_300;
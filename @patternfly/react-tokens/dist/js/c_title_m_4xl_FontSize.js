"use strict";
exports.__esModule = true;
exports.c_title_m_4xl_FontSize = {
  "name": "--pf-c-title--m-4xl--FontSize",
  "value": "2.25rem",
  "var": "var(--pf-c-title--m-4xl--FontSize)"
};
exports["default"] = exports.c_title_m_4xl_FontSize;
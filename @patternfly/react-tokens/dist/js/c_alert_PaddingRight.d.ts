export const c_alert_PaddingRight: {
  "name": "--pf-c-alert--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-alert--PaddingRight)"
};
export default c_alert_PaddingRight;
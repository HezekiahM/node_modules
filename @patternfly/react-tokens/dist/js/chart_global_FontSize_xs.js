"use strict";
exports.__esModule = true;
exports.chart_global_FontSize_xs = {
  "name": "--pf-chart-global--FontSize--xs",
  "value": 12,
  "var": "var(--pf-chart-global--FontSize--xs)"
};
exports["default"] = exports.chart_global_FontSize_xs;
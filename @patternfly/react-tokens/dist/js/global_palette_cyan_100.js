"use strict";
exports.__esModule = true;
exports.global_palette_cyan_100 = {
  "name": "--pf-global--palette--cyan-100",
  "value": "#a2d9d9",
  "var": "var(--pf-global--palette--cyan-100)"
};
exports["default"] = exports.global_palette_cyan_100;
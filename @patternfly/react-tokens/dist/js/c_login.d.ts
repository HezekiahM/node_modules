export const c_login: {
  ".pf-c-login__header": {
    "global_Color_100": {
      "name": "--pf-global--Color--100",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "global_Color_200": {
      "name": "--pf-global--Color--200",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--Color--light-200",
        "$pf-global--Color--light-200",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "global_BorderColor_100": {
      "name": "--pf-global--BorderColor--100",
      "value": "#b8bbbe",
      "values": [
        "--pf-global--BorderColor--light-100",
        "$pf-global--BorderColor--light-100",
        "$pf-color-black-400",
        "#b8bbbe"
      ]
    },
    "global_primary_color_100": {
      "name": "--pf-global--primary-color--100",
      "value": "#73bcf7",
      "values": [
        "--pf-global--primary-color--light-100",
        "$pf-global--primary-color--light-100",
        "$pf-color-blue-200",
        "#73bcf7"
      ]
    },
    "global_link_Color": {
      "name": "--pf-global--link--Color",
      "value": "#73bcf7",
      "values": [
        "--pf-global--link--Color--light",
        "$pf-global--link--Color--light",
        "$pf-global--active-color--300",
        "$pf-color-blue-200",
        "#73bcf7"
      ]
    },
    "global_link_Color_hover": {
      "name": "--pf-global--link--Color--hover",
      "value": "#73bcf7",
      "values": [
        "--pf-global--link--Color--light",
        "$pf-global--link--Color--light",
        "$pf-global--active-color--300",
        "$pf-color-blue-200",
        "#73bcf7"
      ]
    },
    "global_BackgroundColor_100": {
      "name": "--pf-global--BackgroundColor--100",
      "value": "#151515",
      "values": [
        "--pf-global--BackgroundColor--dark-100",
        "$pf-global--BackgroundColor--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    }
  },
  ".pf-c-login__header .pf-c-card": {
    "c_card_BackgroundColor": {
      "name": "--pf-c-card--BackgroundColor",
      "value": "rgba(#030303, .32)",
      "values": [
        "--pf-global--BackgroundColor--dark-transparent-200",
        "$pf-global--BackgroundColor--dark-transparent-200",
        "rgba($pf-color-black-1000, .32)",
        "rgba(#030303, .32)"
      ]
    }
  },
  ".pf-c-login__header .pf-c-button": {
    "c_button_m_primary_Color": {
      "name": "--pf-c-button--m-primary--Color",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--dark-100",
        "$pf-global--primary-color--dark-100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_primary_hover_Color": {
      "name": "--pf-c-button--m-primary--hover--Color",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--dark-100",
        "$pf-global--primary-color--dark-100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_primary_focus_Color": {
      "name": "--pf-c-button--m-primary--focus--Color",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--dark-100",
        "$pf-global--primary-color--dark-100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_primary_active_Color": {
      "name": "--pf-c-button--m-primary--active--Color",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--dark-100",
        "$pf-global--primary-color--dark-100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_primary_BackgroundColor": {
      "name": "--pf-c-button--m-primary--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--light-100",
        "$pf-global--BackgroundColor--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_primary_hover_BackgroundColor": {
      "name": "--pf-c-button--m-primary--hover--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_button_m_primary_focus_BackgroundColor": {
      "name": "--pf-c-button--m-primary--focus--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_button_m_primary_active_BackgroundColor": {
      "name": "--pf-c-button--m-primary--active--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_button_m_secondary_Color": {
      "name": "--pf-c-button--m-secondary--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_hover_Color": {
      "name": "--pf-c-button--m-secondary--hover--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_focus_Color": {
      "name": "--pf-c-button--m-secondary--focus--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_active_Color": {
      "name": "--pf-c-button--m-secondary--active--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_BorderColor": {
      "name": "--pf-c-button--m-secondary--BorderColor",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_hover_BorderColor": {
      "name": "--pf-c-button--m-secondary--hover--BorderColor",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_focus_BorderColor": {
      "name": "--pf-c-button--m-secondary--focus--BorderColor",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_active_BorderColor": {
      "name": "--pf-c-button--m-secondary--active--BorderColor",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    }
  },
  ".pf-c-login": {
    "c_login_PaddingTop": {
      "name": "--pf-c-login--PaddingTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_login_PaddingBottom": {
      "name": "--pf-c-login--PaddingBottom",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_login_xl_BackgroundImage": {
      "name": "--pf-c-login--xl--BackgroundImage",
      "value": "none"
    },
    "c_login__container_xl_GridColumnGap": {
      "name": "--pf-c-login__container--xl--GridColumnGap",
      "value": "4rem",
      "values": [
        "--pf-global--spacer--3xl",
        "$pf-global--spacer--3xl",
        "pf-size-prem(64px)",
        "4rem"
      ]
    },
    "c_login__container_MaxWidth": {
      "name": "--pf-c-login__container--MaxWidth",
      "value": "31.25rem"
    },
    "c_login__container_xl_MaxWidth": {
      "name": "--pf-c-login__container--xl--MaxWidth",
      "value": "none"
    },
    "c_login__container_PaddingLeft": {
      "name": "--pf-c-login__container--PaddingLeft",
      "value": "6.125rem"
    },
    "c_login__container_PaddingRight": {
      "name": "--pf-c-login__container--PaddingRight",
      "value": "6.125rem"
    },
    "c_login__container_xl_GridTemplateColumns": {
      "name": "--pf-c-login__container--xl--GridTemplateColumns",
      "value": "34rem minmax(auto, 34rem)"
    },
    "c_login__header_MarginBottom": {
      "name": "--pf-c-login__header--MarginBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_login__header_PaddingLeft": {
      "name": "--pf-c-login__header--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_login__header_PaddingRight": {
      "name": "--pf-c-login__header--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_login__header_xl_MarginBottom": {
      "name": "--pf-c-login__header--xl--MarginBottom",
      "value": "3rem",
      "values": [
        "--pf-global--spacer--2xl",
        "$pf-global--spacer--2xl",
        "pf-size-prem(48px)",
        "3rem"
      ]
    },
    "c_login__header_xl_MarginTop": {
      "name": "--pf-c-login__header--xl--MarginTop",
      "value": "4rem",
      "values": [
        "--pf-global--spacer--3xl",
        "$pf-global--spacer--3xl",
        "pf-size-prem(64px)",
        "4rem"
      ]
    },
    "c_login__header_c_brand_MarginBottom": {
      "name": "--pf-c-login__header--c-brand--MarginBottom",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_login__header_c_brand_xl_MarginBottom": {
      "name": "--pf-c-login__header--c-brand--xl--MarginBottom",
      "value": "3rem",
      "values": [
        "--pf-global--spacer--2xl",
        "$pf-global--spacer--2xl",
        "pf-size-prem(48px)",
        "3rem"
      ]
    },
    "c_login__main_BackgroundColor": {
      "name": "--pf-c-login__main--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--light-100",
        "$pf-global--BackgroundColor--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_login__main_MarginBottom": {
      "name": "--pf-c-login__main--MarginBottom",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_login__main_header_PaddingTop": {
      "name": "--pf-c-login__main-header--PaddingTop",
      "value": "3rem",
      "values": [
        "--pf-global--spacer--2xl",
        "$pf-global--spacer--2xl",
        "pf-size-prem(48px)",
        "3rem"
      ]
    },
    "c_login__main_header_PaddingRight": {
      "name": "--pf-c-login__main-header--PaddingRight",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_login__main_header_PaddingBottom": {
      "name": "--pf-c-login__main-header--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_login__main_header_PaddingLeft": {
      "name": "--pf-c-login__main-header--PaddingLeft",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_login__main_header_md_PaddingRight": {
      "name": "--pf-c-login__main-header--md--PaddingRight",
      "value": "3rem",
      "values": [
        "--pf-global--spacer--2xl",
        "$pf-global--spacer--2xl",
        "pf-size-prem(48px)",
        "3rem"
      ]
    },
    "c_login__main_header_md_PaddingLeft": {
      "name": "--pf-c-login__main-header--md--PaddingLeft",
      "value": "3rem",
      "values": [
        "--pf-global--spacer--2xl",
        "$pf-global--spacer--2xl",
        "pf-size-prem(48px)",
        "3rem"
      ]
    },
    "c_login__main_header_ColumnGap": {
      "name": "--pf-c-login__main-header--ColumnGap",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_login__main_header_RowGap": {
      "name": "--pf-c-login__main-header--RowGap",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_login__main_header_desc_MarginBottom": {
      "name": "--pf-c-login__main-header-desc--MarginBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_login__main_header_desc_md_MarginBottom": {
      "name": "--pf-c-login__main-header-desc--md--MarginBottom",
      "value": "0"
    },
    "c_login__main_header_desc_FontSize": {
      "name": "--pf-c-login__main-header-desc--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_login__main_body_PaddingRight": {
      "name": "--pf-c-login__main-body--PaddingRight",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_login__main_body_PaddingBottom": {
      "name": "--pf-c-login__main-body--PaddingBottom",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_login__main_body_PaddingLeft": {
      "name": "--pf-c-login__main-body--PaddingLeft",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_login__main_body_md_PaddingRight": {
      "name": "--pf-c-login__main-body--md--PaddingRight",
      "value": "3rem",
      "values": [
        "--pf-global--spacer--2xl",
        "$pf-global--spacer--2xl",
        "pf-size-prem(48px)",
        "3rem"
      ]
    },
    "c_login__main_body_md_PaddingLeft": {
      "name": "--pf-c-login__main-body--md--PaddingLeft",
      "value": "3rem",
      "values": [
        "--pf-global--spacer--2xl",
        "$pf-global--spacer--2xl",
        "pf-size-prem(48px)",
        "3rem"
      ]
    },
    "c_login__main_footer_PaddingBottom": {
      "name": "--pf-c-login__main-footer--PaddingBottom",
      "value": "4rem",
      "values": [
        "--pf-global--spacer--3xl",
        "$pf-global--spacer--3xl",
        "pf-size-prem(64px)",
        "4rem"
      ]
    },
    "c_login__main_footer_c_title_MarginBottom": {
      "name": "--pf-c-login__main-footer--c-title--MarginBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_login__main_footer_links_PaddingTop": {
      "name": "--pf-c-login__main-footer-links--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_login__main_footer_links_PaddingRight": {
      "name": "--pf-c-login__main-footer-links--PaddingRight",
      "value": "4rem",
      "values": [
        "--pf-global--spacer--3xl",
        "$pf-global--spacer--3xl",
        "pf-size-prem(64px)",
        "4rem"
      ]
    },
    "c_login__main_footer_links_PaddingBottom": {
      "name": "--pf-c-login__main-footer-links--PaddingBottom",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_login__main_footer_links_PaddingLeft": {
      "name": "--pf-c-login__main-footer-links--PaddingLeft",
      "value": "4rem",
      "values": [
        "--pf-global--spacer--3xl",
        "$pf-global--spacer--3xl",
        "pf-size-prem(64px)",
        "4rem"
      ]
    },
    "c_login__main_footer_links_item_PaddingRight": {
      "name": "--pf-c-login__main-footer-links-item--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_login__main_footer_links_item_PaddingLeft": {
      "name": "--pf-c-login__main-footer-links-item--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_login__main_footer_links_item_MarginBottom": {
      "name": "--pf-c-login__main-footer-links-item--MarginBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_login__main_footer_links_item_link_svg_Fill": {
      "name": "--pf-c-login__main-footer-links-item-link-svg--Fill",
      "value": "#6a6e73",
      "values": [
        "--pf-global--icon--Color--light",
        "$pf-global--icon--Color--light",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_login__main_footer_links_item_link_svg_Width": {
      "name": "--pf-c-login__main-footer-links-item-link-svg--Width",
      "value": "1.5rem",
      "values": [
        "--pf-global--icon--FontSize--lg",
        "$pf-global--icon--FontSize--lg",
        "pf-font-prem(24px)",
        "1.5rem"
      ]
    },
    "c_login__main_footer_links_item_link_svg_Height": {
      "name": "--pf-c-login__main-footer-links-item-link-svg--Height",
      "value": "1.5rem",
      "values": [
        "--pf-global--icon--FontSize--lg",
        "$pf-global--icon--FontSize--lg",
        "pf-font-prem(24px)",
        "1.5rem"
      ]
    },
    "c_login__main_footer_links_item_link_svg_hover_Fill": {
      "name": "--pf-c-login__main-footer-links-item-link-svg--hover--Fill",
      "value": "#151515",
      "values": [
        "--pf-global--icon--Color--dark",
        "$pf-global--icon--Color--dark",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_login__main_footer_band_PaddingTop": {
      "name": "--pf-c-login__main-footer-band--PaddingTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_login__main_footer_band_PaddingRight": {
      "name": "--pf-c-login__main-footer-band--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_login__main_footer_band_PaddingBottom": {
      "name": "--pf-c-login__main-footer-band--PaddingBottom",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_login__main_footer_band_PaddingLeft": {
      "name": "--pf-c-login__main-footer-band--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_login__main_footer_band_BackgroundColor": {
      "name": "--pf-c-login__main-footer-band--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--200",
        "$pf-global--BackgroundColor--200",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_login__main_footer_band_item_PaddingTop": {
      "name": "--pf-c-login__main-footer-band-item--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_login__footer_PaddingLeft": {
      "name": "--pf-c-login__footer--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_login__footer_PaddingRight": {
      "name": "--pf-c-login__footer--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_login__footer_c_list_PaddingTop": {
      "name": "--pf-c-login__footer--c-list--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_login__footer_c_list_xl_PaddingTop": {
      "name": "--pf-c-login__footer--c-list--xl--PaddingTop",
      "value": "3rem",
      "values": [
        "--pf-global--spacer--2xl",
        "$pf-global--spacer--2xl",
        "pf-size-prem(48px)",
        "3rem"
      ]
    }
  }
};
export default c_login;
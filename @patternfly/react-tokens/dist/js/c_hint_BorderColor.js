"use strict";
exports.__esModule = true;
exports.c_hint_BorderColor = {
  "name": "--pf-c-hint--BorderColor",
  "value": "#bee1f4",
  "var": "var(--pf-c-hint--BorderColor)"
};
exports["default"] = exports.c_hint_BorderColor;
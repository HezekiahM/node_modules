"use strict";
exports.__esModule = true;
exports.global_palette_red_100 = {
  "name": "--pf-global--palette--red-100",
  "value": "#c9190b",
  "var": "var(--pf-global--palette--red-100)"
};
exports["default"] = exports.global_palette_red_100;
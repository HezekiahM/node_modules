"use strict";
exports.__esModule = true;
exports.c_table_m_compact__expandable_row_content_PaddingRight = {
  "name": "--pf-c-table--m-compact__expandable-row-content--PaddingRight",
  "value": "1.5rem",
  "var": "var(--pf-c-table--m-compact__expandable-row-content--PaddingRight)"
};
exports["default"] = exports.c_table_m_compact__expandable_row_content_PaddingRight;
"use strict";
exports.__esModule = true;
exports.c_data_list__expandable_content_MarginLeft = {
  "name": "--pf-c-data-list__expandable-content--MarginLeft",
  "value": "calc(1rem * -1)",
  "var": "var(--pf-c-data-list__expandable-content--MarginLeft)"
};
exports["default"] = exports.c_data_list__expandable_content_MarginLeft;
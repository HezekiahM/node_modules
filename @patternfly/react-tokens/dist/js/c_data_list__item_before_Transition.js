"use strict";
exports.__esModule = true;
exports.c_data_list__item_before_Transition = {
  "name": "--pf-c-data-list__item--before--Transition",
  "value": "all 250ms cubic-bezier(.42, 0, .58, 1)",
  "var": "var(--pf-c-data-list__item--before--Transition)"
};
exports["default"] = exports.c_data_list__item_before_Transition;
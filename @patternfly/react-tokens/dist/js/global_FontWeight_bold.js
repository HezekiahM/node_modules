"use strict";
exports.__esModule = true;
exports.global_FontWeight_bold = {
  "name": "--pf-global--FontWeight--bold",
  "value": "600",
  "var": "var(--pf-global--FontWeight--bold)"
};
exports["default"] = exports.global_FontWeight_bold;
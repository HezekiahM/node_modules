"use strict";
exports.__esModule = true;
exports.c_select__menu_item_icon_FontSize = {
  "name": "--pf-c-select__menu-item-icon--FontSize",
  "value": "0.625rem",
  "var": "var(--pf-c-select__menu-item-icon--FontSize)"
};
exports["default"] = exports.c_select__menu_item_icon_FontSize;
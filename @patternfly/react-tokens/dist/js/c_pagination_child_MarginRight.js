"use strict";
exports.__esModule = true;
exports.c_pagination_child_MarginRight = {
  "name": "--pf-c-pagination--child--MarginRight",
  "value": "0.5rem",
  "var": "var(--pf-c-pagination--child--MarginRight)"
};
exports["default"] = exports.c_pagination_child_MarginRight;
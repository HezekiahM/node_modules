"use strict";
exports.__esModule = true;
exports.c_label_m_red__content_link_hover_before_BorderColor = {
  "name": "--pf-c-label--m-red__content--link--hover--before--BorderColor",
  "value": "#c9190b",
  "var": "var(--pf-c-label--m-red__content--link--hover--before--BorderColor)"
};
exports["default"] = exports.c_label_m_red__content_link_hover_before_BorderColor;
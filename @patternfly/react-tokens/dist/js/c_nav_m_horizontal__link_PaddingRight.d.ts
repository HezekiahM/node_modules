export const c_nav_m_horizontal__link_PaddingRight: {
  "name": "--pf-c-nav--m-horizontal__link--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-nav--m-horizontal__link--PaddingRight)"
};
export default c_nav_m_horizontal__link_PaddingRight;
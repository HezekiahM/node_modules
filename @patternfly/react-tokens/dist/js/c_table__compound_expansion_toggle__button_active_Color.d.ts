export const c_table__compound_expansion_toggle__button_active_Color: {
  "name": "--pf-c-table__compound-expansion-toggle__button--active--Color",
  "value": "#004080",
  "var": "var(--pf-c-table__compound-expansion-toggle__button--active--Color)"
};
export default c_table__compound_expansion_toggle__button_active_Color;
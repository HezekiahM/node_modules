"use strict";
exports.__esModule = true;
exports.c_button_LineHeight = {
  "name": "--pf-c-button--LineHeight",
  "value": "1.5",
  "var": "var(--pf-c-button--LineHeight)"
};
exports["default"] = exports.c_button_LineHeight;
"use strict";
exports.__esModule = true;
exports.c_accordion__toggle_PaddingBottom = {
  "name": "--pf-c-accordion__toggle--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-accordion__toggle--PaddingBottom)"
};
exports["default"] = exports.c_accordion__toggle_PaddingBottom;
"use strict";
exports.__esModule = true;
exports.c_options_menu_c_divider_MarginTop = {
  "name": "--pf-c-options-menu--c-divider--MarginTop",
  "value": "0.5rem",
  "var": "var(--pf-c-options-menu--c-divider--MarginTop)"
};
exports["default"] = exports.c_options_menu_c_divider_MarginTop;
"use strict";
exports.__esModule = true;
exports.c_breadcrumb__item_FontSize = {
  "name": "--pf-c-breadcrumb__item--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-breadcrumb__item--FontSize)"
};
exports["default"] = exports.c_breadcrumb__item_FontSize;
"use strict";
exports.__esModule = true;
exports.c_tile__title_Color = {
  "name": "--pf-c-tile__title--Color",
  "value": "#6a6e73",
  "var": "var(--pf-c-tile__title--Color)"
};
exports["default"] = exports.c_tile__title_Color;
export const c_chip__c_button_MarginBottom: {
  "name": "--pf-c-chip__c-button--MarginBottom",
  "value": "calc(0.25rem * -1)",
  "var": "var(--pf-c-chip__c-button--MarginBottom)"
};
export default c_chip__c_button_MarginBottom;
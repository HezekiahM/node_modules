export const c_toolbar_spacer_base: {
  "name": "--pf-c-toolbar--spacer--base",
  "value": "1rem",
  "var": "var(--pf-c-toolbar--spacer--base)"
};
export default c_toolbar_spacer_base;
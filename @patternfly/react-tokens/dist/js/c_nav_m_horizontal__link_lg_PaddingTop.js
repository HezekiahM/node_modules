"use strict";
exports.__esModule = true;
exports.c_nav_m_horizontal__link_lg_PaddingTop = {
  "name": "--pf-c-nav--m-horizontal__link--lg--PaddingTop",
  "value": "1.5rem",
  "var": "var(--pf-c-nav--m-horizontal__link--lg--PaddingTop)"
};
exports["default"] = exports.c_nav_m_horizontal__link_lg_PaddingTop;
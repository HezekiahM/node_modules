"use strict";
exports.__esModule = true;
exports.c_input_group__text_FontSize = {
  "name": "--pf-c-input-group__text--FontSize",
  "value": "1rem",
  "var": "var(--pf-c-input-group__text--FontSize)"
};
exports["default"] = exports.c_input_group__text_FontSize;
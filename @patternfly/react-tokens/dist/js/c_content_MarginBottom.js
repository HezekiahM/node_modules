"use strict";
exports.__esModule = true;
exports.c_content_MarginBottom = {
  "name": "--pf-c-content--MarginBottom",
  "value": "1rem",
  "var": "var(--pf-c-content--MarginBottom)"
};
exports["default"] = exports.c_content_MarginBottom;
export const c_nav__scroll_button_BackgroundColor: {
  "name": "--pf-c-nav__scroll-button--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-nav__scroll-button--BackgroundColor)"
};
export default c_nav__scroll_button_BackgroundColor;
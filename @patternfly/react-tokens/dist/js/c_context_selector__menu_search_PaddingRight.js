"use strict";
exports.__esModule = true;
exports.c_context_selector__menu_search_PaddingRight = {
  "name": "--pf-c-context-selector__menu-search--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-context-selector__menu-search--PaddingRight)"
};
exports["default"] = exports.c_context_selector__menu_search_PaddingRight;
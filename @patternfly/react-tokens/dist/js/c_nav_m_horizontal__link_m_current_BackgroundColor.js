"use strict";
exports.__esModule = true;
exports.c_nav_m_horizontal__link_m_current_BackgroundColor = {
  "name": "--pf-c-nav--m-horizontal__link--m-current--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-nav--m-horizontal__link--m-current--BackgroundColor)"
};
exports["default"] = exports.c_nav_m_horizontal__link_m_current_BackgroundColor;
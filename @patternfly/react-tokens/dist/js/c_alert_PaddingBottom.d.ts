export const c_alert_PaddingBottom: {
  "name": "--pf-c-alert--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-alert--PaddingBottom)"
};
export default c_alert_PaddingBottom;
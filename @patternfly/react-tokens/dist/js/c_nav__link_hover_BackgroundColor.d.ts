export const c_nav__link_hover_BackgroundColor: {
  "name": "--pf-c-nav__link--hover--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-nav__link--hover--BackgroundColor)"
};
export default c_nav__link_hover_BackgroundColor;
"use strict";
exports.__esModule = true;
exports.c_alert_m_success_BorderTopColor = {
  "name": "--pf-c-alert--m-success--BorderTopColor",
  "value": "#3e8635",
  "var": "var(--pf-c-alert--m-success--BorderTopColor)"
};
exports["default"] = exports.c_alert_m_success_BorderTopColor;
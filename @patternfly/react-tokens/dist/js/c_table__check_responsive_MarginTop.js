"use strict";
exports.__esModule = true;
exports.c_table__check_responsive_MarginTop = {
  "name": "--pf-c-table__check--responsive--MarginTop",
  "value": "0.375rem",
  "var": "var(--pf-c-table__check--responsive--MarginTop)"
};
exports["default"] = exports.c_table__check_responsive_MarginTop;
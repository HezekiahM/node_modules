export const c_accordion__toggle_m_expanded__toggle_text_FontWeight: {
  "name": "--pf-c-accordion__toggle--m-expanded__toggle-text--FontWeight",
  "value": "700",
  "var": "var(--pf-c-accordion__toggle--m-expanded__toggle-text--FontWeight)"
};
export default c_accordion__toggle_m_expanded__toggle_text_FontWeight;
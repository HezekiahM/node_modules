"use strict";
exports.__esModule = true;
exports.global_arrow_width = {
  "name": "--pf-global--arrow--width",
  "value": "0.9375rem",
  "var": "var(--pf-global--arrow--width)"
};
exports["default"] = exports.global_arrow_width;
"use strict";
exports.__esModule = true;
exports.c_notification_drawer__header_BackgroundColor = {
  "name": "--pf-c-notification-drawer__header--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-notification-drawer__header--BackgroundColor)"
};
exports["default"] = exports.c_notification_drawer__header_BackgroundColor;
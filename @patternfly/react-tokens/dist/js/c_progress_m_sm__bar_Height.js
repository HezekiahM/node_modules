"use strict";
exports.__esModule = true;
exports.c_progress_m_sm__bar_Height = {
  "name": "--pf-c-progress--m-sm__bar--Height",
  "value": "0.5rem",
  "var": "var(--pf-c-progress--m-sm__bar--Height)"
};
exports["default"] = exports.c_progress_m_sm__bar_Height;
export const c_wizard__nav_lg_Width: {
  "name": "--pf-c-wizard__nav--lg--Width",
  "value": "15.625rem",
  "var": "var(--pf-c-wizard__nav--lg--Width)"
};
export default c_wizard__nav_lg_Width;
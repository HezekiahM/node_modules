"use strict";
exports.__esModule = true;
exports.c_options_menu__menu_item_icon_PaddingLeft = {
  "name": "--pf-c-options-menu__menu-item-icon--PaddingLeft",
  "value": "1.5rem",
  "var": "var(--pf-c-options-menu__menu-item-icon--PaddingLeft)"
};
exports["default"] = exports.c_options_menu__menu_item_icon_PaddingLeft;
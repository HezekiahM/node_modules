"use strict";
exports.__esModule = true;
exports.chart_area_stroke_Width = {
  "name": "--pf-chart-area--stroke--Width",
  "value": 2,
  "var": "var(--pf-chart-area--stroke--Width)"
};
exports["default"] = exports.chart_area_stroke_Width;
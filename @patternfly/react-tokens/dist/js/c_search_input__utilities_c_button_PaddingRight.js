"use strict";
exports.__esModule = true;
exports.c_search_input__utilities_c_button_PaddingRight = {
  "name": "--pf-c-search-input__utilities--c-button--PaddingRight",
  "value": "0.25rem",
  "var": "var(--pf-c-search-input__utilities--c-button--PaddingRight)"
};
exports["default"] = exports.c_search_input__utilities_c_button_PaddingRight;
export const c_nav_m_horizontal__link_lg_PaddingTop: {
  "name": "--pf-c-nav--m-horizontal__link--lg--PaddingTop",
  "value": "1.5rem",
  "var": "var(--pf-c-nav--m-horizontal__link--lg--PaddingTop)"
};
export default c_nav_m_horizontal__link_lg_PaddingTop;
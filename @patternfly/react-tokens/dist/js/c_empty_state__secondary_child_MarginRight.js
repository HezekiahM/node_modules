"use strict";
exports.__esModule = true;
exports.c_empty_state__secondary_child_MarginRight = {
  "name": "--pf-c-empty-state__secondary--child--MarginRight",
  "value": "calc(0.25rem / 2)",
  "var": "var(--pf-c-empty-state__secondary--child--MarginRight)"
};
exports["default"] = exports.c_empty_state__secondary_child_MarginRight;
"use strict";
exports.__esModule = true;
exports.c_button_m_plain_active_BackgroundColor = {
  "name": "--pf-c-button--m-plain--active--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-button--m-plain--active--BackgroundColor)"
};
exports["default"] = exports.c_button_m_plain_active_BackgroundColor;
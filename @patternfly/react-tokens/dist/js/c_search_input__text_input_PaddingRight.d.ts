export const c_search_input__text_input_PaddingRight: {
  "name": "--pf-c-search-input__text-input--PaddingRight",
  "value": "0.5rem",
  "var": "var(--pf-c-search-input__text-input--PaddingRight)"
};
export default c_search_input__text_input_PaddingRight;
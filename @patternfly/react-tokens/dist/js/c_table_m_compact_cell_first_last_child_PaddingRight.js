"use strict";
exports.__esModule = true;
exports.c_table_m_compact_cell_first_last_child_PaddingRight = {
  "name": "--pf-c-table--m-compact--cell--first-last-child--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-table--m-compact--cell--first-last-child--PaddingRight)"
};
exports["default"] = exports.c_table_m_compact_cell_first_last_child_PaddingRight;
"use strict";
exports.__esModule = true;
exports.c_app_launcher__group_group_PaddingTop = {
  "name": "--pf-c-app-launcher__group--group--PaddingTop",
  "value": "0.5rem",
  "var": "var(--pf-c-app-launcher__group--group--PaddingTop)"
};
exports["default"] = exports.c_app_launcher__group_group_PaddingTop;
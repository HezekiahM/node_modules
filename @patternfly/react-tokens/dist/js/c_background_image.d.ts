export const c_background_image: {
  ".pf-c-background-image": {
    "c_background_image_BackgroundColor": {
      "name": "--pf-c-background-image--BackgroundColor",
      "value": "#151515",
      "values": [
        "--pf-global--BackgroundColor--dark-100",
        "$pf-global--BackgroundColor--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_background_image_BackgroundImage": {
      "name": "--pf-c-background-image--BackgroundImage",
      "value": "url(\"../../assets/images/pfbg_576.jpg\")"
    },
    "c_background_image_BackgroundImage_2x": {
      "name": "--pf-c-background-image--BackgroundImage-2x",
      "value": "url(\"../../assets/images/pfbg_576@2x.jpg\")"
    },
    "c_background_image_BackgroundImage_sm": {
      "name": "--pf-c-background-image--BackgroundImage--sm",
      "value": "url(\"../../assets/images/pfbg_768.jpg\")"
    },
    "c_background_image_BackgroundImage_sm_2x": {
      "name": "--pf-c-background-image--BackgroundImage--sm-2x",
      "value": "url(\"../../assets/images/pfbg_768@2x.jpg\")"
    },
    "c_background_image_BackgroundImage_lg": {
      "name": "--pf-c-background-image--BackgroundImage--lg",
      "value": "url(\"../../assets/images/pfbg_2000.jpg\")"
    },
    "c_background_image_Filter": {
      "name": "--pf-c-background-image--Filter",
      "value": "url(\"#image_overlay\")"
    }
  }
};
export default c_background_image;
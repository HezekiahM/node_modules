"use strict";
exports.__esModule = true;
exports.c_wizard__toggle_PaddingRight = {
  "name": "--pf-c-wizard__toggle--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-wizard__toggle--PaddingRight)"
};
exports["default"] = exports.c_wizard__toggle_PaddingRight;
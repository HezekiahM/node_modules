"use strict";
exports.__esModule = true;
exports.global_palette_green_100 = {
  "name": "--pf-global--palette--green-100",
  "value": "#bde5b8",
  "var": "var(--pf-global--palette--green-100)"
};
exports["default"] = exports.global_palette_green_100;
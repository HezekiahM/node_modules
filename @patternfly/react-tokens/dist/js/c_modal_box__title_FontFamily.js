"use strict";
exports.__esModule = true;
exports.c_modal_box__title_FontFamily = {
  "name": "--pf-c-modal-box__title--FontFamily",
  "value": "\"RedHatDisplay\", \"Overpass\", overpass, helvetica, arial, sans-serif",
  "var": "var(--pf-c-modal-box__title--FontFamily)"
};
exports["default"] = exports.c_modal_box__title_FontFamily;
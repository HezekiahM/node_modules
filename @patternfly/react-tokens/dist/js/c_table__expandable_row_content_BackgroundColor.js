"use strict";
exports.__esModule = true;
exports.c_table__expandable_row_content_BackgroundColor = {
  "name": "--pf-c-table__expandable-row-content--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-table__expandable-row-content--BackgroundColor)"
};
exports["default"] = exports.c_table__expandable_row_content_BackgroundColor;
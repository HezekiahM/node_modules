"use strict";
exports.__esModule = true;
exports.chart_color_black_100 = {
  "name": "--pf-chart-color-black-100",
  "value": "#f0f0f0",
  "var": "var(--pf-chart-color-black-100)"
};
exports["default"] = exports.chart_color_black_100;
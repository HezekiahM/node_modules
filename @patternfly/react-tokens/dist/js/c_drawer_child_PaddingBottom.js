"use strict";
exports.__esModule = true;
exports.c_drawer_child_PaddingBottom = {
  "name": "--pf-c-drawer--child--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-drawer--child--PaddingBottom)"
};
exports["default"] = exports.c_drawer_child_PaddingBottom;
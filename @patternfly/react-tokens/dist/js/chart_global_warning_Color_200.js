"use strict";
exports.__esModule = true;
exports.chart_global_warning_Color_200 = {
  "name": "--pf-chart-global--warning--Color--200",
  "value": "#f0ab00",
  "var": "var(--pf-chart-global--warning--Color--200)"
};
exports["default"] = exports.chart_global_warning_Color_200;
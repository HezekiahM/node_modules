"use strict";
exports.__esModule = true;
exports.c_wizard__nav_list_nested_MarginLeft = {
  "name": "--pf-c-wizard__nav-list--nested--MarginLeft",
  "value": "1rem",
  "var": "var(--pf-c-wizard__nav-list--nested--MarginLeft)"
};
exports["default"] = exports.c_wizard__nav_list_nested_MarginLeft;
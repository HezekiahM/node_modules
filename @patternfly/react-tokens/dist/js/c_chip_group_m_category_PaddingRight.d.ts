export const c_chip_group_m_category_PaddingRight: {
  "name": "--pf-c-chip-group--m-category--PaddingRight",
  "value": "0.25rem",
  "var": "var(--pf-c-chip-group--m-category--PaddingRight)"
};
export default c_chip_group_m_category_PaddingRight;
"use strict";
exports.__esModule = true;
exports.c_content_a_hover_Color = {
  "name": "--pf-c-content--a--hover--Color",
  "value": "#004080",
  "var": "var(--pf-c-content--a--hover--Color)"
};
exports["default"] = exports.c_content_a_hover_Color;
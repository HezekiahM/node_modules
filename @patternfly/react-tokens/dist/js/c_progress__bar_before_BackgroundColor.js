"use strict";
exports.__esModule = true;
exports.c_progress__bar_before_BackgroundColor = {
  "name": "--pf-c-progress__bar--before--BackgroundColor",
  "value": "#c9190b",
  "var": "var(--pf-c-progress__bar--before--BackgroundColor)"
};
exports["default"] = exports.c_progress__bar_before_BackgroundColor;
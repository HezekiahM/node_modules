"use strict";
exports.__esModule = true;
exports.c_content_h1_FontSize = {
  "name": "--pf-c-content--h1--FontSize",
  "value": "1.5rem",
  "var": "var(--pf-c-content--h1--FontSize)"
};
exports["default"] = exports.c_content_h1_FontSize;
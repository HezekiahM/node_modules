"use strict";
exports.__esModule = true;
exports.c_form_control_LineHeight = {
  "name": "--pf-c-form-control--LineHeight",
  "value": "1.5",
  "var": "var(--pf-c-form-control--LineHeight)"
};
exports["default"] = exports.c_form_control_LineHeight;
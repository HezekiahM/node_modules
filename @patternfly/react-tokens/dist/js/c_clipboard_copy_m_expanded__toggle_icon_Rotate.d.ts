export const c_clipboard_copy_m_expanded__toggle_icon_Rotate: {
  "name": "--pf-c-clipboard-copy--m-expanded__toggle-icon--Rotate",
  "value": "90deg",
  "var": "var(--pf-c-clipboard-copy--m-expanded__toggle-icon--Rotate)"
};
export default c_clipboard_copy_m_expanded__toggle_icon_Rotate;
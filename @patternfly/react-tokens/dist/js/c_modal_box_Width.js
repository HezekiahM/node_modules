"use strict";
exports.__esModule = true;
exports.c_modal_box_Width = {
  "name": "--pf-c-modal-box--Width",
  "value": "70rem",
  "var": "var(--pf-c-modal-box--Width)"
};
exports["default"] = exports.c_modal_box_Width;
"use strict";
exports.__esModule = true;
exports.c_button_m_danger_focus_BackgroundColor = {
  "name": "--pf-c-button--m-danger--focus--BackgroundColor",
  "value": "#a30000",
  "var": "var(--pf-c-button--m-danger--focus--BackgroundColor)"
};
exports["default"] = exports.c_button_m_danger_focus_BackgroundColor;
export const chart_donut_threshold_first_Color: {
  "name": "--pf-chart-donut--threshold--first--Color",
  "value": "#f0f0f0",
  "var": "var(--pf-chart-donut--threshold--first--Color)"
};
export default chart_donut_threshold_first_Color;
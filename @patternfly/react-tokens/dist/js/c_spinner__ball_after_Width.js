"use strict";
exports.__esModule = true;
exports.c_spinner__ball_after_Width = {
  "name": "--pf-c-spinner__ball--after--Width",
  "value": "calc(3.375rem * .1)",
  "var": "var(--pf-c-spinner__ball--after--Width)"
};
exports["default"] = exports.c_spinner__ball_after_Width;
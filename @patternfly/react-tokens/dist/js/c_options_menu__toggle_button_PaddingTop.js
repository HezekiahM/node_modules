"use strict";
exports.__esModule = true;
exports.c_options_menu__toggle_button_PaddingTop = {
  "name": "--pf-c-options-menu__toggle-button--PaddingTop",
  "value": "0.375rem",
  "var": "var(--pf-c-options-menu__toggle-button--PaddingTop)"
};
exports["default"] = exports.c_options_menu__toggle_button_PaddingTop;
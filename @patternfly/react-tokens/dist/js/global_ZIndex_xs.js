"use strict";
exports.__esModule = true;
exports.global_ZIndex_xs = {
  "name": "--pf-global--ZIndex--xs",
  "value": "100",
  "var": "var(--pf-global--ZIndex--xs)"
};
exports["default"] = exports.global_ZIndex_xs;
export const c_label_m_outline_m_orange__content_link_hover_before_BorderColor: {
  "name": "--pf-c-label--m-outline--m-orange__content--link--hover--before--BorderColor",
  "value": "#f9e0a2",
  "var": "var(--pf-c-label--m-outline--m-orange__content--link--hover--before--BorderColor)"
};
export default c_label_m_outline_m_orange__content_link_hover_before_BorderColor;
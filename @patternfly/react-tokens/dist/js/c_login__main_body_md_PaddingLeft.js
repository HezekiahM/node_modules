"use strict";
exports.__esModule = true;
exports.c_login__main_body_md_PaddingLeft = {
  "name": "--pf-c-login__main-body--md--PaddingLeft",
  "value": "3rem",
  "var": "var(--pf-c-login__main-body--md--PaddingLeft)"
};
exports["default"] = exports.c_login__main_body_md_PaddingLeft;
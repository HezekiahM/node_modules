"use strict";
exports.__esModule = true;
exports.c_form__actions_MarginLeft = {
  "name": "--pf-c-form__actions--MarginLeft",
  "value": "calc(0.5rem * -1)",
  "var": "var(--pf-c-form__actions--MarginLeft)"
};
exports["default"] = exports.c_form__actions_MarginLeft;
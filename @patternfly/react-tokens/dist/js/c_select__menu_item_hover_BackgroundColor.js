"use strict";
exports.__esModule = true;
exports.c_select__menu_item_hover_BackgroundColor = {
  "name": "--pf-c-select__menu-item--hover--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-select__menu-item--hover--BackgroundColor)"
};
exports["default"] = exports.c_select__menu_item_hover_BackgroundColor;
"use strict";
exports.__esModule = true;
exports.global_target_size_MinWidth = {
  "name": "--pf-global--target-size--MinWidth",
  "value": "44px",
  "var": "var(--pf-global--target-size--MinWidth)"
};
exports["default"] = exports.global_target_size_MinWidth;
export const c_nav_m_light__link_before_BorderColor: {
  "name": "--pf-c-nav--m-light__link--before--BorderColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-nav--m-light__link--before--BorderColor)"
};
export default c_nav_m_light__link_before_BorderColor;
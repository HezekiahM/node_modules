"use strict";
exports.__esModule = true;
exports.c_empty_state__primary_secondary_MarginTop = {
  "name": "--pf-c-empty-state__primary--secondary--MarginTop",
  "value": "0.5rem",
  "var": "var(--pf-c-empty-state__primary--secondary--MarginTop)"
};
exports["default"] = exports.c_empty_state__primary_secondary_MarginTop;
"use strict";
exports.__esModule = true;
exports.c_tooltip__content_PaddingTop = {
  "name": "--pf-c-tooltip__content--PaddingTop",
  "value": "0.5rem",
  "var": "var(--pf-c-tooltip__content--PaddingTop)"
};
exports["default"] = exports.c_tooltip__content_PaddingTop;
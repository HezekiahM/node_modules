"use strict";
exports.__esModule = true;
exports.c_search_input__text_before_BorderWidth = {
  "name": "--pf-c-search-input__text--before--BorderWidth",
  "value": "1px",
  "var": "var(--pf-c-search-input__text--before--BorderWidth)"
};
exports["default"] = exports.c_search_input__text_before_BorderWidth;
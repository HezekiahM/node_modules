"use strict";
exports.__esModule = true;
exports.c_data_list__item_control_not_last_child_MarginRight = {
  "name": "--pf-c-data-list__item-control--not-last-child--MarginRight",
  "value": "1rem",
  "var": "var(--pf-c-data-list__item-control--not-last-child--MarginRight)"
};
exports["default"] = exports.c_data_list__item_control_not_last_child_MarginRight;
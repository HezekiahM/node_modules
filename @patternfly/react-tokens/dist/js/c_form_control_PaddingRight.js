"use strict";
exports.__esModule = true;
exports.c_form_control_PaddingRight = {
  "name": "--pf-c-form-control--PaddingRight",
  "value": "calc(0.5rem + 3rem)",
  "var": "var(--pf-c-form-control--PaddingRight)"
};
exports["default"] = exports.c_form_control_PaddingRight;
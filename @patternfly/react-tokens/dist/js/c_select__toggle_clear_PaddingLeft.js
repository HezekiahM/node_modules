"use strict";
exports.__esModule = true;
exports.c_select__toggle_clear_PaddingLeft = {
  "name": "--pf-c-select__toggle-clear--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-select__toggle-clear--PaddingLeft)"
};
exports["default"] = exports.c_select__toggle_clear_PaddingLeft;
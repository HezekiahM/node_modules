export const c_alert_m_inline_m_warning_BackgroundColor: {
  "name": "--pf-c-alert--m-inline--m-warning--BackgroundColor",
  "value": "#fdf7e7",
  "var": "var(--pf-c-alert--m-inline--m-warning--BackgroundColor)"
};
export default c_alert_m_inline_m_warning_BackgroundColor;
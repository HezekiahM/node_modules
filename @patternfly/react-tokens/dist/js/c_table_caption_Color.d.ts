export const c_table_caption_Color: {
  "name": "--pf-c-table-caption--Color",
  "value": "#6a6e73",
  "var": "var(--pf-c-table-caption--Color)"
};
export default c_table_caption_Color;
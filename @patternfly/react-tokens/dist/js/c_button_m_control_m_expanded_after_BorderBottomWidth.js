"use strict";
exports.__esModule = true;
exports.c_button_m_control_m_expanded_after_BorderBottomWidth = {
  "name": "--pf-c-button--m-control--m-expanded--after--BorderBottomWidth",
  "value": "2px",
  "var": "var(--pf-c-button--m-control--m-expanded--after--BorderBottomWidth)"
};
exports["default"] = exports.c_button_m_control_m_expanded_after_BorderBottomWidth;
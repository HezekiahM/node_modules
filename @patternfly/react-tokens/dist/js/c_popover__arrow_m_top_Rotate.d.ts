export const c_popover__arrow_m_top_Rotate: {
  "name": "--pf-c-popover__arrow--m-top--Rotate",
  "value": "45deg",
  "var": "var(--pf-c-popover__arrow--m-top--Rotate)"
};
export default c_popover__arrow_m_top_Rotate;
"use strict";
exports.__esModule = true;
exports.c_button_m_plain_BackgroundColor = {
  "name": "--pf-c-button--m-plain--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-button--m-plain--BackgroundColor)"
};
exports["default"] = exports.c_button_m_plain_BackgroundColor;
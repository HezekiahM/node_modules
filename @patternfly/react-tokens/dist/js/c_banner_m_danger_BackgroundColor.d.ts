export const c_banner_m_danger_BackgroundColor: {
  "name": "--pf-c-banner--m-danger--BackgroundColor",
  "value": "#c9190b",
  "var": "var(--pf-c-banner--m-danger--BackgroundColor)"
};
export default c_banner_m_danger_BackgroundColor;
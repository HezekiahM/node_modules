"use strict";
exports.__esModule = true;
exports.chart_axis_tick_label_Fill = {
  "name": "--pf-chart-axis--tick-label--Fill",
  "value": "#4f5255",
  "var": "var(--pf-chart-axis--tick-label--Fill)"
};
exports["default"] = exports.chart_axis_tick_label_Fill;
"use strict";
exports.__esModule = true;
exports.c_accordion__toggle_m_expanded__toggle_icon_Rotate = {
  "name": "--pf-c-accordion__toggle--m-expanded__toggle-icon--Rotate",
  "value": "90deg",
  "var": "var(--pf-c-accordion__toggle--m-expanded__toggle-icon--Rotate)"
};
exports["default"] = exports.c_accordion__toggle_m_expanded__toggle_icon_Rotate;
export const c_label__c_button_MarginTop: {
  "name": "--pf-c-label__c-button--MarginTop",
  "value": "calc(0.375rem * -1)",
  "var": "var(--pf-c-label__c-button--MarginTop)"
};
export default c_label__c_button_MarginTop;
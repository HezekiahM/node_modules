"use strict";
exports.__esModule = true;
exports.c_modal_box_BackgroundColor = {
  "name": "--pf-c-modal-box--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-modal-box--BackgroundColor)"
};
exports["default"] = exports.c_modal_box_BackgroundColor;
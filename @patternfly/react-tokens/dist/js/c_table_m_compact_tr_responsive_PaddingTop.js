"use strict";
exports.__esModule = true;
exports.c_table_m_compact_tr_responsive_PaddingTop = {
  "name": "--pf-c-table--m-compact-tr--responsive--PaddingTop",
  "value": "0.5rem",
  "var": "var(--pf-c-table--m-compact-tr--responsive--PaddingTop)"
};
exports["default"] = exports.c_table_m_compact_tr_responsive_PaddingTop;
"use strict";
exports.__esModule = true;
exports.c_drawer__content_FlexBasis = {
  "name": "--pf-c-drawer__content--FlexBasis",
  "value": "100%",
  "var": "var(--pf-c-drawer__content--FlexBasis)"
};
exports["default"] = exports.c_drawer__content_FlexBasis;
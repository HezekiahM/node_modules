"use strict";
exports.__esModule = true;
exports.c_options_menu__menu_PaddingBottom = {
  "name": "--pf-c-options-menu__menu--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-options-menu__menu--PaddingBottom)"
};
exports["default"] = exports.c_options_menu__menu_PaddingBottom;
"use strict";
exports.__esModule = true;
exports.c_label__c_button_PaddingTop = {
  "name": "--pf-c-label__c-button--PaddingTop",
  "value": "0.25rem",
  "var": "var(--pf-c-label__c-button--PaddingTop)"
};
exports["default"] = exports.c_label__c_button_PaddingTop;
export const c_nav__subnav__link_FontSize: {
  "name": "--pf-c-nav__subnav__link--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-nav__subnav__link--FontSize)"
};
export default c_nav__subnav__link_FontSize;
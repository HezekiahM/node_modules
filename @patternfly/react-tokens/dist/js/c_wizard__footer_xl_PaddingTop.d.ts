export const c_wizard__footer_xl_PaddingTop: {
  "name": "--pf-c-wizard__footer--xl--PaddingTop",
  "value": "1.5rem",
  "var": "var(--pf-c-wizard__footer--xl--PaddingTop)"
};
export default c_wizard__footer_xl_PaddingTop;
"use strict";
exports.__esModule = true;
exports.c_skip_to_content_ZIndex = {
  "name": "--pf-c-skip-to-content--ZIndex",
  "value": "600",
  "var": "var(--pf-c-skip-to-content--ZIndex)"
};
exports["default"] = exports.c_skip_to_content_ZIndex;
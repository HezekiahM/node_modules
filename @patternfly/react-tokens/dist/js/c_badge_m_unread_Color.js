"use strict";
exports.__esModule = true;
exports.c_badge_m_unread_Color = {
  "name": "--pf-c-badge--m-unread--Color",
  "value": "#fff",
  "var": "var(--pf-c-badge--m-unread--Color)"
};
exports["default"] = exports.c_badge_m_unread_Color;
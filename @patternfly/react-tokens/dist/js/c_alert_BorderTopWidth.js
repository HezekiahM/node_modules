"use strict";
exports.__esModule = true;
exports.c_alert_BorderTopWidth = {
  "name": "--pf-c-alert--BorderTopWidth",
  "value": "2px",
  "var": "var(--pf-c-alert--BorderTopWidth)"
};
exports["default"] = exports.c_alert_BorderTopWidth;
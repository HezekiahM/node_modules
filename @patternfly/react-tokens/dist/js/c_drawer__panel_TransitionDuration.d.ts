export const c_drawer__panel_TransitionDuration: {
  "name": "--pf-c-drawer__panel--TransitionDuration",
  "value": "250ms",
  "var": "var(--pf-c-drawer__panel--TransitionDuration)"
};
export default c_drawer__panel_TransitionDuration;
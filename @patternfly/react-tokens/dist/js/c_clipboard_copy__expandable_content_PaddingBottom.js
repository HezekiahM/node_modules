"use strict";
exports.__esModule = true;
exports.c_clipboard_copy__expandable_content_PaddingBottom = {
  "name": "--pf-c-clipboard-copy__expandable-content--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-clipboard-copy__expandable-content--PaddingBottom)"
};
exports["default"] = exports.c_clipboard_copy__expandable_content_PaddingBottom;
"use strict";
exports.__esModule = true;
exports.c_select__menu_item_FontSize = {
  "name": "--pf-c-select__menu-item--FontSize",
  "value": "1rem",
  "var": "var(--pf-c-select__menu-item--FontSize)"
};
exports["default"] = exports.c_select__menu_item_FontSize;
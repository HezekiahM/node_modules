export const c_pagination_m_bottom_xl_PaddingRight: {
  "name": "--pf-c-pagination--m-bottom--xl--PaddingRight",
  "value": "1.5rem",
  "var": "var(--pf-c-pagination--m-bottom--xl--PaddingRight)"
};
export default c_pagination_m_bottom_xl_PaddingRight;
"use strict";
exports.__esModule = true;
exports.global_FontSize_xl = {
  "name": "--pf-global--FontSize--xl",
  "value": "1.25rem",
  "var": "var(--pf-global--FontSize--xl)"
};
exports["default"] = exports.global_FontSize_xl;
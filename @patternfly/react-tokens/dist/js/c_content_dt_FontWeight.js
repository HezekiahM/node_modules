"use strict";
exports.__esModule = true;
exports.c_content_dt_FontWeight = {
  "name": "--pf-c-content--dt--FontWeight",
  "value": "700",
  "var": "var(--pf-c-content--dt--FontWeight)"
};
exports["default"] = exports.c_content_dt_FontWeight;
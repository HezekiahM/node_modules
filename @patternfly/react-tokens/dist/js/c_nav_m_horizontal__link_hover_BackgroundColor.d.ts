export const c_nav_m_horizontal__link_hover_BackgroundColor: {
  "name": "--pf-c-nav--m-horizontal__link--hover--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-nav--m-horizontal__link--hover--BackgroundColor)"
};
export default c_nav_m_horizontal__link_hover_BackgroundColor;
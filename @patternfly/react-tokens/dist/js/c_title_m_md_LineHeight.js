"use strict";
exports.__esModule = true;
exports.c_title_m_md_LineHeight = {
  "name": "--pf-c-title--m-md--LineHeight",
  "value": "1.5",
  "var": "var(--pf-c-title--m-md--LineHeight)"
};
exports["default"] = exports.c_title_m_md_LineHeight;
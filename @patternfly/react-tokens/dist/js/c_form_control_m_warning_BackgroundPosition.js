"use strict";
exports.__esModule = true;
exports.c_form_control_m_warning_BackgroundPosition = {
  "name": "--pf-c-form-control--m-warning--BackgroundPosition",
  "value": "calc(100% - calc(0.5rem - 0.0625rem)) 0.5rem",
  "var": "var(--pf-c-form-control--m-warning--BackgroundPosition)"
};
exports["default"] = exports.c_form_control_m_warning_BackgroundPosition;
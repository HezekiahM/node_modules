"use strict";
exports.__esModule = true;
exports.global_BackgroundColor_100 = {
  "name": "--pf-global--BackgroundColor--100",
  "value": "#151515",
  "var": "var(--pf-global--BackgroundColor--100)"
};
exports["default"] = exports.global_BackgroundColor_100;
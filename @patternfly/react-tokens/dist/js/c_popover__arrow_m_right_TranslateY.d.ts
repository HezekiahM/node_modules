export const c_popover__arrow_m_right_TranslateY: {
  "name": "--pf-c-popover__arrow--m-right--TranslateY",
  "value": "-50%",
  "var": "var(--pf-c-popover__arrow--m-right--TranslateY)"
};
export default c_popover__arrow_m_right_TranslateY;
export const c_clipboard_copy__expandable_content_BorderLeftWidth: {
  "name": "--pf-c-clipboard-copy__expandable-content--BorderLeftWidth",
  "value": "1px",
  "var": "var(--pf-c-clipboard-copy__expandable-content--BorderLeftWidth)"
};
export default c_clipboard_copy__expandable_content_BorderLeftWidth;
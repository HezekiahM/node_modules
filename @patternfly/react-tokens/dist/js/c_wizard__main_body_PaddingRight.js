"use strict";
exports.__esModule = true;
exports.c_wizard__main_body_PaddingRight = {
  "name": "--pf-c-wizard__main-body--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-wizard__main-body--PaddingRight)"
};
exports["default"] = exports.c_wizard__main_body_PaddingRight;
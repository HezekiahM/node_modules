"use strict";
exports.__esModule = true;
exports.c_alert__action_MarginBottom = {
  "name": "--pf-c-alert__action--MarginBottom",
  "value": "calc(0.375rem * -1)",
  "var": "var(--pf-c-alert__action--MarginBottom)"
};
exports["default"] = exports.c_alert__action_MarginBottom;
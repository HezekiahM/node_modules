"use strict";
exports.__esModule = true;
exports.c_switch__input_disabled__toggle_before_BackgroundColor = {
  "name": "--pf-c-switch__input--disabled__toggle--before--BackgroundColor",
  "value": "#d2d2d2",
  "var": "var(--pf-c-switch__input--disabled__toggle--before--BackgroundColor)"
};
exports["default"] = exports.c_switch__input_disabled__toggle_before_BackgroundColor;
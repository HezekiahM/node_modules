"use strict";
exports.__esModule = true;
exports.c_switch__toggle_BackgroundColor = {
  "name": "--pf-c-switch__toggle--BackgroundColor",
  "value": "#d2d2d2",
  "var": "var(--pf-c-switch__toggle--BackgroundColor)"
};
exports["default"] = exports.c_switch__toggle_BackgroundColor;
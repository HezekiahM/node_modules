"use strict";
exports.__esModule = true;
exports.c_context_selector__toggle_active_BorderBottomWidth = {
  "name": "--pf-c-context-selector__toggle--active--BorderBottomWidth",
  "value": "2px",
  "var": "var(--pf-c-context-selector__toggle--active--BorderBottomWidth)"
};
exports["default"] = exports.c_context_selector__toggle_active_BorderBottomWidth;
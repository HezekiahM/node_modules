"use strict";
exports.__esModule = true;
exports.c_data_list__expandable_content_BorderTopColor = {
  "name": "--pf-c-data-list__expandable-content--BorderTopColor",
  "value": "#d2d2d2",
  "var": "var(--pf-c-data-list__expandable-content--BorderTopColor)"
};
exports["default"] = exports.c_data_list__expandable_content_BorderTopColor;
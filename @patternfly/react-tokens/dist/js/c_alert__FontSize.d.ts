export const c_alert__FontSize: {
  "name": "--pf-c-alert__FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-alert__FontSize)"
};
export default c_alert__FontSize;
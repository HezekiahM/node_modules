"use strict";
exports.__esModule = true;
exports.c_about_modal_box__brand_PaddingBottom = {
  "name": "--pf-c-about-modal-box__brand--PaddingBottom",
  "value": "2rem",
  "var": "var(--pf-c-about-modal-box__brand--PaddingBottom)"
};
exports["default"] = exports.c_about_modal_box__brand_PaddingBottom;
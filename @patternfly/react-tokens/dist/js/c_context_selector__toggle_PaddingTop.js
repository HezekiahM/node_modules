"use strict";
exports.__esModule = true;
exports.c_context_selector__toggle_PaddingTop = {
  "name": "--pf-c-context-selector__toggle--PaddingTop",
  "value": "0.375rem",
  "var": "var(--pf-c-context-selector__toggle--PaddingTop)"
};
exports["default"] = exports.c_context_selector__toggle_PaddingTop;
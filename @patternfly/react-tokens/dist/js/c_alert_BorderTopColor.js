"use strict";
exports.__esModule = true;
exports.c_alert_BorderTopColor = {
  "name": "--pf-c-alert--BorderTopColor",
  "value": "#2b9af3",
  "var": "var(--pf-c-alert--BorderTopColor)"
};
exports["default"] = exports.c_alert_BorderTopColor;
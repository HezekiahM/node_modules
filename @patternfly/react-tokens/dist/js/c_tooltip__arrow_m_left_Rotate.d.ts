export const c_tooltip__arrow_m_left_Rotate: {
  "name": "--pf-c-tooltip__arrow--m-left--Rotate",
  "value": "45deg",
  "var": "var(--pf-c-tooltip__arrow--m-left--Rotate)"
};
export default c_tooltip__arrow_m_left_Rotate;
"use strict";
exports.__esModule = true;
exports.c_tile_BackgroundColor = {
  "name": "--pf-c-tile--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-tile--BackgroundColor)"
};
exports["default"] = exports.c_tile_BackgroundColor;
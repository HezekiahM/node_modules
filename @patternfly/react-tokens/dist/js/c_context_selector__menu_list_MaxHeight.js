"use strict";
exports.__esModule = true;
exports.c_context_selector__menu_list_MaxHeight = {
  "name": "--pf-c-context-selector__menu-list--MaxHeight",
  "value": "12.5rem",
  "var": "var(--pf-c-context-selector__menu-list--MaxHeight)"
};
exports["default"] = exports.c_context_selector__menu_list_MaxHeight;
"use strict";
exports.__esModule = true;
exports.c_input_group__text_BorderRightColor = {
  "name": "--pf-c-input-group__text--BorderRightColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-input-group__text--BorderRightColor)"
};
exports["default"] = exports.c_input_group__text_BorderRightColor;
"use strict";
exports.__esModule = true;
exports.c_wizard__nav_lg_BorderRightWidth = {
  "name": "--pf-c-wizard__nav--lg--BorderRightWidth",
  "value": "1px",
  "var": "var(--pf-c-wizard__nav--lg--BorderRightWidth)"
};
exports["default"] = exports.c_wizard__nav_lg_BorderRightWidth;
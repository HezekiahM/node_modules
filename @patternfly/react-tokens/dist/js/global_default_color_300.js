"use strict";
exports.__esModule = true;
exports.global_default_color_300 = {
  "name": "--pf-global--default-color--300",
  "value": "#003737",
  "var": "var(--pf-global--default-color--300)"
};
exports["default"] = exports.global_default_color_300;
"use strict";
exports.__esModule = true;
exports.c_button_m_link_disabled_BackgroundColor = {
  "name": "--pf-c-button--m-link--disabled--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-button--m-link--disabled--BackgroundColor)"
};
exports["default"] = exports.c_button_m_link_disabled_BackgroundColor;
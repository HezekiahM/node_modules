export const c_chip_group_m_category_BorderRadius: {
  "name": "--pf-c-chip-group--m-category--BorderRadius",
  "value": "3px",
  "var": "var(--pf-c-chip-group--m-category--BorderRadius)"
};
export default c_chip_group_m_category_BorderRadius;
"use strict";
exports.__esModule = true;
exports.c_login__main_body_PaddingRight = {
  "name": "--pf-c-login__main-body--PaddingRight",
  "value": "2rem",
  "var": "var(--pf-c-login__main-body--PaddingRight)"
};
exports["default"] = exports.c_login__main_body_PaddingRight;
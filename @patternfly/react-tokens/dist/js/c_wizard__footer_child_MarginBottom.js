"use strict";
exports.__esModule = true;
exports.c_wizard__footer_child_MarginBottom = {
  "name": "--pf-c-wizard__footer--child--MarginBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-wizard__footer--child--MarginBottom)"
};
exports["default"] = exports.c_wizard__footer_child_MarginBottom;
"use strict";
exports.__esModule = true;
exports.c_button_m_danger_focus_Color = {
  "name": "--pf-c-button--m-danger--focus--Color",
  "value": "#fff",
  "var": "var(--pf-c-button--m-danger--focus--Color)"
};
exports["default"] = exports.c_button_m_danger_focus_Color;
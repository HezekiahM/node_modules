"use strict";
exports.__esModule = true;
exports.c_options_menu__menu_item_PaddingBottom = {
  "name": "--pf-c-options-menu__menu-item--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-options-menu__menu-item--PaddingBottom)"
};
exports["default"] = exports.c_options_menu__menu_item_PaddingBottom;
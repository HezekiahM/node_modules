"use strict";
exports.__esModule = true;
exports.chart_color_gold_500 = {
  "name": "--pf-chart-color-gold-500",
  "value": "#c58c00",
  "var": "var(--pf-chart-color-gold-500)"
};
exports["default"] = exports.chart_color_gold_500;
"use strict";
exports.__esModule = true;
exports.c_select__menu_item_m_link_focus_BackgroundColor = {
  "name": "--pf-c-select__menu-item--m-link--focus--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-select__menu-item--m-link--focus--BackgroundColor)"
};
exports["default"] = exports.c_select__menu_item_m_link_focus_BackgroundColor;
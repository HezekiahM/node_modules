"use strict";
exports.__esModule = true;
exports.global_palette_light_blue_100 = {
  "name": "--pf-global--palette--light-blue-100",
  "value": "#beedf9",
  "var": "var(--pf-global--palette--light-blue-100)"
};
exports["default"] = exports.global_palette_light_blue_100;
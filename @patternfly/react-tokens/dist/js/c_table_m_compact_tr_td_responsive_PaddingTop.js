"use strict";
exports.__esModule = true;
exports.c_table_m_compact_tr_td_responsive_PaddingTop = {
  "name": "--pf-c-table--m-compact-tr-td--responsive--PaddingTop",
  "value": "0.25rem",
  "var": "var(--pf-c-table--m-compact-tr-td--responsive--PaddingTop)"
};
exports["default"] = exports.c_table_m_compact_tr_td_responsive_PaddingTop;
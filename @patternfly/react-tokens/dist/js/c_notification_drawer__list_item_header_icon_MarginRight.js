"use strict";
exports.__esModule = true;
exports.c_notification_drawer__list_item_header_icon_MarginRight = {
  "name": "--pf-c-notification-drawer__list-item-header-icon--MarginRight",
  "value": "0.5rem",
  "var": "var(--pf-c-notification-drawer__list-item-header-icon--MarginRight)"
};
exports["default"] = exports.c_notification_drawer__list_item_header_icon_MarginRight;
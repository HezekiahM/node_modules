"use strict";
exports.__esModule = true;
exports.chart_global_label_stroke = {
  "name": "--pf-chart-global--label--stroke",
  "value": "transparent",
  "var": "var(--pf-chart-global--label--stroke)"
};
exports["default"] = exports.chart_global_label_stroke;
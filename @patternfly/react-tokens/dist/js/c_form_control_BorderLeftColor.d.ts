export const c_form_control_BorderLeftColor: {
  "name": "--pf-c-form-control--BorderLeftColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-form-control--BorderLeftColor)"
};
export default c_form_control_BorderLeftColor;
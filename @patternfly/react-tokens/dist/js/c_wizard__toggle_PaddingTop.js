"use strict";
exports.__esModule = true;
exports.c_wizard__toggle_PaddingTop = {
  "name": "--pf-c-wizard__toggle--PaddingTop",
  "value": "1.5rem",
  "var": "var(--pf-c-wizard__toggle--PaddingTop)"
};
exports["default"] = exports.c_wizard__toggle_PaddingTop;
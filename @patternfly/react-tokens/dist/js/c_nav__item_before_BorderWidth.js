"use strict";
exports.__esModule = true;
exports.c_nav__item_before_BorderWidth = {
  "name": "--pf-c-nav__item--before--BorderWidth",
  "value": "1px",
  "var": "var(--pf-c-nav__item--before--BorderWidth)"
};
exports["default"] = exports.c_nav__item_before_BorderWidth;
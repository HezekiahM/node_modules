"use strict";
exports.__esModule = true;
exports.c_title_m_lg_FontWeight = {
  "name": "--pf-c-title--m-lg--FontWeight",
  "value": "700",
  "var": "var(--pf-c-title--m-lg--FontWeight)"
};
exports["default"] = exports.c_title_m_lg_FontWeight;
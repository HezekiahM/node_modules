"use strict";
exports.__esModule = true;
exports.c_pagination_m_bottom_md_PaddingTop = {
  "name": "--pf-c-pagination--m-bottom--md--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-pagination--m-bottom--md--PaddingTop)"
};
exports["default"] = exports.c_pagination_m_bottom_md_PaddingTop;
"use strict";
exports.__esModule = true;
exports.c_options_menu__toggle_Color = {
  "name": "--pf-c-options-menu__toggle--Color",
  "value": "#151515",
  "var": "var(--pf-c-options-menu__toggle--Color)"
};
exports["default"] = exports.c_options_menu__toggle_Color;
"use strict";
exports.__esModule = true;
exports.c_table__button_focus_Color = {
  "name": "--pf-c-table__button--focus--Color",
  "value": "#004080",
  "var": "var(--pf-c-table__button--focus--Color)"
};
exports["default"] = exports.c_table__button_focus_Color;
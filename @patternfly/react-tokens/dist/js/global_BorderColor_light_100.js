"use strict";
exports.__esModule = true;
exports.global_BorderColor_light_100 = {
  "name": "--pf-global--BorderColor--light-100",
  "value": "#b8bbbe",
  "var": "var(--pf-global--BorderColor--light-100)"
};
exports["default"] = exports.global_BorderColor_light_100;
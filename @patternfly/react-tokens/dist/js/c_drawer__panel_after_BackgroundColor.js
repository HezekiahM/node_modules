"use strict";
exports.__esModule = true;
exports.c_drawer__panel_after_BackgroundColor = {
  "name": "--pf-c-drawer__panel--after--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-drawer__panel--after--BackgroundColor)"
};
exports["default"] = exports.c_drawer__panel_after_BackgroundColor;
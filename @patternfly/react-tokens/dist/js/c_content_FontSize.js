"use strict";
exports.__esModule = true;
exports.c_content_FontSize = {
  "name": "--pf-c-content--FontSize",
  "value": "1rem",
  "var": "var(--pf-c-content--FontSize)"
};
exports["default"] = exports.c_content_FontSize;
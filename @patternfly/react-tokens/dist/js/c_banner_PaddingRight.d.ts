export const c_banner_PaddingRight: {
  "name": "--pf-c-banner--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-banner--PaddingRight)"
};
export default c_banner_PaddingRight;
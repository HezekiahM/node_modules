"use strict";
exports.__esModule = true;
exports.c_form_control_m_warning_BorderBottomWidth = {
  "name": "--pf-c-form-control--m-warning--BorderBottomWidth",
  "value": "2px",
  "var": "var(--pf-c-form-control--m-warning--BorderBottomWidth)"
};
exports["default"] = exports.c_form_control_m_warning_BorderBottomWidth;
"use strict";
exports.__esModule = true;
exports.c_overflow_menu__group_spacer = {
  "name": "--pf-c-overflow-menu__group--spacer",
  "value": "1rem",
  "var": "var(--pf-c-overflow-menu__group--spacer)"
};
exports["default"] = exports.c_overflow_menu__group_spacer;
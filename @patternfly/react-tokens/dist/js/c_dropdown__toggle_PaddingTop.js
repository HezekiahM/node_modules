"use strict";
exports.__esModule = true;
exports.c_dropdown__toggle_PaddingTop = {
  "name": "--pf-c-dropdown__toggle--PaddingTop",
  "value": "0.375rem",
  "var": "var(--pf-c-dropdown__toggle--PaddingTop)"
};
exports["default"] = exports.c_dropdown__toggle_PaddingTop;
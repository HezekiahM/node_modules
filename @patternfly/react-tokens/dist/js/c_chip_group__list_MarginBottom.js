"use strict";
exports.__esModule = true;
exports.c_chip_group__list_MarginBottom = {
  "name": "--pf-c-chip-group__list--MarginBottom",
  "value": "calc(0.25rem * -1)",
  "var": "var(--pf-c-chip-group__list--MarginBottom)"
};
exports["default"] = exports.c_chip_group__list_MarginBottom;
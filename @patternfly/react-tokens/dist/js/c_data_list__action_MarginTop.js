"use strict";
exports.__esModule = true;
exports.c_data_list__action_MarginTop = {
  "name": "--pf-c-data-list__action--MarginTop",
  "value": "calc(0.375rem * -1)",
  "var": "var(--pf-c-data-list__action--MarginTop)"
};
exports["default"] = exports.c_data_list__action_MarginTop;
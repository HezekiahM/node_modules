"use strict";
exports.__esModule = true;
exports.c_tabs__link_before_border_width_base = {
  "name": "--pf-c-tabs__link--before--border-width--base",
  "value": "1px",
  "var": "var(--pf-c-tabs__link--before--border-width--base)"
};
exports["default"] = exports.c_tabs__link_before_border_width_base;
"use strict";
exports.__esModule = true;
exports.c_table__expandable_row_content_PaddingBottom = {
  "name": "--pf-c-table__expandable-row-content--PaddingBottom",
  "value": "1.5rem",
  "var": "var(--pf-c-table__expandable-row-content--PaddingBottom)"
};
exports["default"] = exports.c_table__expandable_row_content_PaddingBottom;
export const c_options_menu_c_divider_MarginTop: {
  "name": "--pf-c-options-menu--c-divider--MarginTop",
  "value": "0.5rem",
  "var": "var(--pf-c-options-menu--c-divider--MarginTop)"
};
export default c_options_menu_c_divider_MarginTop;
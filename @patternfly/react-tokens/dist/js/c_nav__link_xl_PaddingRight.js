"use strict";
exports.__esModule = true;
exports.c_nav__link_xl_PaddingRight = {
  "name": "--pf-c-nav__link--xl--PaddingRight",
  "value": "1.5rem",
  "var": "var(--pf-c-nav__link--xl--PaddingRight)"
};
exports["default"] = exports.c_nav__link_xl_PaddingRight;
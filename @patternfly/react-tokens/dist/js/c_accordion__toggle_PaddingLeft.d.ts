export const c_accordion__toggle_PaddingLeft: {
  "name": "--pf-c-accordion__toggle--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-accordion__toggle--PaddingLeft)"
};
export default c_accordion__toggle_PaddingLeft;
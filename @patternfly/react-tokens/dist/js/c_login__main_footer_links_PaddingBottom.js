"use strict";
exports.__esModule = true;
exports.c_login__main_footer_links_PaddingBottom = {
  "name": "--pf-c-login__main-footer-links--PaddingBottom",
  "value": "2rem",
  "var": "var(--pf-c-login__main-footer-links--PaddingBottom)"
};
exports["default"] = exports.c_login__main_footer_links_PaddingBottom;
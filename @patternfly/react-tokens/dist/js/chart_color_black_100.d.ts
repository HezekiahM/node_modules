export const chart_color_black_100: {
  "name": "--pf-chart-color-black-100",
  "value": "#f0f0f0",
  "var": "var(--pf-chart-color-black-100)"
};
export default chart_color_black_100;
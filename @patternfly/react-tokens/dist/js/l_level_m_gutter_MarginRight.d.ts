export const l_level_m_gutter_MarginRight: {
  "name": "--pf-l-level--m-gutter--MarginRight",
  "value": "1rem",
  "var": "var(--pf-l-level--m-gutter--MarginRight)"
};
export default l_level_m_gutter_MarginRight;
"use strict";
exports.__esModule = true;
exports.c_alert__icon_MarginRight = {
  "name": "--pf-c-alert__icon--MarginRight",
  "value": "0.5rem",
  "var": "var(--pf-c-alert__icon--MarginRight)"
};
exports["default"] = exports.c_alert__icon_MarginRight;
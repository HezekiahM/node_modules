export const c_wizard__toggle_m_expanded_BorderBottomWidth: {
  "name": "--pf-c-wizard__toggle--m-expanded--BorderBottomWidth",
  "value": "1px",
  "var": "var(--pf-c-wizard__toggle--m-expanded--BorderBottomWidth)"
};
export default c_wizard__toggle_m_expanded_BorderBottomWidth;
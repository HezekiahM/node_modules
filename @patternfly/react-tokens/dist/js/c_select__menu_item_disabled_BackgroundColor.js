"use strict";
exports.__esModule = true;
exports.c_select__menu_item_disabled_BackgroundColor = {
  "name": "--pf-c-select__menu-item--disabled--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-select__menu-item--disabled--BackgroundColor)"
};
exports["default"] = exports.c_select__menu_item_disabled_BackgroundColor;
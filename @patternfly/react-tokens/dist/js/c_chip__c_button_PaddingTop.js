"use strict";
exports.__esModule = true;
exports.c_chip__c_button_PaddingTop = {
  "name": "--pf-c-chip__c-button--PaddingTop",
  "value": "0.25rem",
  "var": "var(--pf-c-chip__c-button--PaddingTop)"
};
exports["default"] = exports.c_chip__c_button_PaddingTop;
export const global_success_color_100: {
  "name": "--pf-global--success-color--100",
  "value": "#3e8635",
  "var": "var(--pf-global--success-color--100)"
};
export default global_success_color_100;
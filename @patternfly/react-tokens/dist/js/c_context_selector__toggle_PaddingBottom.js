"use strict";
exports.__esModule = true;
exports.c_context_selector__toggle_PaddingBottom = {
  "name": "--pf-c-context-selector__toggle--PaddingBottom",
  "value": "0.375rem",
  "var": "var(--pf-c-context-selector__toggle--PaddingBottom)"
};
exports["default"] = exports.c_context_selector__toggle_PaddingBottom;
"use strict";
exports.__esModule = true;
exports.c_input_group__text_PaddingLeft = {
  "name": "--pf-c-input-group__text--PaddingLeft",
  "value": "0.5rem",
  "var": "var(--pf-c-input-group__text--PaddingLeft)"
};
exports["default"] = exports.c_input_group__text_PaddingLeft;
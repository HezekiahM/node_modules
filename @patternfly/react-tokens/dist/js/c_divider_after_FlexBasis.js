"use strict";
exports.__esModule = true;
exports.c_divider_after_FlexBasis = {
  "name": "--pf-c-divider--after--FlexBasis",
  "value": "100%",
  "var": "var(--pf-c-divider--after--FlexBasis)"
};
exports["default"] = exports.c_divider_after_FlexBasis;
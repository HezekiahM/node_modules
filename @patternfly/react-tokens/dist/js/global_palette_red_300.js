"use strict";
exports.__esModule = true;
exports.global_palette_red_300 = {
  "name": "--pf-global--palette--red-300",
  "value": "#7d1007",
  "var": "var(--pf-global--palette--red-300)"
};
exports["default"] = exports.global_palette_red_300;
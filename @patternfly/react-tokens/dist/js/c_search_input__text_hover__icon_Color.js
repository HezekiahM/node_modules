"use strict";
exports.__esModule = true;
exports.c_search_input__text_hover__icon_Color = {
  "name": "--pf-c-search-input__text--hover__icon--Color",
  "value": "#151515",
  "var": "var(--pf-c-search-input__text--hover__icon--Color)"
};
exports["default"] = exports.c_search_input__text_hover__icon_Color;
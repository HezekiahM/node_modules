"use strict";
exports.__esModule = true;
exports.global_active_color_100 = {
  "name": "--pf-global--active-color--100",
  "value": "#06c",
  "var": "var(--pf-global--active-color--100)"
};
exports["default"] = exports.global_active_color_100;
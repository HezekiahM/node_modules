export const c_form_control_m_warning_BorderBottomColor: {
  "name": "--pf-c-form-control--m-warning--BorderBottomColor",
  "value": "#f0ab00",
  "var": "var(--pf-c-form-control--m-warning--BorderBottomColor)"
};
export default c_form_control_m_warning_BorderBottomColor;
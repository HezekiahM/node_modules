"use strict";
exports.__esModule = true;
exports.c_alert__icon_MarginTop = {
  "name": "--pf-c-alert__icon--MarginTop",
  "value": "0.0625rem",
  "var": "var(--pf-c-alert__icon--MarginTop)"
};
exports["default"] = exports.c_alert__icon_MarginTop;
export const c_wizard__main_body_xl_PaddingLeft: {
  "name": "--pf-c-wizard__main-body--xl--PaddingLeft",
  "value": "1.5rem",
  "var": "var(--pf-c-wizard__main-body--xl--PaddingLeft)"
};
export default c_wizard__main_body_xl_PaddingLeft;
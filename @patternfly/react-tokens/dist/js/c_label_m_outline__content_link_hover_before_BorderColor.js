"use strict";
exports.__esModule = true;
exports.c_label_m_outline__content_link_hover_before_BorderColor = {
  "name": "--pf-c-label--m-outline__content--link--hover--before--BorderColor",
  "value": "#a2d9d9",
  "var": "var(--pf-c-label--m-outline__content--link--hover--before--BorderColor)"
};
exports["default"] = exports.c_label_m_outline__content_link_hover_before_BorderColor;
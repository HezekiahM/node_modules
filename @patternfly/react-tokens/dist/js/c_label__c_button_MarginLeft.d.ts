export const c_label__c_button_MarginLeft: {
  "name": "--pf-c-label__c-button--MarginLeft",
  "value": "0.25rem",
  "var": "var(--pf-c-label__c-button--MarginLeft)"
};
export default c_label__c_button_MarginLeft;
export const c_expandable_section__toggle_PaddingBottom: {
  "name": "--pf-c-expandable-section__toggle--PaddingBottom",
  "value": "0.375rem",
  "var": "var(--pf-c-expandable-section__toggle--PaddingBottom)"
};
export default c_expandable_section__toggle_PaddingBottom;
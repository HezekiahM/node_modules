"use strict";
exports.__esModule = true;
exports.c_popover_FontSize = {
  "name": "--pf-c-popover--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-popover--FontSize)"
};
exports["default"] = exports.c_popover_FontSize;
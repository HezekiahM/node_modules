export const c_banner_PaddingBottom: {
  "name": "--pf-c-banner--PaddingBottom",
  "value": "0.25rem",
  "var": "var(--pf-c-banner--PaddingBottom)"
};
export default c_banner_PaddingBottom;
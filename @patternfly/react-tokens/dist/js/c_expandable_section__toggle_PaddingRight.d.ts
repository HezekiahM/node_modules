export const c_expandable_section__toggle_PaddingRight: {
  "name": "--pf-c-expandable-section__toggle--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-expandable-section__toggle--PaddingRight)"
};
export default c_expandable_section__toggle_PaddingRight;
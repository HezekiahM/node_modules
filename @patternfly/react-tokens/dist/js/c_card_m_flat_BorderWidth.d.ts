export const c_card_m_flat_BorderWidth: {
  "name": "--pf-c-card--m-flat--BorderWidth",
  "value": "1px",
  "var": "var(--pf-c-card--m-flat--BorderWidth)"
};
export default c_card_m_flat_BorderWidth;
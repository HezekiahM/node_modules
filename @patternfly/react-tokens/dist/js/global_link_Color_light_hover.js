"use strict";
exports.__esModule = true;
exports.global_link_Color_light_hover = {
  "name": "--pf-global--link--Color--light--hover",
  "value": "#2b9af3",
  "var": "var(--pf-global--link--Color--light--hover)"
};
exports["default"] = exports.global_link_Color_light_hover;
"use strict";
exports.__esModule = true;
exports.c_notification_badge_m_unread_after_BackgroundColor = {
  "name": "--pf-c-notification-badge--m-unread--after--BackgroundColor",
  "value": "#2b9af3",
  "var": "var(--pf-c-notification-badge--m-unread--after--BackgroundColor)"
};
exports["default"] = exports.c_notification_badge_m_unread_after_BackgroundColor;
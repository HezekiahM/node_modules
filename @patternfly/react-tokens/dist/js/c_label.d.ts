export const c_label: {
  ".pf-c-label": {
    "c_label_PaddingTop": {
      "name": "--pf-c-label--PaddingTop",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_label_PaddingRight": {
      "name": "--pf-c-label--PaddingRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_label_PaddingBottom": {
      "name": "--pf-c-label--PaddingBottom",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_label_PaddingLeft": {
      "name": "--pf-c-label--PaddingLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_label_BorderRadius": {
      "name": "--pf-c-label--BorderRadius",
      "value": "30em",
      "values": [
        "--pf-global--BorderRadius--lg",
        "$pf-global--BorderRadius--lg",
        "30em"
      ]
    },
    "c_label_BackgroundColor": {
      "name": "--pf-c-label--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--200",
        "$pf-global--BackgroundColor--200",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_label_Color": {
      "name": "--pf-c-label--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_label_FontSize": {
      "name": "--pf-c-label--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_label__content_before_BorderWidth": {
      "name": "--pf-c-label__content--before--BorderWidth",
      "value": "0"
    },
    "c_label__content_before_BorderColor": {
      "name": "--pf-c-label__content--before--BorderColor",
      "value": "transparent"
    },
    "c_label_m_outline_BackgroundColor": {
      "name": "--pf-c-label--m-outline--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_label_m_outline__content_before_BorderWidth": {
      "name": "--pf-c-label--m-outline__content--before--BorderWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_label_m_outline__content_before_BorderColor": {
      "name": "--pf-c-label--m-outline__content--before--BorderColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_label__content_link_hover_before_BorderWidth": {
      "name": "--pf-c-label__content--link--hover--before--BorderWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_label__content_link_focus_before_BorderWidth": {
      "name": "--pf-c-label__content--link--focus--before--BorderWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_label__content_link_hover_before_BorderColor": {
      "name": "--pf-c-label__content--link--hover--before--BorderColor",
      "value": "#8a8d90",
      "values": [
        "--pf-global--BorderColor--200",
        "$pf-global--BorderColor--200",
        "$pf-color-black-500",
        "#8a8d90"
      ]
    },
    "c_label__content_link_focus_before_BorderColor": {
      "name": "--pf-c-label__content--link--focus--before--BorderColor",
      "value": "#8a8d90",
      "values": [
        "--pf-global--BorderColor--200",
        "$pf-global--BorderColor--200",
        "$pf-color-black-500",
        "#8a8d90"
      ]
    },
    "c_label_m_outline__content_link_hover_before_BorderWidth": {
      "name": "--pf-c-label--m-outline__content--link--hover--before--BorderWidth",
      "value": "2px",
      "values": [
        "--pf-global--BorderWidth--md",
        "$pf-global--BorderWidth--md",
        "2px"
      ]
    },
    "c_label_m_outline__content_link_focus_before_BorderWidth": {
      "name": "--pf-c-label--m-outline__content--link--focus--before--BorderWidth",
      "value": "2px",
      "values": [
        "--pf-global--BorderWidth--md",
        "$pf-global--BorderWidth--md",
        "2px"
      ]
    },
    "c_label_m_outline__content_link_hover_before_BorderColor": {
      "name": "--pf-c-label--m-outline__content--link--hover--before--BorderColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_label_m_outline__content_link_focus_before_BorderColor": {
      "name": "--pf-c-label--m-outline__content--link--focus--before--BorderColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_label_m_blue_BackgroundColor": {
      "name": "--pf-c-label--m-blue--BackgroundColor",
      "value": "#e7f1fa",
      "values": [
        "--pf-global--palette--blue-50",
        "$pf-color-blue-50",
        "#e7f1fa"
      ]
    },
    "c_label_m_blue__content_Color": {
      "name": "--pf-c-label--m-blue__content--Color",
      "value": "#002952",
      "values": [
        "--pf-global--info-color--200",
        "$pf-global--info-color--200",
        "$pf-color-blue-600",
        "#002952"
      ]
    },
    "c_label_m_blue__icon_Color": {
      "name": "--pf-c-label--m-blue__icon--Color",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_label_m_blue__content_link_hover_before_BorderColor": {
      "name": "--pf-c-label--m-blue__content--link--hover--before--BorderColor",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_label_m_blue__content_link_focus_before_BorderColor": {
      "name": "--pf-c-label--m-blue__content--link--focus--before--BorderColor",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_label_m_outline_m_blue__content_before_BorderColor": {
      "name": "--pf-c-label--m-outline--m-blue__content--before--BorderColor",
      "value": "#bee1f4",
      "values": [
        "--pf-global--active-color--200",
        "$pf-global--active-color--200",
        "$pf-color-blue-100",
        "#bee1f4"
      ]
    },
    "c_label_m_outline_m_blue__content_link_hover_before_BorderColor": {
      "name": "--pf-c-label--m-outline--m-blue__content--link--hover--before--BorderColor",
      "value": "#bee1f4",
      "values": [
        "--pf-global--active-color--200",
        "$pf-global--active-color--200",
        "$pf-color-blue-100",
        "#bee1f4"
      ]
    },
    "c_label_m_outline_m_blue__content_link_focus_before_BorderColor": {
      "name": "--pf-c-label--m-outline--m-blue__content--link--focus--before--BorderColor",
      "value": "#bee1f4",
      "values": [
        "--pf-global--active-color--200",
        "$pf-global--active-color--200",
        "$pf-color-blue-100",
        "#bee1f4"
      ]
    },
    "c_label_m_green_BackgroundColor": {
      "name": "--pf-c-label--m-green--BackgroundColor",
      "value": "#f3faf2",
      "values": [
        "--pf-global--palette--green-50",
        "$pf-color-green-50",
        "#f3faf2"
      ]
    },
    "c_label_m_green__content_Color": {
      "name": "--pf-c-label--m-green__content--Color",
      "value": "#0f280d",
      "values": [
        "--pf-global--success-color--200",
        "$pf-global--success-color--200",
        "$pf-color-green-700",
        "#0f280d"
      ]
    },
    "c_label_m_green__icon_Color": {
      "name": "--pf-c-label--m-green__icon--Color",
      "value": "#3e8635",
      "values": [
        "--pf-global--success-color--100",
        "$pf-global--success-color--100",
        "$pf-color-green-500",
        "#3e8635"
      ]
    },
    "c_label_m_green__content_link_hover_before_BorderColor": {
      "name": "--pf-c-label--m-green__content--link--hover--before--BorderColor",
      "value": "#3e8635",
      "values": [
        "--pf-global--success-color--100",
        "$pf-global--success-color--100",
        "$pf-color-green-500",
        "#3e8635"
      ]
    },
    "c_label_m_green__content_link_focus_before_BorderColor": {
      "name": "--pf-c-label--m-green__content--link--focus--before--BorderColor",
      "value": "#3e8635",
      "values": [
        "--pf-global--success-color--100",
        "$pf-global--success-color--100",
        "$pf-color-green-500",
        "#3e8635"
      ]
    },
    "c_label_m_outline_m_green__content_before_BorderColor": {
      "name": "--pf-c-label--m-outline--m-green__content--before--BorderColor",
      "value": "#bde5b8",
      "values": [
        "--pf-global--palette--green-100",
        "$pf-color-green-100",
        "#bde5b8"
      ]
    },
    "c_label_m_outline_m_green__content_link_hover_before_BorderColor": {
      "name": "--pf-c-label--m-outline--m-green__content--link--hover--before--BorderColor",
      "value": "#bde5b8",
      "values": [
        "--pf-global--palette--green-100",
        "$pf-color-green-100",
        "#bde5b8"
      ]
    },
    "c_label_m_outline_m_green__content_link_focus_before_BorderColor": {
      "name": "--pf-c-label--m-outline--m-green__content--link--focus--before--BorderColor",
      "value": "#bde5b8",
      "values": [
        "--pf-global--palette--green-100",
        "$pf-color-green-100",
        "#bde5b8"
      ]
    },
    "c_label_m_orange_BackgroundColor": {
      "name": "--pf-c-label--m-orange--BackgroundColor",
      "value": "#fdf7e7",
      "values": [
        "--pf-global--palette--gold-50",
        "$pf-color-gold-50",
        "#fdf7e7"
      ]
    },
    "c_label_m_orange__content_Color": {
      "name": "--pf-c-label--m-orange__content--Color",
      "value": "#3d2c00",
      "values": [
        "--pf-global--palette--gold-700",
        "$pf-color-gold-700",
        "#3d2c00"
      ]
    },
    "c_label_m_orange__icon_Color": {
      "name": "--pf-c-label--m-orange__icon--Color",
      "value": "#ec7a08",
      "values": [
        "--pf-global--palette--orange-300",
        "$pf-color-orange-300",
        "#ec7a08"
      ]
    },
    "c_label_m_orange__content_link_hover_before_BorderColor": {
      "name": "--pf-c-label--m-orange__content--link--hover--before--BorderColor",
      "value": "#ec7a08",
      "values": [
        "--pf-global--palette--orange-300",
        "$pf-color-orange-300",
        "#ec7a08"
      ]
    },
    "c_label_m_orange__content_link_focus_before_BorderColor": {
      "name": "--pf-c-label--m-orange__content--link--focus--before--BorderColor",
      "value": "#ec7a08",
      "values": [
        "--pf-global--palette--orange-300",
        "$pf-color-orange-300",
        "#ec7a08"
      ]
    },
    "c_label_m_outline_m_orange__content_before_BorderColor": {
      "name": "--pf-c-label--m-outline--m-orange__content--before--BorderColor",
      "value": "#f9e0a2",
      "values": [
        "--pf-global--palette--gold-100",
        "$pf-color-gold-100",
        "#f9e0a2"
      ]
    },
    "c_label_m_outline_m_orange__content_link_hover_before_BorderColor": {
      "name": "--pf-c-label--m-outline--m-orange__content--link--hover--before--BorderColor",
      "value": "#f9e0a2",
      "values": [
        "--pf-global--palette--gold-100",
        "$pf-color-gold-100",
        "#f9e0a2"
      ]
    },
    "c_label_m_outline_m_orange__content_link_focus_before_BorderColor": {
      "name": "--pf-c-label--m-outline--m-orange__content--link--focus--before--BorderColor",
      "value": "#f9e0a2",
      "values": [
        "--pf-global--palette--gold-100",
        "$pf-color-gold-100",
        "#f9e0a2"
      ]
    },
    "c_label_m_red_BackgroundColor": {
      "name": "--pf-c-label--m-red--BackgroundColor",
      "value": "#faeae8",
      "values": [
        "--pf-global--palette--red-50",
        "$pf-color-red-50",
        "#faeae8"
      ]
    },
    "c_label_m_red__content_Color": {
      "name": "--pf-c-label--m-red__content--Color",
      "value": "#7d1007",
      "values": [
        "--pf-global--palette--red-300",
        "$pf-color-red-300",
        "#7d1007"
      ]
    },
    "c_label_m_red__icon_Color": {
      "name": "--pf-c-label--m-red__icon--Color",
      "value": "#c9190b",
      "values": [
        "--pf-global--danger-color--100",
        "$pf-global--danger-color--100",
        "$pf-color-red-100",
        "#c9190b"
      ]
    },
    "c_label_m_red__content_link_hover_before_BorderColor": {
      "name": "--pf-c-label--m-red__content--link--hover--before--BorderColor",
      "value": "#c9190b",
      "values": [
        "--pf-global--danger-color--100",
        "$pf-global--danger-color--100",
        "$pf-color-red-100",
        "#c9190b"
      ]
    },
    "c_label_m_red__content_link_focus_before_BorderColor": {
      "name": "--pf-c-label--m-red__content--link--focus--before--BorderColor",
      "value": "#c9190b",
      "values": [
        "--pf-global--danger-color--100",
        "$pf-global--danger-color--100",
        "$pf-color-red-100",
        "#c9190b"
      ]
    },
    "c_label_m_outline_m_red__content_before_BorderColor": {
      "name": "--pf-c-label--m-outline--m-red__content--before--BorderColor",
      "value": "#c9190b",
      "values": [
        "--pf-global--danger-color--100",
        "$pf-global--danger-color--100",
        "$pf-color-red-100",
        "#c9190b"
      ]
    },
    "c_label_m_outline_m_red__content_link_hover_before_BorderColor": {
      "name": "--pf-c-label--m-outline--m-red__content--link--hover--before--BorderColor",
      "value": "#c9190b",
      "values": [
        "--pf-global--danger-color--100",
        "$pf-global--danger-color--100",
        "$pf-color-red-100",
        "#c9190b"
      ]
    },
    "c_label_m_outline_m_red__content_link_focus_before_BorderColor": {
      "name": "--pf-c-label--m-outline--m-red__content--link--focus--before--BorderColor",
      "value": "#c9190b",
      "values": [
        "--pf-global--danger-color--100",
        "$pf-global--danger-color--100",
        "$pf-color-red-100",
        "#c9190b"
      ]
    },
    "c_label_m_purple_BackgroundColor": {
      "name": "--pf-c-label--m-purple--BackgroundColor",
      "value": "#f2f0fc",
      "values": [
        "--pf-global--palette--purple-50",
        "$pf-color-purple-50",
        "#f2f0fc"
      ]
    },
    "c_label_m_purple__content_Color": {
      "name": "--pf-c-label--m-purple__content--Color",
      "value": "#1f0066",
      "values": [
        "--pf-global--palette--purple-700",
        "$pf-color-purple-700",
        "#1f0066"
      ]
    },
    "c_label_m_purple__icon_Color": {
      "name": "--pf-c-label--m-purple__icon--Color",
      "value": "#6753ac",
      "values": [
        "--pf-global--palette--purple-500",
        "$pf-color-purple-500",
        "#6753ac"
      ]
    },
    "c_label_m_purple__content_link_hover_before_BorderColor": {
      "name": "--pf-c-label--m-purple__content--link--hover--before--BorderColor",
      "value": "#6753ac",
      "values": [
        "--pf-global--palette--purple-500",
        "$pf-color-purple-500",
        "#6753ac"
      ]
    },
    "c_label_m_purple__content_link_focus_before_BorderColor": {
      "name": "--pf-c-label--m-purple__content--link--focus--before--BorderColor",
      "value": "#6753ac",
      "values": [
        "--pf-global--palette--purple-500",
        "$pf-color-purple-500",
        "#6753ac"
      ]
    },
    "c_label_m_outline_m_purple__content_before_BorderColor": {
      "name": "--pf-c-label--m-outline--m-purple__content--before--BorderColor",
      "value": "#cbc1ff",
      "values": [
        "--pf-global--palette--purple-100",
        "$pf-color-purple-100",
        "#cbc1ff"
      ]
    },
    "c_label_m_outline_m_purple__content_link_hover_before_BorderColor": {
      "name": "--pf-c-label--m-outline--m-purple__content--link--hover--before--BorderColor",
      "value": "#cbc1ff",
      "values": [
        "--pf-global--palette--purple-100",
        "$pf-color-purple-100",
        "#cbc1ff"
      ]
    },
    "c_label_m_outline_m_purple__content_link_focus_before_BorderColor": {
      "name": "--pf-c-label--m-outline--m-purple__content--link--focus--before--BorderColor",
      "value": "#cbc1ff",
      "values": [
        "--pf-global--palette--purple-100",
        "$pf-color-purple-100",
        "#cbc1ff"
      ]
    },
    "c_label_m_cyan_BackgroundColor": {
      "name": "--pf-c-label--m-cyan--BackgroundColor",
      "value": "#f2f9f9",
      "values": [
        "--pf-global--palette--cyan-50",
        "$pf-color-cyan-50",
        "#f2f9f9"
      ]
    },
    "c_label_m_cyan__content_Color": {
      "name": "--pf-c-label--m-cyan__content--Color",
      "value": "#003737",
      "values": [
        "--pf-global--default-color--300",
        "$pf-global--default-color--300",
        "$pf-color-cyan-500",
        "#003737"
      ]
    },
    "c_label_m_cyan__icon_Color": {
      "name": "--pf-c-label--m-cyan__icon--Color",
      "value": "#009596",
      "values": [
        "--pf-global--default-color--200",
        "$pf-global--default-color--200",
        "$pf-color-cyan-300",
        "#009596"
      ]
    },
    "c_label_m_cyan__content_link_hover_before_BorderColor": {
      "name": "--pf-c-label--m-cyan__content--link--hover--before--BorderColor",
      "value": "#009596",
      "values": [
        "--pf-global--default-color--200",
        "$pf-global--default-color--200",
        "$pf-color-cyan-300",
        "#009596"
      ]
    },
    "c_label_m_cyan__content_link_focus_before_BorderColor": {
      "name": "--pf-c-label--m-cyan__content--link--focus--before--BorderColor",
      "value": "#009596",
      "values": [
        "--pf-global--default-color--200",
        "$pf-global--default-color--200",
        "$pf-color-cyan-300",
        "#009596"
      ]
    },
    "c_label_m_outline_m_cyan__content_before_BorderColor": {
      "name": "--pf-c-label--m-outline--m-cyan__content--before--BorderColor",
      "value": "#a2d9d9",
      "values": [
        "--pf-global--palette--cyan-100",
        "$pf-color-cyan-100",
        "#a2d9d9"
      ]
    },
    "c_label_m_outline_m_cyan__content_link_hover_before_BorderColor": {
      "name": "--pf-c-label--m-outline--m-cyan__content--link--hover--before--BorderColor",
      "value": "#a2d9d9",
      "values": [
        "--pf-global--palette--cyan-100",
        "$pf-color-cyan-100",
        "#a2d9d9"
      ]
    },
    "c_label_m_outline_m_cyan__content_link_focus_before_BorderColor": {
      "name": "--pf-c-label--m-outline--m-cyan__content--link--focus--before--BorderColor",
      "value": "#a2d9d9",
      "values": [
        "--pf-global--palette--cyan-100",
        "$pf-color-cyan-100",
        "#a2d9d9"
      ]
    },
    "c_label__content_Color": {
      "name": "--pf-c-label__content--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_label__icon_Color": {
      "name": "--pf-c-label__icon--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_label__icon_MarginRight": {
      "name": "--pf-c-label__icon--MarginRight",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_label__c_button_FontSize": {
      "name": "--pf-c-label__c-button--FontSize",
      "value": "0.75rem",
      "values": [
        "--pf-global--FontSize--xs",
        "$pf-global--FontSize--xs",
        "pf-font-prem(12px)",
        "0.75rem"
      ]
    },
    "c_label__c_button_MarginTop": {
      "name": "--pf-c-label__c-button--MarginTop",
      "value": "calc(0.375rem * -1)",
      "values": [
        "calc(--pf-global--spacer--form-element * -1)",
        "calc($pf-global--spacer--form-element * -1)",
        "calc(pf-size-prem(6px) * -1)",
        "calc(0.375rem * -1)"
      ]
    },
    "c_label__c_button_MarginRight": {
      "name": "--pf-c-label__c-button--MarginRight",
      "value": "calc(0.375rem * -1)",
      "values": [
        "calc(--pf-global--spacer--form-element * -1)",
        "calc($pf-global--spacer--form-element * -1)",
        "calc(pf-size-prem(6px) * -1)",
        "calc(0.375rem * -1)"
      ]
    },
    "c_label__c_button_MarginBottom": {
      "name": "--pf-c-label__c-button--MarginBottom",
      "value": "calc(0.375rem * -1)",
      "values": [
        "calc(--pf-global--spacer--form-element * -1)",
        "calc($pf-global--spacer--form-element * -1)",
        "calc(pf-size-prem(6px) * -1)",
        "calc(0.375rem * -1)"
      ]
    },
    "c_label__c_button_MarginLeft": {
      "name": "--pf-c-label__c-button--MarginLeft",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_label__c_button_PaddingTop": {
      "name": "--pf-c-label__c-button--PaddingTop",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_label__c_button_PaddingRight": {
      "name": "--pf-c-label__c-button--PaddingRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_label__c_button_PaddingBottom": {
      "name": "--pf-c-label__c-button--PaddingBottom",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_label__c_button_PaddingLeft": {
      "name": "--pf-c-label__c-button--PaddingLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-c-label.pf-m-blue": {
    "c_label_BackgroundColor": {
      "name": "--pf-c-label--BackgroundColor",
      "value": "#e7f1fa",
      "values": [
        "--pf-c-label--m-blue--BackgroundColor",
        "--pf-global--palette--blue-50",
        "$pf-color-blue-50",
        "#e7f1fa"
      ]
    },
    "c_label__content_Color": {
      "name": "--pf-c-label__content--Color",
      "value": "#002952",
      "values": [
        "--pf-c-label--m-blue__content--Color",
        "--pf-global--info-color--200",
        "$pf-global--info-color--200",
        "$pf-color-blue-600",
        "#002952"
      ]
    },
    "c_label__icon_Color": {
      "name": "--pf-c-label__icon--Color",
      "value": "#06c",
      "values": [
        "--pf-c-label--m-blue__icon--Color",
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_label_m_outline__content_before_BorderColor": {
      "name": "--pf-c-label--m-outline__content--before--BorderColor",
      "value": "#bee1f4",
      "values": [
        "--pf-c-label--m-outline--m-blue__content--before--BorderColor",
        "--pf-global--active-color--200",
        "$pf-global--active-color--200",
        "$pf-color-blue-100",
        "#bee1f4"
      ]
    },
    "c_label__content_link_hover_before_BorderColor": {
      "name": "--pf-c-label__content--link--hover--before--BorderColor",
      "value": "#06c",
      "values": [
        "--pf-c-label--m-blue__content--link--hover--before--BorderColor",
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_label__content_link_focus_before_BorderColor": {
      "name": "--pf-c-label__content--link--focus--before--BorderColor",
      "value": "#06c",
      "values": [
        "--pf-c-label--m-blue__content--link--focus--before--BorderColor",
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_label_m_outline__content_link_hover_before_BorderColor": {
      "name": "--pf-c-label--m-outline__content--link--hover--before--BorderColor",
      "value": "#bee1f4",
      "values": [
        "--pf-c-label--m-outline--m-blue__content--link--hover--before--BorderColor",
        "--pf-global--active-color--200",
        "$pf-global--active-color--200",
        "$pf-color-blue-100",
        "#bee1f4"
      ]
    },
    "c_label_m_outline__content_link_focus_before_BorderColor": {
      "name": "--pf-c-label--m-outline__content--link--focus--before--BorderColor",
      "value": "#bee1f4",
      "values": [
        "--pf-c-label--m-outline--m-blue__content--link--focus--before--BorderColor",
        "--pf-global--active-color--200",
        "$pf-global--active-color--200",
        "$pf-color-blue-100",
        "#bee1f4"
      ]
    }
  },
  ".pf-c-label.pf-m-green": {
    "c_label_BackgroundColor": {
      "name": "--pf-c-label--BackgroundColor",
      "value": "#f3faf2",
      "values": [
        "--pf-c-label--m-green--BackgroundColor",
        "--pf-global--palette--green-50",
        "$pf-color-green-50",
        "#f3faf2"
      ]
    },
    "c_label__content_Color": {
      "name": "--pf-c-label__content--Color",
      "value": "#0f280d",
      "values": [
        "--pf-c-label--m-green__content--Color",
        "--pf-global--success-color--200",
        "$pf-global--success-color--200",
        "$pf-color-green-700",
        "#0f280d"
      ]
    },
    "c_label__icon_Color": {
      "name": "--pf-c-label__icon--Color",
      "value": "#3e8635",
      "values": [
        "--pf-c-label--m-green__icon--Color",
        "--pf-global--success-color--100",
        "$pf-global--success-color--100",
        "$pf-color-green-500",
        "#3e8635"
      ]
    },
    "c_label_m_outline__content_before_BorderColor": {
      "name": "--pf-c-label--m-outline__content--before--BorderColor",
      "value": "#bde5b8",
      "values": [
        "--pf-c-label--m-outline--m-green__content--before--BorderColor",
        "--pf-global--palette--green-100",
        "$pf-color-green-100",
        "#bde5b8"
      ]
    },
    "c_label__content_link_hover_before_BorderColor": {
      "name": "--pf-c-label__content--link--hover--before--BorderColor",
      "value": "#3e8635",
      "values": [
        "--pf-c-label--m-green__content--link--hover--before--BorderColor",
        "--pf-global--success-color--100",
        "$pf-global--success-color--100",
        "$pf-color-green-500",
        "#3e8635"
      ]
    },
    "c_label__content_link_focus_before_BorderColor": {
      "name": "--pf-c-label__content--link--focus--before--BorderColor",
      "value": "#3e8635",
      "values": [
        "--pf-c-label--m-green__content--link--focus--before--BorderColor",
        "--pf-global--success-color--100",
        "$pf-global--success-color--100",
        "$pf-color-green-500",
        "#3e8635"
      ]
    },
    "c_label_m_outline__content_link_hover_before_BorderColor": {
      "name": "--pf-c-label--m-outline__content--link--hover--before--BorderColor",
      "value": "#bde5b8",
      "values": [
        "--pf-c-label--m-outline--m-green__content--link--hover--before--BorderColor",
        "--pf-global--palette--green-100",
        "$pf-color-green-100",
        "#bde5b8"
      ]
    },
    "c_label_m_outline__content_link_focus_before_BorderColor": {
      "name": "--pf-c-label--m-outline__content--link--focus--before--BorderColor",
      "value": "#bde5b8",
      "values": [
        "--pf-c-label--m-outline--m-green__content--link--focus--before--BorderColor",
        "--pf-global--palette--green-100",
        "$pf-color-green-100",
        "#bde5b8"
      ]
    }
  },
  ".pf-c-label.pf-m-orange": {
    "c_label_BackgroundColor": {
      "name": "--pf-c-label--BackgroundColor",
      "value": "#fdf7e7",
      "values": [
        "--pf-c-label--m-orange--BackgroundColor",
        "--pf-global--palette--gold-50",
        "$pf-color-gold-50",
        "#fdf7e7"
      ]
    },
    "c_label__content_Color": {
      "name": "--pf-c-label__content--Color",
      "value": "#3d2c00",
      "values": [
        "--pf-c-label--m-orange__content--Color",
        "--pf-global--palette--gold-700",
        "$pf-color-gold-700",
        "#3d2c00"
      ]
    },
    "c_label__icon_Color": {
      "name": "--pf-c-label__icon--Color",
      "value": "#ec7a08",
      "values": [
        "--pf-c-label--m-orange__icon--Color",
        "--pf-global--palette--orange-300",
        "$pf-color-orange-300",
        "#ec7a08"
      ]
    },
    "c_label_m_outline__content_before_BorderColor": {
      "name": "--pf-c-label--m-outline__content--before--BorderColor",
      "value": "#f9e0a2",
      "values": [
        "--pf-c-label--m-outline--m-orange__content--before--BorderColor",
        "--pf-global--palette--gold-100",
        "$pf-color-gold-100",
        "#f9e0a2"
      ]
    },
    "c_label__content_link_hover_before_BorderColor": {
      "name": "--pf-c-label__content--link--hover--before--BorderColor",
      "value": "#ec7a08",
      "values": [
        "--pf-c-label--m-orange__content--link--hover--before--BorderColor",
        "--pf-global--palette--orange-300",
        "$pf-color-orange-300",
        "#ec7a08"
      ]
    },
    "c_label__content_link_focus_before_BorderColor": {
      "name": "--pf-c-label__content--link--focus--before--BorderColor",
      "value": "#ec7a08",
      "values": [
        "--pf-c-label--m-orange__content--link--focus--before--BorderColor",
        "--pf-global--palette--orange-300",
        "$pf-color-orange-300",
        "#ec7a08"
      ]
    },
    "c_label_m_outline__content_link_hover_before_BorderColor": {
      "name": "--pf-c-label--m-outline__content--link--hover--before--BorderColor",
      "value": "#f9e0a2",
      "values": [
        "--pf-c-label--m-outline--m-orange__content--link--hover--before--BorderColor",
        "--pf-global--palette--gold-100",
        "$pf-color-gold-100",
        "#f9e0a2"
      ]
    },
    "c_label_m_outline__content_link_focus_before_BorderColor": {
      "name": "--pf-c-label--m-outline__content--link--focus--before--BorderColor",
      "value": "#f9e0a2",
      "values": [
        "--pf-c-label--m-outline--m-orange__content--link--focus--before--BorderColor",
        "--pf-global--palette--gold-100",
        "$pf-color-gold-100",
        "#f9e0a2"
      ]
    }
  },
  ".pf-c-label.pf-m-red": {
    "c_label_BackgroundColor": {
      "name": "--pf-c-label--BackgroundColor",
      "value": "#faeae8",
      "values": [
        "--pf-c-label--m-red--BackgroundColor",
        "--pf-global--palette--red-50",
        "$pf-color-red-50",
        "#faeae8"
      ]
    },
    "c_label__content_Color": {
      "name": "--pf-c-label__content--Color",
      "value": "#7d1007",
      "values": [
        "--pf-c-label--m-red__content--Color",
        "--pf-global--palette--red-300",
        "$pf-color-red-300",
        "#7d1007"
      ]
    },
    "c_label__icon_Color": {
      "name": "--pf-c-label__icon--Color",
      "value": "#c9190b",
      "values": [
        "--pf-c-label--m-red__icon--Color",
        "--pf-global--danger-color--100",
        "$pf-global--danger-color--100",
        "$pf-color-red-100",
        "#c9190b"
      ]
    },
    "c_label_m_outline__content_before_BorderColor": {
      "name": "--pf-c-label--m-outline__content--before--BorderColor",
      "value": "#c9190b",
      "values": [
        "--pf-c-label--m-outline--m-red__content--before--BorderColor",
        "--pf-global--danger-color--100",
        "$pf-global--danger-color--100",
        "$pf-color-red-100",
        "#c9190b"
      ]
    },
    "c_label__content_link_hover_before_BorderColor": {
      "name": "--pf-c-label__content--link--hover--before--BorderColor",
      "value": "#c9190b",
      "values": [
        "--pf-c-label--m-red__content--link--hover--before--BorderColor",
        "--pf-global--danger-color--100",
        "$pf-global--danger-color--100",
        "$pf-color-red-100",
        "#c9190b"
      ]
    },
    "c_label__content_link_focus_before_BorderColor": {
      "name": "--pf-c-label__content--link--focus--before--BorderColor",
      "value": "#c9190b",
      "values": [
        "--pf-c-label--m-red__content--link--focus--before--BorderColor",
        "--pf-global--danger-color--100",
        "$pf-global--danger-color--100",
        "$pf-color-red-100",
        "#c9190b"
      ]
    },
    "c_label_m_outline__content_link_hover_before_BorderColor": {
      "name": "--pf-c-label--m-outline__content--link--hover--before--BorderColor",
      "value": "#c9190b",
      "values": [
        "--pf-c-label--m-outline--m-red__content--link--hover--before--BorderColor",
        "--pf-global--danger-color--100",
        "$pf-global--danger-color--100",
        "$pf-color-red-100",
        "#c9190b"
      ]
    },
    "c_label_m_outline__content_link_focus_before_BorderColor": {
      "name": "--pf-c-label--m-outline__content--link--focus--before--BorderColor",
      "value": "#c9190b",
      "values": [
        "--pf-c-label--m-outline--m-red__content--link--focus--before--BorderColor",
        "--pf-global--danger-color--100",
        "$pf-global--danger-color--100",
        "$pf-color-red-100",
        "#c9190b"
      ]
    }
  },
  ".pf-c-label.pf-m-purple": {
    "c_label_BackgroundColor": {
      "name": "--pf-c-label--BackgroundColor",
      "value": "#f2f0fc",
      "values": [
        "--pf-c-label--m-purple--BackgroundColor",
        "--pf-global--palette--purple-50",
        "$pf-color-purple-50",
        "#f2f0fc"
      ]
    },
    "c_label__content_Color": {
      "name": "--pf-c-label__content--Color",
      "value": "#1f0066",
      "values": [
        "--pf-c-label--m-purple__content--Color",
        "--pf-global--palette--purple-700",
        "$pf-color-purple-700",
        "#1f0066"
      ]
    },
    "c_label__icon_Color": {
      "name": "--pf-c-label__icon--Color",
      "value": "#6753ac",
      "values": [
        "--pf-c-label--m-purple__icon--Color",
        "--pf-global--palette--purple-500",
        "$pf-color-purple-500",
        "#6753ac"
      ]
    },
    "c_label_m_outline__content_before_BorderColor": {
      "name": "--pf-c-label--m-outline__content--before--BorderColor",
      "value": "#cbc1ff",
      "values": [
        "--pf-c-label--m-outline--m-purple__content--before--BorderColor",
        "--pf-global--palette--purple-100",
        "$pf-color-purple-100",
        "#cbc1ff"
      ]
    },
    "c_label__content_link_hover_before_BorderColor": {
      "name": "--pf-c-label__content--link--hover--before--BorderColor",
      "value": "#6753ac",
      "values": [
        "--pf-c-label--m-purple__content--link--hover--before--BorderColor",
        "--pf-global--palette--purple-500",
        "$pf-color-purple-500",
        "#6753ac"
      ]
    },
    "c_label__content_link_focus_before_BorderColor": {
      "name": "--pf-c-label__content--link--focus--before--BorderColor",
      "value": "#6753ac",
      "values": [
        "--pf-c-label--m-purple__content--link--focus--before--BorderColor",
        "--pf-global--palette--purple-500",
        "$pf-color-purple-500",
        "#6753ac"
      ]
    },
    "c_label_m_outline__content_link_hover_before_BorderColor": {
      "name": "--pf-c-label--m-outline__content--link--hover--before--BorderColor",
      "value": "#cbc1ff",
      "values": [
        "--pf-c-label--m-outline--m-purple__content--link--hover--before--BorderColor",
        "--pf-global--palette--purple-100",
        "$pf-color-purple-100",
        "#cbc1ff"
      ]
    },
    "c_label_m_outline__content_link_focus_before_BorderColor": {
      "name": "--pf-c-label--m-outline__content--link--focus--before--BorderColor",
      "value": "#cbc1ff",
      "values": [
        "--pf-c-label--m-outline--m-purple__content--link--focus--before--BorderColor",
        "--pf-global--palette--purple-100",
        "$pf-color-purple-100",
        "#cbc1ff"
      ]
    }
  },
  ".pf-c-label.pf-m-cyan": {
    "c_label_BackgroundColor": {
      "name": "--pf-c-label--BackgroundColor",
      "value": "#f2f9f9",
      "values": [
        "--pf-c-label--m-cyan--BackgroundColor",
        "--pf-global--palette--cyan-50",
        "$pf-color-cyan-50",
        "#f2f9f9"
      ]
    },
    "c_label__content_Color": {
      "name": "--pf-c-label__content--Color",
      "value": "#003737",
      "values": [
        "--pf-c-label--m-cyan__content--Color",
        "--pf-global--default-color--300",
        "$pf-global--default-color--300",
        "$pf-color-cyan-500",
        "#003737"
      ]
    },
    "c_label__icon_Color": {
      "name": "--pf-c-label__icon--Color",
      "value": "#009596",
      "values": [
        "--pf-c-label--m-cyan__icon--Color",
        "--pf-global--default-color--200",
        "$pf-global--default-color--200",
        "$pf-color-cyan-300",
        "#009596"
      ]
    },
    "c_label_m_outline__content_before_BorderColor": {
      "name": "--pf-c-label--m-outline__content--before--BorderColor",
      "value": "#a2d9d9",
      "values": [
        "--pf-c-label--m-outline--m-cyan__content--before--BorderColor",
        "--pf-global--palette--cyan-100",
        "$pf-color-cyan-100",
        "#a2d9d9"
      ]
    },
    "c_label__content_link_hover_before_BorderColor": {
      "name": "--pf-c-label__content--link--hover--before--BorderColor",
      "value": "#009596",
      "values": [
        "--pf-c-label--m-cyan__content--link--hover--before--BorderColor",
        "--pf-global--default-color--200",
        "$pf-global--default-color--200",
        "$pf-color-cyan-300",
        "#009596"
      ]
    },
    "c_label__content_link_focus_before_BorderColor": {
      "name": "--pf-c-label__content--link--focus--before--BorderColor",
      "value": "#009596",
      "values": [
        "--pf-c-label--m-cyan__content--link--focus--before--BorderColor",
        "--pf-global--default-color--200",
        "$pf-global--default-color--200",
        "$pf-color-cyan-300",
        "#009596"
      ]
    },
    "c_label_m_outline__content_link_hover_before_BorderColor": {
      "name": "--pf-c-label--m-outline__content--link--hover--before--BorderColor",
      "value": "#a2d9d9",
      "values": [
        "--pf-c-label--m-outline--m-cyan__content--link--hover--before--BorderColor",
        "--pf-global--palette--cyan-100",
        "$pf-color-cyan-100",
        "#a2d9d9"
      ]
    },
    "c_label_m_outline__content_link_focus_before_BorderColor": {
      "name": "--pf-c-label--m-outline__content--link--focus--before--BorderColor",
      "value": "#a2d9d9",
      "values": [
        "--pf-c-label--m-outline--m-cyan__content--link--focus--before--BorderColor",
        "--pf-global--palette--cyan-100",
        "$pf-color-cyan-100",
        "#a2d9d9"
      ]
    }
  },
  ".pf-c-label.pf-m-outline": {
    "c_label__content_before_BorderWidth": {
      "name": "--pf-c-label__content--before--BorderWidth",
      "value": "1px",
      "values": [
        "--pf-c-label--m-outline__content--before--BorderWidth",
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_label__content_before_BorderColor": {
      "name": "--pf-c-label__content--before--BorderColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-c-label--m-outline__content--before--BorderColor",
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_label_BackgroundColor": {
      "name": "--pf-c-label--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-c-label--m-outline--BackgroundColor",
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    }
  },
  ".pf-c-label.pf-m-outline a.pf-c-label__content:hover": {
    "c_label__content_before_BorderWidth": {
      "name": "--pf-c-label__content--before--BorderWidth",
      "value": "2px",
      "values": [
        "--pf-c-label--m-outline__content--link--hover--before--BorderWidth",
        "--pf-global--BorderWidth--md",
        "$pf-global--BorderWidth--md",
        "2px"
      ]
    },
    "c_label__content_before_BorderColor": {
      "name": "--pf-c-label__content--before--BorderColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-c-label--m-outline__content--link--hover--before--BorderColor",
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    }
  },
  ".pf-c-label.pf-m-outline a.pf-c-label__content:focus": {
    "c_label__content_before_BorderWidth": {
      "name": "--pf-c-label__content--before--BorderWidth",
      "value": "2px",
      "values": [
        "--pf-c-label--m-outline__content--link--focus--before--BorderWidth",
        "--pf-global--BorderWidth--md",
        "$pf-global--BorderWidth--md",
        "2px"
      ]
    },
    "c_label__content_before_BorderColor": {
      "name": "--pf-c-label__content--before--BorderColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-c-label--m-outline__content--link--focus--before--BorderColor",
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    }
  },
  ".pf-c-label .pf-c-button": {
    "c_button_FontSize": {
      "name": "--pf-c-button--FontSize",
      "value": "0.75rem",
      "values": [
        "--pf-c-label__c-button--FontSize",
        "--pf-global--FontSize--xs",
        "$pf-global--FontSize--xs",
        "pf-font-prem(12px)",
        "0.75rem"
      ]
    },
    "c_button_PaddingTop": {
      "name": "--pf-c-button--PaddingTop",
      "value": "0.25rem",
      "values": [
        "--pf-c-label__c-button--PaddingTop",
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_button_PaddingRight": {
      "name": "--pf-c-button--PaddingRight",
      "value": "0.5rem",
      "values": [
        "--pf-c-label__c-button--PaddingRight",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_button_PaddingBottom": {
      "name": "--pf-c-button--PaddingBottom",
      "value": "0.25rem",
      "values": [
        "--pf-c-label__c-button--PaddingBottom",
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_button_PaddingLeft": {
      "name": "--pf-c-button--PaddingLeft",
      "value": "0.5rem",
      "values": [
        "--pf-c-label__c-button--PaddingLeft",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  "a.pf-c-label__content:hover": {
    "c_label__content_before_BorderWidth": {
      "name": "--pf-c-label__content--before--BorderWidth",
      "value": "1px",
      "values": [
        "--pf-c-label__content--link--hover--before--BorderWidth",
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_label__content_before_BorderColor": {
      "name": "--pf-c-label__content--before--BorderColor",
      "value": "#8a8d90",
      "values": [
        "--pf-c-label__content--link--hover--before--BorderColor",
        "--pf-global--BorderColor--200",
        "$pf-global--BorderColor--200",
        "$pf-color-black-500",
        "#8a8d90"
      ]
    }
  },
  "a.pf-c-label__content:focus": {
    "c_label__content_before_BorderWidth": {
      "name": "--pf-c-label__content--before--BorderWidth",
      "value": "1px",
      "values": [
        "--pf-c-label__content--link--focus--before--BorderWidth",
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_label__content_before_BorderColor": {
      "name": "--pf-c-label__content--before--BorderColor",
      "value": "#8a8d90",
      "values": [
        "--pf-c-label__content--link--focus--before--BorderColor",
        "--pf-global--BorderColor--200",
        "$pf-global--BorderColor--200",
        "$pf-color-black-500",
        "#8a8d90"
      ]
    }
  }
};
export default c_label;
"use strict";
exports.__esModule = true;
exports.chart_global_Fill_Color_200 = {
  "name": "--pf-chart-global--Fill--Color--200",
  "value": "#f0f0f0",
  "var": "var(--pf-chart-global--Fill--Color--200)"
};
exports["default"] = exports.chart_global_Fill_Color_200;
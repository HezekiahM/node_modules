"use strict";
exports.__esModule = true;
exports.global_palette_blue_700 = {
  "name": "--pf-global--palette--blue-700",
  "value": "#001223",
  "var": "var(--pf-global--palette--blue-700)"
};
exports["default"] = exports.global_palette_blue_700;
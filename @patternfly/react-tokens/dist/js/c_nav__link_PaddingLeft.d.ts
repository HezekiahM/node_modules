export const c_nav__link_PaddingLeft: {
  "name": "--pf-c-nav__link--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-nav__link--PaddingLeft)"
};
export default c_nav__link_PaddingLeft;
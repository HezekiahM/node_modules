export const c_tooltip__arrow_m_top_TranslateX: {
  "name": "--pf-c-tooltip__arrow--m-top--TranslateX",
  "value": "-50%",
  "var": "var(--pf-c-tooltip__arrow--m-top--TranslateX)"
};
export default c_tooltip__arrow_m_top_TranslateX;
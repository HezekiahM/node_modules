"use strict";
exports.__esModule = true;
exports.global_Color_dark_200 = {
  "name": "--pf-global--Color--dark-200",
  "value": "#6a6e73",
  "var": "var(--pf-global--Color--dark-200)"
};
exports["default"] = exports.global_Color_dark_200;
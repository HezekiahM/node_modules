"use strict";
exports.__esModule = true;
exports.c_table_cell_Width = {
  "name": "--pf-c-table--cell--Width",
  "value": "100%",
  "var": "var(--pf-c-table--cell--Width)"
};
exports["default"] = exports.c_table_cell_Width;
export const c_hint_PaddingRight: {
  "name": "--pf-c-hint--PaddingRight",
  "value": "1.5rem",
  "var": "var(--pf-c-hint--PaddingRight)"
};
export default c_hint_PaddingRight;
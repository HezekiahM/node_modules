"use strict";
exports.__esModule = true;
exports.c_dropdown__menu_item_LineHeight = {
  "name": "--pf-c-dropdown__menu-item--LineHeight",
  "value": "1.5",
  "var": "var(--pf-c-dropdown__menu-item--LineHeight)"
};
exports["default"] = exports.c_dropdown__menu_item_LineHeight;
export const c_accordion__toggle_active__toggle_text_FontWeight: {
  "name": "--pf-c-accordion__toggle--active__toggle-text--FontWeight",
  "value": "700",
  "var": "var(--pf-c-accordion__toggle--active__toggle-text--FontWeight)"
};
export default c_accordion__toggle_active__toggle_text_FontWeight;
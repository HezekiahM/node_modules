export const c_popover_MinWidth: {
  "name": "--pf-c-popover--MinWidth",
  "value": "calc(1rem + 1rem + 18.75rem)",
  "var": "var(--pf-c-popover--MinWidth)"
};
export default c_popover_MinWidth;
"use strict";
exports.__esModule = true;
exports.c_alert__FontSize = {
  "name": "--pf-c-alert__FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-alert__FontSize)"
};
exports["default"] = exports.c_alert__FontSize;
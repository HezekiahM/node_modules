export const c_tile_m_disabled__header_m_stacked__icon_Color: {
  "name": "--pf-c-tile--m-disabled__header--m-stacked__icon--Color",
  "value": "#6a6e73",
  "var": "var(--pf-c-tile--m-disabled__header--m-stacked__icon--Color)"
};
export default c_tile_m_disabled__header_m_stacked__icon_Color;
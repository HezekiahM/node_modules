"use strict";
exports.__esModule = true;
exports.c_nav_m_horizontal__link_m_current_Color = {
  "name": "--pf-c-nav--m-horizontal__link--m-current--Color",
  "value": "#2b9af3",
  "var": "var(--pf-c-nav--m-horizontal__link--m-current--Color)"
};
exports["default"] = exports.c_nav_m_horizontal__link_m_current_Color;
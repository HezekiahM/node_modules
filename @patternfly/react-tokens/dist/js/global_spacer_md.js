"use strict";
exports.__esModule = true;
exports.global_spacer_md = {
  "name": "--pf-global--spacer--md",
  "value": "1rem",
  "var": "var(--pf-global--spacer--md)"
};
exports["default"] = exports.global_spacer_md;
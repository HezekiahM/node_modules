"use strict";
exports.__esModule = true;
exports.c_table_tr_responsive_PaddingRight = {
  "name": "--pf-c-table-tr--responsive--PaddingRight",
  "value": "1.5rem",
  "var": "var(--pf-c-table-tr--responsive--PaddingRight)"
};
exports["default"] = exports.c_table_tr_responsive_PaddingRight;
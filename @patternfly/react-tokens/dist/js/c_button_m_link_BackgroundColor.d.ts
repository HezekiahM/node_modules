export const c_button_m_link_BackgroundColor: {
  "name": "--pf-c-button--m-link--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-button--m-link--BackgroundColor)"
};
export default c_button_m_link_BackgroundColor;
"use strict";
exports.__esModule = true;
exports.c_radio__label_FontWeight = {
  "name": "--pf-c-radio__label--FontWeight",
  "value": "400",
  "var": "var(--pf-c-radio__label--FontWeight)"
};
exports["default"] = exports.c_radio__label_FontWeight;
"use strict";
exports.__esModule = true;
exports.c_content_blockquote_BorderLeftWidth = {
  "name": "--pf-c-content--blockquote--BorderLeftWidth",
  "value": "3px",
  "var": "var(--pf-c-content--blockquote--BorderLeftWidth)"
};
exports["default"] = exports.c_content_blockquote_BorderLeftWidth;
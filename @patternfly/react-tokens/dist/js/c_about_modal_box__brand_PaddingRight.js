"use strict";
exports.__esModule = true;
exports.c_about_modal_box__brand_PaddingRight = {
  "name": "--pf-c-about-modal-box__brand--PaddingRight",
  "value": "2rem",
  "var": "var(--pf-c-about-modal-box__brand--PaddingRight)"
};
exports["default"] = exports.c_about_modal_box__brand_PaddingRight;
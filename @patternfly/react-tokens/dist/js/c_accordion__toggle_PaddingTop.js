"use strict";
exports.__esModule = true;
exports.c_accordion__toggle_PaddingTop = {
  "name": "--pf-c-accordion__toggle--PaddingTop",
  "value": "0.5rem",
  "var": "var(--pf-c-accordion__toggle--PaddingTop)"
};
exports["default"] = exports.c_accordion__toggle_PaddingTop;
"use strict";
exports.__esModule = true;
exports.global_palette_cyan_200 = {
  "name": "--pf-global--palette--cyan-200",
  "value": "#73c5c5",
  "var": "var(--pf-global--palette--cyan-200)"
};
exports["default"] = exports.global_palette_cyan_200;
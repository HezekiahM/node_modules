export const c_nav_m_horizontal__link_before_BorderColor: {
  "name": "--pf-c-nav--m-horizontal__link--before--BorderColor",
  "value": "#2b9af3",
  "var": "var(--pf-c-nav--m-horizontal__link--before--BorderColor)"
};
export default c_nav_m_horizontal__link_before_BorderColor;
"use strict";
exports.__esModule = true;
exports.global_BorderColor_300 = {
  "name": "--pf-global--BorderColor--300",
  "value": "#f0f0f0",
  "var": "var(--pf-global--BorderColor--300)"
};
exports["default"] = exports.global_BorderColor_300;
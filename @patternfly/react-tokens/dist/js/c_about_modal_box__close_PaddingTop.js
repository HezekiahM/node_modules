"use strict";
exports.__esModule = true;
exports.c_about_modal_box__close_PaddingTop = {
  "name": "--pf-c-about-modal-box__close--PaddingTop",
  "value": "3rem",
  "var": "var(--pf-c-about-modal-box__close--PaddingTop)"
};
exports["default"] = exports.c_about_modal_box__close_PaddingTop;
export const c_alert__title_max_lines: {
  "name": "--pf-c-alert__title--max-lines",
  "value": "1",
  "var": "var(--pf-c-alert__title--max-lines)"
};
export default c_alert__title_max_lines;
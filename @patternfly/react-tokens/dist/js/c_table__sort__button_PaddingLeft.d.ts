export const c_table__sort__button_PaddingLeft: {
  "name": "--pf-c-table__sort__button--PaddingLeft",
  "value": "0.5rem",
  "var": "var(--pf-c-table__sort__button--PaddingLeft)"
};
export default c_table__sort__button_PaddingLeft;
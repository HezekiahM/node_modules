"use strict";
exports.__esModule = true;
exports.c_button_m_danger_hover_Color = {
  "name": "--pf-c-button--m-danger--hover--Color",
  "value": "#fff",
  "var": "var(--pf-c-button--m-danger--hover--Color)"
};
exports["default"] = exports.c_button_m_danger_hover_Color;
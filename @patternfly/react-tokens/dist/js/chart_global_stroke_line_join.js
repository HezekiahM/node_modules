"use strict";
exports.__esModule = true;
exports.chart_global_stroke_line_join = {
  "name": "--pf-chart-global--stroke-line-join",
  "value": "round",
  "var": "var(--pf-chart-global--stroke-line-join)"
};
exports["default"] = exports.chart_global_stroke_line_join;
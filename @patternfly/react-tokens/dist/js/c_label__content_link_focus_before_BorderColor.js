"use strict";
exports.__esModule = true;
exports.c_label__content_link_focus_before_BorderColor = {
  "name": "--pf-c-label__content--link--focus--before--BorderColor",
  "value": "#009596",
  "var": "var(--pf-c-label__content--link--focus--before--BorderColor)"
};
exports["default"] = exports.c_label__content_link_focus_before_BorderColor;
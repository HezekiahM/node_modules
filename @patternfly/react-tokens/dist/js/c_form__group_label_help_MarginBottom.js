"use strict";
exports.__esModule = true;
exports.c_form__group_label_help_MarginBottom = {
  "name": "--pf-c-form__group-label-help--MarginBottom",
  "value": "calc(0.25rem * -1)",
  "var": "var(--pf-c-form__group-label-help--MarginBottom)"
};
exports["default"] = exports.c_form__group_label_help_MarginBottom;
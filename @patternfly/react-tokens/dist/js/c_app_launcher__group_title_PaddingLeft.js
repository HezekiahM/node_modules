"use strict";
exports.__esModule = true;
exports.c_app_launcher__group_title_PaddingLeft = {
  "name": "--pf-c-app-launcher__group-title--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-app-launcher__group-title--PaddingLeft)"
};
exports["default"] = exports.c_app_launcher__group_title_PaddingLeft;
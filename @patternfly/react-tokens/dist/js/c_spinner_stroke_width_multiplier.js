"use strict";
exports.__esModule = true;
exports.c_spinner_stroke_width_multiplier = {
  "name": "--pf-c-spinner--stroke-width-multiplier",
  "value": ".1",
  "var": "var(--pf-c-spinner--stroke-width-multiplier)"
};
exports["default"] = exports.c_spinner_stroke_width_multiplier;
"use strict";
exports.__esModule = true;
exports.chart_pie_data_Padding = {
  "name": "--pf-chart-pie--data--Padding",
  "value": 8,
  "var": "var(--pf-chart-pie--data--Padding)"
};
exports["default"] = exports.chart_pie_data_Padding;
"use strict";
exports.__esModule = true;
exports.chart_global_stroke_Width_xs = {
  "name": "--pf-chart-global--stroke--Width--xs",
  "value": 1,
  "var": "var(--pf-chart-global--stroke--Width--xs)"
};
exports["default"] = exports.chart_global_stroke_Width_xs;
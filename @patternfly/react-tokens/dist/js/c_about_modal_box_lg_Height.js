"use strict";
exports.__esModule = true;
exports.c_about_modal_box_lg_Height = {
  "name": "--pf-c-about-modal-box--lg--Height",
  "value": "47.625rem",
  "var": "var(--pf-c-about-modal-box--lg--Height)"
};
exports["default"] = exports.c_about_modal_box_lg_Height;
"use strict";
exports.__esModule = true;
exports.c_app_launcher__menu_search_PaddingRight = {
  "name": "--pf-c-app-launcher__menu-search--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-app-launcher__menu-search--PaddingRight)"
};
exports["default"] = exports.c_app_launcher__menu_search_PaddingRight;
"use strict";
exports.__esModule = true;
exports.c_login__main_footer_PaddingBottom = {
  "name": "--pf-c-login__main-footer--PaddingBottom",
  "value": "4rem",
  "var": "var(--pf-c-login__main-footer--PaddingBottom)"
};
exports["default"] = exports.c_login__main_footer_PaddingBottom;
"use strict";
exports.__esModule = true;
exports.chart_color_cyan_100 = {
  "name": "--pf-chart-color-cyan-100",
  "value": "#a2d9d9",
  "var": "var(--pf-chart-color-cyan-100)"
};
exports["default"] = exports.chart_color_cyan_100;
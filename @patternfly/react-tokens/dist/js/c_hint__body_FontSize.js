"use strict";
exports.__esModule = true;
exports.c_hint__body_FontSize = {
  "name": "--pf-c-hint__body--FontSize",
  "value": "1rem",
  "var": "var(--pf-c-hint__body--FontSize)"
};
exports["default"] = exports.c_hint__body_FontSize;
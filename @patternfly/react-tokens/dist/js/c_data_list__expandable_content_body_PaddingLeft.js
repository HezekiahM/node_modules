"use strict";
exports.__esModule = true;
exports.c_data_list__expandable_content_body_PaddingLeft = {
  "name": "--pf-c-data-list__expandable-content-body--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-data-list__expandable-content-body--PaddingLeft)"
};
exports["default"] = exports.c_data_list__expandable_content_body_PaddingLeft;
"use strict";
exports.__esModule = true;
exports.c_data_list__cell_PaddingBottom = {
  "name": "--pf-c-data-list__cell--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-data-list__cell--PaddingBottom)"
};
exports["default"] = exports.c_data_list__cell_PaddingBottom;
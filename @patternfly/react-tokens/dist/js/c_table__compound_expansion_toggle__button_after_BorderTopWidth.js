"use strict";
exports.__esModule = true;
exports.c_table__compound_expansion_toggle__button_after_BorderTopWidth = {
  "name": "--pf-c-table__compound-expansion-toggle__button--after--BorderTopWidth",
  "value": "3px",
  "var": "var(--pf-c-table__compound-expansion-toggle__button--after--BorderTopWidth)"
};
exports["default"] = exports.c_table__compound_expansion_toggle__button_after_BorderTopWidth;
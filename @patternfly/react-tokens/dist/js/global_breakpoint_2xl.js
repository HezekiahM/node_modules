"use strict";
exports.__esModule = true;
exports.global_breakpoint_2xl = {
  "name": "--pf-global--breakpoint--2xl",
  "value": "1450px",
  "var": "var(--pf-global--breakpoint--2xl)"
};
exports["default"] = exports.global_breakpoint_2xl;
export const c_description_list_ColumnGap: {
  "name": "--pf-c-description-list--ColumnGap",
  "value": "1rem",
  "var": "var(--pf-c-description-list--ColumnGap)"
};
export default c_description_list_ColumnGap;
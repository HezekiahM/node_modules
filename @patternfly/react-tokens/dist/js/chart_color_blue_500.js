"use strict";
exports.__esModule = true;
exports.chart_color_blue_500 = {
  "name": "--pf-chart-color-blue-500",
  "value": "#002f5d",
  "var": "var(--pf-chart-color-blue-500)"
};
exports["default"] = exports.chart_color_blue_500;
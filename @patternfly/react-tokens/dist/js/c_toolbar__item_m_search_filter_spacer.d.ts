export const c_toolbar__item_m_search_filter_spacer: {
  "name": "--pf-c-toolbar__item--m-search-filter--spacer",
  "value": "0.5rem",
  "var": "var(--pf-c-toolbar__item--m-search-filter--spacer)"
};
export default c_toolbar__item_m_search_filter_spacer;
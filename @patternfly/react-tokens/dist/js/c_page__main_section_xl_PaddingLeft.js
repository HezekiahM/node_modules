"use strict";
exports.__esModule = true;
exports.c_page__main_section_xl_PaddingLeft = {
  "name": "--pf-c-page__main-section--xl--PaddingLeft",
  "value": "1.5rem",
  "var": "var(--pf-c-page__main-section--xl--PaddingLeft)"
};
exports["default"] = exports.c_page__main_section_xl_PaddingLeft;
export const c_description_list__group_RowGap: {
  "name": "--pf-c-description-list__group--RowGap",
  "value": "0.5rem",
  "var": "var(--pf-c-description-list__group--RowGap)"
};
export default c_description_list__group_RowGap;
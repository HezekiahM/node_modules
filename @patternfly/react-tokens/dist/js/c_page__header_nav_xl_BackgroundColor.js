"use strict";
exports.__esModule = true;
exports.c_page__header_nav_xl_BackgroundColor = {
  "name": "--pf-c-page__header-nav--xl--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-page__header-nav--xl--BackgroundColor)"
};
exports["default"] = exports.c_page__header_nav_xl_BackgroundColor;
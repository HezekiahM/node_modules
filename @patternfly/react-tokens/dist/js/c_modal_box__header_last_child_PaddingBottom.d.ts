export const c_modal_box__header_last_child_PaddingBottom: {
  "name": "--pf-c-modal-box__header--last-child--PaddingBottom",
  "value": "1.5rem",
  "var": "var(--pf-c-modal-box__header--last-child--PaddingBottom)"
};
export default c_modal_box__header_last_child_PaddingBottom;
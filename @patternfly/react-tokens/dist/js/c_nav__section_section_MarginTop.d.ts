export const c_nav__section_section_MarginTop: {
  "name": "--pf-c-nav__section--section--MarginTop",
  "value": "2rem",
  "var": "var(--pf-c-nav__section--section--MarginTop)"
};
export default c_nav__section_section_MarginTop;
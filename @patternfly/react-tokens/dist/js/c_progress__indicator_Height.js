"use strict";
exports.__esModule = true;
exports.c_progress__indicator_Height = {
  "name": "--pf-c-progress__indicator--Height",
  "value": "1rem",
  "var": "var(--pf-c-progress__indicator--Height)"
};
exports["default"] = exports.c_progress__indicator_Height;
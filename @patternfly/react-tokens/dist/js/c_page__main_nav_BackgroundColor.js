"use strict";
exports.__esModule = true;
exports.c_page__main_nav_BackgroundColor = {
  "name": "--pf-c-page__main-nav--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-page__main-nav--BackgroundColor)"
};
exports["default"] = exports.c_page__main_nav_BackgroundColor;
export const c_table__toggle_c_button_m_expanded__toggle_icon_Rotate: {
  "name": "--pf-c-table__toggle--c-button--m-expanded__toggle-icon--Rotate",
  "value": "360deg",
  "var": "var(--pf-c-table__toggle--c-button--m-expanded__toggle-icon--Rotate)"
};
export default c_table__toggle_c_button_m_expanded__toggle_icon_Rotate;
"use strict";
exports.__esModule = true;
exports.global_palette_gold_400 = {
  "name": "--pf-global--palette--gold-400",
  "value": "#f0ab00",
  "var": "var(--pf-global--palette--gold-400)"
};
exports["default"] = exports.global_palette_gold_400;
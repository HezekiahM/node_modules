"use strict";
exports.__esModule = true;
exports.c_modal_box_m_md_Width = {
  "name": "--pf-c-modal-box--m-md--Width",
  "value": "52.5rem",
  "var": "var(--pf-c-modal-box--m-md--Width)"
};
exports["default"] = exports.c_modal_box_m_md_Width;
"use strict";
exports.__esModule = true;
exports.c_drawer__panel_md_FlexBasis = {
  "name": "--pf-c-drawer__panel--md--FlexBasis",
  "value": "50%",
  "var": "var(--pf-c-drawer__panel--md--FlexBasis)"
};
exports["default"] = exports.c_drawer__panel_md_FlexBasis;
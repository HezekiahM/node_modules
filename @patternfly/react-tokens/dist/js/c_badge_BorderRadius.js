"use strict";
exports.__esModule = true;
exports.c_badge_BorderRadius = {
  "name": "--pf-c-badge--BorderRadius",
  "value": "30em",
  "var": "var(--pf-c-badge--BorderRadius)"
};
exports["default"] = exports.c_badge_BorderRadius;
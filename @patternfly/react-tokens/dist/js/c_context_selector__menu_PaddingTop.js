"use strict";
exports.__esModule = true;
exports.c_context_selector__menu_PaddingTop = {
  "name": "--pf-c-context-selector__menu--PaddingTop",
  "value": "0.5rem",
  "var": "var(--pf-c-context-selector__menu--PaddingTop)"
};
exports["default"] = exports.c_context_selector__menu_PaddingTop;
export const c_alert_PaddingLeft: {
  "name": "--pf-c-alert--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-alert--PaddingLeft)"
};
export default c_alert_PaddingLeft;
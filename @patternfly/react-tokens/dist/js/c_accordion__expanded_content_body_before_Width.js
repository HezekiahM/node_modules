"use strict";
exports.__esModule = true;
exports.c_accordion__expanded_content_body_before_Width = {
  "name": "--pf-c-accordion__expanded-content-body--before--Width",
  "value": "3px",
  "var": "var(--pf-c-accordion__expanded-content-body--before--Width)"
};
exports["default"] = exports.c_accordion__expanded_content_body_before_Width;
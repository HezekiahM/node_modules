"use strict";
exports.__esModule = true;
exports.c_search_input__utilities_child_MarginLeft = {
  "name": "--pf-c-search-input__utilities--child--MarginLeft",
  "value": "0.25rem",
  "var": "var(--pf-c-search-input__utilities--child--MarginLeft)"
};
exports["default"] = exports.c_search_input__utilities_child_MarginLeft;
export const c_label_m_outline__content_link_focus_before_BorderColor: {
  "name": "--pf-c-label--m-outline__content--link--focus--before--BorderColor",
  "value": "#a2d9d9",
  "var": "var(--pf-c-label--m-outline__content--link--focus--before--BorderColor)"
};
export default c_label_m_outline__content_link_focus_before_BorderColor;
export const c_alert_group: {
  ".pf-c-alert-group": {
    "c_alert_group__item_MarginTop": {
      "name": "--pf-c-alert-group__item--MarginTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_alert_group_m_toast_Top": {
      "name": "--pf-c-alert-group--m-toast--Top",
      "value": "3rem",
      "values": [
        "--pf-global--spacer--2xl",
        "$pf-global--spacer--2xl",
        "pf-size-prem(48px)",
        "3rem"
      ]
    },
    "c_alert_group_m_toast_Right": {
      "name": "--pf-c-alert-group--m-toast--Right",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_alert_group_m_toast_MaxWidth": {
      "name": "--pf-c-alert-group--m-toast--MaxWidth",
      "value": "37.5rem"
    },
    "c_alert_group_m_toast_ZIndex": {
      "name": "--pf-c-alert-group--m-toast--ZIndex",
      "value": "600",
      "values": [
        "--pf-global--ZIndex--2xl",
        "$pf-global--ZIndex--2xl",
        "600"
      ]
    }
  }
};
export default c_alert_group;
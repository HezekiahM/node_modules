export const c_label_m_outline_m_blue__content_link_hover_before_BorderColor: {
  "name": "--pf-c-label--m-outline--m-blue__content--link--hover--before--BorderColor",
  "value": "#bee1f4",
  "var": "var(--pf-c-label--m-outline--m-blue__content--link--hover--before--BorderColor)"
};
export default c_label_m_outline_m_blue__content_link_hover_before_BorderColor;
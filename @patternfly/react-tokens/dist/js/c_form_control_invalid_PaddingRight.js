"use strict";
exports.__esModule = true;
exports.c_form_control_invalid_PaddingRight = {
  "name": "--pf-c-form-control--invalid--PaddingRight",
  "value": "2rem",
  "var": "var(--pf-c-form-control--invalid--PaddingRight)"
};
exports["default"] = exports.c_form_control_invalid_PaddingRight;
"use strict";
exports.__esModule = true;
exports.c_login__header_PaddingRight = {
  "name": "--pf-c-login__header--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-login__header--PaddingRight)"
};
exports["default"] = exports.c_login__header_PaddingRight;
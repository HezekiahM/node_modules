"use strict";
exports.__esModule = true;
exports.c_accordion__toggle_PaddingLeft = {
  "name": "--pf-c-accordion__toggle--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-accordion__toggle--PaddingLeft)"
};
exports["default"] = exports.c_accordion__toggle_PaddingLeft;
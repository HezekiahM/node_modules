"use strict";
exports.__esModule = true;
exports.c_input_group__text_BorderBottomColor = {
  "name": "--pf-c-input-group__text--BorderBottomColor",
  "value": "#8a8d90",
  "var": "var(--pf-c-input-group__text--BorderBottomColor)"
};
exports["default"] = exports.c_input_group__text_BorderBottomColor;
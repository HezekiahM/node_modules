"use strict";
exports.__esModule = true;
exports.c_form_control_disabled_BorderColor = {
  "name": "--pf-c-form-control--disabled--BorderColor",
  "value": "transparent",
  "var": "var(--pf-c-form-control--disabled--BorderColor)"
};
exports["default"] = exports.c_form_control_disabled_BorderColor;
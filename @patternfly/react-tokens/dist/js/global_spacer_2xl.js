"use strict";
exports.__esModule = true;
exports.global_spacer_2xl = {
  "name": "--pf-global--spacer--2xl",
  "value": "3rem",
  "var": "var(--pf-global--spacer--2xl)"
};
exports["default"] = exports.global_spacer_2xl;
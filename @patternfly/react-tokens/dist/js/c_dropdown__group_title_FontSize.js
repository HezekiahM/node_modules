"use strict";
exports.__esModule = true;
exports.c_dropdown__group_title_FontSize = {
  "name": "--pf-c-dropdown__group-title--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-dropdown__group-title--FontSize)"
};
exports["default"] = exports.c_dropdown__group_title_FontSize;
export const c_simple_list: {
  ".pf-c-simple-list": {
    "c_simple_list__item_link_PaddingTop": {
      "name": "--pf-c-simple-list__item-link--PaddingTop",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_simple_list__item_link_PaddingRight": {
      "name": "--pf-c-simple-list__item-link--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_simple_list__item_link_PaddingBottom": {
      "name": "--pf-c-simple-list__item-link--PaddingBottom",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_simple_list__item_link_PaddingLeft": {
      "name": "--pf-c-simple-list__item-link--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_simple_list__item_link_BackgroundColor": {
      "name": "--pf-c-simple-list__item-link--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_simple_list__item_link_Color": {
      "name": "--pf-c-simple-list__item-link--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_simple_list__item_link_FontSize": {
      "name": "--pf-c-simple-list__item-link--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_simple_list__item_link_FontWeight": {
      "name": "--pf-c-simple-list__item-link--FontWeight",
      "value": "400",
      "values": [
        "--pf-global--FontWeight--normal",
        "$pf-global--FontWeight--normal",
        "400"
      ]
    },
    "c_simple_list__item_link_m_current_Color": {
      "name": "--pf-c-simple-list__item-link--m-current--Color",
      "value": "#06c",
      "values": [
        "--pf-global--link--Color",
        "$pf-global--link--Color",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_simple_list__item_link_m_current_BackgroundColor": {
      "name": "--pf-c-simple-list__item-link--m-current--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--200",
        "$pf-global--BackgroundColor--200",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_simple_list__item_link_m_current_FontWeight": {
      "name": "--pf-c-simple-list__item-link--m-current--FontWeight",
      "value": "700",
      "values": [
        "--pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    },
    "c_simple_list__item_link_hover_Color": {
      "name": "--pf-c-simple-list__item-link--hover--Color",
      "value": "#06c",
      "values": [
        "--pf-global--link--Color",
        "$pf-global--link--Color",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_simple_list__item_link_hover_BackgroundColor": {
      "name": "--pf-c-simple-list__item-link--hover--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--200",
        "$pf-global--BackgroundColor--200",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_simple_list__item_link_focus_Color": {
      "name": "--pf-c-simple-list__item-link--focus--Color",
      "value": "#06c",
      "values": [
        "--pf-global--link--Color",
        "$pf-global--link--Color",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_simple_list__item_link_focus_BackgroundColor": {
      "name": "--pf-c-simple-list__item-link--focus--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--200",
        "$pf-global--BackgroundColor--200",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_simple_list__item_link_focus_FontWeight": {
      "name": "--pf-c-simple-list__item-link--focus--FontWeight",
      "value": "700",
      "values": [
        "--pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    },
    "c_simple_list__item_link_active_Color": {
      "name": "--pf-c-simple-list__item-link--active--Color",
      "value": "#06c",
      "values": [
        "--pf-global--link--Color",
        "$pf-global--link--Color",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_simple_list__item_link_active_BackgroundColor": {
      "name": "--pf-c-simple-list__item-link--active--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--200",
        "$pf-global--BackgroundColor--200",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_simple_list__item_link_active_FontWeight": {
      "name": "--pf-c-simple-list__item-link--active--FontWeight",
      "value": "700",
      "values": [
        "--pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    },
    "c_simple_list__title_PaddingTop": {
      "name": "--pf-c-simple-list__title--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_simple_list__title_PaddingRight": {
      "name": "--pf-c-simple-list__title--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_simple_list__title_PaddingBottom": {
      "name": "--pf-c-simple-list__title--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_simple_list__title_PaddingLeft": {
      "name": "--pf-c-simple-list__title--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_simple_list__title_FontSize": {
      "name": "--pf-c-simple-list__title--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_simple_list__title_Color": {
      "name": "--pf-c-simple-list__title--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--dark-200",
        "$pf-global--Color--dark-200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_simple_list__title_FontWeight": {
      "name": "--pf-c-simple-list__title--FontWeight",
      "value": "700",
      "values": [
        "--pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    },
    "c_simple_list__section_section_MarginTop": {
      "name": "--pf-c-simple-list__section--section--MarginTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-c-simple-list__item-link.pf-m-current": {
    "c_simple_list__item_link_FontWeight": {
      "name": "--pf-c-simple-list__item-link--FontWeight",
      "value": "700",
      "values": [
        "--pf-c-simple-list__item-link--m-current--FontWeight",
        "--pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    },
    "c_simple_list__item_link_BackgroundColor": {
      "name": "--pf-c-simple-list__item-link--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-c-simple-list__item-link--m-current--BackgroundColor",
        "--pf-global--BackgroundColor--200",
        "$pf-global--BackgroundColor--200",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_simple_list__item_link_Color": {
      "name": "--pf-c-simple-list__item-link--Color",
      "value": "#06c",
      "values": [
        "--pf-c-simple-list__item-link--m-current--Color",
        "--pf-global--link--Color",
        "$pf-global--link--Color",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-simple-list__item-link:hover": {
    "c_simple_list__item_link_BackgroundColor": {
      "name": "--pf-c-simple-list__item-link--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-c-simple-list__item-link--hover--BackgroundColor",
        "--pf-global--BackgroundColor--200",
        "$pf-global--BackgroundColor--200",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_simple_list__item_link_Color": {
      "name": "--pf-c-simple-list__item-link--Color",
      "value": "#06c",
      "values": [
        "--pf-c-simple-list__item-link--hover--Color",
        "--pf-global--link--Color",
        "$pf-global--link--Color",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-simple-list__item-link:focus": {
    "c_simple_list__item_link_FontWeight": {
      "name": "--pf-c-simple-list__item-link--FontWeight",
      "value": "700",
      "values": [
        "--pf-c-simple-list__item-link--focus--FontWeight",
        "--pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    },
    "c_simple_list__item_link_BackgroundColor": {
      "name": "--pf-c-simple-list__item-link--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-c-simple-list__item-link--focus--BackgroundColor",
        "--pf-global--BackgroundColor--200",
        "$pf-global--BackgroundColor--200",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_simple_list__item_link_Color": {
      "name": "--pf-c-simple-list__item-link--Color",
      "value": "#06c",
      "values": [
        "--pf-c-simple-list__item-link--focus--Color",
        "--pf-global--link--Color",
        "$pf-global--link--Color",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-simple-list__item-link:active": {
    "c_simple_list__item_link_FontWeight": {
      "name": "--pf-c-simple-list__item-link--FontWeight",
      "value": "700",
      "values": [
        "--pf-c-simple-list__item-link--active--FontWeight",
        "--pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    },
    "c_simple_list__item_link_BackgroundColor": {
      "name": "--pf-c-simple-list__item-link--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-c-simple-list__item-link--active--BackgroundColor",
        "--pf-global--BackgroundColor--200",
        "$pf-global--BackgroundColor--200",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_simple_list__item_link_Color": {
      "name": "--pf-c-simple-list__item-link--Color",
      "value": "#06c",
      "values": [
        "--pf-c-simple-list__item-link--active--Color",
        "--pf-global--link--Color",
        "$pf-global--link--Color",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  }
};
export default c_simple_list;
"use strict";
exports.__esModule = true;
exports.chart_legend_data_type = {
  "name": "--pf-chart-legend--data--type",
  "value": "square",
  "var": "var(--pf-chart-legend--data--type)"
};
exports["default"] = exports.chart_legend_data_type;
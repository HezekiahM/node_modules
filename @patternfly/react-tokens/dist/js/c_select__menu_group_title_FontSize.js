"use strict";
exports.__esModule = true;
exports.c_select__menu_group_title_FontSize = {
  "name": "--pf-c-select__menu-group-title--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-select__menu-group-title--FontSize)"
};
exports["default"] = exports.c_select__menu_group_title_FontSize;
"use strict";
exports.__esModule = true;
exports.chart_donut_utilization_dynamic_pie_Width = {
  "name": "--pf-chart-donut--utilization--dynamic--pie--Width",
  "value": 230,
  "var": "var(--pf-chart-donut--utilization--dynamic--pie--Width)"
};
exports["default"] = exports.chart_donut_utilization_dynamic_pie_Width;
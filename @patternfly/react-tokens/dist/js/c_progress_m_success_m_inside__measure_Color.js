"use strict";
exports.__esModule = true;
exports.c_progress_m_success_m_inside__measure_Color = {
  "name": "--pf-c-progress--m-success--m-inside__measure--Color",
  "value": "#151515",
  "var": "var(--pf-c-progress--m-success--m-inside__measure--Color)"
};
exports["default"] = exports.c_progress_m_success_m_inside__measure_Color;
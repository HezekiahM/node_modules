"use strict";
exports.__esModule = true;
exports.c_notification_badge_after_TranslateY = {
  "name": "--pf-c-notification-badge--after--TranslateY",
  "value": "calc(1px * -1)",
  "var": "var(--pf-c-notification-badge--after--TranslateY)"
};
exports["default"] = exports.c_notification_badge_after_TranslateY;
"use strict";
exports.__esModule = true;
exports.c_nav__link_BackgroundColor = {
  "name": "--pf-c-nav__link--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-nav__link--BackgroundColor)"
};
exports["default"] = exports.c_nav__link_BackgroundColor;
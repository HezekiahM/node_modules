"use strict";
exports.__esModule = true;
exports.c_modal_box__header_PaddingRight = {
  "name": "--pf-c-modal-box__header--PaddingRight",
  "value": "1.5rem",
  "var": "var(--pf-c-modal-box__header--PaddingRight)"
};
exports["default"] = exports.c_modal_box__header_PaddingRight;
export const c_table_m_compact_tr_responsive_PaddingTop: {
  "name": "--pf-c-table--m-compact-tr--responsive--PaddingTop",
  "value": "0.5rem",
  "var": "var(--pf-c-table--m-compact-tr--responsive--PaddingTop)"
};
export default c_table_m_compact_tr_responsive_PaddingTop;
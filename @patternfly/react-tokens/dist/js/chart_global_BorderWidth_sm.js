"use strict";
exports.__esModule = true;
exports.chart_global_BorderWidth_sm = {
  "name": "--pf-chart-global--BorderWidth--sm",
  "value": 2,
  "var": "var(--pf-chart-global--BorderWidth--sm)"
};
exports["default"] = exports.chart_global_BorderWidth_sm;
"use strict";
exports.__esModule = true;
exports.c_app_launcher__menu_BackgroundColor = {
  "name": "--pf-c-app-launcher__menu--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-app-launcher__menu--BackgroundColor)"
};
exports["default"] = exports.c_app_launcher__menu_BackgroundColor;
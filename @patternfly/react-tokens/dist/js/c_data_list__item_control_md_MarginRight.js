"use strict";
exports.__esModule = true;
exports.c_data_list__item_control_md_MarginRight = {
  "name": "--pf-c-data-list__item-control--md--MarginRight",
  "value": "2rem",
  "var": "var(--pf-c-data-list__item-control--md--MarginRight)"
};
exports["default"] = exports.c_data_list__item_control_md_MarginRight;
"use strict";
exports.__esModule = true;
exports.c_table_caption_PaddingLeft = {
  "name": "--pf-c-table-caption--PaddingLeft",
  "value": "1.5rem",
  "var": "var(--pf-c-table-caption--PaddingLeft)"
};
exports["default"] = exports.c_table_caption_PaddingLeft;
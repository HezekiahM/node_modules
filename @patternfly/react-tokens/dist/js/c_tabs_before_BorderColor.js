"use strict";
exports.__esModule = true;
exports.c_tabs_before_BorderColor = {
  "name": "--pf-c-tabs--before--BorderColor",
  "value": "#d2d2d2",
  "var": "var(--pf-c-tabs--before--BorderColor)"
};
exports["default"] = exports.c_tabs_before_BorderColor;
"use strict";
exports.__esModule = true;
exports.c_title_m_lg_FontSize = {
  "name": "--pf-c-title--m-lg--FontSize",
  "value": "1.125rem",
  "var": "var(--pf-c-title--m-lg--FontSize)"
};
exports["default"] = exports.c_title_m_lg_FontSize;
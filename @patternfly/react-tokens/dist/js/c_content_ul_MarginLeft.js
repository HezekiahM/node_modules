"use strict";
exports.__esModule = true;
exports.c_content_ul_MarginLeft = {
  "name": "--pf-c-content--ul--MarginLeft",
  "value": "0.5rem",
  "var": "var(--pf-c-content--ul--MarginLeft)"
};
exports["default"] = exports.c_content_ul_MarginLeft;
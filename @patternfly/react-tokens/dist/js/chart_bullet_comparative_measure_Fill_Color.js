"use strict";
exports.__esModule = true;
exports.chart_bullet_comparative_measure_Fill_Color = {
  "name": "--pf-chart-bullet--comparative-measure--Fill--Color",
  "value": "#4f5255",
  "var": "var(--pf-chart-bullet--comparative-measure--Fill--Color)"
};
exports["default"] = exports.chart_bullet_comparative_measure_Fill_Color;
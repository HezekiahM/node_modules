"use strict";
exports.__esModule = true;
exports.global_link_Color_light = {
  "name": "--pf-global--link--Color--light",
  "value": "#73bcf7",
  "var": "var(--pf-global--link--Color--light)"
};
exports["default"] = exports.global_link_Color_light;
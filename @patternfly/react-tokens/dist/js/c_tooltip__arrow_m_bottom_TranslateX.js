"use strict";
exports.__esModule = true;
exports.c_tooltip__arrow_m_bottom_TranslateX = {
  "name": "--pf-c-tooltip__arrow--m-bottom--TranslateX",
  "value": "-50%",
  "var": "var(--pf-c-tooltip__arrow--m-bottom--TranslateX)"
};
exports["default"] = exports.c_tooltip__arrow_m_bottom_TranslateX;
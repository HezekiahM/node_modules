"use strict";
exports.__esModule = true;
exports.c_about_modal_box_lg_Width = {
  "name": "--pf-c-about-modal-box--lg--Width",
  "value": "calc(100% - (4rem * 2))",
  "var": "var(--pf-c-about-modal-box--lg--Width)"
};
exports["default"] = exports.c_about_modal_box_lg_Width;
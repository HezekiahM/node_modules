"use strict";
exports.__esModule = true;
exports.c_pagination__nav_page_select_child_MarginRight = {
  "name": "--pf-c-pagination__nav-page-select--child--MarginRight",
  "value": "0.25rem",
  "var": "var(--pf-c-pagination__nav-page-select--child--MarginRight)"
};
exports["default"] = exports.c_pagination__nav_page_select_child_MarginRight;
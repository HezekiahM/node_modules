"use strict";
exports.__esModule = true;
exports.c_content_h5_FontSize = {
  "name": "--pf-c-content--h5--FontSize",
  "value": "1rem",
  "var": "var(--pf-c-content--h5--FontSize)"
};
exports["default"] = exports.c_content_h5_FontSize;
"use strict";
exports.__esModule = true;
exports.c_tabs__link_focus_after_BorderWidth = {
  "name": "--pf-c-tabs__link--focus--after--BorderWidth",
  "value": "3px",
  "var": "var(--pf-c-tabs__link--focus--after--BorderWidth)"
};
exports["default"] = exports.c_tabs__link_focus_after_BorderWidth;
"use strict";
exports.__esModule = true;
exports.c_backdrop_BackgroundColor = {
  "name": "--pf-c-backdrop--BackgroundColor",
  "value": "rgba(#030303, .62)",
  "var": "var(--pf-c-backdrop--BackgroundColor)"
};
exports["default"] = exports.c_backdrop_BackgroundColor;
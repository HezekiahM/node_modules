"use strict";
exports.__esModule = true;
exports.c_context_selector__toggle_icon_MarginLeft = {
  "name": "--pf-c-context-selector__toggle-icon--MarginLeft",
  "value": "1rem",
  "var": "var(--pf-c-context-selector__toggle-icon--MarginLeft)"
};
exports["default"] = exports.c_context_selector__toggle_icon_MarginLeft;
"use strict";
exports.__esModule = true;
exports.c_dropdown__menu_item_icon_Width = {
  "name": "--pf-c-dropdown__menu-item-icon--Width",
  "value": "1.5rem",
  "var": "var(--pf-c-dropdown__menu-item-icon--Width)"
};
exports["default"] = exports.c_dropdown__menu_item_icon_Width;
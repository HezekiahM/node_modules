export const c_nav: {
  ".pf-c-nav": {
    "c_nav_Transition": {
      "name": "--pf-c-nav--Transition",
      "value": "all 250ms cubic-bezier(.42, 0, .58, 1)",
      "values": [
        "--pf-global--Transition",
        "$pf-global--Transition",
        "all 250ms cubic-bezier(.42, 0, .58, 1)"
      ]
    },
    "c_nav__item_m_expanded__toggle_icon_Rotate": {
      "name": "--pf-c-nav__item--m-expanded__toggle-icon--Rotate",
      "value": "90deg"
    },
    "c_nav_m_light__item_before_BorderColor": {
      "name": "--pf-c-nav--m-light__item--before--BorderColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_nav_m_light__item_m_current_not_m_expanded__link_BackgroundColor": {
      "name": "--pf-c-nav--m-light__item--m-current--not--m-expanded__link--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_nav_m_light__link_Color": {
      "name": "--pf-c-nav--m-light__link--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_nav_m_light__link_hover_Color": {
      "name": "--pf-c-nav--m-light__link--hover--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_nav_m_light__link_focus_Color": {
      "name": "--pf-c-nav--m-light__link--focus--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_nav_m_light__link_active_Color": {
      "name": "--pf-c-nav--m-light__link--active--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_nav_m_light__link_m_current_Color": {
      "name": "--pf-c-nav--m-light__link--m-current--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_nav_m_light__link_hover_BackgroundColor": {
      "name": "--pf-c-nav--m-light__link--hover--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_nav_m_light__link_focus_BackgroundColor": {
      "name": "--pf-c-nav--m-light__link--focus--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_nav_m_light__link_active_BackgroundColor": {
      "name": "--pf-c-nav--m-light__link--active--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_nav_m_light__link_m_current_BackgroundColor": {
      "name": "--pf-c-nav--m-light__link--m-current--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_nav_m_light__link_before_BorderColor": {
      "name": "--pf-c-nav--m-light__link--before--BorderColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_nav_m_light__link_after_BorderColor": {
      "name": "--pf-c-nav--m-light__link--after--BorderColor",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_nav_m_light__link_m_current_after_BorderColor": {
      "name": "--pf-c-nav--m-light__link--m-current--after--BorderColor",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_nav_m_light__section_title_Color": {
      "name": "--pf-c-nav--m-light__section-title--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--dark-200",
        "$pf-global--Color--dark-200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_nav_m_light__section_title_BorderBottomColor": {
      "name": "--pf-c-nav--m-light__section-title--BorderBottomColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_nav_m_light_c_divider_BackgroundColor": {
      "name": "--pf-c-nav--m-light--c-divider--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_nav_m_light__subnav__link_hover_after_BorderColor": {
      "name": "--pf-c-nav--m-light__subnav__link--hover--after--BorderColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--dark-100",
        "$pf-global--BorderColor--dark-100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_nav_m_light__subnav__link_focus_after_BorderColor": {
      "name": "--pf-c-nav--m-light__subnav__link--focus--after--BorderColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--dark-100",
        "$pf-global--BorderColor--dark-100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_nav_m_light__subnav__link_active_after_BorderColor": {
      "name": "--pf-c-nav--m-light__subnav__link--active--after--BorderColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--dark-100",
        "$pf-global--BorderColor--dark-100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_nav_m_light__subnav__link_m_current_after_BorderColor": {
      "name": "--pf-c-nav--m-light__subnav__link--m-current--after--BorderColor",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_nav__item_MarginTop": {
      "name": "--pf-c-nav__item--MarginTop",
      "value": "0"
    },
    "c_nav__item_m_current_not_m_expanded__link_BackgroundColor": {
      "name": "--pf-c-nav__item--m-current--not--m-expanded__link--BackgroundColor",
      "value": "#4f5255",
      "values": [
        "--pf-global--BackgroundColor--dark-400",
        "$pf-global--BackgroundColor--dark-400",
        "$pf-color-black-700",
        "#4f5255"
      ]
    },
    "c_nav__link_m_current_not_m_expanded__link_after_BorderWidth": {
      "name": "--pf-c-nav__link--m-current--not--m-expanded__link--after--BorderWidth",
      "value": "4px",
      "values": [
        "--pf-global--BorderWidth--xl",
        "$pf-global--BorderWidth--xl",
        "4px"
      ]
    },
    "c_nav__item_before_BorderColor": {
      "name": "--pf-c-nav__item--before--BorderColor",
      "value": "#3c3f42",
      "values": [
        "--pf-global--BackgroundColor--dark-200",
        "$pf-global--BackgroundColor--dark-200",
        "$pf-color-black-800",
        "#3c3f42"
      ]
    },
    "c_nav__item_before_BorderWidth": {
      "name": "--pf-c-nav__item--before--BorderWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_nav__link_FontSize": {
      "name": "--pf-c-nav__link--FontSize",
      "value": "1rem",
      "values": [
        "--pf-global--FontSize--md",
        "$pf-global--FontSize--md",
        "pf-font-prem(16px)",
        "1rem"
      ]
    },
    "c_nav__link_FontWeight": {
      "name": "--pf-c-nav__link--FontWeight",
      "value": "400",
      "values": [
        "--pf-global--FontWeight--normal",
        "$pf-global--FontWeight--normal",
        "400"
      ]
    },
    "c_nav__link_PaddingTop": {
      "name": "--pf-c-nav__link--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_nav__link_PaddingRight": {
      "name": "--pf-c-nav__link--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_nav__link_PaddingBottom": {
      "name": "--pf-c-nav__link--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_nav__link_PaddingLeft": {
      "name": "--pf-c-nav__link--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_nav__link_xl_PaddingRight": {
      "name": "--pf-c-nav__link--xl--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_nav__link_xl_PaddingLeft": {
      "name": "--pf-c-nav__link--xl--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_nav__link_Color": {
      "name": "--pf-c-nav__link--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_nav__link_hover_Color": {
      "name": "--pf-c-nav__link--hover--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_nav__link_focus_Color": {
      "name": "--pf-c-nav__link--focus--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_nav__link_active_Color": {
      "name": "--pf-c-nav__link--active--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_nav__link_m_current_Color": {
      "name": "--pf-c-nav__link--m-current--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_nav__link_BackgroundColor": {
      "name": "--pf-c-nav__link--BackgroundColor",
      "value": "transparent"
    },
    "c_nav__link_hover_BackgroundColor": {
      "name": "--pf-c-nav__link--hover--BackgroundColor",
      "value": "#3c3f42",
      "values": [
        "--pf-global--BackgroundColor--dark-200",
        "$pf-global--BackgroundColor--dark-200",
        "$pf-color-black-800",
        "#3c3f42"
      ]
    },
    "c_nav__link_focus_BackgroundColor": {
      "name": "--pf-c-nav__link--focus--BackgroundColor",
      "value": "#3c3f42",
      "values": [
        "--pf-global--BackgroundColor--dark-200",
        "$pf-global--BackgroundColor--dark-200",
        "$pf-color-black-800",
        "#3c3f42"
      ]
    },
    "c_nav__link_active_BackgroundColor": {
      "name": "--pf-c-nav__link--active--BackgroundColor",
      "value": "#3c3f42",
      "values": [
        "--pf-global--BackgroundColor--dark-200",
        "$pf-global--BackgroundColor--dark-200",
        "$pf-color-black-800",
        "#3c3f42"
      ]
    },
    "c_nav__link_m_current_BackgroundColor": {
      "name": "--pf-c-nav__link--m-current--BackgroundColor",
      "value": "#4f5255",
      "values": [
        "--pf-global--BackgroundColor--dark-400",
        "$pf-global--BackgroundColor--dark-400",
        "$pf-color-black-700",
        "#4f5255"
      ]
    },
    "c_nav__link_OutlineOffset": {
      "name": "--pf-c-nav__link--OutlineOffset",
      "value": "calc(0.25rem * -1)",
      "values": [
        "calc(--pf-global--spacer--xs * -1)",
        "calc($pf-global--spacer--xs * -1)",
        "calc(pf-size-prem(4px) * -1)",
        "calc(0.25rem * -1)"
      ]
    },
    "c_nav__link_before_BorderColor": {
      "name": "--pf-c-nav__link--before--BorderColor",
      "value": "#3c3f42",
      "values": [
        "--pf-global--BackgroundColor--dark-200",
        "$pf-global--BackgroundColor--dark-200",
        "$pf-color-black-800",
        "#3c3f42"
      ]
    },
    "c_nav__link_before_BorderBottomWidth": {
      "name": "--pf-c-nav__link--before--BorderBottomWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_nav__link_hover_before_BorderBottomWidth": {
      "name": "--pf-c-nav__link--hover--before--BorderBottomWidth",
      "value": "0"
    },
    "c_nav__link_focus_before_BorderBottomWidth": {
      "name": "--pf-c-nav__link--focus--before--BorderBottomWidth",
      "value": "0"
    },
    "c_nav__link_active_before_BorderBottomWidth": {
      "name": "--pf-c-nav__link--active--before--BorderBottomWidth",
      "value": "0"
    },
    "c_nav__link_m_current_before_BorderBottomWidth": {
      "name": "--pf-c-nav__link--m-current--before--BorderBottomWidth",
      "value": "0"
    },
    "c_nav__link_after_BorderColor": {
      "name": "--pf-c-nav__link--after--BorderColor",
      "value": "#2b9af3",
      "values": [
        "--pf-global--active-color--400",
        "$pf-global--active-color--400",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    },
    "c_nav__link_hover_after_BorderColor": {
      "name": "--pf-c-nav__link--hover--after--BorderColor",
      "value": "#2b9af3",
      "values": [
        "--pf-global--active-color--400",
        "$pf-global--active-color--400",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    },
    "c_nav__link_focus_after_BorderColor": {
      "name": "--pf-c-nav__link--focus--after--BorderColor",
      "value": "#2b9af3",
      "values": [
        "--pf-global--active-color--400",
        "$pf-global--active-color--400",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    },
    "c_nav__link_active_after_BorderColor": {
      "name": "--pf-c-nav__link--active--after--BorderColor",
      "value": "#2b9af3",
      "values": [
        "--pf-global--active-color--400",
        "$pf-global--active-color--400",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    },
    "c_nav__link_m_current_after_BorderColor": {
      "name": "--pf-c-nav__link--m-current--after--BorderColor",
      "value": "#2b9af3",
      "values": [
        "--pf-global--active-color--400",
        "$pf-global--active-color--400",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    },
    "c_nav__link_after_BorderLeftWidth": {
      "name": "--pf-c-nav__link--after--BorderLeftWidth",
      "value": "0"
    },
    "c_nav__link_hover_after_BorderLeftWidth": {
      "name": "--pf-c-nav__link--hover--after--BorderLeftWidth",
      "value": "0"
    },
    "c_nav__link_focus_after_BorderLeftWidth": {
      "name": "--pf-c-nav__link--focus--after--BorderLeftWidth",
      "value": "0"
    },
    "c_nav__link_active_after_BorderLeftWidth": {
      "name": "--pf-c-nav__link--active--after--BorderLeftWidth",
      "value": "0"
    },
    "c_nav__link_m_current_after_BorderLeftWidth": {
      "name": "--pf-c-nav__link--m-current--after--BorderLeftWidth",
      "value": "4px",
      "values": [
        "--pf-global--BorderWidth--xl",
        "$pf-global--BorderWidth--xl",
        "4px"
      ]
    },
    "c_nav_m_horizontal__link_PaddingTop": {
      "name": "--pf-c-nav--m-horizontal__link--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_nav_m_horizontal__link_PaddingRight": {
      "name": "--pf-c-nav--m-horizontal__link--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_nav_m_horizontal__link_PaddingBottom": {
      "name": "--pf-c-nav--m-horizontal__link--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_nav_m_horizontal__link_PaddingLeft": {
      "name": "--pf-c-nav--m-horizontal__link--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_nav_m_horizontal__link_lg_PaddingTop": {
      "name": "--pf-c-nav--m-horizontal__link--lg--PaddingTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_nav_m_horizontal__link_lg_PaddingBottom": {
      "name": "--pf-c-nav--m-horizontal__link--lg--PaddingBottom",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_nav_m_horizontal__link_Right": {
      "name": "--pf-c-nav--m-horizontal__link--Right",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_nav_m_horizontal__link_Left": {
      "name": "--pf-c-nav--m-horizontal__link--Left",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_nav_m_horizontal__link_Color": {
      "name": "--pf-c-nav--m-horizontal__link--Color",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--Color--light-300",
        "$pf-global--Color--light-300",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_nav_m_horizontal__link_hover_Color": {
      "name": "--pf-c-nav--m-horizontal__link--hover--Color",
      "value": "#2b9af3",
      "values": [
        "--pf-global--active-color--400",
        "$pf-global--active-color--400",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    },
    "c_nav_m_horizontal__link_focus_Color": {
      "name": "--pf-c-nav--m-horizontal__link--focus--Color",
      "value": "#2b9af3",
      "values": [
        "--pf-global--active-color--400",
        "$pf-global--active-color--400",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    },
    "c_nav_m_horizontal__link_active_Color": {
      "name": "--pf-c-nav--m-horizontal__link--active--Color",
      "value": "#2b9af3",
      "values": [
        "--pf-global--active-color--400",
        "$pf-global--active-color--400",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    },
    "c_nav_m_horizontal__link_m_current_Color": {
      "name": "--pf-c-nav--m-horizontal__link--m-current--Color",
      "value": "#2b9af3",
      "values": [
        "--pf-global--active-color--400",
        "$pf-global--active-color--400",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    },
    "c_nav_m_horizontal__link_BackgroundColor": {
      "name": "--pf-c-nav--m-horizontal__link--BackgroundColor",
      "value": "transparent"
    },
    "c_nav_m_horizontal__link_hover_BackgroundColor": {
      "name": "--pf-c-nav--m-horizontal__link--hover--BackgroundColor",
      "value": "transparent"
    },
    "c_nav_m_horizontal__link_focus_BackgroundColor": {
      "name": "--pf-c-nav--m-horizontal__link--focus--BackgroundColor",
      "value": "transparent"
    },
    "c_nav_m_horizontal__link_active_BackgroundColor": {
      "name": "--pf-c-nav--m-horizontal__link--active--BackgroundColor",
      "value": "transparent"
    },
    "c_nav_m_horizontal__link_m_current_BackgroundColor": {
      "name": "--pf-c-nav--m-horizontal__link--m-current--BackgroundColor",
      "value": "transparent"
    },
    "c_nav_m_horizontal__link_before_BorderColor": {
      "name": "--pf-c-nav--m-horizontal__link--before--BorderColor",
      "value": "#2b9af3",
      "values": [
        "--pf-global--active-color--400",
        "$pf-global--active-color--400",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    },
    "c_nav_m_horizontal__link_before_BorderWidth": {
      "name": "--pf-c-nav--m-horizontal__link--before--BorderWidth",
      "value": "0"
    },
    "c_nav_m_horizontal__link_hover_before_BorderWidth": {
      "name": "--pf-c-nav--m-horizontal__link--hover--before--BorderWidth",
      "value": "3px",
      "values": [
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    },
    "c_nav_m_horizontal__link_focus_before_BorderWidth": {
      "name": "--pf-c-nav--m-horizontal__link--focus--before--BorderWidth",
      "value": "3px",
      "values": [
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    },
    "c_nav_m_horizontal__link_active_before_BorderWidth": {
      "name": "--pf-c-nav--m-horizontal__link--active--before--BorderWidth",
      "value": "3px",
      "values": [
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    },
    "c_nav_m_horizontal__link_m_current_before_BorderWidth": {
      "name": "--pf-c-nav--m-horizontal__link--m-current--before--BorderWidth",
      "value": "3px",
      "values": [
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    },
    "c_nav_m_tertiary__link_PaddingTop": {
      "name": "--pf-c-nav--m-tertiary__link--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_nav_m_tertiary__link_PaddingRight": {
      "name": "--pf-c-nav--m-tertiary__link--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_nav_m_tertiary__link_PaddingBottom": {
      "name": "--pf-c-nav--m-tertiary__link--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_nav_m_tertiary__link_PaddingLeft": {
      "name": "--pf-c-nav--m-tertiary__link--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_nav_m_tertiary__link_Right": {
      "name": "--pf-c-nav--m-tertiary__link--Right",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_nav_m_tertiary__link_Left": {
      "name": "--pf-c-nav--m-tertiary__link--Left",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_nav_m_tertiary__link_Color": {
      "name": "--pf-c-nav--m-tertiary__link--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_nav_m_tertiary__link_hover_Color": {
      "name": "--pf-c-nav--m-tertiary__link--hover--Color",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_nav_m_tertiary__link_focus_Color": {
      "name": "--pf-c-nav--m-tertiary__link--focus--Color",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_nav_m_tertiary__link_active_Color": {
      "name": "--pf-c-nav--m-tertiary__link--active--Color",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_nav_m_tertiary__link_m_current_Color": {
      "name": "--pf-c-nav--m-tertiary__link--m-current--Color",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_nav_m_tertiary__link_BackgroundColor": {
      "name": "--pf-c-nav--m-tertiary__link--BackgroundColor",
      "value": "transparent"
    },
    "c_nav_m_tertiary__link_hover_BackgroundColor": {
      "name": "--pf-c-nav--m-tertiary__link--hover--BackgroundColor",
      "value": "transparent"
    },
    "c_nav_m_tertiary__link_focus_BackgroundColor": {
      "name": "--pf-c-nav--m-tertiary__link--focus--BackgroundColor",
      "value": "transparent"
    },
    "c_nav_m_tertiary__link_active_BackgroundColor": {
      "name": "--pf-c-nav--m-tertiary__link--active--BackgroundColor",
      "value": "transparent"
    },
    "c_nav_m_tertiary__link_m_current_BackgroundColor": {
      "name": "--pf-c-nav--m-tertiary__link--m-current--BackgroundColor",
      "value": "transparent"
    },
    "c_nav_m_tertiary__link_before_BorderColor": {
      "name": "--pf-c-nav--m-tertiary__link--before--BorderColor",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_nav_m_tertiary__link_before_BorderWidth": {
      "name": "--pf-c-nav--m-tertiary__link--before--BorderWidth",
      "value": "0"
    },
    "c_nav_m_tertiary__link_hover_before_BorderWidth": {
      "name": "--pf-c-nav--m-tertiary__link--hover--before--BorderWidth",
      "value": "3px",
      "values": [
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    },
    "c_nav_m_tertiary__link_focus_before_BorderWidth": {
      "name": "--pf-c-nav--m-tertiary__link--focus--before--BorderWidth",
      "value": "3px",
      "values": [
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    },
    "c_nav_m_tertiary__link_active_before_BorderWidth": {
      "name": "--pf-c-nav--m-tertiary__link--active--before--BorderWidth",
      "value": "3px",
      "values": [
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    },
    "c_nav_m_tertiary__link_m_current_before_BorderWidth": {
      "name": "--pf-c-nav--m-tertiary__link--m-current--before--BorderWidth",
      "value": "3px",
      "values": [
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    },
    "c_nav_m_tertiary__scroll_button_Color": {
      "name": "--pf-c-nav--m-tertiary__scroll-button--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_nav_m_tertiary__scroll_button_hover_Color": {
      "name": "--pf-c-nav--m-tertiary__scroll-button--hover--Color",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_nav_m_tertiary__scroll_button_focus_Color": {
      "name": "--pf-c-nav--m-tertiary__scroll-button--focus--Color",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_nav_m_tertiary__scroll_button_active_Color": {
      "name": "--pf-c-nav--m-tertiary__scroll-button--active--Color",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_nav_m_tertiary__scroll_button_disabled_Color": {
      "name": "--pf-c-nav--m-tertiary__scroll-button--disabled--Color",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--disabled-color--200",
        "$pf-global--disabled-color--200",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_nav_m_tertiary__scroll_button_before_BorderColor": {
      "name": "--pf-c-nav--m-tertiary__scroll-button--before--BorderColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_nav_m_tertiary__scroll_button_disabled_before_BorderColor": {
      "name": "--pf-c-nav--m-tertiary__scroll-button--disabled--before--BorderColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--disabled-color--300",
        "$pf-global--disabled-color--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_nav__subnav_PaddingBottom": {
      "name": "--pf-c-nav__subnav--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_nav__subnav_xl_PaddingLeft": {
      "name": "--pf-c-nav__subnav--xl--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-c-nav__link--PaddingLeft",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_nav__subnav__link_MarginTop": {
      "name": "--pf-c-nav__subnav__link--MarginTop",
      "value": "0"
    },
    "c_nav__subnav__link_PaddingTop": {
      "name": "--pf-c-nav__subnav__link--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_nav__subnav__link_PaddingRight": {
      "name": "--pf-c-nav__subnav__link--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_nav__subnav__link_PaddingBottom": {
      "name": "--pf-c-nav__subnav__link--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_nav__subnav__link_PaddingLeft": {
      "name": "--pf-c-nav__subnav__link--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_nav__subnav__link_FontSize": {
      "name": "--pf-c-nav__subnav__link--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_nav__subnav__link_hover_after_BorderColor": {
      "name": "--pf-c-nav__subnav__link--hover--after--BorderColor",
      "value": "#8a8d90",
      "values": [
        "--pf-global--BorderColor--200",
        "$pf-global--BorderColor--200",
        "$pf-color-black-500",
        "#8a8d90"
      ]
    },
    "c_nav__subnav__link_focus_after_BorderColor": {
      "name": "--pf-c-nav__subnav__link--focus--after--BorderColor",
      "value": "#8a8d90",
      "values": [
        "--pf-global--BorderColor--200",
        "$pf-global--BorderColor--200",
        "$pf-color-black-500",
        "#8a8d90"
      ]
    },
    "c_nav__subnav__link_active_after_BorderColor": {
      "name": "--pf-c-nav__subnav__link--active--after--BorderColor",
      "value": "#8a8d90",
      "values": [
        "--pf-global--BorderColor--200",
        "$pf-global--BorderColor--200",
        "$pf-color-black-500",
        "#8a8d90"
      ]
    },
    "c_nav__subnav__link_m_current_after_BorderColor": {
      "name": "--pf-c-nav__subnav__link--m-current--after--BorderColor",
      "value": "#2b9af3",
      "values": [
        "--pf-global--active-color--400",
        "$pf-global--active-color--400",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    },
    "c_nav__subnav__link_hover_after_BorderWidth": {
      "name": "--pf-c-nav__subnav__link--hover--after--BorderWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_nav__subnav__link_focus_after_BorderWidth": {
      "name": "--pf-c-nav__subnav__link--focus--after--BorderWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_nav__subnav__link_active_after_BorderWidth": {
      "name": "--pf-c-nav__subnav__link--active--after--BorderWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_nav__subnav__link_m_current_after_BorderWidth": {
      "name": "--pf-c-nav__subnav__link--m-current--after--BorderWidth",
      "value": "4px",
      "values": [
        "--pf-global--BorderWidth--xl",
        "$pf-global--BorderWidth--xl",
        "4px"
      ]
    },
    "c_nav__subnav_MaxHeight": {
      "name": "--pf-c-nav__subnav--MaxHeight",
      "value": "0"
    },
    "c_nav__item_m_expanded__subnav_MaxHeight": {
      "name": "--pf-c-nav__item--m-expanded__subnav--MaxHeight",
      "value": "100%"
    },
    "c_nav__subnav_c_divider_PaddingRight": {
      "name": "--pf-c-nav__subnav--c-divider--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_nav__subnav_c_divider_PaddingLeft": {
      "name": "--pf-c-nav__subnav--c-divider--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_nav__section_MarginTop": {
      "name": "--pf-c-nav__section--MarginTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_nav__section__item_MarginTop": {
      "name": "--pf-c-nav__section__item--MarginTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_nav__section__link_PaddingTop": {
      "name": "--pf-c-nav__section__link--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_nav__section__link_PaddingRight": {
      "name": "--pf-c-nav__section__link--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_nav__section__link_PaddingBottom": {
      "name": "--pf-c-nav__section__link--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_nav__section__link_PaddingLeft": {
      "name": "--pf-c-nav__section__link--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_nav__section__link_xl_PaddingRight": {
      "name": "--pf-c-nav__section__link--xl--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_nav__section__link_xl_PaddingLeft": {
      "name": "--pf-c-nav__section__link--xl--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_nav__section__link_FontSize": {
      "name": "--pf-c-nav__section__link--FontSize",
      "value": "1rem",
      "values": [
        "--pf-global--FontSize--md",
        "$pf-global--FontSize--md",
        "pf-font-prem(16px)",
        "1rem"
      ]
    },
    "c_nav__section__link_before_BorderBottomWidth": {
      "name": "--pf-c-nav__section__link--before--BorderBottomWidth",
      "value": "0"
    },
    "c_nav__section__link_hover_after_BorderColor": {
      "name": "--pf-c-nav__section__link--hover--after--BorderColor",
      "value": "transparent"
    },
    "c_nav__section__link_focus_after_BorderColor": {
      "name": "--pf-c-nav__section__link--focus--after--BorderColor",
      "value": "transparent"
    },
    "c_nav__section__link_active_after_BorderColor": {
      "name": "--pf-c-nav__section__link--active--after--BorderColor",
      "value": "transparent"
    },
    "c_nav__section__link_m_current_after_BorderColor": {
      "name": "--pf-c-nav__section__link--m-current--after--BorderColor",
      "value": "#2b9af3",
      "values": [
        "--pf-global--active-color--400",
        "$pf-global--active-color--400",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    },
    "c_nav__section__link_hover_after_BorderWidth": {
      "name": "--pf-c-nav__section__link--hover--after--BorderWidth",
      "value": "0"
    },
    "c_nav__section__link_focus_after_BorderWidth": {
      "name": "--pf-c-nav__section__link--focus--after--BorderWidth",
      "value": "0"
    },
    "c_nav__section__link_active_after_BorderWidth": {
      "name": "--pf-c-nav__section__link--active--after--BorderWidth",
      "value": "0"
    },
    "c_nav__section__link_m_current_after_BorderWidth": {
      "name": "--pf-c-nav__section__link--m-current--after--BorderWidth",
      "value": "4px",
      "values": [
        "--pf-global--BorderWidth--xl",
        "$pf-global--BorderWidth--xl",
        "4px"
      ]
    },
    "c_nav__section_section_MarginTop": {
      "name": "--pf-c-nav__section--section--MarginTop",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_nav__section_title_PaddingTop": {
      "name": "--pf-c-nav__section-title--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_nav__section_title_PaddingRight": {
      "name": "--pf-c-nav__section-title--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_nav__section_title_PaddingBottom": {
      "name": "--pf-c-nav__section-title--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_nav__section_title_PaddingLeft": {
      "name": "--pf-c-nav__section-title--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_nav__section_title_xl_PaddingRight": {
      "name": "--pf-c-nav__section-title--xl--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_nav__section_title_xl_PaddingLeft": {
      "name": "--pf-c-nav__section-title--xl--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_nav__section_title_FontSize": {
      "name": "--pf-c-nav__section-title--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_nav__section_title_Color": {
      "name": "--pf-c-nav__section-title--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_nav__section_title_BorderBottomColor": {
      "name": "--pf-c-nav__section-title--BorderBottomColor",
      "value": "#3c3f42",
      "values": [
        "--pf-global--BackgroundColor--dark-200",
        "$pf-global--BackgroundColor--dark-200",
        "$pf-color-black-800",
        "#3c3f42"
      ]
    },
    "c_nav__section_title_BorderBottomWidth": {
      "name": "--pf-c-nav__section-title--BorderBottomWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_nav__scroll_button_Color": {
      "name": "--pf-c-nav__scroll-button--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_nav__scroll_button_hover_Color": {
      "name": "--pf-c-nav__scroll-button--hover--Color",
      "value": "#2b9af3",
      "values": [
        "--pf-global--active-color--400",
        "$pf-global--active-color--400",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    },
    "c_nav__scroll_button_focus_Color": {
      "name": "--pf-c-nav__scroll-button--focus--Color",
      "value": "#2b9af3",
      "values": [
        "--pf-global--active-color--400",
        "$pf-global--active-color--400",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    },
    "c_nav__scroll_button_active_Color": {
      "name": "--pf-c-nav__scroll-button--active--Color",
      "value": "#2b9af3",
      "values": [
        "--pf-global--active-color--400",
        "$pf-global--active-color--400",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    },
    "c_nav__scroll_button_disabled_Color": {
      "name": "--pf-c-nav__scroll-button--disabled--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--disabled-color--100",
        "$pf-global--disabled-color--100",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_nav__scroll_button_BackgroundColor": {
      "name": "--pf-c-nav__scroll-button--BackgroundColor",
      "value": "transparent"
    },
    "c_nav__scroll_button_Width": {
      "name": "--pf-c-nav__scroll-button--Width",
      "value": "44px",
      "values": [
        "--pf-global--target-size--MinWidth",
        "$pf-global--target-size--MinWidth",
        "44px"
      ]
    },
    "c_nav__scroll_button_OutlineOffset": {
      "name": "--pf-c-nav__scroll-button--OutlineOffset",
      "value": "calc(-1 * 0.25rem)",
      "values": [
        "calc(-1 * --pf-global--spacer--xs)",
        "calc(-1 * $pf-global--spacer--xs)",
        "calc(-1 * pf-size-prem(4px))",
        "calc(-1 * 0.25rem)"
      ]
    },
    "c_nav__scroll_button_Transition": {
      "name": "--pf-c-nav__scroll-button--Transition",
      "value": "margin .125s, transform .125s, opacity .125s"
    },
    "c_nav__scroll_button_before_BorderColor": {
      "name": "--pf-c-nav__scroll-button--before--BorderColor",
      "value": "#3c3f42",
      "values": [
        "--pf-global--BackgroundColor--dark-200",
        "$pf-global--BackgroundColor--dark-200",
        "$pf-color-black-800",
        "#3c3f42"
      ]
    },
    "c_nav__scroll_button_before_BorderWidth": {
      "name": "--pf-c-nav__scroll-button--before--BorderWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_nav__scroll_button_before_BorderRightWidth": {
      "name": "--pf-c-nav__scroll-button--before--BorderRightWidth",
      "value": "0"
    },
    "c_nav__scroll_button_before_BorderLeftWidth": {
      "name": "--pf-c-nav__scroll-button--before--BorderLeftWidth",
      "value": "0"
    },
    "c_nav__scroll_button_disabled_before_BorderColor": {
      "name": "--pf-c-nav__scroll-button--disabled--before--BorderColor",
      "value": "transparent"
    },
    "c_nav__toggle_PaddingRight": {
      "name": "--pf-c-nav__toggle--PaddingRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_nav__toggle_PaddingLeft": {
      "name": "--pf-c-nav__toggle--PaddingLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_nav__toggle_FontSize": {
      "name": "--pf-c-nav__toggle--FontSize",
      "value": "1.125rem",
      "values": [
        "--pf-global--icon--FontSize--md",
        "$pf-global--icon--FontSize--md",
        "pf-font-prem(18px)",
        "1.125rem"
      ]
    },
    "c_nav__toggle_icon_Transition": {
      "name": "--pf-c-nav__toggle-icon--Transition",
      "value": "250ms",
      "values": [
        "--pf-global--TransitionDuration",
        "$pf-global--TransitionDuration",
        "250ms"
      ]
    },
    "c_nav_c_divider_MarginTop": {
      "name": "--pf-c-nav--c-divider--MarginTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_nav_c_divider_MarginBottom": {
      "name": "--pf-c-nav--c-divider--MarginBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_nav_c_divider_PaddingRight": {
      "name": "--pf-c-nav--c-divider--PaddingRight",
      "value": "0"
    },
    "c_nav_c_divider_PaddingLeft": {
      "name": "--pf-c-nav--c-divider--PaddingLeft",
      "value": "0"
    },
    "c_nav_c_divider_BackgroundColor": {
      "name": "--pf-c-nav--c-divider--BackgroundColor",
      "value": "#3c3f42",
      "values": [
        "--pf-global--BackgroundColor--dark-200",
        "$pf-global--BackgroundColor--dark-200",
        "$pf-color-black-800",
        "#3c3f42"
      ]
    }
  },
  ".pf-c-nav.pf-m-light": {
    "c_nav__item_before_BorderColor": {
      "name": "--pf-c-nav__item--before--BorderColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-c-nav--m-light__item--before--BorderColor",
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_nav__item_m_current_not_m_expanded__link_BackgroundColor": {
      "name": "--pf-c-nav__item--m-current--not--m-expanded__link--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-c-nav--m-light__item--m-current--not--m-expanded__link--BackgroundColor",
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_nav__link_Color": {
      "name": "--pf-c-nav__link--Color",
      "value": "#151515",
      "values": [
        "--pf-c-nav--m-light__link--Color",
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_nav__link_hover_Color": {
      "name": "--pf-c-nav__link--hover--Color",
      "value": "#151515",
      "values": [
        "--pf-c-nav--m-light__link--hover--Color",
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_nav__link_focus_Color": {
      "name": "--pf-c-nav__link--focus--Color",
      "value": "#151515",
      "values": [
        "--pf-c-nav--m-light__link--focus--Color",
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_nav__link_active_Color": {
      "name": "--pf-c-nav__link--active--Color",
      "value": "#151515",
      "values": [
        "--pf-c-nav--m-light__link--active--Color",
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_nav__link_m_current_Color": {
      "name": "--pf-c-nav__link--m-current--Color",
      "value": "#151515",
      "values": [
        "--pf-c-nav--m-light__link--m-current--Color",
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_nav__link_hover_BackgroundColor": {
      "name": "--pf-c-nav__link--hover--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-c-nav--m-light__link--hover--BackgroundColor",
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_nav__link_focus_BackgroundColor": {
      "name": "--pf-c-nav__link--focus--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-c-nav--m-light__link--focus--BackgroundColor",
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_nav__link_active_BackgroundColor": {
      "name": "--pf-c-nav__link--active--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-c-nav--m-light__link--active--BackgroundColor",
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_nav__link_m_current_BackgroundColor": {
      "name": "--pf-c-nav__link--m-current--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-c-nav--m-light__link--m-current--BackgroundColor",
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_nav__link_before_BorderColor": {
      "name": "--pf-c-nav__link--before--BorderColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-c-nav--m-light__link--before--BorderColor",
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_nav__link_after_BorderColor": {
      "name": "--pf-c-nav__link--after--BorderColor",
      "value": "#06c",
      "values": [
        "--pf-c-nav--m-light__link--after--BorderColor",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_nav__link_m_current_after_BorderColor": {
      "name": "--pf-c-nav__link--m-current--after--BorderColor",
      "value": "#06c",
      "values": [
        "--pf-c-nav--m-light__link--m-current--after--BorderColor",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_nav__subnav__link_hover_after_BorderColor": {
      "name": "--pf-c-nav__subnav__link--hover--after--BorderColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-c-nav--m-light__subnav__link--hover--after--BorderColor",
        "--pf-global--BorderColor--dark-100",
        "$pf-global--BorderColor--dark-100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_nav__subnav__link_focus_after_BorderColor": {
      "name": "--pf-c-nav__subnav__link--focus--after--BorderColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-c-nav--m-light__subnav__link--focus--after--BorderColor",
        "--pf-global--BorderColor--dark-100",
        "$pf-global--BorderColor--dark-100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_nav__subnav__link_active_after_BorderColor": {
      "name": "--pf-c-nav__subnav__link--active--after--BorderColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-c-nav--m-light__subnav__link--active--after--BorderColor",
        "--pf-global--BorderColor--dark-100",
        "$pf-global--BorderColor--dark-100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_nav__subnav__link_m_current_after_BorderColor": {
      "name": "--pf-c-nav__subnav__link--m-current--after--BorderColor",
      "value": "#06c",
      "values": [
        "--pf-c-nav--m-light__subnav__link--m-current--after--BorderColor",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_nav__section_title_Color": {
      "name": "--pf-c-nav__section-title--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-c-nav--m-light__section-title--Color",
        "--pf-global--Color--dark-200",
        "$pf-global--Color--dark-200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_nav__section_title_BorderBottomColor": {
      "name": "--pf-c-nav__section-title--BorderBottomColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-c-nav--m-light__section-title--BorderBottomColor",
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    }
  },
  ".pf-c-nav.pf-m-light .pf-c-divider": {
    "c_divider_after_BackgroundColor": {
      "name": "--pf-c-divider--after--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-c-nav--m-light--c-divider--BackgroundColor",
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    }
  },
  ".pf-c-nav.pf-m-horizontal": {
    "c_nav__link_PaddingTop": {
      "name": "--pf-c-nav__link--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-c-nav--m-horizontal__link--PaddingTop",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_nav__link_PaddingRight": {
      "name": "--pf-c-nav__link--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-c-nav--m-horizontal__link--PaddingRight",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_nav__link_PaddingBottom": {
      "name": "--pf-c-nav__link--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-c-nav--m-horizontal__link--PaddingBottom",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_nav__link_PaddingLeft": {
      "name": "--pf-c-nav__link--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-c-nav--m-horizontal__link--PaddingLeft",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_nav__link_Right": {
      "name": "--pf-c-nav__link--Right",
      "value": "1rem",
      "values": [
        "--pf-c-nav--m-horizontal__link--Right",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_nav__link_Left": {
      "name": "--pf-c-nav__link--Left",
      "value": "1rem",
      "values": [
        "--pf-c-nav--m-horizontal__link--Left",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_nav__link_Color": {
      "name": "--pf-c-nav__link--Color",
      "value": "#d2d2d2",
      "values": [
        "--pf-c-nav--m-horizontal__link--Color",
        "--pf-global--Color--light-300",
        "$pf-global--Color--light-300",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_nav__link_hover_Color": {
      "name": "--pf-c-nav__link--hover--Color",
      "value": "#2b9af3",
      "values": [
        "--pf-c-nav--m-horizontal__link--hover--Color",
        "--pf-global--active-color--400",
        "$pf-global--active-color--400",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    },
    "c_nav__link_active_Color": {
      "name": "--pf-c-nav__link--active--Color",
      "value": "#2b9af3",
      "values": [
        "--pf-c-nav--m-horizontal__link--active--Color",
        "--pf-global--active-color--400",
        "$pf-global--active-color--400",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    },
    "c_nav__link_focus_Color": {
      "name": "--pf-c-nav__link--focus--Color",
      "value": "#2b9af3",
      "values": [
        "--pf-c-nav--m-horizontal__link--focus--Color",
        "--pf-global--active-color--400",
        "$pf-global--active-color--400",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    },
    "c_nav__link_m_current_Color": {
      "name": "--pf-c-nav__link--m-current--Color",
      "value": "#2b9af3",
      "values": [
        "--pf-c-nav--m-horizontal__link--m-current--Color",
        "--pf-global--active-color--400",
        "$pf-global--active-color--400",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    },
    "c_nav__link_BackgroundColor": {
      "name": "--pf-c-nav__link--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-nav--m-horizontal__link--BackgroundColor",
        "transparent"
      ]
    },
    "c_nav__link_hover_BackgroundColor": {
      "name": "--pf-c-nav__link--hover--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-nav--m-horizontal__link--hover--BackgroundColor",
        "transparent"
      ]
    },
    "c_nav__link_focus_BackgroundColor": {
      "name": "--pf-c-nav__link--focus--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-nav--m-horizontal__link--focus--BackgroundColor",
        "transparent"
      ]
    },
    "c_nav__link_active_BackgroundColor": {
      "name": "--pf-c-nav__link--active--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-nav--m-horizontal__link--active--BackgroundColor",
        "transparent"
      ]
    },
    "c_nav__link_m_current_BackgroundColor": {
      "name": "--pf-c-nav__link--m-current--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-nav--m-horizontal__link--m-current--BackgroundColor",
        "transparent"
      ]
    },
    "c_nav__link_before_BorderColor": {
      "name": "--pf-c-nav__link--before--BorderColor",
      "value": "#2b9af3",
      "values": [
        "--pf-c-nav--m-horizontal__link--before--BorderColor",
        "--pf-global--active-color--400",
        "$pf-global--active-color--400",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    },
    "c_nav__link_before_BorderBottomWidth": {
      "name": "--pf-c-nav__link--before--BorderBottomWidth",
      "value": "0",
      "values": [
        "--pf-c-nav--m-horizontal__link--before--BorderWidth",
        "0"
      ]
    },
    "c_nav__link_hover_before_BorderBottomWidth": {
      "name": "--pf-c-nav__link--hover--before--BorderBottomWidth",
      "value": "3px",
      "values": [
        "--pf-c-nav--m-horizontal__link--hover--before--BorderWidth",
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    },
    "c_nav__link_focus_before_BorderBottomWidth": {
      "name": "--pf-c-nav__link--focus--before--BorderBottomWidth",
      "value": "3px",
      "values": [
        "--pf-c-nav--m-horizontal__link--focus--before--BorderWidth",
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    },
    "c_nav__link_active_before_BorderBottomWidth": {
      "name": "--pf-c-nav__link--active--before--BorderBottomWidth",
      "value": "3px",
      "values": [
        "--pf-c-nav--m-horizontal__link--active--before--BorderWidth",
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    },
    "c_nav__link_m_current_before_BorderBottomWidth": {
      "name": "--pf-c-nav__link--m-current--before--BorderBottomWidth",
      "value": "3px",
      "values": [
        "--pf-c-nav--m-horizontal__link--m-current--before--BorderWidth",
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    }
  },
  ".pf-c-nav.pf-m-tertiary": {
    "c_nav__link_PaddingTop": {
      "name": "--pf-c-nav__link--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-c-nav--m-tertiary__link--PaddingTop",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_nav__link_PaddingRight": {
      "name": "--pf-c-nav__link--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-c-nav--m-tertiary__link--PaddingRight",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_nav__link_PaddingBottom": {
      "name": "--pf-c-nav__link--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-c-nav--m-tertiary__link--PaddingBottom",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_nav__link_PaddingLeft": {
      "name": "--pf-c-nav__link--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-c-nav--m-tertiary__link--PaddingLeft",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_nav__link_Right": {
      "name": "--pf-c-nav__link--Right",
      "value": "1rem",
      "values": [
        "--pf-c-nav--m-tertiary__link--Right",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_nav__link_Left": {
      "name": "--pf-c-nav__link--Left",
      "value": "1rem",
      "values": [
        "--pf-c-nav--m-tertiary__link--Left",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_nav__link_Color": {
      "name": "--pf-c-nav__link--Color",
      "value": "#151515",
      "values": [
        "--pf-c-nav--m-tertiary__link--Color",
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_nav__link_hover_Color": {
      "name": "--pf-c-nav__link--hover--Color",
      "value": "#06c",
      "values": [
        "--pf-c-nav--m-tertiary__link--hover--Color",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_nav__link_active_Color": {
      "name": "--pf-c-nav__link--active--Color",
      "value": "#06c",
      "values": [
        "--pf-c-nav--m-tertiary__link--active--Color",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_nav__link_focus_Color": {
      "name": "--pf-c-nav__link--focus--Color",
      "value": "#06c",
      "values": [
        "--pf-c-nav--m-tertiary__link--focus--Color",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_nav__link_m_current_Color": {
      "name": "--pf-c-nav__link--m-current--Color",
      "value": "#06c",
      "values": [
        "--pf-c-nav--m-tertiary__link--m-current--Color",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_nav__link_BackgroundColor": {
      "name": "--pf-c-nav__link--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-nav--m-tertiary__link--BackgroundColor",
        "transparent"
      ]
    },
    "c_nav__link_hover_BackgroundColor": {
      "name": "--pf-c-nav__link--hover--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-nav--m-tertiary__link--hover--BackgroundColor",
        "transparent"
      ]
    },
    "c_nav__link_focus_BackgroundColor": {
      "name": "--pf-c-nav__link--focus--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-nav--m-tertiary__link--focus--BackgroundColor",
        "transparent"
      ]
    },
    "c_nav__link_active_BackgroundColor": {
      "name": "--pf-c-nav__link--active--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-nav--m-tertiary__link--active--BackgroundColor",
        "transparent"
      ]
    },
    "c_nav__link_m_current_BackgroundColor": {
      "name": "--pf-c-nav__link--m-current--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-nav--m-tertiary__link--m-current--BackgroundColor",
        "transparent"
      ]
    },
    "c_nav__link_before_BorderColor": {
      "name": "--pf-c-nav__link--before--BorderColor",
      "value": "#06c",
      "values": [
        "--pf-c-nav--m-tertiary__link--before--BorderColor",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_nav__link_before_BorderBottomWidth": {
      "name": "--pf-c-nav__link--before--BorderBottomWidth",
      "value": "0",
      "values": [
        "--pf-c-nav--m-tertiary__link--before--BorderWidth",
        "0"
      ]
    },
    "c_nav__link_hover_before_BorderBottomWidth": {
      "name": "--pf-c-nav__link--hover--before--BorderBottomWidth",
      "value": "3px",
      "values": [
        "--pf-c-nav--m-tertiary__link--hover--before--BorderWidth",
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    },
    "c_nav__link_focus_before_BorderBottomWidth": {
      "name": "--pf-c-nav__link--focus--before--BorderBottomWidth",
      "value": "3px",
      "values": [
        "--pf-c-nav--m-tertiary__link--focus--before--BorderWidth",
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    },
    "c_nav__link_active_before_BorderBottomWidth": {
      "name": "--pf-c-nav__link--active--before--BorderBottomWidth",
      "value": "3px",
      "values": [
        "--pf-c-nav--m-tertiary__link--active--before--BorderWidth",
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    },
    "c_nav__link_m_current_before_BorderBottomWidth": {
      "name": "--pf-c-nav__link--m-current--before--BorderBottomWidth",
      "value": "3px",
      "values": [
        "--pf-c-nav--m-tertiary__link--m-current--before--BorderWidth",
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    },
    "c_nav__scroll_button_Color": {
      "name": "--pf-c-nav__scroll-button--Color",
      "value": "#151515",
      "values": [
        "--pf-c-nav--m-tertiary__scroll-button--Color",
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_nav__scroll_button_hover_Color": {
      "name": "--pf-c-nav__scroll-button--hover--Color",
      "value": "#06c",
      "values": [
        "--pf-c-nav--m-tertiary__scroll-button--hover--Color",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_nav__scroll_button_focus_Color": {
      "name": "--pf-c-nav__scroll-button--focus--Color",
      "value": "#06c",
      "values": [
        "--pf-c-nav--m-tertiary__scroll-button--focus--Color",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_nav__scroll_button_active_Color": {
      "name": "--pf-c-nav__scroll-button--active--Color",
      "value": "#06c",
      "values": [
        "--pf-c-nav--m-tertiary__scroll-button--active--Color",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_nav__scroll_button_disabled_Color": {
      "name": "--pf-c-nav__scroll-button--disabled--Color",
      "value": "#d2d2d2",
      "values": [
        "--pf-c-nav--m-tertiary__scroll-button--disabled--Color",
        "--pf-global--disabled-color--200",
        "$pf-global--disabled-color--200",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_nav__scroll_button_before_BorderColor": {
      "name": "--pf-c-nav__scroll-button--before--BorderColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-c-nav--m-tertiary__scroll-button--before--BorderColor",
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_nav__scroll_button_disabled_before_BorderColor": {
      "name": "--pf-c-nav__scroll-button--disabled--before--BorderColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-c-nav--m-tertiary__scroll-button--disabled--before--BorderColor",
        "--pf-global--disabled-color--300",
        "$pf-global--disabled-color--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    }
  },
  ".pf-c-nav .pf-c-divider": {
    "c_divider_after_BackgroundColor": {
      "name": "--pf-c-divider--after--BackgroundColor",
      "value": "#3c3f42",
      "values": [
        "--pf-c-nav--c-divider--BackgroundColor",
        "--pf-global--BackgroundColor--dark-200",
        "$pf-global--BackgroundColor--dark-200",
        "$pf-color-black-800",
        "#3c3f42"
      ]
    }
  },
  ".pf-c-nav__item.pf-m-expandable": {
    "c_nav__link_before_BorderBottomWidth": {
      "name": "--pf-c-nav__link--before--BorderBottomWidth",
      "value": "0"
    }
  },
  ".pf-c-nav__subnav": {
    "c_nav__link_PaddingTop": {
      "name": "--pf-c-nav__link--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-c-nav__subnav__link--PaddingTop",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_nav__link_PaddingRight": {
      "name": "--pf-c-nav__link--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-c-nav__subnav__link--PaddingRight",
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_nav__link_PaddingBottom": {
      "name": "--pf-c-nav__link--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-c-nav__subnav__link--PaddingBottom",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_nav__link_PaddingLeft": {
      "name": "--pf-c-nav__link--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-c-nav__subnav__link--PaddingLeft",
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_nav__link_FontSize": {
      "name": "--pf-c-nav__link--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-c-nav__subnav__link--FontSize",
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_nav__link_hover_after_BorderColor": {
      "name": "--pf-c-nav__link--hover--after--BorderColor",
      "value": "#8a8d90",
      "values": [
        "--pf-c-nav__subnav__link--hover--after--BorderColor",
        "--pf-global--BorderColor--200",
        "$pf-global--BorderColor--200",
        "$pf-color-black-500",
        "#8a8d90"
      ]
    },
    "c_nav__link_focus_after_BorderColor": {
      "name": "--pf-c-nav__link--focus--after--BorderColor",
      "value": "#8a8d90",
      "values": [
        "--pf-c-nav__subnav__link--focus--after--BorderColor",
        "--pf-global--BorderColor--200",
        "$pf-global--BorderColor--200",
        "$pf-color-black-500",
        "#8a8d90"
      ]
    },
    "c_nav__link_active_after_BorderColor": {
      "name": "--pf-c-nav__link--active--after--BorderColor",
      "value": "#8a8d90",
      "values": [
        "--pf-c-nav__subnav__link--active--after--BorderColor",
        "--pf-global--BorderColor--200",
        "$pf-global--BorderColor--200",
        "$pf-color-black-500",
        "#8a8d90"
      ]
    },
    "c_nav__link_m_current_after_BorderColor": {
      "name": "--pf-c-nav__link--m-current--after--BorderColor",
      "value": "#2b9af3",
      "values": [
        "--pf-c-nav__subnav__link--m-current--after--BorderColor",
        "--pf-global--active-color--400",
        "$pf-global--active-color--400",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    },
    "c_nav__link_hover_after_BorderLeftWidth": {
      "name": "--pf-c-nav__link--hover--after--BorderLeftWidth",
      "value": "1px",
      "values": [
        "--pf-c-nav__subnav__link--hover--after--BorderWidth",
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_nav__link_focus_after_BorderLeftWidth": {
      "name": "--pf-c-nav__link--focus--after--BorderLeftWidth",
      "value": "1px",
      "values": [
        "--pf-c-nav__subnav__link--focus--after--BorderWidth",
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_nav__link_active_after_BorderLeftWidth": {
      "name": "--pf-c-nav__link--active--after--BorderLeftWidth",
      "value": "1px",
      "values": [
        "--pf-c-nav__subnav__link--active--after--BorderWidth",
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_nav__link_m_current_after_BorderLeftWidth": {
      "name": "--pf-c-nav__link--m-current--after--BorderLeftWidth",
      "value": "4px",
      "values": [
        "--pf-c-nav__subnav__link--m-current--after--BorderWidth",
        "--pf-global--BorderWidth--xl",
        "$pf-global--BorderWidth--xl",
        "4px"
      ]
    },
    "c_nav_c_divider_PaddingRight": {
      "name": "--pf-c-nav--c-divider--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-c-nav__subnav--c-divider--PaddingRight",
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_nav_c_divider_PaddingLeft": {
      "name": "--pf-c-nav--c-divider--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-c-nav__subnav--c-divider--PaddingLeft",
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    }
  },
  ".pf-c-nav__item.pf-m-expanded .pf-c-nav__subnav": {
    "c_nav__subnav_MaxHeight": {
      "name": "--pf-c-nav__subnav--MaxHeight",
      "value": "100%",
      "values": [
        "--pf-c-nav__item--m-expanded__subnav--MaxHeight",
        "100%"
      ]
    }
  },
  ".pf-c-nav__section": {
    "c_nav__item_MarginTop": {
      "name": "--pf-c-nav__item--MarginTop",
      "value": "0.5rem",
      "values": [
        "--pf-c-nav__section__item--MarginTop",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_nav__link_PaddingTop": {
      "name": "--pf-c-nav__link--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-c-nav__section__link--PaddingTop",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_nav__link_PaddingRight": {
      "name": "--pf-c-nav__link--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-c-nav__section__link--PaddingRight",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_nav__link_PaddingBottom": {
      "name": "--pf-c-nav__link--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-c-nav__section__link--PaddingBottom",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_nav__link_PaddingLeft": {
      "name": "--pf-c-nav__link--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-c-nav__section__link--PaddingLeft",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_nav__link_FontSize": {
      "name": "--pf-c-nav__link--FontSize",
      "value": "1rem",
      "values": [
        "--pf-c-nav__section__link--FontSize",
        "--pf-global--FontSize--md",
        "$pf-global--FontSize--md",
        "pf-font-prem(16px)",
        "1rem"
      ]
    },
    "c_nav__link_before_BorderBottomWidth": {
      "name": "--pf-c-nav__link--before--BorderBottomWidth",
      "value": "0",
      "values": [
        "--pf-c-nav__section__link--before--BorderBottomWidth",
        "0"
      ]
    },
    "c_nav__link_hover_after_BorderColor": {
      "name": "--pf-c-nav__link--hover--after--BorderColor",
      "value": "transparent",
      "values": [
        "--pf-c-nav__section__link--hover--after--BorderColor",
        "transparent"
      ]
    },
    "c_nav__link_focus_after_BorderColor": {
      "name": "--pf-c-nav__link--focus--after--BorderColor",
      "value": "transparent",
      "values": [
        "--pf-c-nav__section__link--focus--after--BorderColor",
        "transparent"
      ]
    },
    "c_nav__link_active_after_BorderColor": {
      "name": "--pf-c-nav__link--active--after--BorderColor",
      "value": "transparent",
      "values": [
        "--pf-c-nav__section__link--active--after--BorderColor",
        "transparent"
      ]
    },
    "c_nav__link_m_current_after_BorderColor": {
      "name": "--pf-c-nav__link--m-current--after--BorderColor",
      "value": "#2b9af3",
      "values": [
        "--pf-c-nav__section__link--m-current--after--BorderColor",
        "--pf-global--active-color--400",
        "$pf-global--active-color--400",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    },
    "c_nav__link_hover_after_BorderLeftWidth": {
      "name": "--pf-c-nav__link--hover--after--BorderLeftWidth",
      "value": "0",
      "values": [
        "--pf-c-nav__section__link--hover--after--BorderWidth",
        "0"
      ]
    },
    "c_nav__link_focus_after_BorderLeftWidth": {
      "name": "--pf-c-nav__link--focus--after--BorderLeftWidth",
      "value": "0",
      "values": [
        "--pf-c-nav__section__link--focus--after--BorderWidth",
        "0"
      ]
    },
    "c_nav__link_active_after_BorderLeftWidth": {
      "name": "--pf-c-nav__link--active--after--BorderLeftWidth",
      "value": "0",
      "values": [
        "--pf-c-nav__section__link--active--after--BorderWidth",
        "0"
      ]
    },
    "c_nav__link_m_current_after_BorderLeftWidth": {
      "name": "--pf-c-nav__link--m-current--after--BorderLeftWidth",
      "value": "4px",
      "values": [
        "--pf-c-nav__section__link--m-current--after--BorderWidth",
        "--pf-global--BorderWidth--xl",
        "$pf-global--BorderWidth--xl",
        "4px"
      ]
    },
    "c_nav_c_divider_MarginBottom": {
      "name": "--pf-c-nav--c-divider--MarginBottom",
      "value": "0"
    }
  },
  ".pf-c-nav__section + .pf-c-nav__section": {
    "c_nav__section_MarginTop": {
      "name": "--pf-c-nav__section--MarginTop",
      "value": "2rem",
      "values": [
        "--pf-c-nav__section--section--MarginTop",
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    }
  },
  ".pf-c-nav__scroll-button:nth-of-type(1)": {
    "c_nav__scroll_button_before_BorderRightWidth": {
      "name": "--pf-c-nav__scroll-button--before--BorderRightWidth",
      "value": "1px",
      "values": [
        "--pf-c-nav__scroll-button--before--BorderWidth",
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    }
  },
  ".pf-c-nav__scroll-button:nth-of-type(2)": {
    "c_nav__scroll_button_before_BorderLeftWidth": {
      "name": "--pf-c-nav__scroll-button--before--BorderLeftWidth",
      "value": "1px",
      "values": [
        "--pf-c-nav__scroll-button--before--BorderWidth",
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    }
  }
};
export default c_nav;
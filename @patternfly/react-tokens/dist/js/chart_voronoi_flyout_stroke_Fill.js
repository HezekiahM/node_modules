"use strict";
exports.__esModule = true;
exports.chart_voronoi_flyout_stroke_Fill = {
  "name": "--pf-chart-voronoi--flyout--stroke--Fill",
  "value": "#151515",
  "var": "var(--pf-chart-voronoi--flyout--stroke--Fill)"
};
exports["default"] = exports.chart_voronoi_flyout_stroke_Fill;
"use strict";
exports.__esModule = true;
exports.c_data_list_m_compact__item_content_PaddingBottom = {
  "name": "--pf-c-data-list--m-compact__item-content--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-data-list--m-compact__item-content--PaddingBottom)"
};
exports["default"] = exports.c_data_list_m_compact__item_content_PaddingBottom;
export const c_data_list__item_BackgroundColor: {
  "name": "--pf-c-data-list__item--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-data-list__item--BackgroundColor)"
};
export default c_data_list__item_BackgroundColor;
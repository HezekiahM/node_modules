"use strict";
exports.__esModule = true;
exports.c_popover__content_PaddingRight = {
  "name": "--pf-c-popover__content--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-popover__content--PaddingRight)"
};
exports["default"] = exports.c_popover__content_PaddingRight;
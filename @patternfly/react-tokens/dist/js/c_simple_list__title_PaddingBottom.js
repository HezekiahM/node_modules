"use strict";
exports.__esModule = true;
exports.c_simple_list__title_PaddingBottom = {
  "name": "--pf-c-simple-list__title--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-simple-list__title--PaddingBottom)"
};
exports["default"] = exports.c_simple_list__title_PaddingBottom;
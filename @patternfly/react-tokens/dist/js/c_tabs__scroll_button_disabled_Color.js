"use strict";
exports.__esModule = true;
exports.c_tabs__scroll_button_disabled_Color = {
  "name": "--pf-c-tabs__scroll-button--disabled--Color",
  "value": "#d2d2d2",
  "var": "var(--pf-c-tabs__scroll-button--disabled--Color)"
};
exports["default"] = exports.c_tabs__scroll_button_disabled_Color;
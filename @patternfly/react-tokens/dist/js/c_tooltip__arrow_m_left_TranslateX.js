"use strict";
exports.__esModule = true;
exports.c_tooltip__arrow_m_left_TranslateX = {
  "name": "--pf-c-tooltip__arrow--m-left--TranslateX",
  "value": "50%",
  "var": "var(--pf-c-tooltip__arrow--m-left--TranslateX)"
};
exports["default"] = exports.c_tooltip__arrow_m_left_TranslateX;
"use strict";
exports.__esModule = true;
exports.c_form__label_required_FontSize = {
  "name": "--pf-c-form__label-required--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-form__label-required--FontSize)"
};
exports["default"] = exports.c_form__label_required_FontSize;
"use strict";
exports.__esModule = true;
exports.c_hint_BackgroundColor = {
  "name": "--pf-c-hint--BackgroundColor",
  "value": "#e7f1fa",
  "var": "var(--pf-c-hint--BackgroundColor)"
};
exports["default"] = exports.c_hint_BackgroundColor;
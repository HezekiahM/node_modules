"use strict";
exports.__esModule = true;
exports.c_notification_badge__i_Width = {
  "name": "--pf-c-notification-badge__i--Width",
  "value": "1rem",
  "var": "var(--pf-c-notification-badge__i--Width)"
};
exports["default"] = exports.c_notification_badge__i_Width;
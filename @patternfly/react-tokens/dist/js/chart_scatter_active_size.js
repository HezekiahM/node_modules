"use strict";
exports.__esModule = true;
exports.chart_scatter_active_size = {
  "name": "--pf-chart-scatter--active--size",
  "value": 5,
  "var": "var(--pf-chart-scatter--active--size)"
};
exports["default"] = exports.chart_scatter_active_size;
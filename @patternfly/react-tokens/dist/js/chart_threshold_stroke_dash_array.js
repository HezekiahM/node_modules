"use strict";
exports.__esModule = true;
exports.chart_threshold_stroke_dash_array = {
  "name": "--pf-chart-threshold--stroke-dash-array",
  "value": "4,2",
  "var": "var(--pf-chart-threshold--stroke-dash-array)"
};
exports["default"] = exports.chart_threshold_stroke_dash_array;
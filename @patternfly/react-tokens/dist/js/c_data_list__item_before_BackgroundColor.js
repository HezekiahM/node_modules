"use strict";
exports.__esModule = true;
exports.c_data_list__item_before_BackgroundColor = {
  "name": "--pf-c-data-list__item--before--BackgroundColor",
  "value": "#73bcf7",
  "var": "var(--pf-c-data-list__item--before--BackgroundColor)"
};
exports["default"] = exports.c_data_list__item_before_BackgroundColor;
"use strict";
exports.__esModule = true;
exports.c_popover_c_button_MarginLeft = {
  "name": "--pf-c-popover--c-button--MarginLeft",
  "value": "0.5rem",
  "var": "var(--pf-c-popover--c-button--MarginLeft)"
};
exports["default"] = exports.c_popover_c_button_MarginLeft;
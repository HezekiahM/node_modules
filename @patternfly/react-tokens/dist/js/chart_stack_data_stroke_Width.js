"use strict";
exports.__esModule = true;
exports.chart_stack_data_stroke_Width = {
  "name": "--pf-chart-stack--data--stroke--Width",
  "value": 1,
  "var": "var(--pf-chart-stack--data--stroke--Width)"
};
exports["default"] = exports.chart_stack_data_stroke_Width;
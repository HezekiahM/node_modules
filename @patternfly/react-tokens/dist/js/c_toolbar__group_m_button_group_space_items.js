"use strict";
exports.__esModule = true;
exports.c_toolbar__group_m_button_group_space_items = {
  "name": "--pf-c-toolbar__group--m-button-group--space-items",
  "value": "0.5rem",
  "var": "var(--pf-c-toolbar__group--m-button-group--space-items)"
};
exports["default"] = exports.c_toolbar__group_m_button_group_space_items;
export const c_tooltip__arrow_m_bottom_TranslateY: {
  "name": "--pf-c-tooltip__arrow--m-bottom--TranslateY",
  "value": "-50%",
  "var": "var(--pf-c-tooltip__arrow--m-bottom--TranslateY)"
};
export default c_tooltip__arrow_m_bottom_TranslateY;
"use strict";
exports.__esModule = true;
exports.c_tooltip__content_FontSize = {
  "name": "--pf-c-tooltip__content--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-tooltip__content--FontSize)"
};
exports["default"] = exports.c_tooltip__content_FontSize;
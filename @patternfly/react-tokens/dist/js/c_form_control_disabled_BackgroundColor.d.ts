export const c_form_control_disabled_BackgroundColor: {
  "name": "--pf-c-form-control--disabled--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-form-control--disabled--BackgroundColor)"
};
export default c_form_control_disabled_BackgroundColor;
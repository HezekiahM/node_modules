"use strict";
exports.__esModule = true;
exports.c_login__container_PaddingRight = {
  "name": "--pf-c-login__container--PaddingRight",
  "value": "6.125rem",
  "var": "var(--pf-c-login__container--PaddingRight)"
};
exports["default"] = exports.c_login__container_PaddingRight;
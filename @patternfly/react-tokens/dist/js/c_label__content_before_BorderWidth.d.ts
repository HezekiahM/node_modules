export const c_label__content_before_BorderWidth: {
  "name": "--pf-c-label__content--before--BorderWidth",
  "value": "1px",
  "var": "var(--pf-c-label__content--before--BorderWidth)"
};
export default c_label__content_before_BorderWidth;
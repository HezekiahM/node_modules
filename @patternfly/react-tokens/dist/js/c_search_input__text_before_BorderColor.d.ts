export const c_search_input__text_before_BorderColor: {
  "name": "--pf-c-search-input__text--before--BorderColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-search-input__text--before--BorderColor)"
};
export default c_search_input__text_before_BorderColor;
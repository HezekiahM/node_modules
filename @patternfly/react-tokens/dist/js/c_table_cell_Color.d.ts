export const c_table_cell_Color: {
  "name": "--pf-c-table--cell--Color",
  "value": "#151515",
  "var": "var(--pf-c-table--cell--Color)"
};
export default c_table_cell_Color;
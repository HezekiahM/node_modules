"use strict";
exports.__esModule = true;
exports.c_button_after_BorderRadius = {
  "name": "--pf-c-button--after--BorderRadius",
  "value": "3px",
  "var": "var(--pf-c-button--after--BorderRadius)"
};
exports["default"] = exports.c_button_after_BorderRadius;
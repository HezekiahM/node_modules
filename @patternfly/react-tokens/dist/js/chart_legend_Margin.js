"use strict";
exports.__esModule = true;
exports.chart_legend_Margin = {
  "name": "--pf-chart-legend--Margin",
  "value": 16,
  "var": "var(--pf-chart-legend--Margin)"
};
exports["default"] = exports.chart_legend_Margin;
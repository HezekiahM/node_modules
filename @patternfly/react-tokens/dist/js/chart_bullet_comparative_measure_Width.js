"use strict";
exports.__esModule = true;
exports.chart_bullet_comparative_measure_Width = {
  "name": "--pf-chart-bullet--comparative-measure--Width",
  "value": 30,
  "var": "var(--pf-chart-bullet--comparative-measure--Width)"
};
exports["default"] = exports.chart_bullet_comparative_measure_Width;
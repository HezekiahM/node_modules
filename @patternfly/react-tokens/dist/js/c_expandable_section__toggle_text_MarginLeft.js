"use strict";
exports.__esModule = true;
exports.c_expandable_section__toggle_text_MarginLeft = {
  "name": "--pf-c-expandable-section__toggle-text--MarginLeft",
  "value": "calc(0.25rem + 0.5rem)",
  "var": "var(--pf-c-expandable-section__toggle-text--MarginLeft)"
};
exports["default"] = exports.c_expandable_section__toggle_text_MarginLeft;
"use strict";
exports.__esModule = true;
exports.global_spacer_sm = {
  "name": "--pf-global--spacer--sm",
  "value": "0.5rem",
  "var": "var(--pf-global--spacer--sm)"
};
exports["default"] = exports.global_spacer_sm;
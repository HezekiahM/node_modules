export const c_alert_m_info__title_Color: {
  "name": "--pf-c-alert--m-info__title--Color",
  "value": "#002952",
  "var": "var(--pf-c-alert--m-info__title--Color)"
};
export default c_alert_m_info__title_Color;
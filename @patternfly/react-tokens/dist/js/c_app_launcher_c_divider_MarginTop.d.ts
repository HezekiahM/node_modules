export const c_app_launcher_c_divider_MarginTop: {
  "name": "--pf-c-app-launcher--c-divider--MarginTop",
  "value": "0.5rem",
  "var": "var(--pf-c-app-launcher--c-divider--MarginTop)"
};
export default c_app_launcher_c_divider_MarginTop;
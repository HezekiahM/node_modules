"use strict";
exports.__esModule = true;
exports.c_chip__c_badge_MarginLeft = {
  "name": "--pf-c-chip__c-badge--MarginLeft",
  "value": "0.25rem",
  "var": "var(--pf-c-chip__c-badge--MarginLeft)"
};
exports["default"] = exports.c_chip__c_badge_MarginLeft;
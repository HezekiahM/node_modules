"use strict";
exports.__esModule = true;
exports.c_dropdown__menu_item_PaddingBottom = {
  "name": "--pf-c-dropdown__menu-item--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-dropdown__menu-item--PaddingBottom)"
};
exports["default"] = exports.c_dropdown__menu_item_PaddingBottom;
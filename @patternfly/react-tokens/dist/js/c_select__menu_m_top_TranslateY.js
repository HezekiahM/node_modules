"use strict";
exports.__esModule = true;
exports.c_select__menu_m_top_TranslateY = {
  "name": "--pf-c-select__menu--m-top--TranslateY",
  "value": "calc(-100% - 0.25rem)",
  "var": "var(--pf-c-select__menu--m-top--TranslateY)"
};
exports["default"] = exports.c_select__menu_m_top_TranslateY;
"use strict";
exports.__esModule = true;
exports.c_button_FontSize = {
  "name": "--pf-c-button--FontSize",
  "value": "0.75rem",
  "var": "var(--pf-c-button--FontSize)"
};
exports["default"] = exports.c_button_FontSize;
"use strict";
exports.__esModule = true;
exports.c_wizard__header_lg_PaddingRight = {
  "name": "--pf-c-wizard__header--lg--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-wizard__header--lg--PaddingRight)"
};
exports["default"] = exports.c_wizard__header_lg_PaddingRight;
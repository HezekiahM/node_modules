"use strict";
exports.__esModule = true;
exports.c_spinner_AnimationDuration = {
  "name": "--pf-c-spinner--AnimationDuration",
  "value": "1.5s",
  "var": "var(--pf-c-spinner--AnimationDuration)"
};
exports["default"] = exports.c_spinner_AnimationDuration;
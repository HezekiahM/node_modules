"use strict";
exports.__esModule = true;
exports.c_page__header_sidebar_toggle__c_button_MarginRight = {
  "name": "--pf-c-page__header-sidebar-toggle__c-button--MarginRight",
  "value": "1rem",
  "var": "var(--pf-c-page__header-sidebar-toggle__c-button--MarginRight)"
};
exports["default"] = exports.c_page__header_sidebar_toggle__c_button_MarginRight;
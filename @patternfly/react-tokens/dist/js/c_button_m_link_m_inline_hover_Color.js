"use strict";
exports.__esModule = true;
exports.c_button_m_link_m_inline_hover_Color = {
  "name": "--pf-c-button--m-link--m-inline--hover--Color",
  "value": "#004080",
  "var": "var(--pf-c-button--m-link--m-inline--hover--Color)"
};
exports["default"] = exports.c_button_m_link_m_inline_hover_Color;
"use strict";
exports.__esModule = true;
exports.c_data_list = {
  ".pf-c-data-list": {
    "global_Color_100": {
      "name": "--pf-global--Color--100",
      "value": "#151515",
      "values": [
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "global_Color_200": {
      "name": "--pf-global--Color--200",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--dark-200",
        "$pf-global--Color--dark-200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "global_BorderColor_100": {
      "name": "--pf-global--BorderColor--100",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--dark-100",
        "$pf-global--BorderColor--dark-100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "global_primary_color_100": {
      "name": "--pf-global--primary-color--100",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--dark-100",
        "$pf-global--primary-color--dark-100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "global_link_Color": {
      "name": "--pf-global--link--Color",
      "value": "#06c",
      "values": [
        "--pf-global--link--Color--dark",
        "$pf-global--link--Color--dark",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "global_link_Color_hover": {
      "name": "--pf-global--link--Color--hover",
      "value": "#004080",
      "values": [
        "--pf-global--link--Color--dark--hover",
        "$pf-global--link--Color--dark--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "global_BackgroundColor_100": {
      "name": "--pf-global--BackgroundColor--100",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--light-100",
        "$pf-global--BackgroundColor--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_data_list_BorderTopColor": {
      "name": "--pf-c-data-list--BorderTopColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_data_list_BorderTopWidth": {
      "name": "--pf-c-data-list--BorderTopWidth",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_data_list_sm_BorderTopWidth": {
      "name": "--pf-c-data-list--sm--BorderTopWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_data_list_sm_BorderTopColor": {
      "name": "--pf-c-data-list--sm--BorderTopColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_data_list__item_BackgroundColor": {
      "name": "--pf-c-data-list__item--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_data_list__item_m_selected_ZIndex": {
      "name": "--pf-c-data-list__item--m-selected--ZIndex",
      "value": "100",
      "values": [
        "--pf-global--ZIndex--xs",
        "$pf-global--ZIndex--xs",
        "100"
      ]
    },
    "c_data_list__item_m_expanded_before_BackgroundColor": {
      "name": "--pf-c-data-list__item--m-expanded--before--BackgroundColor",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_data_list__item_m_selected_before_BackgroundColor": {
      "name": "--pf-c-data-list__item--m-selected--before--BackgroundColor",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_data_list__item_m_selected_BoxShadow": {
      "name": "--pf-c-data-list__item--m-selected--BoxShadow",
      "value": "0 -0.125rem 0.25rem -0.0625rem rgba(3, 3, 3, 0.16), 0 0.125rem 0.25rem -0.0625rem rgba(3, 3, 3, 0.16)",
      "values": [
        "--pf-global--BoxShadow--sm-top, --pf-global--BoxShadow--sm-bottom",
        "$pf-global--BoxShadow--sm-top, $pf-global--BoxShadow--sm-bottom",
        "0 pf-size-prem(-2px) pf-size-prem(4px) pf-size-prem(-1px) rgba($pf-color-black-1000, .16), 0 pf-size-prem(2px) pf-size-prem(4px) pf-size-prem(-1px) rgba($pf-color-black-1000, .16)",
        "0 pf-size-prem(-2px) pf-size-prem(4px) pf-size-prem(-1px) rgba(#030303, .16), 0 pf-size-prem(2px) pf-size-prem(4px) pf-size-prem(-1px) rgba(#030303, .16)",
        "0 -0.125rem 0.25rem -0.0625rem rgba(3, 3, 3, 0.16), 0 0.125rem 0.25rem -0.0625rem rgba(3, 3, 3, 0.16)"
      ]
    },
    "c_data_list__item_m_selectable_OutlineOffset": {
      "name": "--pf-c-data-list__item--m-selectable--OutlineOffset",
      "value": "calc(-1 * 0.25rem)",
      "values": [
        "calc(-1 * --pf-global--spacer--xs)",
        "calc(-1 * $pf-global--spacer--xs)",
        "calc(-1 * pf-size-prem(4px))",
        "calc(-1 * 0.25rem)"
      ]
    },
    "c_data_list__item_m_selectable_hover_ZIndex": {
      "name": "--pf-c-data-list__item--m-selectable--hover--ZIndex",
      "value": "calc(100 + 1)",
      "values": [
        "calc(--pf-c-data-list__item--m-selected--ZIndex + 1)",
        "calc(--pf-global--ZIndex--xs + 1)",
        "calc($pf-global--ZIndex--xs + 1)",
        "calc(100 + 1)"
      ]
    },
    "c_data_list__item_m_selectable_hover_BoxShadow": {
      "name": "--pf-c-data-list__item--m-selectable--hover--BoxShadow",
      "value": "0 -0.125rem 0.25rem -0.0625rem rgba(3, 3, 3, 0.16), 0 0.125rem 0.25rem -0.0625rem rgba(3, 3, 3, 0.16)",
      "values": [
        "--pf-global--BoxShadow--sm-top, --pf-global--BoxShadow--sm-bottom",
        "$pf-global--BoxShadow--sm-top, $pf-global--BoxShadow--sm-bottom",
        "0 pf-size-prem(-2px) pf-size-prem(4px) pf-size-prem(-1px) rgba($pf-color-black-1000, .16), 0 pf-size-prem(2px) pf-size-prem(4px) pf-size-prem(-1px) rgba($pf-color-black-1000, .16)",
        "0 pf-size-prem(-2px) pf-size-prem(4px) pf-size-prem(-1px) rgba(#030303, .16), 0 pf-size-prem(2px) pf-size-prem(4px) pf-size-prem(-1px) rgba(#030303, .16)",
        "0 -0.125rem 0.25rem -0.0625rem rgba(3, 3, 3, 0.16), 0 0.125rem 0.25rem -0.0625rem rgba(3, 3, 3, 0.16)"
      ]
    },
    "c_data_list__item_m_selectable_focus_BoxShadow": {
      "name": "--pf-c-data-list__item--m-selectable--focus--BoxShadow",
      "value": "0 -0.125rem 0.25rem -0.0625rem rgba(3, 3, 3, 0.16), 0 0.125rem 0.25rem -0.0625rem rgba(3, 3, 3, 0.16)",
      "values": [
        "--pf-global--BoxShadow--sm-top, --pf-global--BoxShadow--sm-bottom",
        "$pf-global--BoxShadow--sm-top, $pf-global--BoxShadow--sm-bottom",
        "0 pf-size-prem(-2px) pf-size-prem(4px) pf-size-prem(-1px) rgba($pf-color-black-1000, .16), 0 pf-size-prem(2px) pf-size-prem(4px) pf-size-prem(-1px) rgba($pf-color-black-1000, .16)",
        "0 pf-size-prem(-2px) pf-size-prem(4px) pf-size-prem(-1px) rgba(#030303, .16), 0 pf-size-prem(2px) pf-size-prem(4px) pf-size-prem(-1px) rgba(#030303, .16)",
        "0 -0.125rem 0.25rem -0.0625rem rgba(3, 3, 3, 0.16), 0 0.125rem 0.25rem -0.0625rem rgba(3, 3, 3, 0.16)"
      ]
    },
    "c_data_list__item_m_selectable_active_BoxShadow": {
      "name": "--pf-c-data-list__item--m-selectable--active--BoxShadow",
      "value": "0 -0.125rem 0.25rem -0.0625rem rgba(3, 3, 3, 0.16), 0 0.125rem 0.25rem -0.0625rem rgba(3, 3, 3, 0.16)",
      "values": [
        "--pf-global--BoxShadow--sm-top, --pf-global--BoxShadow--sm-bottom",
        "$pf-global--BoxShadow--sm-top, $pf-global--BoxShadow--sm-bottom",
        "0 pf-size-prem(-2px) pf-size-prem(4px) pf-size-prem(-1px) rgba($pf-color-black-1000, .16), 0 pf-size-prem(2px) pf-size-prem(4px) pf-size-prem(-1px) rgba($pf-color-black-1000, .16)",
        "0 pf-size-prem(-2px) pf-size-prem(4px) pf-size-prem(-1px) rgba(#030303, .16), 0 pf-size-prem(2px) pf-size-prem(4px) pf-size-prem(-1px) rgba(#030303, .16)",
        "0 -0.125rem 0.25rem -0.0625rem rgba(3, 3, 3, 0.16), 0 0.125rem 0.25rem -0.0625rem rgba(3, 3, 3, 0.16)"
      ]
    },
    "c_data_list__item_m_expanded_m_selectable_before_BackgroundColor": {
      "name": "--pf-c-data-list__item--m-expanded--m-selectable--before--BackgroundColor",
      "value": "#73bcf7",
      "values": [
        "--pf-global--active-color--300",
        "$pf-global--active-color--300",
        "$pf-color-blue-200",
        "#73bcf7"
      ]
    },
    "c_data_list__item_BorderBottomColor": {
      "name": "--pf-c-data-list__item--BorderBottomColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_data_list__item_BorderBottomWidth": {
      "name": "--pf-c-data-list__item--BorderBottomWidth",
      "value": "0.5rem"
    },
    "c_data_list__item_m_selectable_hover_item_BorderTopColor": {
      "name": "--pf-c-data-list__item--m-selectable--hover--item--BorderTopColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-c-data-list__item--BorderBottomColor",
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_data_list__item_m_selectable_hover_item_BorderTopWidth": {
      "name": "--pf-c-data-list__item--m-selectable--hover--item--BorderTopWidth",
      "value": "0.5rem",
      "values": [
        "--pf-c-data-list__item--BorderBottomWidth",
        "0.5rem"
      ]
    },
    "c_data_list__item_sm_BorderBottomWidth": {
      "name": "--pf-c-data-list__item--sm--BorderBottomWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_data_list__item_sm_BorderBottomColor": {
      "name": "--pf-c-data-list__item--sm--BorderBottomColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_data_list__item_before_BackgroundColor": {
      "name": "--pf-c-data-list__item--before--BackgroundColor",
      "value": "transparent"
    },
    "c_data_list__item_before_Width": {
      "name": "--pf-c-data-list__item--before--Width",
      "value": "3px",
      "values": [
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    },
    "c_data_list__item_before_Transition": {
      "name": "--pf-c-data-list__item--before--Transition",
      "value": "all 250ms cubic-bezier(.42, 0, .58, 1)",
      "values": [
        "--pf-global--Transition",
        "$pf-global--Transition",
        "all 250ms cubic-bezier(.42, 0, .58, 1)"
      ]
    },
    "c_data_list__item_before_Top": {
      "name": "--pf-c-data-list__item--before--Top",
      "value": "0"
    },
    "c_data_list__item_before_sm_Top": {
      "name": "--pf-c-data-list__item--before--sm--Top",
      "value": "calc(0.5rem * -1)",
      "values": [
        "calc(--pf-c-data-list__item--BorderBottomWidth * -1)",
        "calc(0.5rem * -1)"
      ]
    },
    "c_data_list__item_row_PaddingRight": {
      "name": "--pf-c-data-list__item-row--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_data_list__item_row_PaddingLeft": {
      "name": "--pf-c-data-list__item-row--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_data_list__item_row_xl_PaddingRight": {
      "name": "--pf-c-data-list__item-row--xl--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_data_list__item_row_xl_PaddingLeft": {
      "name": "--pf-c-data-list__item-row--xl--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_data_list__item_content_md_PaddingBottom": {
      "name": "--pf-c-data-list__item-content--md--PaddingBottom",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_data_list__cell_PaddingTop": {
      "name": "--pf-c-data-list__cell--PaddingTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_data_list__cell_PaddingBottom": {
      "name": "--pf-c-data-list__cell--PaddingBottom",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_data_list__cell_MarginRight": {
      "name": "--pf-c-data-list__cell--MarginRight",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_data_list__cell_md_PaddingBottom": {
      "name": "--pf-c-data-list__cell--md--PaddingBottom",
      "value": "0"
    },
    "c_data_list__cell_m_icon_MarginRight": {
      "name": "--pf-c-data-list__cell--m-icon--MarginRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_data_list__cell_cell_PaddingTop": {
      "name": "--pf-c-data-list__cell--cell--PaddingTop",
      "value": "0"
    },
    "c_data_list__cell_cell_md_PaddingTop": {
      "name": "--pf-c-data-list__cell--cell--md--PaddingTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_data_list__cell_m_icon_cell_PaddingTop": {
      "name": "--pf-c-data-list__cell--m-icon--cell--PaddingTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_data_list__toggle_MarginLeft": {
      "name": "--pf-c-data-list__toggle--MarginLeft",
      "value": "calc(0.5rem * -1)",
      "values": [
        "calc(--pf-global--spacer--sm * -1)",
        "calc($pf-global--spacer--sm * -1)",
        "calc(pf-size-prem(8px) * -1)",
        "calc(0.5rem * -1)"
      ]
    },
    "c_data_list__toggle_MarginTop": {
      "name": "--pf-c-data-list__toggle--MarginTop",
      "value": "calc(0.375rem * -1)",
      "values": [
        "calc(--pf-global--spacer--form-element * -1)",
        "calc($pf-global--spacer--form-element * -1)",
        "calc(pf-size-prem(6px) * -1)",
        "calc(0.375rem * -1)"
      ]
    },
    "c_data_list__toggle_icon_Transition": {
      "name": "--pf-c-data-list__toggle-icon--Transition",
      "value": ".2s ease-in 0s"
    },
    "c_data_list__item_m_expanded__toggle_icon_Rotate": {
      "name": "--pf-c-data-list__item--m-expanded__toggle-icon--Rotate",
      "value": "90deg"
    },
    "c_data_list__item_control_PaddingTop": {
      "name": "--pf-c-data-list__item-control--PaddingTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_data_list__item_control_PaddingBottom": {
      "name": "--pf-c-data-list__item-control--PaddingBottom",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_data_list__item_control_MarginRight": {
      "name": "--pf-c-data-list__item-control--MarginRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_data_list__item_control_md_MarginRight": {
      "name": "--pf-c-data-list__item-control--md--MarginRight",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_data_list__item_control_not_last_child_MarginRight": {
      "name": "--pf-c-data-list__item-control--not-last-child--MarginRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_data_list__item_action_Display": {
      "name": "--pf-c-data-list__item-action--Display",
      "value": "flex"
    },
    "c_data_list__item_action_PaddingTop": {
      "name": "--pf-c-data-list__item-action--PaddingTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_data_list__item_action_PaddingBottom": {
      "name": "--pf-c-data-list__item-action--PaddingBottom",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_data_list__item_action_MarginLeft": {
      "name": "--pf-c-data-list__item-action--MarginLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_data_list__item_action_md_MarginLeft": {
      "name": "--pf-c-data-list__item-action--md--MarginLeft",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_data_list__item_action_not_last_child_MarginRight": {
      "name": "--pf-c-data-list__item-action--not-last-child--MarginRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_data_list__action_MarginTop": {
      "name": "--pf-c-data-list__action--MarginTop",
      "value": "calc(0.375rem * -1)",
      "values": [
        "calc(--pf-global--spacer--form-element * -1)",
        "calc($pf-global--spacer--form-element * -1)",
        "calc(pf-size-prem(6px) * -1)",
        "calc(0.375rem * -1)"
      ]
    },
    "c_data_list__expandable_content_BorderTopWidth": {
      "name": "--pf-c-data-list__expandable-content--BorderTopWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_data_list__expandable_content_BorderTopColor": {
      "name": "--pf-c-data-list__expandable-content--BorderTopColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_data_list__expandable_content_MarginRight": {
      "name": "--pf-c-data-list__expandable-content--MarginRight",
      "value": "calc(1rem * -1)",
      "values": [
        "calc(--pf-c-data-list__expandable-content-body--PaddingRight * -1)",
        "calc(--pf-global--spacer--md * -1)",
        "calc($pf-global--spacer--md * -1)",
        "calc(pf-size-prem(16px) * -1)",
        "calc(1rem * -1)"
      ]
    },
    "c_data_list__expandable_content_MarginLeft": {
      "name": "--pf-c-data-list__expandable-content--MarginLeft",
      "value": "calc(1rem * -1)",
      "values": [
        "calc(--pf-c-data-list__expandable-content-body--PaddingLeft * -1)",
        "calc(--pf-global--spacer--md * -1)",
        "calc($pf-global--spacer--md * -1)",
        "calc(pf-size-prem(16px) * -1)",
        "calc(1rem * -1)"
      ]
    },
    "c_data_list__expandable_content_MaxHeight": {
      "name": "--pf-c-data-list__expandable-content--MaxHeight",
      "value": "37.5rem"
    },
    "c_data_list__expandable_content_before_Top": {
      "name": "--pf-c-data-list__expandable-content--before--Top",
      "value": "calc(0.5rem * -1)",
      "values": [
        "calc(--pf-c-data-list__item--BorderBottomWidth * -1)",
        "calc(0.5rem * -1)"
      ]
    },
    "c_data_list__expandable_content_body_PaddingTop": {
      "name": "--pf-c-data-list__expandable-content-body--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_data_list__expandable_content_body_PaddingRight": {
      "name": "--pf-c-data-list__expandable-content-body--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_data_list__expandable_content_body_PaddingBottom": {
      "name": "--pf-c-data-list__expandable-content-body--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_data_list__expandable_content_body_PaddingLeft": {
      "name": "--pf-c-data-list__expandable-content-body--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_data_list__expandable_content_body_md_PaddingTop": {
      "name": "--pf-c-data-list__expandable-content-body--md--PaddingTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_data_list__expandable_content_body_xl_PaddingRight": {
      "name": "--pf-c-data-list__expandable-content-body--xl--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_data_list__expandable_content_body_md_PaddingBottom": {
      "name": "--pf-c-data-list__expandable-content-body--md--PaddingBottom",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_data_list__expandable_content_body_xl_PaddingLeft": {
      "name": "--pf-c-data-list__expandable-content-body--xl--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_data_list_m_compact_FontSize": {
      "name": "--pf-c-data-list--m-compact--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_data_list_m_compact__check_FontSize": {
      "name": "--pf-c-data-list--m-compact__check--FontSize",
      "value": "1rem",
      "values": [
        "--pf-global--FontSize--md",
        "$pf-global--FontSize--md",
        "pf-font-prem(16px)",
        "1rem"
      ]
    },
    "c_data_list_m_compact__cell_PaddingTop": {
      "name": "--pf-c-data-list--m-compact__cell--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_data_list_m_compact__cell_PaddingBottom": {
      "name": "--pf-c-data-list--m-compact__cell--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_data_list_m_compact__cell_md_PaddingBottom": {
      "name": "--pf-c-data-list--m-compact__cell--md--PaddingBottom",
      "value": "0"
    },
    "c_data_list_m_compact__cell_cell_PaddingTop": {
      "name": "--pf-c-data-list--m-compact__cell-cell--PaddingTop",
      "value": "0"
    },
    "c_data_list_m_compact__cell_cell_md_PaddingTop": {
      "name": "--pf-c-data-list--m-compact__cell-cell--md--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_data_list_m_compact__cell_cell_MarginRight": {
      "name": "--pf-c-data-list--m-compact__cell--cell--MarginRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_data_list_m_compact__item_control_PaddingTop": {
      "name": "--pf-c-data-list--m-compact__item-control--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_data_list_m_compact__item_control_PaddingBottom": {
      "name": "--pf-c-data-list--m-compact__item-control--PaddingBottom",
      "value": "0"
    },
    "c_data_list_m_compact__item_control_MarginRight": {
      "name": "--pf-c-data-list--m-compact__item-control--MarginRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_data_list_m_compact__item_action_PaddingTop": {
      "name": "--pf-c-data-list--m-compact__item-action--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_data_list_m_compact__item_action_PaddingBottom": {
      "name": "--pf-c-data-list--m-compact__item-action--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_data_list_m_compact__item_action_MarginLeft": {
      "name": "--pf-c-data-list--m-compact__item-action--MarginLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_data_list_m_compact__item_content_PaddingBottom": {
      "name": "--pf-c-data-list--m-compact__item-content--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-c-data-list__item-action": {
    "hidden_visible_visible_Visibility": {
      "name": "--pf-hidden-visible--visible--Visibility",
      "value": "visible"
    },
    "hidden_visible_hidden_Display": {
      "name": "--pf-hidden-visible--hidden--Display",
      "value": "none"
    },
    "hidden_visible_hidden_Visibility": {
      "name": "--pf-hidden-visible--hidden--Visibility",
      "value": "hidden"
    },
    "hidden_visible_Display": {
      "name": "--pf-hidden-visible--Display",
      "value": "flex",
      "values": [
        "--pf-hidden-visible--visible--Display",
        "--pf-c-data-list__item-action--Display",
        "flex"
      ]
    },
    "hidden_visible_Visibility": {
      "name": "--pf-hidden-visible--Visibility",
      "value": "visible",
      "values": [
        "--pf-hidden-visible--visible--Visibility",
        "visible"
      ]
    },
    "hidden_visible_visible_Display": {
      "name": "--pf-hidden-visible--visible--Display",
      "value": "flex",
      "values": [
        "--pf-c-data-list__item-action--Display",
        "flex"
      ]
    }
  },
  ".pf-m-hidden.pf-c-data-list__item-action": {
    "hidden_visible_Display": {
      "name": "--pf-hidden-visible--Display",
      "value": "none",
      "values": [
        "--pf-hidden-visible--hidden--Display",
        "none"
      ]
    },
    "hidden_visible_Visibility": {
      "name": "--pf-hidden-visible--Visibility",
      "value": "hidden",
      "values": [
        "--pf-hidden-visible--hidden--Visibility",
        "hidden"
      ]
    }
  },
  ".pf-c-data-list.pf-m-compact": {
    "c_data_list__cell_PaddingTop": {
      "name": "--pf-c-data-list__cell--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-c-data-list--m-compact__cell--PaddingTop",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_data_list__cell_PaddingBottom": {
      "name": "--pf-c-data-list__cell--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-c-data-list--m-compact__cell--PaddingBottom",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_data_list__cell_MarginRight": {
      "name": "--pf-c-data-list__cell--MarginRight",
      "value": "1rem",
      "values": [
        "--pf-c-data-list--m-compact__cell--cell--MarginRight",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_data_list__cell_cell_PaddingTop": {
      "name": "--pf-c-data-list__cell--cell--PaddingTop",
      "value": "0",
      "values": [
        "--pf-c-data-list--m-compact__cell-cell--PaddingTop",
        "0"
      ]
    },
    "c_data_list__item_action_MarginLeft": {
      "name": "--pf-c-data-list__item-action--MarginLeft",
      "value": "1rem",
      "values": [
        "--pf-c-data-list--m-compact__item-action--MarginLeft",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_data_list__item_action_PaddingTop": {
      "name": "--pf-c-data-list__item-action--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-c-data-list--m-compact__item-action--PaddingTop",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_data_list__item_action_PaddingBottom": {
      "name": "--pf-c-data-list__item-action--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-c-data-list--m-compact__item-action--PaddingBottom",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_data_list__item_control_MarginRight": {
      "name": "--pf-c-data-list__item-control--MarginRight",
      "value": "1rem",
      "values": [
        "--pf-c-data-list--m-compact__item-control--MarginRight",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_data_list__item_control_PaddingTop": {
      "name": "--pf-c-data-list__item-control--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-c-data-list--m-compact__item-control--PaddingTop",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_data_list__item_control_PaddingBottom": {
      "name": "--pf-c-data-list__item-control--PaddingBottom",
      "value": "0",
      "values": [
        "--pf-c-data-list--m-compact__item-control--PaddingBottom",
        "0"
      ]
    },
    "c_data_list__item_content_md_PaddingBottom": {
      "name": "--pf-c-data-list__item-content--md--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-c-data-list--m-compact__item-content--PaddingBottom",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-c-data-list__item.pf-m-selectable:hover:not(.pf-m-selected):not(:last-child)": {
    "c_data_list__item_BorderBottomWidth": {
      "name": "--pf-c-data-list__item--BorderBottomWidth",
      "value": "0"
    }
  },
  ".pf-c-data-list__item.pf-m-selected": {
    "c_data_list__item_before_BackgroundColor": {
      "name": "--pf-c-data-list__item--before--BackgroundColor",
      "value": "#06c",
      "values": [
        "--pf-c-data-list__item--m-selected--before--BackgroundColor",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-data-list__item.pf-m-expanded": {
    "c_data_list__item_before_BackgroundColor": {
      "name": "--pf-c-data-list__item--before--BackgroundColor",
      "value": "#06c",
      "values": [
        "--pf-c-data-list__item--m-expanded--before--BackgroundColor",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-data-list__item.pf-m-expanded.pf-m-selectable:not(.pf-m-selected)": {
    "c_data_list__item_before_BackgroundColor": {
      "name": "--pf-c-data-list__item--before--BackgroundColor",
      "value": "#73bcf7",
      "values": [
        "--pf-c-data-list__item--m-expanded--m-selectable--before--BackgroundColor",
        "--pf-global--active-color--300",
        "$pf-global--active-color--300",
        "$pf-color-blue-200",
        "#73bcf7"
      ]
    }
  }
};
exports["default"] = exports.c_data_list;
"use strict";
exports.__esModule = true;
exports.c_chip_PaddingBottom = {
  "name": "--pf-c-chip--PaddingBottom",
  "value": "0.25rem",
  "var": "var(--pf-c-chip--PaddingBottom)"
};
exports["default"] = exports.c_chip_PaddingBottom;
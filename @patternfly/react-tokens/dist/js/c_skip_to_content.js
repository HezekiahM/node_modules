"use strict";
exports.__esModule = true;
exports.c_skip_to_content = {
  ".pf-c-skip-to-content": {
    "c_skip_to_content_Top": {
      "name": "--pf-c-skip-to-content--Top",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_skip_to_content_ZIndex": {
      "name": "--pf-c-skip-to-content--ZIndex",
      "value": "600",
      "values": [
        "--pf-global--ZIndex--2xl",
        "$pf-global--ZIndex--2xl",
        "600"
      ]
    },
    "c_skip_to_content_focus_Left": {
      "name": "--pf-c-skip-to-content--focus--Left",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    }
  }
};
exports["default"] = exports.c_skip_to_content;
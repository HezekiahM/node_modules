"use strict";
exports.__esModule = true;
exports.global_fonticon_path = {
  "name": "--pf-global--fonticon-path",
  "value": "./assets/pficon",
  "var": "var(--pf-global--fonticon-path)"
};
exports["default"] = exports.global_fonticon_path;
"use strict";
exports.__esModule = true;
exports.c_tooltip__content_BackgroundColor = {
  "name": "--pf-c-tooltip__content--BackgroundColor",
  "value": "#151515",
  "var": "var(--pf-c-tooltip__content--BackgroundColor)"
};
exports["default"] = exports.c_tooltip__content_BackgroundColor;
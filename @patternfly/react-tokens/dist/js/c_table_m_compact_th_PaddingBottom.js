"use strict";
exports.__esModule = true;
exports.c_table_m_compact_th_PaddingBottom = {
  "name": "--pf-c-table--m-compact-th--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-table--m-compact-th--PaddingBottom)"
};
exports["default"] = exports.c_table_m_compact_th_PaddingBottom;
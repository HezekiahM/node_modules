"use strict";
exports.__esModule = true;
exports.c_toolbar__item_spacer = {
  "name": "--pf-c-toolbar__item--spacer",
  "value": "1rem",
  "var": "var(--pf-c-toolbar__item--spacer)"
};
exports["default"] = exports.c_toolbar__item_spacer;
"use strict";
exports.__esModule = true;
exports.c_table__action_responsive_MarginLeft = {
  "name": "--pf-c-table__action--responsive--MarginLeft",
  "value": "2rem",
  "var": "var(--pf-c-table__action--responsive--MarginLeft)"
};
exports["default"] = exports.c_table__action_responsive_MarginLeft;
export const c_table__sort__button_hover__sort_indicator_Color: {
  "name": "--pf-c-table__sort__button--hover__sort-indicator--Color",
  "value": "#151515",
  "var": "var(--pf-c-table__sort__button--hover__sort-indicator--Color)"
};
export default c_table__sort__button_hover__sort_indicator_Color;
"use strict";
exports.__esModule = true;
exports.c_form__helper_text_m_warning_Color = {
  "name": "--pf-c-form__helper-text--m-warning--Color",
  "value": "#795600",
  "var": "var(--pf-c-form__helper-text--m-warning--Color)"
};
exports["default"] = exports.c_form__helper_text_m_warning_Color;
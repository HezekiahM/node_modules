"use strict";
exports.__esModule = true;
exports.c_toolbar_PaddingTop = {
  "name": "--pf-c-toolbar--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-toolbar--PaddingTop)"
};
exports["default"] = exports.c_toolbar_PaddingTop;
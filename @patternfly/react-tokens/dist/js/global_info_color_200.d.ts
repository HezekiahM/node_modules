export const global_info_color_200: {
  "name": "--pf-global--info-color--200",
  "value": "#002952",
  "var": "var(--pf-global--info-color--200)"
};
export default global_info_color_200;
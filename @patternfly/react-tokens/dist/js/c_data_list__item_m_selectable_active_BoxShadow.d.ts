export const c_data_list__item_m_selectable_active_BoxShadow: {
  "name": "--pf-c-data-list__item--m-selectable--active--BoxShadow",
  "value": "0 -0.125rem 0.25rem -0.0625rem rgba(3, 3, 3, 0.16), 0 0.125rem 0.25rem -0.0625rem rgba(3, 3, 3, 0.16)",
  "var": "var(--pf-c-data-list__item--m-selectable--active--BoxShadow)"
};
export default c_data_list__item_m_selectable_active_BoxShadow;
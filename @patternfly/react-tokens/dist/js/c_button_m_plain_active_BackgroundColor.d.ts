export const c_button_m_plain_active_BackgroundColor: {
  "name": "--pf-c-button--m-plain--active--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-button--m-plain--active--BackgroundColor)"
};
export default c_button_m_plain_active_BackgroundColor;
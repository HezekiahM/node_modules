"use strict";
exports.__esModule = true;
exports.chart_voronoi_labels_Fill = {
  "name": "--pf-chart-voronoi--labels--Fill",
  "value": "#f0f0f0",
  "var": "var(--pf-chart-voronoi--labels--Fill)"
};
exports["default"] = exports.chart_voronoi_labels_Fill;
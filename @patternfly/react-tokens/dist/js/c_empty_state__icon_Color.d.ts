export const c_empty_state__icon_Color: {
  "name": "--pf-c-empty-state__icon--Color",
  "value": "#6a6e73",
  "var": "var(--pf-c-empty-state__icon--Color)"
};
export default c_empty_state__icon_Color;
"use strict";
exports.__esModule = true;
exports.c_simple_list__title_FontSize = {
  "name": "--pf-c-simple-list__title--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-simple-list__title--FontSize)"
};
exports["default"] = exports.c_simple_list__title_FontSize;
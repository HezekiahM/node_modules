"use strict";
exports.__esModule = true;
exports.c_card_m_flat_BorderColor = {
  "name": "--pf-c-card--m-flat--BorderColor",
  "value": "#d2d2d2",
  "var": "var(--pf-c-card--m-flat--BorderColor)"
};
exports["default"] = exports.c_card_m_flat_BorderColor;
"use strict";
exports.__esModule = true;
exports.c_badge_m_read_Color = {
  "name": "--pf-c-badge--m-read--Color",
  "value": "#151515",
  "var": "var(--pf-c-badge--m-read--Color)"
};
exports["default"] = exports.c_badge_m_read_Color;
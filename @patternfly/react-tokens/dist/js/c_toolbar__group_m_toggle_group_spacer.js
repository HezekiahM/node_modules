"use strict";
exports.__esModule = true;
exports.c_toolbar__group_m_toggle_group_spacer = {
  "name": "--pf-c-toolbar__group--m-toggle-group--spacer",
  "value": "0.5rem",
  "var": "var(--pf-c-toolbar__group--m-toggle-group--spacer)"
};
exports["default"] = exports.c_toolbar__group_m_toggle_group_spacer;
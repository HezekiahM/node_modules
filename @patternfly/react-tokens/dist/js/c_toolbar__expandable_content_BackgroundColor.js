"use strict";
exports.__esModule = true;
exports.c_toolbar__expandable_content_BackgroundColor = {
  "name": "--pf-c-toolbar__expandable-content--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-toolbar__expandable-content--BackgroundColor)"
};
exports["default"] = exports.c_toolbar__expandable_content_BackgroundColor;
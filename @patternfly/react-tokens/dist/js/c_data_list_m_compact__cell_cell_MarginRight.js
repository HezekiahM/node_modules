"use strict";
exports.__esModule = true;
exports.c_data_list_m_compact__cell_cell_MarginRight = {
  "name": "--pf-c-data-list--m-compact__cell--cell--MarginRight",
  "value": "1rem",
  "var": "var(--pf-c-data-list--m-compact__cell--cell--MarginRight)"
};
exports["default"] = exports.c_data_list_m_compact__cell_cell_MarginRight;
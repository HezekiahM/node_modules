export const c_popover__content_PaddingLeft: {
  "name": "--pf-c-popover__content--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-popover__content--PaddingLeft)"
};
export default c_popover__content_PaddingLeft;
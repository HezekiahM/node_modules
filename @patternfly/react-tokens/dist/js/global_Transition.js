"use strict";
exports.__esModule = true;
exports.global_Transition = {
  "name": "--pf-global--Transition",
  "value": "all 250ms cubic-bezier(0.42, 0, 0.58, 1)",
  "var": "var(--pf-global--Transition)"
};
exports["default"] = exports.global_Transition;
export const c_chip_group_m_category_PaddingBottom: {
  "name": "--pf-c-chip-group--m-category--PaddingBottom",
  "value": "0.25rem",
  "var": "var(--pf-c-chip-group--m-category--PaddingBottom)"
};
export default c_chip_group_m_category_PaddingBottom;
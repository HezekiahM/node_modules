"use strict";
exports.__esModule = true;
exports.c_tile_PaddingRight = {
  "name": "--pf-c-tile--PaddingRight",
  "value": "1.5rem",
  "var": "var(--pf-c-tile--PaddingRight)"
};
exports["default"] = exports.c_tile_PaddingRight;
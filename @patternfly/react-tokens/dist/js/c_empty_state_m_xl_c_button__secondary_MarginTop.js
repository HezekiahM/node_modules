"use strict";
exports.__esModule = true;
exports.c_empty_state_m_xl_c_button__secondary_MarginTop = {
  "name": "--pf-c-empty-state--m-xl--c-button__secondary--MarginTop",
  "value": "1rem",
  "var": "var(--pf-c-empty-state--m-xl--c-button__secondary--MarginTop)"
};
exports["default"] = exports.c_empty_state_m_xl_c_button__secondary_MarginTop;
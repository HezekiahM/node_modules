"use strict";
exports.__esModule = true;
exports.c_check__label_FontSize = {
  "name": "--pf-c-check__label--FontSize",
  "value": "1rem",
  "var": "var(--pf-c-check__label--FontSize)"
};
exports["default"] = exports.c_check__label_FontSize;
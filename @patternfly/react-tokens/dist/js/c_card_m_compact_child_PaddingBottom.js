"use strict";
exports.__esModule = true;
exports.c_card_m_compact_child_PaddingBottom = {
  "name": "--pf-c-card--m-compact--child--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-card--m-compact--child--PaddingBottom)"
};
exports["default"] = exports.c_card_m_compact_child_PaddingBottom;
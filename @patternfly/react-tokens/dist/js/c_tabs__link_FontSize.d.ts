export const c_tabs__link_FontSize: {
  "name": "--pf-c-tabs__link--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-tabs__link--FontSize)"
};
export default c_tabs__link_FontSize;
export const c_alert_m_inline_BackgroundColor: {
  "name": "--pf-c-alert--m-inline--BackgroundColor",
  "value": "#e7f1fa",
  "var": "var(--pf-c-alert--m-inline--BackgroundColor)"
};
export default c_alert_m_inline_BackgroundColor;
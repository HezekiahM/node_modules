"use strict";
exports.__esModule = true;
exports.c_button_active_after_BorderWidth = {
  "name": "--pf-c-button--active--after--BorderWidth",
  "value": "2px",
  "var": "var(--pf-c-button--active--after--BorderWidth)"
};
exports["default"] = exports.c_button_active_after_BorderWidth;
"use strict";
exports.__esModule = true;
exports.chart_donut_threshold_warning_Color = {
  "name": "--pf-chart-donut--threshold--warning--Color",
  "value": "#f0ab00",
  "var": "var(--pf-chart-donut--threshold--warning--Color)"
};
exports["default"] = exports.chart_donut_threshold_warning_Color;
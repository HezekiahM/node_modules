"use strict";
exports.__esModule = true;
exports.c_alert__title_FontWeight = {
  "name": "--pf-c-alert__title--FontWeight",
  "value": "400",
  "var": "var(--pf-c-alert__title--FontWeight)"
};
exports["default"] = exports.c_alert__title_FontWeight;
export const c_table__sort_MinWidth: {
  "name": "--pf-c-table__sort--MinWidth",
  "value": "calc(6ch + 1rem + 1rem + 1rem)",
  "var": "var(--pf-c-table__sort--MinWidth)"
};
export default c_table__sort_MinWidth;
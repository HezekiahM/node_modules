"use strict";
exports.__esModule = true;
exports.c_page__header_brand_xl_PaddingLeft = {
  "name": "--pf-c-page__header-brand--xl--PaddingLeft",
  "value": "1.5rem",
  "var": "var(--pf-c-page__header-brand--xl--PaddingLeft)"
};
exports["default"] = exports.c_page__header_brand_xl_PaddingLeft;
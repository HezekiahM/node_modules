"use strict";
exports.__esModule = true;
exports.c_hint__footer_child_MarginRight = {
  "name": "--pf-c-hint__footer--child--MarginRight",
  "value": "1rem",
  "var": "var(--pf-c-hint__footer--child--MarginRight)"
};
exports["default"] = exports.c_hint__footer_child_MarginRight;
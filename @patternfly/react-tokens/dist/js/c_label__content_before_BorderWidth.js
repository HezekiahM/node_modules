"use strict";
exports.__esModule = true;
exports.c_label__content_before_BorderWidth = {
  "name": "--pf-c-label__content--before--BorderWidth",
  "value": "1px",
  "var": "var(--pf-c-label__content--before--BorderWidth)"
};
exports["default"] = exports.c_label__content_before_BorderWidth;
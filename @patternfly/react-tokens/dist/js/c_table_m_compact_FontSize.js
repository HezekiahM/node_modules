"use strict";
exports.__esModule = true;
exports.c_table_m_compact_FontSize = {
  "name": "--pf-c-table--m-compact--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-table--m-compact--FontSize)"
};
exports["default"] = exports.c_table_m_compact_FontSize;
"use strict";
exports.__esModule = true;
exports.c_pagination_m_bottom__nav_control_c_button_md_PaddingRight = {
  "name": "--pf-c-pagination--m-bottom__nav-control--c-button--md--PaddingRight",
  "value": "0.5rem",
  "var": "var(--pf-c-pagination--m-bottom__nav-control--c-button--md--PaddingRight)"
};
exports["default"] = exports.c_pagination_m_bottom__nav_control_c_button_md_PaddingRight;
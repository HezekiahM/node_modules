"use strict";
exports.__esModule = true;
exports.c_toolbar_c_divider_m_vertical_spacer = {
  "name": "--pf-c-toolbar--c-divider--m-vertical--spacer",
  "value": "1rem",
  "var": "var(--pf-c-toolbar--c-divider--m-vertical--spacer)"
};
exports["default"] = exports.c_toolbar_c_divider_m_vertical_spacer;
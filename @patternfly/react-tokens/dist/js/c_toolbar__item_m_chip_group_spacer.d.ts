export const c_toolbar__item_m_chip_group_spacer: {
  "name": "--pf-c-toolbar__item--m-chip-group--spacer",
  "value": "0.5rem",
  "var": "var(--pf-c-toolbar__item--m-chip-group--spacer)"
};
export default c_toolbar__item_m_chip_group_spacer;
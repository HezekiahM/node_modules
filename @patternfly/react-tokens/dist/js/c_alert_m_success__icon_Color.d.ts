export const c_alert_m_success__icon_Color: {
  "name": "--pf-c-alert--m-success__icon--Color",
  "value": "#3e8635",
  "var": "var(--pf-c-alert--m-success__icon--Color)"
};
export default c_alert_m_success__icon_Color;
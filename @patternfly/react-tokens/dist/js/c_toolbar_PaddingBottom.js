"use strict";
exports.__esModule = true;
exports.c_toolbar_PaddingBottom = {
  "name": "--pf-c-toolbar--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-toolbar--PaddingBottom)"
};
exports["default"] = exports.c_toolbar_PaddingBottom;
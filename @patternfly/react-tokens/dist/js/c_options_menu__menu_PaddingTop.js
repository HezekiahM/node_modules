"use strict";
exports.__esModule = true;
exports.c_options_menu__menu_PaddingTop = {
  "name": "--pf-c-options-menu__menu--PaddingTop",
  "value": "0.5rem",
  "var": "var(--pf-c-options-menu__menu--PaddingTop)"
};
exports["default"] = exports.c_options_menu__menu_PaddingTop;
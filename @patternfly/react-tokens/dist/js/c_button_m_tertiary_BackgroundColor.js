"use strict";
exports.__esModule = true;
exports.c_button_m_tertiary_BackgroundColor = {
  "name": "--pf-c-button--m-tertiary--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-button--m-tertiary--BackgroundColor)"
};
exports["default"] = exports.c_button_m_tertiary_BackgroundColor;
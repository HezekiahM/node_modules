"use strict";
exports.__esModule = true;
exports.c_wizard__close_FontSize = {
  "name": "--pf-c-wizard__close--FontSize",
  "value": "1.25rem",
  "var": "var(--pf-c-wizard__close--FontSize)"
};
exports["default"] = exports.c_wizard__close_FontSize;
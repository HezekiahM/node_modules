"use strict";
exports.__esModule = true;
exports.c_nav__section__link_FontSize = {
  "name": "--pf-c-nav__section__link--FontSize",
  "value": "1rem",
  "var": "var(--pf-c-nav__section__link--FontSize)"
};
exports["default"] = exports.c_nav__section__link_FontSize;
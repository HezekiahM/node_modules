"use strict";
exports.__esModule = true;
exports.chart_voronoi_data_stroke_Color = {
  "name": "--pf-chart-voronoi--data--stroke--Color",
  "value": "transparent",
  "var": "var(--pf-chart-voronoi--data--stroke--Color)"
};
exports["default"] = exports.chart_voronoi_data_stroke_Color;
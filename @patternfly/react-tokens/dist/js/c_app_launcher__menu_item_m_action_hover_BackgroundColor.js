"use strict";
exports.__esModule = true;
exports.c_app_launcher__menu_item_m_action_hover_BackgroundColor = {
  "name": "--pf-c-app-launcher__menu-item--m-action--hover--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-app-launcher__menu-item--m-action--hover--BackgroundColor)"
};
exports["default"] = exports.c_app_launcher__menu_item_m_action_hover_BackgroundColor;
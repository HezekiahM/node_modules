"use strict";
exports.__esModule = true;
exports.global_ZIndex_2xl = {
  "name": "--pf-global--ZIndex--2xl",
  "value": "600",
  "var": "var(--pf-global--ZIndex--2xl)"
};
exports["default"] = exports.global_ZIndex_2xl;
"use strict";
exports.__esModule = true;
exports.c_dropdown__menu_item_disabled_BackgroundColor = {
  "name": "--pf-c-dropdown__menu-item--disabled--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-dropdown__menu-item--disabled--BackgroundColor)"
};
exports["default"] = exports.c_dropdown__menu_item_disabled_BackgroundColor;
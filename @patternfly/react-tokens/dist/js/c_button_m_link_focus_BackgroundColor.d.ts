export const c_button_m_link_focus_BackgroundColor: {
  "name": "--pf-c-button--m-link--focus--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-button--m-link--focus--BackgroundColor)"
};
export default c_button_m_link_focus_BackgroundColor;
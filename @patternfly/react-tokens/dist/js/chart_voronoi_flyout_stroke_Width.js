"use strict";
exports.__esModule = true;
exports.chart_voronoi_flyout_stroke_Width = {
  "name": "--pf-chart-voronoi--flyout--stroke--Width",
  "value": 1,
  "var": "var(--pf-chart-voronoi--flyout--stroke--Width)"
};
exports["default"] = exports.chart_voronoi_flyout_stroke_Width;
"use strict";
exports.__esModule = true;
exports.chart_errorbar_data_stroke_Color = {
  "name": "--pf-chart-errorbar--data-stroke--Color",
  "value": "#151515",
  "var": "var(--pf-chart-errorbar--data-stroke--Color)"
};
exports["default"] = exports.chart_errorbar_data_stroke_Color;
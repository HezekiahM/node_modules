"use strict";
exports.__esModule = true;
exports.c_tile_m_display_lg__header_m_stacked__icon_FontSize = {
  "name": "--pf-c-tile--m-display-lg__header--m-stacked__icon--FontSize",
  "value": "3.375rem",
  "var": "var(--pf-c-tile--m-display-lg__header--m-stacked__icon--FontSize)"
};
exports["default"] = exports.c_tile_m_display_lg__header_m_stacked__icon_FontSize;
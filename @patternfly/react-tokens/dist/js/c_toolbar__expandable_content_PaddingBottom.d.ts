export const c_toolbar__expandable_content_PaddingBottom: {
  "name": "--pf-c-toolbar__expandable-content--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-toolbar__expandable-content--PaddingBottom)"
};
export default c_toolbar__expandable_content_PaddingBottom;
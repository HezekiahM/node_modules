"use strict";
exports.__esModule = true;
exports.c_alert__action_MarginTop = {
  "name": "--pf-c-alert__action--MarginTop",
  "value": "calc(0.375rem * -1)",
  "var": "var(--pf-c-alert__action--MarginTop)"
};
exports["default"] = exports.c_alert__action_MarginTop;
"use strict";
exports.__esModule = true;
exports.c_alert_group__item_MarginTop = {
  "name": "--pf-c-alert-group__item--MarginTop",
  "value": "0.5rem",
  "var": "var(--pf-c-alert-group__item--MarginTop)"
};
exports["default"] = exports.c_alert_group__item_MarginTop;
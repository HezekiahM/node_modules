"use strict";
exports.__esModule = true;
exports.c_search_input__text_input_PaddingTop = {
  "name": "--pf-c-search-input__text-input--PaddingTop",
  "value": "0.375rem",
  "var": "var(--pf-c-search-input__text-input--PaddingTop)"
};
exports["default"] = exports.c_search_input__text_input_PaddingTop;
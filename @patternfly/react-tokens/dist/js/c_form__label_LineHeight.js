"use strict";
exports.__esModule = true;
exports.c_form__label_LineHeight = {
  "name": "--pf-c-form__label--LineHeight",
  "value": "1.3",
  "var": "var(--pf-c-form__label--LineHeight)"
};
exports["default"] = exports.c_form__label_LineHeight;
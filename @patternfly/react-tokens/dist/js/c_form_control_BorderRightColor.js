"use strict";
exports.__esModule = true;
exports.c_form_control_BorderRightColor = {
  "name": "--pf-c-form-control--BorderRightColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-form-control--BorderRightColor)"
};
exports["default"] = exports.c_form_control_BorderRightColor;
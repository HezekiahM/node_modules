"use strict";
exports.__esModule = true;
exports.global_icon_FontSize_sm = {
  "name": "--pf-global--icon--FontSize--sm",
  "value": "0.625rem",
  "var": "var(--pf-global--icon--FontSize--sm)"
};
exports["default"] = exports.global_icon_FontSize_sm;
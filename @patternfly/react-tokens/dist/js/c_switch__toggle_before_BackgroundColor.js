"use strict";
exports.__esModule = true;
exports.c_switch__toggle_before_BackgroundColor = {
  "name": "--pf-c-switch__toggle--before--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-switch__toggle--before--BackgroundColor)"
};
exports["default"] = exports.c_switch__toggle_before_BackgroundColor;
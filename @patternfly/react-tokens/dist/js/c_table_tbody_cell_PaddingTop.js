"use strict";
exports.__esModule = true;
exports.c_table_tbody_cell_PaddingTop = {
  "name": "--pf-c-table--tbody--cell--PaddingTop",
  "value": "1.5rem",
  "var": "var(--pf-c-table--tbody--cell--PaddingTop)"
};
exports["default"] = exports.c_table_tbody_cell_PaddingTop;
"use strict";
exports.__esModule = true;
exports.c_accordion__toggle_icon_Transition = {
  "name": "--pf-c-accordion__toggle-icon--Transition",
  "value": ".2s ease-in 0s",
  "var": "var(--pf-c-accordion__toggle-icon--Transition)"
};
exports["default"] = exports.c_accordion__toggle_icon_Transition;
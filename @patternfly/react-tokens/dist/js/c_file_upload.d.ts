export const c_file_upload: {
  ".pf-c-file-upload": {
    "c_file_upload_m_loading__file_details_before_BackgroundColor": {
      "name": "--pf-c-file-upload--m-loading__file-details--before--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_file_upload_m_loading__file_details_before_Left": {
      "name": "--pf-c-file-upload--m-loading__file-details--before--Left",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_file_upload_m_loading__file_details_before_Right": {
      "name": "--pf-c-file-upload--m-loading__file-details--before--Right",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_file_upload_m_loading__file_details_before_Bottom": {
      "name": "--pf-c-file-upload--m-loading__file-details--before--Bottom",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_file_upload_m_drag_hover_before_BorderWidth": {
      "name": "--pf-c-file-upload--m-drag-hover--before--BorderWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_file_upload_m_drag_hover_before_BorderColor": {
      "name": "--pf-c-file-upload--m-drag-hover--before--BorderColor",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_file_upload_m_drag_hover_before_ZIndex": {
      "name": "--pf-c-file-upload--m-drag-hover--before--ZIndex",
      "value": "100",
      "values": [
        "--pf-global--ZIndex--xs",
        "$pf-global--ZIndex--xs",
        "100"
      ]
    },
    "c_file_upload_m_drag_hover_after_BackgroundColor": {
      "name": "--pf-c-file-upload--m-drag-hover--after--BackgroundColor",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_file_upload_m_drag_hover_after_Opacity": {
      "name": "--pf-c-file-upload--m-drag-hover--after--Opacity",
      "value": ".1"
    },
    "c_file_upload__file_details__c_form_control_MinHeight": {
      "name": "--pf-c-file-upload__file-details__c-form-control--MinHeight",
      "value": "calc(4rem * 2)",
      "values": [
        "calc(--pf-global--spacer--3xl * 2)",
        "calc($pf-global--spacer--3xl * 2)",
        "calc(pf-size-prem(64px) * 2)",
        "calc(4rem * 2)"
      ]
    },
    "c_file_upload__file_select__c_button_m_control_OutlineOffset": {
      "name": "--pf-c-file-upload__file-select__c-button--m-control--OutlineOffset",
      "value": "calc(-1 * 0.25rem)",
      "values": [
        "calc(-1 * --pf-global--spacer--xs)",
        "calc(-1 * $pf-global--spacer--xs)",
        "calc(-1 * pf-size-prem(4px))",
        "calc(-1 * 0.25rem)"
      ]
    }
  }
};
export default c_file_upload;
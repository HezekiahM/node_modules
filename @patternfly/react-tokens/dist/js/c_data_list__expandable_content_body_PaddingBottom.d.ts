export const c_data_list__expandable_content_body_PaddingBottom: {
  "name": "--pf-c-data-list__expandable-content-body--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-data-list__expandable-content-body--PaddingBottom)"
};
export default c_data_list__expandable_content_body_PaddingBottom;
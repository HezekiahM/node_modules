"use strict";
exports.__esModule = true;
exports.chart_color_purple_100 = {
  "name": "--pf-chart-color-purple-100",
  "value": "#b2b0ea",
  "var": "var(--pf-chart-color-purple-100)"
};
exports["default"] = exports.chart_color_purple_100;
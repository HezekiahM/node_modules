"use strict";
exports.__esModule = true;
exports.global_default_color_200 = {
  "name": "--pf-global--default-color--200",
  "value": "#009596",
  "var": "var(--pf-global--default-color--200)"
};
exports["default"] = exports.global_default_color_200;
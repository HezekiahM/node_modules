export const c_tooltip__content_PaddingLeft: {
  "name": "--pf-c-tooltip__content--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-tooltip__content--PaddingLeft)"
};
export default c_tooltip__content_PaddingLeft;
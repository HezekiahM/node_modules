"use strict";
exports.__esModule = true;
exports.c_input_group_BackgroundColor = {
  "name": "--pf-c-input-group--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-input-group--BackgroundColor)"
};
exports["default"] = exports.c_input_group_BackgroundColor;
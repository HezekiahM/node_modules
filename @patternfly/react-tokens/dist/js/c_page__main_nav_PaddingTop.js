"use strict";
exports.__esModule = true;
exports.c_page__main_nav_PaddingTop = {
  "name": "--pf-c-page__main-nav--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-page__main-nav--PaddingTop)"
};
exports["default"] = exports.c_page__main_nav_PaddingTop;
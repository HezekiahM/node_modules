export const c_table__expandable_row_after_Top: {
  "name": "--pf-c-table__expandable-row--after--Top",
  "value": "calc(1px * -1)",
  "var": "var(--pf-c-table__expandable-row--after--Top)"
};
export default c_table__expandable_row_after_Top;
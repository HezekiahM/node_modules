export const c_table_cell_Width: {
  "name": "--pf-c-table--cell--Width",
  "value": "100%",
  "var": "var(--pf-c-table--cell--Width)"
};
export default c_table_cell_Width;
"use strict";
exports.__esModule = true;
exports.c_about_modal_box__content_sm_MarginRight = {
  "name": "--pf-c-about-modal-box__content--sm--MarginRight",
  "value": "4rem",
  "var": "var(--pf-c-about-modal-box__content--sm--MarginRight)"
};
exports["default"] = exports.c_about_modal_box__content_sm_MarginRight;
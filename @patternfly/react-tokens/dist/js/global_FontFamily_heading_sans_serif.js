"use strict";
exports.__esModule = true;
exports.global_FontFamily_heading_sans_serif = {
  "name": "--pf-global--FontFamily--heading--sans-serif",
  "value": "\"RedHatText\", \"Overpass\", overpass, helvetica, arial, sans-serif",
  "var": "var(--pf-global--FontFamily--heading--sans-serif)"
};
exports["default"] = exports.global_FontFamily_heading_sans_serif;
export const c_pagination: {
  ".pf-c-pagination": {
    "c_pagination_child_MarginRight": {
      "name": "--pf-c-pagination--child--MarginRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_pagination_m_bottom_child_MarginRight": {
      "name": "--pf-c-pagination--m-bottom--child--MarginRight",
      "value": "0"
    },
    "c_pagination_m_bottom_child_md_MarginRight": {
      "name": "--pf-c-pagination--m-bottom--child--md--MarginRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_pagination_m_compact_child_MarginRight": {
      "name": "--pf-c-pagination--m-compact--child--MarginRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_pagination_c_options_menu__toggle_FontSize": {
      "name": "--pf-c-pagination--c-options-menu__toggle--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_pagination__nav_control_c_button_PaddingRight": {
      "name": "--pf-c-pagination__nav-control--c-button--PaddingRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_pagination__nav_control_c_button_PaddingLeft": {
      "name": "--pf-c-pagination__nav-control--c-button--PaddingLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_pagination__nav_control_c_button_FontSize": {
      "name": "--pf-c-pagination__nav-control--c-button--FontSize",
      "value": "1rem",
      "values": [
        "--pf-global--FontSize--md",
        "$pf-global--FontSize--md",
        "pf-font-prem(16px)",
        "1rem"
      ]
    },
    "c_pagination_m_bottom__nav_control_c_button_OutlineOffset": {
      "name": "--pf-c-pagination--m-bottom__nav-control--c-button--OutlineOffset",
      "value": "calc(0.25rem * -1)",
      "values": [
        "calc(--pf-global--spacer--xs * -1)",
        "calc($pf-global--spacer--xs * -1)",
        "calc(pf-size-prem(4px) * -1)",
        "calc(0.25rem * -1)"
      ]
    },
    "c_pagination_m_bottom__nav_control_c_button_PaddingTop": {
      "name": "--pf-c-pagination--m-bottom__nav-control--c-button--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_pagination_m_bottom__nav_control_c_button_PaddingBottom": {
      "name": "--pf-c-pagination--m-bottom__nav-control--c-button--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_pagination_m_bottom__nav_control_c_button_PaddingRight": {
      "name": "--pf-c-pagination--m-bottom__nav-control--c-button--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_pagination_m_bottom__nav_control_c_button_md_PaddingTop": {
      "name": "--pf-c-pagination--m-bottom__nav-control--c-button--md--PaddingTop",
      "value": "0.375rem",
      "values": [
        "--pf-global--spacer--form-element",
        "$pf-global--spacer--form-element",
        "pf-size-prem(6px)",
        "0.375rem"
      ]
    },
    "c_pagination_m_bottom__nav_control_c_button_md_PaddingRight": {
      "name": "--pf-c-pagination--m-bottom__nav-control--c-button--md--PaddingRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_pagination_m_bottom__nav_control_c_button_md_PaddingBottom": {
      "name": "--pf-c-pagination--m-bottom__nav-control--c-button--md--PaddingBottom",
      "value": "0.375rem",
      "values": [
        "--pf-global--spacer--form-element",
        "$pf-global--spacer--form-element",
        "pf-size-prem(6px)",
        "0.375rem"
      ]
    },
    "c_pagination_m_bottom__nav_control_c_button_md_PaddingLeft": {
      "name": "--pf-c-pagination--m-bottom__nav-control--c-button--md--PaddingLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_pagination_m_compact__nav_control_nav_control_MarginLeft": {
      "name": "--pf-c-pagination--m-compact__nav-control--nav-control--MarginLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_pagination__nav_page_select_FontSize": {
      "name": "--pf-c-pagination__nav-page-select--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_pagination__nav_page_select_PaddingLeft": {
      "name": "--pf-c-pagination__nav-page-select--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_pagination__nav_page_select_PaddingRight": {
      "name": "--pf-c-pagination__nav-page-select--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_pagination__nav_page_select_child_MarginRight": {
      "name": "--pf-c-pagination__nav-page-select--child--MarginRight",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_pagination__nav_page_select_c_form_control_width_base": {
      "name": "--pf-c-pagination__nav-page-select--c-form-control--width-base",
      "value": "3.5ch"
    },
    "c_pagination__nav_page_select_c_form_control_width_chars": {
      "name": "--pf-c-pagination__nav-page-select--c-form-control--width-chars",
      "value": "2"
    },
    "c_pagination__nav_page_select_c_form_control_Width": {
      "name": "--pf-c-pagination__nav-page-select--c-form-control--Width",
      "value": "calc(3.5ch + (2 * 1ch))",
      "values": [
        "calc(--pf-c-pagination__nav-page-select--c-form-control--width-base + (--pf-c-pagination__nav-page-select--c-form-control--width-chars * 1ch))",
        "calc(3.5ch + (2 * 1ch))"
      ]
    },
    "c_pagination_m_bottom_BackgroundColor": {
      "name": "--pf-c-pagination--m-bottom--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_pagination_m_bottom_BoxShadow": {
      "name": "--pf-c-pagination--m-bottom--BoxShadow",
      "value": "0 -0.125rem 0.25rem -0.0625rem rgba(3, 3, 3, 0.16)",
      "values": [
        "--pf-global--BoxShadow--sm-top",
        "$pf-global--BoxShadow--sm-top",
        "0 pf-size-prem(-2px) pf-size-prem(4px) pf-size-prem(-1px) rgba($pf-color-black-1000, .16)",
        "0 pf-size-prem(-2px) pf-size-prem(4px) pf-size-prem(-1px) rgba(#030303, .16)",
        "0 -0.125rem 0.25rem -0.0625rem rgba(3, 3, 3, 0.16)"
      ]
    },
    "c_pagination_m_bottom_md_PaddingTop": {
      "name": "--pf-c-pagination--m-bottom--md--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_pagination_m_bottom_md_PaddingRight": {
      "name": "--pf-c-pagination--m-bottom--md--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_pagination_m_bottom_md_PaddingBottom": {
      "name": "--pf-c-pagination--m-bottom--md--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_pagination_m_bottom_md_PaddingLeft": {
      "name": "--pf-c-pagination--m-bottom--md--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_pagination_m_bottom_xl_PaddingRight": {
      "name": "--pf-c-pagination--m-bottom--xl--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_pagination_m_bottom_xl_PaddingLeft": {
      "name": "--pf-c-pagination--m-bottom--xl--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    }
  },
  ".pf-c-pagination.pf-m-bottom": {
    "c_pagination_child_MarginRight": {
      "name": "--pf-c-pagination--child--MarginRight",
      "value": "0",
      "values": [
        "--pf-c-pagination--m-bottom--child--MarginRight",
        "0"
      ]
    },
    "c_pagination__nav_control_c_button_PaddingRight": {
      "name": "--pf-c-pagination__nav-control--c-button--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-c-pagination--m-bottom__nav-control--c-button--PaddingRight",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_pagination__nav_control_c_button_PaddingLeft": {
      "name": "--pf-c-pagination__nav-control--c-button--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-c-pagination--m-bottom__nav-control--c-button--PaddingRight",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    }
  },
  ".pf-c-pagination.pf-m-bottom .pf-c-pagination__nav-control .pf-c-button": {
    "c_button_PaddingTop": {
      "name": "--pf-c-button--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-c-pagination--m-bottom__nav-control--c-button--PaddingTop",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_button_PaddingBottom": {
      "name": "--pf-c-button--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-c-pagination--m-bottom__nav-control--c-button--PaddingBottom",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    }
  },
  ".pf-c-pagination.pf-m-bottom.pf-m-static": {
    "c_pagination_m_bottom_MarginTop": {
      "name": "--pf-c-pagination--m-bottom--MarginTop",
      "value": "0"
    },
    "c_pagination_m_bottom_BorderTopWidth": {
      "name": "--pf-c-pagination--m-bottom--BorderTopWidth",
      "value": "0"
    }
  },
  ".pf-c-pagination.pf-m-compact": {
    "c_pagination_child_MarginRight": {
      "name": "--pf-c-pagination--child--MarginRight",
      "value": "0.5rem",
      "values": [
        "--pf-c-pagination--m-compact--child--MarginRight",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  }
};
export default c_pagination;
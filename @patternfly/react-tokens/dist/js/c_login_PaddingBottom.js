"use strict";
exports.__esModule = true;
exports.c_login_PaddingBottom = {
  "name": "--pf-c-login--PaddingBottom",
  "value": "1.5rem",
  "var": "var(--pf-c-login--PaddingBottom)"
};
exports["default"] = exports.c_login_PaddingBottom;
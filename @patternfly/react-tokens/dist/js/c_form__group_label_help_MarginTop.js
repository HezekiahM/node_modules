"use strict";
exports.__esModule = true;
exports.c_form__group_label_help_MarginTop = {
  "name": "--pf-c-form__group-label-help--MarginTop",
  "value": "calc(0.25rem * -1)",
  "var": "var(--pf-c-form__group-label-help--MarginTop)"
};
exports["default"] = exports.c_form__group_label_help_MarginTop;
"use strict";
exports.__esModule = true;
exports.c_table = {
  ".pf-c-table": {
    "global_Color_100": {
      "name": "--pf-global--Color--100",
      "value": "#151515",
      "values": [
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "global_Color_200": {
      "name": "--pf-global--Color--200",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--dark-200",
        "$pf-global--Color--dark-200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "global_BorderColor_100": {
      "name": "--pf-global--BorderColor--100",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--dark-100",
        "$pf-global--BorderColor--dark-100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "global_primary_color_100": {
      "name": "--pf-global--primary-color--100",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--dark-100",
        "$pf-global--primary-color--dark-100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "global_link_Color": {
      "name": "--pf-global--link--Color",
      "value": "#06c",
      "values": [
        "--pf-global--link--Color--dark",
        "$pf-global--link--Color--dark",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "global_link_Color_hover": {
      "name": "--pf-global--link--Color--hover",
      "value": "#004080",
      "values": [
        "--pf-global--link--Color--dark--hover",
        "$pf-global--link--Color--dark--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "global_BackgroundColor_100": {
      "name": "--pf-global--BackgroundColor--100",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--light-100",
        "$pf-global--BackgroundColor--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_table_BackgroundColor": {
      "name": "--pf-c-table--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_table_BorderColor": {
      "name": "--pf-c-table--BorderColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_table_border_width_base": {
      "name": "--pf-c-table--border-width--base",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_table_caption_FontSize": {
      "name": "--pf-c-table-caption--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_table_caption_Color": {
      "name": "--pf-c-table-caption--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--200",
        "$pf-global--Color--200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_table_caption_PaddingTop": {
      "name": "--pf-c-table-caption--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_table_caption_PaddingRight": {
      "name": "--pf-c-table-caption--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_table_caption_PaddingBottom": {
      "name": "--pf-c-table-caption--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_table_caption_PaddingLeft": {
      "name": "--pf-c-table-caption--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_table_caption_xl_PaddingRight": {
      "name": "--pf-c-table-caption--xl--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_table_caption_xl_PaddingLeft": {
      "name": "--pf-c-table-caption--xl--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_table_thead_cell_FontSize": {
      "name": "--pf-c-table--thead--cell--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_table_thead_cell_FontWeight": {
      "name": "--pf-c-table--thead--cell--FontWeight",
      "value": "700",
      "values": [
        "--pf-global--FontWeight--bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    },
    "c_table_tbody_cell_PaddingTop": {
      "name": "--pf-c-table--tbody--cell--PaddingTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_table_tbody_cell_PaddingBottom": {
      "name": "--pf-c-table--tbody--cell--PaddingBottom",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_table_cell_FontSize": {
      "name": "--pf-c-table--cell--FontSize",
      "value": "1rem",
      "values": [
        "--pf-global--FontSize--md",
        "$pf-global--FontSize--md",
        "pf-font-prem(16px)",
        "1rem"
      ]
    },
    "c_table_cell_FontWeight": {
      "name": "--pf-c-table--cell--FontWeight",
      "value": "400",
      "values": [
        "--pf-global--FontWeight--normal",
        "$pf-global--FontWeight--normal",
        "400"
      ]
    },
    "c_table_cell_Color": {
      "name": "--pf-c-table--cell--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_table_cell_PaddingTop": {
      "name": "--pf-c-table--cell--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_table_cell_PaddingRight": {
      "name": "--pf-c-table--cell--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_table_cell_PaddingBottom": {
      "name": "--pf-c-table--cell--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_table_cell_PaddingLeft": {
      "name": "--pf-c-table--cell--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_table_cell_first_last_child_PaddingLeft": {
      "name": "--pf-c-table--cell--first-last-child--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_table_cell_first_last_child_PaddingRight": {
      "name": "--pf-c-table--cell--first-last-child--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_table_cell_first_last_child_xl_PaddingLeft": {
      "name": "--pf-c-table--cell--first-last-child--xl--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_table_cell_first_last_child_xl_PaddingRight": {
      "name": "--pf-c-table--cell--first-last-child--xl--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_table_cell_MinWidth": {
      "name": "--pf-c-table--cell--MinWidth",
      "value": "0"
    },
    "c_table_cell_MaxWidth": {
      "name": "--pf-c-table--cell--MaxWidth",
      "value": "none"
    },
    "c_table_cell_Width": {
      "name": "--pf-c-table--cell--Width",
      "value": "auto"
    },
    "c_table_cell_Overflow": {
      "name": "--pf-c-table--cell--Overflow",
      "value": "visible"
    },
    "c_table_cell_TextOverflow": {
      "name": "--pf-c-table--cell--TextOverflow",
      "value": "clip"
    },
    "c_table_cell_WhiteSpace": {
      "name": "--pf-c-table--cell--WhiteSpace",
      "value": "normal"
    },
    "c_table_cell_WordBreak": {
      "name": "--pf-c-table--cell--WordBreak",
      "value": "normal"
    },
    "c_table_m_truncate_cell_MaxWidth": {
      "name": "--pf-c-table--m-truncate--cell--MaxWidth",
      "value": "1px"
    },
    "c_table_m_truncate_cell_MinWidth": {
      "name": "--pf-c-table--m-truncate--cell--MinWidth",
      "value": "calc(5ch + 1rem + 1rem)",
      "values": [
        "calc(5ch + --pf-c-table--cell--PaddingRight + --pf-c-table--cell--PaddingLeft)",
        "calc(5ch + --pf-global--spacer--md + --pf-global--spacer--md)",
        "calc(5ch + $pf-global--spacer--md + $pf-global--spacer--md)",
        "calc(5ch + pf-size-prem(16px) + pf-size-prem(16px))",
        "calc(5ch + 1rem + 1rem)"
      ]
    },
    "c_table_cell_hidden_visible_Display": {
      "name": "--pf-c-table--cell--hidden-visible--Display",
      "value": "table-cell"
    },
    "c_table__toggle_c_button_MarginTop": {
      "name": "--pf-c-table__toggle--c-button--MarginTop",
      "value": "calc(0.375rem * -1)"
    },
    "c_table__toggle_c_button__toggle_icon_Rotate": {
      "name": "--pf-c-table__toggle--c-button__toggle-icon--Rotate",
      "value": "270deg"
    },
    "c_table__toggle_c_button__toggle_icon_Transition": {
      "name": "--pf-c-table__toggle--c-button__toggle-icon--Transition",
      "value": ".2s ease-in 0s"
    },
    "c_table__toggle_c_button_m_expanded__toggle_icon_Rotate": {
      "name": "--pf-c-table__toggle--c-button--m-expanded__toggle-icon--Rotate",
      "value": "360deg"
    },
    "c_table__button_BackgroundColor": {
      "name": "--pf-c-table__button--BackgroundColor",
      "value": "transparent"
    },
    "c_table__button_Color": {
      "name": "--pf-c-table__button--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_table__button_hover_Color": {
      "name": "--pf-c-table__button--hover--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_table__button_focus_Color": {
      "name": "--pf-c-table__button--focus--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_table__button_active_Color": {
      "name": "--pf-c-table__button--active--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_table__button_OutlineOffset": {
      "name": "--pf-c-table__button--OutlineOffset",
      "value": "calc(3px * -1)",
      "values": [
        "calc(--pf-global--BorderWidth--lg * -1)",
        "calc($pf-global--BorderWidth--lg * -1)",
        "calc(3px * -1)"
      ]
    },
    "c_table_m_compact__toggle_PaddingTop": {
      "name": "--pf-c-table--m-compact__toggle--PaddingTop",
      "value": "0"
    },
    "c_table_m_compact__toggle_PaddingBottom": {
      "name": "--pf-c-table--m-compact__toggle--PaddingBottom",
      "value": "0"
    },
    "c_table__check_input_MarginTop": {
      "name": "--pf-c-table__check--input--MarginTop",
      "value": "0.25rem"
    },
    "c_table__check_input_FontSize": {
      "name": "--pf-c-table__check--input--FontSize",
      "value": "1rem",
      "values": [
        "--pf-global--FontSize--md",
        "$pf-global--FontSize--md",
        "pf-font-prem(16px)",
        "1rem"
      ]
    },
    "c_table__action_PaddingTop": {
      "name": "--pf-c-table__action--PaddingTop",
      "value": "0"
    },
    "c_table__action_PaddingRight": {
      "name": "--pf-c-table__action--PaddingRight",
      "value": "0"
    },
    "c_table__action_PaddingBottom": {
      "name": "--pf-c-table__action--PaddingBottom",
      "value": "0"
    },
    "c_table__action_PaddingLeft": {
      "name": "--pf-c-table__action--PaddingLeft",
      "value": "0"
    },
    "c_table__inline_edit_action_PaddingTop": {
      "name": "--pf-c-table__inline-edit-action--PaddingTop",
      "value": "0"
    },
    "c_table__inline_edit_action_PaddingRight": {
      "name": "--pf-c-table__inline-edit-action--PaddingRight",
      "value": "0"
    },
    "c_table__inline_edit_action_PaddingBottom": {
      "name": "--pf-c-table__inline-edit-action--PaddingBottom",
      "value": "0"
    },
    "c_table__inline_edit_action_PaddingLeft": {
      "name": "--pf-c-table__inline-edit-action--PaddingLeft",
      "value": "0"
    },
    "c_table__expandable_row_Transition": {
      "name": "--pf-c-table__expandable-row--Transition",
      "value": "all 250ms cubic-bezier(.42, 0, .58, 1)",
      "values": [
        "--pf-global--Transition",
        "$pf-global--Transition",
        "all 250ms cubic-bezier(.42, 0, .58, 1)"
      ]
    },
    "c_table__expandable_row_MaxHeight": {
      "name": "--pf-c-table__expandable-row--MaxHeight",
      "value": "28.125rem"
    },
    "c_table__expandable_row_content_Transition": {
      "name": "--pf-c-table__expandable-row-content--Transition",
      "value": "all 250ms cubic-bezier(.42, 0, .58, 1)",
      "values": [
        "--pf-global--Transition",
        "$pf-global--Transition",
        "all 250ms cubic-bezier(.42, 0, .58, 1)"
      ]
    },
    "c_table__expandable_row_content_PaddingTop": {
      "name": "--pf-c-table__expandable-row-content--PaddingTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_table__expandable_row_content_PaddingBottom": {
      "name": "--pf-c-table__expandable-row-content--PaddingBottom",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_table__expandable_row_after_Top": {
      "name": "--pf-c-table__expandable-row--after--Top",
      "value": "calc(1px * -1)",
      "values": [
        "calc(--pf-c-table--border-width--base * -1)",
        "calc(--pf-global--BorderWidth--sm * -1)",
        "calc($pf-global--BorderWidth--sm * -1)",
        "calc(1px * -1)"
      ]
    },
    "c_table__expandable_row_after_Bottom": {
      "name": "--pf-c-table__expandable-row--after--Bottom",
      "value": "calc(1px * -1)",
      "values": [
        "calc(--pf-c-table--border-width--base * -1)",
        "calc(--pf-global--BorderWidth--sm * -1)",
        "calc($pf-global--BorderWidth--sm * -1)",
        "calc(1px * -1)"
      ]
    },
    "c_table__expandable_row_after_border_width_base": {
      "name": "--pf-c-table__expandable-row--after--border-width--base",
      "value": "3px",
      "values": [
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    },
    "c_table__expandable_row_after_BorderLeftWidth": {
      "name": "--pf-c-table__expandable-row--after--BorderLeftWidth",
      "value": "0"
    },
    "c_table__expandable_row_after_BorderColor": {
      "name": "--pf-c-table__expandable-row--after--BorderColor",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_table__icon_inline_MarginRight": {
      "name": "--pf-c-table__icon-inline--MarginRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_table__sort_MinWidth": {
      "name": "--pf-c-table__sort--MinWidth",
      "value": "calc(6ch + 1rem + 1rem + 1rem)",
      "values": [
        "calc(6ch + --pf-c-table--cell--PaddingRight + --pf-c-table--cell--PaddingLeft + --pf-c-table__sort-indicator--MarginLeft)",
        "calc(6ch + --pf-global--spacer--md + --pf-global--spacer--md + --pf-global--spacer--md)",
        "calc(6ch + $pf-global--spacer--md + $pf-global--spacer--md + $pf-global--spacer--md)",
        "calc(6ch + pf-size-prem(16px) + pf-size-prem(16px) + pf-size-prem(16px))",
        "calc(6ch + 1rem + 1rem + 1rem)"
      ]
    },
    "c_table__sort__button_PaddingTop": {
      "name": "--pf-c-table__sort__button--PaddingTop",
      "value": "0.375rem",
      "values": [
        "--pf-global--spacer--form-element",
        "$pf-global--spacer--form-element",
        "pf-size-prem(6px)",
        "0.375rem"
      ]
    },
    "c_table__sort__button_PaddingRight": {
      "name": "--pf-c-table__sort__button--PaddingRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_table__sort__button_PaddingBottom": {
      "name": "--pf-c-table__sort__button--PaddingBottom",
      "value": "0.375rem",
      "values": [
        "--pf-global--spacer--form-element",
        "$pf-global--spacer--form-element",
        "pf-size-prem(6px)",
        "0.375rem"
      ]
    },
    "c_table__sort__button_PaddingLeft": {
      "name": "--pf-c-table__sort__button--PaddingLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_table__sort__button_MarginTop": {
      "name": "--pf-c-table__sort__button--MarginTop",
      "value": "calc(0.375rem * -1)",
      "values": [
        "calc(--pf-c-table__sort__button--PaddingTop * -1)",
        "calc(--pf-global--spacer--form-element * -1)",
        "calc($pf-global--spacer--form-element * -1)",
        "calc(pf-size-prem(6px) * -1)",
        "calc(0.375rem * -1)"
      ]
    },
    "c_table__sort__button_MarginBottom": {
      "name": "--pf-c-table__sort__button--MarginBottom",
      "value": "calc(0.375rem * -1)",
      "values": [
        "calc(--pf-c-table__sort__button--PaddingBottom * -1)",
        "calc(--pf-global--spacer--form-element * -1)",
        "calc($pf-global--spacer--form-element * -1)",
        "calc(pf-size-prem(6px) * -1)",
        "calc(0.375rem * -1)"
      ]
    },
    "c_table__sort__button_MarginLeft": {
      "name": "--pf-c-table__sort__button--MarginLeft",
      "value": "calc(0.5rem * -1)",
      "values": [
        "calc(--pf-c-table__sort__button--PaddingLeft * -1)",
        "calc(--pf-global--spacer--sm * -1)",
        "calc($pf-global--spacer--sm * -1)",
        "calc(pf-size-prem(8px) * -1)",
        "calc(0.5rem * -1)"
      ]
    },
    "c_table__sort__button_Color": {
      "name": "--pf-c-table__sort__button--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_table__sort_m_selected__button_Color": {
      "name": "--pf-c-table__sort--m-selected__button--Color",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_table__sort_indicator_Color": {
      "name": "--pf-c-table__sort-indicator--Color",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--disabled-color--200",
        "$pf-global--disabled-color--200",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_table__sort_indicator_MarginLeft": {
      "name": "--pf-c-table__sort-indicator--MarginLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_table__sort_m_selected__sort_indicator_Color": {
      "name": "--pf-c-table__sort--m-selected__sort-indicator--Color",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_table__sort__button_hover__sort_indicator_Color": {
      "name": "--pf-c-table__sort__button--hover__sort-indicator--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_table__sort__button_active__sort_indicator_Color": {
      "name": "--pf-c-table__sort__button--active__sort-indicator--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_table__sort__button_focus__sort_indicator_Color": {
      "name": "--pf-c-table__sort__button--focus__sort-indicator--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_table__column_help_MarginLeft": {
      "name": "--pf-c-table__column-help--MarginLeft",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_table__column_help_TranslateY": {
      "name": "--pf-c-table__column-help--TranslateY",
      "value": "0.125rem"
    },
    "c_table__column_help_c_button_MarginTop": {
      "name": "--pf-c-table__column-help--c-button--MarginTop",
      "value": "calc(0.375rem * -1)",
      "values": [
        "calc(--pf-global--spacer--form-element * -1)",
        "calc($pf-global--spacer--form-element * -1)",
        "calc(pf-size-prem(6px) * -1)",
        "calc(0.375rem * -1)"
      ]
    },
    "c_table__column_help_c_button_MarginBottom": {
      "name": "--pf-c-table__column-help--c-button--MarginBottom",
      "value": "calc(0.375rem * -1)",
      "values": [
        "calc(--pf-global--spacer--form-element * -1)",
        "calc($pf-global--spacer--form-element * -1)",
        "calc(pf-size-prem(6px) * -1)",
        "calc(0.375rem * -1)"
      ]
    },
    "c_table__column_help_c_button_PaddingRight": {
      "name": "--pf-c-table__column-help--c-button--PaddingRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_table__column_help_c_button_PaddingLeft": {
      "name": "--pf-c-table__column-help--c-button--PaddingLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_table__compound_expansion_toggle__button_Color": {
      "name": "--pf-c-table__compound-expansion-toggle__button--Color",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_table__compound_expansion_toggle__button_hover_Color": {
      "name": "--pf-c-table__compound-expansion-toggle__button--hover--Color",
      "value": "#004080",
      "values": [
        "--pf-global--link--Color--hover",
        "$pf-global--link--Color--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "c_table__compound_expansion_toggle__button_focus_Color": {
      "name": "--pf-c-table__compound-expansion-toggle__button--focus--Color",
      "value": "#004080",
      "values": [
        "--pf-global--link--Color--hover",
        "$pf-global--link--Color--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "c_table__compound_expansion_toggle__button_active_Color": {
      "name": "--pf-c-table__compound-expansion-toggle__button--active--Color",
      "value": "#004080",
      "values": [
        "--pf-global--link--Color--hover",
        "$pf-global--link--Color--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "c_table__compound_expansion_toggle__button_before_border_width_base": {
      "name": "--pf-c-table__compound-expansion-toggle__button--before--border-width--base",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_table__compound_expansion_toggle__button_before_BorderColor": {
      "name": "--pf-c-table__compound-expansion-toggle__button--before--BorderColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_table__compound_expansion_toggle__button_before_BorderRightWidth": {
      "name": "--pf-c-table__compound-expansion-toggle__button--before--BorderRightWidth",
      "value": "0"
    },
    "c_table__compound_expansion_toggle__button_before_BorderLeftWidth": {
      "name": "--pf-c-table__compound-expansion-toggle__button--before--BorderLeftWidth",
      "value": "0"
    },
    "c_table__compound_expansion_toggle__button_before_Bottom": {
      "name": "--pf-c-table__compound-expansion-toggle__button--before--Bottom",
      "value": "calc(1px * -1)",
      "values": [
        "calc(--pf-c-table__compound-expansion-toggle__button--before--border-width--base * -1)",
        "calc(--pf-global--BorderWidth--sm * -1)",
        "calc($pf-global--BorderWidth--sm * -1)",
        "calc(1px * -1)"
      ]
    },
    "c_table__compound_expansion_toggle__button_before_Left": {
      "name": "--pf-c-table__compound-expansion-toggle__button--before--Left",
      "value": "calc(1px * -1)",
      "values": [
        "calc(--pf-c-table__compound-expansion-toggle__button--before--border-width--base * -1)",
        "calc(--pf-global--BorderWidth--sm * -1)",
        "calc($pf-global--BorderWidth--sm * -1)",
        "calc(1px * -1)"
      ]
    },
    "c_table__compound_expansion_toggle__button_after_border_width_base": {
      "name": "--pf-c-table__compound-expansion-toggle__button--after--border-width--base",
      "value": "3px",
      "values": [
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    },
    "c_table__compound_expansion_toggle__button_after_BorderColor": {
      "name": "--pf-c-table__compound-expansion-toggle__button--after--BorderColor",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_table__compound_expansion_toggle__button_after_BorderTopWidth": {
      "name": "--pf-c-table__compound-expansion-toggle__button--after--BorderTopWidth",
      "value": "0"
    },
    "c_table__compound_expansion_toggle__button_after_Top": {
      "name": "--pf-c-table__compound-expansion-toggle__button--after--Top",
      "value": "calc(1px * -1)",
      "values": [
        "calc(--pf-c-table__compound-expansion-toggle__button--before--border-width--base * -1)",
        "calc(--pf-global--BorderWidth--sm * -1)",
        "calc($pf-global--BorderWidth--sm * -1)",
        "calc(1px * -1)"
      ]
    },
    "c_table__compound_expansion_toggle__button_after_Left": {
      "name": "--pf-c-table__compound-expansion-toggle__button--after--Left",
      "value": "calc(1px * -1)",
      "values": [
        "calc(--pf-c-table__compound-expansion-toggle__button--before--border-width--base * -1)",
        "calc(--pf-global--BorderWidth--sm * -1)",
        "calc($pf-global--BorderWidth--sm * -1)",
        "calc(1px * -1)"
      ]
    },
    "c_table_m_compact_th_PaddingTop": {
      "name": "--pf-c-table--m-compact-th--PaddingTop",
      "value": "calc(0.5rem + 0.25rem)",
      "values": [
        "calc(--pf-global--spacer--sm + --pf-global--spacer--xs)",
        "calc($pf-global--spacer--sm + $pf-global--spacer--xs)",
        "calc(pf-size-prem(8px) + pf-size-prem(4px))",
        "calc(0.5rem + 0.25rem)"
      ]
    },
    "c_table_m_compact_th_PaddingBottom": {
      "name": "--pf-c-table--m-compact-th--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_table_m_compact_cell_PaddingTop": {
      "name": "--pf-c-table--m-compact--cell--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_table_m_compact_cell_PaddingRight": {
      "name": "--pf-c-table--m-compact--cell--PaddingRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_table_m_compact_cell_PaddingBottom": {
      "name": "--pf-c-table--m-compact--cell--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_table_m_compact_cell_PaddingLeft": {
      "name": "--pf-c-table--m-compact--cell--PaddingLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_table_m_compact_cell_first_last_child_PaddingLeft": {
      "name": "--pf-c-table--m-compact--cell--first-last-child--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_table_m_compact_cell_first_last_child_PaddingRight": {
      "name": "--pf-c-table--m-compact--cell--first-last-child--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_table_m_compact_cell_first_last_child_xl_PaddingLeft": {
      "name": "--pf-c-table--m-compact--cell--first-last-child--xl--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_table_m_compact_cell_first_last_child_xl_PaddingRight": {
      "name": "--pf-c-table--m-compact--cell--first-last-child--xl--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_table_m_compact_FontSize": {
      "name": "--pf-c-table--m-compact--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_table_m_compact__expandable_row_content_PaddingTop": {
      "name": "--pf-c-table--m-compact__expandable-row-content--PaddingTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_table_m_compact__expandable_row_content_PaddingRight": {
      "name": "--pf-c-table--m-compact__expandable-row-content--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_table_m_compact__expandable_row_content_PaddingBottom": {
      "name": "--pf-c-table--m-compact__expandable-row-content--PaddingBottom",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_table_m_compact__expandable_row_content_PaddingLeft": {
      "name": "--pf-c-table--m-compact__expandable-row-content--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_table_nested_first_last_child_PaddingRight": {
      "name": "--pf-c-table--nested--first-last-child--PaddingRight",
      "value": "4rem",
      "values": [
        "--pf-global--spacer--3xl",
        "$pf-global--spacer--3xl",
        "pf-size-prem(64px)",
        "4rem"
      ]
    },
    "c_table_nested_first_last_child_PaddingLeft": {
      "name": "--pf-c-table--nested--first-last-child--PaddingLeft",
      "value": "4rem",
      "values": [
        "--pf-global--spacer--3xl",
        "$pf-global--spacer--3xl",
        "pf-size-prem(64px)",
        "4rem"
      ]
    },
    "c_table__expandable_row_m_expanded_BorderBottomColor": {
      "name": "--pf-c-table__expandable-row--m-expanded--BorderBottomColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    }
  },
  ".pf-c-table tr > *": {
    "hidden_visible_visible_Visibility": {
      "name": "--pf-hidden-visible--visible--Visibility",
      "value": "visible"
    },
    "hidden_visible_hidden_Display": {
      "name": "--pf-hidden-visible--hidden--Display",
      "value": "none"
    },
    "hidden_visible_hidden_Visibility": {
      "name": "--pf-hidden-visible--hidden--Visibility",
      "value": "hidden"
    },
    "hidden_visible_Display": {
      "name": "--pf-hidden-visible--Display",
      "value": "table-cell",
      "values": [
        "--pf-hidden-visible--visible--Display",
        "--pf-c-table--cell--hidden-visible--Display",
        "table-cell"
      ]
    },
    "hidden_visible_Visibility": {
      "name": "--pf-hidden-visible--Visibility",
      "value": "visible",
      "values": [
        "--pf-hidden-visible--visible--Visibility",
        "visible"
      ]
    },
    "hidden_visible_visible_Display": {
      "name": "--pf-hidden-visible--visible--Display",
      "value": "table-cell",
      "values": [
        "--pf-c-table--cell--hidden-visible--Display",
        "table-cell"
      ]
    }
  },
  ".pf-c-table tr > .pf-m-hidden": {
    "hidden_visible_Display": {
      "name": "--pf-hidden-visible--Display",
      "value": "none",
      "values": [
        "--pf-hidden-visible--hidden--Display",
        "none"
      ]
    },
    "hidden_visible_Visibility": {
      "name": "--pf-hidden-visible--Visibility",
      "value": "hidden",
      "values": [
        "--pf-hidden-visible--hidden--Visibility",
        "hidden"
      ]
    }
  },
  ".pf-c-table tr > *:first-child": {
    "c_table_cell_PaddingLeft": {
      "name": "--pf-c-table--cell--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-c-table--cell--first-last-child--PaddingLeft",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    }
  },
  ".pf-c-table tr > *:last-child": {
    "c_table_cell_PaddingRight": {
      "name": "--pf-c-table--cell--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-c-table--cell--first-last-child--PaddingRight",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    }
  },
  ".pf-c-table thead": {
    "c_table_cell_FontSize": {
      "name": "--pf-c-table--cell--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-c-table--thead--cell--FontSize",
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_table_cell_FontWeight": {
      "name": "--pf-c-table--cell--FontWeight",
      "value": "700",
      "values": [
        "--pf-c-table--thead--cell--FontWeight",
        "--pf-global--FontWeight--bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    },
    "c_table_cell_MinWidth": {
      "name": "--pf-c-table--cell--MinWidth",
      "value": "calc(5ch + 1rem + 1rem)",
      "values": [
        "--pf-c-table--m-truncate--cell--MinWidth",
        "calc(5ch + --pf-c-table--cell--PaddingRight + --pf-c-table--cell--PaddingLeft)",
        "calc(5ch + --pf-global--spacer--md + --pf-global--spacer--md)",
        "calc(5ch + $pf-global--spacer--md + $pf-global--spacer--md)",
        "calc(5ch + pf-size-prem(16px) + pf-size-prem(16px))",
        "calc(5ch + 1rem + 1rem)"
      ]
    },
    "c_table_cell_MaxWidth": {
      "name": "--pf-c-table--cell--MaxWidth",
      "value": "1px",
      "values": [
        "--pf-c-table--m-truncate--cell--MaxWidth",
        "1px"
      ]
    },
    "c_table_cell_Overflow": {
      "name": "--pf-c-table--cell--Overflow",
      "value": "hidden"
    },
    "c_table_cell_TextOverflow": {
      "name": "--pf-c-table--cell--TextOverflow",
      "value": "ellipsis"
    },
    "c_table_cell_WhiteSpace": {
      "name": "--pf-c-table--cell--WhiteSpace",
      "value": "nowrap"
    }
  },
  ".pf-c-table tbody": {
    "c_table_cell_PaddingTop": {
      "name": "--pf-c-table--cell--PaddingTop",
      "value": "1.5rem",
      "values": [
        "--pf-c-table--tbody--cell--PaddingTop",
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_table_cell_PaddingBottom": {
      "name": "--pf-c-table--cell--PaddingBottom",
      "value": "1.5rem",
      "values": [
        "--pf-c-table--tbody--cell--PaddingBottom",
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    }
  },
  ".pf-c-table .pf-m-wrap": {
    "c_table_cell_MinWidth": {
      "name": "--pf-c-table--cell--MinWidth",
      "value": "0"
    },
    "c_table_cell_MaxWidth": {
      "name": "--pf-c-table--cell--MaxWidth",
      "value": "none"
    },
    "c_table_cell_Overflow": {
      "name": "--pf-c-table--cell--Overflow",
      "value": "visible"
    },
    "c_table_cell_TextOverflow": {
      "name": "--pf-c-table--cell--TextOverflow",
      "value": "clip"
    },
    "c_table_cell_WhiteSpace": {
      "name": "--pf-c-table--cell--WhiteSpace",
      "value": "normal"
    }
  },
  ".pf-c-table .pf-m-nowrap": {
    "c_table_cell_MinWidth": {
      "name": "--pf-c-table--cell--MinWidth",
      "value": "0"
    },
    "c_table_cell_MaxWidth": {
      "name": "--pf-c-table--cell--MaxWidth",
      "value": "none"
    },
    "c_table_cell_Overflow": {
      "name": "--pf-c-table--cell--Overflow",
      "value": "visible"
    },
    "c_table_cell_TextOverflow": {
      "name": "--pf-c-table--cell--TextOverflow",
      "value": "clip"
    },
    "c_table_cell_WhiteSpace": {
      "name": "--pf-c-table--cell--WhiteSpace",
      "value": "nowrap"
    }
  },
  ".pf-c-table .pf-c-table__icon": {
    "c_table_cell_MinWidth": {
      "name": "--pf-c-table--cell--MinWidth",
      "value": "fit-content"
    },
    "c_table_cell_MaxWidth": {
      "name": "--pf-c-table--cell--MaxWidth",
      "value": "fit-content"
    },
    "c_table_cell_Width": {
      "name": "--pf-c-table--cell--Width",
      "value": "1%"
    },
    "c_table_cell_Overflow": {
      "name": "--pf-c-table--cell--Overflow",
      "value": "visible"
    },
    "c_table_cell_TextOverflow": {
      "name": "--pf-c-table--cell--TextOverflow",
      "value": "clip"
    },
    "c_table_cell_WhiteSpace": {
      "name": "--pf-c-table--cell--WhiteSpace",
      "value": "nowrap"
    }
  },
  ".pf-c-table .pf-m-break-word": {
    "c_table_cell_WordBreak": {
      "name": "--pf-c-table--cell--WordBreak",
      "value": "break-word"
    },
    "c_table_cell_WhiteSpace": {
      "name": "--pf-c-table--cell--WhiteSpace",
      "value": "normal"
    }
  },
  ".pf-c-table__text": {
    "c_table_cell_MaxWidth": {
      "name": "--pf-c-table--cell--MaxWidth",
      "value": "100%"
    }
  },
  ".pf-c-table__text.pf-m-truncate": {
    "c_table_cell_MinWidth": {
      "name": "--pf-c-table--cell--MinWidth",
      "value": "100%"
    }
  },
  ".pf-c-table__sort .pf-c-table__text": {
    "c_table_cell_MinWidth": {
      "name": "--pf-c-table--cell--MinWidth",
      "value": "0"
    }
  },
  ".pf-c-table .pf-c-table__toggle": {
    "c_table_cell_PaddingBottom": {
      "name": "--pf-c-table--cell--PaddingBottom",
      "value": "0"
    }
  },
  ".pf-c-table .pf-c-table__check": {
    "c_table_cell_MinWidth": {
      "name": "--pf-c-table--cell--MinWidth",
      "value": "0"
    },
    "c_table_cell_Width": {
      "name": "--pf-c-table--cell--Width",
      "value": "1%"
    }
  },
  ".pf-c-table__toggle": {
    "c_table_cell_PaddingRight": {
      "name": "--pf-c-table--cell--PaddingRight",
      "value": "0"
    },
    "c_table_cell_PaddingLeft": {
      "name": "--pf-c-table--cell--PaddingLeft",
      "value": "0"
    }
  },
  ".pf-c-table__check": {
    "c_table_cell_FontSize": {
      "name": "--pf-c-table--cell--FontSize",
      "value": "1rem",
      "values": [
        "--pf-c-table__check--input--FontSize",
        "--pf-global--FontSize--md",
        "$pf-global--FontSize--md",
        "pf-font-prem(16px)",
        "1rem"
      ]
    }
  },
  ".pf-c-table__action": {
    "c_table_cell_PaddingTop": {
      "name": "--pf-c-table--cell--PaddingTop",
      "value": "0"
    },
    "c_table_cell_PaddingRight": {
      "name": "--pf-c-table--cell--PaddingRight",
      "value": "0",
      "values": [
        "--pf-c-table__action--PaddingRight",
        "0"
      ]
    },
    "c_table_cell_PaddingBottom": {
      "name": "--pf-c-table--cell--PaddingBottom",
      "value": "0"
    },
    "c_table_cell_PaddingLeft": {
      "name": "--pf-c-table--cell--PaddingLeft",
      "value": "0",
      "values": [
        "--pf-c-table__action--PaddingLeft",
        "0"
      ]
    }
  },
  ".pf-c-table__inline-edit-action": {
    "c_table_cell_PaddingLeft": {
      "name": "--pf-c-table--cell--PaddingLeft",
      "value": "0"
    },
    "c_table_cell_PaddingRight": {
      "name": "--pf-c-table--cell--PaddingRight",
      "value": "0"
    }
  },
  ".pf-c-table__compound-expansion-toggle": {
    "c_table__button_Color": {
      "name": "--pf-c-table__button--Color",
      "value": "#06c",
      "values": [
        "--pf-c-table__compound-expansion-toggle__button--Color",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_table__button_hover_Color": {
      "name": "--pf-c-table__button--hover--Color",
      "value": "#004080",
      "values": [
        "--pf-c-table__compound-expansion-toggle__button--hover--Color",
        "--pf-global--link--Color--hover",
        "$pf-global--link--Color--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "c_table__button_focus_Color": {
      "name": "--pf-c-table__button--focus--Color",
      "value": "#004080",
      "values": [
        "--pf-c-table__compound-expansion-toggle__button--focus--Color",
        "--pf-global--link--Color--hover",
        "$pf-global--link--Color--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "c_table__button_active_Color": {
      "name": "--pf-c-table__button--active--Color",
      "value": "#004080",
      "values": [
        "--pf-c-table__compound-expansion-toggle__button--active--Color",
        "--pf-global--link--Color--hover",
        "$pf-global--link--Color--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    }
  },
  ".pf-c-table__compound-expansion-toggle:hover": {
    "c_table__compound_expansion_toggle__button_before_BorderRightWidth": {
      "name": "--pf-c-table__compound-expansion-toggle__button--before--BorderRightWidth",
      "value": "1px",
      "values": [
        "--pf-c-table__compound-expansion-toggle__button--before--border-width--base",
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_table__compound_expansion_toggle__button_before_BorderLeftWidth": {
      "name": "--pf-c-table__compound-expansion-toggle__button--before--BorderLeftWidth",
      "value": "1px",
      "values": [
        "--pf-c-table__compound-expansion-toggle__button--before--border-width--base",
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_table__compound_expansion_toggle__button_after_BorderTopWidth": {
      "name": "--pf-c-table__compound-expansion-toggle__button--after--BorderTopWidth",
      "value": "3px",
      "values": [
        "--pf-c-table__compound-expansion-toggle__button--after--border-width--base",
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    }
  },
  ".pf-c-table__compound-expansion-toggle:first-child": {
    "c_table__compound_expansion_toggle__button_before_Left": {
      "name": "--pf-c-table__compound-expansion-toggle__button--before--Left",
      "value": "0"
    },
    "c_table__compound_expansion_toggle__button_after_Left": {
      "name": "--pf-c-table__compound-expansion-toggle__button--after--Left",
      "value": "0"
    }
  },
  ".pf-c-table__compound-expansion-toggle.pf-m-expanded:first-child": {
    "c_table__compound_expansion_toggle__button_before_BorderLeftWidth": {
      "name": "--pf-c-table__compound-expansion-toggle__button--before--BorderLeftWidth",
      "value": "0"
    },
    "c_table__expandable_row_after_BorderLeftWidth": {
      "name": "--pf-c-table__expandable-row--after--BorderLeftWidth",
      "value": "3px",
      "values": [
        "--pf-c-table__expandable-row--after--border-width--base",
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    }
  },
  ".pf-c-table__column-help-action .pf-c-button": {
    "c_button_PaddingRight": {
      "name": "--pf-c-button--PaddingRight",
      "value": "0.5rem",
      "values": [
        "--pf-c-table__column-help--c-button--PaddingRight",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_button_PaddingLeft": {
      "name": "--pf-c-button--PaddingLeft",
      "value": "0.5rem",
      "values": [
        "--pf-c-table__column-help--c-button--PaddingLeft",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-c-table__sort .pf-c-table__button": {
    "c_table_cell_PaddingTop": {
      "name": "--pf-c-table--cell--PaddingTop",
      "value": "0.375rem",
      "values": [
        "--pf-c-table__sort__button--PaddingTop",
        "--pf-global--spacer--form-element",
        "$pf-global--spacer--form-element",
        "pf-size-prem(6px)",
        "0.375rem"
      ]
    },
    "c_table_cell_PaddingRight": {
      "name": "--pf-c-table--cell--PaddingRight",
      "value": "0.5rem",
      "values": [
        "--pf-c-table__sort__button--PaddingRight",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_table_cell_PaddingBottom": {
      "name": "--pf-c-table--cell--PaddingBottom",
      "value": "0.375rem",
      "values": [
        "--pf-c-table__sort__button--PaddingBottom",
        "--pf-global--spacer--form-element",
        "$pf-global--spacer--form-element",
        "pf-size-prem(6px)",
        "0.375rem"
      ]
    },
    "c_table_cell_PaddingLeft": {
      "name": "--pf-c-table--cell--PaddingLeft",
      "value": "0.5rem",
      "values": [
        "--pf-c-table__sort__button--PaddingLeft",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-c-table__sort .pf-c-table__button:hover": {
    "c_table__sort_indicator_Color": {
      "name": "--pf-c-table__sort-indicator--Color",
      "value": "#151515",
      "values": [
        "--pf-c-table__sort__button--hover__sort-indicator--Color",
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    }
  },
  ".pf-c-table__sort .pf-c-table__button:focus": {
    "c_table__sort_indicator_Color": {
      "name": "--pf-c-table__sort-indicator--Color",
      "value": "#151515",
      "values": [
        "--pf-c-table__sort__button--focus__sort-indicator--Color",
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    }
  },
  ".pf-c-table__sort .pf-c-table__button:active": {
    "c_table__sort_indicator_Color": {
      "name": "--pf-c-table__sort-indicator--Color",
      "value": "#151515",
      "values": [
        "--pf-c-table__sort__button--active__sort-indicator--Color",
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    }
  },
  ".pf-c-table__sort.pf-m-selected .pf-c-table__button": {
    "c_table__sort_indicator_Color": {
      "name": "--pf-c-table__sort-indicator--Color",
      "value": "#06c",
      "values": [
        "--pf-c-table__sort--m-selected__sort-indicator--Color",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-table__expandable-row": {
    "c_table_cell_PaddingTop": {
      "name": "--pf-c-table--cell--PaddingTop",
      "value": "0"
    },
    "c_table_cell_PaddingBottom": {
      "name": "--pf-c-table--cell--PaddingBottom",
      "value": "0"
    }
  },
  ".pf-c-table .pf-c-table tr > *:first-child": {
    "c_table_cell_PaddingLeft": {
      "name": "--pf-c-table--cell--PaddingLeft",
      "value": "4rem",
      "values": [
        "--pf-c-table--nested--first-last-child--PaddingLeft",
        "--pf-global--spacer--3xl",
        "$pf-global--spacer--3xl",
        "pf-size-prem(64px)",
        "4rem"
      ]
    }
  },
  ".pf-c-table .pf-c-table tr > *:last-child": {
    "c_table_cell_PaddingRight": {
      "name": "--pf-c-table--cell--PaddingRight",
      "value": "4rem",
      "values": [
        "--pf-c-table--nested--first-last-child--PaddingRight",
        "--pf-global--spacer--3xl",
        "$pf-global--spacer--3xl",
        "pf-size-prem(64px)",
        "4rem"
      ]
    }
  },
  ".pf-c-table.pf-m-compact": {
    "c_table_cell_FontSize": {
      "name": "--pf-c-table--cell--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-c-table--m-compact--FontSize",
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_table_cell_PaddingTop": {
      "name": "--pf-c-table--cell--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-c-table--m-compact--cell--PaddingTop",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_table_cell_PaddingRight": {
      "name": "--pf-c-table--cell--PaddingRight",
      "value": "0.5rem",
      "values": [
        "--pf-c-table--m-compact--cell--PaddingLeft",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_table_cell_PaddingBottom": {
      "name": "--pf-c-table--cell--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-c-table--m-compact--cell--PaddingBottom",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-c-table.pf-m-compact.pf-m-no-border-rows:not(.pf-m-expandable) tbody": {
    "c_table_border_width_base": {
      "name": "--pf-c-table--border-width--base",
      "value": "0"
    },
    "c_table_BorderColor": {
      "name": "--pf-c-table--BorderColor",
      "value": "transparent"
    }
  },
  ".pf-c-table.pf-m-compact tr": {
    "c_table_cell_PaddingLeft": {
      "name": "--pf-c-table--cell--PaddingLeft",
      "value": "0.5rem",
      "values": [
        "--pf-c-table--m-compact--cell--PaddingLeft",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_table_cell_PaddingRight": {
      "name": "--pf-c-table--cell--PaddingRight",
      "value": "0.5rem",
      "values": [
        "--pf-c-table--m-compact--cell--PaddingRight",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-c-table.pf-m-compact tr:not(.pf-c-table__expandable-row)": {
    "c_table_cell_FontSize": {
      "name": "--pf-c-table--cell--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-c-table--m-compact--FontSize",
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_table_cell_PaddingTop": {
      "name": "--pf-c-table--cell--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-c-table--m-compact--cell--PaddingTop",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_table_cell_PaddingBottom": {
      "name": "--pf-c-table--cell--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-c-table--m-compact--cell--PaddingBottom",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-c-table.pf-m-compact tr:not(.pf-c-table__expandable-row) > *:first-child": {
    "c_table_cell_PaddingLeft": {
      "name": "--pf-c-table--cell--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-c-table--m-compact--cell--first-last-child--PaddingLeft",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    }
  },
  ".pf-c-table.pf-m-compact tr:not(.pf-c-table__expandable-row) > *:last-child": {
    "c_table_cell_PaddingRight": {
      "name": "--pf-c-table--cell--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-c-table--m-compact--cell--first-last-child--PaddingRight",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    }
  },
  ".pf-c-table.pf-m-compact thead th": {
    "c_table_cell_PaddingTop": {
      "name": "--pf-c-table--cell--PaddingTop",
      "value": "calc(0.5rem + 0.25rem)",
      "values": [
        "--pf-c-table--m-compact-th--PaddingTop",
        "calc(--pf-global--spacer--sm + --pf-global--spacer--xs)",
        "calc($pf-global--spacer--sm + $pf-global--spacer--xs)",
        "calc(pf-size-prem(8px) + pf-size-prem(4px))",
        "calc(0.5rem + 0.25rem)"
      ]
    },
    "c_table_cell_PaddingBottom": {
      "name": "--pf-c-table--cell--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-c-table--m-compact-th--PaddingBottom",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-c-table.pf-m-compact .pf-c-table__action": {
    "c_table_cell_PaddingTop": {
      "name": "--pf-c-table--cell--PaddingTop",
      "value": "0",
      "values": [
        "--pf-c-table__action--PaddingTop",
        "0"
      ]
    },
    "c_table_cell_PaddingBottom": {
      "name": "--pf-c-table--cell--PaddingBottom",
      "value": "0",
      "values": [
        "--pf-c-table__action--PaddingBottom",
        "0"
      ]
    },
    "c_table_cell_PaddingLeft": {
      "name": "--pf-c-table--cell--PaddingLeft",
      "value": "0",
      "values": [
        "--pf-c-table__action--PaddingLeft",
        "0"
      ]
    }
  },
  ".pf-c-table.pf-m-compact .pf-c-table__toggle": {
    "c_table_cell_PaddingTop": {
      "name": "--pf-c-table--cell--PaddingTop",
      "value": "0",
      "values": [
        "--pf-c-table--m-compact__toggle--PaddingTop",
        "0"
      ]
    },
    "c_table_cell_PaddingBottom": {
      "name": "--pf-c-table--cell--PaddingBottom",
      "value": "0",
      "values": [
        "--pf-c-table--m-compact__toggle--PaddingBottom",
        "0"
      ]
    }
  },
  ".pf-c-table .pf-c-table.pf-m-compact tr > *:first-child": {
    "c_table_cell_PaddingLeft": {
      "name": "--pf-c-table--cell--PaddingLeft",
      "value": "4rem",
      "values": [
        "--pf-c-table--nested--first-last-child--PaddingLeft",
        "--pf-global--spacer--3xl",
        "$pf-global--spacer--3xl",
        "pf-size-prem(64px)",
        "4rem"
      ]
    }
  },
  ".pf-c-table .pf-c-table.pf-m-compact tr > *:last-child": {
    "c_table_cell_PaddingRight": {
      "name": "--pf-c-table--cell--PaddingRight",
      "value": "4rem",
      "values": [
        "--pf-c-table--nested--first-last-child--PaddingRight",
        "--pf-global--spacer--3xl",
        "$pf-global--spacer--3xl",
        "pf-size-prem(64px)",
        "4rem"
      ]
    }
  },
  ".pf-c-table.pf-m-compact .pf-c-table__expandable-row-content": {
    "c_table__expandable_row_content_PaddingTop": {
      "name": "--pf-c-table__expandable-row-content--PaddingTop",
      "value": "1.5rem",
      "values": [
        "--pf-c-table--m-compact__expandable-row-content--PaddingTop",
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_table__expandable_row_content_PaddingBottom": {
      "name": "--pf-c-table__expandable-row-content--PaddingBottom",
      "value": "1.5rem",
      "values": [
        "--pf-c-table--m-compact__expandable-row-content--PaddingBottom",
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    }
  },
  ".pf-c-table .pf-m-width-10": {
    "c_table_cell_Width": {
      "name": "--pf-c-table--cell--Width",
      "value": "10%"
    }
  },
  ".pf-c-table .pf-m-width-15": {
    "c_table_cell_Width": {
      "name": "--pf-c-table--cell--Width",
      "value": "15%"
    }
  },
  ".pf-c-table .pf-m-width-20": {
    "c_table_cell_Width": {
      "name": "--pf-c-table--cell--Width",
      "value": "20%"
    }
  },
  ".pf-c-table .pf-m-width-25": {
    "c_table_cell_Width": {
      "name": "--pf-c-table--cell--Width",
      "value": "25%"
    }
  },
  ".pf-c-table .pf-m-width-30": {
    "c_table_cell_Width": {
      "name": "--pf-c-table--cell--Width",
      "value": "30%"
    }
  },
  ".pf-c-table .pf-m-width-35": {
    "c_table_cell_Width": {
      "name": "--pf-c-table--cell--Width",
      "value": "35%"
    }
  },
  ".pf-c-table .pf-m-width-40": {
    "c_table_cell_Width": {
      "name": "--pf-c-table--cell--Width",
      "value": "40%"
    }
  },
  ".pf-c-table .pf-m-width-45": {
    "c_table_cell_Width": {
      "name": "--pf-c-table--cell--Width",
      "value": "45%"
    }
  },
  ".pf-c-table .pf-m-width-50": {
    "c_table_cell_Width": {
      "name": "--pf-c-table--cell--Width",
      "value": "50%"
    }
  },
  ".pf-c-table .pf-m-width-60": {
    "c_table_cell_Width": {
      "name": "--pf-c-table--cell--Width",
      "value": "60%"
    }
  },
  ".pf-c-table .pf-m-width-70": {
    "c_table_cell_Width": {
      "name": "--pf-c-table--cell--Width",
      "value": "70%"
    }
  },
  ".pf-c-table .pf-m-width-80": {
    "c_table_cell_Width": {
      "name": "--pf-c-table--cell--Width",
      "value": "80%"
    }
  },
  ".pf-c-table .pf-m-width-90": {
    "c_table_cell_Width": {
      "name": "--pf-c-table--cell--Width",
      "value": "90%"
    }
  },
  ".pf-c-table .pf-m-width-100": {
    "c_table_cell_Width": {
      "name": "--pf-c-table--cell--Width",
      "value": "100%"
    }
  }
};
exports["default"] = exports.c_table;
"use strict";
exports.__esModule = true;
exports.c_radio__input_first_child_MarginLeft = {
  "name": "--pf-c-radio__input--first-child--MarginLeft",
  "value": "0.0625rem",
  "var": "var(--pf-c-radio__input--first-child--MarginLeft)"
};
exports["default"] = exports.c_radio__input_first_child_MarginLeft;
"use strict";
exports.__esModule = true;
exports.c_login__main_header_md_PaddingRight = {
  "name": "--pf-c-login__main-header--md--PaddingRight",
  "value": "3rem",
  "var": "var(--pf-c-login__main-header--md--PaddingRight)"
};
exports["default"] = exports.c_login__main_header_md_PaddingRight;
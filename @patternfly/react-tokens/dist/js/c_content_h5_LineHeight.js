"use strict";
exports.__esModule = true;
exports.c_content_h5_LineHeight = {
  "name": "--pf-c-content--h5--LineHeight",
  "value": "1.5",
  "var": "var(--pf-c-content--h5--LineHeight)"
};
exports["default"] = exports.c_content_h5_LineHeight;
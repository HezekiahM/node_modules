"use strict";
exports.__esModule = true;
exports.c_notification_drawer__group_toggle_BackgroundColor = {
  "name": "--pf-c-notification-drawer__group-toggle--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-notification-drawer__group-toggle--BackgroundColor)"
};
exports["default"] = exports.c_notification_drawer__group_toggle_BackgroundColor;
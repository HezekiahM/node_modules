"use strict";
exports.__esModule = true;
exports.c_background_image_BackgroundImage_lg = {
  "name": "--pf-c-background-image--BackgroundImage--lg",
  "value": "url(\"../../assets/images/pfbg_2000.jpg\")",
  "var": "var(--pf-c-background-image--BackgroundImage--lg)"
};
exports["default"] = exports.c_background_image_BackgroundImage_lg;
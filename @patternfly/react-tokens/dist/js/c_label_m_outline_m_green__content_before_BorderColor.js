"use strict";
exports.__esModule = true;
exports.c_label_m_outline_m_green__content_before_BorderColor = {
  "name": "--pf-c-label--m-outline--m-green__content--before--BorderColor",
  "value": "#bde5b8",
  "var": "var(--pf-c-label--m-outline--m-green__content--before--BorderColor)"
};
exports["default"] = exports.c_label_m_outline_m_green__content_before_BorderColor;
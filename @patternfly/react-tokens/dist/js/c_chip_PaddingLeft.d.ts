export const c_chip_PaddingLeft: {
  "name": "--pf-c-chip--PaddingLeft",
  "value": "0.5rem",
  "var": "var(--pf-c-chip--PaddingLeft)"
};
export default c_chip_PaddingLeft;
"use strict";
exports.__esModule = true;
exports.c_app_launcher__menu_item_m_link_hover_BackgroundColor = {
  "name": "--pf-c-app-launcher__menu-item--m-link--hover--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-app-launcher__menu-item--m-link--hover--BackgroundColor)"
};
exports["default"] = exports.c_app_launcher__menu_item_m_link_hover_BackgroundColor;
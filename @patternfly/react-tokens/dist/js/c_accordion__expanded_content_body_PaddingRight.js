"use strict";
exports.__esModule = true;
exports.c_accordion__expanded_content_body_PaddingRight = {
  "name": "--pf-c-accordion__expanded-content-body--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-accordion__expanded-content-body--PaddingRight)"
};
exports["default"] = exports.c_accordion__expanded_content_body_PaddingRight;
"use strict";
exports.__esModule = true;
exports.c_nav__scroll_button_BackgroundColor = {
  "name": "--pf-c-nav__scroll-button--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-nav__scroll-button--BackgroundColor)"
};
exports["default"] = exports.c_nav__scroll_button_BackgroundColor;
"use strict";
exports.__esModule = true;
exports.c_dropdown__toggle_PaddingRight = {
  "name": "--pf-c-dropdown__toggle--PaddingRight",
  "value": "0.5rem",
  "var": "var(--pf-c-dropdown__toggle--PaddingRight)"
};
exports["default"] = exports.c_dropdown__toggle_PaddingRight;
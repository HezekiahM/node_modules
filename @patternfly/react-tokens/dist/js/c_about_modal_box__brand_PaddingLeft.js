"use strict";
exports.__esModule = true;
exports.c_about_modal_box__brand_PaddingLeft = {
  "name": "--pf-c-about-modal-box__brand--PaddingLeft",
  "value": "2rem",
  "var": "var(--pf-c-about-modal-box__brand--PaddingLeft)"
};
exports["default"] = exports.c_about_modal_box__brand_PaddingLeft;
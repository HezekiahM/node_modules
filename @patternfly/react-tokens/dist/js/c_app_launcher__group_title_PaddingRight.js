"use strict";
exports.__esModule = true;
exports.c_app_launcher__group_title_PaddingRight = {
  "name": "--pf-c-app-launcher__group-title--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-app-launcher__group-title--PaddingRight)"
};
exports["default"] = exports.c_app_launcher__group_title_PaddingRight;
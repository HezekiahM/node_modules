"use strict";
exports.__esModule = true;
exports.c_card__actions_child_MarginLeft = {
  "name": "--pf-c-card__actions--child--MarginLeft",
  "value": "0.5rem",
  "var": "var(--pf-c-card__actions--child--MarginLeft)"
};
exports["default"] = exports.c_card__actions_child_MarginLeft;
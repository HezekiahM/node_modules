export const c_accordion__expanded_content_body_before_Width: {
  "name": "--pf-c-accordion__expanded-content-body--before--Width",
  "value": "3px",
  "var": "var(--pf-c-accordion__expanded-content-body--before--Width)"
};
export default c_accordion__expanded_content_body_before_Width;
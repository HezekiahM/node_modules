"use strict";
exports.__esModule = true;
exports.global_BackgroundColor_light_200 = {
  "name": "--pf-global--BackgroundColor--light-200",
  "value": "#fafafa",
  "var": "var(--pf-global--BackgroundColor--light-200)"
};
exports["default"] = exports.global_BackgroundColor_light_200;
"use strict";
exports.__esModule = true;
exports.c_toolbar_BackgroundColor = {
  "name": "--pf-c-toolbar--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-toolbar--BackgroundColor)"
};
exports["default"] = exports.c_toolbar_BackgroundColor;
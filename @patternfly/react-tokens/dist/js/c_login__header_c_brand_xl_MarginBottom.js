"use strict";
exports.__esModule = true;
exports.c_login__header_c_brand_xl_MarginBottom = {
  "name": "--pf-c-login__header--c-brand--xl--MarginBottom",
  "value": "3rem",
  "var": "var(--pf-c-login__header--c-brand--xl--MarginBottom)"
};
exports["default"] = exports.c_login__header_c_brand_xl_MarginBottom;
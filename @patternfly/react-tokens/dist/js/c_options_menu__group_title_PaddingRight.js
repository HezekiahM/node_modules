"use strict";
exports.__esModule = true;
exports.c_options_menu__group_title_PaddingRight = {
  "name": "--pf-c-options-menu__group-title--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-options-menu__group-title--PaddingRight)"
};
exports["default"] = exports.c_options_menu__group_title_PaddingRight;
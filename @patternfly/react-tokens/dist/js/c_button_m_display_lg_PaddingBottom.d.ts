export const c_button_m_display_lg_PaddingBottom: {
  "name": "--pf-c-button--m-display-lg--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-button--m-display-lg--PaddingBottom)"
};
export default c_button_m_display_lg_PaddingBottom;
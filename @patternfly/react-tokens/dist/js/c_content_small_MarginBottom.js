"use strict";
exports.__esModule = true;
exports.c_content_small_MarginBottom = {
  "name": "--pf-c-content--small--MarginBottom",
  "value": "1rem",
  "var": "var(--pf-c-content--small--MarginBottom)"
};
exports["default"] = exports.c_content_small_MarginBottom;
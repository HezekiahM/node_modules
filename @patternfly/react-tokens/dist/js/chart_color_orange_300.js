"use strict";
exports.__esModule = true;
exports.chart_color_orange_300 = {
  "name": "--pf-chart-color-orange-300",
  "value": "#ec7a08",
  "var": "var(--pf-chart-color-orange-300)"
};
exports["default"] = exports.chart_color_orange_300;
export const c_chip_group__list_item_MarginRight: {
  "name": "--pf-c-chip-group__list-item--MarginRight",
  "value": "0.25rem",
  "var": "var(--pf-c-chip-group__list-item--MarginRight)"
};
export default c_chip_group__list_item_MarginRight;
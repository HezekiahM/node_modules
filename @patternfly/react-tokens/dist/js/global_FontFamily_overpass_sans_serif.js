"use strict";
exports.__esModule = true;
exports.global_FontFamily_overpass_sans_serif = {
  "name": "--pf-global--FontFamily--overpass--sans-serif",
  "value": "overpass, overpass, open sans, -apple-system, blinkmacsystemfont, Segoe UI, roboto, Helvetica Neue, arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol",
  "var": "var(--pf-global--FontFamily--overpass--sans-serif)"
};
exports["default"] = exports.global_FontFamily_overpass_sans_serif;
"use strict";
exports.__esModule = true;
exports.c_popover__footer_MarginTop = {
  "name": "--pf-c-popover__footer--MarginTop",
  "value": "1rem",
  "var": "var(--pf-c-popover__footer--MarginTop)"
};
exports["default"] = exports.c_popover__footer_MarginTop;
"use strict";
exports.__esModule = true;
exports.c_wizard__nav_list_lg_PaddingBottom = {
  "name": "--pf-c-wizard__nav-list--lg--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-wizard__nav-list--lg--PaddingBottom)"
};
exports["default"] = exports.c_wizard__nav_list_lg_PaddingBottom;
"use strict";
exports.__esModule = true;
exports.c_about_modal_box__content_sm_MarginBottom = {
  "name": "--pf-c-about-modal-box__content--sm--MarginBottom",
  "value": "3rem",
  "var": "var(--pf-c-about-modal-box__content--sm--MarginBottom)"
};
exports["default"] = exports.c_about_modal_box__content_sm_MarginBottom;
"use strict";
exports.__esModule = true;
exports.chart_line_data_stroke_Color = {
  "name": "--pf-chart-line--data--stroke--Color",
  "value": "#151515",
  "var": "var(--pf-chart-line--data--stroke--Color)"
};
exports["default"] = exports.chart_line_data_stroke_Color;
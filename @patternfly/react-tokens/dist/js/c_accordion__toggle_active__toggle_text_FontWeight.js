"use strict";
exports.__esModule = true;
exports.c_accordion__toggle_active__toggle_text_FontWeight = {
  "name": "--pf-c-accordion__toggle--active__toggle-text--FontWeight",
  "value": "700",
  "var": "var(--pf-c-accordion__toggle--active__toggle-text--FontWeight)"
};
exports["default"] = exports.c_accordion__toggle_active__toggle_text_FontWeight;
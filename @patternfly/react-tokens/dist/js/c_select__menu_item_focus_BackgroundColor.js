"use strict";
exports.__esModule = true;
exports.c_select__menu_item_focus_BackgroundColor = {
  "name": "--pf-c-select__menu-item--focus--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-select__menu-item--focus--BackgroundColor)"
};
exports["default"] = exports.c_select__menu_item_focus_BackgroundColor;
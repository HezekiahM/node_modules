export const c_backdrop_BackgroundColor: {
  "name": "--pf-c-backdrop--BackgroundColor",
  "value": "rgba(#030303, .62)",
  "var": "var(--pf-c-backdrop--BackgroundColor)"
};
export default c_backdrop_BackgroundColor;
"use strict";
exports.__esModule = true;
exports.c_data_list__item_BackgroundColor = {
  "name": "--pf-c-data-list__item--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-data-list__item--BackgroundColor)"
};
exports["default"] = exports.c_data_list__item_BackgroundColor;
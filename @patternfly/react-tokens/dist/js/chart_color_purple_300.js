"use strict";
exports.__esModule = true;
exports.chart_color_purple_300 = {
  "name": "--pf-chart-color-purple-300",
  "value": "#5752d1",
  "var": "var(--pf-chart-color-purple-300)"
};
exports["default"] = exports.chart_color_purple_300;
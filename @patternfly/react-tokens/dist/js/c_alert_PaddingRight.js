"use strict";
exports.__esModule = true;
exports.c_alert_PaddingRight = {
  "name": "--pf-c-alert--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-alert--PaddingRight)"
};
exports["default"] = exports.c_alert_PaddingRight;
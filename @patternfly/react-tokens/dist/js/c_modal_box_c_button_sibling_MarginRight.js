"use strict";
exports.__esModule = true;
exports.c_modal_box_c_button_sibling_MarginRight = {
  "name": "--pf-c-modal-box--c-button--sibling--MarginRight",
  "value": "2rem",
  "var": "var(--pf-c-modal-box--c-button--sibling--MarginRight)"
};
exports["default"] = exports.c_modal_box_c_button_sibling_MarginRight;
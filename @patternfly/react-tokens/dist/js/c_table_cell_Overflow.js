"use strict";
exports.__esModule = true;
exports.c_table_cell_Overflow = {
  "name": "--pf-c-table--cell--Overflow",
  "value": "visible",
  "var": "var(--pf-c-table--cell--Overflow)"
};
exports["default"] = exports.c_table_cell_Overflow;
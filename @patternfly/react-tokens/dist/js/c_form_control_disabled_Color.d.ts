export const c_form_control_disabled_Color: {
  "name": "--pf-c-form-control--disabled--Color",
  "value": "#6a6e73",
  "var": "var(--pf-c-form-control--disabled--Color)"
};
export default c_form_control_disabled_Color;
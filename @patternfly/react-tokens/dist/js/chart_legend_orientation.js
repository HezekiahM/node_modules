"use strict";
exports.__esModule = true;
exports.chart_legend_orientation = {
  "name": "--pf-chart-legend--orientation",
  "value": "horizontal",
  "var": "var(--pf-chart-legend--orientation)"
};
exports["default"] = exports.chart_legend_orientation;
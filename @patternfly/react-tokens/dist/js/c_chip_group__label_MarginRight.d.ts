export const c_chip_group__label_MarginRight: {
  "name": "--pf-c-chip-group__label--MarginRight",
  "value": "0.5rem",
  "var": "var(--pf-c-chip-group__label--MarginRight)"
};
export default c_chip_group__label_MarginRight;
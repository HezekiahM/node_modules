"use strict";
exports.__esModule = true;
exports.c_data_list_m_compact__item_action_MarginLeft = {
  "name": "--pf-c-data-list--m-compact__item-action--MarginLeft",
  "value": "1rem",
  "var": "var(--pf-c-data-list--m-compact__item-action--MarginLeft)"
};
exports["default"] = exports.c_data_list_m_compact__item_action_MarginLeft;
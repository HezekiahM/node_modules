"use strict";
exports.__esModule = true;
exports.chart_color_green_100 = {
  "name": "--pf-chart-color-green-100",
  "value": "#bde2b9",
  "var": "var(--pf-chart-color-green-100)"
};
exports["default"] = exports.chart_color_green_100;
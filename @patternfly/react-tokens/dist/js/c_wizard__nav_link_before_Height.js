"use strict";
exports.__esModule = true;
exports.c_wizard__nav_link_before_Height = {
  "name": "--pf-c-wizard__nav-link--before--Height",
  "value": "1.5rem",
  "var": "var(--pf-c-wizard__nav-link--before--Height)"
};
exports["default"] = exports.c_wizard__nav_link_before_Height;
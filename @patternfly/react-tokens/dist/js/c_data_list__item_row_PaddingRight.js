"use strict";
exports.__esModule = true;
exports.c_data_list__item_row_PaddingRight = {
  "name": "--pf-c-data-list__item-row--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-data-list__item-row--PaddingRight)"
};
exports["default"] = exports.c_data_list__item_row_PaddingRight;
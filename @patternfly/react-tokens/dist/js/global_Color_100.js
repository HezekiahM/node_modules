"use strict";
exports.__esModule = true;
exports.global_Color_100 = {
  "name": "--pf-global--Color--100",
  "value": "#fff",
  "var": "var(--pf-global--Color--100)"
};
exports["default"] = exports.global_Color_100;
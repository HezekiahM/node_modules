"use strict";
exports.__esModule = true;
exports.c_card = {
  ".pf-c-card": {
    "c_card_BackgroundColor": {
      "name": "--pf-c-card--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_card_BoxShadow": {
      "name": "--pf-c-card--BoxShadow",
      "value": "0 0.0625rem 0.125rem 0 rgba(3, 3, 3, 0.12), 0 0 0.125rem 0 rgba(3, 3, 3, 0.06)",
      "values": [
        "--pf-global--BoxShadow--sm",
        "$pf-global--BoxShadow--sm",
        "0 pf-size-prem(1px) pf-size-prem(2px) 0 rgba($pf-color-black-1000, .12), 0 0 pf-size-prem(2px) 0 rgba($pf-color-black-1000, .06)",
        "0 pf-size-prem(1px) pf-size-prem(2px) 0 rgba(#030303, .12), 0 0 pf-size-prem(2px) 0 rgba(#030303, .06)",
        "0 0.0625rem 0.125rem 0 rgba(3, 3, 3, 0.12), 0 0 0.125rem 0 rgba(3, 3, 3, 0.06)"
      ]
    },
    "c_card_m_hoverable_hover_BoxShadow": {
      "name": "--pf-c-card--m-hoverable--hover--BoxShadow",
      "value": "0 0.5rem 1rem 0 rgba(3, 3, 3, 0.16), 0 0 0.375rem 0 rgba(3, 3, 3, 0.08)",
      "values": [
        "--pf-global--BoxShadow--lg",
        "$pf-global--BoxShadow--lg",
        "0 pf-size-prem(8px) pf-size-prem(16px) 0 rgba($pf-color-black-1000, .16), 0 0 pf-size-prem(6px) 0 rgba($pf-color-black-1000, .08)",
        "0 pf-size-prem(8px) pf-size-prem(16px) 0 rgba(#030303, .16), 0 0 pf-size-prem(6px) 0 rgba(#030303, .08)",
        "0 0.5rem 1rem 0 rgba(3, 3, 3, 0.16), 0 0 0.375rem 0 rgba(3, 3, 3, 0.08)"
      ]
    },
    "c_card_m_selectable_hover_BoxShadow": {
      "name": "--pf-c-card--m-selectable--hover--BoxShadow",
      "value": "0 0.5rem 1rem 0 rgba(3, 3, 3, 0.16), 0 0 0.375rem 0 rgba(3, 3, 3, 0.08)",
      "values": [
        "--pf-global--BoxShadow--lg",
        "$pf-global--BoxShadow--lg",
        "0 pf-size-prem(8px) pf-size-prem(16px) 0 rgba($pf-color-black-1000, .16), 0 0 pf-size-prem(6px) 0 rgba($pf-color-black-1000, .08)",
        "0 pf-size-prem(8px) pf-size-prem(16px) 0 rgba(#030303, .16), 0 0 pf-size-prem(6px) 0 rgba(#030303, .08)",
        "0 0.5rem 1rem 0 rgba(3, 3, 3, 0.16), 0 0 0.375rem 0 rgba(3, 3, 3, 0.08)"
      ]
    },
    "c_card_m_selectable_focus_BoxShadow": {
      "name": "--pf-c-card--m-selectable--focus--BoxShadow",
      "value": "0 0.5rem 1rem 0 rgba(3, 3, 3, 0.16), 0 0 0.375rem 0 rgba(3, 3, 3, 0.08)",
      "values": [
        "--pf-global--BoxShadow--lg",
        "$pf-global--BoxShadow--lg",
        "0 pf-size-prem(8px) pf-size-prem(16px) 0 rgba($pf-color-black-1000, .16), 0 0 pf-size-prem(6px) 0 rgba($pf-color-black-1000, .08)",
        "0 pf-size-prem(8px) pf-size-prem(16px) 0 rgba(#030303, .16), 0 0 pf-size-prem(6px) 0 rgba(#030303, .08)",
        "0 0.5rem 1rem 0 rgba(3, 3, 3, 0.16), 0 0 0.375rem 0 rgba(3, 3, 3, 0.08)"
      ]
    },
    "c_card_m_selectable_active_BoxShadow": {
      "name": "--pf-c-card--m-selectable--active--BoxShadow",
      "value": "0 0.5rem 1rem 0 rgba(3, 3, 3, 0.16), 0 0 0.375rem 0 rgba(3, 3, 3, 0.08)",
      "values": [
        "--pf-global--BoxShadow--lg",
        "$pf-global--BoxShadow--lg",
        "0 pf-size-prem(8px) pf-size-prem(16px) 0 rgba($pf-color-black-1000, .16), 0 0 pf-size-prem(6px) 0 rgba($pf-color-black-1000, .08)",
        "0 pf-size-prem(8px) pf-size-prem(16px) 0 rgba(#030303, .16), 0 0 pf-size-prem(6px) 0 rgba(#030303, .08)",
        "0 0.5rem 1rem 0 rgba(3, 3, 3, 0.16), 0 0 0.375rem 0 rgba(3, 3, 3, 0.08)"
      ]
    },
    "c_card_m_selectable_m_selected_BoxShadow": {
      "name": "--pf-c-card--m-selectable--m-selected--BoxShadow",
      "value": "0 0.5rem 1rem 0 rgba(3, 3, 3, 0.16), 0 0 0.375rem 0 rgba(3, 3, 3, 0.08)",
      "values": [
        "--pf-global--BoxShadow--lg",
        "$pf-global--BoxShadow--lg",
        "0 pf-size-prem(8px) pf-size-prem(16px) 0 rgba($pf-color-black-1000, .16), 0 0 pf-size-prem(6px) 0 rgba($pf-color-black-1000, .08)",
        "0 pf-size-prem(8px) pf-size-prem(16px) 0 rgba(#030303, .16), 0 0 pf-size-prem(6px) 0 rgba(#030303, .08)",
        "0 0.5rem 1rem 0 rgba(3, 3, 3, 0.16), 0 0 0.375rem 0 rgba(3, 3, 3, 0.08)"
      ]
    },
    "c_card_m_selectable_m_selected_before_Height": {
      "name": "--pf-c-card--m-selectable--m-selected--before--Height",
      "value": "3px",
      "values": [
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    },
    "c_card_m_selectable_m_selected_before_BackgroundColor": {
      "name": "--pf-c-card--m-selectable--m-selected--before--BackgroundColor",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_card_m_compact__body_FontSize": {
      "name": "--pf-c-card--m-compact__body--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_card_m_compact__footer_FontSize": {
      "name": "--pf-c-card--m-compact__footer--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_card_m_compact_first_child_PaddingTop": {
      "name": "--pf-c-card--m-compact--first-child--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_card_m_compact_child_PaddingRight": {
      "name": "--pf-c-card--m-compact--child--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_card_m_compact_child_PaddingBottom": {
      "name": "--pf-c-card--m-compact--child--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_card_m_compact_child_PaddingLeft": {
      "name": "--pf-c-card--m-compact--child--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_card_m_compact__title_not_last_child_PaddingBottom": {
      "name": "--pf-c-card--m-compact__title--not--last-child--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_card_m_flat_BorderWidth": {
      "name": "--pf-c-card--m-flat--BorderWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_card_m_flat_BorderColor": {
      "name": "--pf-c-card--m-flat--BorderColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_card_first_child_PaddingTop": {
      "name": "--pf-c-card--first-child--PaddingTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_card_child_PaddingRight": {
      "name": "--pf-c-card--child--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_card_child_PaddingBottom": {
      "name": "--pf-c-card--child--PaddingBottom",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_card_child_PaddingLeft": {
      "name": "--pf-c-card--child--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_card__title_FontSize": {
      "name": "--pf-c-card__title--FontSize",
      "value": "1rem",
      "values": [
        "--pf-global--FontSize--md",
        "$pf-global--FontSize--md",
        "pf-font-prem(16px)",
        "1rem"
      ]
    },
    "c_card__title_FontWeight": {
      "name": "--pf-c-card__title--FontWeight",
      "value": "700",
      "values": [
        "--pf-global--FontWeight--bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    },
    "c_card__title_not_last_child_PaddingBottom": {
      "name": "--pf-c-card__title--not--last-child--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_card__body_FontSize": {
      "name": "--pf-c-card__body--FontSize",
      "value": "1rem",
      "values": [
        "--pf-global--FontSize--md",
        "$pf-global--FontSize--md",
        "pf-font-prem(16px)",
        "1rem"
      ]
    },
    "c_card__footer_FontSize": {
      "name": "--pf-c-card__footer--FontSize",
      "value": "1rem",
      "values": [
        "--pf-global--FontSize--md",
        "$pf-global--FontSize--md",
        "pf-font-prem(16px)",
        "1rem"
      ]
    },
    "c_card__actions_PaddingLeft": {
      "name": "--pf-c-card__actions--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_card__actions_child_MarginLeft": {
      "name": "--pf-c-card__actions--child--MarginLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-c-card.pf-m-compact": {
    "c_card__body_FontSize": {
      "name": "--pf-c-card__body--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-c-card--m-compact__body--FontSize",
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_card__footer_FontSize": {
      "name": "--pf-c-card__footer--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-c-card--m-compact__footer--FontSize",
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_card_first_child_PaddingTop": {
      "name": "--pf-c-card--first-child--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-c-card--m-compact--first-child--PaddingTop",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_card_child_PaddingRight": {
      "name": "--pf-c-card--child--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-c-card--m-compact--child--PaddingRight",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_card_child_PaddingBottom": {
      "name": "--pf-c-card--child--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-c-card--m-compact--child--PaddingBottom",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_card_child_PaddingLeft": {
      "name": "--pf-c-card--child--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-c-card--m-compact--child--PaddingLeft",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_card__title_not_last_child_PaddingBottom": {
      "name": "--pf-c-card__title--not--last-child--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-c-card--m-compact__title--not--last-child--PaddingBottom",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-c-card.pf-m-flat": {
    "c_card_BoxShadow": {
      "name": "--pf-c-card--BoxShadow",
      "value": "none"
    }
  }
};
exports["default"] = exports.c_card;
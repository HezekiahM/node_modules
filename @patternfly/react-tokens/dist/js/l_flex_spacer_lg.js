"use strict";
exports.__esModule = true;
exports.l_flex_spacer_lg = {
  "name": "--pf-l-flex--spacer--lg",
  "value": "1.5rem",
  "var": "var(--pf-l-flex--spacer--lg)"
};
exports["default"] = exports.l_flex_spacer_lg;
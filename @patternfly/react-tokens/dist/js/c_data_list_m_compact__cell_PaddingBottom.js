"use strict";
exports.__esModule = true;
exports.c_data_list_m_compact__cell_PaddingBottom = {
  "name": "--pf-c-data-list--m-compact__cell--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-data-list--m-compact__cell--PaddingBottom)"
};
exports["default"] = exports.c_data_list_m_compact__cell_PaddingBottom;
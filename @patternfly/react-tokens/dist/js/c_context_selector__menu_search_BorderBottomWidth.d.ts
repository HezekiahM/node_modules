export const c_context_selector__menu_search_BorderBottomWidth: {
  "name": "--pf-c-context-selector__menu-search--BorderBottomWidth",
  "value": "1px",
  "var": "var(--pf-c-context-selector__menu-search--BorderBottomWidth)"
};
export default c_context_selector__menu_search_BorderBottomWidth;
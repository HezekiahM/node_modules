"use strict";
exports.__esModule = true;
exports.c_form__actions_MarginTop = {
  "name": "--pf-c-form__actions--MarginTop",
  "value": "calc(0.5rem * -1)",
  "var": "var(--pf-c-form__actions--MarginTop)"
};
exports["default"] = exports.c_form__actions_MarginTop;
"use strict";
exports.__esModule = true;
exports.c_divider_after_BackgroundColor = {
  "name": "--pf-c-divider--after--BackgroundColor",
  "value": "#3c3f42",
  "var": "var(--pf-c-divider--after--BackgroundColor)"
};
exports["default"] = exports.c_divider_after_BackgroundColor;
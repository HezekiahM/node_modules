"use strict";
exports.__esModule = true;
exports.chart_donut_threshold_third_Color = {
  "name": "--pf-chart-donut--threshold--third--Color",
  "value": "#b8bbbe",
  "var": "var(--pf-chart-donut--threshold--third--Color)"
};
exports["default"] = exports.chart_donut_threshold_third_Color;
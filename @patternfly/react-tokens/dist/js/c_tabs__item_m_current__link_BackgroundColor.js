"use strict";
exports.__esModule = true;
exports.c_tabs__item_m_current__link_BackgroundColor = {
  "name": "--pf-c-tabs__item--m-current__link--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-tabs__item--m-current__link--BackgroundColor)"
};
exports["default"] = exports.c_tabs__item_m_current__link_BackgroundColor;
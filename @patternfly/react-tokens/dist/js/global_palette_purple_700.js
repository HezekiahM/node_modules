"use strict";
exports.__esModule = true;
exports.global_palette_purple_700 = {
  "name": "--pf-global--palette--purple-700",
  "value": "#1f0066",
  "var": "var(--pf-global--palette--purple-700)"
};
exports["default"] = exports.global_palette_purple_700;
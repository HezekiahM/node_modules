export const c_modal_box_MaxHeight: {
  "name": "--pf-c-modal-box--MaxHeight",
  "value": "calc(100% - 3rem)",
  "var": "var(--pf-c-modal-box--MaxHeight)"
};
export default c_modal_box_MaxHeight;
"use strict";
exports.__esModule = true;
exports.global_palette_cyan_300 = {
  "name": "--pf-global--palette--cyan-300",
  "value": "#009596",
  "var": "var(--pf-global--palette--cyan-300)"
};
exports["default"] = exports.global_palette_cyan_300;
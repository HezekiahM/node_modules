export const c_nav_m_light__subnav__link_focus_after_BorderColor: {
  "name": "--pf-c-nav--m-light__subnav__link--focus--after--BorderColor",
  "value": "#d2d2d2",
  "var": "var(--pf-c-nav--m-light__subnav__link--focus--after--BorderColor)"
};
export default c_nav_m_light__subnav__link_focus_after_BorderColor;
"use strict";
exports.__esModule = true;
exports.c_button_m_secondary_hover_BorderColor = {
  "name": "--pf-c-button--m-secondary--hover--BorderColor",
  "value": "#fff",
  "var": "var(--pf-c-button--m-secondary--hover--BorderColor)"
};
exports["default"] = exports.c_button_m_secondary_hover_BorderColor;
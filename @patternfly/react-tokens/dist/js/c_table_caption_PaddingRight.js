"use strict";
exports.__esModule = true;
exports.c_table_caption_PaddingRight = {
  "name": "--pf-c-table-caption--PaddingRight",
  "value": "1.5rem",
  "var": "var(--pf-c-table-caption--PaddingRight)"
};
exports["default"] = exports.c_table_caption_PaddingRight;
"use strict";
exports.__esModule = true;
exports.global_link_Color = {
  "name": "--pf-global--link--Color",
  "value": "#73bcf7",
  "var": "var(--pf-global--link--Color)"
};
exports["default"] = exports.global_link_Color;
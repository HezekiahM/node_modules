"use strict";
exports.__esModule = true;
exports.c_drawer_child_m_padding_md_PaddingLeft = {
  "name": "--pf-c-drawer--child--m-padding--md--PaddingLeft",
  "value": "1.5rem",
  "var": "var(--pf-c-drawer--child--m-padding--md--PaddingLeft)"
};
exports["default"] = exports.c_drawer_child_m_padding_md_PaddingLeft;
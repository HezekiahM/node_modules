export const c_nav_m_horizontal__link_m_current_BackgroundColor: {
  "name": "--pf-c-nav--m-horizontal__link--m-current--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-nav--m-horizontal__link--m-current--BackgroundColor)"
};
export default c_nav_m_horizontal__link_m_current_BackgroundColor;
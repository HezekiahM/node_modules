"use strict";
exports.__esModule = true;
exports.c_content_h3_FontSize = {
  "name": "--pf-c-content--h3--FontSize",
  "value": "1.125rem",
  "var": "var(--pf-c-content--h3--FontSize)"
};
exports["default"] = exports.c_content_h3_FontSize;
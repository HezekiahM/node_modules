"use strict";
exports.__esModule = true;
exports.c_empty_state_m_xl__body_FontSize = {
  "name": "--pf-c-empty-state--m-xl__body--FontSize",
  "value": "1.25rem",
  "var": "var(--pf-c-empty-state--m-xl__body--FontSize)"
};
exports["default"] = exports.c_empty_state_m_xl__body_FontSize;
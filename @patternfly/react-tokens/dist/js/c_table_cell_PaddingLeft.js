"use strict";
exports.__esModule = true;
exports.c_table_cell_PaddingLeft = {
  "name": "--pf-c-table--cell--PaddingLeft",
  "value": "4rem",
  "var": "var(--pf-c-table--cell--PaddingLeft)"
};
exports["default"] = exports.c_table_cell_PaddingLeft;
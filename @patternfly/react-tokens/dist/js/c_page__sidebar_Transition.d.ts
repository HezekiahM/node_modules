export const c_page__sidebar_Transition: {
  "name": "--pf-c-page__sidebar--Transition",
  "value": "all 250ms cubic-bezier(.42, 0, .58, 1)",
  "var": "var(--pf-c-page__sidebar--Transition)"
};
export default c_page__sidebar_Transition;
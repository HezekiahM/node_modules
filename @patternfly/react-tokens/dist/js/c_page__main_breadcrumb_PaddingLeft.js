"use strict";
exports.__esModule = true;
exports.c_page__main_breadcrumb_PaddingLeft = {
  "name": "--pf-c-page__main-breadcrumb--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-page__main-breadcrumb--PaddingLeft)"
};
exports["default"] = exports.c_page__main_breadcrumb_PaddingLeft;
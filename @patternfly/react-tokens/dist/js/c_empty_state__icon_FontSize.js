"use strict";
exports.__esModule = true;
exports.c_empty_state__icon_FontSize = {
  "name": "--pf-c-empty-state__icon--FontSize",
  "value": "6.25rem",
  "var": "var(--pf-c-empty-state__icon--FontSize)"
};
exports["default"] = exports.c_empty_state__icon_FontSize;
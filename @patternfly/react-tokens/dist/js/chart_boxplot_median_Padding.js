"use strict";
exports.__esModule = true;
exports.chart_boxplot_median_Padding = {
  "name": "--pf-chart-boxplot--median--Padding",
  "value": 8,
  "var": "var(--pf-chart-boxplot--median--Padding)"
};
exports["default"] = exports.chart_boxplot_median_Padding;
"use strict";
exports.__esModule = true;
exports.global_palette_black_1000 = {
  "name": "--pf-global--palette--black-1000",
  "value": "#030303",
  "var": "var(--pf-global--palette--black-1000)"
};
exports["default"] = exports.global_palette_black_1000;
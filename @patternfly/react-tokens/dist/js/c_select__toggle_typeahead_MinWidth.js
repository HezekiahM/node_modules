"use strict";
exports.__esModule = true;
exports.c_select__toggle_typeahead_MinWidth = {
  "name": "--pf-c-select__toggle-typeahead--MinWidth",
  "value": "7.5rem",
  "var": "var(--pf-c-select__toggle-typeahead--MinWidth)"
};
exports["default"] = exports.c_select__toggle_typeahead_MinWidth;
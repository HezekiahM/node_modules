"use strict";
exports.__esModule = true;
exports.c_tabs__scroll_button_TransitionDuration_margin = {
  "name": "--pf-c-tabs__scroll-button--TransitionDuration--margin",
  "value": ".125s",
  "var": "var(--pf-c-tabs__scroll-button--TransitionDuration--margin)"
};
exports["default"] = exports.c_tabs__scroll_button_TransitionDuration_margin;
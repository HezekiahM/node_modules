"use strict";
exports.__esModule = true;
exports.c_form__actions_child_MarginBottom = {
  "name": "--pf-c-form__actions--child--MarginBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-form__actions--child--MarginBottom)"
};
exports["default"] = exports.c_form__actions_child_MarginBottom;
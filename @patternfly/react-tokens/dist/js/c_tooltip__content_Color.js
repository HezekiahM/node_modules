"use strict";
exports.__esModule = true;
exports.c_tooltip__content_Color = {
  "name": "--pf-c-tooltip__content--Color",
  "value": "#fff",
  "var": "var(--pf-c-tooltip__content--Color)"
};
exports["default"] = exports.c_tooltip__content_Color;
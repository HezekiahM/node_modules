"use strict";
exports.__esModule = true;
exports.c_label_BackgroundColor = {
  "name": "--pf-c-label--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-label--BackgroundColor)"
};
exports["default"] = exports.c_label_BackgroundColor;
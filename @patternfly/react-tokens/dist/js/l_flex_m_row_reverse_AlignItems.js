"use strict";
exports.__esModule = true;
exports.l_flex_m_row_reverse_AlignItems = {
  "name": "--pf-l-flex--m-row-reverse--AlignItems",
  "value": "baseline",
  "var": "var(--pf-l-flex--m-row-reverse--AlignItems)"
};
exports["default"] = exports.l_flex_m_row_reverse_AlignItems;
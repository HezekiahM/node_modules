"use strict";
exports.__esModule = true;
exports.c_context_selector__menu_BackgroundColor = {
  "name": "--pf-c-context-selector__menu--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-context-selector__menu--BackgroundColor)"
};
exports["default"] = exports.c_context_selector__menu_BackgroundColor;
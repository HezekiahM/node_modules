export const c_page__sidebar_m_light_BackgroundColor: {
  "name": "--pf-c-page__sidebar--m-light--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-page__sidebar--m-light--BackgroundColor)"
};
export default c_page__sidebar_m_light_BackgroundColor;
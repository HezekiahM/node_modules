"use strict";
exports.__esModule = true;
exports.c_alert__title_Color = {
  "name": "--pf-c-alert__title--Color",
  "value": "#002952",
  "var": "var(--pf-c-alert__title--Color)"
};
exports["default"] = exports.c_alert__title_Color;
"use strict";
exports.__esModule = true;
exports.global_BorderWidth_xl = {
  "name": "--pf-global--BorderWidth--xl",
  "value": "4px",
  "var": "var(--pf-global--BorderWidth--xl)"
};
exports["default"] = exports.global_BorderWidth_xl;
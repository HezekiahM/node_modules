export const global_BoxShadow_lg_right: {
  "name": "--pf-global--BoxShadow--lg-right",
  "value": "0.75rem 0 0.75rem -0.5rem rgba(3, 3, 3, 0.18)",
  "var": "var(--pf-global--BoxShadow--lg-right)"
};
export default global_BoxShadow_lg_right;
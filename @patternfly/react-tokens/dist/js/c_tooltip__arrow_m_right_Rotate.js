"use strict";
exports.__esModule = true;
exports.c_tooltip__arrow_m_right_Rotate = {
  "name": "--pf-c-tooltip__arrow--m-right--Rotate",
  "value": "45deg",
  "var": "var(--pf-c-tooltip__arrow--m-right--Rotate)"
};
exports["default"] = exports.c_tooltip__arrow_m_right_Rotate;
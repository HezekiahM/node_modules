export const c_button_m_control_hover_Color: {
  "name": "--pf-c-button--m-control--hover--Color",
  "value": "#151515",
  "var": "var(--pf-c-button--m-control--hover--Color)"
};
export default c_button_m_control_hover_Color;
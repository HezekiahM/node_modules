"use strict";
exports.__esModule = true;
exports.c_card__footer_FontSize = {
  "name": "--pf-c-card__footer--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-card__footer--FontSize)"
};
exports["default"] = exports.c_card__footer_FontSize;
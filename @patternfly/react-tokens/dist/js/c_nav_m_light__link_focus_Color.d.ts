export const c_nav_m_light__link_focus_Color: {
  "name": "--pf-c-nav--m-light__link--focus--Color",
  "value": "#151515",
  "var": "var(--pf-c-nav--m-light__link--focus--Color)"
};
export default c_nav_m_light__link_focus_Color;
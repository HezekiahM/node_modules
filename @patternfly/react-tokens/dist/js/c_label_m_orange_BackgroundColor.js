"use strict";
exports.__esModule = true;
exports.c_label_m_orange_BackgroundColor = {
  "name": "--pf-c-label--m-orange--BackgroundColor",
  "value": "#fdf7e7",
  "var": "var(--pf-c-label--m-orange--BackgroundColor)"
};
exports["default"] = exports.c_label_m_orange_BackgroundColor;
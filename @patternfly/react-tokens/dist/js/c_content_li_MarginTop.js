"use strict";
exports.__esModule = true;
exports.c_content_li_MarginTop = {
  "name": "--pf-c-content--li--MarginTop",
  "value": "0.5rem",
  "var": "var(--pf-c-content--li--MarginTop)"
};
exports["default"] = exports.c_content_li_MarginTop;
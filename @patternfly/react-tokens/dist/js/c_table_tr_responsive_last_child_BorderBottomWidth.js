"use strict";
exports.__esModule = true;
exports.c_table_tr_responsive_last_child_BorderBottomWidth = {
  "name": "--pf-c-table-tr--responsive--last-child--BorderBottomWidth",
  "value": "1px",
  "var": "var(--pf-c-table-tr--responsive--last-child--BorderBottomWidth)"
};
exports["default"] = exports.c_table_tr_responsive_last_child_BorderBottomWidth;
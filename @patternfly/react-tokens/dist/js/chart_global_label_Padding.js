"use strict";
exports.__esModule = true;
exports.chart_global_label_Padding = {
  "name": "--pf-chart-global--label--Padding",
  "value": 10,
  "var": "var(--pf-chart-global--label--Padding)"
};
exports["default"] = exports.chart_global_label_Padding;
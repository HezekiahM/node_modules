export const c_search_input__utilities_MarginLeft: {
  "name": "--pf-c-search-input__utilities--MarginLeft",
  "value": "0.25rem",
  "var": "var(--pf-c-search-input__utilities--MarginLeft)"
};
export default c_search_input__utilities_MarginLeft;
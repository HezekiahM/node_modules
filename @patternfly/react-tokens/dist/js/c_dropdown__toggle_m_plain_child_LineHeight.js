"use strict";
exports.__esModule = true;
exports.c_dropdown__toggle_m_plain_child_LineHeight = {
  "name": "--pf-c-dropdown__toggle--m-plain--child--LineHeight",
  "value": "1.5",
  "var": "var(--pf-c-dropdown__toggle--m-plain--child--LineHeight)"
};
exports["default"] = exports.c_dropdown__toggle_m_plain_child_LineHeight;
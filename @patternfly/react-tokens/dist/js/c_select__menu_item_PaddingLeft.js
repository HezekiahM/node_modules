"use strict";
exports.__esModule = true;
exports.c_select__menu_item_PaddingLeft = {
  "name": "--pf-c-select__menu-item--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-select__menu-item--PaddingLeft)"
};
exports["default"] = exports.c_select__menu_item_PaddingLeft;
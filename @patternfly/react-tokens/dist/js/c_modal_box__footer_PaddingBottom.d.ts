export const c_modal_box__footer_PaddingBottom: {
  "name": "--pf-c-modal-box__footer--PaddingBottom",
  "value": "1.5rem",
  "var": "var(--pf-c-modal-box__footer--PaddingBottom)"
};
export default c_modal_box__footer_PaddingBottom;
"use strict";
exports.__esModule = true;
exports.global_primary_color_200 = {
  "name": "--pf-global--primary-color--200",
  "value": "#004080",
  "var": "var(--pf-global--primary-color--200)"
};
exports["default"] = exports.global_primary_color_200;
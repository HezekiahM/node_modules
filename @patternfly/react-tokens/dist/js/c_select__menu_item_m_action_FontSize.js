"use strict";
exports.__esModule = true;
exports.c_select__menu_item_m_action_FontSize = {
  "name": "--pf-c-select__menu-item--m-action--FontSize",
  "value": "0.625rem",
  "var": "var(--pf-c-select__menu-item--m-action--FontSize)"
};
exports["default"] = exports.c_select__menu_item_m_action_FontSize;
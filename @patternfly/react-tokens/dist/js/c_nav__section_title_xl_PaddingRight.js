"use strict";
exports.__esModule = true;
exports.c_nav__section_title_xl_PaddingRight = {
  "name": "--pf-c-nav__section-title--xl--PaddingRight",
  "value": "1.5rem",
  "var": "var(--pf-c-nav__section-title--xl--PaddingRight)"
};
exports["default"] = exports.c_nav__section_title_xl_PaddingRight;
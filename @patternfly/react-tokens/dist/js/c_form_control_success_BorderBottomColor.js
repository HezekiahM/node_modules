"use strict";
exports.__esModule = true;
exports.c_form_control_success_BorderBottomColor = {
  "name": "--pf-c-form-control--success--BorderBottomColor",
  "value": "#3e8635",
  "var": "var(--pf-c-form-control--success--BorderBottomColor)"
};
exports["default"] = exports.c_form_control_success_BorderBottomColor;
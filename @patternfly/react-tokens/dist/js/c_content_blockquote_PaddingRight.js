"use strict";
exports.__esModule = true;
exports.c_content_blockquote_PaddingRight = {
  "name": "--pf-c-content--blockquote--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-content--blockquote--PaddingRight)"
};
exports["default"] = exports.c_content_blockquote_PaddingRight;
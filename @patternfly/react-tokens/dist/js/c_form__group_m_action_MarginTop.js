"use strict";
exports.__esModule = true;
exports.c_form__group_m_action_MarginTop = {
  "name": "--pf-c-form__group--m-action--MarginTop",
  "value": "2rem",
  "var": "var(--pf-c-form__group--m-action--MarginTop)"
};
exports["default"] = exports.c_form__group_m_action_MarginTop;
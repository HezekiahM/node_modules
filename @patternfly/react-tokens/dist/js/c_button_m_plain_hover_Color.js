"use strict";
exports.__esModule = true;
exports.c_button_m_plain_hover_Color = {
  "name": "--pf-c-button--m-plain--hover--Color",
  "value": "#151515",
  "var": "var(--pf-c-button--m-plain--hover--Color)"
};
exports["default"] = exports.c_button_m_plain_hover_Color;
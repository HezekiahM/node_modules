"use strict";
exports.__esModule = true;
exports.global_breakpoint_md = {
  "name": "--pf-global--breakpoint--md",
  "value": "768px",
  "var": "var(--pf-global--breakpoint--md)"
};
exports["default"] = exports.global_breakpoint_md;
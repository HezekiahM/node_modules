"use strict";
exports.__esModule = true;
exports.c_label_m_purple__content_Color = {
  "name": "--pf-c-label--m-purple__content--Color",
  "value": "#1f0066",
  "var": "var(--pf-c-label--m-purple__content--Color)"
};
exports["default"] = exports.c_label_m_purple__content_Color;
export const c_expandable_section__toggle_m_expanded_Color: {
  "name": "--pf-c-expandable-section__toggle--m-expanded--Color",
  "value": "#004080",
  "var": "var(--pf-c-expandable-section__toggle--m-expanded--Color)"
};
export default c_expandable_section__toggle_m_expanded_Color;
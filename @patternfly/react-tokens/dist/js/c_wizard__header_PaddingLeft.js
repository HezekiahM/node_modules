"use strict";
exports.__esModule = true;
exports.c_wizard__header_PaddingLeft = {
  "name": "--pf-c-wizard__header--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-wizard__header--PaddingLeft)"
};
exports["default"] = exports.c_wizard__header_PaddingLeft;
"use strict";
exports.__esModule = true;
exports.c_nav_m_light_c_divider_BackgroundColor = {
  "name": "--pf-c-nav--m-light--c-divider--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-nav--m-light--c-divider--BackgroundColor)"
};
exports["default"] = exports.c_nav_m_light_c_divider_BackgroundColor;
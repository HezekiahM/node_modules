"use strict";
exports.__esModule = true;
exports.c_content_h1_MarginTop = {
  "name": "--pf-c-content--h1--MarginTop",
  "value": "1.5rem",
  "var": "var(--pf-c-content--h1--MarginTop)"
};
exports["default"] = exports.c_content_h1_MarginTop;
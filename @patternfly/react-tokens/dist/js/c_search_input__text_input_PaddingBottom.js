"use strict";
exports.__esModule = true;
exports.c_search_input__text_input_PaddingBottom = {
  "name": "--pf-c-search-input__text-input--PaddingBottom",
  "value": "0.375rem",
  "var": "var(--pf-c-search-input__text-input--PaddingBottom)"
};
exports["default"] = exports.c_search_input__text_input_PaddingBottom;
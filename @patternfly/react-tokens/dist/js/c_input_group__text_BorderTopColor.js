"use strict";
exports.__esModule = true;
exports.c_input_group__text_BorderTopColor = {
  "name": "--pf-c-input-group__text--BorderTopColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-input-group__text--BorderTopColor)"
};
exports["default"] = exports.c_input_group__text_BorderTopColor;
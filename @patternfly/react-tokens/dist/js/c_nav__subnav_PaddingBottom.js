"use strict";
exports.__esModule = true;
exports.c_nav__subnav_PaddingBottom = {
  "name": "--pf-c-nav__subnav--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-nav__subnav--PaddingBottom)"
};
exports["default"] = exports.c_nav__subnav_PaddingBottom;
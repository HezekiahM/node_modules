export const c_table__expandable_row_after_BorderLeftWidth: {
  "name": "--pf-c-table__expandable-row--after--BorderLeftWidth",
  "value": "3px",
  "var": "var(--pf-c-table__expandable-row--after--BorderLeftWidth)"
};
export default c_table__expandable_row_after_BorderLeftWidth;
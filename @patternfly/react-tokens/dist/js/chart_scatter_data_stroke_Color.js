"use strict";
exports.__esModule = true;
exports.chart_scatter_data_stroke_Color = {
  "name": "--pf-chart-scatter--data--stroke--Color",
  "value": "transparent",
  "var": "var(--pf-chart-scatter--data--stroke--Color)"
};
exports["default"] = exports.chart_scatter_data_stroke_Color;
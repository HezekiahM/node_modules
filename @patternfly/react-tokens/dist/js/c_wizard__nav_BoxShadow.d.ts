export const c_wizard__nav_BoxShadow: {
  "name": "--pf-c-wizard__nav--BoxShadow",
  "value": "0 0.5rem 0.5rem -0.375rem rgba(3, 3, 3, 0.18)",
  "var": "var(--pf-c-wizard__nav--BoxShadow)"
};
export default c_wizard__nav_BoxShadow;
"use strict";
exports.__esModule = true;
exports.c_simple_list__title_PaddingLeft = {
  "name": "--pf-c-simple-list__title--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-simple-list__title--PaddingLeft)"
};
exports["default"] = exports.c_simple_list__title_PaddingLeft;
"use strict";
exports.__esModule = true;
exports.c_wizard__toggle_list_item_MarginBottom = {
  "name": "--pf-c-wizard__toggle-list-item--MarginBottom",
  "value": "0.25rem",
  "var": "var(--pf-c-wizard__toggle-list-item--MarginBottom)"
};
exports["default"] = exports.c_wizard__toggle_list_item_MarginBottom;
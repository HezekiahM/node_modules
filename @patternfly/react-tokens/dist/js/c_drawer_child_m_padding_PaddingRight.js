"use strict";
exports.__esModule = true;
exports.c_drawer_child_m_padding_PaddingRight = {
  "name": "--pf-c-drawer--child--m-padding--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-drawer--child--m-padding--PaddingRight)"
};
exports["default"] = exports.c_drawer_child_m_padding_PaddingRight;
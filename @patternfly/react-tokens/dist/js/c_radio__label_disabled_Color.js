"use strict";
exports.__esModule = true;
exports.c_radio__label_disabled_Color = {
  "name": "--pf-c-radio__label--disabled--Color",
  "value": "#6a6e73",
  "var": "var(--pf-c-radio__label--disabled--Color)"
};
exports["default"] = exports.c_radio__label_disabled_Color;
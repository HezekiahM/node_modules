"use strict";
exports.__esModule = true;
exports.c_app_launcher__group_title_FontWeight = {
  "name": "--pf-c-app-launcher__group-title--FontWeight",
  "value": "700",
  "var": "var(--pf-c-app-launcher__group-title--FontWeight)"
};
exports["default"] = exports.c_app_launcher__group_title_FontWeight;
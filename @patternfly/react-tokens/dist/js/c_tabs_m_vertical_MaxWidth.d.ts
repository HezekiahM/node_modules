export const c_tabs_m_vertical_MaxWidth: {
  "name": "--pf-c-tabs--m-vertical--MaxWidth",
  "value": "15.625rem",
  "var": "var(--pf-c-tabs--m-vertical--MaxWidth)"
};
export default c_tabs_m_vertical_MaxWidth;
export const c_modal_box__header_body_PaddingTop: {
  "name": "--pf-c-modal-box__header--body--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-modal-box__header--body--PaddingTop)"
};
export default c_modal_box__header_body_PaddingTop;
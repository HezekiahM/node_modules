"use strict";
exports.__esModule = true;
exports.c_card_first_child_PaddingTop = {
  "name": "--pf-c-card--first-child--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-card--first-child--PaddingTop)"
};
exports["default"] = exports.c_card_first_child_PaddingTop;
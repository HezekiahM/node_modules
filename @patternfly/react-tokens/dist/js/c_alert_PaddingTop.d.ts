export const c_alert_PaddingTop: {
  "name": "--pf-c-alert--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-alert--PaddingTop)"
};
export default c_alert_PaddingTop;
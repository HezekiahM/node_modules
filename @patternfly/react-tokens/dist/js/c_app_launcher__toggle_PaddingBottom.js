"use strict";
exports.__esModule = true;
exports.c_app_launcher__toggle_PaddingBottom = {
  "name": "--pf-c-app-launcher__toggle--PaddingBottom",
  "value": "0.375rem",
  "var": "var(--pf-c-app-launcher__toggle--PaddingBottom)"
};
exports["default"] = exports.c_app_launcher__toggle_PaddingBottom;
"use strict";
exports.__esModule = true;
exports.c_chip__c_button_PaddingBottom = {
  "name": "--pf-c-chip__c-button--PaddingBottom",
  "value": "0.25rem",
  "var": "var(--pf-c-chip__c-button--PaddingBottom)"
};
exports["default"] = exports.c_chip__c_button_PaddingBottom;
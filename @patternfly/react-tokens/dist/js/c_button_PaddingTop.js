"use strict";
exports.__esModule = true;
exports.c_button_PaddingTop = {
  "name": "--pf-c-button--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-button--PaddingTop)"
};
exports["default"] = exports.c_button_PaddingTop;
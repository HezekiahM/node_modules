"use strict";
exports.__esModule = true;
exports.chart_bullet_comparative_measure_error_Fill_Color = {
  "name": "--pf-chart-bullet--comparative-measure--error--Fill--Color",
  "value": "#c9190b",
  "var": "var(--pf-chart-bullet--comparative-measure--error--Fill--Color)"
};
exports["default"] = exports.chart_bullet_comparative_measure_error_Fill_Color;
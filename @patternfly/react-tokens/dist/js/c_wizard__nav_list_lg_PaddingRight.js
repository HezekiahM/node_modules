"use strict";
exports.__esModule = true;
exports.c_wizard__nav_list_lg_PaddingRight = {
  "name": "--pf-c-wizard__nav-list--lg--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-wizard__nav-list--lg--PaddingRight)"
};
exports["default"] = exports.c_wizard__nav_list_lg_PaddingRight;
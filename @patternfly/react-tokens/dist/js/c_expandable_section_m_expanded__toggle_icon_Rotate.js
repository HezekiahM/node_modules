"use strict";
exports.__esModule = true;
exports.c_expandable_section_m_expanded__toggle_icon_Rotate = {
  "name": "--pf-c-expandable-section--m-expanded__toggle-icon--Rotate",
  "value": "90deg",
  "var": "var(--pf-c-expandable-section--m-expanded__toggle-icon--Rotate)"
};
exports["default"] = exports.c_expandable_section_m_expanded__toggle_icon_Rotate;
"use strict";
exports.__esModule = true;
exports.c_dropdown__group_title_FontWeight = {
  "name": "--pf-c-dropdown__group-title--FontWeight",
  "value": "700",
  "var": "var(--pf-c-dropdown__group-title--FontWeight)"
};
exports["default"] = exports.c_dropdown__group_title_FontWeight;
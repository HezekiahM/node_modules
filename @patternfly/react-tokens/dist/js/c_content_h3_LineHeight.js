"use strict";
exports.__esModule = true;
exports.c_content_h3_LineHeight = {
  "name": "--pf-c-content--h3--LineHeight",
  "value": "1.5",
  "var": "var(--pf-c-content--h3--LineHeight)"
};
exports["default"] = exports.c_content_h3_LineHeight;
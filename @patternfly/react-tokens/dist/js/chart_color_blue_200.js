"use strict";
exports.__esModule = true;
exports.chart_color_blue_200 = {
  "name": "--pf-chart-color-blue-200",
  "value": "#519de9",
  "var": "var(--pf-chart-color-blue-200)"
};
exports["default"] = exports.chart_color_blue_200;
"use strict";
exports.__esModule = true;
exports.c_context_selector__menu_search_PaddingTop = {
  "name": "--pf-c-context-selector__menu-search--PaddingTop",
  "value": "0.5rem",
  "var": "var(--pf-c-context-selector__menu-search--PaddingTop)"
};
exports["default"] = exports.c_context_selector__menu_search_PaddingTop;
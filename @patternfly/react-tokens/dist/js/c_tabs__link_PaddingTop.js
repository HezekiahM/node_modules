"use strict";
exports.__esModule = true;
exports.c_tabs__link_PaddingTop = {
  "name": "--pf-c-tabs__link--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-tabs__link--PaddingTop)"
};
exports["default"] = exports.c_tabs__link_PaddingTop;
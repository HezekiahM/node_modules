"use strict";
exports.__esModule = true;
exports.global_palette_black_700 = {
  "name": "--pf-global--palette--black-700",
  "value": "#4f5255",
  "var": "var(--pf-global--palette--black-700)"
};
exports["default"] = exports.global_palette_black_700;
export const c_card__title_not_last_child_PaddingBottom: {
  "name": "--pf-c-card__title--not--last-child--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-card__title--not--last-child--PaddingBottom)"
};
export default c_card__title_not_last_child_PaddingBottom;
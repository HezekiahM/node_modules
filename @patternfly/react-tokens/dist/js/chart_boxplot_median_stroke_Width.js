"use strict";
exports.__esModule = true;
exports.chart_boxplot_median_stroke_Width = {
  "name": "--pf-chart-boxplot--median--stroke--Width",
  "value": 1,
  "var": "var(--pf-chart-boxplot--median--stroke--Width)"
};
exports["default"] = exports.chart_boxplot_median_stroke_Width;
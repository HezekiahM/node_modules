"use strict";
exports.__esModule = true;
exports.chart_bullet_comparative_measure_warning_Width = {
  "name": "--pf-chart-bullet--comparative-measure--warning--Width",
  "value": 30,
  "var": "var(--pf-chart-bullet--comparative-measure--warning--Width)"
};
exports["default"] = exports.chart_bullet_comparative_measure_warning_Width;
"use strict";
exports.__esModule = true;
exports.c_divider_m_vertical_after_FlexBasis = {
  "name": "--pf-c-divider--m-vertical--after--FlexBasis",
  "value": "100%",
  "var": "var(--pf-c-divider--m-vertical--after--FlexBasis)"
};
exports["default"] = exports.c_divider_m_vertical_after_FlexBasis;
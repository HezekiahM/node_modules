"use strict";
exports.__esModule = true;
exports.c_table__button_active_Color = {
  "name": "--pf-c-table__button--active--Color",
  "value": "#004080",
  "var": "var(--pf-c-table__button--active--Color)"
};
exports["default"] = exports.c_table__button_active_Color;
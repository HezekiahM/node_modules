"use strict";
exports.__esModule = true;
exports.chart_errorbar_BorderWidth = {
  "name": "--pf-chart-errorbar--BorderWidth",
  "value": 8,
  "var": "var(--pf-chart-errorbar--BorderWidth)"
};
exports["default"] = exports.chart_errorbar_BorderWidth;
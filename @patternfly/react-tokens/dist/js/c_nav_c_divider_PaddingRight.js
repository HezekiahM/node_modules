"use strict";
exports.__esModule = true;
exports.c_nav_c_divider_PaddingRight = {
  "name": "--pf-c-nav--c-divider--PaddingRight",
  "value": "1.5rem",
  "var": "var(--pf-c-nav--c-divider--PaddingRight)"
};
exports["default"] = exports.c_nav_c_divider_PaddingRight;
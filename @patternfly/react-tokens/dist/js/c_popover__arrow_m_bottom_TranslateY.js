"use strict";
exports.__esModule = true;
exports.c_popover__arrow_m_bottom_TranslateY = {
  "name": "--pf-c-popover__arrow--m-bottom--TranslateY",
  "value": "-50%",
  "var": "var(--pf-c-popover__arrow--m-bottom--TranslateY)"
};
exports["default"] = exports.c_popover__arrow_m_bottom_TranslateY;
"use strict";
exports.__esModule = true;
exports.c_alert_m_inline_BackgroundColor = {
  "name": "--pf-c-alert--m-inline--BackgroundColor",
  "value": "#e7f1fa",
  "var": "var(--pf-c-alert--m-inline--BackgroundColor)"
};
exports["default"] = exports.c_alert_m_inline_BackgroundColor;
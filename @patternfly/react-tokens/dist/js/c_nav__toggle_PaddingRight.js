"use strict";
exports.__esModule = true;
exports.c_nav__toggle_PaddingRight = {
  "name": "--pf-c-nav__toggle--PaddingRight",
  "value": "0.5rem",
  "var": "var(--pf-c-nav__toggle--PaddingRight)"
};
exports["default"] = exports.c_nav__toggle_PaddingRight;
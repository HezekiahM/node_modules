export const c_select__menu_m_top_TranslateY: {
  "name": "--pf-c-select__menu--m-top--TranslateY",
  "value": "calc(-100% - 0.25rem)",
  "var": "var(--pf-c-select__menu--m-top--TranslateY)"
};
export default c_select__menu_m_top_TranslateY;
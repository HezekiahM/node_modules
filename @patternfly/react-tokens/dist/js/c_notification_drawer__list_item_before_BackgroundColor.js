"use strict";
exports.__esModule = true;
exports.c_notification_drawer__list_item_before_BackgroundColor = {
  "name": "--pf-c-notification-drawer__list-item--before--BackgroundColor",
  "value": "#3e8635",
  "var": "var(--pf-c-notification-drawer__list-item--before--BackgroundColor)"
};
exports["default"] = exports.c_notification_drawer__list_item_before_BackgroundColor;
"use strict";
exports.__esModule = true;
exports.c_toolbar__expandable_content_PaddingBottom = {
  "name": "--pf-c-toolbar__expandable-content--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-toolbar__expandable-content--PaddingBottom)"
};
exports["default"] = exports.c_toolbar__expandable_content_PaddingBottom;
"use strict";
exports.__esModule = true;
exports.c_dropdown__toggle_image_MarginBottom = {
  "name": "--pf-c-dropdown__toggle-image--MarginBottom",
  "value": "0.25rem",
  "var": "var(--pf-c-dropdown__toggle-image--MarginBottom)"
};
exports["default"] = exports.c_dropdown__toggle_image_MarginBottom;
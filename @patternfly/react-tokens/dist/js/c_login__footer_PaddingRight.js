"use strict";
exports.__esModule = true;
exports.c_login__footer_PaddingRight = {
  "name": "--pf-c-login__footer--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-login__footer--PaddingRight)"
};
exports["default"] = exports.c_login__footer_PaddingRight;
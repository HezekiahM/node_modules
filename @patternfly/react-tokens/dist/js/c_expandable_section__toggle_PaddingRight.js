"use strict";
exports.__esModule = true;
exports.c_expandable_section__toggle_PaddingRight = {
  "name": "--pf-c-expandable-section__toggle--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-expandable-section__toggle--PaddingRight)"
};
exports["default"] = exports.c_expandable_section__toggle_PaddingRight;
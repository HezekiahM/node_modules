"use strict";
exports.__esModule = true;
exports.c_nav_m_horizontal__link_Color = {
  "name": "--pf-c-nav--m-horizontal__link--Color",
  "value": "#d2d2d2",
  "var": "var(--pf-c-nav--m-horizontal__link--Color)"
};
exports["default"] = exports.c_nav_m_horizontal__link_Color;
"use strict";
exports.__esModule = true;
exports.c_search_input__utilities_c_button_PaddingLeft = {
  "name": "--pf-c-search-input__utilities--c-button--PaddingLeft",
  "value": "0.25rem",
  "var": "var(--pf-c-search-input__utilities--c-button--PaddingLeft)"
};
exports["default"] = exports.c_search_input__utilities_c_button_PaddingLeft;
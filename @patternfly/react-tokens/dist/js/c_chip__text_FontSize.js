"use strict";
exports.__esModule = true;
exports.c_chip__text_FontSize = {
  "name": "--pf-c-chip__text--FontSize",
  "value": "0.75rem",
  "var": "var(--pf-c-chip__text--FontSize)"
};
exports["default"] = exports.c_chip__text_FontSize;
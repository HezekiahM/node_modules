"use strict";
exports.__esModule = true;
exports.c_button_PaddingBottom = {
  "name": "--pf-c-button--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-button--PaddingBottom)"
};
exports["default"] = exports.c_button_PaddingBottom;
"use strict";
exports.__esModule = true;
exports.c_select__toggle_FontWeight = {
  "name": "--pf-c-select__toggle--FontWeight",
  "value": "400",
  "var": "var(--pf-c-select__toggle--FontWeight)"
};
exports["default"] = exports.c_select__toggle_FontWeight;
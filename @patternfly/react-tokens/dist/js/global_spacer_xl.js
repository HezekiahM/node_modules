"use strict";
exports.__esModule = true;
exports.global_spacer_xl = {
  "name": "--pf-global--spacer--xl",
  "value": "2rem",
  "var": "var(--pf-global--spacer--xl)"
};
exports["default"] = exports.global_spacer_xl;
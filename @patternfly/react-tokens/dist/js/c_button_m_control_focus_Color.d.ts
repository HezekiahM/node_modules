export const c_button_m_control_focus_Color: {
  "name": "--pf-c-button--m-control--focus--Color",
  "value": "#151515",
  "var": "var(--pf-c-button--m-control--focus--Color)"
};
export default c_button_m_control_focus_Color;
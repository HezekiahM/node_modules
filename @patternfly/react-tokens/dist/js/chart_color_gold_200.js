"use strict";
exports.__esModule = true;
exports.chart_color_gold_200 = {
  "name": "--pf-chart-color-gold-200",
  "value": "#f6d173",
  "var": "var(--pf-chart-color-gold-200)"
};
exports["default"] = exports.chart_color_gold_200;
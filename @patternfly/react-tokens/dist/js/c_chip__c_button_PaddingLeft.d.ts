export const c_chip__c_button_PaddingLeft: {
  "name": "--pf-c-chip__c-button--PaddingLeft",
  "value": "0.5rem",
  "var": "var(--pf-c-chip__c-button--PaddingLeft)"
};
export default c_chip__c_button_PaddingLeft;
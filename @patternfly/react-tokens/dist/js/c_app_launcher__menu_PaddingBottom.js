"use strict";
exports.__esModule = true;
exports.c_app_launcher__menu_PaddingBottom = {
  "name": "--pf-c-app-launcher__menu--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-app-launcher__menu--PaddingBottom)"
};
exports["default"] = exports.c_app_launcher__menu_PaddingBottom;
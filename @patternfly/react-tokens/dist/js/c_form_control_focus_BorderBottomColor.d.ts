export const c_form_control_focus_BorderBottomColor: {
  "name": "--pf-c-form-control--focus--BorderBottomColor",
  "value": "#8a8d90",
  "var": "var(--pf-c-form-control--focus--BorderBottomColor)"
};
export default c_form_control_focus_BorderBottomColor;
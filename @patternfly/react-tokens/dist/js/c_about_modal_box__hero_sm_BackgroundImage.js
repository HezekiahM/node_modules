"use strict";
exports.__esModule = true;
exports.c_about_modal_box__hero_sm_BackgroundImage = {
  "name": "--pf-c-about-modal-box__hero--sm--BackgroundImage",
  "value": "url(\"../../assets/images/pfbg_992@2x.jpg\")",
  "var": "var(--pf-c-about-modal-box__hero--sm--BackgroundImage)"
};
exports["default"] = exports.c_about_modal_box__hero_sm_BackgroundImage;
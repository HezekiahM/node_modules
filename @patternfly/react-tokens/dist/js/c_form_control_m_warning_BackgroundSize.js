"use strict";
exports.__esModule = true;
exports.c_form_control_m_warning_BackgroundSize = {
  "name": "--pf-c-form-control--m-warning--BackgroundSize",
  "value": "1.25rem 1rem",
  "var": "var(--pf-c-form-control--m-warning--BackgroundSize)"
};
exports["default"] = exports.c_form_control_m_warning_BackgroundSize;
"use strict";
exports.__esModule = true;
exports.global_FontSize_xs = {
  "name": "--pf-global--FontSize--xs",
  "value": "0.75rem",
  "var": "var(--pf-global--FontSize--xs)"
};
exports["default"] = exports.global_FontSize_xs;
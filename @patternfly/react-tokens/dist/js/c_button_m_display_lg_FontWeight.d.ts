export const c_button_m_display_lg_FontWeight: {
  "name": "--pf-c-button--m-display-lg--FontWeight",
  "value": "700",
  "var": "var(--pf-c-button--m-display-lg--FontWeight)"
};
export default c_button_m_display_lg_FontWeight;
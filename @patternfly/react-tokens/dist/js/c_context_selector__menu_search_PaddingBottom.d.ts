export const c_context_selector__menu_search_PaddingBottom: {
  "name": "--pf-c-context-selector__menu-search--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-context-selector__menu-search--PaddingBottom)"
};
export default c_context_selector__menu_search_PaddingBottom;
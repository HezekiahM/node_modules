"use strict";
exports.__esModule = true;
exports.c_notification_drawer__list_item_m_read_before_BackgroundColor = {
  "name": "--pf-c-notification-drawer__list-item--m-read--before--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-notification-drawer__list-item--m-read--before--BackgroundColor)"
};
exports["default"] = exports.c_notification_drawer__list_item_m_read_before_BackgroundColor;
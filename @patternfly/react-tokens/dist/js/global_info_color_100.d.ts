export const global_info_color_100: {
  "name": "--pf-global--info-color--100",
  "value": "#2b9af3",
  "var": "var(--pf-global--info-color--100)"
};
export default global_info_color_100;
"use strict";
exports.__esModule = true;
exports.c_title_m_4xl_FontWeight = {
  "name": "--pf-c-title--m-4xl--FontWeight",
  "value": "400",
  "var": "var(--pf-c-title--m-4xl--FontWeight)"
};
exports["default"] = exports.c_title_m_4xl_FontWeight;
"use strict";
exports.__esModule = true;
exports.global_ZIndex_xl = {
  "name": "--pf-global--ZIndex--xl",
  "value": "500",
  "var": "var(--pf-global--ZIndex--xl)"
};
exports["default"] = exports.global_ZIndex_xl;
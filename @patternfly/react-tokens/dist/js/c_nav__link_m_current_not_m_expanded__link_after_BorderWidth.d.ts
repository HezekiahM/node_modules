export const c_nav__link_m_current_not_m_expanded__link_after_BorderWidth: {
  "name": "--pf-c-nav__link--m-current--not--m-expanded__link--after--BorderWidth",
  "value": "4px",
  "var": "var(--pf-c-nav__link--m-current--not--m-expanded__link--after--BorderWidth)"
};
export default c_nav__link_m_current_not_m_expanded__link_after_BorderWidth;
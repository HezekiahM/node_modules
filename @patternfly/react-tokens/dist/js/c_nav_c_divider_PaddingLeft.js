"use strict";
exports.__esModule = true;
exports.c_nav_c_divider_PaddingLeft = {
  "name": "--pf-c-nav--c-divider--PaddingLeft",
  "value": "1.5rem",
  "var": "var(--pf-c-nav--c-divider--PaddingLeft)"
};
exports["default"] = exports.c_nav_c_divider_PaddingLeft;
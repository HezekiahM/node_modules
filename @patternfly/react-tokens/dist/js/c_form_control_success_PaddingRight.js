"use strict";
exports.__esModule = true;
exports.c_form_control_success_PaddingRight = {
  "name": "--pf-c-form-control--success--PaddingRight",
  "value": "2rem",
  "var": "var(--pf-c-form-control--success--PaddingRight)"
};
exports["default"] = exports.c_form_control_success_PaddingRight;
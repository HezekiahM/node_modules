"use strict";
exports.__esModule = true;
exports.c_nav_m_horizontal__link_PaddingTop = {
  "name": "--pf-c-nav--m-horizontal__link--PaddingTop",
  "value": "0.5rem",
  "var": "var(--pf-c-nav--m-horizontal__link--PaddingTop)"
};
exports["default"] = exports.c_nav_m_horizontal__link_PaddingTop;
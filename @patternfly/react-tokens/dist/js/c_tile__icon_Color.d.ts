export const c_tile__icon_Color: {
  "name": "--pf-c-tile__icon--Color",
  "value": "#151515",
  "var": "var(--pf-c-tile__icon--Color)"
};
export default c_tile__icon_Color;
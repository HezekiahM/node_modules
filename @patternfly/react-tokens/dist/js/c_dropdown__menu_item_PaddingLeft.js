"use strict";
exports.__esModule = true;
exports.c_dropdown__menu_item_PaddingLeft = {
  "name": "--pf-c-dropdown__menu-item--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-dropdown__menu-item--PaddingLeft)"
};
exports["default"] = exports.c_dropdown__menu_item_PaddingLeft;
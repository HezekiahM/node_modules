"use strict";
exports.__esModule = true;
exports.l_flex_spacer_none = {
  "name": "--pf-l-flex--spacer--none",
  "value": "0",
  "var": "var(--pf-l-flex--spacer--none)"
};
exports["default"] = exports.l_flex_spacer_none;
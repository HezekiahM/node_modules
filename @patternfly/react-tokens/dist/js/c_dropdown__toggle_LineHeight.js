"use strict";
exports.__esModule = true;
exports.c_dropdown__toggle_LineHeight = {
  "name": "--pf-c-dropdown__toggle--LineHeight",
  "value": "1.5",
  "var": "var(--pf-c-dropdown__toggle--LineHeight)"
};
exports["default"] = exports.c_dropdown__toggle_LineHeight;
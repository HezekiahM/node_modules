export const c_banner_PaddingLeft: {
  "name": "--pf-c-banner--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-banner--PaddingLeft)"
};
export default c_banner_PaddingLeft;
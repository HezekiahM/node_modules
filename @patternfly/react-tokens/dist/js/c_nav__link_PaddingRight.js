"use strict";
exports.__esModule = true;
exports.c_nav__link_PaddingRight = {
  "name": "--pf-c-nav__link--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-nav__link--PaddingRight)"
};
exports["default"] = exports.c_nav__link_PaddingRight;
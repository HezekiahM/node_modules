"use strict";
exports.__esModule = true;
exports.c_form_control__select_BackgroundSize = {
  "name": "--pf-c-form-control__select--BackgroundSize",
  "value": ".625em",
  "var": "var(--pf-c-form-control__select--BackgroundSize)"
};
exports["default"] = exports.c_form_control__select_BackgroundSize;
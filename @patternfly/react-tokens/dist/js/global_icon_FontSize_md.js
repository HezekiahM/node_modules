"use strict";
exports.__esModule = true;
exports.global_icon_FontSize_md = {
  "name": "--pf-global--icon--FontSize--md",
  "value": "1.125rem",
  "var": "var(--pf-global--icon--FontSize--md)"
};
exports["default"] = exports.global_icon_FontSize_md;
export const c_dropdown_m_top__menu_TranslateY: {
  "name": "--pf-c-dropdown--m-top__menu--TranslateY",
  "value": "calc(-100% - 0.25rem)",
  "var": "var(--pf-c-dropdown--m-top__menu--TranslateY)"
};
export default c_dropdown_m_top__menu_TranslateY;
"use strict";
exports.__esModule = true;
exports.c_select__toggle_clear_PaddingRight = {
  "name": "--pf-c-select__toggle-clear--PaddingRight",
  "value": "0.5rem",
  "var": "var(--pf-c-select__toggle-clear--PaddingRight)"
};
exports["default"] = exports.c_select__toggle_clear_PaddingRight;
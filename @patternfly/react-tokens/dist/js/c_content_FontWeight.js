"use strict";
exports.__esModule = true;
exports.c_content_FontWeight = {
  "name": "--pf-c-content--FontWeight",
  "value": "400",
  "var": "var(--pf-c-content--FontWeight)"
};
exports["default"] = exports.c_content_FontWeight;
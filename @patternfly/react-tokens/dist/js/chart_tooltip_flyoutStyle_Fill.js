"use strict";
exports.__esModule = true;
exports.chart_tooltip_flyoutStyle_Fill = {
  "name": "--pf-chart-tooltip--flyoutStyle--Fill",
  "value": "#151515",
  "var": "var(--pf-chart-tooltip--flyoutStyle--Fill)"
};
exports["default"] = exports.chart_tooltip_flyoutStyle_Fill;
export const c_notification_drawer__group_toggle_title_MarginRight: {
  "name": "--pf-c-notification-drawer__group-toggle-title--MarginRight",
  "value": "1rem",
  "var": "var(--pf-c-notification-drawer__group-toggle-title--MarginRight)"
};
export default c_notification_drawer__group_toggle_title_MarginRight;
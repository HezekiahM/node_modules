"use strict";
exports.__esModule = true;
exports.c_badge_PaddingRight = {
  "name": "--pf-c-badge--PaddingRight",
  "value": "0.5rem",
  "var": "var(--pf-c-badge--PaddingRight)"
};
exports["default"] = exports.c_badge_PaddingRight;
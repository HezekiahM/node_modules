"use strict";
exports.__esModule = true;
exports.global_palette_light_green_400 = {
  "name": "--pf-global--palette--light-green-400",
  "value": "#92d400",
  "var": "var(--pf-global--palette--light-green-400)"
};
exports["default"] = exports.global_palette_light_green_400;
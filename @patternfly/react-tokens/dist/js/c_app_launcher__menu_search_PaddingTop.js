"use strict";
exports.__esModule = true;
exports.c_app_launcher__menu_search_PaddingTop = {
  "name": "--pf-c-app-launcher__menu-search--PaddingTop",
  "value": "0.5rem",
  "var": "var(--pf-c-app-launcher__menu-search--PaddingTop)"
};
exports["default"] = exports.c_app_launcher__menu_search_PaddingTop;
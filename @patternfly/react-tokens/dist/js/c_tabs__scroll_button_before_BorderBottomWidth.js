"use strict";
exports.__esModule = true;
exports.c_tabs__scroll_button_before_BorderBottomWidth = {
  "name": "--pf-c-tabs__scroll-button--before--BorderBottomWidth",
  "value": "1px",
  "var": "var(--pf-c-tabs__scroll-button--before--BorderBottomWidth)"
};
exports["default"] = exports.c_tabs__scroll_button_before_BorderBottomWidth;
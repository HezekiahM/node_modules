"use strict";
exports.__esModule = true;
exports.c_nav_m_tertiary__link_Color = {
  "name": "--pf-c-nav--m-tertiary__link--Color",
  "value": "#151515",
  "var": "var(--pf-c-nav--m-tertiary__link--Color)"
};
exports["default"] = exports.c_nav_m_tertiary__link_Color;
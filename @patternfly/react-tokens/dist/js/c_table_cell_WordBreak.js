"use strict";
exports.__esModule = true;
exports.c_table_cell_WordBreak = {
  "name": "--pf-c-table--cell--WordBreak",
  "value": "break-word",
  "var": "var(--pf-c-table--cell--WordBreak)"
};
exports["default"] = exports.c_table_cell_WordBreak;
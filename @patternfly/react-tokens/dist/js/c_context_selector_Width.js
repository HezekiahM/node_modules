"use strict";
exports.__esModule = true;
exports.c_context_selector_Width = {
  "name": "--pf-c-context-selector--Width",
  "value": "15.625rem",
  "var": "var(--pf-c-context-selector--Width)"
};
exports["default"] = exports.c_context_selector_Width;
"use strict";
exports.__esModule = true;
exports.c_form_control__select_BackgroundPosition = {
  "name": "--pf-c-form-control__select--BackgroundPosition",
  "value": "calc(100% - 0.5rem) center",
  "var": "var(--pf-c-form-control__select--BackgroundPosition)"
};
exports["default"] = exports.c_form_control__select_BackgroundPosition;
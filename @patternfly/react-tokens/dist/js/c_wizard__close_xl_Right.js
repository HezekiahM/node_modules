"use strict";
exports.__esModule = true;
exports.c_wizard__close_xl_Right = {
  "name": "--pf-c-wizard__close--xl--Right",
  "value": "1.5rem",
  "var": "var(--pf-c-wizard__close--xl--Right)"
};
exports["default"] = exports.c_wizard__close_xl_Right;
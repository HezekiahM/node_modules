"use strict";
exports.__esModule = true;
exports.c_data_list__item_BorderBottomColor = {
  "name": "--pf-c-data-list__item--BorderBottomColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-data-list__item--BorderBottomColor)"
};
exports["default"] = exports.c_data_list__item_BorderBottomColor;
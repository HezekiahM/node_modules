"use strict";
exports.__esModule = true;
exports.c_banner_Color = {
  "name": "--pf-c-banner--Color",
  "value": "#151515",
  "var": "var(--pf-c-banner--Color)"
};
exports["default"] = exports.c_banner_Color;
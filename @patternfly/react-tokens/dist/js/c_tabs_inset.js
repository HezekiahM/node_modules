"use strict";
exports.__esModule = true;
exports.c_tabs_inset = {
  "name": "--pf-c-tabs--inset",
  "value": "3rem",
  "var": "var(--pf-c-tabs--inset)"
};
exports["default"] = exports.c_tabs_inset;
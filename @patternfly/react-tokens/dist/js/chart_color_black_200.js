"use strict";
exports.__esModule = true;
exports.chart_color_black_200 = {
  "name": "--pf-chart-color-black-200",
  "value": "#d2d2d2",
  "var": "var(--pf-chart-color-black-200)"
};
exports["default"] = exports.chart_color_black_200;
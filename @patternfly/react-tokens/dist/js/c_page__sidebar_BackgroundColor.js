"use strict";
exports.__esModule = true;
exports.c_page__sidebar_BackgroundColor = {
  "name": "--pf-c-page__sidebar--BackgroundColor",
  "value": "#212427",
  "var": "var(--pf-c-page__sidebar--BackgroundColor)"
};
exports["default"] = exports.c_page__sidebar_BackgroundColor;
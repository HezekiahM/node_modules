"use strict";
exports.__esModule = true;
exports.c_label_m_outline_BackgroundColor = {
  "name": "--pf-c-label--m-outline--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-label--m-outline--BackgroundColor)"
};
exports["default"] = exports.c_label_m_outline_BackgroundColor;
"use strict";
exports.__esModule = true;
exports.c_wizard__toggle_list_item_not_last_child_MarginRight = {
  "name": "--pf-c-wizard__toggle-list-item--not-last-child--MarginRight",
  "value": "0.5rem",
  "var": "var(--pf-c-wizard__toggle-list-item--not-last-child--MarginRight)"
};
exports["default"] = exports.c_wizard__toggle_list_item_not_last_child_MarginRight;
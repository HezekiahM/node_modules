"use strict";
exports.__esModule = true;
exports.c_form_control_m_search_BackgroundSize = {
  "name": "--pf-c-form-control--m-search--BackgroundSize",
  "value": "1rem 1rem",
  "var": "var(--pf-c-form-control--m-search--BackgroundSize)"
};
exports["default"] = exports.c_form_control_m_search_BackgroundSize;
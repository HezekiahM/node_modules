"use strict";
exports.__esModule = true;
exports.l_flex_spacer_3xl = {
  "name": "--pf-l-flex--spacer--3xl",
  "value": "4rem",
  "var": "var(--pf-l-flex--spacer--3xl)"
};
exports["default"] = exports.l_flex_spacer_3xl;
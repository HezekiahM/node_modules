"use strict";
exports.__esModule = true;
exports.chart_boxplot_lower_quartile_Padding = {
  "name": "--pf-chart-boxplot--lower-quartile--Padding",
  "value": 8,
  "var": "var(--pf-chart-boxplot--lower-quartile--Padding)"
};
exports["default"] = exports.chart_boxplot_lower_quartile_Padding;
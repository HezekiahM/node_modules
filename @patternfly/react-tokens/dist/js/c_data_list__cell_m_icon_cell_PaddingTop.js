"use strict";
exports.__esModule = true;
exports.c_data_list__cell_m_icon_cell_PaddingTop = {
  "name": "--pf-c-data-list__cell--m-icon--cell--PaddingTop",
  "value": "1.5rem",
  "var": "var(--pf-c-data-list__cell--m-icon--cell--PaddingTop)"
};
exports["default"] = exports.c_data_list__cell_m_icon_cell_PaddingTop;
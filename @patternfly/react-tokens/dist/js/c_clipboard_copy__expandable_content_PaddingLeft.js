"use strict";
exports.__esModule = true;
exports.c_clipboard_copy__expandable_content_PaddingLeft = {
  "name": "--pf-c-clipboard-copy__expandable-content--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-clipboard-copy__expandable-content--PaddingLeft)"
};
exports["default"] = exports.c_clipboard_copy__expandable_content_PaddingLeft;
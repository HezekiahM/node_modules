"use strict";
exports.__esModule = true;
exports.c_overflow_menu__group_m_button_group_spacer = {
  "name": "--pf-c-overflow-menu__group--m-button-group--spacer",
  "value": "1rem",
  "var": "var(--pf-c-overflow-menu__group--m-button-group--spacer)"
};
exports["default"] = exports.c_overflow_menu__group_m_button_group_spacer;
"use strict";
exports.__esModule = true;
exports.c_title_m_md_FontSize = {
  "name": "--pf-c-title--m-md--FontSize",
  "value": "1rem",
  "var": "var(--pf-c-title--m-md--FontSize)"
};
exports["default"] = exports.c_title_m_md_FontSize;
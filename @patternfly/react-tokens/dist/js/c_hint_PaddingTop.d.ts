export const c_hint_PaddingTop: {
  "name": "--pf-c-hint--PaddingTop",
  "value": "1.5rem",
  "var": "var(--pf-c-hint--PaddingTop)"
};
export default c_hint_PaddingTop;
"use strict";
exports.__esModule = true;
exports.c_button_m_plain_focus_BackgroundColor = {
  "name": "--pf-c-button--m-plain--focus--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-button--m-plain--focus--BackgroundColor)"
};
exports["default"] = exports.c_button_m_plain_focus_BackgroundColor;
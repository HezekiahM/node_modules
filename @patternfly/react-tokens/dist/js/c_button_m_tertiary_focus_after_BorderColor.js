"use strict";
exports.__esModule = true;
exports.c_button_m_tertiary_focus_after_BorderColor = {
  "name": "--pf-c-button--m-tertiary--focus--after--BorderColor",
  "value": "#151515",
  "var": "var(--pf-c-button--m-tertiary--focus--after--BorderColor)"
};
exports["default"] = exports.c_button_m_tertiary_focus_after_BorderColor;
"use strict";
exports.__esModule = true;
exports.c_alert = {
  ".pf-c-alert": {
    "global_Color_100": {
      "name": "--pf-global--Color--100",
      "value": "#151515",
      "values": [
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "global_Color_200": {
      "name": "--pf-global--Color--200",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--dark-200",
        "$pf-global--Color--dark-200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "global_BorderColor_100": {
      "name": "--pf-global--BorderColor--100",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--dark-100",
        "$pf-global--BorderColor--dark-100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "global_primary_color_100": {
      "name": "--pf-global--primary-color--100",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--dark-100",
        "$pf-global--primary-color--dark-100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "global_link_Color": {
      "name": "--pf-global--link--Color",
      "value": "#06c",
      "values": [
        "--pf-global--link--Color--dark",
        "$pf-global--link--Color--dark",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "global_link_Color_hover": {
      "name": "--pf-global--link--Color--hover",
      "value": "#004080",
      "values": [
        "--pf-global--link--Color--dark--hover",
        "$pf-global--link--Color--dark--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "global_BackgroundColor_100": {
      "name": "--pf-global--BackgroundColor--100",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--light-100",
        "$pf-global--BackgroundColor--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_alert_BoxShadow": {
      "name": "--pf-c-alert--BoxShadow",
      "value": "0 0.5rem 1rem 0 rgba(3, 3, 3, 0.16), 0 0 0.375rem 0 rgba(3, 3, 3, 0.08)",
      "values": [
        "--pf-global--BoxShadow--lg",
        "$pf-global--BoxShadow--lg",
        "0 pf-size-prem(8px) pf-size-prem(16px) 0 rgba($pf-color-black-1000, .16), 0 0 pf-size-prem(6px) 0 rgba($pf-color-black-1000, .08)",
        "0 pf-size-prem(8px) pf-size-prem(16px) 0 rgba(#030303, .16), 0 0 pf-size-prem(6px) 0 rgba(#030303, .08)",
        "0 0.5rem 1rem 0 rgba(3, 3, 3, 0.16), 0 0 0.375rem 0 rgba(3, 3, 3, 0.08)"
      ]
    },
    "c_alert_BackgroundColor": {
      "name": "--pf-c-alert--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_alert_GridTemplateColumns": {
      "name": "--pf-c-alert--GridTemplateColumns",
      "value": "max-content 1fr max-content"
    },
    "c_alert_BorderTopWidth": {
      "name": "--pf-c-alert--BorderTopWidth",
      "value": "2px",
      "values": [
        "--pf-global--BorderWidth--md",
        "$pf-global--BorderWidth--md",
        "2px"
      ]
    },
    "c_alert_BorderTopColor": {
      "name": "--pf-c-alert--BorderTopColor",
      "value": "#009596",
      "values": [
        "--pf-global--default-color--200",
        "$pf-global--default-color--200",
        "$pf-color-cyan-300",
        "#009596"
      ]
    },
    "c_alert_PaddingTop": {
      "name": "--pf-c-alert--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_alert_PaddingRight": {
      "name": "--pf-c-alert--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_alert_PaddingBottom": {
      "name": "--pf-c-alert--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_alert_PaddingLeft": {
      "name": "--pf-c-alert--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_alert__FontSize": {
      "name": "--pf-c-alert__FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_alert__icon_Color": {
      "name": "--pf-c-alert__icon--Color",
      "value": "#009596",
      "values": [
        "--pf-global--default-color--200",
        "$pf-global--default-color--200",
        "$pf-color-cyan-300",
        "#009596"
      ]
    },
    "c_alert__icon_MarginTop": {
      "name": "--pf-c-alert__icon--MarginTop",
      "value": "0.0625rem"
    },
    "c_alert__icon_MarginRight": {
      "name": "--pf-c-alert__icon--MarginRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_alert__icon_FontSize": {
      "name": "--pf-c-alert__icon--FontSize",
      "value": "1.125rem",
      "values": [
        "--pf-global--icon--FontSize--md",
        "$pf-global--icon--FontSize--md",
        "pf-font-prem(18px)",
        "1.125rem"
      ]
    },
    "c_alert__title_FontWeight": {
      "name": "--pf-c-alert__title--FontWeight",
      "value": "700",
      "values": [
        "--pf-global--FontWeight--bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    },
    "c_alert__title_Color": {
      "name": "--pf-c-alert__title--Color",
      "value": "#003737",
      "values": [
        "--pf-global--default-color--300",
        "$pf-global--default-color--300",
        "$pf-color-cyan-500",
        "#003737"
      ]
    },
    "c_alert__title_max_lines": {
      "name": "--pf-c-alert__title--max-lines",
      "value": "1"
    },
    "c_alert__action_MarginTop": {
      "name": "--pf-c-alert__action--MarginTop",
      "value": "calc(0.375rem * -1)",
      "values": [
        "calc(--pf-global--spacer--form-element * -1)",
        "calc($pf-global--spacer--form-element * -1)",
        "calc(pf-size-prem(6px) * -1)",
        "calc(0.375rem * -1)"
      ]
    },
    "c_alert__action_MarginBottom": {
      "name": "--pf-c-alert__action--MarginBottom",
      "value": "calc(0.375rem * -1)",
      "values": [
        "calc(--pf-global--spacer--form-element * -1)",
        "calc($pf-global--spacer--form-element * -1)",
        "calc(pf-size-prem(6px) * -1)",
        "calc(0.375rem * -1)"
      ]
    },
    "c_alert__action_TranslateY": {
      "name": "--pf-c-alert__action--TranslateY",
      "value": "0.125rem"
    },
    "c_alert__action_MarginRight": {
      "name": "--pf-c-alert__action--MarginRight",
      "value": "calc(0.5rem * -1)",
      "values": [
        "calc(--pf-global--spacer--sm * -1)",
        "calc($pf-global--spacer--sm * -1)",
        "calc(pf-size-prem(8px) * -1)",
        "calc(0.5rem * -1)"
      ]
    },
    "c_alert__description_PaddingTop": {
      "name": "--pf-c-alert__description--PaddingTop",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_alert__action_group_PaddingTop": {
      "name": "--pf-c-alert__action-group--PaddingTop",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_alert__description_action_group_PaddingTop": {
      "name": "--pf-c-alert__description--action-group--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_alert__action_group__c_button_not_last_child_MarginRight": {
      "name": "--pf-c-alert__action-group__c-button--not-last-child--MarginRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_alert_m_success_BorderTopColor": {
      "name": "--pf-c-alert--m-success--BorderTopColor",
      "value": "#3e8635",
      "values": [
        "--pf-global--success-color--100",
        "$pf-global--success-color--100",
        "$pf-color-green-500",
        "#3e8635"
      ]
    },
    "c_alert_m_success__icon_Color": {
      "name": "--pf-c-alert--m-success__icon--Color",
      "value": "#3e8635",
      "values": [
        "--pf-global--success-color--100",
        "$pf-global--success-color--100",
        "$pf-color-green-500",
        "#3e8635"
      ]
    },
    "c_alert_m_success__title_Color": {
      "name": "--pf-c-alert--m-success__title--Color",
      "value": "#0f280d",
      "values": [
        "--pf-global--success-color--200",
        "$pf-global--success-color--200",
        "$pf-color-green-700",
        "#0f280d"
      ]
    },
    "c_alert_m_danger_BorderTopColor": {
      "name": "--pf-c-alert--m-danger--BorderTopColor",
      "value": "#c9190b",
      "values": [
        "--pf-global--danger-color--100",
        "$pf-global--danger-color--100",
        "$pf-color-red-100",
        "#c9190b"
      ]
    },
    "c_alert_m_danger__icon_Color": {
      "name": "--pf-c-alert--m-danger__icon--Color",
      "value": "#c9190b",
      "values": [
        "--pf-global--danger-color--100",
        "$pf-global--danger-color--100",
        "$pf-color-red-100",
        "#c9190b"
      ]
    },
    "c_alert_m_danger__title_Color": {
      "name": "--pf-c-alert--m-danger__title--Color",
      "value": "#a30000",
      "values": [
        "--pf-global--danger-color--200",
        "$pf-global--danger-color--200",
        "$pf-color-red-200",
        "#a30000"
      ]
    },
    "c_alert_m_warning_BorderTopColor": {
      "name": "--pf-c-alert--m-warning--BorderTopColor",
      "value": "#f0ab00",
      "values": [
        "--pf-global--warning-color--100",
        "$pf-global--warning-color--100",
        "$pf-color-gold-400",
        "#f0ab00"
      ]
    },
    "c_alert_m_warning__icon_Color": {
      "name": "--pf-c-alert--m-warning__icon--Color",
      "value": "#f0ab00",
      "values": [
        "--pf-global--warning-color--100",
        "$pf-global--warning-color--100",
        "$pf-color-gold-400",
        "#f0ab00"
      ]
    },
    "c_alert_m_warning__title_Color": {
      "name": "--pf-c-alert--m-warning__title--Color",
      "value": "#795600",
      "values": [
        "--pf-global--warning-color--200",
        "$pf-global--warning-color--200",
        "$pf-color-gold-600",
        "#795600"
      ]
    },
    "c_alert_m_info_BorderTopColor": {
      "name": "--pf-c-alert--m-info--BorderTopColor",
      "value": "#2b9af3",
      "values": [
        "--pf-global--info-color--100",
        "$pf-global--info-color--100",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    },
    "c_alert_m_info__icon_Color": {
      "name": "--pf-c-alert--m-info__icon--Color",
      "value": "#2b9af3",
      "values": [
        "--pf-global--info-color--100",
        "$pf-global--info-color--100",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    },
    "c_alert_m_info__title_Color": {
      "name": "--pf-c-alert--m-info__title--Color",
      "value": "#002952",
      "values": [
        "--pf-global--info-color--200",
        "$pf-global--info-color--200",
        "$pf-color-blue-600",
        "#002952"
      ]
    },
    "c_alert_m_inline_BoxShadow": {
      "name": "--pf-c-alert--m-inline--BoxShadow",
      "value": "none"
    },
    "c_alert_m_inline_BackgroundColor": {
      "name": "--pf-c-alert--m-inline--BackgroundColor",
      "value": "#f2f9f9",
      "values": [
        "--pf-global--palette--cyan-50",
        "$pf-color-cyan-50",
        "#f2f9f9"
      ]
    },
    "c_alert_m_inline_m_success_BackgroundColor": {
      "name": "--pf-c-alert--m-inline--m-success--BackgroundColor",
      "value": "#f3faf2",
      "values": [
        "--pf-global--palette--green-50",
        "$pf-color-green-50",
        "#f3faf2"
      ]
    },
    "c_alert_m_inline_m_danger_BackgroundColor": {
      "name": "--pf-c-alert--m-inline--m-danger--BackgroundColor",
      "value": "#faeae8",
      "values": [
        "--pf-global--palette--red-50",
        "$pf-color-red-50",
        "#faeae8"
      ]
    },
    "c_alert_m_inline_m_warning_BackgroundColor": {
      "name": "--pf-c-alert--m-inline--m-warning--BackgroundColor",
      "value": "#fdf7e7",
      "values": [
        "--pf-global--palette--gold-50",
        "$pf-color-gold-50",
        "#fdf7e7"
      ]
    },
    "c_alert_m_inline_m_info_BackgroundColor": {
      "name": "--pf-c-alert--m-inline--m-info--BackgroundColor",
      "value": "#e7f1fa",
      "values": [
        "--pf-global--palette--blue-50",
        "$pf-color-blue-50",
        "#e7f1fa"
      ]
    }
  },
  ".pf-c-alert.pf-m-success": {
    "c_alert_BorderTopColor": {
      "name": "--pf-c-alert--BorderTopColor",
      "value": "#3e8635",
      "values": [
        "--pf-c-alert--m-success--BorderTopColor",
        "--pf-global--success-color--100",
        "$pf-global--success-color--100",
        "$pf-color-green-500",
        "#3e8635"
      ]
    },
    "c_alert__icon_Color": {
      "name": "--pf-c-alert__icon--Color",
      "value": "#3e8635",
      "values": [
        "--pf-c-alert--m-success__icon--Color",
        "--pf-global--success-color--100",
        "$pf-global--success-color--100",
        "$pf-color-green-500",
        "#3e8635"
      ]
    },
    "c_alert__title_Color": {
      "name": "--pf-c-alert__title--Color",
      "value": "#0f280d",
      "values": [
        "--pf-c-alert--m-success__title--Color",
        "--pf-global--success-color--200",
        "$pf-global--success-color--200",
        "$pf-color-green-700",
        "#0f280d"
      ]
    },
    "c_alert_m_inline_BackgroundColor": {
      "name": "--pf-c-alert--m-inline--BackgroundColor",
      "value": "#f3faf2",
      "values": [
        "--pf-c-alert--m-inline--m-success--BackgroundColor",
        "--pf-global--palette--green-50",
        "$pf-color-green-50",
        "#f3faf2"
      ]
    }
  },
  ".pf-c-alert.pf-m-danger": {
    "c_alert_BorderTopColor": {
      "name": "--pf-c-alert--BorderTopColor",
      "value": "#c9190b",
      "values": [
        "--pf-c-alert--m-danger--BorderTopColor",
        "--pf-global--danger-color--100",
        "$pf-global--danger-color--100",
        "$pf-color-red-100",
        "#c9190b"
      ]
    },
    "c_alert__icon_Color": {
      "name": "--pf-c-alert__icon--Color",
      "value": "#c9190b",
      "values": [
        "--pf-c-alert--m-danger__icon--Color",
        "--pf-global--danger-color--100",
        "$pf-global--danger-color--100",
        "$pf-color-red-100",
        "#c9190b"
      ]
    },
    "c_alert__title_Color": {
      "name": "--pf-c-alert__title--Color",
      "value": "#a30000",
      "values": [
        "--pf-c-alert--m-danger__title--Color",
        "--pf-global--danger-color--200",
        "$pf-global--danger-color--200",
        "$pf-color-red-200",
        "#a30000"
      ]
    },
    "c_alert_m_inline_BackgroundColor": {
      "name": "--pf-c-alert--m-inline--BackgroundColor",
      "value": "#faeae8",
      "values": [
        "--pf-c-alert--m-inline--m-danger--BackgroundColor",
        "--pf-global--palette--red-50",
        "$pf-color-red-50",
        "#faeae8"
      ]
    }
  },
  ".pf-c-alert.pf-m-warning": {
    "c_alert_BorderTopColor": {
      "name": "--pf-c-alert--BorderTopColor",
      "value": "#f0ab00",
      "values": [
        "--pf-c-alert--m-warning--BorderTopColor",
        "--pf-global--warning-color--100",
        "$pf-global--warning-color--100",
        "$pf-color-gold-400",
        "#f0ab00"
      ]
    },
    "c_alert__icon_Color": {
      "name": "--pf-c-alert__icon--Color",
      "value": "#f0ab00",
      "values": [
        "--pf-c-alert--m-warning__icon--Color",
        "--pf-global--warning-color--100",
        "$pf-global--warning-color--100",
        "$pf-color-gold-400",
        "#f0ab00"
      ]
    },
    "c_alert__title_Color": {
      "name": "--pf-c-alert__title--Color",
      "value": "#795600",
      "values": [
        "--pf-c-alert--m-warning__title--Color",
        "--pf-global--warning-color--200",
        "$pf-global--warning-color--200",
        "$pf-color-gold-600",
        "#795600"
      ]
    },
    "c_alert_m_inline_BackgroundColor": {
      "name": "--pf-c-alert--m-inline--BackgroundColor",
      "value": "#fdf7e7",
      "values": [
        "--pf-c-alert--m-inline--m-warning--BackgroundColor",
        "--pf-global--palette--gold-50",
        "$pf-color-gold-50",
        "#fdf7e7"
      ]
    }
  },
  ".pf-c-alert.pf-m-info": {
    "c_alert_BorderTopColor": {
      "name": "--pf-c-alert--BorderTopColor",
      "value": "#2b9af3",
      "values": [
        "--pf-c-alert--m-info--BorderTopColor",
        "--pf-global--info-color--100",
        "$pf-global--info-color--100",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    },
    "c_alert__icon_Color": {
      "name": "--pf-c-alert__icon--Color",
      "value": "#2b9af3",
      "values": [
        "--pf-c-alert--m-info__icon--Color",
        "--pf-global--info-color--100",
        "$pf-global--info-color--100",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    },
    "c_alert__title_Color": {
      "name": "--pf-c-alert__title--Color",
      "value": "#002952",
      "values": [
        "--pf-c-alert--m-info__title--Color",
        "--pf-global--info-color--200",
        "$pf-global--info-color--200",
        "$pf-color-blue-600",
        "#002952"
      ]
    },
    "c_alert_m_inline_BackgroundColor": {
      "name": "--pf-c-alert--m-inline--BackgroundColor",
      "value": "#e7f1fa",
      "values": [
        "--pf-c-alert--m-inline--m-info--BackgroundColor",
        "--pf-global--palette--blue-50",
        "$pf-color-blue-50",
        "#e7f1fa"
      ]
    }
  },
  ".pf-c-alert.pf-m-inline": {
    "c_alert_BoxShadow": {
      "name": "--pf-c-alert--BoxShadow",
      "value": "none",
      "values": [
        "--pf-c-alert--m-inline--BoxShadow",
        "none"
      ]
    },
    "c_alert_BackgroundColor": {
      "name": "--pf-c-alert--BackgroundColor",
      "value": "#f2f9f9",
      "values": [
        "--pf-c-alert--m-inline--BackgroundColor",
        "--pf-global--palette--cyan-50",
        "$pf-color-cyan-50",
        "#f2f9f9"
      ]
    }
  },
  ".pf-c-alert__description + .pf-c-alert__action-group": {
    "c_alert__action_group_PaddingTop": {
      "name": "--pf-c-alert__action-group--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-c-alert__description--action-group--PaddingTop",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    }
  },
  ".pf-c-alert__action > .pf-c-button": {
    "c_button_LineHeight": {
      "name": "--pf-c-button--LineHeight",
      "value": "1"
    }
  },
  ".pf-c-alert__action-group > .pf-c-button": {
    "c_button_m_link_m_inline_hover_TextDecoration": {
      "name": "--pf-c-button--m-link--m-inline--hover--TextDecoration",
      "value": "none"
    }
  },
  ".pf-m-overpass-font .pf-c-alert__title": {
    "c_alert__title_FontWeight": {
      "name": "--pf-c-alert__title--FontWeight",
      "value": "400",
      "values": [
        "--pf-global--FontWeight--normal",
        "$pf-global--FontWeight--normal",
        "400"
      ]
    }
  }
};
exports["default"] = exports.c_alert;
"use strict";
exports.__esModule = true;
exports.c_drawer__panel_BackgroundColor = {
  "name": "--pf-c-drawer__panel--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-drawer__panel--BackgroundColor)"
};
exports["default"] = exports.c_drawer__panel_BackgroundColor;
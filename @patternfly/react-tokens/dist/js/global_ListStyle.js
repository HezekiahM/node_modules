"use strict";
exports.__esModule = true;
exports.global_ListStyle = {
  "name": "--pf-global--ListStyle",
  "value": "disc outside",
  "var": "var(--pf-global--ListStyle)"
};
exports["default"] = exports.global_ListStyle;
export const c_page__main_breadcrumb_main_section_PaddingTop: {
  "name": "--pf-c-page__main-breadcrumb--main-section--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-page__main-breadcrumb--main-section--PaddingTop)"
};
export default c_page__main_breadcrumb_main_section_PaddingTop;
"use strict";
exports.__esModule = true;
exports.c_context_selector__menu_list_item_PaddingLeft = {
  "name": "--pf-c-context-selector__menu-list-item--PaddingLeft",
  "value": "1.5rem",
  "var": "var(--pf-c-context-selector__menu-list-item--PaddingLeft)"
};
exports["default"] = exports.c_context_selector__menu_list_item_PaddingLeft;
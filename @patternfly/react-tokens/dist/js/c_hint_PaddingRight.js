"use strict";
exports.__esModule = true;
exports.c_hint_PaddingRight = {
  "name": "--pf-c-hint--PaddingRight",
  "value": "1.5rem",
  "var": "var(--pf-c-hint--PaddingRight)"
};
exports["default"] = exports.c_hint_PaddingRight;
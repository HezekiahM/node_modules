"use strict";
exports.__esModule = true;
exports.c_toolbar__group_m_chip_container__item_MarginTop = {
  "name": "--pf-c-toolbar__group--m-chip-container__item--MarginTop",
  "value": "1rem",
  "var": "var(--pf-c-toolbar__group--m-chip-container__item--MarginTop)"
};
exports["default"] = exports.c_toolbar__group_m_chip_container__item_MarginTop;
"use strict";
exports.__esModule = true;
exports.c_chip__c_button_PaddingLeft = {
  "name": "--pf-c-chip__c-button--PaddingLeft",
  "value": "0.5rem",
  "var": "var(--pf-c-chip__c-button--PaddingLeft)"
};
exports["default"] = exports.c_chip__c_button_PaddingLeft;
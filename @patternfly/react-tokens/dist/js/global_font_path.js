"use strict";
exports.__esModule = true;
exports.global_font_path = {
  "name": "--pf-global--font-path",
  "value": "./assets/fonts",
  "var": "var(--pf-global--font-path)"
};
exports["default"] = exports.global_font_path;
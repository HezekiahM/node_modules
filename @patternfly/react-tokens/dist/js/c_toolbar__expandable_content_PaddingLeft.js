"use strict";
exports.__esModule = true;
exports.c_toolbar__expandable_content_PaddingLeft = {
  "name": "--pf-c-toolbar__expandable-content--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-toolbar__expandable-content--PaddingLeft)"
};
exports["default"] = exports.c_toolbar__expandable_content_PaddingLeft;
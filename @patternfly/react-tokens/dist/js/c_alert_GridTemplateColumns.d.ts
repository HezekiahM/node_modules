export const c_alert_GridTemplateColumns: {
  "name": "--pf-c-alert--GridTemplateColumns",
  "value": "max-content 1fr max-content",
  "var": "var(--pf-c-alert--GridTemplateColumns)"
};
export default c_alert_GridTemplateColumns;
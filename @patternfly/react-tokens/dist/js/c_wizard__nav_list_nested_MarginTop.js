"use strict";
exports.__esModule = true;
exports.c_wizard__nav_list_nested_MarginTop = {
  "name": "--pf-c-wizard__nav-list--nested--MarginTop",
  "value": "1rem",
  "var": "var(--pf-c-wizard__nav-list--nested--MarginTop)"
};
exports["default"] = exports.c_wizard__nav_list_nested_MarginTop;
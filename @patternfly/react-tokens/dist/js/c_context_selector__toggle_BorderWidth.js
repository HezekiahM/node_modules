"use strict";
exports.__esModule = true;
exports.c_context_selector__toggle_BorderWidth = {
  "name": "--pf-c-context-selector__toggle--BorderWidth",
  "value": "1px",
  "var": "var(--pf-c-context-selector__toggle--BorderWidth)"
};
exports["default"] = exports.c_context_selector__toggle_BorderWidth;
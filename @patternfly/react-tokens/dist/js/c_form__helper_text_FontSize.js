"use strict";
exports.__esModule = true;
exports.c_form__helper_text_FontSize = {
  "name": "--pf-c-form__helper-text--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-form__helper-text--FontSize)"
};
exports["default"] = exports.c_form__helper_text_FontSize;
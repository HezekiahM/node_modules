"use strict";
exports.__esModule = true;
exports.chart_bullet_comparative_measure_warning_stroke_Color = {
  "name": "--pf-chart-bullet--comparative-measure--warning--stroke--Color",
  "value": "#ec7a08",
  "var": "var(--pf-chart-bullet--comparative-measure--warning--stroke--Color)"
};
exports["default"] = exports.chart_bullet_comparative_measure_warning_stroke_Color;
export const c_table__compound_expansion_toggle__button_before_BorderColor: {
  "name": "--pf-c-table__compound-expansion-toggle__button--before--BorderColor",
  "value": "#d2d2d2",
  "var": "var(--pf-c-table__compound-expansion-toggle__button--before--BorderColor)"
};
export default c_table__compound_expansion_toggle__button_before_BorderColor;
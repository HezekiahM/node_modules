"use strict";
exports.__esModule = true;
exports.l_flex_spacer_base = {
  "name": "--pf-l-flex--spacer-base",
  "value": "1rem",
  "var": "var(--pf-l-flex--spacer-base)"
};
exports["default"] = exports.l_flex_spacer_base;
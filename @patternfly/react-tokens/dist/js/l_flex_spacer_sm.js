"use strict";
exports.__esModule = true;
exports.l_flex_spacer_sm = {
  "name": "--pf-l-flex--spacer--sm",
  "value": "0.5rem",
  "var": "var(--pf-l-flex--spacer--sm)"
};
exports["default"] = exports.l_flex_spacer_sm;
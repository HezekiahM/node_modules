"use strict";
exports.__esModule = true;
exports.global_palette_gold_200 = {
  "name": "--pf-global--palette--gold-200",
  "value": "#f6d173",
  "var": "var(--pf-global--palette--gold-200)"
};
exports["default"] = exports.global_palette_gold_200;
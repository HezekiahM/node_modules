"use strict";
exports.__esModule = true;
exports.c_options_menu__toggle_PaddingLeft = {
  "name": "--pf-c-options-menu__toggle--PaddingLeft",
  "value": "0.5rem",
  "var": "var(--pf-c-options-menu__toggle--PaddingLeft)"
};
exports["default"] = exports.c_options_menu__toggle_PaddingLeft;
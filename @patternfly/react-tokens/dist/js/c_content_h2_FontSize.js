"use strict";
exports.__esModule = true;
exports.c_content_h2_FontSize = {
  "name": "--pf-c-content--h2--FontSize",
  "value": "1.25rem",
  "var": "var(--pf-c-content--h2--FontSize)"
};
exports["default"] = exports.c_content_h2_FontSize;
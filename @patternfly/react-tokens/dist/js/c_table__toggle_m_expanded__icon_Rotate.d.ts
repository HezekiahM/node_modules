export const c_table__toggle_m_expanded__icon_Rotate: {
  "name": "--pf-c-table__toggle--m-expanded__icon--Rotate",
  "value": "180deg",
  "var": "var(--pf-c-table__toggle--m-expanded__icon--Rotate)"
};
export default c_table__toggle_m_expanded__icon_Rotate;
"use strict";
exports.__esModule = true;
exports.l_flex_FlexWrap = {
  "name": "--pf-l-flex--FlexWrap",
  "value": "wrap",
  "var": "var(--pf-l-flex--FlexWrap)"
};
exports["default"] = exports.l_flex_FlexWrap;
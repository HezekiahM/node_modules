"use strict";
exports.__esModule = true;
exports.c_tile_PaddingTop = {
  "name": "--pf-c-tile--PaddingTop",
  "value": "1.5rem",
  "var": "var(--pf-c-tile--PaddingTop)"
};
exports["default"] = exports.c_tile_PaddingTop;
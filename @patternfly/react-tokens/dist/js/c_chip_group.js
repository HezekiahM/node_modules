"use strict";
exports.__esModule = true;
exports.c_chip_group = {
  ".pf-c-chip-group": {
    "global_Color_100": {
      "name": "--pf-global--Color--100",
      "value": "#151515",
      "values": [
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "global_Color_200": {
      "name": "--pf-global--Color--200",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--dark-200",
        "$pf-global--Color--dark-200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "global_BorderColor_100": {
      "name": "--pf-global--BorderColor--100",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--dark-100",
        "$pf-global--BorderColor--dark-100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "global_primary_color_100": {
      "name": "--pf-global--primary-color--100",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--dark-100",
        "$pf-global--primary-color--dark-100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "global_link_Color": {
      "name": "--pf-global--link--Color",
      "value": "#06c",
      "values": [
        "--pf-global--link--Color--dark",
        "$pf-global--link--Color--dark",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "global_link_Color_hover": {
      "name": "--pf-global--link--Color--hover",
      "value": "#004080",
      "values": [
        "--pf-global--link--Color--dark--hover",
        "$pf-global--link--Color--dark--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "global_BackgroundColor_100": {
      "name": "--pf-global--BackgroundColor--100",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--light-100",
        "$pf-global--BackgroundColor--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_chip_group__list_MarginBottom": {
      "name": "--pf-c-chip-group__list--MarginBottom",
      "value": "calc(0.25rem * -1)",
      "values": [
        "calc(--pf-global--spacer--xs * -1)",
        "calc($pf-global--spacer--xs * -1)",
        "calc(pf-size-prem(4px) * -1)",
        "calc(0.25rem * -1)"
      ]
    },
    "c_chip_group__list_MarginRight": {
      "name": "--pf-c-chip-group__list--MarginRight",
      "value": "calc(0.25rem * -1)",
      "values": [
        "calc(--pf-global--spacer--xs * -1)",
        "calc($pf-global--spacer--xs * -1)",
        "calc(pf-size-prem(4px) * -1)",
        "calc(0.25rem * -1)"
      ]
    },
    "c_chip_group_m_category_PaddingTop": {
      "name": "--pf-c-chip-group--m-category--PaddingTop",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_chip_group_m_category_PaddingRight": {
      "name": "--pf-c-chip-group--m-category--PaddingRight",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_chip_group_m_category_PaddingBottom": {
      "name": "--pf-c-chip-group--m-category--PaddingBottom",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_chip_group_m_category_PaddingLeft": {
      "name": "--pf-c-chip-group--m-category--PaddingLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_chip_group_m_category_BorderRadius": {
      "name": "--pf-c-chip-group--m-category--BorderRadius",
      "value": "3px",
      "values": [
        "--pf-global--BorderRadius--sm",
        "$pf-global--BorderRadius--sm",
        "3px"
      ]
    },
    "c_chip_group_m_category_BackgroundColor": {
      "name": "--pf-c-chip-group--m-category--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--200",
        "$pf-global--BackgroundColor--200",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_chip_group__label_MarginRight": {
      "name": "--pf-c-chip-group__label--MarginRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_chip_group__label_FontSize": {
      "name": "--pf-c-chip-group__label--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_chip_group__label_MaxWidth": {
      "name": "--pf-c-chip-group__label--MaxWidth",
      "value": "18ch"
    },
    "c_chip_group__close_MarginTop": {
      "name": "--pf-c-chip-group__close--MarginTop",
      "value": "calc(0.25rem * -1)",
      "values": [
        "calc(--pf-global--spacer--xs * -1)",
        "calc($pf-global--spacer--xs * -1)",
        "calc(pf-size-prem(4px) * -1)",
        "calc(0.25rem * -1)"
      ]
    },
    "c_chip_group__close_MarginBottom": {
      "name": "--pf-c-chip-group__close--MarginBottom",
      "value": "calc(0.25rem * -1)",
      "values": [
        "calc(--pf-global--spacer--xs * -1)",
        "calc($pf-global--spacer--xs * -1)",
        "calc(pf-size-prem(4px) * -1)",
        "calc(0.25rem * -1)"
      ]
    },
    "c_chip_group__list_item_MarginRight": {
      "name": "--pf-c-chip-group__list-item--MarginRight",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_chip_group__list_item_MarginBottom": {
      "name": "--pf-c-chip-group__list-item--MarginBottom",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    }
  }
};
exports["default"] = exports.c_chip_group;
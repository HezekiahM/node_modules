export const c_tile_m_disabled_BackgroundColor: {
  "name": "--pf-c-tile--m-disabled--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-tile--m-disabled--BackgroundColor)"
};
export default c_tile_m_disabled_BackgroundColor;
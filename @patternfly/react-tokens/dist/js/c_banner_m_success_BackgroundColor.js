"use strict";
exports.__esModule = true;
exports.c_banner_m_success_BackgroundColor = {
  "name": "--pf-c-banner--m-success--BackgroundColor",
  "value": "#3e8635",
  "var": "var(--pf-c-banner--m-success--BackgroundColor)"
};
exports["default"] = exports.c_banner_m_success_BackgroundColor;
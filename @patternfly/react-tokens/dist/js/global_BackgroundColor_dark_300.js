"use strict";
exports.__esModule = true;
exports.global_BackgroundColor_dark_300 = {
  "name": "--pf-global--BackgroundColor--dark-300",
  "value": "#212427",
  "var": "var(--pf-global--BackgroundColor--dark-300)"
};
exports["default"] = exports.global_BackgroundColor_dark_300;
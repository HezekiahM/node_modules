"use strict";
exports.__esModule = true;
exports.global_success_color_200 = {
  "name": "--pf-global--success-color--200",
  "value": "#0f280d",
  "var": "var(--pf-global--success-color--200)"
};
exports["default"] = exports.global_success_color_200;
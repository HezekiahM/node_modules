"use strict";
exports.__esModule = true;
exports.c_login__main_BackgroundColor = {
  "name": "--pf-c-login__main--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-login__main--BackgroundColor)"
};
exports["default"] = exports.c_login__main_BackgroundColor;
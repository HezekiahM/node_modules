"use strict";
exports.__esModule = true;
exports.c_select__toggle_before_BorderWidth = {
  "name": "--pf-c-select__toggle--before--BorderWidth",
  "value": "1px",
  "var": "var(--pf-c-select__toggle--before--BorderWidth)"
};
exports["default"] = exports.c_select__toggle_before_BorderWidth;
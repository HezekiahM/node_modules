"use strict";
exports.__esModule = true;
exports.c_select__toggle_arrow_MarginRight = {
  "name": "--pf-c-select__toggle-arrow--MarginRight",
  "value": "0.5rem",
  "var": "var(--pf-c-select__toggle-arrow--MarginRight)"
};
exports["default"] = exports.c_select__toggle_arrow_MarginRight;
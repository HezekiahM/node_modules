"use strict";
exports.__esModule = true;
exports.c_app_launcher__menu_search_BottomBorderWidth = {
  "name": "--pf-c-app-launcher__menu-search--BottomBorderWidth",
  "value": "1px",
  "var": "var(--pf-c-app-launcher__menu-search--BottomBorderWidth)"
};
exports["default"] = exports.c_app_launcher__menu_search_BottomBorderWidth;
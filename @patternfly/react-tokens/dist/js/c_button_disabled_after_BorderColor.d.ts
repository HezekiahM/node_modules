export const c_button_disabled_after_BorderColor: {
  "name": "--pf-c-button--disabled--after--BorderColor",
  "value": "transparent",
  "var": "var(--pf-c-button--disabled--after--BorderColor)"
};
export default c_button_disabled_after_BorderColor;
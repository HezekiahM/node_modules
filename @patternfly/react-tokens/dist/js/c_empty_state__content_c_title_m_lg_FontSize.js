"use strict";
exports.__esModule = true;
exports.c_empty_state__content_c_title_m_lg_FontSize = {
  "name": "--pf-c-empty-state__content--c-title--m-lg--FontSize",
  "value": "1.25rem",
  "var": "var(--pf-c-empty-state__content--c-title--m-lg--FontSize)"
};
exports["default"] = exports.c_empty_state__content_c_title_m_lg_FontSize;
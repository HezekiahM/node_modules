"use strict";
exports.__esModule = true;
exports.c_form__group_label_help_FontSize = {
  "name": "--pf-c-form__group-label-help--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-form__group-label-help--FontSize)"
};
exports["default"] = exports.c_form__group_label_help_FontSize;
export const c_clipboard_copy__expandable_content_BorderRightWidth: {
  "name": "--pf-c-clipboard-copy__expandable-content--BorderRightWidth",
  "value": "1px",
  "var": "var(--pf-c-clipboard-copy__expandable-content--BorderRightWidth)"
};
export default c_clipboard_copy__expandable_content_BorderRightWidth;
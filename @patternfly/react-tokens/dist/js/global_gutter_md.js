"use strict";
exports.__esModule = true;
exports.global_gutter_md = {
  "name": "--pf-global--gutter--md",
  "value": "1.5rem",
  "var": "var(--pf-global--gutter--md)"
};
exports["default"] = exports.global_gutter_md;
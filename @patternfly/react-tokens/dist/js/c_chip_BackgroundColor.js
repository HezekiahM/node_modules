"use strict";
exports.__esModule = true;
exports.c_chip_BackgroundColor = {
  "name": "--pf-c-chip--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-chip--BackgroundColor)"
};
exports["default"] = exports.c_chip_BackgroundColor;
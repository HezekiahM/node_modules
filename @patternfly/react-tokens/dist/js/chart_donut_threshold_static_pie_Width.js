"use strict";
exports.__esModule = true;
exports.chart_donut_threshold_static_pie_Width = {
  "name": "--pf-chart-donut--threshold--static--pie--Width",
  "value": 230,
  "var": "var(--pf-chart-donut--threshold--static--pie--Width)"
};
exports["default"] = exports.chart_donut_threshold_static_pie_Width;
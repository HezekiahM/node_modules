"use strict";
exports.__esModule = true;
exports.c_table_caption_PaddingTop = {
  "name": "--pf-c-table-caption--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-table-caption--PaddingTop)"
};
exports["default"] = exports.c_table_caption_PaddingTop;
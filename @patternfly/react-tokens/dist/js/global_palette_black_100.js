"use strict";
exports.__esModule = true;
exports.global_palette_black_100 = {
  "name": "--pf-global--palette--black-100",
  "value": "#fafafa",
  "var": "var(--pf-global--palette--black-100)"
};
exports["default"] = exports.global_palette_black_100;
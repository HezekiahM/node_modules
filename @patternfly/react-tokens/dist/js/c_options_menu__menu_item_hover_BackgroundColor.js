"use strict";
exports.__esModule = true;
exports.c_options_menu__menu_item_hover_BackgroundColor = {
  "name": "--pf-c-options-menu__menu-item--hover--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-options-menu__menu-item--hover--BackgroundColor)"
};
exports["default"] = exports.c_options_menu__menu_item_hover_BackgroundColor;
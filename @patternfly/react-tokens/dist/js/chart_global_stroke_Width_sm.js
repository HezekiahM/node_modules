"use strict";
exports.__esModule = true;
exports.chart_global_stroke_Width_sm = {
  "name": "--pf-chart-global--stroke--Width--sm",
  "value": 2,
  "var": "var(--pf-chart-global--stroke--Width--sm)"
};
exports["default"] = exports.chart_global_stroke_Width_sm;
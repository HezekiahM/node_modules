export const c_alert_m_inline_m_danger_BackgroundColor: {
  "name": "--pf-c-alert--m-inline--m-danger--BackgroundColor",
  "value": "#faeae8",
  "var": "var(--pf-c-alert--m-inline--m-danger--BackgroundColor)"
};
export default c_alert_m_inline_m_danger_BackgroundColor;
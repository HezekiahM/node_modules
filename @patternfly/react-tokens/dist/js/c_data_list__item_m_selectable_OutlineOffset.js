"use strict";
exports.__esModule = true;
exports.c_data_list__item_m_selectable_OutlineOffset = {
  "name": "--pf-c-data-list__item--m-selectable--OutlineOffset",
  "value": "calc(-1 * 0.25rem)",
  "var": "var(--pf-c-data-list__item--m-selectable--OutlineOffset)"
};
exports["default"] = exports.c_data_list__item_m_selectable_OutlineOffset;
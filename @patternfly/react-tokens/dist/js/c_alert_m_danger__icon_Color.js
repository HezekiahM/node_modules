"use strict";
exports.__esModule = true;
exports.c_alert_m_danger__icon_Color = {
  "name": "--pf-c-alert--m-danger__icon--Color",
  "value": "#c9190b",
  "var": "var(--pf-c-alert--m-danger__icon--Color)"
};
exports["default"] = exports.c_alert_m_danger__icon_Color;
"use strict";
exports.__esModule = true;
exports.chart_bullet_primary_measure_segmented_Width = {
  "name": "--pf-chart-bullet--primary-measure--segmented--Width",
  "value": 9,
  "var": "var(--pf-chart-bullet--primary-measure--segmented--Width)"
};
exports["default"] = exports.chart_bullet_primary_measure_segmented_Width;
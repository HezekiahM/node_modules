"use strict";
exports.__esModule = true;
exports.c_tabs__scroll_button_before_BorderRightWidth = {
  "name": "--pf-c-tabs__scroll-button--before--BorderRightWidth",
  "value": "1px",
  "var": "var(--pf-c-tabs__scroll-button--before--BorderRightWidth)"
};
exports["default"] = exports.c_tabs__scroll_button_before_BorderRightWidth;
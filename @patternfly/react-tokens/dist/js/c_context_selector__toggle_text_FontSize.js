"use strict";
exports.__esModule = true;
exports.c_context_selector__toggle_text_FontSize = {
  "name": "--pf-c-context-selector__toggle-text--FontSize",
  "value": "1rem",
  "var": "var(--pf-c-context-selector__toggle-text--FontSize)"
};
exports["default"] = exports.c_context_selector__toggle_text_FontSize;
export const global_BorderColor_100: {
  "name": "--pf-global--BorderColor--100",
  "value": "#b8bbbe",
  "var": "var(--pf-global--BorderColor--100)"
};
export default global_BorderColor_100;
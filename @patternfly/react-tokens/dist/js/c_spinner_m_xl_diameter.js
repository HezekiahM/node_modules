"use strict";
exports.__esModule = true;
exports.c_spinner_m_xl_diameter = {
  "name": "--pf-c-spinner--m-xl--diameter",
  "value": "3.375rem",
  "var": "var(--pf-c-spinner--m-xl--diameter)"
};
exports["default"] = exports.c_spinner_m_xl_diameter;
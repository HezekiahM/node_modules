"use strict";
exports.__esModule = true;
exports.c_about_modal_box_PaddingTop = {
  "name": "--pf-c-about-modal-box--PaddingTop",
  "value": "2rem",
  "var": "var(--pf-c-about-modal-box--PaddingTop)"
};
exports["default"] = exports.c_about_modal_box_PaddingTop;
"use strict";
exports.__esModule = true;
exports.c_label__c_button_MarginLeft = {
  "name": "--pf-c-label__c-button--MarginLeft",
  "value": "0.25rem",
  "var": "var(--pf-c-label__c-button--MarginLeft)"
};
exports["default"] = exports.c_label__c_button_MarginLeft;
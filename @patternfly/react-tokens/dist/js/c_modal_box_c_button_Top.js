"use strict";
exports.__esModule = true;
exports.c_modal_box_c_button_Top = {
  "name": "--pf-c-modal-box--c-button--Top",
  "value": "calc(1.5rem - 0.375rem + 0.0625rem)",
  "var": "var(--pf-c-modal-box--c-button--Top)"
};
exports["default"] = exports.c_modal_box_c_button_Top;
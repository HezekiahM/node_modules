"use strict";
exports.__esModule = true;
exports.c_table_caption_PaddingBottom = {
  "name": "--pf-c-table-caption--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-table-caption--PaddingBottom)"
};
exports["default"] = exports.c_table_caption_PaddingBottom;
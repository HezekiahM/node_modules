export const c_table_m_compact_tr_responsive_PaddingBottom: {
  "name": "--pf-c-table--m-compact-tr--responsive--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-table--m-compact-tr--responsive--PaddingBottom)"
};
export default c_table_m_compact_tr_responsive_PaddingBottom;
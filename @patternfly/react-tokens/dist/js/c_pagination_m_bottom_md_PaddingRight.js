"use strict";
exports.__esModule = true;
exports.c_pagination_m_bottom_md_PaddingRight = {
  "name": "--pf-c-pagination--m-bottom--md--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-pagination--m-bottom--md--PaddingRight)"
};
exports["default"] = exports.c_pagination_m_bottom_md_PaddingRight;
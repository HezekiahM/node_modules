"use strict";
exports.__esModule = true;
exports.c_notification_badge_after_BorderWidth = {
  "name": "--pf-c-notification-badge--after--BorderWidth",
  "value": "1px",
  "var": "var(--pf-c-notification-badge--after--BorderWidth)"
};
exports["default"] = exports.c_notification_badge_after_BorderWidth;
"use strict";
exports.__esModule = true;
exports.chart_bullet_group_title_divider_stroke_Color = {
  "name": "--pf-chart-bullet--group-title--divider--stroke--Color",
  "value": "#f0f0f0",
  "var": "var(--pf-chart-bullet--group-title--divider--stroke--Color)"
};
exports["default"] = exports.chart_bullet_group_title_divider_stroke_Color;
export const global_palette_gold_50: {
  "name": "--pf-global--palette--gold-50",
  "value": "#fdf7e7",
  "var": "var(--pf-global--palette--gold-50)"
};
export default global_palette_gold_50;
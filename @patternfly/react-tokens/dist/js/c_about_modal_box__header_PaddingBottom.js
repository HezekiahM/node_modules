"use strict";
exports.__esModule = true;
exports.c_about_modal_box__header_PaddingBottom = {
  "name": "--pf-c-about-modal-box__header--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-about-modal-box__header--PaddingBottom)"
};
exports["default"] = exports.c_about_modal_box__header_PaddingBottom;
"use strict";
exports.__esModule = true;
exports.global_default_color_100 = {
  "name": "--pf-global--default-color--100",
  "value": "#73c5c5",
  "var": "var(--pf-global--default-color--100)"
};
exports["default"] = exports.global_default_color_100;
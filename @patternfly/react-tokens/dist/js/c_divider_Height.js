"use strict";
exports.__esModule = true;
exports.c_divider_Height = {
  "name": "--pf-c-divider--Height",
  "value": "1px",
  "var": "var(--pf-c-divider--Height)"
};
exports["default"] = exports.c_divider_Height;
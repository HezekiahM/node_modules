"use strict";
exports.__esModule = true;
exports.c_nav_m_light__section_title_BorderBottomColor = {
  "name": "--pf-c-nav--m-light__section-title--BorderBottomColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-nav--m-light__section-title--BorderBottomColor)"
};
exports["default"] = exports.c_nav_m_light__section_title_BorderBottomColor;
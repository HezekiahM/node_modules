export const c_table__expandable_row_after_Bottom: {
  "name": "--pf-c-table__expandable-row--after--Bottom",
  "value": "calc(1px * -1)",
  "var": "var(--pf-c-table__expandable-row--after--Bottom)"
};
export default c_table__expandable_row_after_Bottom;
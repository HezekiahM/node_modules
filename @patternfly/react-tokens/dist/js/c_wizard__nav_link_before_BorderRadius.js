"use strict";
exports.__esModule = true;
exports.c_wizard__nav_link_before_BorderRadius = {
  "name": "--pf-c-wizard__nav-link--before--BorderRadius",
  "value": "30em",
  "var": "var(--pf-c-wizard__nav-link--before--BorderRadius)"
};
exports["default"] = exports.c_wizard__nav_link_before_BorderRadius;
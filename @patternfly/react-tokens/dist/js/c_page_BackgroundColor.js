"use strict";
exports.__esModule = true;
exports.c_page_BackgroundColor = {
  "name": "--pf-c-page--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-page--BackgroundColor)"
};
exports["default"] = exports.c_page_BackgroundColor;
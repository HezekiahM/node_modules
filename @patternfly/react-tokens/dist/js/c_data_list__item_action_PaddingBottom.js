"use strict";
exports.__esModule = true;
exports.c_data_list__item_action_PaddingBottom = {
  "name": "--pf-c-data-list__item-action--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-data-list__item-action--PaddingBottom)"
};
exports["default"] = exports.c_data_list__item_action_PaddingBottom;
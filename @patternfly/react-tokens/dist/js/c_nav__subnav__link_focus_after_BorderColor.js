"use strict";
exports.__esModule = true;
exports.c_nav__subnav__link_focus_after_BorderColor = {
  "name": "--pf-c-nav__subnav__link--focus--after--BorderColor",
  "value": "#d2d2d2",
  "var": "var(--pf-c-nav__subnav__link--focus--after--BorderColor)"
};
exports["default"] = exports.c_nav__subnav__link_focus_after_BorderColor;
"use strict";
exports.__esModule = true;
exports.c_alert_PaddingBottom = {
  "name": "--pf-c-alert--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-alert--PaddingBottom)"
};
exports["default"] = exports.c_alert_PaddingBottom;
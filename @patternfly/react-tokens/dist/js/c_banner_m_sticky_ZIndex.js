"use strict";
exports.__esModule = true;
exports.c_banner_m_sticky_ZIndex = {
  "name": "--pf-c-banner--m-sticky--ZIndex",
  "value": "300",
  "var": "var(--pf-c-banner--m-sticky--ZIndex)"
};
exports["default"] = exports.c_banner_m_sticky_ZIndex;
"use strict";
exports.__esModule = true;
exports.c_tooltip__arrow_Height = {
  "name": "--pf-c-tooltip__arrow--Height",
  "value": "0.9375rem",
  "var": "var(--pf-c-tooltip__arrow--Height)"
};
exports["default"] = exports.c_tooltip__arrow_Height;
"use strict";
exports.__esModule = true;
exports.c_empty_state_PaddingLeft = {
  "name": "--pf-c-empty-state--PaddingLeft",
  "value": "2rem",
  "var": "var(--pf-c-empty-state--PaddingLeft)"
};
exports["default"] = exports.c_empty_state_PaddingLeft;
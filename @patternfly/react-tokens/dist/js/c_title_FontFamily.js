"use strict";
exports.__esModule = true;
exports.c_title_FontFamily = {
  "name": "--pf-c-title--FontFamily",
  "value": "\"RedHatDisplay\", \"Overpass\", overpass, helvetica, arial, sans-serif",
  "var": "var(--pf-c-title--FontFamily)"
};
exports["default"] = exports.c_title_FontFamily;
"use strict";
exports.__esModule = true;
exports.c_content_blockquote_BorderLeftColor = {
  "name": "--pf-c-content--blockquote--BorderLeftColor",
  "value": "#d2d2d2",
  "var": "var(--pf-c-content--blockquote--BorderLeftColor)"
};
exports["default"] = exports.c_content_blockquote_BorderLeftColor;
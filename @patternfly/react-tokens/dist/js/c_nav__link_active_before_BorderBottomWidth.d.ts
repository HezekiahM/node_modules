export const c_nav__link_active_before_BorderBottomWidth: {
  "name": "--pf-c-nav__link--active--before--BorderBottomWidth",
  "value": "3px",
  "var": "var(--pf-c-nav__link--active--before--BorderBottomWidth)"
};
export default c_nav__link_active_before_BorderBottomWidth;
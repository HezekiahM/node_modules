"use strict";
exports.__esModule = true;
exports.c_page__main_wizard_BorderTopWidth = {
  "name": "--pf-c-page__main-wizard--BorderTopWidth",
  "value": "1px",
  "var": "var(--pf-c-page__main-wizard--BorderTopWidth)"
};
exports["default"] = exports.c_page__main_wizard_BorderTopWidth;
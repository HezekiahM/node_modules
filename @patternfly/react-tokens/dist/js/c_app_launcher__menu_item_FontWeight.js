"use strict";
exports.__esModule = true;
exports.c_app_launcher__menu_item_FontWeight = {
  "name": "--pf-c-app-launcher__menu-item--FontWeight",
  "value": "400",
  "var": "var(--pf-c-app-launcher__menu-item--FontWeight)"
};
exports["default"] = exports.c_app_launcher__menu_item_FontWeight;
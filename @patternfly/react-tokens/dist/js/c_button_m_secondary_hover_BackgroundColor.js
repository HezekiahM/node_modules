"use strict";
exports.__esModule = true;
exports.c_button_m_secondary_hover_BackgroundColor = {
  "name": "--pf-c-button--m-secondary--hover--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-button--m-secondary--hover--BackgroundColor)"
};
exports["default"] = exports.c_button_m_secondary_hover_BackgroundColor;
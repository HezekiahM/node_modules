"use strict";
exports.__esModule = true;
exports.c_pagination__nav_page_select_c_form_control_Width = {
  "name": "--pf-c-pagination__nav-page-select--c-form-control--Width",
  "value": "calc(3.5ch + (2 * 1ch))",
  "var": "var(--pf-c-pagination__nav-page-select--c-form-control--Width)"
};
exports["default"] = exports.c_pagination__nav_page_select_c_form_control_Width;
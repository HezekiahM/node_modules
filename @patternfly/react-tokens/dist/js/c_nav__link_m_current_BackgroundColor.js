"use strict";
exports.__esModule = true;
exports.c_nav__link_m_current_BackgroundColor = {
  "name": "--pf-c-nav__link--m-current--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-nav__link--m-current--BackgroundColor)"
};
exports["default"] = exports.c_nav__link_m_current_BackgroundColor;
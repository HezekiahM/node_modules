"use strict";
exports.__esModule = true;
exports.chart_global_FontFamily = {
  "name": "--pf-chart-global--FontFamily",
  "value": "RedHatText, Overpass, overpass, helvetica, arial, sans-serif",
  "var": "var(--pf-chart-global--FontFamily)"
};
exports["default"] = exports.chart_global_FontFamily;
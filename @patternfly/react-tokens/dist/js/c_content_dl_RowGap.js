"use strict";
exports.__esModule = true;
exports.c_content_dl_RowGap = {
  "name": "--pf-c-content--dl--RowGap",
  "value": "1rem",
  "var": "var(--pf-c-content--dl--RowGap)"
};
exports["default"] = exports.c_content_dl_RowGap;
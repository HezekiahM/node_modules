"use strict";
exports.__esModule = true;
exports.global_palette_blue_100 = {
  "name": "--pf-global--palette--blue-100",
  "value": "#bee1f4",
  "var": "var(--pf-global--palette--blue-100)"
};
exports["default"] = exports.global_palette_blue_100;
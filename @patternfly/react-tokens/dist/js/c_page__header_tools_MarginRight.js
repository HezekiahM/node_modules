"use strict";
exports.__esModule = true;
exports.c_page__header_tools_MarginRight = {
  "name": "--pf-c-page__header-tools--MarginRight",
  "value": "1rem",
  "var": "var(--pf-c-page__header-tools--MarginRight)"
};
exports["default"] = exports.c_page__header_tools_MarginRight;
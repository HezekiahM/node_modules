"use strict";
exports.__esModule = true;
exports.c_data_list__expandable_content_body_md_PaddingBottom = {
  "name": "--pf-c-data-list__expandable-content-body--md--PaddingBottom",
  "value": "1.5rem",
  "var": "var(--pf-c-data-list__expandable-content-body--md--PaddingBottom)"
};
exports["default"] = exports.c_data_list__expandable_content_body_md_PaddingBottom;
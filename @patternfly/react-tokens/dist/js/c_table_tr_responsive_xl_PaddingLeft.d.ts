export const c_table_tr_responsive_xl_PaddingLeft: {
  "name": "--pf-c-table-tr--responsive--xl--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-table-tr--responsive--xl--PaddingLeft)"
};
export default c_table_tr_responsive_xl_PaddingLeft;
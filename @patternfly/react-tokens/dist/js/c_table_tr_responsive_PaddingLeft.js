"use strict";
exports.__esModule = true;
exports.c_table_tr_responsive_PaddingLeft = {
  "name": "--pf-c-table-tr--responsive--PaddingLeft",
  "value": "1.5rem",
  "var": "var(--pf-c-table-tr--responsive--PaddingLeft)"
};
exports["default"] = exports.c_table_tr_responsive_PaddingLeft;
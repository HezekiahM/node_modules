"use strict";
exports.__esModule = true;
exports.c_label__c_button_PaddingLeft = {
  "name": "--pf-c-label__c-button--PaddingLeft",
  "value": "0.5rem",
  "var": "var(--pf-c-label__c-button--PaddingLeft)"
};
exports["default"] = exports.c_label__c_button_PaddingLeft;
"use strict";
exports.__esModule = true;
exports.c_expandable_section__toggle_PaddingTop = {
  "name": "--pf-c-expandable-section__toggle--PaddingTop",
  "value": "0.375rem",
  "var": "var(--pf-c-expandable-section__toggle--PaddingTop)"
};
exports["default"] = exports.c_expandable_section__toggle_PaddingTop;
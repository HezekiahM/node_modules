"use strict";
exports.__esModule = true;
exports.c_alert_group_m_toast_ZIndex = {
  "name": "--pf-c-alert-group--m-toast--ZIndex",
  "value": "600",
  "var": "var(--pf-c-alert-group--m-toast--ZIndex)"
};
exports["default"] = exports.c_alert_group_m_toast_ZIndex;
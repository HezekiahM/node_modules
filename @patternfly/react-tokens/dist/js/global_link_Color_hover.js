"use strict";
exports.__esModule = true;
exports.global_link_Color_hover = {
  "name": "--pf-global--link--Color--hover",
  "value": "#73bcf7",
  "var": "var(--pf-global--link--Color--hover)"
};
exports["default"] = exports.global_link_Color_hover;
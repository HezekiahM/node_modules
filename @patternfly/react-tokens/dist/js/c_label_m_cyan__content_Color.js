"use strict";
exports.__esModule = true;
exports.c_label_m_cyan__content_Color = {
  "name": "--pf-c-label--m-cyan__content--Color",
  "value": "#003737",
  "var": "var(--pf-c-label--m-cyan__content--Color)"
};
exports["default"] = exports.c_label_m_cyan__content_Color;
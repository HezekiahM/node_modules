"use strict";
exports.__esModule = true;
exports.c_notification_badge_after_BorderRadius = {
  "name": "--pf-c-notification-badge--after--BorderRadius",
  "value": "30em",
  "var": "var(--pf-c-notification-badge--after--BorderRadius)"
};
exports["default"] = exports.c_notification_badge_after_BorderRadius;
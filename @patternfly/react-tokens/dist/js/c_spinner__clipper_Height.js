"use strict";
exports.__esModule = true;
exports.c_spinner__clipper_Height = {
  "name": "--pf-c-spinner__clipper--Height",
  "value": "3.375rem",
  "var": "var(--pf-c-spinner__clipper--Height)"
};
exports["default"] = exports.c_spinner__clipper_Height;
"use strict";
exports.__esModule = true;
exports.global_palette_green_600 = {
  "name": "--pf-global--palette--green-600",
  "value": "#1e4f18",
  "var": "var(--pf-global--palette--green-600)"
};
exports["default"] = exports.global_palette_green_600;
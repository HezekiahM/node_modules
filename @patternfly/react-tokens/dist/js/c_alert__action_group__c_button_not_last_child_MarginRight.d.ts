export const c_alert__action_group__c_button_not_last_child_MarginRight: {
  "name": "--pf-c-alert__action-group__c-button--not-last-child--MarginRight",
  "value": "1.5rem",
  "var": "var(--pf-c-alert__action-group__c-button--not-last-child--MarginRight)"
};
export default c_alert__action_group__c_button_not_last_child_MarginRight;
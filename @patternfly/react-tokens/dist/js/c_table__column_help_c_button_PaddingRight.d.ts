export const c_table__column_help_c_button_PaddingRight: {
  "name": "--pf-c-table__column-help--c-button--PaddingRight",
  "value": "0.5rem",
  "var": "var(--pf-c-table__column-help--c-button--PaddingRight)"
};
export default c_table__column_help_c_button_PaddingRight;
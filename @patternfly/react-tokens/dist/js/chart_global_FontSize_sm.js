"use strict";
exports.__esModule = true;
exports.chart_global_FontSize_sm = {
  "name": "--pf-chart-global--FontSize--sm",
  "value": 14,
  "var": "var(--pf-chart-global--FontSize--sm)"
};
exports["default"] = exports.chart_global_FontSize_sm;
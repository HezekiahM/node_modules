"use strict";
exports.__esModule = true;
exports.c_chip_before_BorderWidth = {
  "name": "--pf-c-chip--before--BorderWidth",
  "value": "1px",
  "var": "var(--pf-c-chip--before--BorderWidth)"
};
exports["default"] = exports.c_chip_before_BorderWidth;
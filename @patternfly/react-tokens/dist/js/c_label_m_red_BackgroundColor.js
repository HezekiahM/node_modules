"use strict";
exports.__esModule = true;
exports.c_label_m_red_BackgroundColor = {
  "name": "--pf-c-label--m-red--BackgroundColor",
  "value": "#faeae8",
  "var": "var(--pf-c-label--m-red--BackgroundColor)"
};
exports["default"] = exports.c_label_m_red_BackgroundColor;
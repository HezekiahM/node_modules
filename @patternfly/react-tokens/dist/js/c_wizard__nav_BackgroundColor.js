"use strict";
exports.__esModule = true;
exports.c_wizard__nav_BackgroundColor = {
  "name": "--pf-c-wizard__nav--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-wizard__nav--BackgroundColor)"
};
exports["default"] = exports.c_wizard__nav_BackgroundColor;
"use strict";
exports.__esModule = true;
exports.c_table_tr_responsive_nested_table_PaddingBottom = {
  "name": "--pf-c-table-tr--responsive--nested-table--PaddingBottom",
  "value": "2rem",
  "var": "var(--pf-c-table-tr--responsive--nested-table--PaddingBottom)"
};
exports["default"] = exports.c_table_tr_responsive_nested_table_PaddingBottom;
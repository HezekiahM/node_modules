"use strict";
exports.__esModule = true;
exports.c_banner_FontSize = {
  "name": "--pf-c-banner--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-banner--FontSize)"
};
exports["default"] = exports.c_banner_FontSize;
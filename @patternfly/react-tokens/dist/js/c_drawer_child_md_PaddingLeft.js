"use strict";
exports.__esModule = true;
exports.c_drawer_child_md_PaddingLeft = {
  "name": "--pf-c-drawer--child--md--PaddingLeft",
  "value": "1.5rem",
  "var": "var(--pf-c-drawer--child--md--PaddingLeft)"
};
exports["default"] = exports.c_drawer_child_md_PaddingLeft;
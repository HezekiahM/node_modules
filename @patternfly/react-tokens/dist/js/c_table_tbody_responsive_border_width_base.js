"use strict";
exports.__esModule = true;
exports.c_table_tbody_responsive_border_width_base = {
  "name": "--pf-c-table--tbody--responsive--border-width--base",
  "value": "0.5rem",
  "var": "var(--pf-c-table--tbody--responsive--border-width--base)"
};
exports["default"] = exports.c_table_tbody_responsive_border_width_base;
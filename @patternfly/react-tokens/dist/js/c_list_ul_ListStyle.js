"use strict";
exports.__esModule = true;
exports.c_list_ul_ListStyle = {
  "name": "--pf-c-list--ul--ListStyle",
  "value": "disc outside",
  "var": "var(--pf-c-list--ul--ListStyle)"
};
exports["default"] = exports.c_list_ul_ListStyle;
"use strict";
exports.__esModule = true;
exports.c_breadcrumb = {
  ".pf-c-breadcrumb": {
    "c_breadcrumb__item_FontSize": {
      "name": "--pf-c-breadcrumb__item--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_breadcrumb__item_LineHeight": {
      "name": "--pf-c-breadcrumb__item--LineHeight",
      "value": "1.3",
      "values": [
        "--pf-global--LineHeight--sm",
        "$pf-global--LineHeight--sm",
        "1.3"
      ]
    },
    "c_breadcrumb__item_MarginRight": {
      "name": "--pf-c-breadcrumb__item--MarginRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_breadcrumb__item_divider_Color": {
      "name": "--pf-c-breadcrumb__item-divider--Color",
      "value": "#8a8d90",
      "values": [
        "--pf-global--BorderColor--200",
        "$pf-global--BorderColor--200",
        "$pf-color-black-500",
        "#8a8d90"
      ]
    },
    "c_breadcrumb__item_divider_MarginRight": {
      "name": "--pf-c-breadcrumb__item-divider--MarginRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_breadcrumb__item_divider_FontSize": {
      "name": "--pf-c-breadcrumb__item-divider--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_breadcrumb__link_m_current_Color": {
      "name": "--pf-c-breadcrumb__link--m-current--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_breadcrumb__heading_FontSize": {
      "name": "--pf-c-breadcrumb__heading--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    }
  }
};
exports["default"] = exports.c_breadcrumb;
export const c_nav_m_horizontal__link_BackgroundColor: {
  "name": "--pf-c-nav--m-horizontal__link--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-nav--m-horizontal__link--BackgroundColor)"
};
export default c_nav_m_horizontal__link_BackgroundColor;
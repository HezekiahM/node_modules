export const c_label_m_outline_m_green__content_link_focus_before_BorderColor: {
  "name": "--pf-c-label--m-outline--m-green__content--link--focus--before--BorderColor",
  "value": "#bde5b8",
  "var": "var(--pf-c-label--m-outline--m-green__content--link--focus--before--BorderColor)"
};
export default c_label_m_outline_m_green__content_link_focus_before_BorderColor;
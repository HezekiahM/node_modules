export const c_expandable_section__content_MarginTop: {
  "name": "--pf-c-expandable-section__content--MarginTop",
  "value": "1rem",
  "var": "var(--pf-c-expandable-section__content--MarginTop)"
};
export default c_expandable_section__content_MarginTop;
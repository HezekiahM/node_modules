"use strict";
exports.__esModule = true;
exports.c_content_Color = {
  "name": "--pf-c-content--Color",
  "value": "#151515",
  "var": "var(--pf-c-content--Color)"
};
exports["default"] = exports.c_content_Color;
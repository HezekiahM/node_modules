"use strict";
exports.__esModule = true;
exports.c_wizard__nav_link_before_FontSize = {
  "name": "--pf-c-wizard__nav-link--before--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-wizard__nav-link--before--FontSize)"
};
exports["default"] = exports.c_wizard__nav_link_before_FontSize;
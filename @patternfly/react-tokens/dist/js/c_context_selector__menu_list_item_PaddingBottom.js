"use strict";
exports.__esModule = true;
exports.c_context_selector__menu_list_item_PaddingBottom = {
  "name": "--pf-c-context-selector__menu-list-item--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-context-selector__menu-list-item--PaddingBottom)"
};
exports["default"] = exports.c_context_selector__menu_list_item_PaddingBottom;
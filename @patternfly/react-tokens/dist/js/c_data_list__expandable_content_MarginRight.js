"use strict";
exports.__esModule = true;
exports.c_data_list__expandable_content_MarginRight = {
  "name": "--pf-c-data-list__expandable-content--MarginRight",
  "value": "calc(1rem * -1)",
  "var": "var(--pf-c-data-list__expandable-content--MarginRight)"
};
exports["default"] = exports.c_data_list__expandable_content_MarginRight;
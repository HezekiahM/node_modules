export const c_button_m_control_Color: {
  "name": "--pf-c-button--m-control--Color",
  "value": "#151515",
  "var": "var(--pf-c-button--m-control--Color)"
};
export default c_button_m_control_Color;
"use strict";
exports.__esModule = true;
exports.c_card_m_compact_child_PaddingRight = {
  "name": "--pf-c-card--m-compact--child--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-card--m-compact--child--PaddingRight)"
};
exports["default"] = exports.c_card_m_compact_child_PaddingRight;
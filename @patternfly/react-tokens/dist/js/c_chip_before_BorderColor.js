"use strict";
exports.__esModule = true;
exports.c_chip_before_BorderColor = {
  "name": "--pf-c-chip--before--BorderColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-chip--before--BorderColor)"
};
exports["default"] = exports.c_chip_before_BorderColor;
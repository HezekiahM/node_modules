"use strict";
exports.__esModule = true;
exports.c_background_image_Filter = {
  "name": "--pf-c-background-image--Filter",
  "value": "url(\"#image_overlay\")",
  "var": "var(--pf-c-background-image--Filter)"
};
exports["default"] = exports.c_background_image_Filter;
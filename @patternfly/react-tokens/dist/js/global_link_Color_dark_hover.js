"use strict";
exports.__esModule = true;
exports.global_link_Color_dark_hover = {
  "name": "--pf-global--link--Color--dark--hover",
  "value": "#004080",
  "var": "var(--pf-global--link--Color--dark--hover)"
};
exports["default"] = exports.global_link_Color_dark_hover;
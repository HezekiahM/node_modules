export const c_banner_m_warning_BackgroundColor: {
  "name": "--pf-c-banner--m-warning--BackgroundColor",
  "value": "#f0ab00",
  "var": "var(--pf-c-banner--m-warning--BackgroundColor)"
};
export default c_banner_m_warning_BackgroundColor;
"use strict";
exports.__esModule = true;
exports.l_gallery_GridTemplateColumns = {
  "name": "--pf-l-gallery--GridTemplateColumns",
  "value": "repeat(auto-fill, minmax(250px, 1fr))",
  "var": "var(--pf-l-gallery--GridTemplateColumns)"
};
exports["default"] = exports.l_gallery_GridTemplateColumns;
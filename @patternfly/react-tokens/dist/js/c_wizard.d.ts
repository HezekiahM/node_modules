export const c_wizard: {
  ".pf-c-wizard__header": {
    "global_Color_100": {
      "name": "--pf-global--Color--100",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "global_Color_200": {
      "name": "--pf-global--Color--200",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--Color--light-200",
        "$pf-global--Color--light-200",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "global_BorderColor_100": {
      "name": "--pf-global--BorderColor--100",
      "value": "#b8bbbe",
      "values": [
        "--pf-global--BorderColor--light-100",
        "$pf-global--BorderColor--light-100",
        "$pf-color-black-400",
        "#b8bbbe"
      ]
    },
    "global_primary_color_100": {
      "name": "--pf-global--primary-color--100",
      "value": "#73bcf7",
      "values": [
        "--pf-global--primary-color--light-100",
        "$pf-global--primary-color--light-100",
        "$pf-color-blue-200",
        "#73bcf7"
      ]
    },
    "global_link_Color": {
      "name": "--pf-global--link--Color",
      "value": "#73bcf7",
      "values": [
        "--pf-global--link--Color--light",
        "$pf-global--link--Color--light",
        "$pf-global--active-color--300",
        "$pf-color-blue-200",
        "#73bcf7"
      ]
    },
    "global_link_Color_hover": {
      "name": "--pf-global--link--Color--hover",
      "value": "#73bcf7",
      "values": [
        "--pf-global--link--Color--light",
        "$pf-global--link--Color--light",
        "$pf-global--active-color--300",
        "$pf-color-blue-200",
        "#73bcf7"
      ]
    },
    "global_BackgroundColor_100": {
      "name": "--pf-global--BackgroundColor--100",
      "value": "#151515",
      "values": [
        "--pf-global--BackgroundColor--dark-100",
        "$pf-global--BackgroundColor--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    }
  },
  ".pf-c-wizard__header .pf-c-card": {
    "c_card_BackgroundColor": {
      "name": "--pf-c-card--BackgroundColor",
      "value": "rgba(#030303, .32)",
      "values": [
        "--pf-global--BackgroundColor--dark-transparent-200",
        "$pf-global--BackgroundColor--dark-transparent-200",
        "rgba($pf-color-black-1000, .32)",
        "rgba(#030303, .32)"
      ]
    }
  },
  ".pf-c-wizard__header .pf-c-button": {
    "c_button_m_primary_Color": {
      "name": "--pf-c-button--m-primary--Color",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--dark-100",
        "$pf-global--primary-color--dark-100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_primary_hover_Color": {
      "name": "--pf-c-button--m-primary--hover--Color",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--dark-100",
        "$pf-global--primary-color--dark-100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_primary_focus_Color": {
      "name": "--pf-c-button--m-primary--focus--Color",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--dark-100",
        "$pf-global--primary-color--dark-100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_primary_active_Color": {
      "name": "--pf-c-button--m-primary--active--Color",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--dark-100",
        "$pf-global--primary-color--dark-100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_primary_BackgroundColor": {
      "name": "--pf-c-button--m-primary--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--light-100",
        "$pf-global--BackgroundColor--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_primary_hover_BackgroundColor": {
      "name": "--pf-c-button--m-primary--hover--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_button_m_primary_focus_BackgroundColor": {
      "name": "--pf-c-button--m-primary--focus--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_button_m_primary_active_BackgroundColor": {
      "name": "--pf-c-button--m-primary--active--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_button_m_secondary_Color": {
      "name": "--pf-c-button--m-secondary--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_hover_Color": {
      "name": "--pf-c-button--m-secondary--hover--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_focus_Color": {
      "name": "--pf-c-button--m-secondary--focus--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_active_Color": {
      "name": "--pf-c-button--m-secondary--active--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_BorderColor": {
      "name": "--pf-c-button--m-secondary--BorderColor",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_hover_BorderColor": {
      "name": "--pf-c-button--m-secondary--hover--BorderColor",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_focus_BorderColor": {
      "name": "--pf-c-button--m-secondary--focus--BorderColor",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_active_BorderColor": {
      "name": "--pf-c-button--m-secondary--active--BorderColor",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    }
  },
  ".pf-c-wizard": {
    "c_wizard_Height": {
      "name": "--pf-c-wizard--Height",
      "value": "100%"
    },
    "c_modal_box_c_wizard_FlexBasis": {
      "name": "--pf-c-modal-box--c-wizard--FlexBasis",
      "value": "47.625rem"
    },
    "c_wizard__header_BackgroundColor": {
      "name": "--pf-c-wizard__header--BackgroundColor",
      "value": "#151515",
      "values": [
        "--pf-global--BackgroundColor--dark-100",
        "$pf-global--BackgroundColor--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_wizard__header_ZIndex": {
      "name": "--pf-c-wizard__header--ZIndex",
      "value": "300",
      "values": [
        "--pf-global--ZIndex--md",
        "$pf-global--ZIndex--md",
        "300"
      ]
    },
    "c_wizard__header_PaddingTop": {
      "name": "--pf-c-wizard__header--PaddingTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_wizard__header_PaddingRight": {
      "name": "--pf-c-wizard__header--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_wizard__header_PaddingBottom": {
      "name": "--pf-c-wizard__header--PaddingBottom",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_wizard__header_PaddingLeft": {
      "name": "--pf-c-wizard__header--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_wizard__header_lg_PaddingRight": {
      "name": "--pf-c-wizard__header--lg--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_wizard__header_lg_PaddingLeft": {
      "name": "--pf-c-wizard__header--lg--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_wizard__header_xl_PaddingRight": {
      "name": "--pf-c-wizard__header--xl--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_wizard__header_xl_PaddingLeft": {
      "name": "--pf-c-wizard__header--xl--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_wizard__close_Top": {
      "name": "--pf-c-wizard__close--Top",
      "value": "calc(1.5rem - 0.375rem)",
      "values": [
        "calc(--pf-global--spacer--lg - --pf-global--spacer--form-element)",
        "calc($pf-global--spacer--lg - $pf-global--spacer--form-element)",
        "calc(pf-size-prem(24px) - pf-size-prem(6px))",
        "calc(1.5rem - 0.375rem)"
      ]
    },
    "c_wizard__close_Right": {
      "name": "--pf-c-wizard__close--Right",
      "value": "0"
    },
    "c_wizard__close_xl_Right": {
      "name": "--pf-c-wizard__close--xl--Right",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_wizard__close_FontSize": {
      "name": "--pf-c-wizard__close--FontSize",
      "value": "1.25rem",
      "values": [
        "--pf-global--FontSize--xl",
        "$pf-global--FontSize--xl",
        "pf-font-prem(20px)",
        "1.25rem"
      ]
    },
    "c_wizard__title_PaddingRight": {
      "name": "--pf-c-wizard__title--PaddingRight",
      "value": "3rem",
      "values": [
        "--pf-global--spacer--2xl",
        "$pf-global--spacer--2xl",
        "pf-size-prem(48px)",
        "3rem"
      ]
    },
    "c_wizard__description_PaddingTop": {
      "name": "--pf-c-wizard__description--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_wizard__description_Color": {
      "name": "--pf-c-wizard__description--Color",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--Color--light-200",
        "$pf-global--Color--light-200",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_wizard__nav_link_Color": {
      "name": "--pf-c-wizard__nav-link--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_wizard__nav_link_TextDecoration": {
      "name": "--pf-c-wizard__nav-link--TextDecoration",
      "value": "none",
      "values": [
        "--pf-global--link--TextDecoration",
        "$pf-global--link--TextDecoration",
        "none"
      ]
    },
    "c_wizard__nav_link_hover_Color": {
      "name": "--pf-c-wizard__nav-link--hover--Color",
      "value": "#06c",
      "values": [
        "--pf-global--link--Color",
        "$pf-global--link--Color",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_wizard__nav_link_focus_Color": {
      "name": "--pf-c-wizard__nav-link--focus--Color",
      "value": "#06c",
      "values": [
        "--pf-global--link--Color",
        "$pf-global--link--Color",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_wizard__nav_link_m_current_Color": {
      "name": "--pf-c-wizard__nav-link--m-current--Color",
      "value": "#06c",
      "values": [
        "--pf-global--link--Color",
        "$pf-global--link--Color",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_wizard__nav_link_m_current_FontWeight": {
      "name": "--pf-c-wizard__nav-link--m-current--FontWeight",
      "value": "700",
      "values": [
        "--pf-global--FontWeight--bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    },
    "c_wizard__nav_link_m_disabled_Color": {
      "name": "--pf-c-wizard__nav-link--m-disabled--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--dark-200",
        "$pf-global--Color--dark-200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_wizard__nav_list__nav_list__nav_link_m_current_FontWeight": {
      "name": "--pf-c-wizard__nav-list__nav-list__nav-link--m-current--FontWeight",
      "value": "700",
      "values": [
        "--pf-global--FontWeight--bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    },
    "c_wizard__nav_link_before_Width": {
      "name": "--pf-c-wizard__nav-link--before--Width",
      "value": "1.5rem"
    },
    "c_wizard__nav_link_before_Height": {
      "name": "--pf-c-wizard__nav-link--before--Height",
      "value": "1.5rem"
    },
    "c_wizard__nav_link_before_Top": {
      "name": "--pf-c-wizard__nav-link--before--Top",
      "value": "0"
    },
    "c_wizard__nav_link_before_BackgroundColor": {
      "name": "--pf-c-wizard__nav-link--before--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--200",
        "$pf-global--BackgroundColor--200",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_wizard__nav_link_before_BorderRadius": {
      "name": "--pf-c-wizard__nav-link--before--BorderRadius",
      "value": "30em",
      "values": [
        "--pf-global--BorderRadius--lg",
        "$pf-global--BorderRadius--lg",
        "30em"
      ]
    },
    "c_wizard__nav_link_before_Color": {
      "name": "--pf-c-wizard__nav-link--before--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_wizard__nav_link_before_FontSize": {
      "name": "--pf-c-wizard__nav-link--before--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_wizard__nav_link_before_TranslateX": {
      "name": "--pf-c-wizard__nav-link--before--TranslateX",
      "value": "calc(-100% - 0.5rem)",
      "values": [
        "calc(-100% - --pf-global--spacer--sm)",
        "calc(-100% - $pf-global--spacer--sm)",
        "calc(-100% - pf-size-prem(8px))",
        "calc(-100% - 0.5rem)"
      ]
    },
    "c_wizard__nav_link_m_current_before_BackgroundColor": {
      "name": "--pf-c-wizard__nav-link--m-current--before--BackgroundColor",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_wizard__nav_link_m_current_before_Color": {
      "name": "--pf-c-wizard__nav-link--m-current--before--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_wizard__nav_link_m_disabled_before_BackgroundColor": {
      "name": "--pf-c-wizard__nav-link--m-disabled--before--BackgroundColor",
      "value": "transparent"
    },
    "c_wizard__nav_link_m_disabled_before_Color": {
      "name": "--pf-c-wizard__nav-link--m-disabled--before--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--dark-200",
        "$pf-global--Color--dark-200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_wizard__toggle_BackgroundColor": {
      "name": "--pf-c-wizard__toggle--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_wizard__toggle_ZIndex": {
      "name": "--pf-c-wizard__toggle--ZIndex",
      "value": "300",
      "values": [
        "--pf-global--ZIndex--md",
        "$pf-global--ZIndex--md",
        "300"
      ]
    },
    "c_wizard__toggle_BoxShadow": {
      "name": "--pf-c-wizard__toggle--BoxShadow",
      "value": "0 0.5rem 0.5rem -0.375rem rgba(3, 3, 3, 0.18)",
      "values": [
        "--pf-global--BoxShadow--md-bottom",
        "$pf-global--BoxShadow--md-bottom",
        "0 pf-size-prem(8px) pf-size-prem(8px) pf-size-prem(-6px) rgba($pf-color-black-1000, .18)",
        "0 pf-size-prem(8px) pf-size-prem(8px) pf-size-prem(-6px) rgba(#030303, .18)",
        "0 0.5rem 0.5rem -0.375rem rgba(3, 3, 3, 0.18)"
      ]
    },
    "c_wizard__toggle_PaddingTop": {
      "name": "--pf-c-wizard__toggle--PaddingTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_wizard__toggle_PaddingRight": {
      "name": "--pf-c-wizard__toggle--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_wizard__toggle_PaddingBottom": {
      "name": "--pf-c-wizard__toggle--PaddingBottom",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_wizard__toggle_PaddingLeft": {
      "name": "--pf-c-wizard__toggle--PaddingLeft",
      "value": "calc(1rem + 1.5rem + 0.5rem)",
      "values": [
        "calc(--pf-global--spacer--md + --pf-c-wizard__nav-link--before--Width + --pf-global--spacer--sm)",
        "calc($pf-global--spacer--md + 1.5rem + $pf-global--spacer--sm)",
        "calc(pf-size-prem(16px) + 1.5rem + pf-size-prem(8px))",
        "calc(1rem + 1.5rem + 0.5rem)"
      ]
    },
    "c_wizard__toggle_m_expanded_BorderBottomWidth": {
      "name": "--pf-c-wizard__toggle--m-expanded--BorderBottomWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_wizard__toggle_m_expanded_BorderBottomColor": {
      "name": "--pf-c-wizard__toggle--m-expanded--BorderBottomColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_wizard_m_in_page__toggle_xl_PaddingLeft": {
      "name": "--pf-c-wizard--m-in-page__toggle--xl--PaddingLeft",
      "value": "calc(2rem + 1.5rem + 0.5rem)",
      "values": [
        "calc(--pf-global--spacer--xl + --pf-c-wizard__nav-link--before--Width + --pf-global--spacer--sm)",
        "calc($pf-global--spacer--xl + 1.5rem + $pf-global--spacer--sm)",
        "calc(pf-size-prem(32px) + 1.5rem + pf-size-prem(8px))",
        "calc(2rem + 1.5rem + 0.5rem)"
      ]
    },
    "c_wizard__toggle_num_before_Top": {
      "name": "--pf-c-wizard__toggle-num--before--Top",
      "value": "0"
    },
    "c_wizard__toggle_list_item_not_last_child_MarginRight": {
      "name": "--pf-c-wizard__toggle-list-item--not-last-child--MarginRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_wizard__toggle_list_item_MarginBottom": {
      "name": "--pf-c-wizard__toggle-list-item--MarginBottom",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_wizard__toggle_list_MarginRight": {
      "name": "--pf-c-wizard__toggle-list--MarginRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_wizard__toggle_list_MarginBottom": {
      "name": "--pf-c-wizard__toggle-list--MarginBottom",
      "value": "calc(0.25rem * -1)",
      "values": [
        "calc(--pf-c-wizard__toggle-list-item--MarginBottom * -1)",
        "calc(--pf-global--spacer--xs * -1)",
        "calc($pf-global--spacer--xs * -1)",
        "calc(pf-size-prem(4px) * -1)",
        "calc(0.25rem * -1)"
      ]
    },
    "c_wizard__toggle_separator_MarginLeft": {
      "name": "--pf-c-wizard__toggle-separator--MarginLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_wizard__toggle_separator_Color": {
      "name": "--pf-c-wizard__toggle-separator--Color",
      "value": "#8a8d90",
      "values": [
        "--pf-global--BorderColor--200",
        "$pf-global--BorderColor--200",
        "$pf-color-black-500",
        "#8a8d90"
      ]
    },
    "c_wizard__toggle_icon_LineHeight": {
      "name": "--pf-c-wizard__toggle-icon--LineHeight",
      "value": "1.5",
      "values": [
        "--pf-global--LineHeight--md",
        "$pf-global--LineHeight--md",
        "1.5"
      ]
    },
    "c_wizard__toggle_m_expanded__toggle_icon_Rotate": {
      "name": "--pf-c-wizard__toggle--m-expanded__toggle-icon--Rotate",
      "value": "180deg"
    },
    "c_wizard__nav_ZIndex": {
      "name": "--pf-c-wizard__nav--ZIndex",
      "value": "200",
      "values": [
        "--pf-global--ZIndex--sm",
        "$pf-global--ZIndex--sm",
        "200"
      ]
    },
    "c_wizard__nav_BackgroundColor": {
      "name": "--pf-c-wizard__nav--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_wizard__nav_BoxShadow": {
      "name": "--pf-c-wizard__nav--BoxShadow",
      "value": "0 0.5rem 0.5rem -0.375rem rgba(3, 3, 3, 0.18)",
      "values": [
        "--pf-global--BoxShadow--md-bottom",
        "$pf-global--BoxShadow--md-bottom",
        "0 pf-size-prem(8px) pf-size-prem(8px) pf-size-prem(-6px) rgba($pf-color-black-1000, .18)",
        "0 pf-size-prem(8px) pf-size-prem(8px) pf-size-prem(-6px) rgba(#030303, .18)",
        "0 0.5rem 0.5rem -0.375rem rgba(3, 3, 3, 0.18)"
      ]
    },
    "c_wizard__nav_Width": {
      "name": "--pf-c-wizard__nav--Width",
      "value": "100%"
    },
    "c_wizard__nav_lg_Width": {
      "name": "--pf-c-wizard__nav--lg--Width",
      "value": "15.625rem"
    },
    "c_wizard__nav_lg_BorderRightWidth": {
      "name": "--pf-c-wizard__nav--lg--BorderRightWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_wizard__nav_lg_BorderRightColor": {
      "name": "--pf-c-wizard__nav--lg--BorderRightColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_wizard__nav_list_PaddingTop": {
      "name": "--pf-c-wizard__nav-list--PaddingTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_wizard__nav_list_PaddingRight": {
      "name": "--pf-c-wizard__nav-list--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_wizard__nav_list_PaddingBottom": {
      "name": "--pf-c-wizard__nav-list--PaddingBottom",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_wizard__nav_list_PaddingLeft": {
      "name": "--pf-c-wizard__nav-list--PaddingLeft",
      "value": "calc(1rem + 1.5rem + 0.5rem)",
      "values": [
        "calc(--pf-global--spacer--md + --pf-c-wizard__nav-link--before--Width + --pf-global--spacer--sm)",
        "calc($pf-global--spacer--md + 1.5rem + $pf-global--spacer--sm)",
        "calc(pf-size-prem(16px) + 1.5rem + pf-size-prem(8px))",
        "calc(1rem + 1.5rem + 0.5rem)"
      ]
    },
    "c_wizard__nav_list_lg_PaddingTop": {
      "name": "--pf-c-wizard__nav-list--lg--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_wizard__nav_list_lg_PaddingRight": {
      "name": "--pf-c-wizard__nav-list--lg--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_wizard__nav_list_lg_PaddingBottom": {
      "name": "--pf-c-wizard__nav-list--lg--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_wizard__nav_list_xl_PaddingTop": {
      "name": "--pf-c-wizard__nav-list--xl--PaddingTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_wizard__nav_list_xl_PaddingRight": {
      "name": "--pf-c-wizard__nav-list--xl--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_wizard__nav_list_xl_PaddingBottom": {
      "name": "--pf-c-wizard__nav-list--xl--PaddingBottom",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_wizard__nav_list_xl_PaddingLeft": {
      "name": "--pf-c-wizard__nav-list--xl--PaddingLeft",
      "value": "calc(1.5rem + 1.5rem + 0.5rem)",
      "values": [
        "calc(--pf-global--spacer--lg + --pf-c-wizard__nav-link--before--Width + --pf-global--spacer--sm)",
        "calc($pf-global--spacer--lg + 1.5rem + $pf-global--spacer--sm)",
        "calc(pf-size-prem(24px) + 1.5rem + pf-size-prem(8px))",
        "calc(1.5rem + 1.5rem + 0.5rem)"
      ]
    },
    "c_wizard__nav_list_nested_MarginLeft": {
      "name": "--pf-c-wizard__nav-list--nested--MarginLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_wizard__nav_list_nested_MarginTop": {
      "name": "--pf-c-wizard__nav-list--nested--MarginTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_wizard__nav_item_MarginTop": {
      "name": "--pf-c-wizard__nav-item--MarginTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_wizard__outer_wrap_BackgroundColor": {
      "name": "--pf-c-wizard__outer-wrap--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_wizard__outer_wrap_lg_PaddingLeft": {
      "name": "--pf-c-wizard__outer-wrap--lg--PaddingLeft",
      "value": "100%",
      "values": [
        "--pf-c-wizard__nav--Width",
        "100%"
      ]
    },
    "c_wizard__main_ZIndex": {
      "name": "--pf-c-wizard__main--ZIndex",
      "value": "100",
      "values": [
        "--pf-global--ZIndex--xs",
        "$pf-global--ZIndex--xs",
        "100"
      ]
    },
    "c_wizard__main_body_PaddingTop": {
      "name": "--pf-c-wizard__main-body--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_wizard__main_body_PaddingRight": {
      "name": "--pf-c-wizard__main-body--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_wizard__main_body_PaddingBottom": {
      "name": "--pf-c-wizard__main-body--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_wizard__main_body_PaddingLeft": {
      "name": "--pf-c-wizard__main-body--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_wizard__main_body_xl_PaddingTop": {
      "name": "--pf-c-wizard__main-body--xl--PaddingTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_wizard__main_body_xl_PaddingRight": {
      "name": "--pf-c-wizard__main-body--xl--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_wizard__main_body_xl_PaddingBottom": {
      "name": "--pf-c-wizard__main-body--xl--PaddingBottom",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_wizard__main_body_xl_PaddingLeft": {
      "name": "--pf-c-wizard__main-body--xl--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_wizard__footer_PaddingTop": {
      "name": "--pf-c-wizard__footer--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_wizard__footer_PaddingRight": {
      "name": "--pf-c-wizard__footer--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_wizard__footer_PaddingBottom": {
      "name": "--pf-c-wizard__footer--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_wizard__footer_PaddingLeft": {
      "name": "--pf-c-wizard__footer--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_wizard__footer_xl_PaddingTop": {
      "name": "--pf-c-wizard__footer--xl--PaddingTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_wizard__footer_xl_PaddingRight": {
      "name": "--pf-c-wizard__footer--xl--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_wizard__footer_xl_PaddingBottom": {
      "name": "--pf-c-wizard__footer--xl--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_wizard__footer_xl_PaddingLeft": {
      "name": "--pf-c-wizard__footer--xl--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_wizard__footer_child_MarginRight": {
      "name": "--pf-c-wizard__footer--child--MarginRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_wizard__footer_child_MarginBottom": {
      "name": "--pf-c-wizard__footer--child--MarginBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-c-wizard.pf-m-finished": {
    "c_wizard__outer_wrap_lg_PaddingLeft": {
      "name": "--pf-c-wizard__outer-wrap--lg--PaddingLeft",
      "value": "0"
    }
  },
  ".pf-c-wizard__toggle.pf-m-expanded": {
    "c_wizard__toggle_BoxShadow": {
      "name": "--pf-c-wizard__toggle--BoxShadow",
      "value": "none"
    }
  },
  ".pf-c-wizard__toggle-num": {
    "c_wizard__nav_link_before_Top": {
      "name": "--pf-c-wizard__nav-link--before--Top",
      "value": "0",
      "values": [
        "--pf-c-wizard__toggle-num--before--Top",
        "0"
      ]
    },
    "c_wizard__nav_link_before_BackgroundColor": {
      "name": "--pf-c-wizard__nav-link--before--BackgroundColor",
      "value": "#06c",
      "values": [
        "--pf-c-wizard__nav-link--m-current--before--BackgroundColor",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_wizard__nav_link_before_Color": {
      "name": "--pf-c-wizard__nav-link--before--Color",
      "value": "#fff",
      "values": [
        "--pf-c-wizard__nav-link--m-current--before--Color",
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    }
  },
  ".pf-c-wizard__nav-link:hover": {
    "c_wizard__nav_link_Color": {
      "name": "--pf-c-wizard__nav-link--Color",
      "value": "#06c",
      "values": [
        "--pf-c-wizard__nav-link--hover--Color",
        "--pf-global--link--Color",
        "$pf-global--link--Color",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-wizard__nav-link:focus": {
    "c_wizard__nav_link_Color": {
      "name": "--pf-c-wizard__nav-link--Color",
      "value": "#06c",
      "values": [
        "--pf-c-wizard__nav-link--focus--Color",
        "--pf-global--link--Color",
        "$pf-global--link--Color",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-wizard__nav-link.pf-m-current": {
    "c_wizard__nav_link_Color": {
      "name": "--pf-c-wizard__nav-link--Color",
      "value": "#06c",
      "values": [
        "--pf-c-wizard__nav-link--m-current--Color",
        "--pf-global--link--Color",
        "$pf-global--link--Color",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-wizard__nav-link:disabled": {
    "c_wizard__nav_link_Color": {
      "name": "--pf-c-wizard__nav-link--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-c-wizard__nav-link--m-disabled--Color",
        "--pf-global--Color--dark-200",
        "$pf-global--Color--dark-200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    }
  },
  ".pf-c-wizard__nav-link:disabled::before": {
    "c_wizard__nav_link_before_BackgroundColor": {
      "name": "--pf-c-wizard__nav-link--before--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-wizard__nav-link--m-disabled--before--BackgroundColor",
        "transparent"
      ]
    },
    "c_wizard__nav_link_before_Color": {
      "name": "--pf-c-wizard__nav-link--before--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-c-wizard__nav-link--m-disabled--before--Color",
        "--pf-global--Color--dark-200",
        "$pf-global--Color--dark-200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    }
  }
};
export default c_wizard;
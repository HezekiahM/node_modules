"use strict";
exports.__esModule = true;
exports.c_table__sort__button_PaddingBottom = {
  "name": "--pf-c-table__sort__button--PaddingBottom",
  "value": "0.375rem",
  "var": "var(--pf-c-table__sort__button--PaddingBottom)"
};
exports["default"] = exports.c_table__sort__button_PaddingBottom;
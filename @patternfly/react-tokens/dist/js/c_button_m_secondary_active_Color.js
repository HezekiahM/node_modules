"use strict";
exports.__esModule = true;
exports.c_button_m_secondary_active_Color = {
  "name": "--pf-c-button--m-secondary--active--Color",
  "value": "#fff",
  "var": "var(--pf-c-button--m-secondary--active--Color)"
};
exports["default"] = exports.c_button_m_secondary_active_Color;
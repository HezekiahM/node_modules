export const c_chip__c_button_MarginRight: {
  "name": "--pf-c-chip__c-button--MarginRight",
  "value": "calc(0.5rem / 2 * -1)",
  "var": "var(--pf-c-chip__c-button--MarginRight)"
};
export default c_chip__c_button_MarginRight;
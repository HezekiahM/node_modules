export const c_popover__arrow_m_top_TranslateY: {
  "name": "--pf-c-popover__arrow--m-top--TranslateY",
  "value": "50%",
  "var": "var(--pf-c-popover__arrow--m-top--TranslateY)"
};
export default c_popover__arrow_m_top_TranslateY;
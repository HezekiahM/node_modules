"use strict";
exports.__esModule = true;
exports.c_context_selector__menu_Top = {
  "name": "--pf-c-context-selector__menu--Top",
  "value": "calc(100% + 0.25rem)",
  "var": "var(--pf-c-context-selector__menu--Top)"
};
exports["default"] = exports.c_context_selector__menu_Top;
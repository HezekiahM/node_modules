export const c_search_input__utilities_MarginRight: {
  "name": "--pf-c-search-input__utilities--MarginRight",
  "value": "0.5rem",
  "var": "var(--pf-c-search-input__utilities--MarginRight)"
};
export default c_search_input__utilities_MarginRight;
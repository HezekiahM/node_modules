"use strict";
exports.__esModule = true;
exports.c_content_ul_ListStyle = {
  "name": "--pf-c-content--ul--ListStyle",
  "value": "disc outside",
  "var": "var(--pf-c-content--ul--ListStyle)"
};
exports["default"] = exports.c_content_ul_ListStyle;
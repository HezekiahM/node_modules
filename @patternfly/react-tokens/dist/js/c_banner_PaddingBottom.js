"use strict";
exports.__esModule = true;
exports.c_banner_PaddingBottom = {
  "name": "--pf-c-banner--PaddingBottom",
  "value": "0.25rem",
  "var": "var(--pf-c-banner--PaddingBottom)"
};
exports["default"] = exports.c_banner_PaddingBottom;
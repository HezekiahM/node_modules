"use strict";
exports.__esModule = true;
exports.c_dropdown__group_title_PaddingTop = {
  "name": "--pf-c-dropdown__group-title--PaddingTop",
  "value": "0.5rem",
  "var": "var(--pf-c-dropdown__group-title--PaddingTop)"
};
exports["default"] = exports.c_dropdown__group_title_PaddingTop;
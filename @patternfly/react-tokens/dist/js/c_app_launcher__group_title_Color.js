"use strict";
exports.__esModule = true;
exports.c_app_launcher__group_title_Color = {
  "name": "--pf-c-app-launcher__group-title--Color",
  "value": "#6a6e73",
  "var": "var(--pf-c-app-launcher__group-title--Color)"
};
exports["default"] = exports.c_app_launcher__group_title_Color;
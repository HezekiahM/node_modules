"use strict";
exports.__esModule = true;
exports.global_active_color_300 = {
  "name": "--pf-global--active-color--300",
  "value": "#73bcf7",
  "var": "var(--pf-global--active-color--300)"
};
exports["default"] = exports.global_active_color_300;
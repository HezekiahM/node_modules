export const c_empty_state_PaddingTop = {
  "name": "--pf-c-empty-state--PaddingTop",
  "value": "2rem",
  "var": "var(--pf-c-empty-state--PaddingTop)"
};
export default c_empty_state_PaddingTop;
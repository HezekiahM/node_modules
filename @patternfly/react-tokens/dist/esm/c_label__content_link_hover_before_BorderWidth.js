export const c_label__content_link_hover_before_BorderWidth = {
  "name": "--pf-c-label__content--link--hover--before--BorderWidth",
  "value": "1px",
  "var": "var(--pf-c-label__content--link--hover--before--BorderWidth)"
};
export default c_label__content_link_hover_before_BorderWidth;
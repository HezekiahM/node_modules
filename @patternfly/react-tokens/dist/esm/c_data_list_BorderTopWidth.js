export const c_data_list_BorderTopWidth = {
  "name": "--pf-c-data-list--BorderTopWidth",
  "value": "0.5rem",
  "var": "var(--pf-c-data-list--BorderTopWidth)"
};
export default c_data_list_BorderTopWidth;
export const c_nav_m_horizontal__link_focus_Color = {
  "name": "--pf-c-nav--m-horizontal__link--focus--Color",
  "value": "#2b9af3",
  "var": "var(--pf-c-nav--m-horizontal__link--focus--Color)"
};
export default c_nav_m_horizontal__link_focus_Color;
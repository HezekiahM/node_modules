export const c_tabs__scroll_button_TransitionDuration_margin = {
  "name": "--pf-c-tabs__scroll-button--TransitionDuration--margin",
  "value": ".125s",
  "var": "var(--pf-c-tabs__scroll-button--TransitionDuration--margin)"
};
export default c_tabs__scroll_button_TransitionDuration_margin;
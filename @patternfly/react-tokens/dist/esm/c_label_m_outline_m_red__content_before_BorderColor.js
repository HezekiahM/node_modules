export const c_label_m_outline_m_red__content_before_BorderColor = {
  "name": "--pf-c-label--m-outline--m-red__content--before--BorderColor",
  "value": "#c9190b",
  "var": "var(--pf-c-label--m-outline--m-red__content--before--BorderColor)"
};
export default c_label_m_outline_m_red__content_before_BorderColor;
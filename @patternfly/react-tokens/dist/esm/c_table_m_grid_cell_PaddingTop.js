export const c_table_m_grid_cell_PaddingTop = {
  "name": "--pf-c-table--m-grid--cell--PaddingTop",
  "value": "0",
  "var": "var(--pf-c-table--m-grid--cell--PaddingTop)"
};
export default c_table_m_grid_cell_PaddingTop;
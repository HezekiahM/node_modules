export const global_FontFamily_sans_serif = {
  "name": "--pf-global--FontFamily--sans-serif",
  "value": "\"overpass\", overpass, \"open sans\", -apple-system, blinkmacsystemfont, \"Segoe UI\", roboto, \"Helvetica Neue\", arial, sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\"",
  "var": "var(--pf-global--FontFamily--sans-serif)"
};
export default global_FontFamily_sans_serif;
export const c_nav_m_light__section_title_Color = {
  "name": "--pf-c-nav--m-light__section-title--Color",
  "value": "#6a6e73",
  "var": "var(--pf-c-nav--m-light__section-title--Color)"
};
export default c_nav_m_light__section_title_Color;
export const global_success_color_200 = {
  "name": "--pf-global--success-color--200",
  "value": "#0f280d",
  "var": "var(--pf-global--success-color--200)"
};
export default global_success_color_200;
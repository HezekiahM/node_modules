export const c_label_m_orange__content_link_hover_before_BorderColor = {
  "name": "--pf-c-label--m-orange__content--link--hover--before--BorderColor",
  "value": "#ec7a08",
  "var": "var(--pf-c-label--m-orange__content--link--hover--before--BorderColor)"
};
export default c_label_m_orange__content_link_hover_before_BorderColor;
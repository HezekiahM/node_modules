export const c_data_list__item_sm_BorderBottomWidth = {
  "name": "--pf-c-data-list__item--sm--BorderBottomWidth",
  "value": "1px",
  "var": "var(--pf-c-data-list__item--sm--BorderBottomWidth)"
};
export default c_data_list__item_sm_BorderBottomWidth;
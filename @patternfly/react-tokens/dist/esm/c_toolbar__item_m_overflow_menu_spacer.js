export const c_toolbar__item_m_overflow_menu_spacer = {
  "name": "--pf-c-toolbar__item--m-overflow-menu--spacer",
  "value": "1rem",
  "var": "var(--pf-c-toolbar__item--m-overflow-menu--spacer)"
};
export default c_toolbar__item_m_overflow_menu_spacer;
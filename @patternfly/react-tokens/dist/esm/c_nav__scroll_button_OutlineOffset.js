export const c_nav__scroll_button_OutlineOffset = {
  "name": "--pf-c-nav__scroll-button--OutlineOffset",
  "value": "calc(-1 * 0.25rem)",
  "var": "var(--pf-c-nav__scroll-button--OutlineOffset)"
};
export default c_nav__scroll_button_OutlineOffset;
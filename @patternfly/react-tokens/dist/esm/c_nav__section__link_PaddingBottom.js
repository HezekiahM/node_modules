export const c_nav__section__link_PaddingBottom = {
  "name": "--pf-c-nav__section__link--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-nav__section__link--PaddingBottom)"
};
export default c_nav__section__link_PaddingBottom;
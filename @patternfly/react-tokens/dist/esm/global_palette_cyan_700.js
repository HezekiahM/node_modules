export const global_palette_cyan_700 = {
  "name": "--pf-global--palette--cyan-700",
  "value": "#000f0f",
  "var": "var(--pf-global--palette--cyan-700)"
};
export default global_palette_cyan_700;
export const c_data_list__expandable_content_body_xl_PaddingLeft = {
  "name": "--pf-c-data-list__expandable-content-body--xl--PaddingLeft",
  "value": "1.5rem",
  "var": "var(--pf-c-data-list__expandable-content-body--xl--PaddingLeft)"
};
export default c_data_list__expandable_content_body_xl_PaddingLeft;
export const global_Color_dark_200 = {
  "name": "--pf-global--Color--dark-200",
  "value": "#6a6e73",
  "var": "var(--pf-global--Color--dark-200)"
};
export default global_Color_dark_200;
export const c_button_m_link_hover_BackgroundColor = {
  "name": "--pf-c-button--m-link--hover--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-button--m-link--hover--BackgroundColor)"
};
export default c_button_m_link_hover_BackgroundColor;
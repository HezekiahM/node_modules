export const c_button_m_link_m_inline_FontSize = {
  "name": "--pf-c-button--m-link--m-inline--FontSize",
  "value": "inherit",
  "var": "var(--pf-c-button--m-link--m-inline--FontSize)"
};
export default c_button_m_link_m_inline_FontSize;
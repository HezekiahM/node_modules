export const c_page__header_nav_xl_PaddingLeft = {
  "name": "--pf-c-page__header-nav--xl--PaddingLeft",
  "value": "2rem",
  "var": "var(--pf-c-page__header-nav--xl--PaddingLeft)"
};
export default c_page__header_nav_xl_PaddingLeft;
export const c_pagination_m_bottom_xl_PaddingLeft = {
  "name": "--pf-c-pagination--m-bottom--xl--PaddingLeft",
  "value": "1.5rem",
  "var": "var(--pf-c-pagination--m-bottom--xl--PaddingLeft)"
};
export default c_pagination_m_bottom_xl_PaddingLeft;
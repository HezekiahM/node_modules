export const c_banner_BackgroundColor = {
  "name": "--pf-c-banner--BackgroundColor",
  "value": "#f0ab00",
  "var": "var(--pf-c-banner--BackgroundColor)"
};
export default c_banner_BackgroundColor;
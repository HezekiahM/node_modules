export const c_modal_box__header_PaddingRight = {
  "name": "--pf-c-modal-box__header--PaddingRight",
  "value": "1.5rem",
  "var": "var(--pf-c-modal-box__header--PaddingRight)"
};
export default c_modal_box__header_PaddingRight;
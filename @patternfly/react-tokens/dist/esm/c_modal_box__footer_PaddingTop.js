export const c_modal_box__footer_PaddingTop = {
  "name": "--pf-c-modal-box__footer--PaddingTop",
  "value": "1.5rem",
  "var": "var(--pf-c-modal-box__footer--PaddingTop)"
};
export default c_modal_box__footer_PaddingTop;
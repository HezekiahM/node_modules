export const c_chip__c_button_MarginTop = {
  "name": "--pf-c-chip__c-button--MarginTop",
  "value": "calc(0.25rem * -1)",
  "var": "var(--pf-c-chip__c-button--MarginTop)"
};
export default c_chip__c_button_MarginTop;
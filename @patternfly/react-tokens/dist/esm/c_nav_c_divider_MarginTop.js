export const c_nav_c_divider_MarginTop = {
  "name": "--pf-c-nav--c-divider--MarginTop",
  "value": "0.5rem",
  "var": "var(--pf-c-nav--c-divider--MarginTop)"
};
export default c_nav_c_divider_MarginTop;
export const c_chip__c_button_FontSize = {
  "name": "--pf-c-chip__c-button--FontSize",
  "value": "0.75rem",
  "var": "var(--pf-c-chip__c-button--FontSize)"
};
export default c_chip__c_button_FontSize;
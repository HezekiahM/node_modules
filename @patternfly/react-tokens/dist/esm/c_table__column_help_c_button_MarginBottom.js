export const c_table__column_help_c_button_MarginBottom = {
  "name": "--pf-c-table__column-help--c-button--MarginBottom",
  "value": "calc(0.375rem * -1)",
  "var": "var(--pf-c-table__column-help--c-button--MarginBottom)"
};
export default c_table__column_help_c_button_MarginBottom;
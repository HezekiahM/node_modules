export const c_accordion = {
  ".pf-c-accordion": {
    "global_Color_100": {
      "name": "--pf-global--Color--100",
      "value": "#151515",
      "values": [
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "global_Color_200": {
      "name": "--pf-global--Color--200",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--dark-200",
        "$pf-global--Color--dark-200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "global_BorderColor_100": {
      "name": "--pf-global--BorderColor--100",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--dark-100",
        "$pf-global--BorderColor--dark-100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "global_primary_color_100": {
      "name": "--pf-global--primary-color--100",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--dark-100",
        "$pf-global--primary-color--dark-100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "global_link_Color": {
      "name": "--pf-global--link--Color",
      "value": "#06c",
      "values": [
        "--pf-global--link--Color--dark",
        "$pf-global--link--Color--dark",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "global_link_Color_hover": {
      "name": "--pf-global--link--Color--hover",
      "value": "#004080",
      "values": [
        "--pf-global--link--Color--dark--hover",
        "$pf-global--link--Color--dark--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "global_BackgroundColor_100": {
      "name": "--pf-global--BackgroundColor--100",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--light-100",
        "$pf-global--BackgroundColor--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_accordion_BackgroundColor": {
      "name": "--pf-c-accordion--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_accordion__toggle_PaddingTop": {
      "name": "--pf-c-accordion__toggle--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_accordion__toggle_PaddingRight": {
      "name": "--pf-c-accordion__toggle--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_accordion__toggle_PaddingBottom": {
      "name": "--pf-c-accordion__toggle--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_accordion__toggle_PaddingLeft": {
      "name": "--pf-c-accordion__toggle--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_accordion__toggle_before_BackgroundColor": {
      "name": "--pf-c-accordion__toggle--before--BackgroundColor",
      "value": "transparent"
    },
    "c_accordion__toggle_hover_BackgroundColor": {
      "name": "--pf-c-accordion__toggle--hover--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--200",
        "$pf-global--BackgroundColor--200",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_accordion__toggle_focus_BackgroundColor": {
      "name": "--pf-c-accordion__toggle--focus--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--200",
        "$pf-global--BackgroundColor--200",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_accordion__toggle_active_BackgroundColor": {
      "name": "--pf-c-accordion__toggle--active--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--200",
        "$pf-global--BackgroundColor--200",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_accordion__toggle_before_Width": {
      "name": "--pf-c-accordion__toggle--before--Width",
      "value": "3px",
      "values": [
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    },
    "c_accordion__toggle_m_expanded_before_BackgroundColor": {
      "name": "--pf-c-accordion__toggle--m-expanded--before--BackgroundColor",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_accordion__toggle_text_MaxWidth": {
      "name": "--pf-c-accordion__toggle-text--MaxWidth",
      "value": "calc(100% - 1.5rem)",
      "values": [
        "calc(100% - --pf-global--spacer--lg)",
        "calc(100% - $pf-global--spacer--lg)",
        "calc(100% - pf-size-prem(24px))",
        "calc(100% - 1.5rem)"
      ]
    },
    "c_accordion__toggle_hover__toggle_text_Color": {
      "name": "--pf-c-accordion__toggle--hover__toggle-text--Color",
      "value": "#06c",
      "values": [
        "--pf-global--link--Color",
        "$pf-global--link--Color",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_accordion__toggle_active__toggle_text_Color": {
      "name": "--pf-c-accordion__toggle--active__toggle-text--Color",
      "value": "#06c",
      "values": [
        "--pf-global--link--Color",
        "$pf-global--link--Color",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_accordion__toggle_active__toggle_text_FontWeight": {
      "name": "--pf-c-accordion__toggle--active__toggle-text--FontWeight",
      "value": "700",
      "values": [
        "--pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    },
    "c_accordion__toggle_focus__toggle_text_Color": {
      "name": "--pf-c-accordion__toggle--focus__toggle-text--Color",
      "value": "#06c",
      "values": [
        "--pf-global--link--Color",
        "$pf-global--link--Color",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_accordion__toggle_focus__toggle_text_FontWeight": {
      "name": "--pf-c-accordion__toggle--focus__toggle-text--FontWeight",
      "value": "700",
      "values": [
        "--pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    },
    "c_accordion__toggle_m_expanded__toggle_text_Color": {
      "name": "--pf-c-accordion__toggle--m-expanded__toggle-text--Color",
      "value": "#06c",
      "values": [
        "--pf-global--link--Color",
        "$pf-global--link--Color",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_accordion__toggle_m_expanded__toggle_text_FontWeight": {
      "name": "--pf-c-accordion__toggle--m-expanded__toggle-text--FontWeight",
      "value": "700",
      "values": [
        "--pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    },
    "c_accordion__toggle_icon_Transition": {
      "name": "--pf-c-accordion__toggle-icon--Transition",
      "value": ".2s ease-in 0s"
    },
    "c_accordion__toggle_m_expanded__toggle_icon_Rotate": {
      "name": "--pf-c-accordion__toggle--m-expanded__toggle-icon--Rotate",
      "value": "90deg"
    },
    "c_accordion__expanded_content_body_PaddingTop": {
      "name": "--pf-c-accordion__expanded-content-body--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_accordion__expanded_content_body_PaddingRight": {
      "name": "--pf-c-accordion__expanded-content-body--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_accordion__expanded_content_body_PaddingBottom": {
      "name": "--pf-c-accordion__expanded-content-body--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_accordion__expanded_content_body_PaddingLeft": {
      "name": "--pf-c-accordion__expanded-content-body--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_accordion__expanded_content_Color": {
      "name": "--pf-c-accordion__expanded-content--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--secondary-color--100",
        "$pf-global--secondary-color--100",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_accordion__expanded_content_FontSize": {
      "name": "--pf-c-accordion__expanded-content--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_accordion__expanded_content_body_before_BackgroundColor": {
      "name": "--pf-c-accordion__expanded-content-body--before--BackgroundColor",
      "value": "transparent"
    },
    "c_accordion__expanded_content_body_before_Width": {
      "name": "--pf-c-accordion__expanded-content-body--before--Width",
      "value": "3px",
      "values": [
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    },
    "c_accordion__expanded_content_m_expanded__expanded_content_body_before_BackgroundColor": {
      "name": "--pf-c-accordion__expanded-content--m-expanded__expanded-content-body--before--BackgroundColor",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_accordion__expanded_content_m_fixed_MaxHeight": {
      "name": "--pf-c-accordion__expanded-content--m-fixed--MaxHeight",
      "value": "9.375rem"
    }
  },
  ".pf-c-accordion__toggle.pf-m-expanded": {
    "c_accordion__toggle_before_BackgroundColor": {
      "name": "--pf-c-accordion__toggle--before--BackgroundColor",
      "value": "#06c",
      "values": [
        "--pf-c-accordion__toggle--m-expanded--before--BackgroundColor",
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-accordion__expanded-content.pf-m-expanded": {
    "c_accordion__expanded_content_body_before_BackgroundColor": {
      "name": "--pf-c-accordion__expanded-content-body--before--BackgroundColor",
      "value": "#06c",
      "values": [
        "--pf-c-accordion__expanded-content--m-expanded__expanded-content-body--before--BackgroundColor",
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  }
};
export default c_accordion;
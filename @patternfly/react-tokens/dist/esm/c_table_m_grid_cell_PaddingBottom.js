export const c_table_m_grid_cell_PaddingBottom = {
  "name": "--pf-c-table--m-grid--cell--PaddingBottom",
  "value": "0",
  "var": "var(--pf-c-table--m-grid--cell--PaddingBottom)"
};
export default c_table_m_grid_cell_PaddingBottom;
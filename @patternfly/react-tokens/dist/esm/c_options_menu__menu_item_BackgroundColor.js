export const c_options_menu__menu_item_BackgroundColor = {
  "name": "--pf-c-options-menu__menu-item--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-options-menu__menu-item--BackgroundColor)"
};
export default c_options_menu__menu_item_BackgroundColor;
export const c_nav__link_m_current_before_BorderBottomWidth = {
  "name": "--pf-c-nav__link--m-current--before--BorderBottomWidth",
  "value": "3px",
  "var": "var(--pf-c-nav__link--m-current--before--BorderBottomWidth)"
};
export default c_nav__link_m_current_before_BorderBottomWidth;
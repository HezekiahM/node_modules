export const c_search_input = {
  ".pf-c-search-input": {
    "c_search_input__text_before_BorderWidth": {
      "name": "--pf-c-search-input__text--before--BorderWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_search_input__text_before_BorderColor": {
      "name": "--pf-c-search-input__text--before--BorderColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_search_input__text_after_BorderBottomWidth": {
      "name": "--pf-c-search-input__text--after--BorderBottomWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_search_input__text_after_BorderBottomColor": {
      "name": "--pf-c-search-input__text--after--BorderBottomColor",
      "value": "#8a8d90",
      "values": [
        "--pf-global--BorderColor--200",
        "$pf-global--BorderColor--200",
        "$pf-color-black-500",
        "#8a8d90"
      ]
    },
    "c_search_input_hover__text_after_BorderBottomColor": {
      "name": "--pf-c-search-input--hover__text--after--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_search_input__text_focus_within_after_BorderBottomWidth": {
      "name": "--pf-c-search-input__text--focus-within--after--BorderBottomWidth",
      "value": "2px",
      "values": [
        "--pf-global--BorderWidth--md",
        "$pf-global--BorderWidth--md",
        "2px"
      ]
    },
    "c_search_input__text_focus_within_after_BorderBottomColor": {
      "name": "--pf-c-search-input__text--focus-within--after--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_search_input__text_input_PaddingTop": {
      "name": "--pf-c-search-input__text-input--PaddingTop",
      "value": "0.375rem",
      "values": [
        "--pf-global--spacer--form-element",
        "$pf-global--spacer--form-element",
        "pf-size-prem(6px)",
        "0.375rem"
      ]
    },
    "c_search_input__text_input_PaddingRight": {
      "name": "--pf-c-search-input__text-input--PaddingRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_search_input__text_input_PaddingBottom": {
      "name": "--pf-c-search-input__text-input--PaddingBottom",
      "value": "0.375rem",
      "values": [
        "--pf-global--spacer--form-element",
        "$pf-global--spacer--form-element",
        "pf-size-prem(6px)",
        "0.375rem"
      ]
    },
    "c_search_input__text_input_PaddingLeft": {
      "name": "--pf-c-search-input__text-input--PaddingLeft",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_search_input__text_input_MinWidth": {
      "name": "--pf-c-search-input__text-input--MinWidth",
      "value": "6ch"
    },
    "c_search_input__icon_Left": {
      "name": "--pf-c-search-input__icon--Left",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_search_input__icon_Color": {
      "name": "--pf-c-search-input__icon--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--200",
        "$pf-global--Color--200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_search_input__text_hover__icon_Color": {
      "name": "--pf-c-search-input__text--hover__icon--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_search_input__icon_TranslateY": {
      "name": "--pf-c-search-input__icon--TranslateY",
      "value": "-50%"
    },
    "c_search_input__utilities_MarginRight": {
      "name": "--pf-c-search-input__utilities--MarginRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_search_input__utilities_MarginLeft": {
      "name": "--pf-c-search-input__utilities--MarginLeft",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_search_input__utilities_child_MarginLeft": {
      "name": "--pf-c-search-input__utilities--child--MarginLeft",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_search_input__utilities_c_button_PaddingRight": {
      "name": "--pf-c-search-input__utilities--c-button--PaddingRight",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_search_input__utilities_c_button_PaddingLeft": {
      "name": "--pf-c-search-input__utilities--c-button--PaddingLeft",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    }
  },
  ".pf-c-search-input:hover": {
    "c_search_input__text_after_BorderBottomColor": {
      "name": "--pf-c-search-input__text--after--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-c-search-input--hover__text--after--BorderBottomColor",
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-search-input__text:hover": {
    "c_search_input__icon_Color": {
      "name": "--pf-c-search-input__icon--Color",
      "value": "#151515",
      "values": [
        "--pf-c-search-input__text--hover__icon--Color",
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    }
  },
  ".pf-c-search-input__text:focus-within": {
    "c_search_input__text_after_BorderBottomWidth": {
      "name": "--pf-c-search-input__text--after--BorderBottomWidth",
      "value": "2px",
      "values": [
        "--pf-c-search-input__text--focus-within--after--BorderBottomWidth",
        "--pf-global--BorderWidth--md",
        "$pf-global--BorderWidth--md",
        "2px"
      ]
    },
    "c_search_input__text_after_BorderBottomColor": {
      "name": "--pf-c-search-input__text--after--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-c-search-input__text--focus-within--after--BorderBottomColor",
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-search-input__utilities .pf-c-button": {
    "c_button_PaddingRight": {
      "name": "--pf-c-button--PaddingRight",
      "value": "0.25rem",
      "values": [
        "--pf-c-search-input__utilities--c-button--PaddingRight",
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_button_PaddingLeft": {
      "name": "--pf-c-button--PaddingLeft",
      "value": "0.25rem",
      "values": [
        "--pf-c-search-input__utilities--c-button--PaddingLeft",
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    }
  }
};
export default c_search_input;
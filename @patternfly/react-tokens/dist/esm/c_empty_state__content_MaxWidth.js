export const c_empty_state__content_MaxWidth = {
  "name": "--pf-c-empty-state__content--MaxWidth",
  "value": "37.5rem",
  "var": "var(--pf-c-empty-state__content--MaxWidth)"
};
export default c_empty_state__content_MaxWidth;
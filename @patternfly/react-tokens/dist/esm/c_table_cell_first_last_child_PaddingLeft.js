export const c_table_cell_first_last_child_PaddingLeft = {
  "name": "--pf-c-table--cell--first-last-child--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-table--cell--first-last-child--PaddingLeft)"
};
export default c_table_cell_first_last_child_PaddingLeft;
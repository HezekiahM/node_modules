export const c_label_BorderRadius = {
  "name": "--pf-c-label--BorderRadius",
  "value": "30em",
  "var": "var(--pf-c-label--BorderRadius)"
};
export default c_label_BorderRadius;
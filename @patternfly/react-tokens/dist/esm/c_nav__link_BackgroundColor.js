export const c_nav__link_BackgroundColor = {
  "name": "--pf-c-nav__link--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-nav__link--BackgroundColor)"
};
export default c_nav__link_BackgroundColor;
export const c_hint_BorderWidth = {
  "name": "--pf-c-hint--BorderWidth",
  "value": "1px",
  "var": "var(--pf-c-hint--BorderWidth)"
};
export default c_hint_BorderWidth;
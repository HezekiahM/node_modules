export const global_FontFamily_monospace = {
  "name": "--pf-global--FontFamily--monospace",
  "value": "\"overpass-mono\", overpass-mono, \"SFMono-Regular\", menlo, monaco, consolas, \"Liberation Mono\", \"Courier New\", monospace",
  "var": "var(--pf-global--FontFamily--monospace)"
};
export default global_FontFamily_monospace;
export const c_hint__title_FontSize = {
  "name": "--pf-c-hint__title--FontSize",
  "value": "1.125rem",
  "var": "var(--pf-c-hint__title--FontSize)"
};
export default c_hint__title_FontSize;
export const c_context_selector__menu_search_PaddingLeft = {
  "name": "--pf-c-context-selector__menu-search--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-context-selector__menu-search--PaddingLeft)"
};
export default c_context_selector__menu_search_PaddingLeft;
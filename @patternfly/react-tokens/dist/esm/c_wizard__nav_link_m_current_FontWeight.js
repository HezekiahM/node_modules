export const c_wizard__nav_link_m_current_FontWeight = {
  "name": "--pf-c-wizard__nav-link--m-current--FontWeight",
  "value": "700",
  "var": "var(--pf-c-wizard__nav-link--m-current--FontWeight)"
};
export default c_wizard__nav_link_m_current_FontWeight;
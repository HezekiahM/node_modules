export const c_label__c_button_PaddingRight = {
  "name": "--pf-c-label__c-button--PaddingRight",
  "value": "0.5rem",
  "var": "var(--pf-c-label__c-button--PaddingRight)"
};
export default c_label__c_button_PaddingRight;
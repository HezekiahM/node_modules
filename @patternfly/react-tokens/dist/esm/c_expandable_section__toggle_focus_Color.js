export const c_expandable_section__toggle_focus_Color = {
  "name": "--pf-c-expandable-section__toggle--focus--Color",
  "value": "#004080",
  "var": "var(--pf-c-expandable-section__toggle--focus--Color)"
};
export default c_expandable_section__toggle_focus_Color;
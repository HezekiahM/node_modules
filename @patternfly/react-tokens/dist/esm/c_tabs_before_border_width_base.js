export const c_tabs_before_border_width_base = {
  "name": "--pf-c-tabs--before--border-width--base",
  "value": "1px",
  "var": "var(--pf-c-tabs--before--border-width--base)"
};
export default c_tabs_before_border_width_base;
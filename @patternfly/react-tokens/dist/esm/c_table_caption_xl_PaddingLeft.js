export const c_table_caption_xl_PaddingLeft = {
  "name": "--pf-c-table-caption--xl--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-table-caption--xl--PaddingLeft)"
};
export default c_table_caption_xl_PaddingLeft;
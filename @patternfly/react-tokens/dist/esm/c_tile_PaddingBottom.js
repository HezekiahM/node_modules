export const c_tile_PaddingBottom = {
  "name": "--pf-c-tile--PaddingBottom",
  "value": "1.5rem",
  "var": "var(--pf-c-tile--PaddingBottom)"
};
export default c_tile_PaddingBottom;
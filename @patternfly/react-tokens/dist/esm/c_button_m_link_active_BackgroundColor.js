export const c_button_m_link_active_BackgroundColor = {
  "name": "--pf-c-button--m-link--active--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-button--m-link--active--BackgroundColor)"
};
export default c_button_m_link_active_BackgroundColor;
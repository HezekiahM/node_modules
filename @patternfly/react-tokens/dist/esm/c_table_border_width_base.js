export const c_table_border_width_base = {
  "name": "--pf-c-table--border-width--base",
  "value": "0",
  "var": "var(--pf-c-table--border-width--base)"
};
export default c_table_border_width_base;
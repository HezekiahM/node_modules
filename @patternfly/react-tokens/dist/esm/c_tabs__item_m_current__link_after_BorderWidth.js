export const c_tabs__item_m_current__link_after_BorderWidth = {
  "name": "--pf-c-tabs__item--m-current__link--after--BorderWidth",
  "value": "3px",
  "var": "var(--pf-c-tabs__item--m-current__link--after--BorderWidth)"
};
export default c_tabs__item_m_current__link_after_BorderWidth;
export const c_nav__link_PaddingTop = {
  "name": "--pf-c-nav__link--PaddingTop",
  "value": "0.5rem",
  "var": "var(--pf-c-nav__link--PaddingTop)"
};
export default c_nav__link_PaddingTop;
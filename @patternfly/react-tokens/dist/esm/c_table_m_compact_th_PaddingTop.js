export const c_table_m_compact_th_PaddingTop = {
  "name": "--pf-c-table--m-compact-th--PaddingTop",
  "value": "calc(0.5rem + 0.25rem)",
  "var": "var(--pf-c-table--m-compact-th--PaddingTop)"
};
export default c_table_m_compact_th_PaddingTop;
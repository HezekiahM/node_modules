export const c_tile_PaddingRight = {
  "name": "--pf-c-tile--PaddingRight",
  "value": "1.5rem",
  "var": "var(--pf-c-tile--PaddingRight)"
};
export default c_tile_PaddingRight;
export const l_stack_m_gutter_MarginBottom = {
  "name": "--pf-l-stack--m-gutter--MarginBottom",
  "value": "1rem",
  "var": "var(--pf-l-stack--m-gutter--MarginBottom)"
};
export default l_stack_m_gutter_MarginBottom;
export const c_list = {
  ".pf-c-list": {
    "c_list_PaddingLeft": {
      "name": "--pf-c-list--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_list_nested_MarginTop": {
      "name": "--pf-c-list--nested--MarginTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_list_nested_MarginLeft": {
      "name": "--pf-c-list--nested--MarginLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_list_ul_ListStyle": {
      "name": "--pf-c-list--ul--ListStyle",
      "value": "disc outside",
      "values": [
        "--pf-global--ListStyle",
        "$pf-global--ListStyle",
        "disc outside"
      ]
    },
    "c_list_li_MarginTop": {
      "name": "--pf-c-list--li--MarginTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_list_m_inline_li_MarginRight": {
      "name": "--pf-c-list--m-inline--li--MarginRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    }
  },
  ".pf-c-list.pf-m-inline": {
    "c_list_PaddingLeft": {
      "name": "--pf-c-list--PaddingLeft",
      "value": "0"
    }
  },
  ".pf-c-list.pf-m-inline li": {
    "c_list_li_MarginTop": {
      "name": "--pf-c-list--li--MarginTop",
      "value": "0"
    }
  }
};
export default c_list;
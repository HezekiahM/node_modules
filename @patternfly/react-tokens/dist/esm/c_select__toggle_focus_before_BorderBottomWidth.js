export const c_select__toggle_focus_before_BorderBottomWidth = {
  "name": "--pf-c-select__toggle--focus--before--BorderBottomWidth",
  "value": "2px",
  "var": "var(--pf-c-select__toggle--focus--before--BorderBottomWidth)"
};
export default c_select__toggle_focus_before_BorderBottomWidth;
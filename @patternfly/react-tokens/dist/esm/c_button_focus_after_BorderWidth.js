export const c_button_focus_after_BorderWidth = {
  "name": "--pf-c-button--focus--after--BorderWidth",
  "value": "2px",
  "var": "var(--pf-c-button--focus--after--BorderWidth)"
};
export default c_button_focus_after_BorderWidth;
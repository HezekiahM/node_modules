export const c_hint_Color = {
  "name": "--pf-c-hint--Color",
  "value": "#151515",
  "var": "var(--pf-c-hint--Color)"
};
export default c_hint_Color;
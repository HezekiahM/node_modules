export const c_expandable_section__toggle_text_MarginLeft = {
  "name": "--pf-c-expandable-section__toggle-text--MarginLeft",
  "value": "calc(0.25rem + 0.5rem)",
  "var": "var(--pf-c-expandable-section__toggle-text--MarginLeft)"
};
export default c_expandable_section__toggle_text_MarginLeft;
export const c_data_list__item_before_sm_Top = {
  "name": "--pf-c-data-list__item--before--sm--Top",
  "value": "calc(0.5rem * -1)",
  "var": "var(--pf-c-data-list__item--before--sm--Top)"
};
export default c_data_list__item_before_sm_Top;
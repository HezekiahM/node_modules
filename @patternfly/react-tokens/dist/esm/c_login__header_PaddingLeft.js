export const c_login__header_PaddingLeft = {
  "name": "--pf-c-login__header--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-login__header--PaddingLeft)"
};
export default c_login__header_PaddingLeft;
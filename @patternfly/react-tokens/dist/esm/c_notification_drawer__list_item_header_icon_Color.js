export const c_notification_drawer__list_item_header_icon_Color = {
  "name": "--pf-c-notification-drawer__list-item-header-icon--Color",
  "value": "#3e8635",
  "var": "var(--pf-c-notification-drawer__list-item-header-icon--Color)"
};
export default c_notification_drawer__list_item_header_icon_Color;
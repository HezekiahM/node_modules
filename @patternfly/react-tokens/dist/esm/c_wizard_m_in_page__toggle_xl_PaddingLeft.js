export const c_wizard_m_in_page__toggle_xl_PaddingLeft = {
  "name": "--pf-c-wizard--m-in-page__toggle--xl--PaddingLeft",
  "value": "calc(2rem + 1.5rem + 0.5rem)",
  "var": "var(--pf-c-wizard--m-in-page__toggle--xl--PaddingLeft)"
};
export default c_wizard_m_in_page__toggle_xl_PaddingLeft;
export const c_page = {
  ".pf-c-page__sidebar.pf-m-light": {
    "global_Color_100": {
      "name": "--pf-global--Color--100",
      "value": "#151515",
      "values": [
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "global_Color_200": {
      "name": "--pf-global--Color--200",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--dark-200",
        "$pf-global--Color--dark-200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "global_BorderColor_100": {
      "name": "--pf-global--BorderColor--100",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--dark-100",
        "$pf-global--BorderColor--dark-100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "global_primary_color_100": {
      "name": "--pf-global--primary-color--100",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--dark-100",
        "$pf-global--primary-color--dark-100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "global_link_Color": {
      "name": "--pf-global--link--Color",
      "value": "#06c",
      "values": [
        "--pf-global--link--Color--dark",
        "$pf-global--link--Color--dark",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "global_link_Color_hover": {
      "name": "--pf-global--link--Color--hover",
      "value": "#004080",
      "values": [
        "--pf-global--link--Color--dark--hover",
        "$pf-global--link--Color--dark--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "global_BackgroundColor_100": {
      "name": "--pf-global--BackgroundColor--100",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--light-100",
        "$pf-global--BackgroundColor--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_page__sidebar_BackgroundColor": {
      "name": "--pf-c-page__sidebar--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-c-page__sidebar--m-light--BackgroundColor",
        "--pf-global--BackgroundColor--light-100",
        "$pf-global--BackgroundColor--light-100",
        "$pf-color-white",
        "#fff"
      ]
    }
  },
  ".pf-c-page__header": {
    "global_Color_100": {
      "name": "--pf-global--Color--100",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "global_Color_200": {
      "name": "--pf-global--Color--200",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--Color--light-200",
        "$pf-global--Color--light-200",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "global_BorderColor_100": {
      "name": "--pf-global--BorderColor--100",
      "value": "#b8bbbe",
      "values": [
        "--pf-global--BorderColor--light-100",
        "$pf-global--BorderColor--light-100",
        "$pf-color-black-400",
        "#b8bbbe"
      ]
    },
    "global_primary_color_100": {
      "name": "--pf-global--primary-color--100",
      "value": "#73bcf7",
      "values": [
        "--pf-global--primary-color--light-100",
        "$pf-global--primary-color--light-100",
        "$pf-color-blue-200",
        "#73bcf7"
      ]
    },
    "global_link_Color": {
      "name": "--pf-global--link--Color",
      "value": "#73bcf7",
      "values": [
        "--pf-global--link--Color--light",
        "$pf-global--link--Color--light",
        "$pf-global--active-color--300",
        "$pf-color-blue-200",
        "#73bcf7"
      ]
    },
    "global_link_Color_hover": {
      "name": "--pf-global--link--Color--hover",
      "value": "#73bcf7",
      "values": [
        "--pf-global--link--Color--light",
        "$pf-global--link--Color--light",
        "$pf-global--active-color--300",
        "$pf-color-blue-200",
        "#73bcf7"
      ]
    },
    "global_BackgroundColor_100": {
      "name": "--pf-global--BackgroundColor--100",
      "value": "#151515",
      "values": [
        "--pf-global--BackgroundColor--dark-100",
        "$pf-global--BackgroundColor--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    }
  },
  ".pf-c-page__header .pf-c-card": {
    "c_card_BackgroundColor": {
      "name": "--pf-c-card--BackgroundColor",
      "value": "rgba(#030303, .32)",
      "values": [
        "--pf-global--BackgroundColor--dark-transparent-200",
        "$pf-global--BackgroundColor--dark-transparent-200",
        "rgba($pf-color-black-1000, .32)",
        "rgba(#030303, .32)"
      ]
    }
  },
  ".pf-c-page__header .pf-c-button": {
    "c_button_m_primary_Color": {
      "name": "--pf-c-button--m-primary--Color",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--dark-100",
        "$pf-global--primary-color--dark-100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_primary_hover_Color": {
      "name": "--pf-c-button--m-primary--hover--Color",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--dark-100",
        "$pf-global--primary-color--dark-100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_primary_focus_Color": {
      "name": "--pf-c-button--m-primary--focus--Color",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--dark-100",
        "$pf-global--primary-color--dark-100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_primary_active_Color": {
      "name": "--pf-c-button--m-primary--active--Color",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--dark-100",
        "$pf-global--primary-color--dark-100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_primary_BackgroundColor": {
      "name": "--pf-c-button--m-primary--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--light-100",
        "$pf-global--BackgroundColor--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_primary_hover_BackgroundColor": {
      "name": "--pf-c-button--m-primary--hover--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_button_m_primary_focus_BackgroundColor": {
      "name": "--pf-c-button--m-primary--focus--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_button_m_primary_active_BackgroundColor": {
      "name": "--pf-c-button--m-primary--active--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_button_m_secondary_Color": {
      "name": "--pf-c-button--m-secondary--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_hover_Color": {
      "name": "--pf-c-button--m-secondary--hover--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_focus_Color": {
      "name": "--pf-c-button--m-secondary--focus--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_active_Color": {
      "name": "--pf-c-button--m-secondary--active--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_BorderColor": {
      "name": "--pf-c-button--m-secondary--BorderColor",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_hover_BorderColor": {
      "name": "--pf-c-button--m-secondary--hover--BorderColor",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_focus_BorderColor": {
      "name": "--pf-c-button--m-secondary--focus--BorderColor",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_active_BorderColor": {
      "name": "--pf-c-button--m-secondary--active--BorderColor",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    }
  },
  ".pf-c-page__header-tools-group": {
    "hidden_visible_visible_Visibility": {
      "name": "--pf-hidden-visible--visible--Visibility",
      "value": "visible"
    },
    "hidden_visible_hidden_Display": {
      "name": "--pf-hidden-visible--hidden--Display",
      "value": "none"
    },
    "hidden_visible_hidden_Visibility": {
      "name": "--pf-hidden-visible--hidden--Visibility",
      "value": "hidden"
    },
    "hidden_visible_Display": {
      "name": "--pf-hidden-visible--Display",
      "value": "flex",
      "values": [
        "--pf-hidden-visible--visible--Display",
        "--pf-c-page__header-tools-group--Display",
        "flex"
      ]
    },
    "hidden_visible_Visibility": {
      "name": "--pf-hidden-visible--Visibility",
      "value": "visible",
      "values": [
        "--pf-hidden-visible--visible--Visibility",
        "visible"
      ]
    },
    "hidden_visible_visible_Display": {
      "name": "--pf-hidden-visible--visible--Display",
      "value": "flex",
      "values": [
        "--pf-c-page__header-tools-group--Display",
        "flex"
      ]
    }
  },
  ".pf-m-hidden.pf-c-page__header-tools-group": {
    "hidden_visible_Display": {
      "name": "--pf-hidden-visible--Display",
      "value": "none",
      "values": [
        "--pf-hidden-visible--hidden--Display",
        "none"
      ]
    },
    "hidden_visible_Visibility": {
      "name": "--pf-hidden-visible--Visibility",
      "value": "hidden",
      "values": [
        "--pf-hidden-visible--hidden--Visibility",
        "hidden"
      ]
    }
  },
  ".pf-c-page": {
    "c_page_BackgroundColor": {
      "name": "--pf-c-page--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_page__header_BackgroundColor": {
      "name": "--pf-c-page__header--BackgroundColor",
      "value": "#151515",
      "values": [
        "--pf-global--BackgroundColor--dark-100",
        "$pf-global--BackgroundColor--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_page__header_ZIndex": {
      "name": "--pf-c-page__header--ZIndex",
      "value": "300",
      "values": [
        "--pf-global--ZIndex--md",
        "$pf-global--ZIndex--md",
        "300"
      ]
    },
    "c_page__header_MinHeight": {
      "name": "--pf-c-page__header--MinHeight",
      "value": "4.75rem"
    },
    "c_page__header_brand_PaddingLeft": {
      "name": "--pf-c-page__header-brand--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_page__header_brand_xl_PaddingRight": {
      "name": "--pf-c-page__header-brand--xl--PaddingRight",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_page__header_brand_xl_PaddingLeft": {
      "name": "--pf-c-page__header-brand--xl--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_page__header_sidebar_toggle__c_button_PaddingTop": {
      "name": "--pf-c-page__header-sidebar-toggle__c-button--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_page__header_sidebar_toggle__c_button_PaddingRight": {
      "name": "--pf-c-page__header-sidebar-toggle__c-button--PaddingRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_page__header_sidebar_toggle__c_button_PaddingBottom": {
      "name": "--pf-c-page__header-sidebar-toggle__c-button--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_page__header_sidebar_toggle__c_button_PaddingLeft": {
      "name": "--pf-c-page__header-sidebar-toggle__c-button--PaddingLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_page__header_sidebar_toggle__c_button_MarginRight": {
      "name": "--pf-c-page__header-sidebar-toggle__c-button--MarginRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_page__header_sidebar_toggle__c_button_MarginLeft": {
      "name": "--pf-c-page__header-sidebar-toggle__c-button--MarginLeft",
      "value": "calc(0.5rem * -1)",
      "values": [
        "calc(--pf-c-page__header-sidebar-toggle__c-button--PaddingLeft * -1)",
        "calc(--pf-global--spacer--sm * -1)",
        "calc($pf-global--spacer--sm * -1)",
        "calc(pf-size-prem(8px) * -1)",
        "calc(0.5rem * -1)"
      ]
    },
    "c_page__header_sidebar_toggle__c_button_FontSize": {
      "name": "--pf-c-page__header-sidebar-toggle__c-button--FontSize",
      "value": "1.5rem",
      "values": [
        "--pf-global--FontSize--2xl",
        "$pf-global--FontSize--2xl",
        "pf-font-prem(24px)",
        "1.5rem"
      ]
    },
    "c_page__header_brand_link_c_brand_MaxHeight": {
      "name": "--pf-c-page__header-brand-link--c-brand--MaxHeight",
      "value": "3.75rem"
    },
    "c_page__header_nav_BackgroundColor": {
      "name": "--pf-c-page__header-nav--BackgroundColor",
      "value": "#212427",
      "values": [
        "--pf-global--BackgroundColor--dark-300",
        "$pf-global--BackgroundColor--dark-300",
        "$pf-color-black-850",
        "#212427"
      ]
    },
    "c_page__header_nav_xl_BackgroundColor": {
      "name": "--pf-c-page__header-nav--xl--BackgroundColor",
      "value": "transparent"
    },
    "c_page__header_nav_xl_PaddingRight": {
      "name": "--pf-c-page__header-nav--xl--PaddingRight",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_page__header_nav_xl_PaddingLeft": {
      "name": "--pf-c-page__header-nav--xl--PaddingLeft",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_page__header_tools_MarginRight": {
      "name": "--pf-c-page__header-tools--MarginRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_page__header_tools_xl_MarginRight": {
      "name": "--pf-c-page__header-tools--xl--MarginRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_page__header_tools_c_avatar_MarginLeft": {
      "name": "--pf-c-page__header-tools--c-avatar--MarginLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_page__header_tools_group_MarginLeft": {
      "name": "--pf-c-page__header-tools-group--MarginLeft",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_page__header_tools_group_Display": {
      "name": "--pf-c-page__header-tools-group--Display",
      "value": "flex"
    },
    "c_page__header_tools_item_Display": {
      "name": "--pf-c-page__header-tools-item--Display",
      "value": "block"
    },
    "c_page__header_tools_c_button_m_selected_before_Width": {
      "name": "--pf-c-page__header-tools--c-button--m-selected--before--Width",
      "value": "2.25rem"
    },
    "c_page__header_tools_c_button_m_selected_before_Height": {
      "name": "--pf-c-page__header-tools--c-button--m-selected--before--Height",
      "value": "2.25rem"
    },
    "c_page__header_tools_c_button_m_selected_before_BackgroundColor": {
      "name": "--pf-c-page__header-tools--c-button--m-selected--before--BackgroundColor",
      "value": "#3c3f42",
      "values": [
        "--pf-global--BackgroundColor--dark-200",
        "$pf-global--BackgroundColor--dark-200",
        "$pf-color-black-800",
        "#3c3f42"
      ]
    },
    "c_page__header_tools_c_button_m_selected_before_BorderRadius": {
      "name": "--pf-c-page__header-tools--c-button--m-selected--before--BorderRadius",
      "value": "30em",
      "values": [
        "--pf-global--BorderRadius--lg",
        "$pf-global--BorderRadius--lg",
        "30em"
      ]
    },
    "c_page__header_tools_c_button_m_selected_c_notification_badge_m_unread_after_BorderColor": {
      "name": "--pf-c-page__header-tools--c-button--m-selected--c-notification-badge--m-unread--after--BorderColor",
      "value": "#3c3f42",
      "values": [
        "--pf-global--BackgroundColor--dark-200",
        "$pf-global--BackgroundColor--dark-200",
        "$pf-color-black-800",
        "#3c3f42"
      ]
    },
    "c_page__sidebar_ZIndex": {
      "name": "--pf-c-page__sidebar--ZIndex",
      "value": "200",
      "values": [
        "--pf-global--ZIndex--sm",
        "$pf-global--ZIndex--sm",
        "200"
      ]
    },
    "c_page__sidebar_Width": {
      "name": "--pf-c-page__sidebar--Width",
      "value": "18.125rem"
    },
    "c_page__sidebar_BackgroundColor": {
      "name": "--pf-c-page__sidebar--BackgroundColor",
      "value": "#212427",
      "values": [
        "--pf-global--BackgroundColor--dark-300",
        "$pf-global--BackgroundColor--dark-300",
        "$pf-color-black-850",
        "#212427"
      ]
    },
    "c_page__sidebar_m_light_BackgroundColor": {
      "name": "--pf-c-page__sidebar--m-light--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--light-100",
        "$pf-global--BackgroundColor--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_page__sidebar_BoxShadow": {
      "name": "--pf-c-page__sidebar--BoxShadow",
      "value": "0.75rem 0 0.75rem -0.5rem rgba(3, 3, 3, 0.18)",
      "values": [
        "--pf-global--BoxShadow--lg-right",
        "$pf-global--BoxShadow--lg-right",
        "pf-size-prem(12px) 0 pf-size-prem(12px) pf-size-prem(-8px) rgba($pf-color-black-1000, .18)",
        "pf-size-prem(12px) 0 pf-size-prem(12px) pf-size-prem(-8px) rgba(#030303, .18)",
        "0.75rem 0 0.75rem -0.5rem rgba(3, 3, 3, 0.18)"
      ]
    },
    "c_page__sidebar_Transition": {
      "name": "--pf-c-page__sidebar--Transition",
      "value": "all 250ms cubic-bezier(.42, 0, .58, 1)",
      "values": [
        "--pf-global--Transition",
        "$pf-global--Transition",
        "all 250ms cubic-bezier(.42, 0, .58, 1)"
      ]
    },
    "c_page__sidebar_TranslateX": {
      "name": "--pf-c-page__sidebar--TranslateX",
      "value": "-100%"
    },
    "c_page__sidebar_TranslateZ": {
      "name": "--pf-c-page__sidebar--TranslateZ",
      "value": "0"
    },
    "c_page__sidebar_m_expanded_TranslateX": {
      "name": "--pf-c-page__sidebar--m-expanded--TranslateX",
      "value": "0"
    },
    "c_page__sidebar_xl_TranslateX": {
      "name": "--pf-c-page__sidebar--xl--TranslateX",
      "value": "0"
    },
    "c_page__sidebar_body_PaddingTop": {
      "name": "--pf-c-page__sidebar-body--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_page__sidebar_body_PaddingBottom": {
      "name": "--pf-c-page__sidebar-body--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_page__main_section_PaddingTop": {
      "name": "--pf-c-page__main-section--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_page__main_section_PaddingRight": {
      "name": "--pf-c-page__main-section--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_page__main_section_PaddingBottom": {
      "name": "--pf-c-page__main-section--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_page__main_section_PaddingLeft": {
      "name": "--pf-c-page__main-section--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_page__main_section_xl_PaddingTop": {
      "name": "--pf-c-page__main-section--xl--PaddingTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_page__main_section_xl_PaddingRight": {
      "name": "--pf-c-page__main-section--xl--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_page__main_section_xl_PaddingBottom": {
      "name": "--pf-c-page__main-section--xl--PaddingBottom",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_page__main_section_xl_PaddingLeft": {
      "name": "--pf-c-page__main-section--xl--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_page__main_section_BackgroundColor": {
      "name": "--pf-c-page__main-section--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_page__main_ZIndex": {
      "name": "--pf-c-page__main--ZIndex",
      "value": "100",
      "values": [
        "--pf-global--ZIndex--xs",
        "$pf-global--ZIndex--xs",
        "100"
      ]
    },
    "c_page__main_breadcrumb_main_section_PaddingTop": {
      "name": "--pf-c-page__main-breadcrumb--main-section--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_page__main_nav_BackgroundColor": {
      "name": "--pf-c-page__main-nav--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--light-100",
        "$pf-global--BackgroundColor--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_page__main_nav_PaddingTop": {
      "name": "--pf-c-page__main-nav--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_page__main_nav_PaddingRight": {
      "name": "--pf-c-page__main-nav--PaddingRight",
      "value": "0"
    },
    "c_page__main_nav_PaddingLeft": {
      "name": "--pf-c-page__main-nav--PaddingLeft",
      "value": "0"
    },
    "c_page__main_nav_xl_PaddingRight": {
      "name": "--pf-c-page__main-nav--xl--PaddingRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_page__main_nav_xl_PaddingLeft": {
      "name": "--pf-c-page__main-nav--xl--PaddingLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_page__main_breadcrumb_BackgroundColor": {
      "name": "--pf-c-page__main-breadcrumb--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--light-100",
        "$pf-global--BackgroundColor--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_page__main_breadcrumb_PaddingTop": {
      "name": "--pf-c-page__main-breadcrumb--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_page__main_breadcrumb_PaddingRight": {
      "name": "--pf-c-page__main-breadcrumb--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_page__main_breadcrumb_PaddingBottom": {
      "name": "--pf-c-page__main-breadcrumb--PaddingBottom",
      "value": "0"
    },
    "c_page__main_breadcrumb_PaddingLeft": {
      "name": "--pf-c-page__main-breadcrumb--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_page__main_breadcrumb_xl_PaddingRight": {
      "name": "--pf-c-page__main-breadcrumb--xl--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_page__main_breadcrumb_xl_PaddingLeft": {
      "name": "--pf-c-page__main-breadcrumb--xl--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_page__main_section_m_light_BackgroundColor": {
      "name": "--pf-c-page__main-section--m-light--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--light-100",
        "$pf-global--BackgroundColor--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_page__main_section_m_dark_100_BackgroundColor": {
      "name": "--pf-c-page__main-section--m-dark-100--BackgroundColor",
      "value": "rgba(#030303, .62)",
      "values": [
        "--pf-global--BackgroundColor--dark-transparent-100",
        "$pf-global--BackgroundColor--dark-transparent-100",
        "rgba($pf-color-black-1000, .62)",
        "rgba(#030303, .62)"
      ]
    },
    "c_page__main_section_m_dark_200_BackgroundColor": {
      "name": "--pf-c-page__main-section--m-dark-200--BackgroundColor",
      "value": "rgba(#030303, .32)",
      "values": [
        "--pf-global--BackgroundColor--dark-transparent-200",
        "$pf-global--BackgroundColor--dark-transparent-200",
        "rgba($pf-color-black-1000, .32)",
        "rgba(#030303, .32)"
      ]
    },
    "c_page__main_wizard_BorderTopColor": {
      "name": "--pf-c-page__main-wizard--BorderTopColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_page__main_wizard_BorderTopWidth": {
      "name": "--pf-c-page__main-wizard--BorderTopWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    }
  },
  ".pf-c-page__header-tools-item": {
    "hidden_visible_visible_Display": {
      "name": "--pf-hidden-visible--visible--Display",
      "value": "block",
      "values": [
        "--pf-c-page__header-tools-item--Display",
        "block"
      ]
    }
  },
  ".pf-c-page__sidebar.pf-m-expanded": {
    "c_page__sidebar_TranslateX": {
      "name": "--pf-c-page__sidebar--TranslateX",
      "value": "0",
      "values": [
        "--pf-c-page__sidebar--m-expanded--TranslateX",
        "0"
      ]
    }
  },
  ".pf-c-page__main-breadcrumb + .pf-c-page__main-section": {
    "c_page__main_section_PaddingTop": {
      "name": "--pf-c-page__main-section--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-c-page__main-breadcrumb--main-section--PaddingTop",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    }
  },
  ".pf-c-page__main-section.pf-m-light": {
    "c_page__main_section_BackgroundColor": {
      "name": "--pf-c-page__main-section--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-c-page__main-section--m-light--BackgroundColor",
        "--pf-global--BackgroundColor--light-100",
        "$pf-global--BackgroundColor--light-100",
        "$pf-color-white",
        "#fff"
      ]
    }
  },
  ".pf-c-page__main-section.pf-m-dark-100": {
    "c_page__main_section_BackgroundColor": {
      "name": "--pf-c-page__main-section--BackgroundColor",
      "value": "rgba(#030303, .62)",
      "values": [
        "--pf-c-page__main-section--m-dark-100--BackgroundColor",
        "--pf-global--BackgroundColor--dark-transparent-100",
        "$pf-global--BackgroundColor--dark-transparent-100",
        "rgba($pf-color-black-1000, .62)",
        "rgba(#030303, .62)"
      ]
    }
  },
  ".pf-c-page__main-section.pf-m-dark-200": {
    "c_page__main_section_BackgroundColor": {
      "name": "--pf-c-page__main-section--BackgroundColor",
      "value": "rgba(#030303, .32)",
      "values": [
        "--pf-c-page__main-section--m-dark-200--BackgroundColor",
        "--pf-global--BackgroundColor--dark-transparent-200",
        "$pf-global--BackgroundColor--dark-transparent-200",
        "rgba($pf-color-black-1000, .32)",
        "rgba(#030303, .32)"
      ]
    }
  }
};
export default c_page;
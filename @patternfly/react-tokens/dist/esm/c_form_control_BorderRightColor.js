export const c_form_control_BorderRightColor = {
  "name": "--pf-c-form-control--BorderRightColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-form-control--BorderRightColor)"
};
export default c_form_control_BorderRightColor;
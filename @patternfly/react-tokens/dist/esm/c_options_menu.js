export const c_options_menu = {
  ".pf-c-options-menu": {
    "c_options_menu__toggle_BackgroundColor": {
      "name": "--pf-c-options-menu__toggle--BackgroundColor",
      "value": "transparent"
    },
    "c_options_menu__toggle_PaddingTop": {
      "name": "--pf-c-options-menu__toggle--PaddingTop",
      "value": "0.375rem",
      "values": [
        "--pf-global--spacer--form-element",
        "$pf-global--spacer--form-element",
        "pf-size-prem(6px)",
        "0.375rem"
      ]
    },
    "c_options_menu__toggle_PaddingRight": {
      "name": "--pf-c-options-menu__toggle--PaddingRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_options_menu__toggle_PaddingBottom": {
      "name": "--pf-c-options-menu__toggle--PaddingBottom",
      "value": "0.375rem",
      "values": [
        "--pf-global--spacer--form-element",
        "$pf-global--spacer--form-element",
        "pf-size-prem(6px)",
        "0.375rem"
      ]
    },
    "c_options_menu__toggle_PaddingLeft": {
      "name": "--pf-c-options-menu__toggle--PaddingLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_options_menu__toggle_MinWidth": {
      "name": "--pf-c-options-menu__toggle--MinWidth",
      "value": "44px",
      "values": [
        "--pf-global--target-size--MinWidth",
        "$pf-global--target-size--MinWidth",
        "44px"
      ]
    },
    "c_options_menu__toggle_LineHeight": {
      "name": "--pf-c-options-menu__toggle--LineHeight",
      "value": "1.5",
      "values": [
        "--pf-global--LineHeight--md",
        "$pf-global--LineHeight--md",
        "1.5"
      ]
    },
    "c_options_menu__toggle_BorderWidth": {
      "name": "--pf-c-options-menu__toggle--BorderWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_options_menu__toggle_BorderTopColor": {
      "name": "--pf-c-options-menu__toggle--BorderTopColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_options_menu__toggle_BorderRightColor": {
      "name": "--pf-c-options-menu__toggle--BorderRightColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_options_menu__toggle_BorderBottomColor": {
      "name": "--pf-c-options-menu__toggle--BorderBottomColor",
      "value": "#8a8d90",
      "values": [
        "--pf-global--BorderColor--200",
        "$pf-global--BorderColor--200",
        "$pf-color-black-500",
        "#8a8d90"
      ]
    },
    "c_options_menu__toggle_BorderLeftColor": {
      "name": "--pf-c-options-menu__toggle--BorderLeftColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_options_menu__toggle_Color": {
      "name": "--pf-c-options-menu__toggle--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_options_menu__toggle_hover_BorderBottomColor": {
      "name": "--pf-c-options-menu__toggle--hover--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_options_menu__toggle_active_BorderBottomWidth": {
      "name": "--pf-c-options-menu__toggle--active--BorderBottomWidth",
      "value": "2px",
      "values": [
        "--pf-global--BorderWidth--md",
        "$pf-global--BorderWidth--md",
        "2px"
      ]
    },
    "c_options_menu__toggle_active_BorderBottomColor": {
      "name": "--pf-c-options-menu__toggle--active--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_options_menu__toggle_focus_BorderBottomWidth": {
      "name": "--pf-c-options-menu__toggle--focus--BorderBottomWidth",
      "value": "2px",
      "values": [
        "--pf-global--BorderWidth--md",
        "$pf-global--BorderWidth--md",
        "2px"
      ]
    },
    "c_options_menu__toggle_focus_BorderBottomColor": {
      "name": "--pf-c-options-menu__toggle--focus--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_options_menu__toggle_expanded_BorderBottomWidth": {
      "name": "--pf-c-options-menu__toggle--expanded--BorderBottomWidth",
      "value": "2px",
      "values": [
        "--pf-global--BorderWidth--md",
        "$pf-global--BorderWidth--md",
        "2px"
      ]
    },
    "c_options_menu__toggle_expanded_BorderBottomColor": {
      "name": "--pf-c-options-menu__toggle--expanded--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_options_menu__toggle_disabled_BackgroundColor": {
      "name": "--pf-c-options-menu__toggle--disabled--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--disabled-color--300",
        "$pf-global--disabled-color--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_options_menu__toggle_m_plain_Color": {
      "name": "--pf-c-options-menu__toggle--m-plain--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--200",
        "$pf-global--Color--200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_options_menu__toggle_m_plain_hover_Color": {
      "name": "--pf-c-options-menu__toggle--m-plain--hover--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_options_menu__toggle_m_plain_disabled_Color": {
      "name": "--pf-c-options-menu__toggle--m-plain--disabled--Color",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--disabled-color--200",
        "$pf-global--disabled-color--200",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_options_menu__toggle_icon_MarginRight": {
      "name": "--pf-c-options-menu__toggle-icon--MarginRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_options_menu__toggle_icon_MarginLeft": {
      "name": "--pf-c-options-menu__toggle-icon--MarginLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_options_menu_m_top_m_expanded__toggle_icon_Rotate": {
      "name": "--pf-c-options-menu--m-top--m-expanded__toggle-icon--Rotate",
      "value": "180deg"
    },
    "c_options_menu__toggle_button_BackgroundColor": {
      "name": "--pf-c-options-menu__toggle-button--BackgroundColor",
      "value": "transparent"
    },
    "c_options_menu__toggle_button_PaddingTop": {
      "name": "--pf-c-options-menu__toggle-button--PaddingTop",
      "value": "0.375rem",
      "values": [
        "--pf-global--spacer--form-element",
        "$pf-global--spacer--form-element",
        "pf-size-prem(6px)",
        "0.375rem"
      ]
    },
    "c_options_menu__toggle_button_PaddingRight": {
      "name": "--pf-c-options-menu__toggle-button--PaddingRight",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_options_menu__toggle_button_PaddingBottom": {
      "name": "--pf-c-options-menu__toggle-button--PaddingBottom",
      "value": "0.375rem",
      "values": [
        "--pf-global--spacer--form-element",
        "$pf-global--spacer--form-element",
        "pf-size-prem(6px)",
        "0.375rem"
      ]
    },
    "c_options_menu__toggle_button_PaddingLeft": {
      "name": "--pf-c-options-menu__toggle-button--PaddingLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_options_menu__menu_BackgroundColor": {
      "name": "--pf-c-options-menu__menu--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--light-100",
        "$pf-global--BackgroundColor--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_options_menu__menu_BoxShadow": {
      "name": "--pf-c-options-menu__menu--BoxShadow",
      "value": "0 0.25rem 0.5rem 0rem rgba(3, 3, 3, 0.12), 0 0 0.25rem 0 rgba(3, 3, 3, 0.06)",
      "values": [
        "--pf-global--BoxShadow--md",
        "$pf-global--BoxShadow--md",
        "0 pf-size-prem(4px) pf-size-prem(8px) pf-size-prem(0) rgba($pf-color-black-1000, .12), 0 0 pf-size-prem(4px) 0 rgba($pf-color-black-1000, .06)",
        "0 pf-size-prem(4px) pf-size-prem(8px) pf-size-prem(0) rgba(#030303, .12), 0 0 pf-size-prem(4px) 0 rgba(#030303, .06)",
        "0 0.25rem 0.5rem 0rem rgba(3, 3, 3, 0.12), 0 0 0.25rem 0 rgba(3, 3, 3, 0.06)"
      ]
    },
    "c_options_menu__menu_PaddingTop": {
      "name": "--pf-c-options-menu__menu--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_options_menu__menu_PaddingBottom": {
      "name": "--pf-c-options-menu__menu--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_options_menu__menu_Top": {
      "name": "--pf-c-options-menu__menu--Top",
      "value": "calc(100% + 0.25rem)",
      "values": [
        "calc(100% + --pf-global--spacer--xs)",
        "calc(100% + $pf-global--spacer--xs)",
        "calc(100% + pf-size-prem(4px))",
        "calc(100% + 0.25rem)"
      ]
    },
    "c_options_menu__menu_ZIndex": {
      "name": "--pf-c-options-menu__menu--ZIndex",
      "value": "200",
      "values": [
        "--pf-global--ZIndex--sm",
        "$pf-global--ZIndex--sm",
        "200"
      ]
    },
    "c_options_menu_m_top__menu_Top": {
      "name": "--pf-c-options-menu--m-top__menu--Top",
      "value": "0"
    },
    "c_options_menu_m_top__menu_TranslateY": {
      "name": "--pf-c-options-menu--m-top__menu--TranslateY",
      "value": "calc(-100% - 0.25rem)",
      "values": [
        "calc(-100% - --pf-global--spacer--xs)",
        "calc(-100% - $pf-global--spacer--xs)",
        "calc(-100% - pf-size-prem(4px))",
        "calc(-100% - 0.25rem)"
      ]
    },
    "c_options_menu__menu_item_BackgroundColor": {
      "name": "--pf-c-options-menu__menu-item--BackgroundColor",
      "value": "transparent"
    },
    "c_options_menu__menu_item_Color": {
      "name": "--pf-c-options-menu__menu-item--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_options_menu__menu_item_FontSize": {
      "name": "--pf-c-options-menu__menu-item--FontSize",
      "value": "1rem",
      "values": [
        "--pf-global--FontSize--md",
        "$pf-global--FontSize--md",
        "pf-font-prem(16px)",
        "1rem"
      ]
    },
    "c_options_menu__menu_item_PaddingTop": {
      "name": "--pf-c-options-menu__menu-item--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_options_menu__menu_item_PaddingRight": {
      "name": "--pf-c-options-menu__menu-item--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_options_menu__menu_item_PaddingBottom": {
      "name": "--pf-c-options-menu__menu-item--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_options_menu__menu_item_PaddingLeft": {
      "name": "--pf-c-options-menu__menu-item--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_options_menu__menu_item_disabled_Color": {
      "name": "--pf-c-options-menu__menu-item--disabled--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--dark-200",
        "$pf-global--Color--dark-200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_options_menu__menu_item_hover_BackgroundColor": {
      "name": "--pf-c-options-menu__menu-item--hover--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_options_menu__menu_item_disabled_BackgroundColor": {
      "name": "--pf-c-options-menu__menu-item--disabled--BackgroundColor",
      "value": "transparent"
    },
    "c_options_menu__menu_item_icon_Color": {
      "name": "--pf-c-options-menu__menu-item-icon--Color",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_options_menu__menu_item_icon_FontSize": {
      "name": "--pf-c-options-menu__menu-item-icon--FontSize",
      "value": "0.625rem",
      "values": [
        "--pf-global--icon--FontSize--sm",
        "$pf-global--icon--FontSize--sm",
        "pf-font-prem(10px)",
        "0.625rem"
      ]
    },
    "c_options_menu__menu_item_icon_PaddingLeft": {
      "name": "--pf-c-options-menu__menu-item-icon--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_options_menu__group_group_PaddingTop": {
      "name": "--pf-c-options-menu__group--group--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_options_menu__group_title_PaddingTop": {
      "name": "--pf-c-options-menu__group-title--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_options_menu__group_title_PaddingRight": {
      "name": "--pf-c-options-menu__group-title--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-c-options-menu__menu-item--PaddingRight",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_options_menu__group_title_PaddingBottom": {
      "name": "--pf-c-options-menu__group-title--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-c-options-menu__menu-item--PaddingBottom",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_options_menu__group_title_PaddingLeft": {
      "name": "--pf-c-options-menu__group-title--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-c-options-menu__menu-item--PaddingLeft",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_options_menu__group_title_FontSize": {
      "name": "--pf-c-options-menu__group-title--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_options_menu__group_title_FontWeight": {
      "name": "--pf-c-options-menu__group-title--FontWeight",
      "value": "700",
      "values": [
        "--pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    },
    "c_options_menu__group_title_Color": {
      "name": "--pf-c-options-menu__group-title--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--dark-200",
        "$pf-global--Color--dark-200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_options_menu_c_divider_MarginTop": {
      "name": "--pf-c-options-menu--c-divider--MarginTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_options_menu_c_divider_MarginBottom": {
      "name": "--pf-c-options-menu--c-divider--MarginBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-c-options-menu .pf-c-divider:last-child": {
    "c_options_menu_c_divider_MarginBottom": {
      "name": "--pf-c-options-menu--c-divider--MarginBottom",
      "value": "0"
    }
  },
  ".pf-c-options-menu__toggle:not(.pf-m-plain):hover::before": {
    "c_options_menu__toggle_BorderBottomColor": {
      "name": "--pf-c-options-menu__toggle--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-c-options-menu__toggle--hover--BorderBottomColor",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-options-menu__toggle:not(.pf-m-plain):active::before": {
    "c_options_menu__toggle_BorderBottomColor": {
      "name": "--pf-c-options-menu__toggle--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-c-options-menu__toggle--active--BorderBottomColor",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-options-menu__toggle:not(.pf-m-plain):focus::before": {
    "c_options_menu__toggle_BorderBottomColor": {
      "name": "--pf-c-options-menu__toggle--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-c-options-menu__toggle--focus--BorderBottomColor",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-options-menu.pf-m-expanded > .pf-c-options-menu__toggle::before": {
    "c_options_menu__toggle_BorderBottomColor": {
      "name": "--pf-c-options-menu__toggle--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-c-options-menu__toggle--expanded--BorderBottomColor",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-options-menu__toggle.pf-m-plain:hover": {
    "c_options_menu__toggle_m_plain_Color": {
      "name": "--pf-c-options-menu__toggle--m-plain--Color",
      "value": "#151515",
      "values": [
        "--pf-c-options-menu__toggle--m-plain--hover--Color",
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    }
  },
  ".pf-c-options-menu__toggle.pf-m-plain.pf-m-disabled": {
    "c_options_menu__toggle_m_plain_Color": {
      "name": "--pf-c-options-menu__toggle--m-plain--Color",
      "value": "#d2d2d2",
      "values": [
        "--pf-c-options-menu__toggle--m-plain--disabled--Color",
        "--pf-global--disabled-color--200",
        "$pf-global--disabled-color--200",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    }
  },
  ".pf-c-options-menu__toggle.pf-m-disabled:not(.pf-m-plain)": {
    "c_options_menu__toggle_BackgroundColor": {
      "name": "--pf-c-options-menu__toggle--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-c-options-menu__toggle--disabled--BackgroundColor",
        "--pf-global--disabled-color--300",
        "$pf-global--disabled-color--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    }
  },
  ".pf-c-options-menu.pf-m-top .pf-c-options-menu__menu": {
    "c_options_menu__menu_Top": {
      "name": "--pf-c-options-menu__menu--Top",
      "value": "0",
      "values": [
        "--pf-c-options-menu--m-top__menu--Top",
        "0"
      ]
    }
  }
};
export default c_options_menu;
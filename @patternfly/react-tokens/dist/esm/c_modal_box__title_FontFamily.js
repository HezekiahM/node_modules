export const c_modal_box__title_FontFamily = {
  "name": "--pf-c-modal-box__title--FontFamily",
  "value": "\"RedHatDisplay\", \"Overpass\", overpass, helvetica, arial, sans-serif",
  "var": "var(--pf-c-modal-box__title--FontFamily)"
};
export default c_modal_box__title_FontFamily;
export const c_button_FontWeight = {
  "name": "--pf-c-button--FontWeight",
  "value": "700",
  "var": "var(--pf-c-button--FontWeight)"
};
export default c_button_FontWeight;
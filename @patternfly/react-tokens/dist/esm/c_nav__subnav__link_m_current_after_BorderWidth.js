export const c_nav__subnav__link_m_current_after_BorderWidth = {
  "name": "--pf-c-nav__subnav__link--m-current--after--BorderWidth",
  "value": "4px",
  "var": "var(--pf-c-nav__subnav__link--m-current--after--BorderWidth)"
};
export default c_nav__subnav__link_m_current_after_BorderWidth;
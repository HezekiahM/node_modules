export const c_form_control_m_warning_BackgroundSize = {
  "name": "--pf-c-form-control--m-warning--BackgroundSize",
  "value": "1.25rem 1rem",
  "var": "var(--pf-c-form-control--m-warning--BackgroundSize)"
};
export default c_form_control_m_warning_BackgroundSize;
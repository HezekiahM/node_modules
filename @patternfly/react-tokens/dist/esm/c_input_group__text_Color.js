export const c_input_group__text_Color = {
  "name": "--pf-c-input-group__text--Color",
  "value": "#6a6e73",
  "var": "var(--pf-c-input-group__text--Color)"
};
export default c_input_group__text_Color;
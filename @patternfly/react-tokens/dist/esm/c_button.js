export const c_button = {
  ".pf-c-button": {
    "c_button_PaddingTop": {
      "name": "--pf-c-button--PaddingTop",
      "value": "0.375rem",
      "values": [
        "--pf-global--spacer--form-element",
        "$pf-global--spacer--form-element",
        "pf-size-prem(6px)",
        "0.375rem"
      ]
    },
    "c_button_PaddingRight": {
      "name": "--pf-c-button--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_button_PaddingBottom": {
      "name": "--pf-c-button--PaddingBottom",
      "value": "0.375rem",
      "values": [
        "--pf-global--spacer--form-element",
        "$pf-global--spacer--form-element",
        "pf-size-prem(6px)",
        "0.375rem"
      ]
    },
    "c_button_PaddingLeft": {
      "name": "--pf-c-button--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_button_LineHeight": {
      "name": "--pf-c-button--LineHeight",
      "value": "1.5",
      "values": [
        "--pf-global--LineHeight--md",
        "$pf-global--LineHeight--md",
        "1.5"
      ]
    },
    "c_button_FontWeight": {
      "name": "--pf-c-button--FontWeight",
      "value": "400",
      "values": [
        "--pf-global--FontWeight--normal",
        "$pf-global--FontWeight--normal",
        "400"
      ]
    },
    "c_button_FontSize": {
      "name": "--pf-c-button--FontSize",
      "value": "1rem",
      "values": [
        "--pf-global--FontSize--md",
        "$pf-global--FontSize--md",
        "pf-font-prem(16px)",
        "1rem"
      ]
    },
    "c_button_BorderRadius": {
      "name": "--pf-c-button--BorderRadius",
      "value": "3px",
      "values": [
        "--pf-global--BorderRadius--sm",
        "$pf-global--BorderRadius--sm",
        "3px"
      ]
    },
    "c_button_after_BorderRadius": {
      "name": "--pf-c-button--after--BorderRadius",
      "value": "3px",
      "values": [
        "--pf-global--BorderRadius--sm",
        "$pf-global--BorderRadius--sm",
        "3px"
      ]
    },
    "c_button_after_BorderColor": {
      "name": "--pf-c-button--after--BorderColor",
      "value": "transparent"
    },
    "c_button_after_BorderWidth": {
      "name": "--pf-c-button--after--BorderWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_button_hover_after_BorderWidth": {
      "name": "--pf-c-button--hover--after--BorderWidth",
      "value": "2px",
      "values": [
        "--pf-global--BorderWidth--md",
        "$pf-global--BorderWidth--md",
        "2px"
      ]
    },
    "c_button_focus_after_BorderWidth": {
      "name": "--pf-c-button--focus--after--BorderWidth",
      "value": "2px",
      "values": [
        "--pf-global--BorderWidth--md",
        "$pf-global--BorderWidth--md",
        "2px"
      ]
    },
    "c_button_active_after_BorderWidth": {
      "name": "--pf-c-button--active--after--BorderWidth",
      "value": "2px",
      "values": [
        "--pf-global--BorderWidth--md",
        "$pf-global--BorderWidth--md",
        "2px"
      ]
    },
    "c_button_disabled_Color": {
      "name": "--pf-c-button--disabled--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--disabled-color--100",
        "$pf-global--disabled-color--100",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_button_disabled_BackgroundColor": {
      "name": "--pf-c-button--disabled--BackgroundColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--disabled-color--200",
        "$pf-global--disabled-color--200",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_button_disabled_after_BorderColor": {
      "name": "--pf-c-button--disabled--after--BorderColor",
      "value": "transparent"
    },
    "c_button_m_primary_BackgroundColor": {
      "name": "--pf-c-button--m-primary--BackgroundColor",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_primary_Color": {
      "name": "--pf-c-button--m-primary--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_primary_hover_BackgroundColor": {
      "name": "--pf-c-button--m-primary--hover--BackgroundColor",
      "value": "#004080",
      "values": [
        "--pf-global--primary-color--200",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "c_button_m_primary_hover_Color": {
      "name": "--pf-c-button--m-primary--hover--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_primary_focus_BackgroundColor": {
      "name": "--pf-c-button--m-primary--focus--BackgroundColor",
      "value": "#004080",
      "values": [
        "--pf-global--primary-color--200",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "c_button_m_primary_focus_Color": {
      "name": "--pf-c-button--m-primary--focus--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_primary_active_BackgroundColor": {
      "name": "--pf-c-button--m-primary--active--BackgroundColor",
      "value": "#004080",
      "values": [
        "--pf-global--primary-color--200",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "c_button_m_primary_active_Color": {
      "name": "--pf-c-button--m-primary--active--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_BackgroundColor": {
      "name": "--pf-c-button--m-secondary--BackgroundColor",
      "value": "transparent"
    },
    "c_button_m_secondary_after_BorderColor": {
      "name": "--pf-c-button--m-secondary--after--BorderColor",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_secondary_Color": {
      "name": "--pf-c-button--m-secondary--Color",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_secondary_hover_BackgroundColor": {
      "name": "--pf-c-button--m-secondary--hover--BackgroundColor",
      "value": "transparent"
    },
    "c_button_m_secondary_hover_after_BorderColor": {
      "name": "--pf-c-button--m-secondary--hover--after--BorderColor",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_secondary_hover_Color": {
      "name": "--pf-c-button--m-secondary--hover--Color",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_secondary_focus_BackgroundColor": {
      "name": "--pf-c-button--m-secondary--focus--BackgroundColor",
      "value": "transparent"
    },
    "c_button_m_secondary_focus_after_BorderColor": {
      "name": "--pf-c-button--m-secondary--focus--after--BorderColor",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_secondary_focus_Color": {
      "name": "--pf-c-button--m-secondary--focus--Color",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_secondary_active_BackgroundColor": {
      "name": "--pf-c-button--m-secondary--active--BackgroundColor",
      "value": "transparent"
    },
    "c_button_m_secondary_active_after_BorderColor": {
      "name": "--pf-c-button--m-secondary--active--after--BorderColor",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_secondary_active_Color": {
      "name": "--pf-c-button--m-secondary--active--Color",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_tertiary_BackgroundColor": {
      "name": "--pf-c-button--m-tertiary--BackgroundColor",
      "value": "transparent"
    },
    "c_button_m_tertiary_after_BorderColor": {
      "name": "--pf-c-button--m-tertiary--after--BorderColor",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_button_m_tertiary_Color": {
      "name": "--pf-c-button--m-tertiary--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_button_m_tertiary_hover_BackgroundColor": {
      "name": "--pf-c-button--m-tertiary--hover--BackgroundColor",
      "value": "transparent"
    },
    "c_button_m_tertiary_hover_after_BorderColor": {
      "name": "--pf-c-button--m-tertiary--hover--after--BorderColor",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_button_m_tertiary_hover_Color": {
      "name": "--pf-c-button--m-tertiary--hover--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_button_m_tertiary_focus_BackgroundColor": {
      "name": "--pf-c-button--m-tertiary--focus--BackgroundColor",
      "value": "transparent"
    },
    "c_button_m_tertiary_focus_after_BorderColor": {
      "name": "--pf-c-button--m-tertiary--focus--after--BorderColor",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_button_m_tertiary_focus_Color": {
      "name": "--pf-c-button--m-tertiary--focus--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_button_m_tertiary_active_BackgroundColor": {
      "name": "--pf-c-button--m-tertiary--active--BackgroundColor",
      "value": "transparent"
    },
    "c_button_m_tertiary_active_after_BorderColor": {
      "name": "--pf-c-button--m-tertiary--active--after--BorderColor",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_button_m_tertiary_active_Color": {
      "name": "--pf-c-button--m-tertiary--active--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_button_m_danger_BackgroundColor": {
      "name": "--pf-c-button--m-danger--BackgroundColor",
      "value": "#c9190b",
      "values": [
        "--pf-global--danger-color--100",
        "$pf-global--danger-color--100",
        "$pf-color-red-100",
        "#c9190b"
      ]
    },
    "c_button_m_danger_Color": {
      "name": "--pf-c-button--m-danger--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_danger_hover_BackgroundColor": {
      "name": "--pf-c-button--m-danger--hover--BackgroundColor",
      "value": "#a30000",
      "values": [
        "--pf-global--danger-color--200",
        "$pf-global--danger-color--200",
        "$pf-color-red-200",
        "#a30000"
      ]
    },
    "c_button_m_danger_hover_Color": {
      "name": "--pf-c-button--m-danger--hover--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_danger_focus_BackgroundColor": {
      "name": "--pf-c-button--m-danger--focus--BackgroundColor",
      "value": "#a30000",
      "values": [
        "--pf-global--danger-color--200",
        "$pf-global--danger-color--200",
        "$pf-color-red-200",
        "#a30000"
      ]
    },
    "c_button_m_danger_focus_Color": {
      "name": "--pf-c-button--m-danger--focus--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_danger_active_BackgroundColor": {
      "name": "--pf-c-button--m-danger--active--BackgroundColor",
      "value": "#a30000",
      "values": [
        "--pf-global--danger-color--200",
        "$pf-global--danger-color--200",
        "$pf-color-red-200",
        "#a30000"
      ]
    },
    "c_button_m_danger_active_Color": {
      "name": "--pf-c-button--m-danger--active--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_link_BackgroundColor": {
      "name": "--pf-c-button--m-link--BackgroundColor",
      "value": "transparent"
    },
    "c_button_m_link_Color": {
      "name": "--pf-c-button--m-link--Color",
      "value": "#06c",
      "values": [
        "--pf-global--link--Color",
        "$pf-global--link--Color",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_link_hover_BackgroundColor": {
      "name": "--pf-c-button--m-link--hover--BackgroundColor",
      "value": "transparent"
    },
    "c_button_m_link_hover_Color": {
      "name": "--pf-c-button--m-link--hover--Color",
      "value": "#004080",
      "values": [
        "--pf-global--link--Color--hover",
        "$pf-global--link--Color--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "c_button_m_link_focus_BackgroundColor": {
      "name": "--pf-c-button--m-link--focus--BackgroundColor",
      "value": "transparent"
    },
    "c_button_m_link_focus_Color": {
      "name": "--pf-c-button--m-link--focus--Color",
      "value": "#004080",
      "values": [
        "--pf-global--link--Color--hover",
        "$pf-global--link--Color--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "c_button_m_link_active_BackgroundColor": {
      "name": "--pf-c-button--m-link--active--BackgroundColor",
      "value": "transparent"
    },
    "c_button_m_link_active_Color": {
      "name": "--pf-c-button--m-link--active--Color",
      "value": "#004080",
      "values": [
        "--pf-global--link--Color--hover",
        "$pf-global--link--Color--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "c_button_m_link_disabled_BackgroundColor": {
      "name": "--pf-c-button--m-link--disabled--BackgroundColor",
      "value": "transparent"
    },
    "c_button_m_link_m_inline_FontSize": {
      "name": "--pf-c-button--m-link--m-inline--FontSize",
      "value": "inherit"
    },
    "c_button_m_link_m_inline_hover_TextDecoration": {
      "name": "--pf-c-button--m-link--m-inline--hover--TextDecoration",
      "value": "underline",
      "values": [
        "--pf-global--link--TextDecoration--hover",
        "$pf-global--link--TextDecoration--hover",
        "underline"
      ]
    },
    "c_button_m_link_m_inline_hover_Color": {
      "name": "--pf-c-button--m-link--m-inline--hover--Color",
      "value": "#004080",
      "values": [
        "--pf-global--link--Color--hover",
        "$pf-global--link--Color--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "c_button_m_plain_BackgroundColor": {
      "name": "--pf-c-button--m-plain--BackgroundColor",
      "value": "transparent"
    },
    "c_button_m_plain_Color": {
      "name": "--pf-c-button--m-plain--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--200",
        "$pf-global--Color--200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_button_m_plain_hover_BackgroundColor": {
      "name": "--pf-c-button--m-plain--hover--BackgroundColor",
      "value": "transparent"
    },
    "c_button_m_plain_hover_Color": {
      "name": "--pf-c-button--m-plain--hover--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_button_m_plain_focus_BackgroundColor": {
      "name": "--pf-c-button--m-plain--focus--BackgroundColor",
      "value": "transparent"
    },
    "c_button_m_plain_focus_Color": {
      "name": "--pf-c-button--m-plain--focus--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_button_m_plain_active_BackgroundColor": {
      "name": "--pf-c-button--m-plain--active--BackgroundColor",
      "value": "transparent"
    },
    "c_button_m_plain_active_Color": {
      "name": "--pf-c-button--m-plain--active--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_button_m_plain_disabled_Color": {
      "name": "--pf-c-button--m-plain--disabled--Color",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--disabled-color--200",
        "$pf-global--disabled-color--200",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_button_m_plain_disabled_BackgroundColor": {
      "name": "--pf-c-button--m-plain--disabled--BackgroundColor",
      "value": "transparent"
    },
    "c_button_m_control_BackgroundColor": {
      "name": "--pf-c-button--m-control--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_control_Color": {
      "name": "--pf-c-button--m-control--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_button_m_control_BorderRadius": {
      "name": "--pf-c-button--m-control--BorderRadius",
      "value": "0"
    },
    "c_button_m_control_after_BorderWidth": {
      "name": "--pf-c-button--m-control--after--BorderWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_button_m_control_after_BorderTopColor": {
      "name": "--pf-c-button--m-control--after--BorderTopColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_button_m_control_after_BorderRightColor": {
      "name": "--pf-c-button--m-control--after--BorderRightColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_button_m_control_after_BorderBottomColor": {
      "name": "--pf-c-button--m-control--after--BorderBottomColor",
      "value": "#8a8d90",
      "values": [
        "--pf-global--BorderColor--200",
        "$pf-global--BorderColor--200",
        "$pf-color-black-500",
        "#8a8d90"
      ]
    },
    "c_button_m_control_after_BorderLeftColor": {
      "name": "--pf-c-button--m-control--after--BorderLeftColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BorderColor--300",
        "$pf-global--BorderColor--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_button_m_control_disabled_BackgroundColor": {
      "name": "--pf-c-button--m-control--disabled--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--disabled-color--300",
        "$pf-global--disabled-color--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_button_m_control_hover_BackgroundColor": {
      "name": "--pf-c-button--m-control--hover--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_control_hover_Color": {
      "name": "--pf-c-button--m-control--hover--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_button_m_control_hover_after_BorderBottomWidth": {
      "name": "--pf-c-button--m-control--hover--after--BorderBottomWidth",
      "value": "2px",
      "values": [
        "--pf-global--BorderWidth--md",
        "$pf-global--BorderWidth--md",
        "2px"
      ]
    },
    "c_button_m_control_hover_after_BorderBottomColor": {
      "name": "--pf-c-button--m-control--hover--after--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_control_active_BackgroundColor": {
      "name": "--pf-c-button--m-control--active--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_control_active_Color": {
      "name": "--pf-c-button--m-control--active--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_button_m_control_active_after_BorderBottomWidth": {
      "name": "--pf-c-button--m-control--active--after--BorderBottomWidth",
      "value": "2px",
      "values": [
        "--pf-global--BorderWidth--md",
        "$pf-global--BorderWidth--md",
        "2px"
      ]
    },
    "c_button_m_control_active_after_BorderBottomColor": {
      "name": "--pf-c-button--m-control--active--after--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_control_focus_BackgroundColor": {
      "name": "--pf-c-button--m-control--focus--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_control_focus_Color": {
      "name": "--pf-c-button--m-control--focus--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_button_m_control_focus_after_BorderBottomWidth": {
      "name": "--pf-c-button--m-control--focus--after--BorderBottomWidth",
      "value": "2px",
      "values": [
        "--pf-global--BorderWidth--md",
        "$pf-global--BorderWidth--md",
        "2px"
      ]
    },
    "c_button_m_control_focus_after_BorderBottomColor": {
      "name": "--pf-c-button--m-control--focus--after--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_control_m_expanded_BackgroundColor": {
      "name": "--pf-c-button--m-control--m-expanded--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_control_m_expanded_Color": {
      "name": "--pf-c-button--m-control--m-expanded--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_button_m_control_m_expanded_after_BorderBottomWidth": {
      "name": "--pf-c-button--m-control--m-expanded--after--BorderBottomWidth",
      "value": "2px",
      "values": [
        "--pf-global--BorderWidth--md",
        "$pf-global--BorderWidth--md",
        "2px"
      ]
    },
    "c_button_m_control_m_expanded_after_BorderBottomColor": {
      "name": "--pf-c-button--m-control--m-expanded--after--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_small_FontSize": {
      "name": "--pf-c-button--m-small--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_button_m_display_lg_PaddingTop": {
      "name": "--pf-c-button--m-display-lg--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_button_m_display_lg_PaddingRight": {
      "name": "--pf-c-button--m-display-lg--PaddingRight",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_button_m_display_lg_PaddingBottom": {
      "name": "--pf-c-button--m-display-lg--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_button_m_display_lg_PaddingLeft": {
      "name": "--pf-c-button--m-display-lg--PaddingLeft",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_button_m_display_lg_FontWeight": {
      "name": "--pf-c-button--m-display-lg--FontWeight",
      "value": "700",
      "values": [
        "--pf-global--FontWeight--bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    },
    "c_button_m_link_m_display_lg_FontSize": {
      "name": "--pf-c-button--m-link--m-display-lg--FontSize",
      "value": "1.125rem",
      "values": [
        "--pf-global--FontSize--lg",
        "$pf-global--FontSize--lg",
        "pf-font-prem(18px)",
        "1.125rem"
      ]
    },
    "c_button__icon_m_start_MarginRight": {
      "name": "--pf-c-button__icon--m-start--MarginRight",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_button__icon_m_end_MarginLeft": {
      "name": "--pf-c-button__icon--m-end--MarginLeft",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    }
  },
  ".pf-c-button:hover": {
    "c_button_after_BorderWidth": {
      "name": "--pf-c-button--after--BorderWidth",
      "value": "2px",
      "values": [
        "--pf-c-button--hover--after--BorderWidth",
        "--pf-global--BorderWidth--md",
        "$pf-global--BorderWidth--md",
        "2px"
      ]
    }
  },
  ".pf-c-button:focus": {
    "c_button_after_BorderWidth": {
      "name": "--pf-c-button--after--BorderWidth",
      "value": "2px",
      "values": [
        "--pf-c-button--focus--after--BorderWidth",
        "--pf-global--BorderWidth--md",
        "$pf-global--BorderWidth--md",
        "2px"
      ]
    }
  },
  ".pf-c-button:active": {
    "c_button_after_BorderWidth": {
      "name": "--pf-c-button--after--BorderWidth",
      "value": "2px",
      "values": [
        "--pf-c-button--active--after--BorderWidth",
        "--pf-global--BorderWidth--md",
        "$pf-global--BorderWidth--md",
        "2px"
      ]
    }
  },
  ".pf-c-button.pf-m-small": {
    "c_button_FontSize": {
      "name": "--pf-c-button--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-c-button--m-small--FontSize",
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    }
  },
  ".pf-c-button.pf-m-primary.pf-m-display-lg": {
    "c_button_PaddingTop": {
      "name": "--pf-c-button--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-c-button--m-display-lg--PaddingTop",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_button_PaddingRight": {
      "name": "--pf-c-button--PaddingRight",
      "value": "2rem",
      "values": [
        "--pf-c-button--m-display-lg--PaddingRight",
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_button_PaddingBottom": {
      "name": "--pf-c-button--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-c-button--m-display-lg--PaddingBottom",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_button_PaddingLeft": {
      "name": "--pf-c-button--PaddingLeft",
      "value": "2rem",
      "values": [
        "--pf-c-button--m-display-lg--PaddingLeft",
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_button_FontWeight": {
      "name": "--pf-c-button--FontWeight",
      "value": "700",
      "values": [
        "--pf-c-button--m-display-lg--FontWeight",
        "--pf-global--FontWeight--bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    }
  },
  ".pf-c-button.pf-m-primary:hover": {
    "c_button_m_primary_Color": {
      "name": "--pf-c-button--m-primary--Color",
      "value": "#fff",
      "values": [
        "--pf-c-button--m-primary--hover--Color",
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_primary_BackgroundColor": {
      "name": "--pf-c-button--m-primary--BackgroundColor",
      "value": "#004080",
      "values": [
        "--pf-c-button--m-primary--hover--BackgroundColor",
        "--pf-global--primary-color--200",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    }
  },
  ".pf-c-button.pf-m-primary:focus": {
    "c_button_m_primary_Color": {
      "name": "--pf-c-button--m-primary--Color",
      "value": "#fff",
      "values": [
        "--pf-c-button--m-primary--focus--Color",
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_primary_BackgroundColor": {
      "name": "--pf-c-button--m-primary--BackgroundColor",
      "value": "#004080",
      "values": [
        "--pf-c-button--m-primary--focus--BackgroundColor",
        "--pf-global--primary-color--200",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    }
  },
  ".pf-c-button.pf-m-primary:active": {
    "c_button_m_primary_Color": {
      "name": "--pf-c-button--m-primary--Color",
      "value": "#fff",
      "values": [
        "--pf-c-button--m-primary--active--Color",
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_primary_BackgroundColor": {
      "name": "--pf-c-button--m-primary--BackgroundColor",
      "value": "#004080",
      "values": [
        "--pf-c-button--m-primary--active--BackgroundColor",
        "--pf-global--primary-color--200",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    }
  },
  ".pf-c-button.pf-m-secondary": {
    "c_button_after_BorderColor": {
      "name": "--pf-c-button--after--BorderColor",
      "value": "#06c",
      "values": [
        "--pf-c-button--m-secondary--after--BorderColor",
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-button.pf-m-secondary:hover": {
    "c_button_m_secondary_Color": {
      "name": "--pf-c-button--m-secondary--Color",
      "value": "#06c",
      "values": [
        "--pf-c-button--m-secondary--hover--Color",
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_secondary_BackgroundColor": {
      "name": "--pf-c-button--m-secondary--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-button--m-secondary--hover--BackgroundColor",
        "transparent"
      ]
    },
    "c_button_after_BorderColor": {
      "name": "--pf-c-button--after--BorderColor",
      "value": "#06c",
      "values": [
        "--pf-c-button--m-secondary--hover--after--BorderColor",
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-button.pf-m-secondary:focus": {
    "c_button_m_secondary_Color": {
      "name": "--pf-c-button--m-secondary--Color",
      "value": "#06c",
      "values": [
        "--pf-c-button--m-secondary--focus--Color",
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_secondary_BackgroundColor": {
      "name": "--pf-c-button--m-secondary--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-button--m-secondary--focus--BackgroundColor",
        "transparent"
      ]
    },
    "c_button_after_BorderColor": {
      "name": "--pf-c-button--after--BorderColor",
      "value": "#06c",
      "values": [
        "--pf-c-button--m-secondary--focus--after--BorderColor",
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-button.pf-m-secondary.pf-m-active": {
    "c_button_m_secondary_Color": {
      "name": "--pf-c-button--m-secondary--Color",
      "value": "#06c",
      "values": [
        "--pf-c-button--m-secondary--active--Color",
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_secondary_BackgroundColor": {
      "name": "--pf-c-button--m-secondary--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-button--m-secondary--active--BackgroundColor",
        "transparent"
      ]
    },
    "c_button_after_BorderColor": {
      "name": "--pf-c-button--after--BorderColor",
      "value": "#06c",
      "values": [
        "--pf-c-button--m-secondary--active--after--BorderColor",
        "--pf-global--primary-color--100",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-button.pf-m-tertiary": {
    "c_button_after_BorderColor": {
      "name": "--pf-c-button--after--BorderColor",
      "value": "#151515",
      "values": [
        "--pf-c-button--m-tertiary--after--BorderColor",
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    }
  },
  ".pf-c-button.pf-m-tertiary:hover": {
    "c_button_m_tertiary_Color": {
      "name": "--pf-c-button--m-tertiary--Color",
      "value": "#151515",
      "values": [
        "--pf-c-button--m-tertiary--hover--Color",
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_button_m_tertiary_BackgroundColor": {
      "name": "--pf-c-button--m-tertiary--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-button--m-tertiary--hover--BackgroundColor",
        "transparent"
      ]
    },
    "c_button_after_BorderColor": {
      "name": "--pf-c-button--after--BorderColor",
      "value": "#151515",
      "values": [
        "--pf-c-button--m-tertiary--hover--after--BorderColor",
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    }
  },
  ".pf-c-button.pf-m-tertiary:focus": {
    "c_button_m_tertiary_Color": {
      "name": "--pf-c-button--m-tertiary--Color",
      "value": "#151515",
      "values": [
        "--pf-c-button--m-tertiary--focus--Color",
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_button_m_tertiary_BackgroundColor": {
      "name": "--pf-c-button--m-tertiary--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-button--m-tertiary--focus--BackgroundColor",
        "transparent"
      ]
    },
    "c_button_after_BorderColor": {
      "name": "--pf-c-button--after--BorderColor",
      "value": "#151515",
      "values": [
        "--pf-c-button--m-tertiary--focus--after--BorderColor",
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    }
  },
  ".pf-c-button.pf-m-tertiary:active": {
    "c_button_m_tertiary_Color": {
      "name": "--pf-c-button--m-tertiary--Color",
      "value": "#151515",
      "values": [
        "--pf-c-button--m-tertiary--active--Color",
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_button_m_tertiary_BackgroundColor": {
      "name": "--pf-c-button--m-tertiary--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-button--m-tertiary--active--BackgroundColor",
        "transparent"
      ]
    },
    "c_button_after_BorderColor": {
      "name": "--pf-c-button--after--BorderColor",
      "value": "#151515",
      "values": [
        "--pf-c-button--m-tertiary--active--after--BorderColor",
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    }
  },
  ".pf-c-button.pf-m-danger:hover": {
    "c_button_m_danger_Color": {
      "name": "--pf-c-button--m-danger--Color",
      "value": "#fff",
      "values": [
        "--pf-c-button--m-danger--hover--Color",
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_danger_BackgroundColor": {
      "name": "--pf-c-button--m-danger--BackgroundColor",
      "value": "#a30000",
      "values": [
        "--pf-c-button--m-danger--hover--BackgroundColor",
        "--pf-global--danger-color--200",
        "$pf-global--danger-color--200",
        "$pf-color-red-200",
        "#a30000"
      ]
    }
  },
  ".pf-c-button.pf-m-danger:focus": {
    "c_button_m_danger_Color": {
      "name": "--pf-c-button--m-danger--Color",
      "value": "#fff",
      "values": [
        "--pf-c-button--m-danger--focus--Color",
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_danger_BackgroundColor": {
      "name": "--pf-c-button--m-danger--BackgroundColor",
      "value": "#a30000",
      "values": [
        "--pf-c-button--m-danger--focus--BackgroundColor",
        "--pf-global--danger-color--200",
        "$pf-global--danger-color--200",
        "$pf-color-red-200",
        "#a30000"
      ]
    }
  },
  ".pf-c-button.pf-m-danger:active": {
    "c_button_m_danger_Color": {
      "name": "--pf-c-button--m-danger--Color",
      "value": "#fff",
      "values": [
        "--pf-c-button--m-danger--active--Color",
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_danger_BackgroundColor": {
      "name": "--pf-c-button--m-danger--BackgroundColor",
      "value": "#a30000",
      "values": [
        "--pf-c-button--m-danger--active--BackgroundColor",
        "--pf-global--danger-color--200",
        "$pf-global--danger-color--200",
        "$pf-color-red-200",
        "#a30000"
      ]
    }
  },
  ".pf-c-button.pf-m-link": {
    "c_button_disabled_BackgroundColor": {
      "name": "--pf-c-button--disabled--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-button--m-link--disabled--BackgroundColor",
        "transparent"
      ]
    }
  },
  ".pf-c-button.pf-m-link:not(.pf-m-inline):hover": {
    "c_button_m_link_Color": {
      "name": "--pf-c-button--m-link--Color",
      "value": "#004080",
      "values": [
        "--pf-c-button--m-link--hover--Color",
        "--pf-global--link--Color--hover",
        "$pf-global--link--Color--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "c_button_m_link_BackgroundColor": {
      "name": "--pf-c-button--m-link--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-button--m-link--hover--BackgroundColor",
        "transparent"
      ]
    }
  },
  ".pf-c-button.pf-m-link:not(.pf-m-inline):focus": {
    "c_button_m_link_Color": {
      "name": "--pf-c-button--m-link--Color",
      "value": "#004080",
      "values": [
        "--pf-c-button--m-link--focus--Color",
        "--pf-global--link--Color--hover",
        "$pf-global--link--Color--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "c_button_m_link_BackgroundColor": {
      "name": "--pf-c-button--m-link--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-button--m-link--focus--BackgroundColor",
        "transparent"
      ]
    }
  },
  ".pf-c-button.pf-m-link:not(.pf-m-inline):active": {
    "c_button_m_link_Color": {
      "name": "--pf-c-button--m-link--Color",
      "value": "#004080",
      "values": [
        "--pf-c-button--m-link--active--Color",
        "--pf-global--link--Color--hover",
        "$pf-global--link--Color--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "c_button_m_link_BackgroundColor": {
      "name": "--pf-c-button--m-link--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-button--m-link--active--BackgroundColor",
        "transparent"
      ]
    }
  },
  ".pf-c-button.pf-m-link.pf-m-inline": {
    "c_button_FontSize": {
      "name": "--pf-c-button--FontSize",
      "value": "inherit",
      "values": [
        "--pf-c-button--m-link--m-inline--FontSize",
        "inherit"
      ]
    }
  },
  ".pf-c-button.pf-m-link.pf-m-inline:hover": {
    "c_button_m_link_Color": {
      "name": "--pf-c-button--m-link--Color",
      "value": "#004080",
      "values": [
        "--pf-c-button--m-link--m-inline--hover--Color",
        "--pf-global--link--Color--hover",
        "$pf-global--link--Color--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    }
  },
  ".pf-c-button.pf-m-link.pf-m-display-lg": {
    "c_button_FontSize": {
      "name": "--pf-c-button--FontSize",
      "value": "1.125rem",
      "values": [
        "--pf-c-button--m-link--m-display-lg--FontSize",
        "--pf-global--FontSize--lg",
        "$pf-global--FontSize--lg",
        "pf-font-prem(18px)",
        "1.125rem"
      ]
    }
  },
  ".pf-c-button.pf-m-control": {
    "c_button_BorderRadius": {
      "name": "--pf-c-button--BorderRadius",
      "value": "0",
      "values": [
        "--pf-c-button--m-control--BorderRadius",
        "0"
      ]
    },
    "c_button_disabled_BackgroundColor": {
      "name": "--pf-c-button--disabled--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-c-button--m-control--disabled--BackgroundColor",
        "--pf-global--disabled-color--300",
        "$pf-global--disabled-color--300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_button_after_BorderWidth": {
      "name": "--pf-c-button--after--BorderWidth",
      "value": "1px",
      "values": [
        "--pf-c-button--m-control--after--BorderWidth",
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_button_after_BorderColor": {
      "name": "--pf-c-button--after--BorderColor",
      "value": "#f0f0f0 #f0f0f0 #8a8d90 #f0f0f0",
      "values": [
        "--pf-c-button--m-control--after--BorderTopColor --pf-c-button--m-control--after--BorderRightColor --pf-c-button--m-control--after--BorderBottomColor --pf-c-button--m-control--after--BorderLeftColor",
        "--pf-global--BorderColor--300 --pf-global--BorderColor--300 --pf-global--BorderColor--200 --pf-global--BorderColor--300",
        "$pf-global--BorderColor--300 $pf-global--BorderColor--300 $pf-global--BorderColor--200 $pf-global--BorderColor--300",
        "$pf-color-black-200 $pf-color-black-200 $pf-color-black-500 $pf-color-black-200",
        "#f0f0f0 #f0f0f0 #8a8d90 #f0f0f0"
      ]
    }
  },
  ".pf-c-button.pf-m-control:hover": {
    "c_button_m_control_Color": {
      "name": "--pf-c-button--m-control--Color",
      "value": "#151515",
      "values": [
        "--pf-c-button--m-control--hover--Color",
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_button_m_control_BackgroundColor": {
      "name": "--pf-c-button--m-control--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-c-button--m-control--hover--BackgroundColor",
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_control_after_BorderBottomColor": {
      "name": "--pf-c-button--m-control--after--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-c-button--m-control--hover--after--BorderBottomColor",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-button.pf-m-control:active": {
    "c_button_m_control_Color": {
      "name": "--pf-c-button--m-control--Color",
      "value": "#151515",
      "values": [
        "--pf-c-button--m-control--active--Color",
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_button_m_control_BackgroundColor": {
      "name": "--pf-c-button--m-control--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-c-button--m-control--active--BackgroundColor",
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_control_after_BorderBottomColor": {
      "name": "--pf-c-button--m-control--after--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-c-button--m-control--active--after--BorderBottomColor",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-button.pf-m-control:focus": {
    "c_button_m_control_Color": {
      "name": "--pf-c-button--m-control--Color",
      "value": "#151515",
      "values": [
        "--pf-c-button--m-control--focus--Color",
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_button_m_control_BackgroundColor": {
      "name": "--pf-c-button--m-control--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-c-button--m-control--focus--BackgroundColor",
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_control_after_BorderBottomColor": {
      "name": "--pf-c-button--m-control--after--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-c-button--m-control--focus--after--BorderBottomColor",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-button.pf-m-control.pf-m-expanded": {
    "c_button_m_control_Color": {
      "name": "--pf-c-button--m-control--Color",
      "value": "#151515",
      "values": [
        "--pf-c-button--m-control--m-expanded--Color",
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_button_m_control_BackgroundColor": {
      "name": "--pf-c-button--m-control--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-c-button--m-control--m-expanded--BackgroundColor",
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_control_after_BorderBottomColor": {
      "name": "--pf-c-button--m-control--after--BorderBottomColor",
      "value": "#06c",
      "values": [
        "--pf-c-button--m-control--m-expanded--after--BorderBottomColor",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-button.pf-m-plain": {
    "c_button_disabled_Color": {
      "name": "--pf-c-button--disabled--Color",
      "value": "#d2d2d2",
      "values": [
        "--pf-c-button--m-plain--disabled--Color",
        "--pf-global--disabled-color--200",
        "$pf-global--disabled-color--200",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_button_disabled_BackgroundColor": {
      "name": "--pf-c-button--disabled--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-button--m-plain--disabled--BackgroundColor",
        "transparent"
      ]
    }
  },
  ".pf-c-button.pf-m-plain:hover": {
    "c_button_m_plain_Color": {
      "name": "--pf-c-button--m-plain--Color",
      "value": "#151515",
      "values": [
        "--pf-c-button--m-plain--hover--Color",
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_button_m_plain_BackgroundColor": {
      "name": "--pf-c-button--m-plain--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-button--m-plain--hover--BackgroundColor",
        "transparent"
      ]
    }
  },
  ".pf-c-button.pf-m-plain:active": {
    "c_button_m_plain_Color": {
      "name": "--pf-c-button--m-plain--Color",
      "value": "#151515",
      "values": [
        "--pf-c-button--m-plain--active--Color",
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_button_m_plain_BackgroundColor": {
      "name": "--pf-c-button--m-plain--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-button--m-plain--active--BackgroundColor",
        "transparent"
      ]
    }
  },
  ".pf-c-button.pf-m-plain:focus": {
    "c_button_m_plain_Color": {
      "name": "--pf-c-button--m-plain--Color",
      "value": "#151515",
      "values": [
        "--pf-c-button--m-plain--focus--Color",
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_button_m_plain_BackgroundColor": {
      "name": "--pf-c-button--m-plain--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-button--m-plain--focus--BackgroundColor",
        "transparent"
      ]
    }
  },
  ".pf-c-button:disabled": {
    "c_button_after_BorderColor": {
      "name": "--pf-c-button--after--BorderColor",
      "value": "transparent",
      "values": [
        "--pf-c-button--disabled--after--BorderColor",
        "transparent"
      ]
    }
  },
  ".pf-c-button.pf-m-aria-disabled": {
    "c_button_after_BorderWidth": {
      "name": "--pf-c-button--after--BorderWidth",
      "value": "0"
    },
    "c_button_m_link_m_inline_hover_TextDecoration": {
      "name": "--pf-c-button--m-link--m-inline--hover--TextDecoration",
      "value": "none"
    }
  },
  ".pf-m-overpass-font .pf-c-button": {
    "c_button_FontWeight": {
      "name": "--pf-c-button--FontWeight",
      "value": "700",
      "values": [
        "--pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    }
  }
};
export default c_button;
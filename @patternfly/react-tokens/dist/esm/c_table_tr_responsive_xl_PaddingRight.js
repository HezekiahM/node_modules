export const c_table_tr_responsive_xl_PaddingRight = {
  "name": "--pf-c-table-tr--responsive--xl--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-table-tr--responsive--xl--PaddingRight)"
};
export default c_table_tr_responsive_xl_PaddingRight;
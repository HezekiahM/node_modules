export const c_description_list__term_FontWeight = {
  "name": "--pf-c-description-list__term--FontWeight",
  "value": "700",
  "var": "var(--pf-c-description-list__term--FontWeight)"
};
export default c_description_list__term_FontWeight;
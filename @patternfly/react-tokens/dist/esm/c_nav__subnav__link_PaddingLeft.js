export const c_nav__subnav__link_PaddingLeft = {
  "name": "--pf-c-nav__subnav__link--PaddingLeft",
  "value": "1.5rem",
  "var": "var(--pf-c-nav__subnav__link--PaddingLeft)"
};
export default c_nav__subnav__link_PaddingLeft;
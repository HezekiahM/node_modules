export const c_hint_PaddingBottom = {
  "name": "--pf-c-hint--PaddingBottom",
  "value": "1.5rem",
  "var": "var(--pf-c-hint--PaddingBottom)"
};
export default c_hint_PaddingBottom;
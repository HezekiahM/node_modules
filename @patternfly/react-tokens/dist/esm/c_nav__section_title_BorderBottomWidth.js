export const c_nav__section_title_BorderBottomWidth = {
  "name": "--pf-c-nav__section-title--BorderBottomWidth",
  "value": "1px",
  "var": "var(--pf-c-nav__section-title--BorderBottomWidth)"
};
export default c_nav__section_title_BorderBottomWidth;
export const c_nav__scroll_button_before_BorderLeftWidth = {
  "name": "--pf-c-nav__scroll-button--before--BorderLeftWidth",
  "value": "1px",
  "var": "var(--pf-c-nav__scroll-button--before--BorderLeftWidth)"
};
export default c_nav__scroll_button_before_BorderLeftWidth;
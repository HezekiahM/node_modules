export const c_label__c_button_PaddingBottom = {
  "name": "--pf-c-label__c-button--PaddingBottom",
  "value": "0.25rem",
  "var": "var(--pf-c-label__c-button--PaddingBottom)"
};
export default c_label__c_button_PaddingBottom;
export const c_tabs__link_hover_after_BorderWidth = {
  "name": "--pf-c-tabs__link--hover--after--BorderWidth",
  "value": "3px",
  "var": "var(--pf-c-tabs__link--hover--after--BorderWidth)"
};
export default c_tabs__link_hover_after_BorderWidth;
export const c_toolbar_RowGap = {
  "name": "--pf-c-toolbar--RowGap",
  "value": "1.5rem",
  "var": "var(--pf-c-toolbar--RowGap)"
};
export default c_toolbar_RowGap;
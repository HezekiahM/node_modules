export const c_tabs__link_before_border_color_base = {
  "name": "--pf-c-tabs__link--before--border-color--base",
  "value": "#d2d2d2",
  "var": "var(--pf-c-tabs__link--before--border-color--base)"
};
export default c_tabs__link_before_border_color_base;
export const c_button_m_secondary_hover_BorderColor = {
  "name": "--pf-c-button--m-secondary--hover--BorderColor",
  "value": "#fff",
  "var": "var(--pf-c-button--m-secondary--hover--BorderColor)"
};
export default c_button_m_secondary_hover_BorderColor;
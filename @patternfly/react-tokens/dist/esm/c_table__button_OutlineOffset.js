export const c_table__button_OutlineOffset = {
  "name": "--pf-c-table__button--OutlineOffset",
  "value": "calc(3px * -1)",
  "var": "var(--pf-c-table__button--OutlineOffset)"
};
export default c_table__button_OutlineOffset;
export const c_radio__description_Color = {
  "name": "--pf-c-radio__description--Color",
  "value": "#6a6e73",
  "var": "var(--pf-c-radio__description--Color)"
};
export default c_radio__description_Color;
export const c_select__toggle_BackgroundColor = {
  "name": "--pf-c-select__toggle--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-select__toggle--BackgroundColor)"
};
export default c_select__toggle_BackgroundColor;
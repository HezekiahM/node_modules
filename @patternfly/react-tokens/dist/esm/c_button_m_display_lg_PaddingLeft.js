export const c_button_m_display_lg_PaddingLeft = {
  "name": "--pf-c-button--m-display-lg--PaddingLeft",
  "value": "2rem",
  "var": "var(--pf-c-button--m-display-lg--PaddingLeft)"
};
export default c_button_m_display_lg_PaddingLeft;
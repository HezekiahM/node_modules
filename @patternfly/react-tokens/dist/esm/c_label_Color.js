export const c_label_Color = {
  "name": "--pf-c-label--Color",
  "value": "#151515",
  "var": "var(--pf-c-label--Color)"
};
export default c_label_Color;
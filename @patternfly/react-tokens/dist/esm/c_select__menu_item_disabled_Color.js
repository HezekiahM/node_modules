export const c_select__menu_item_disabled_Color = {
  "name": "--pf-c-select__menu-item--disabled--Color",
  "value": "#6a6e73",
  "var": "var(--pf-c-select__menu-item--disabled--Color)"
};
export default c_select__menu_item_disabled_Color;
export const c_nav__subnav_c_divider_PaddingRight = {
  "name": "--pf-c-nav__subnav--c-divider--PaddingRight",
  "value": "1.5rem",
  "var": "var(--pf-c-nav__subnav--c-divider--PaddingRight)"
};
export default c_nav__subnav_c_divider_PaddingRight;
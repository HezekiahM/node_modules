export const c_content = {
  ".pf-c-content": {
    "c_content_MarginBottom": {
      "name": "--pf-c-content--MarginBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_content_LineHeight": {
      "name": "--pf-c-content--LineHeight",
      "value": "1.5",
      "values": [
        "--pf-global--LineHeight--md",
        "$pf-global--LineHeight--md",
        "1.5"
      ]
    },
    "c_content_FontSize": {
      "name": "--pf-c-content--FontSize",
      "value": "1rem",
      "values": [
        "--pf-global--FontSize--md",
        "$pf-global--FontSize--md",
        "pf-font-prem(16px)",
        "1rem"
      ]
    },
    "c_content_FontWeight": {
      "name": "--pf-c-content--FontWeight",
      "value": "400",
      "values": [
        "--pf-global--FontWeight--normal",
        "$pf-global--FontWeight--normal",
        "400"
      ]
    },
    "c_content_Color": {
      "name": "--pf-c-content--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_content_heading_FontFamily": {
      "name": "--pf-c-content--heading--FontFamily",
      "value": "\"RedHatDisplay\", \"Overpass\", overpass, helvetica, arial, sans-serif",
      "values": [
        "--pf-global--FontFamily--heading--sans-serif",
        "$pf-global--FontFamily--heading--sans-serif",
        "\"RedHatDisplay\", \"Overpass\", overpass, helvetica, arial, sans-serif",
        "\"RedHatDisplay\", \"Overpass\", overpass, helvetica, arial, sans-serif"
      ]
    },
    "c_content_h1_MarginTop": {
      "name": "--pf-c-content--h1--MarginTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_content_h1_MarginBottom": {
      "name": "--pf-c-content--h1--MarginBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_content_h1_LineHeight": {
      "name": "--pf-c-content--h1--LineHeight",
      "value": "1.3",
      "values": [
        "--pf-global--LineHeight--sm",
        "$pf-global--LineHeight--sm",
        "1.3"
      ]
    },
    "c_content_h1_FontSize": {
      "name": "--pf-c-content--h1--FontSize",
      "value": "1.5rem",
      "values": [
        "--pf-global--FontSize--2xl",
        "$pf-global--FontSize--2xl",
        "pf-font-prem(24px)",
        "1.5rem"
      ]
    },
    "c_content_h1_FontWeight": {
      "name": "--pf-c-content--h1--FontWeight",
      "value": "400",
      "values": [
        "--pf-global--FontWeight--normal",
        "$pf-global--FontWeight--normal",
        "400"
      ]
    },
    "c_content_h2_MarginTop": {
      "name": "--pf-c-content--h2--MarginTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_content_h2_MarginBottom": {
      "name": "--pf-c-content--h2--MarginBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_content_h2_LineHeight": {
      "name": "--pf-c-content--h2--LineHeight",
      "value": "1.5",
      "values": [
        "--pf-global--LineHeight--md",
        "$pf-global--LineHeight--md",
        "1.5"
      ]
    },
    "c_content_h2_FontSize": {
      "name": "--pf-c-content--h2--FontSize",
      "value": "1.25rem",
      "values": [
        "--pf-global--FontSize--xl",
        "$pf-global--FontSize--xl",
        "pf-font-prem(20px)",
        "1.25rem"
      ]
    },
    "c_content_h2_FontWeight": {
      "name": "--pf-c-content--h2--FontWeight",
      "value": "400",
      "values": [
        "--pf-global--FontWeight--normal",
        "$pf-global--FontWeight--normal",
        "400"
      ]
    },
    "c_content_h3_MarginTop": {
      "name": "--pf-c-content--h3--MarginTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_content_h3_MarginBottom": {
      "name": "--pf-c-content--h3--MarginBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_content_h3_LineHeight": {
      "name": "--pf-c-content--h3--LineHeight",
      "value": "1.5",
      "values": [
        "--pf-global--LineHeight--md",
        "$pf-global--LineHeight--md",
        "1.5"
      ]
    },
    "c_content_h3_FontSize": {
      "name": "--pf-c-content--h3--FontSize",
      "value": "1.125rem",
      "values": [
        "--pf-global--FontSize--lg",
        "$pf-global--FontSize--lg",
        "pf-font-prem(18px)",
        "1.125rem"
      ]
    },
    "c_content_h3_FontWeight": {
      "name": "--pf-c-content--h3--FontWeight",
      "value": "400",
      "values": [
        "--pf-global--FontWeight--normal",
        "$pf-global--FontWeight--normal",
        "400"
      ]
    },
    "c_content_h4_MarginTop": {
      "name": "--pf-c-content--h4--MarginTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_content_h4_MarginBottom": {
      "name": "--pf-c-content--h4--MarginBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_content_h4_LineHeight": {
      "name": "--pf-c-content--h4--LineHeight",
      "value": "1.5",
      "values": [
        "--pf-global--LineHeight--md",
        "$pf-global--LineHeight--md",
        "1.5"
      ]
    },
    "c_content_h4_FontSize": {
      "name": "--pf-c-content--h4--FontSize",
      "value": "1rem",
      "values": [
        "--pf-global--FontSize--md",
        "$pf-global--FontSize--md",
        "pf-font-prem(16px)",
        "1rem"
      ]
    },
    "c_content_h4_FontWeight": {
      "name": "--pf-c-content--h4--FontWeight",
      "value": "400",
      "values": [
        "--pf-global--FontWeight--normal",
        "$pf-global--FontWeight--normal",
        "400"
      ]
    },
    "c_content_h5_MarginTop": {
      "name": "--pf-c-content--h5--MarginTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_content_h5_MarginBottom": {
      "name": "--pf-c-content--h5--MarginBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_content_h5_LineHeight": {
      "name": "--pf-c-content--h5--LineHeight",
      "value": "1.5",
      "values": [
        "--pf-global--LineHeight--md",
        "$pf-global--LineHeight--md",
        "1.5"
      ]
    },
    "c_content_h5_FontSize": {
      "name": "--pf-c-content--h5--FontSize",
      "value": "1rem",
      "values": [
        "--pf-global--FontSize--md",
        "$pf-global--FontSize--md",
        "pf-font-prem(16px)",
        "1rem"
      ]
    },
    "c_content_h5_FontWeight": {
      "name": "--pf-c-content--h5--FontWeight",
      "value": "400",
      "values": [
        "--pf-global--FontWeight--normal",
        "$pf-global--FontWeight--normal",
        "400"
      ]
    },
    "c_content_h6_MarginTop": {
      "name": "--pf-c-content--h6--MarginTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_content_h6_MarginBottom": {
      "name": "--pf-c-content--h6--MarginBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_content_h6_LineHeight": {
      "name": "--pf-c-content--h6--LineHeight",
      "value": "1.5",
      "values": [
        "--pf-global--LineHeight--md",
        "$pf-global--LineHeight--md",
        "1.5"
      ]
    },
    "c_content_h6_FontSize": {
      "name": "--pf-c-content--h6--FontSize",
      "value": "1rem",
      "values": [
        "--pf-global--FontSize--md",
        "$pf-global--FontSize--md",
        "pf-font-prem(16px)",
        "1rem"
      ]
    },
    "c_content_h6_FontWeight": {
      "name": "--pf-c-content--h6--FontWeight",
      "value": "400",
      "values": [
        "--pf-global--FontWeight--normal",
        "$pf-global--FontWeight--normal",
        "400"
      ]
    },
    "c_content_small_MarginBottom": {
      "name": "--pf-c-content--small--MarginBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_content_small_LineHeight": {
      "name": "--pf-c-content--small--LineHeight",
      "value": "1.5",
      "values": [
        "--pf-global--LineHeight--md",
        "$pf-global--LineHeight--md",
        "1.5"
      ]
    },
    "c_content_small_FontSize": {
      "name": "--pf-c-content--small--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_content_small_Color": {
      "name": "--pf-c-content--small--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--200",
        "$pf-global--Color--200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_content_a_Color": {
      "name": "--pf-c-content--a--Color",
      "value": "#06c",
      "values": [
        "--pf-global--link--Color",
        "$pf-global--link--Color",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_content_a_TextDecoration": {
      "name": "--pf-c-content--a--TextDecoration",
      "value": "none",
      "values": [
        "--pf-global--link--TextDecoration",
        "$pf-global--link--TextDecoration",
        "none"
      ]
    },
    "c_content_a_hover_Color": {
      "name": "--pf-c-content--a--hover--Color",
      "value": "#004080",
      "values": [
        "--pf-global--link--Color--hover",
        "$pf-global--link--Color--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "c_content_a_hover_TextDecoration": {
      "name": "--pf-c-content--a--hover--TextDecoration",
      "value": "underline",
      "values": [
        "--pf-global--link--TextDecoration--hover",
        "$pf-global--link--TextDecoration--hover",
        "underline"
      ]
    },
    "c_content_blockquote_PaddingTop": {
      "name": "--pf-c-content--blockquote--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_content_blockquote_PaddingRight": {
      "name": "--pf-c-content--blockquote--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_content_blockquote_PaddingBottom": {
      "name": "--pf-c-content--blockquote--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_content_blockquote_PaddingLeft": {
      "name": "--pf-c-content--blockquote--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_content_blockquote_Color": {
      "name": "--pf-c-content--blockquote--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--200",
        "$pf-global--Color--200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_content_blockquote_BorderLeftColor": {
      "name": "--pf-c-content--blockquote--BorderLeftColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_content_blockquote_BorderLeftWidth": {
      "name": "--pf-c-content--blockquote--BorderLeftWidth",
      "value": "3px",
      "values": [
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    },
    "c_content_ol_PaddingLeft": {
      "name": "--pf-c-content--ol--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_content_ol_MarginLeft": {
      "name": "--pf-c-content--ol--MarginLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_content_ol_nested_MarginTop": {
      "name": "--pf-c-content--ol--nested--MarginTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_content_ol_nested_MarginLeft": {
      "name": "--pf-c-content--ol--nested--MarginLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_content_ul_PaddingLeft": {
      "name": "--pf-c-content--ul--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_content_ul_MarginLeft": {
      "name": "--pf-c-content--ul--MarginLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_content_ul_nested_MarginTop": {
      "name": "--pf-c-content--ul--nested--MarginTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_content_ul_nested_MarginLeft": {
      "name": "--pf-c-content--ul--nested--MarginLeft",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_content_ul_ListStyle": {
      "name": "--pf-c-content--ul--ListStyle",
      "value": "disc outside",
      "values": [
        "--pf-global--ListStyle",
        "$pf-global--ListStyle",
        "disc outside"
      ]
    },
    "c_content_li_MarginTop": {
      "name": "--pf-c-content--li--MarginTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_content_dl_ColumnGap": {
      "name": "--pf-c-content--dl--ColumnGap",
      "value": "3rem",
      "values": [
        "--pf-global--spacer--2xl",
        "$pf-global--spacer--2xl",
        "pf-size-prem(48px)",
        "3rem"
      ]
    },
    "c_content_dl_RowGap": {
      "name": "--pf-c-content--dl--RowGap",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_content_dt_FontWeight": {
      "name": "--pf-c-content--dt--FontWeight",
      "value": "700",
      "values": [
        "--pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    },
    "c_content_dt_MarginTop": {
      "name": "--pf-c-content--dt--MarginTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_content_dt_sm_MarginTop": {
      "name": "--pf-c-content--dt--sm--MarginTop",
      "value": "0"
    },
    "c_content_hr_Height": {
      "name": "--pf-c-content--hr--Height",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_content_hr_BackgroundColor": {
      "name": "--pf-c-content--hr--BackgroundColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    }
  },
  ".pf-c-content a:hover": {
    "c_content_a_Color": {
      "name": "--pf-c-content--a--Color",
      "value": "#004080",
      "values": [
        "--pf-c-content--a--hover--Color",
        "--pf-global--link--Color--hover",
        "$pf-global--link--Color--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "c_content_a_TextDecoration": {
      "name": "--pf-c-content--a--TextDecoration",
      "value": "underline",
      "values": [
        "--pf-c-content--a--hover--TextDecoration",
        "--pf-global--link--TextDecoration--hover",
        "$pf-global--link--TextDecoration--hover",
        "underline"
      ]
    }
  },
  ".pf-c-content ol ul": {
    "c_content_ul_MarginLeft": {
      "name": "--pf-c-content--ul--MarginLeft",
      "value": "0.5rem",
      "values": [
        "--pf-c-content--ul--nested--MarginLeft",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-c-content ol ol": {
    "c_content_ol_MarginLeft": {
      "name": "--pf-c-content--ol--MarginLeft",
      "value": "0.5rem",
      "values": [
        "--pf-c-content--ol--nested--MarginLeft",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-c-content ul ul": {
    "c_content_ul_MarginLeft": {
      "name": "--pf-c-content--ul--MarginLeft",
      "value": "0.5rem",
      "values": [
        "--pf-c-content--ul--nested--MarginLeft",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-c-content ul ol": {
    "c_content_ol_MarginLeft": {
      "name": "--pf-c-content--ol--MarginLeft",
      "value": "0.5rem",
      "values": [
        "--pf-c-content--ol--nested--MarginLeft",
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-m-overpass-font .pf-c-content": {
    "c_content_h2_LineHeight": {
      "name": "--pf-c-content--h2--LineHeight",
      "value": "1.3",
      "values": [
        "--pf-global--LineHeight--sm",
        "$pf-global--LineHeight--sm",
        "1.3"
      ]
    },
    "c_content_h4_FontWeight": {
      "name": "--pf-c-content--h4--FontWeight",
      "value": "700",
      "values": [
        "--pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    },
    "c_content_h5_FontWeight": {
      "name": "--pf-c-content--h5--FontWeight",
      "value": "700",
      "values": [
        "--pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    },
    "c_content_h6_FontWeight": {
      "name": "--pf-c-content--h6--FontWeight",
      "value": "700",
      "values": [
        "--pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    }
  }
};
export default c_content;
export const c_table__column_help_MarginLeft = {
  "name": "--pf-c-table__column-help--MarginLeft",
  "value": "0.25rem",
  "var": "var(--pf-c-table__column-help--MarginLeft)"
};
export default c_table__column_help_MarginLeft;
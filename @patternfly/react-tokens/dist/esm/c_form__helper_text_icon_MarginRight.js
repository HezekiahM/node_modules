export const c_form__helper_text_icon_MarginRight = {
  "name": "--pf-c-form__helper-text-icon--MarginRight",
  "value": "0.25rem",
  "var": "var(--pf-c-form__helper-text-icon--MarginRight)"
};
export default c_form__helper_text_icon_MarginRight;
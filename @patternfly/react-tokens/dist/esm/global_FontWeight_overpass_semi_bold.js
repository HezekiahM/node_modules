export const global_FontWeight_overpass_semi_bold = {
  "name": "--pf-global--FontWeight--overpass--semi-bold",
  "value": "500",
  "var": "var(--pf-global--FontWeight--overpass--semi-bold)"
};
export default global_FontWeight_overpass_semi_bold;
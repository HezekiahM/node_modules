export const c_toolbar__expandable_content_PaddingTop = {
  "name": "--pf-c-toolbar__expandable-content--PaddingTop",
  "value": "1.5rem",
  "var": "var(--pf-c-toolbar__expandable-content--PaddingTop)"
};
export default c_toolbar__expandable_content_PaddingTop;
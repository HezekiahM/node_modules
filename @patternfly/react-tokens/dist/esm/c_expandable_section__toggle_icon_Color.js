export const c_expandable_section__toggle_icon_Color = {
  "name": "--pf-c-expandable-section__toggle-icon--Color",
  "value": "#151515",
  "var": "var(--pf-c-expandable-section__toggle-icon--Color)"
};
export default c_expandable_section__toggle_icon_Color;
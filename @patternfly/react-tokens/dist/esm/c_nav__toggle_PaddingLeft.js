export const c_nav__toggle_PaddingLeft = {
  "name": "--pf-c-nav__toggle--PaddingLeft",
  "value": "0.5rem",
  "var": "var(--pf-c-nav__toggle--PaddingLeft)"
};
export default c_nav__toggle_PaddingLeft;
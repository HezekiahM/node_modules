export const c_table_m_truncate_cell_MinWidth = {
  "name": "--pf-c-table--m-truncate--cell--MinWidth",
  "value": "calc(5ch + 1rem + 1rem)",
  "var": "var(--pf-c-table--m-truncate--cell--MinWidth)"
};
export default c_table_m_truncate_cell_MinWidth;
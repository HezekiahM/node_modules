export const c_button_m_secondary_focus_BorderColor = {
  "name": "--pf-c-button--m-secondary--focus--BorderColor",
  "value": "#fff",
  "var": "var(--pf-c-button--m-secondary--focus--BorderColor)"
};
export default c_button_m_secondary_focus_BorderColor;
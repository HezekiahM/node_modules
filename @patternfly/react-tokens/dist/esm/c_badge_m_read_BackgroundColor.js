export const c_badge_m_read_BackgroundColor = {
  "name": "--pf-c-badge--m-read--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-badge--m-read--BackgroundColor)"
};
export default c_badge_m_read_BackgroundColor;
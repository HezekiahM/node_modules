export const c_chip_BackgroundColor = {
  "name": "--pf-c-chip--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-chip--BackgroundColor)"
};
export default c_chip_BackgroundColor;
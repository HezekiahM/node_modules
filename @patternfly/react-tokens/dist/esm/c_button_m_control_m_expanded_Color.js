export const c_button_m_control_m_expanded_Color = {
  "name": "--pf-c-button--m-control--m-expanded--Color",
  "value": "#151515",
  "var": "var(--pf-c-button--m-control--m-expanded--Color)"
};
export default c_button_m_control_m_expanded_Color;
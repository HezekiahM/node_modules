export const c_popover__arrow_m_bottom_Rotate = {
  "name": "--pf-c-popover__arrow--m-bottom--Rotate",
  "value": "45deg",
  "var": "var(--pf-c-popover__arrow--m-bottom--Rotate)"
};
export default c_popover__arrow_m_bottom_Rotate;
export const c_button_m_tertiary_after_BorderColor = {
  "name": "--pf-c-button--m-tertiary--after--BorderColor",
  "value": "#151515",
  "var": "var(--pf-c-button--m-tertiary--after--BorderColor)"
};
export default c_button_m_tertiary_after_BorderColor;
export const c_notification_badge = {
  ".pf-c-notification-badge": {
    "c_notification_badge_after_BorderColor": {
      "name": "--pf-c-notification-badge--after--BorderColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_notification_badge_after_BorderRadius": {
      "name": "--pf-c-notification-badge--after--BorderRadius",
      "value": "30em",
      "values": [
        "--pf-global--BorderRadius--lg",
        "$pf-global--BorderRadius--lg",
        "30em"
      ]
    },
    "c_notification_badge_after_BorderWidth": {
      "name": "--pf-c-notification-badge--after--BorderWidth",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_notification_badge_after_Top": {
      "name": "--pf-c-notification-badge--after--Top",
      "value": "0"
    },
    "c_notification_badge_after_Right": {
      "name": "--pf-c-notification-badge--after--Right",
      "value": "0"
    },
    "c_notification_badge_after_Width": {
      "name": "--pf-c-notification-badge--after--Width",
      "value": "calc(0.5rem + 1px + 1px)",
      "values": [
        "calc(--pf-global--spacer--sm + --pf-c-notification-badge--after--BorderWidth + --pf-c-notification-badge--after--BorderWidth)",
        "calc($pf-global--spacer--sm + --pf-global--BorderWidth--sm + --pf-global--BorderWidth--sm)",
        "calc($pf-global--spacer--sm + $pf-global--BorderWidth--sm + $pf-global--BorderWidth--sm)",
        "calc(pf-size-prem(8px) + 1px + 1px)",
        "calc(0.5rem + 1px + 1px)"
      ]
    },
    "c_notification_badge_after_Height": {
      "name": "--pf-c-notification-badge--after--Height",
      "value": "calc(0.5rem + 1px + 1px)",
      "values": [
        "calc(--pf-global--spacer--sm + --pf-c-notification-badge--after--BorderWidth + --pf-c-notification-badge--after--BorderWidth)",
        "calc($pf-global--spacer--sm + --pf-global--BorderWidth--sm + --pf-global--BorderWidth--sm)",
        "calc($pf-global--spacer--sm + $pf-global--BorderWidth--sm + $pf-global--BorderWidth--sm)",
        "calc(pf-size-prem(8px) + 1px + 1px)",
        "calc(0.5rem + 1px + 1px)"
      ]
    },
    "c_notification_badge_after_BackgroundColor": {
      "name": "--pf-c-notification-badge--after--BackgroundColor",
      "value": "transparent"
    },
    "c_notification_badge_after_TranslateX": {
      "name": "--pf-c-notification-badge--after--TranslateX",
      "value": "50%"
    },
    "c_notification_badge_after_TranslateY": {
      "name": "--pf-c-notification-badge--after--TranslateY",
      "value": "calc(1px * -1)",
      "values": [
        "calc(--pf-c-notification-badge--after--BorderWidth * -1)",
        "calc(--pf-global--BorderWidth--sm * -1)",
        "calc($pf-global--BorderWidth--sm * -1)",
        "calc(1px * -1)"
      ]
    },
    "c_notification_badge__i_Width": {
      "name": "--pf-c-notification-badge__i--Width",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_notification_badge__i_Height": {
      "name": "--pf-c-notification-badge__i--Height",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_notification_badge_m_read_after_BackgroundColor": {
      "name": "--pf-c-notification-badge--m-read--after--BackgroundColor",
      "value": "transparent"
    },
    "c_notification_badge_m_read_after_BorderColor": {
      "name": "--pf-c-notification-badge--m-read--after--BorderColor",
      "value": "transparent"
    },
    "c_notification_badge_m_unread_after_BackgroundColor": {
      "name": "--pf-c-notification-badge--m-unread--after--BackgroundColor",
      "value": "#2b9af3",
      "values": [
        "--pf-global--active-color--400",
        "$pf-global--active-color--400",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    }
  },
  ".pf-c-notification-badge.pf-m-unread::after": {
    "c_notification_badge_after_BackgroundColor": {
      "name": "--pf-c-notification-badge--after--BackgroundColor",
      "value": "#2b9af3",
      "values": [
        "--pf-c-notification-badge--m-unread--after--BackgroundColor",
        "--pf-global--active-color--400",
        "$pf-global--active-color--400",
        "$pf-color-blue-300",
        "#2b9af3"
      ]
    }
  },
  ".pf-c-notification-badge.pf-m-read::after": {
    "c_notification_badge_after_BackgroundColor": {
      "name": "--pf-c-notification-badge--after--BackgroundColor",
      "value": "transparent",
      "values": [
        "--pf-c-notification-badge--m-read--after--BackgroundColor",
        "transparent"
      ]
    },
    "c_notification_badge_after_BorderColor": {
      "name": "--pf-c-notification-badge--after--BorderColor",
      "value": "transparent",
      "values": [
        "--pf-c-notification-badge--m-read--after--BorderColor",
        "transparent"
      ]
    }
  }
};
export default c_notification_badge;
export const c_login__header_PaddingRight = {
  "name": "--pf-c-login__header--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-login__header--PaddingRight)"
};
export default c_login__header_PaddingRight;
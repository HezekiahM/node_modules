export const c_page__main_section_BackgroundColor = {
  "name": "--pf-c-page__main-section--BackgroundColor",
  "value": "rgba(#030303, .32)",
  "var": "var(--pf-c-page__main-section--BackgroundColor)"
};
export default c_page__main_section_BackgroundColor;
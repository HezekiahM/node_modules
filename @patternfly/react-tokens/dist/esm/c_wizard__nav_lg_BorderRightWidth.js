export const c_wizard__nav_lg_BorderRightWidth = {
  "name": "--pf-c-wizard__nav--lg--BorderRightWidth",
  "value": "1px",
  "var": "var(--pf-c-wizard__nav--lg--BorderRightWidth)"
};
export default c_wizard__nav_lg_BorderRightWidth;
export const c_table__expandable_row_content_responsive_xl_PaddingLeft = {
  "name": "--pf-c-table__expandable-row-content--responsive--xl--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-table__expandable-row-content--responsive--xl--PaddingLeft)"
};
export default c_table__expandable_row_content_responsive_xl_PaddingLeft;
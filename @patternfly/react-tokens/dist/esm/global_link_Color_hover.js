export const global_link_Color_hover = {
  "name": "--pf-global--link--Color--hover",
  "value": "#73bcf7",
  "var": "var(--pf-global--link--Color--hover)"
};
export default global_link_Color_hover;
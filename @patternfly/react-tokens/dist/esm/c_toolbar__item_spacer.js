export const c_toolbar__item_spacer = {
  "name": "--pf-c-toolbar__item--spacer",
  "value": "1rem",
  "var": "var(--pf-c-toolbar__item--spacer)"
};
export default c_toolbar__item_spacer;
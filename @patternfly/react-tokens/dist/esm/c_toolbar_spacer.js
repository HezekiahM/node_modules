export const c_toolbar_spacer = {
  "name": "--pf-c-toolbar--spacer",
  "value": "0",
  "var": "var(--pf-c-toolbar--spacer)"
};
export default c_toolbar_spacer;
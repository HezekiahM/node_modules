export const c_nav_m_light__link_active_Color = {
  "name": "--pf-c-nav--m-light__link--active--Color",
  "value": "#151515",
  "var": "var(--pf-c-nav--m-light__link--active--Color)"
};
export default c_nav_m_light__link_active_Color;
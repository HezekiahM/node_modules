export const c_alert__title_Color = {
  "name": "--pf-c-alert__title--Color",
  "value": "#002952",
  "var": "var(--pf-c-alert__title--Color)"
};
export default c_alert__title_Color;
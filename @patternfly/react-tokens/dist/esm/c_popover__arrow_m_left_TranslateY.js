export const c_popover__arrow_m_left_TranslateY = {
  "name": "--pf-c-popover__arrow--m-left--TranslateY",
  "value": "-50%",
  "var": "var(--pf-c-popover__arrow--m-left--TranslateY)"
};
export default c_popover__arrow_m_left_TranslateY;
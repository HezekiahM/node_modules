export const c_dropdown__toggle_before_BorderWidth = {
  "name": "--pf-c-dropdown__toggle--before--BorderWidth",
  "value": "1px",
  "var": "var(--pf-c-dropdown__toggle--before--BorderWidth)"
};
export default c_dropdown__toggle_before_BorderWidth;
export const c_page__header_brand_xl_PaddingRight = {
  "name": "--pf-c-page__header-brand--xl--PaddingRight",
  "value": "2rem",
  "var": "var(--pf-c-page__header-brand--xl--PaddingRight)"
};
export default c_page__header_brand_xl_PaddingRight;
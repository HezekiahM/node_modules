export const c_simple_list__title_Color = {
  "name": "--pf-c-simple-list__title--Color",
  "value": "#6a6e73",
  "var": "var(--pf-c-simple-list__title--Color)"
};
export default c_simple_list__title_Color;
export const c_nav_m_tertiary__scroll_button_Color = {
  "name": "--pf-c-nav--m-tertiary__scroll-button--Color",
  "value": "#151515",
  "var": "var(--pf-c-nav--m-tertiary__scroll-button--Color)"
};
export default c_nav_m_tertiary__scroll_button_Color;
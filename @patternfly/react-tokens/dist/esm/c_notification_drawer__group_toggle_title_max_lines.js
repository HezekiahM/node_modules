export const c_notification_drawer__group_toggle_title_max_lines = {
  "name": "--pf-c-notification-drawer__group-toggle-title--max-lines",
  "value": "1",
  "var": "var(--pf-c-notification-drawer__group-toggle-title--max-lines)"
};
export default c_notification_drawer__group_toggle_title_max_lines;
export const c_dropdown__toggle_image_MarginTop = {
  "name": "--pf-c-dropdown__toggle-image--MarginTop",
  "value": "0.25rem",
  "var": "var(--pf-c-dropdown__toggle-image--MarginTop)"
};
export default c_dropdown__toggle_image_MarginTop;
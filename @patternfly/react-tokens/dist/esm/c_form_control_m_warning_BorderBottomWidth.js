export const c_form_control_m_warning_BorderBottomWidth = {
  "name": "--pf-c-form-control--m-warning--BorderBottomWidth",
  "value": "2px",
  "var": "var(--pf-c-form-control--m-warning--BorderBottomWidth)"
};
export default c_form_control_m_warning_BorderBottomWidth;
export const c_banner_PaddingTop = {
  "name": "--pf-c-banner--PaddingTop",
  "value": "0.25rem",
  "var": "var(--pf-c-banner--PaddingTop)"
};
export default c_banner_PaddingTop;
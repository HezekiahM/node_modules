export const c_select__menu_item_m_link_focus_BackgroundColor = {
  "name": "--pf-c-select__menu-item--m-link--focus--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-select__menu-item--m-link--focus--BackgroundColor)"
};
export default c_select__menu_item_m_link_focus_BackgroundColor;
export const c_page__header_nav_xl_BackgroundColor = {
  "name": "--pf-c-page__header-nav--xl--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-page__header-nav--xl--BackgroundColor)"
};
export default c_page__header_nav_xl_BackgroundColor;
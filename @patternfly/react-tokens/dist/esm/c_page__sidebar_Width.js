export const c_page__sidebar_Width = {
  "name": "--pf-c-page__sidebar--Width",
  "value": "18.125rem",
  "var": "var(--pf-c-page__sidebar--Width)"
};
export default c_page__sidebar_Width;
export const c_modal_box = {
  ".pf-c-modal-box": {
    "c_modal_box_BackgroundColor": {
      "name": "--pf-c-modal-box--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_modal_box_BoxShadow": {
      "name": "--pf-c-modal-box--BoxShadow",
      "value": "0 1rem 2rem 0 rgba(3, 3, 3, 0.16), 0 0 0.5rem 0 rgba(3, 3, 3, 0.1)",
      "values": [
        "--pf-global--BoxShadow--xl",
        "$pf-global--BoxShadow--xl",
        "0 pf-size-prem(16px) pf-size-prem(32px) 0 rgba($pf-color-black-1000, .16), 0 0 pf-size-prem(8px) 0 rgba($pf-color-black-1000, .1)",
        "0 pf-size-prem(16px) pf-size-prem(32px) 0 rgba(#030303, .16), 0 0 pf-size-prem(8px) 0 rgba(#030303, .1)",
        "0 1rem 2rem 0 rgba(3, 3, 3, 0.16), 0 0 0.5rem 0 rgba(3, 3, 3, 0.1)"
      ]
    },
    "c_modal_box_ZIndex": {
      "name": "--pf-c-modal-box--ZIndex",
      "value": "500",
      "values": [
        "--pf-global--ZIndex--xl",
        "$pf-global--ZIndex--xl",
        "500"
      ]
    },
    "c_modal_box_Width": {
      "name": "--pf-c-modal-box--Width",
      "value": "100%"
    },
    "c_modal_box_MaxWidth": {
      "name": "--pf-c-modal-box--MaxWidth",
      "value": "calc(100% - 2rem)",
      "values": [
        "calc(100% - --pf-global--spacer--xl)",
        "calc(100% - $pf-global--spacer--xl)",
        "calc(100% - pf-size-prem(32px))",
        "calc(100% - 2rem)"
      ]
    },
    "c_modal_box_m_sm_sm_MaxWidth": {
      "name": "--pf-c-modal-box--m-sm--sm--MaxWidth",
      "value": "35rem"
    },
    "c_modal_box_m_md_Width": {
      "name": "--pf-c-modal-box--m-md--Width",
      "value": "52.5rem"
    },
    "c_modal_box_m_lg_lg_MaxWidth": {
      "name": "--pf-c-modal-box--m-lg--lg--MaxWidth",
      "value": "70rem"
    },
    "c_modal_box_MaxHeight": {
      "name": "--pf-c-modal-box--MaxHeight",
      "value": "calc(100% - 3rem)",
      "values": [
        "calc(100% - --pf-global--spacer--2xl)",
        "calc(100% - $pf-global--spacer--2xl)",
        "calc(100% - pf-size-prem(48px))",
        "calc(100% - 3rem)"
      ]
    },
    "c_modal_box__header_PaddingTop": {
      "name": "--pf-c-modal-box__header--PaddingTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_modal_box__header_PaddingRight": {
      "name": "--pf-c-modal-box__header--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_modal_box__header_PaddingLeft": {
      "name": "--pf-c-modal-box__header--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_modal_box__header_last_child_PaddingBottom": {
      "name": "--pf-c-modal-box__header--last-child--PaddingBottom",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_modal_box__title_LineHeight": {
      "name": "--pf-c-modal-box__title--LineHeight",
      "value": "1.3",
      "values": [
        "--pf-global--LineHeight--sm",
        "$pf-global--LineHeight--sm",
        "1.3"
      ]
    },
    "c_modal_box__title_FontFamily": {
      "name": "--pf-c-modal-box__title--FontFamily",
      "value": "\"RedHatDisplay\", \"Overpass\", overpass, helvetica, arial, sans-serif",
      "values": [
        "--pf-global--FontFamily--heading--sans-serif",
        "$pf-global--FontFamily--heading--sans-serif",
        "\"RedHatDisplay\", \"Overpass\", overpass, helvetica, arial, sans-serif",
        "\"RedHatDisplay\", \"Overpass\", overpass, helvetica, arial, sans-serif"
      ]
    },
    "c_modal_box__title_FontSize": {
      "name": "--pf-c-modal-box__title--FontSize",
      "value": "1.5rem",
      "values": [
        "--pf-global--FontSize--2xl",
        "$pf-global--FontSize--2xl",
        "pf-font-prem(24px)",
        "1.5rem"
      ]
    },
    "c_modal_box__description_PaddingTop": {
      "name": "--pf-c-modal-box__description--PaddingTop",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_modal_box__body_MinHeight": {
      "name": "--pf-c-modal-box__body--MinHeight",
      "value": "calc(1rem * 1.5)",
      "values": [
        "calc(--pf-global--FontSize--md * --pf-global--LineHeight--md)",
        "calc($pf-global--FontSize--md * $pf-global--LineHeight--md)",
        "calc(pf-font-prem(16px) * 1.5)",
        "calc(1rem * 1.5)"
      ]
    },
    "c_modal_box__body_PaddingTop": {
      "name": "--pf-c-modal-box__body--PaddingTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_modal_box__body_PaddingRight": {
      "name": "--pf-c-modal-box__body--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_modal_box__body_PaddingLeft": {
      "name": "--pf-c-modal-box__body--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_modal_box__body_last_child_PaddingBottom": {
      "name": "--pf-c-modal-box__body--last-child--PaddingBottom",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_modal_box__header_body_PaddingTop": {
      "name": "--pf-c-modal-box__header--body--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_modal_box_c_button_Top": {
      "name": "--pf-c-modal-box--c-button--Top",
      "value": "calc(1.5rem - 0.375rem + 0.0625rem)",
      "values": [
        "calc(--pf-global--spacer--lg - --pf-global--spacer--form-element + 0.0625rem)",
        "calc($pf-global--spacer--lg - $pf-global--spacer--form-element + 0.0625rem)",
        "calc(pf-size-prem(24px) - pf-size-prem(6px) + 0.0625rem)",
        "calc(1.5rem - 0.375rem + 0.0625rem)"
      ]
    },
    "c_modal_box_c_button_Right": {
      "name": "--pf-c-modal-box--c-button--Right",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_modal_box_c_button_sibling_MarginRight": {
      "name": "--pf-c-modal-box--c-button--sibling--MarginRight",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_modal_box__footer_PaddingTop": {
      "name": "--pf-c-modal-box__footer--PaddingTop",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_modal_box__footer_PaddingRight": {
      "name": "--pf-c-modal-box__footer--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_modal_box__footer_PaddingBottom": {
      "name": "--pf-c-modal-box__footer--PaddingBottom",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_modal_box__footer_PaddingLeft": {
      "name": "--pf-c-modal-box__footer--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_modal_box__footer_c_button_MarginRight": {
      "name": "--pf-c-modal-box__footer--c-button--MarginRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_modal_box__footer_c_button_sm_MarginRight": {
      "name": "--pf-c-modal-box__footer--c-button--sm--MarginRight",
      "value": "calc(1rem / 2)",
      "values": [
        "calc(--pf-c-modal-box__footer--c-button--MarginRight / 2)",
        "calc(--pf-global--spacer--md / 2)",
        "calc($pf-global--spacer--md / 2)",
        "calc(pf-size-prem(16px) / 2)",
        "calc(1rem / 2)"
      ]
    }
  },
  ".pf-c-modal-box.pf-m-sm": {
    "c_modal_box_Width": {
      "name": "--pf-c-modal-box--Width",
      "value": "35rem",
      "values": [
        "--pf-c-modal-box--m-sm--sm--MaxWidth",
        "35rem"
      ]
    }
  },
  ".pf-c-modal-box.pf-m-md": {
    "c_modal_box_Width": {
      "name": "--pf-c-modal-box--Width",
      "value": "52.5rem",
      "values": [
        "--pf-c-modal-box--m-md--Width",
        "52.5rem"
      ]
    }
  },
  ".pf-c-modal-box.pf-m-lg": {
    "c_modal_box_Width": {
      "name": "--pf-c-modal-box--Width",
      "value": "70rem",
      "values": [
        "--pf-c-modal-box--m-lg--lg--MaxWidth",
        "70rem"
      ]
    }
  },
  ".pf-c-modal-box__header + .pf-c-modal-box__body": {
    "c_modal_box__body_PaddingTop": {
      "name": "--pf-c-modal-box__body--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-c-modal-box__header--body--PaddingTop",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    }
  }
};
export default c_modal_box;
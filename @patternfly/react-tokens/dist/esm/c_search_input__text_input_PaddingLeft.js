export const c_search_input__text_input_PaddingLeft = {
  "name": "--pf-c-search-input__text-input--PaddingLeft",
  "value": "2rem",
  "var": "var(--pf-c-search-input__text-input--PaddingLeft)"
};
export default c_search_input__text_input_PaddingLeft;
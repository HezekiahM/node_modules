export const c_chip_PaddingRight = {
  "name": "--pf-c-chip--PaddingRight",
  "value": "0.5rem",
  "var": "var(--pf-c-chip--PaddingRight)"
};
export default c_chip_PaddingRight;
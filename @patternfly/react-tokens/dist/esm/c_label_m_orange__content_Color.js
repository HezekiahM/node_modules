export const c_label_m_orange__content_Color = {
  "name": "--pf-c-label--m-orange__content--Color",
  "value": "#3d2c00",
  "var": "var(--pf-c-label--m-orange__content--Color)"
};
export default c_label_m_orange__content_Color;
export const c_select__menu_search_PaddingTop = {
  "name": "--pf-c-select__menu-search--PaddingTop",
  "value": "0.5rem",
  "var": "var(--pf-c-select__menu-search--PaddingTop)"
};
export default c_select__menu_search_PaddingTop;
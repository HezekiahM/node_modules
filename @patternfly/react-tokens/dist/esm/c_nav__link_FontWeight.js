export const c_nav__link_FontWeight = {
  "name": "--pf-c-nav__link--FontWeight",
  "value": "400",
  "var": "var(--pf-c-nav__link--FontWeight)"
};
export default c_nav__link_FontWeight;
export const c_data_list__item_m_selectable_hover_item_BorderTopWidth = {
  "name": "--pf-c-data-list__item--m-selectable--hover--item--BorderTopWidth",
  "value": "0.5rem",
  "var": "var(--pf-c-data-list__item--m-selectable--hover--item--BorderTopWidth)"
};
export default c_data_list__item_m_selectable_hover_item_BorderTopWidth;
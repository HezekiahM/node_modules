export const c_search_input__utilities_child_MarginLeft = {
  "name": "--pf-c-search-input__utilities--child--MarginLeft",
  "value": "0.25rem",
  "var": "var(--pf-c-search-input__utilities--child--MarginLeft)"
};
export default c_search_input__utilities_child_MarginLeft;
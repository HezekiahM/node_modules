export const c_table__button_hover_Color = {
  "name": "--pf-c-table__button--hover--Color",
  "value": "#004080",
  "var": "var(--pf-c-table__button--hover--Color)"
};
export default c_table__button_hover_Color;
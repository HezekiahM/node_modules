export const c_banner_m_success_BackgroundColor = {
  "name": "--pf-c-banner--m-success--BackgroundColor",
  "value": "#3e8635",
  "var": "var(--pf-c-banner--m-success--BackgroundColor)"
};
export default c_banner_m_success_BackgroundColor;
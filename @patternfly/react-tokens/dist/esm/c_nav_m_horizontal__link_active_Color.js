export const c_nav_m_horizontal__link_active_Color = {
  "name": "--pf-c-nav--m-horizontal__link--active--Color",
  "value": "#2b9af3",
  "var": "var(--pf-c-nav--m-horizontal__link--active--Color)"
};
export default c_nav_m_horizontal__link_active_Color;
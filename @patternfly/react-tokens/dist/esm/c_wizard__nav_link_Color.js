export const c_wizard__nav_link_Color = {
  "name": "--pf-c-wizard__nav-link--Color",
  "value": "#6a6e73",
  "var": "var(--pf-c-wizard__nav-link--Color)"
};
export default c_wizard__nav_link_Color;
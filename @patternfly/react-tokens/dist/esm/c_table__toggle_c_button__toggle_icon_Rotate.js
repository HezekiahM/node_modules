export const c_table__toggle_c_button__toggle_icon_Rotate = {
  "name": "--pf-c-table__toggle--c-button__toggle-icon--Rotate",
  "value": "270deg",
  "var": "var(--pf-c-table__toggle--c-button__toggle-icon--Rotate)"
};
export default c_table__toggle_c_button__toggle_icon_Rotate;
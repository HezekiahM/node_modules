export const global_palette_blue_700 = {
  "name": "--pf-global--palette--blue-700",
  "value": "#001223",
  "var": "var(--pf-global--palette--blue-700)"
};
export default global_palette_blue_700;
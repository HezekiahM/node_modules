export const c_nav__link_Color = {
  "name": "--pf-c-nav__link--Color",
  "value": "#151515",
  "var": "var(--pf-c-nav__link--Color)"
};
export default c_nav__link_Color;
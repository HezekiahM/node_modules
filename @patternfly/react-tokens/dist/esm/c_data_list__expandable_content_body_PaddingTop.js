export const c_data_list__expandable_content_body_PaddingTop = {
  "name": "--pf-c-data-list__expandable-content-body--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-data-list__expandable-content-body--PaddingTop)"
};
export default c_data_list__expandable_content_body_PaddingTop;
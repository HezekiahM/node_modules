export const c_tooltip__content_PaddingBottom = {
  "name": "--pf-c-tooltip__content--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-tooltip__content--PaddingBottom)"
};
export default c_tooltip__content_PaddingBottom;
export const c_tabs__link_OutlineOffset = {
  "name": "--pf-c-tabs__link--OutlineOffset",
  "value": "calc(-1 * 0.375rem)",
  "var": "var(--pf-c-tabs__link--OutlineOffset)"
};
export default c_tabs__link_OutlineOffset;
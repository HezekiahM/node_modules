export const c_nav__link_focus_BackgroundColor = {
  "name": "--pf-c-nav__link--focus--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-nav__link--focus--BackgroundColor)"
};
export default c_nav__link_focus_BackgroundColor;
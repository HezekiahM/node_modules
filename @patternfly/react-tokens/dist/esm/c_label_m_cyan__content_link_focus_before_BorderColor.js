export const c_label_m_cyan__content_link_focus_before_BorderColor = {
  "name": "--pf-c-label--m-cyan__content--link--focus--before--BorderColor",
  "value": "#009596",
  "var": "var(--pf-c-label--m-cyan__content--link--focus--before--BorderColor)"
};
export default c_label_m_cyan__content_link_focus_before_BorderColor;
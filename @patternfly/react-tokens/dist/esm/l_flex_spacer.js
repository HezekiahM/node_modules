export const l_flex_spacer = {
  "name": "--pf-l-flex--spacer",
  "value": "5rem",
  "var": "var(--pf-l-flex--spacer)"
};
export default l_flex_spacer;
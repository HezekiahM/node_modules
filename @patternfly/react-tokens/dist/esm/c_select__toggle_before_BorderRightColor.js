export const c_select__toggle_before_BorderRightColor = {
  "name": "--pf-c-select__toggle--before--BorderRightColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-select__toggle--before--BorderRightColor)"
};
export default c_select__toggle_before_BorderRightColor;
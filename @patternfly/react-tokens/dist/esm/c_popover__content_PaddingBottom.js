export const c_popover__content_PaddingBottom = {
  "name": "--pf-c-popover__content--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-popover__content--PaddingBottom)"
};
export default c_popover__content_PaddingBottom;
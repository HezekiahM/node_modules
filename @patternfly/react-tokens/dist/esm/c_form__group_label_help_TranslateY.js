export const c_form__group_label_help_TranslateY = {
  "name": "--pf-c-form__group-label-help--TranslateY",
  "value": "0.125rem",
  "var": "var(--pf-c-form__group-label-help--TranslateY)"
};
export default c_form__group_label_help_TranslateY;
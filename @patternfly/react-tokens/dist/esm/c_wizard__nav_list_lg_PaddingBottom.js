export const c_wizard__nav_list_lg_PaddingBottom = {
  "name": "--pf-c-wizard__nav-list--lg--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-wizard__nav-list--lg--PaddingBottom)"
};
export default c_wizard__nav_list_lg_PaddingBottom;
export const c_table_thead_cell_FontSize = {
  "name": "--pf-c-table--thead--cell--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-table--thead--cell--FontSize)"
};
export default c_table_thead_cell_FontSize;
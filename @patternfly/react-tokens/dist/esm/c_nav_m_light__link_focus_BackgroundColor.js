export const c_nav_m_light__link_focus_BackgroundColor = {
  "name": "--pf-c-nav--m-light__link--focus--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-nav--m-light__link--focus--BackgroundColor)"
};
export default c_nav_m_light__link_focus_BackgroundColor;
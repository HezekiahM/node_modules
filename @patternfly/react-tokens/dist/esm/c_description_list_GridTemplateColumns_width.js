export const c_description_list_GridTemplateColumns_width = {
  "name": "--pf-c-description-list--GridTemplateColumns--width",
  "value": "minmax(8ch, max-content)",
  "var": "var(--pf-c-description-list--GridTemplateColumns--width)"
};
export default c_description_list_GridTemplateColumns_width;
export const c_tabs__link_before_BorderRightColor = {
  "name": "--pf-c-tabs__link--before--BorderRightColor",
  "value": "#fff",
  "var": "var(--pf-c-tabs__link--before--BorderRightColor)"
};
export default c_tabs__link_before_BorderRightColor;
export const c_label_m_purple__content_link_hover_before_BorderColor = {
  "name": "--pf-c-label--m-purple__content--link--hover--before--BorderColor",
  "value": "#6753ac",
  "var": "var(--pf-c-label--m-purple__content--link--hover--before--BorderColor)"
};
export default c_label_m_purple__content_link_hover_before_BorderColor;
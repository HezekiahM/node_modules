export const global_BorderColor_300 = {
  "name": "--pf-global--BorderColor--300",
  "value": "#f0f0f0",
  "var": "var(--pf-global--BorderColor--300)"
};
export default global_BorderColor_300;
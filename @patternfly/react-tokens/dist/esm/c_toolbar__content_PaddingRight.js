export const c_toolbar__content_PaddingRight = {
  "name": "--pf-c-toolbar__content--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-toolbar__content--PaddingRight)"
};
export default c_toolbar__content_PaddingRight;
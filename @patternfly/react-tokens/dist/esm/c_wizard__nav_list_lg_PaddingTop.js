export const c_wizard__nav_list_lg_PaddingTop = {
  "name": "--pf-c-wizard__nav-list--lg--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-wizard__nav-list--lg--PaddingTop)"
};
export default c_wizard__nav_list_lg_PaddingTop;
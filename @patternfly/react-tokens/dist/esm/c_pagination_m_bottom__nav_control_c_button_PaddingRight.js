export const c_pagination_m_bottom__nav_control_c_button_PaddingRight = {
  "name": "--pf-c-pagination--m-bottom__nav-control--c-button--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-pagination--m-bottom__nav-control--c-button--PaddingRight)"
};
export default c_pagination_m_bottom__nav_control_c_button_PaddingRight;
export const c_empty_state__secondary_child_MarginRight = {
  "name": "--pf-c-empty-state__secondary--child--MarginRight",
  "value": "calc(0.25rem / 2)",
  "var": "var(--pf-c-empty-state__secondary--child--MarginRight)"
};
export default c_empty_state__secondary_child_MarginRight;
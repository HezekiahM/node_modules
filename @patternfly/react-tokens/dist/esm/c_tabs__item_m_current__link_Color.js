export const c_tabs__item_m_current__link_Color = {
  "name": "--pf-c-tabs__item--m-current__link--Color",
  "value": "#151515",
  "var": "var(--pf-c-tabs__item--m-current__link--Color)"
};
export default c_tabs__item_m_current__link_Color;
export const c_nav__subnav__link_PaddingBottom = {
  "name": "--pf-c-nav__subnav__link--PaddingBottom",
  "value": "0.5rem",
  "var": "var(--pf-c-nav__subnav__link--PaddingBottom)"
};
export default c_nav__subnav__link_PaddingBottom;
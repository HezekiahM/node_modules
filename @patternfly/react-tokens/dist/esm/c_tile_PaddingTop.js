export const c_tile_PaddingTop = {
  "name": "--pf-c-tile--PaddingTop",
  "value": "1.5rem",
  "var": "var(--pf-c-tile--PaddingTop)"
};
export default c_tile_PaddingTop;
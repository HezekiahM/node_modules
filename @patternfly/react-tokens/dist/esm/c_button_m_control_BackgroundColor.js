export const c_button_m_control_BackgroundColor = {
  "name": "--pf-c-button--m-control--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-button--m-control--BackgroundColor)"
};
export default c_button_m_control_BackgroundColor;
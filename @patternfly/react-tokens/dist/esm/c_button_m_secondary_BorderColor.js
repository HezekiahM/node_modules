export const c_button_m_secondary_BorderColor = {
  "name": "--pf-c-button--m-secondary--BorderColor",
  "value": "#fff",
  "var": "var(--pf-c-button--m-secondary--BorderColor)"
};
export default c_button_m_secondary_BorderColor;
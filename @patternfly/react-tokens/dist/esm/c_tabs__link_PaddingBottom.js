export const c_tabs__link_PaddingBottom = {
  "name": "--pf-c-tabs__link--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-tabs__link--PaddingBottom)"
};
export default c_tabs__link_PaddingBottom;
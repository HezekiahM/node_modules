export const c_banner_m_sticky_BoxShadow = {
  "name": "--pf-c-banner--m-sticky--BoxShadow",
  "value": "0 0.5rem 0.5rem -0.375rem rgba(3, 3, 3, 0.18)",
  "var": "var(--pf-c-banner--m-sticky--BoxShadow)"
};
export default c_banner_m_sticky_BoxShadow;
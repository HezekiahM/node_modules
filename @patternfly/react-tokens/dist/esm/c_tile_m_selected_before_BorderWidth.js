export const c_tile_m_selected_before_BorderWidth = {
  "name": "--pf-c-tile--m-selected--before--BorderWidth",
  "value": "2px",
  "var": "var(--pf-c-tile--m-selected--before--BorderWidth)"
};
export default c_tile_m_selected_before_BorderWidth;
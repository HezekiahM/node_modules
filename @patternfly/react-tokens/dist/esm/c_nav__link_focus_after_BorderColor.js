export const c_nav__link_focus_after_BorderColor = {
  "name": "--pf-c-nav__link--focus--after--BorderColor",
  "value": "transparent",
  "var": "var(--pf-c-nav__link--focus--after--BorderColor)"
};
export default c_nav__link_focus_after_BorderColor;
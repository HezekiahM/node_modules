export const c_hint__footer_child_MarginRight = {
  "name": "--pf-c-hint__footer--child--MarginRight",
  "value": "1rem",
  "var": "var(--pf-c-hint__footer--child--MarginRight)"
};
export default c_hint__footer_child_MarginRight;
export const c_wizard__close_xl_Right = {
  "name": "--pf-c-wizard__close--xl--Right",
  "value": "1.5rem",
  "var": "var(--pf-c-wizard__close--xl--Right)"
};
export default c_wizard__close_xl_Right;
export const c_form_control_Color = {
  "name": "--pf-c-form-control--Color",
  "value": "#6a6e73",
  "var": "var(--pf-c-form-control--Color)"
};
export default c_form_control_Color;
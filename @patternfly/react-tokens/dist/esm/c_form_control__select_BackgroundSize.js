export const c_form_control__select_BackgroundSize = {
  "name": "--pf-c-form-control__select--BackgroundSize",
  "value": ".625em",
  "var": "var(--pf-c-form-control__select--BackgroundSize)"
};
export default c_form_control__select_BackgroundSize;
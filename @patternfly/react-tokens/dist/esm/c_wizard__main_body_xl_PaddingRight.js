export const c_wizard__main_body_xl_PaddingRight = {
  "name": "--pf-c-wizard__main-body--xl--PaddingRight",
  "value": "1.5rem",
  "var": "var(--pf-c-wizard__main-body--xl--PaddingRight)"
};
export default c_wizard__main_body_xl_PaddingRight;
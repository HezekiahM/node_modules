export const c_card__title_FontSize = {
  "name": "--pf-c-card__title--FontSize",
  "value": "1rem",
  "var": "var(--pf-c-card__title--FontSize)"
};
export default c_card__title_FontSize;
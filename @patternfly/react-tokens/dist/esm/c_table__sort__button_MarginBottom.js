export const c_table__sort__button_MarginBottom = {
  "name": "--pf-c-table__sort__button--MarginBottom",
  "value": "calc(0.375rem * -1)",
  "var": "var(--pf-c-table__sort__button--MarginBottom)"
};
export default c_table__sort__button_MarginBottom;
export const c_data_list__cell_m_icon_cell_PaddingTop = {
  "name": "--pf-c-data-list__cell--m-icon--cell--PaddingTop",
  "value": "1.5rem",
  "var": "var(--pf-c-data-list__cell--m-icon--cell--PaddingTop)"
};
export default c_data_list__cell_m_icon_cell_PaddingTop;
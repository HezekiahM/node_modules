export const c_alert_BorderTopWidth = {
  "name": "--pf-c-alert--BorderTopWidth",
  "value": "2px",
  "var": "var(--pf-c-alert--BorderTopWidth)"
};
export default c_alert_BorderTopWidth;
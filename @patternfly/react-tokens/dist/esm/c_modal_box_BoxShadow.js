export const c_modal_box_BoxShadow = {
  "name": "--pf-c-modal-box--BoxShadow",
  "value": "0 1rem 2rem 0 rgba(3, 3, 3, 0.16), 0 0 0.5rem 0 rgba(3, 3, 3, 0.1)",
  "var": "var(--pf-c-modal-box--BoxShadow)"
};
export default c_modal_box_BoxShadow;
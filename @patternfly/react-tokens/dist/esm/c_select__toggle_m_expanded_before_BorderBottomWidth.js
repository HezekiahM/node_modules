export const c_select__toggle_m_expanded_before_BorderBottomWidth = {
  "name": "--pf-c-select__toggle--m-expanded--before--BorderBottomWidth",
  "value": "2px",
  "var": "var(--pf-c-select__toggle--m-expanded--before--BorderBottomWidth)"
};
export default c_select__toggle_m_expanded_before_BorderBottomWidth;
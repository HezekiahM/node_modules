export const global_spacer_4xl = {
  "name": "--pf-global--spacer--4xl",
  "value": "5rem",
  "var": "var(--pf-global--spacer--4xl)"
};
export default global_spacer_4xl;
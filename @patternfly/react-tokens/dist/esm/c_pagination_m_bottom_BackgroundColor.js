export const c_pagination_m_bottom_BackgroundColor = {
  "name": "--pf-c-pagination--m-bottom--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-pagination--m-bottom--BackgroundColor)"
};
export default c_pagination_m_bottom_BackgroundColor;
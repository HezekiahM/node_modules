export const c_toolbar__group_m_button_group_space_items = {
  "name": "--pf-c-toolbar__group--m-button-group--space-items",
  "value": "0.5rem",
  "var": "var(--pf-c-toolbar__group--m-button-group--space-items)"
};
export default c_toolbar__group_m_button_group_space_items;
export const c_tabs_m_vertical__link_PaddingBottom = {
  "name": "--pf-c-tabs--m-vertical__link--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-tabs--m-vertical__link--PaddingBottom)"
};
export default c_tabs_m_vertical__link_PaddingBottom;
export const c_tabs_before_BorderRightWidth = {
  "name": "--pf-c-tabs--before--BorderRightWidth",
  "value": "1px",
  "var": "var(--pf-c-tabs--before--BorderRightWidth)"
};
export default c_tabs_before_BorderRightWidth;
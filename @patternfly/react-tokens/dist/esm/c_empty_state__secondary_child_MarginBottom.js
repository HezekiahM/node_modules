export const c_empty_state__secondary_child_MarginBottom = {
  "name": "--pf-c-empty-state__secondary--child--MarginBottom",
  "value": "0.25rem",
  "var": "var(--pf-c-empty-state__secondary--child--MarginBottom)"
};
export default c_empty_state__secondary_child_MarginBottom;
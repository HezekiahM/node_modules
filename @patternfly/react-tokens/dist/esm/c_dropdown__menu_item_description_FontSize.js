export const c_dropdown__menu_item_description_FontSize = {
  "name": "--pf-c-dropdown__menu-item-description--FontSize",
  "value": "0.75rem",
  "var": "var(--pf-c-dropdown__menu-item-description--FontSize)"
};
export default c_dropdown__menu_item_description_FontSize;
export const c_label_BackgroundColor = {
  "name": "--pf-c-label--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-label--BackgroundColor)"
};
export default c_label_BackgroundColor;
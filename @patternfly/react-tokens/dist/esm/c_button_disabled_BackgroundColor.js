export const c_button_disabled_BackgroundColor = {
  "name": "--pf-c-button--disabled--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-button--disabled--BackgroundColor)"
};
export default c_button_disabled_BackgroundColor;
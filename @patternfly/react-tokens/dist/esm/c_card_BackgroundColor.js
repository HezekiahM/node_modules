export const c_card_BackgroundColor = {
  "name": "--pf-c-card--BackgroundColor",
  "value": "rgba(#030303, .32)",
  "var": "var(--pf-c-card--BackgroundColor)"
};
export default c_card_BackgroundColor;
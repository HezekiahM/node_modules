export const global_palette_cyan_50 = {
  "name": "--pf-global--palette--cyan-50",
  "value": "#f2f9f9",
  "var": "var(--pf-global--palette--cyan-50)"
};
export default global_palette_cyan_50;
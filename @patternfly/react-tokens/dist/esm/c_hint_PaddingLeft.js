export const c_hint_PaddingLeft = {
  "name": "--pf-c-hint--PaddingLeft",
  "value": "1.5rem",
  "var": "var(--pf-c-hint--PaddingLeft)"
};
export default c_hint_PaddingLeft;
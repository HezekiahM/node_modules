export const c_table__button_BackgroundColor = {
  "name": "--pf-c-table__button--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-table__button--BackgroundColor)"
};
export default c_table__button_BackgroundColor;
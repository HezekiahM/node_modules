export const c_alert_m_success_BorderTopColor = {
  "name": "--pf-c-alert--m-success--BorderTopColor",
  "value": "#3e8635",
  "var": "var(--pf-c-alert--m-success--BorderTopColor)"
};
export default c_alert_m_success_BorderTopColor;
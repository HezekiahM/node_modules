export const c_app_launcher_m_top__menu_TranslateY = {
  "name": "--pf-c-app-launcher--m-top__menu--TranslateY",
  "value": "calc(-100% - 0.25rem)",
  "var": "var(--pf-c-app-launcher--m-top__menu--TranslateY)"
};
export default c_app_launcher_m_top__menu_TranslateY;
export const c_nav_m_light__link_m_current_BackgroundColor = {
  "name": "--pf-c-nav--m-light__link--m-current--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-nav--m-light__link--m-current--BackgroundColor)"
};
export default c_nav_m_light__link_m_current_BackgroundColor;
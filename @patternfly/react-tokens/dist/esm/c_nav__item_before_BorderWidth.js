export const c_nav__item_before_BorderWidth = {
  "name": "--pf-c-nav__item--before--BorderWidth",
  "value": "1px",
  "var": "var(--pf-c-nav__item--before--BorderWidth)"
};
export default c_nav__item_before_BorderWidth;
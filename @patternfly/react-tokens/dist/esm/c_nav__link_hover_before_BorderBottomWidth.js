export const c_nav__link_hover_before_BorderBottomWidth = {
  "name": "--pf-c-nav__link--hover--before--BorderBottomWidth",
  "value": "3px",
  "var": "var(--pf-c-nav__link--hover--before--BorderBottomWidth)"
};
export default c_nav__link_hover_before_BorderBottomWidth;
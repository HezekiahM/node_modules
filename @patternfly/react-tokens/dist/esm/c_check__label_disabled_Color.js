export const c_check__label_disabled_Color = {
  "name": "--pf-c-check__label--disabled--Color",
  "value": "#6a6e73",
  "var": "var(--pf-c-check__label--disabled--Color)"
};
export default c_check__label_disabled_Color;
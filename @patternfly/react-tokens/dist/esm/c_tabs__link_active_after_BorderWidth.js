export const c_tabs__link_active_after_BorderWidth = {
  "name": "--pf-c-tabs__link--active--after--BorderWidth",
  "value": "3px",
  "var": "var(--pf-c-tabs__link--active--after--BorderWidth)"
};
export default c_tabs__link_active_after_BorderWidth;
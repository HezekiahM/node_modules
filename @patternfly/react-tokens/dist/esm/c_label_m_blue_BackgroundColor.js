export const c_label_m_blue_BackgroundColor = {
  "name": "--pf-c-label--m-blue--BackgroundColor",
  "value": "#e7f1fa",
  "var": "var(--pf-c-label--m-blue--BackgroundColor)"
};
export default c_label_m_blue_BackgroundColor;
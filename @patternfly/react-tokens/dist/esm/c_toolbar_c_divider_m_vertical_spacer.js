export const c_toolbar_c_divider_m_vertical_spacer = {
  "name": "--pf-c-toolbar--c-divider--m-vertical--spacer",
  "value": "1rem",
  "var": "var(--pf-c-toolbar--c-divider--m-vertical--spacer)"
};
export default c_toolbar_c_divider_m_vertical_spacer;
export const c_popover__arrow_m_left_TranslateX = {
  "name": "--pf-c-popover__arrow--m-left--TranslateX",
  "value": "50%",
  "var": "var(--pf-c-popover__arrow--m-left--TranslateX)"
};
export default c_popover__arrow_m_left_TranslateX;
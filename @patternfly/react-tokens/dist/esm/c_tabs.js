export const c_tabs = {
  ".pf-c-tabs": {
    "c_tabs_inset": {
      "name": "--pf-c-tabs--inset",
      "value": "0"
    },
    "c_tabs_before_BorderColor": {
      "name": "--pf-c-tabs--before--BorderColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_tabs_before_border_width_base": {
      "name": "--pf-c-tabs--before--border-width--base",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_tabs_before_BorderTopWidth": {
      "name": "--pf-c-tabs--before--BorderTopWidth",
      "value": "0"
    },
    "c_tabs_before_BorderRightWidth": {
      "name": "--pf-c-tabs--before--BorderRightWidth",
      "value": "0"
    },
    "c_tabs_before_BorderBottomWidth": {
      "name": "--pf-c-tabs--before--BorderBottomWidth",
      "value": "1px",
      "values": [
        "--pf-c-tabs--before--border-width--base",
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_tabs_before_BorderLeftWidth": {
      "name": "--pf-c-tabs--before--BorderLeftWidth",
      "value": "0"
    },
    "c_tabs_m_vertical_inset": {
      "name": "--pf-c-tabs--m-vertical--inset",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_tabs_m_vertical_MaxWidth": {
      "name": "--pf-c-tabs--m-vertical--MaxWidth",
      "value": "15.625rem"
    },
    "c_tabs_m_vertical_m_box_inset": {
      "name": "--pf-c-tabs--m-vertical--m-box--inset",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_tabs_m_box__item_m_current_first_child__link_before_BorderLeftWidth": {
      "name": "--pf-c-tabs--m-box__item--m-current--first-child__link--before--BorderLeftWidth",
      "value": "1px",
      "values": [
        "--pf-c-tabs__link--before--border-width--base",
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_tabs_m_box__item_m_current_last_child__link_before_BorderRightWidth": {
      "name": "--pf-c-tabs--m-box__item--m-current--last-child__link--before--BorderRightWidth",
      "value": "1px",
      "values": [
        "--pf-c-tabs--before--border-width--base",
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_tabs__link_Color": {
      "name": "--pf-c-tabs__link--Color",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--200",
        "$pf-global--Color--200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "c_tabs__link_FontSize": {
      "name": "--pf-c-tabs__link--FontSize",
      "value": "1rem",
      "values": [
        "--pf-global--FontSize--md",
        "$pf-global--FontSize--md",
        "pf-font-prem(16px)",
        "1rem"
      ]
    },
    "c_tabs__link_BackgroundColor": {
      "name": "--pf-c-tabs__link--BackgroundColor",
      "value": "transparent"
    },
    "c_tabs__link_OutlineOffset": {
      "name": "--pf-c-tabs__link--OutlineOffset",
      "value": "calc(-1 * 0.375rem)"
    },
    "c_tabs__link_PaddingTop": {
      "name": "--pf-c-tabs__link--PaddingTop",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_tabs__link_PaddingRight": {
      "name": "--pf-c-tabs__link--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_tabs__link_PaddingBottom": {
      "name": "--pf-c-tabs__link--PaddingBottom",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_tabs__link_PaddingLeft": {
      "name": "--pf-c-tabs__link--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_tabs__item_m_current__link_Color": {
      "name": "--pf-c-tabs__item--m-current__link--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_tabs__item_m_current__link_BackgroundColor": {
      "name": "--pf-c-tabs__item--m-current__link--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_tabs_m_vertical__link_PaddingTop": {
      "name": "--pf-c-tabs--m-vertical__link--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_tabs_m_vertical__link_PaddingBottom": {
      "name": "--pf-c-tabs--m-vertical__link--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_tabs_m_box__link_BackgroundColor": {
      "name": "--pf-c-tabs--m-box__link--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--200",
        "$pf-global--BackgroundColor--200",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_tabs_m_secondary__link_FontSize": {
      "name": "--pf-c-tabs--m-secondary__link--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_tabs__link_before_border_color_base": {
      "name": "--pf-c-tabs__link--before--border-color--base",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_tabs__link_before_BorderRightColor": {
      "name": "--pf-c-tabs__link--before--BorderRightColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-c-tabs__link--before--border-color--base",
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_tabs__link_before_BorderBottomColor": {
      "name": "--pf-c-tabs__link--before--BorderBottomColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-c-tabs__link--before--border-color--base",
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_tabs__link_before_border_width_base": {
      "name": "--pf-c-tabs__link--before--border-width--base",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_tabs__link_before_BorderTopWidth": {
      "name": "--pf-c-tabs__link--before--BorderTopWidth",
      "value": "0"
    },
    "c_tabs__link_before_BorderRightWidth": {
      "name": "--pf-c-tabs__link--before--BorderRightWidth",
      "value": "0"
    },
    "c_tabs__link_before_BorderBottomWidth": {
      "name": "--pf-c-tabs__link--before--BorderBottomWidth",
      "value": "0"
    },
    "c_tabs__link_before_BorderLeftWidth": {
      "name": "--pf-c-tabs__link--before--BorderLeftWidth",
      "value": "0"
    },
    "c_tabs__link_before_Left": {
      "name": "--pf-c-tabs__link--before--Left",
      "value": "calc(1px * -1)",
      "values": [
        "calc(--pf-c-tabs__link--before--border-width--base * -1)",
        "calc(--pf-global--BorderWidth--sm * -1)",
        "calc($pf-global--BorderWidth--sm * -1)",
        "calc(1px * -1)"
      ]
    },
    "c_tabs__link_after_Top": {
      "name": "--pf-c-tabs__link--after--Top",
      "value": "auto"
    },
    "c_tabs__link_after_Right": {
      "name": "--pf-c-tabs__link--after--Right",
      "value": "0"
    },
    "c_tabs__link_after_Bottom": {
      "name": "--pf-c-tabs__link--after--Bottom",
      "value": "0"
    },
    "c_tabs__link_after_BorderColor": {
      "name": "--pf-c-tabs__link--after--BorderColor",
      "value": "#b8bbbe",
      "values": [
        "--pf-global--BorderColor--light-100",
        "$pf-global--BorderColor--light-100",
        "$pf-color-black-400",
        "#b8bbbe"
      ]
    },
    "c_tabs__link_after_BorderWidth": {
      "name": "--pf-c-tabs__link--after--BorderWidth",
      "value": "0"
    },
    "c_tabs__link_after_BorderTopWidth": {
      "name": "--pf-c-tabs__link--after--BorderTopWidth",
      "value": "0"
    },
    "c_tabs__link_after_BorderRightWidth": {
      "name": "--pf-c-tabs__link--after--BorderRightWidth",
      "value": "0"
    },
    "c_tabs__link_after_BorderLeftWidth": {
      "name": "--pf-c-tabs__link--after--BorderLeftWidth",
      "value": "0"
    },
    "c_tabs__link_hover_after_BorderWidth": {
      "name": "--pf-c-tabs__link--hover--after--BorderWidth",
      "value": "3px",
      "values": [
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    },
    "c_tabs__link_focus_after_BorderWidth": {
      "name": "--pf-c-tabs__link--focus--after--BorderWidth",
      "value": "3px",
      "values": [
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    },
    "c_tabs__link_active_after_BorderWidth": {
      "name": "--pf-c-tabs__link--active--after--BorderWidth",
      "value": "3px",
      "values": [
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    },
    "c_tabs__item_m_current__link_after_BorderColor": {
      "name": "--pf-c-tabs__item--m-current__link--after--BorderColor",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_tabs__item_m_current__link_after_BorderWidth": {
      "name": "--pf-c-tabs__item--m-current__link--after--BorderWidth",
      "value": "3px",
      "values": [
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    },
    "c_tabs__link_child_MarginRight": {
      "name": "--pf-c-tabs__link--child--MarginRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_tabs__scroll_button_Color": {
      "name": "--pf-c-tabs__scroll-button--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_tabs__scroll_button_hover_Color": {
      "name": "--pf-c-tabs__scroll-button--hover--Color",
      "value": "#06c",
      "values": [
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_tabs__scroll_button_disabled_Color": {
      "name": "--pf-c-tabs__scroll-button--disabled--Color",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--disabled-color--200",
        "$pf-global--disabled-color--200",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_tabs__scroll_button_BackgroundColor": {
      "name": "--pf-c-tabs__scroll-button--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_tabs__scroll_button_Width": {
      "name": "--pf-c-tabs__scroll-button--Width",
      "value": "3rem",
      "values": [
        "--pf-global--spacer--2xl",
        "$pf-global--spacer--2xl",
        "pf-size-prem(48px)",
        "3rem"
      ]
    },
    "c_tabs__scroll_button_xl_Width": {
      "name": "--pf-c-tabs__scroll-button--xl--Width",
      "value": "4rem",
      "values": [
        "--pf-global--spacer--3xl",
        "$pf-global--spacer--3xl",
        "pf-size-prem(64px)",
        "4rem"
      ]
    },
    "c_tabs__scroll_button_OutlineOffset": {
      "name": "--pf-c-tabs__scroll-button--OutlineOffset",
      "value": "calc(-1 * 0.25rem)",
      "values": [
        "calc(-1 * --pf-global--spacer--xs)",
        "calc(-1 * $pf-global--spacer--xs)",
        "calc(-1 * pf-size-prem(4px))",
        "calc(-1 * 0.25rem)"
      ]
    },
    "c_tabs__scroll_button_TransitionDuration_margin": {
      "name": "--pf-c-tabs__scroll-button--TransitionDuration--margin",
      "value": ".125s"
    },
    "c_tabs__scroll_button_TransitionDuration_transform": {
      "name": "--pf-c-tabs__scroll-button--TransitionDuration--transform",
      "value": ".125s"
    },
    "c_tabs__scroll_button_TransitionDuration_opacity": {
      "name": "--pf-c-tabs__scroll-button--TransitionDuration--opacity",
      "value": ".125s"
    },
    "c_tabs__scroll_button_before_BorderColor": {
      "name": "--pf-c-tabs__scroll-button--before--BorderColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-c-tabs--before--BorderColor",
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_tabs__scroll_button_before_border_width_base": {
      "name": "--pf-c-tabs__scroll-button--before--border-width--base",
      "value": "1px",
      "values": [
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_tabs__scroll_button_before_BorderRightWidth": {
      "name": "--pf-c-tabs__scroll-button--before--BorderRightWidth",
      "value": "0"
    },
    "c_tabs__scroll_button_before_BorderBottomWidth": {
      "name": "--pf-c-tabs__scroll-button--before--BorderBottomWidth",
      "value": "1px",
      "values": [
        "--pf-c-tabs__scroll-button--before--border-width--base",
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_tabs__scroll_button_before_BorderLeftWidth": {
      "name": "--pf-c-tabs__scroll-button--before--BorderLeftWidth",
      "value": "0"
    }
  },
  ".pf-c-tabs.pf-m-fill .pf-c-tabs__item:first-child": {
    "c_tabs_m_box__item_m_current_first_child__link_before_BorderLeftWidth": {
      "name": "--pf-c-tabs--m-box__item--m-current--first-child__link--before--BorderLeftWidth",
      "value": "0"
    }
  },
  ".pf-c-tabs.pf-m-fill .pf-c-tabs__item:last-child": {
    "c_tabs_m_box__item_m_current_last_child__link_before_BorderRightWidth": {
      "name": "--pf-c-tabs--m-box__item--m-current--last-child__link--before--BorderRightWidth",
      "value": "0"
    }
  },
  ".pf-c-tabs.pf-m-secondary": {
    "c_tabs_before_BorderBottomWidth": {
      "name": "--pf-c-tabs--before--BorderBottomWidth",
      "value": "0"
    },
    "c_tabs__link_FontSize": {
      "name": "--pf-c-tabs__link--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-c-tabs--m-secondary__link--FontSize",
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    }
  },
  ".pf-c-tabs.pf-m-box .pf-c-tabs__link": {
    "c_tabs__link_after_BorderBottomWidth": {
      "name": "--pf-c-tabs__link--after--BorderBottomWidth",
      "value": "0"
    },
    "c_tabs__link_after_BorderTopWidth": {
      "name": "--pf-c-tabs__link--after--BorderTopWidth",
      "value": "0",
      "values": [
        "--pf-c-tabs__link--after--BorderWidth",
        "0"
      ]
    }
  },
  ".pf-c-tabs.pf-m-box": {
    "c_tabs__link_BackgroundColor": {
      "name": "--pf-c-tabs__link--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-c-tabs--m-box__link--BackgroundColor",
        "--pf-global--BackgroundColor--200",
        "$pf-global--BackgroundColor--200",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_tabs__link_before_BorderBottomWidth": {
      "name": "--pf-c-tabs__link--before--BorderBottomWidth",
      "value": "1px",
      "values": [
        "--pf-c-tabs__link--before--border-width--base",
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_tabs__link_before_BorderRightWidth": {
      "name": "--pf-c-tabs__link--before--BorderRightWidth",
      "value": "1px",
      "values": [
        "--pf-c-tabs__link--before--border-width--base",
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_tabs__link_after_Top": {
      "name": "--pf-c-tabs__link--after--Top",
      "value": "0"
    },
    "c_tabs__link_after_Bottom": {
      "name": "--pf-c-tabs__link--after--Bottom",
      "value": "auto"
    }
  },
  ".pf-c-tabs.pf-m-box .pf-c-tabs__item:last-child": {
    "c_tabs__link_before_BorderRightWidth": {
      "name": "--pf-c-tabs__link--before--BorderRightWidth",
      "value": "0"
    }
  },
  ".pf-c-tabs.pf-m-box .pf-c-tabs__item.pf-m-current": {
    "c_tabs__link_BackgroundColor": {
      "name": "--pf-c-tabs__link--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-c-tabs__item--m-current__link--BackgroundColor",
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_tabs__link_before_BorderBottomColor": {
      "name": "--pf-c-tabs__link--before--BorderBottomColor",
      "value": "#fff",
      "values": [
        "--pf-c-tabs__link--BackgroundColor",
        "--pf-c-tabs__item--m-current__link--BackgroundColor",
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    }
  },
  ".pf-c-tabs.pf-m-box .pf-c-tabs__item.pf-m-current + .pf-c-tabs__item": {
    "c_tabs__link_before_Left": {
      "name": "--pf-c-tabs__link--before--Left",
      "value": "0"
    }
  },
  ".pf-c-tabs.pf-m-vertical": {
    "c_tabs_inset": {
      "name": "--pf-c-tabs--inset",
      "value": "1.5rem",
      "values": [
        "--pf-c-tabs--m-vertical--inset",
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_tabs_before_BorderBottomWidth": {
      "name": "--pf-c-tabs--before--BorderBottomWidth",
      "value": "0"
    },
    "c_tabs_before_BorderLeftWidth": {
      "name": "--pf-c-tabs--before--BorderLeftWidth",
      "value": "1px",
      "values": [
        "--pf-c-tabs--before--border-width--base",
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    },
    "c_tabs__link_PaddingTop": {
      "name": "--pf-c-tabs__link--PaddingTop",
      "value": "1rem",
      "values": [
        "--pf-c-tabs--m-vertical__link--PaddingTop",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_tabs__link_PaddingBottom": {
      "name": "--pf-c-tabs__link--PaddingBottom",
      "value": "1rem",
      "values": [
        "--pf-c-tabs--m-vertical__link--PaddingBottom",
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_tabs__link_before_Left": {
      "name": "--pf-c-tabs__link--before--Left",
      "value": "0"
    },
    "c_tabs__link_after_Top": {
      "name": "--pf-c-tabs__link--after--Top",
      "value": "0"
    },
    "c_tabs__link_after_Bottom": {
      "name": "--pf-c-tabs__link--after--Bottom",
      "value": "0"
    },
    "c_tabs__link_after_Right": {
      "name": "--pf-c-tabs__link--after--Right",
      "value": "auto"
    }
  },
  ".pf-c-tabs.pf-m-vertical .pf-c-tabs__link": {
    "c_tabs__link_after_BorderTopWidth": {
      "name": "--pf-c-tabs__link--after--BorderTopWidth",
      "value": "0"
    },
    "c_tabs__link_after_BorderLeftWidth": {
      "name": "--pf-c-tabs__link--after--BorderLeftWidth",
      "value": "0",
      "values": [
        "--pf-c-tabs__link--after--BorderWidth",
        "0"
      ]
    }
  },
  ".pf-c-tabs.pf-m-box.pf-m-vertical": {
    "c_tabs_inset": {
      "name": "--pf-c-tabs--inset",
      "value": "2rem",
      "values": [
        "--pf-c-tabs--m-vertical--m-box--inset",
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_tabs_before_BorderLeftWidth": {
      "name": "--pf-c-tabs--before--BorderLeftWidth",
      "value": "0"
    },
    "c_tabs_before_BorderRightWidth": {
      "name": "--pf-c-tabs--before--BorderRightWidth",
      "value": "1px",
      "values": [
        "--pf-c-tabs--before--border-width--base",
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    }
  },
  ".pf-c-tabs.pf-m-box.pf-m-vertical .pf-c-tabs__item:last-child": {
    "c_tabs__link_before_BorderBottomWidth": {
      "name": "--pf-c-tabs__link--before--BorderBottomWidth",
      "value": "0"
    },
    "c_tabs__link_before_BorderRightWidth": {
      "name": "--pf-c-tabs__link--before--BorderRightWidth",
      "value": "1px",
      "values": [
        "--pf-c-tabs__link--before--border-width--base",
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    }
  },
  ".pf-c-tabs.pf-m-box.pf-m-vertical .pf-c-tabs__item.pf-m-current": {
    "c_tabs__link_before_BorderRightColor": {
      "name": "--pf-c-tabs__link--before--BorderRightColor",
      "value": "#fff",
      "values": [
        "--pf-c-tabs__item--m-current__link--BackgroundColor",
        "--pf-global--BackgroundColor--100",
        "$pf-global--BackgroundColor--100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_tabs__link_before_BorderBottomColor": {
      "name": "--pf-c-tabs__link--before--BorderBottomColor",
      "value": "#d2d2d2",
      "values": [
        "--pf-c-tabs__link--before--border-color--base",
        "--pf-global--BorderColor--100",
        "$pf-global--BorderColor--100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "c_tabs__link_before_BorderBottomWidth": {
      "name": "--pf-c-tabs__link--before--BorderBottomWidth",
      "value": "1px",
      "values": [
        "--pf-c-tabs__link--before--border-width--base",
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    }
  },
  ".pf-c-tabs.pf-m-box.pf-m-vertical .pf-c-tabs__item.pf-m-current:first-child": {
    "c_tabs__link_before_BorderTopWidth": {
      "name": "--pf-c-tabs__link--before--BorderTopWidth",
      "value": "1px",
      "values": [
        "--pf-c-tabs__link--before--border-width--base",
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    }
  },
  ".pf-c-tabs.pf-m-box.pf-m-vertical .pf-c-tabs__item:first-child.pf-m-current": {
    "c_tabs__link_before_BorderTopWidth": {
      "name": "--pf-c-tabs__link--before--BorderTopWidth",
      "value": "1px",
      "values": [
        "--pf-c-tabs__link--before--border-width--base",
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    }
  },
  ".pf-c-tabs__item.pf-m-current": {
    "c_tabs__link_Color": {
      "name": "--pf-c-tabs__link--Color",
      "value": "#151515",
      "values": [
        "--pf-c-tabs__item--m-current__link--Color",
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_tabs__link_after_BorderColor": {
      "name": "--pf-c-tabs__link--after--BorderColor",
      "value": "#06c",
      "values": [
        "--pf-c-tabs__item--m-current__link--after--BorderColor",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_tabs__link_after_BorderWidth": {
      "name": "--pf-c-tabs__link--after--BorderWidth",
      "value": "3px",
      "values": [
        "--pf-c-tabs__item--m-current__link--after--BorderWidth",
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    }
  },
  ".pf-c-tabs__link": {
    "c_tabs__link_after_BorderBottomWidth": {
      "name": "--pf-c-tabs__link--after--BorderBottomWidth",
      "value": "0",
      "values": [
        "--pf-c-tabs__link--after--BorderWidth",
        "0"
      ]
    }
  },
  ".pf-c-tabs__link:hover": {
    "c_tabs__link_after_BorderWidth": {
      "name": "--pf-c-tabs__link--after--BorderWidth",
      "value": "3px",
      "values": [
        "--pf-c-tabs__link--hover--after--BorderWidth",
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    }
  },
  ".pf-c-tabs__link:focus": {
    "c_tabs__link_after_BorderWidth": {
      "name": "--pf-c-tabs__link--after--BorderWidth",
      "value": "3px",
      "values": [
        "--pf-c-tabs__link--focus--after--BorderWidth",
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    }
  },
  ".pf-c-tabs__link:active": {
    "c_tabs__link_after_BorderWidth": {
      "name": "--pf-c-tabs__link--after--BorderWidth",
      "value": "3px",
      "values": [
        "--pf-c-tabs__link--active--after--BorderWidth",
        "--pf-global--BorderWidth--lg",
        "$pf-global--BorderWidth--lg",
        "3px"
      ]
    }
  },
  ".pf-c-tabs__link .pf-c-tabs__item-icon:last-child": {
    "c_tabs__link_child_MarginRight": {
      "name": "--pf-c-tabs__link--child--MarginRight",
      "value": "0"
    }
  },
  ".pf-c-tabs__scroll-button:hover": {
    "c_tabs__scroll_button_Color": {
      "name": "--pf-c-tabs__scroll-button--Color",
      "value": "#06c",
      "values": [
        "--pf-c-tabs__scroll-button--hover--Color",
        "--pf-global--active-color--100",
        "$pf-global--active-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    }
  },
  ".pf-c-tabs__scroll-button:nth-of-type(1)": {
    "c_tabs__scroll_button_before_BorderRightWidth": {
      "name": "--pf-c-tabs__scroll-button--before--BorderRightWidth",
      "value": "1px",
      "values": [
        "--pf-c-tabs__scroll-button--before--border-width--base",
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    }
  },
  ".pf-c-tabs__scroll-button:nth-of-type(2)": {
    "c_tabs__scroll_button_before_BorderLeftWidth": {
      "name": "--pf-c-tabs__scroll-button--before--BorderLeftWidth",
      "value": "1px",
      "values": [
        "--pf-c-tabs__scroll-button--before--border-width--base",
        "--pf-global--BorderWidth--sm",
        "$pf-global--BorderWidth--sm",
        "1px"
      ]
    }
  },
  ".pf-c-tabs__scroll-button:disabled": {
    "c_tabs__scroll_button_Color": {
      "name": "--pf-c-tabs__scroll-button--Color",
      "value": "#d2d2d2",
      "values": [
        "--pf-c-tabs__scroll-button--disabled--Color",
        "--pf-global--disabled-color--200",
        "$pf-global--disabled-color--200",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    }
  },
  ".pf-c-tabs.pf-m-inset-none": {
    "c_tabs_inset": {
      "name": "--pf-c-tabs--inset",
      "value": "0"
    },
    "c_tabs_m_vertical_inset": {
      "name": "--pf-c-tabs--m-vertical--inset",
      "value": "0"
    },
    "c_tabs_m_vertical_m_box_inset": {
      "name": "--pf-c-tabs--m-vertical--m-box--inset",
      "value": "0"
    }
  },
  ".pf-c-tabs.pf-m-inset-sm": {
    "c_tabs_inset": {
      "name": "--pf-c-tabs--inset",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_tabs_m_vertical_inset": {
      "name": "--pf-c-tabs--m-vertical--inset",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_tabs_m_vertical_m_box_inset": {
      "name": "--pf-c-tabs--m-vertical--m-box--inset",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    }
  },
  ".pf-c-tabs.pf-m-inset-md": {
    "c_tabs_inset": {
      "name": "--pf-c-tabs--inset",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_tabs_m_vertical_inset": {
      "name": "--pf-c-tabs--m-vertical--inset",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_tabs_m_vertical_m_box_inset": {
      "name": "--pf-c-tabs--m-vertical--m-box--inset",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    }
  },
  ".pf-c-tabs.pf-m-inset-lg": {
    "c_tabs_inset": {
      "name": "--pf-c-tabs--inset",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_tabs_m_vertical_inset": {
      "name": "--pf-c-tabs--m-vertical--inset",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_tabs_m_vertical_m_box_inset": {
      "name": "--pf-c-tabs--m-vertical--m-box--inset",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    }
  },
  ".pf-c-tabs.pf-m-inset-xl": {
    "c_tabs_inset": {
      "name": "--pf-c-tabs--inset",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_tabs_m_vertical_inset": {
      "name": "--pf-c-tabs--m-vertical--inset",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    },
    "c_tabs_m_vertical_m_box_inset": {
      "name": "--pf-c-tabs--m-vertical--m-box--inset",
      "value": "2rem",
      "values": [
        "--pf-global--spacer--xl",
        "$pf-global--spacer--xl",
        "pf-size-prem(32px)",
        "2rem"
      ]
    }
  },
  ".pf-c-tabs.pf-m-inset-2xl": {
    "c_tabs_inset": {
      "name": "--pf-c-tabs--inset",
      "value": "3rem",
      "values": [
        "--pf-global--spacer--2xl",
        "$pf-global--spacer--2xl",
        "pf-size-prem(48px)",
        "3rem"
      ]
    },
    "c_tabs_m_vertical_inset": {
      "name": "--pf-c-tabs--m-vertical--inset",
      "value": "3rem",
      "values": [
        "--pf-global--spacer--2xl",
        "$pf-global--spacer--2xl",
        "pf-size-prem(48px)",
        "3rem"
      ]
    },
    "c_tabs_m_vertical_m_box_inset": {
      "name": "--pf-c-tabs--m-vertical--m-box--inset",
      "value": "3rem",
      "values": [
        "--pf-global--spacer--2xl",
        "$pf-global--spacer--2xl",
        "pf-size-prem(48px)",
        "3rem"
      ]
    }
  }
};
export default c_tabs;
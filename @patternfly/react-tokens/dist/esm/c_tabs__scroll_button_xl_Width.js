export const c_tabs__scroll_button_xl_Width = {
  "name": "--pf-c-tabs__scroll-button--xl--Width",
  "value": "4rem",
  "var": "var(--pf-c-tabs__scroll-button--xl--Width)"
};
export default c_tabs__scroll_button_xl_Width;
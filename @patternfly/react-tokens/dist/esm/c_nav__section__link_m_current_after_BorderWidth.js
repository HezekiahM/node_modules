export const c_nav__section__link_m_current_after_BorderWidth = {
  "name": "--pf-c-nav__section__link--m-current--after--BorderWidth",
  "value": "4px",
  "var": "var(--pf-c-nav__section__link--m-current--after--BorderWidth)"
};
export default c_nav__section__link_m_current_after_BorderWidth;
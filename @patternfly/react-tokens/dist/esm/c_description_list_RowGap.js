export const c_description_list_RowGap = {
  "name": "--pf-c-description-list--RowGap",
  "value": "1.5rem",
  "var": "var(--pf-c-description-list--RowGap)"
};
export default c_description_list_RowGap;
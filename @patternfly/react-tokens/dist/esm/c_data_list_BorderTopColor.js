export const c_data_list_BorderTopColor = {
  "name": "--pf-c-data-list--BorderTopColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-data-list--BorderTopColor)"
};
export default c_data_list_BorderTopColor;
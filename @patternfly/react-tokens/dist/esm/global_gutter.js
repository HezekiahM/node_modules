export const global_gutter = {
  "name": "--pf-global--gutter",
  "value": "1rem",
  "var": "var(--pf-global--gutter)"
};
export default global_gutter;
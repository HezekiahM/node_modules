export const c_label_m_outline__content_before_BorderWidth = {
  "name": "--pf-c-label--m-outline__content--before--BorderWidth",
  "value": "1px",
  "var": "var(--pf-c-label--m-outline__content--before--BorderWidth)"
};
export default c_label_m_outline__content_before_BorderWidth;
export const c_alert__description_PaddingTop = {
  "name": "--pf-c-alert__description--PaddingTop",
  "value": "0.25rem",
  "var": "var(--pf-c-alert__description--PaddingTop)"
};
export default c_alert__description_PaddingTop;
export const c_toolbar__group_m_chip_container_MarginTop = {
  "name": "--pf-c-toolbar__group--m-chip-container--MarginTop",
  "value": "calc(1rem * -1)",
  "var": "var(--pf-c-toolbar__group--m-chip-container--MarginTop)"
};
export default c_toolbar__group_m_chip_container_MarginTop;
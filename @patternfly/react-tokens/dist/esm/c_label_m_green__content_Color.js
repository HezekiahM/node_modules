export const c_label_m_green__content_Color = {
  "name": "--pf-c-label--m-green__content--Color",
  "value": "#0f280d",
  "var": "var(--pf-c-label--m-green__content--Color)"
};
export default c_label_m_green__content_Color;
export const c_table_cell_MaxWidth = {
  "name": "--pf-c-table--cell--MaxWidth",
  "value": "100%",
  "var": "var(--pf-c-table--cell--MaxWidth)"
};
export default c_table_cell_MaxWidth;
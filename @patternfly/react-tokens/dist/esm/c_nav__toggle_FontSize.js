export const c_nav__toggle_FontSize = {
  "name": "--pf-c-nav__toggle--FontSize",
  "value": "1.125rem",
  "var": "var(--pf-c-nav__toggle--FontSize)"
};
export default c_nav__toggle_FontSize;
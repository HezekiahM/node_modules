export const c_empty_state_m_sm__content_MaxWidth = {
  "name": "--pf-c-empty-state--m-sm__content--MaxWidth",
  "value": "25rem",
  "var": "var(--pf-c-empty-state--m-sm__content--MaxWidth)"
};
export default c_empty_state_m_sm__content_MaxWidth;
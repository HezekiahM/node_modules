export const c_table_cell_Overflow = {
  "name": "--pf-c-table--cell--Overflow",
  "value": "visible",
  "var": "var(--pf-c-table--cell--Overflow)"
};
export default c_table_cell_Overflow;
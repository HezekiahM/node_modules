export const c_alert_m_info_BorderTopColor = {
  "name": "--pf-c-alert--m-info--BorderTopColor",
  "value": "#2b9af3",
  "var": "var(--pf-c-alert--m-info--BorderTopColor)"
};
export default c_alert_m_info_BorderTopColor;
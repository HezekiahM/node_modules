export const c_data_list_m_compact_FontSize = {
  "name": "--pf-c-data-list--m-compact--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-data-list--m-compact--FontSize)"
};
export default c_data_list_m_compact_FontSize;
export const c_wizard__nav_link_before_TranslateX = {
  "name": "--pf-c-wizard__nav-link--before--TranslateX",
  "value": "calc(-100% - 0.5rem)",
  "var": "var(--pf-c-wizard__nav-link--before--TranslateX)"
};
export default c_wizard__nav_link_before_TranslateX;
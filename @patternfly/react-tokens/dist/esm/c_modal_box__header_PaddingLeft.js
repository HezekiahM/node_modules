export const c_modal_box__header_PaddingLeft = {
  "name": "--pf-c-modal-box__header--PaddingLeft",
  "value": "1.5rem",
  "var": "var(--pf-c-modal-box__header--PaddingLeft)"
};
export default c_modal_box__header_PaddingLeft;
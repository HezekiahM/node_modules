export const c_toolbar__group_spacer = {
  "name": "--pf-c-toolbar__group--spacer",
  "value": "1rem",
  "var": "var(--pf-c-toolbar__group--spacer)"
};
export default c_toolbar__group_spacer;
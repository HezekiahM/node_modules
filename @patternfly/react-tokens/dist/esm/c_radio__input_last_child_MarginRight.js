export const c_radio__input_last_child_MarginRight = {
  "name": "--pf-c-radio__input--last-child--MarginRight",
  "value": "0.0625rem",
  "var": "var(--pf-c-radio__input--last-child--MarginRight)"
};
export default c_radio__input_last_child_MarginRight;
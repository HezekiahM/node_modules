export const c_button_m_control_active_BackgroundColor = {
  "name": "--pf-c-button--m-control--active--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-button--m-control--active--BackgroundColor)"
};
export default c_button_m_control_active_BackgroundColor;
export const c_card_m_selectable_m_selected_BoxShadow = {
  "name": "--pf-c-card--m-selectable--m-selected--BoxShadow",
  "value": "0 0.5rem 1rem 0 rgba(3, 3, 3, 0.16), 0 0 0.375rem 0 rgba(3, 3, 3, 0.08)",
  "var": "var(--pf-c-card--m-selectable--m-selected--BoxShadow)"
};
export default c_card_m_selectable_m_selected_BoxShadow;
export const c_dropdown__menu_item_description_Color = {
  "name": "--pf-c-dropdown__menu-item-description--Color",
  "value": "#6a6e73",
  "var": "var(--pf-c-dropdown__menu-item-description--Color)"
};
export default c_dropdown__menu_item_description_Color;
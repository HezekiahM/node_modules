export const c_card_m_flat_BorderColor = {
  "name": "--pf-c-card--m-flat--BorderColor",
  "value": "#d2d2d2",
  "var": "var(--pf-c-card--m-flat--BorderColor)"
};
export default c_card_m_flat_BorderColor;
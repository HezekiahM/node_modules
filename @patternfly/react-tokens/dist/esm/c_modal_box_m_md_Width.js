export const c_modal_box_m_md_Width = {
  "name": "--pf-c-modal-box--m-md--Width",
  "value": "52.5rem",
  "var": "var(--pf-c-modal-box--m-md--Width)"
};
export default c_modal_box_m_md_Width;
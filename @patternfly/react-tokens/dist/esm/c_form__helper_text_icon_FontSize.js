export const c_form__helper_text_icon_FontSize = {
  "name": "--pf-c-form__helper-text-icon--FontSize",
  "value": "1rem",
  "var": "var(--pf-c-form__helper-text-icon--FontSize)"
};
export default c_form__helper_text_icon_FontSize;
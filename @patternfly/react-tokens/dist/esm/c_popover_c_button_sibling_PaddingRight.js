export const c_popover_c_button_sibling_PaddingRight = {
  "name": "--pf-c-popover--c-button--sibling--PaddingRight",
  "value": "3rem",
  "var": "var(--pf-c-popover--c-button--sibling--PaddingRight)"
};
export default c_popover_c_button_sibling_PaddingRight;
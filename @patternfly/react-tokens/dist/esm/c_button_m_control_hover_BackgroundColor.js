export const c_button_m_control_hover_BackgroundColor = {
  "name": "--pf-c-button--m-control--hover--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-button--m-control--hover--BackgroundColor)"
};
export default c_button_m_control_hover_BackgroundColor;
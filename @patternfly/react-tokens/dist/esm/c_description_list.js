export const c_description_list = {
  ".pf-c-description-list": {
    "c_description_list_RowGap": {
      "name": "--pf-c-description-list--RowGap",
      "value": "1.5rem",
      "values": [
        "--pf-global--gutter--md",
        "$pf-global--gutter--md",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_description_list_ColumnGap": {
      "name": "--pf-c-description-list--ColumnGap",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_description_list_GridTemplateColumns_count": {
      "name": "--pf-c-description-list--GridTemplateColumns--count",
      "value": "1"
    },
    "c_description_list_GridTemplateColumns_width": {
      "name": "--pf-c-description-list--GridTemplateColumns--width",
      "value": "1fr"
    },
    "c_description_list_GridTemplateColumns": {
      "name": "--pf-c-description-list--GridTemplateColumns",
      "value": "repeat(1, 1fr)",
      "values": [
        "repeat(--pf-c-description-list--GridTemplateColumns--count, --pf-c-description-list--GridTemplateColumns--width)",
        "repeat(1, 1fr)"
      ]
    },
    "c_description_list__group_RowGap": {
      "name": "--pf-c-description-list__group--RowGap",
      "value": "0.5rem",
      "values": [
        "--pf-global--spacer--sm",
        "$pf-global--spacer--sm",
        "pf-size-prem(8px)",
        "0.5rem"
      ]
    },
    "c_description_list__group_ColumnGap": {
      "name": "--pf-c-description-list__group--ColumnGap",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_description_list__group_GridTemplateColumns": {
      "name": "--pf-c-description-list__group--GridTemplateColumns",
      "value": "auto"
    },
    "c_description_list__group_GridColumn": {
      "name": "--pf-c-description-list__group--GridColumn",
      "value": "auto"
    },
    "c_description_list__term_FontWeight": {
      "name": "--pf-c-description-list__term--FontWeight",
      "value": "700",
      "values": [
        "--pf-global--FontWeight--bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    },
    "c_description_list__term_FontSize": {
      "name": "--pf-c-description-list__term--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_description_list_m_horizontal__term_width": {
      "name": "--pf-c-description-list--m-horizontal__term--width",
      "value": "14ch"
    },
    "c_description_list_m_horizontal__description_width": {
      "name": "--pf-c-description-list--m-horizontal__description--width",
      "value": "minmax(10ch, auto)"
    },
    "c_description_list_m_horizontal__group_GridTemplateColumns": {
      "name": "--pf-c-description-list--m-horizontal__group--GridTemplateColumns",
      "value": "14ch minmax(10ch, auto)",
      "values": [
        "--pf-c-description-list--m-horizontal__term--width --pf-c-description-list--m-horizontal__description--width",
        "14ch minmax(10ch, auto)"
      ]
    },
    "c_description_list_m_1_col_GridTemplateColumns_count": {
      "name": "--pf-c-description-list--m-1-col--GridTemplateColumns--count",
      "value": "1"
    }
  },
  ".pf-c-description-list.pf-m-horizontal": {
    "c_description_list__group_GridTemplateColumns": {
      "name": "--pf-c-description-list__group--GridTemplateColumns",
      "value": "14ch minmax(10ch, auto)",
      "values": [
        "--pf-c-description-list--m-horizontal__group--GridTemplateColumns",
        "--pf-c-description-list--m-horizontal__term--width --pf-c-description-list--m-horizontal__description--width",
        "14ch minmax(10ch, auto)"
      ]
    }
  },
  ".pf-c-description-list.pf-m-auto-column-widths": {
    "c_description_list_GridTemplateColumns_width": {
      "name": "--pf-c-description-list--GridTemplateColumns--width",
      "value": "minmax(8ch, max-content)"
    }
  },
  ".pf-c-description-list.pf-m-1-col": {
    "c_description_list_GridTemplateColumns_count": {
      "name": "--pf-c-description-list--GridTemplateColumns--count",
      "value": "1",
      "values": [
        "--pf-c-description-list--m-1-col--GridTemplateColumns--count",
        "1"
      ]
    }
  },
  ".pf-c-description-list.pf-m-2-col": {
    "c_description_list_GridTemplateColumns_count": {
      "name": "--pf-c-description-list--GridTemplateColumns--count",
      "value": "undefined",
      "values": [
        "--pf-c-description-list--m-2-col--GridTemplateColumns--count",
        "undefined"
      ]
    }
  },
  ".pf-c-description-list.pf-m-3-col": {
    "c_description_list_GridTemplateColumns_count": {
      "name": "--pf-c-description-list--GridTemplateColumns--count",
      "value": "undefined",
      "values": [
        "--pf-c-description-list--m-3-col--GridTemplateColumns--count",
        "undefined"
      ]
    }
  }
};
export default c_description_list;
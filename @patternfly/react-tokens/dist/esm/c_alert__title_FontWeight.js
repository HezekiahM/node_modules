export const c_alert__title_FontWeight = {
  "name": "--pf-c-alert__title--FontWeight",
  "value": "400",
  "var": "var(--pf-c-alert__title--FontWeight)"
};
export default c_alert__title_FontWeight;
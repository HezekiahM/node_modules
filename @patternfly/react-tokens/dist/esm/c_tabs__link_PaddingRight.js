export const c_tabs__link_PaddingRight = {
  "name": "--pf-c-tabs__link--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-tabs__link--PaddingRight)"
};
export default c_tabs__link_PaddingRight;
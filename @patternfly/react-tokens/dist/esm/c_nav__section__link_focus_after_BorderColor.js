export const c_nav__section__link_focus_after_BorderColor = {
  "name": "--pf-c-nav__section__link--focus--after--BorderColor",
  "value": "transparent",
  "var": "var(--pf-c-nav__section__link--focus--after--BorderColor)"
};
export default c_nav__section__link_focus_after_BorderColor;
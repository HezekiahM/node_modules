export const c_nav__link_FontSize = {
  "name": "--pf-c-nav__link--FontSize",
  "value": "1rem",
  "var": "var(--pf-c-nav__link--FontSize)"
};
export default c_nav__link_FontSize;
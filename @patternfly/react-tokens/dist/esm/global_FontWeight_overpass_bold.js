export const global_FontWeight_overpass_bold = {
  "name": "--pf-global--FontWeight--overpass--bold",
  "value": "600",
  "var": "var(--pf-global--FontWeight--overpass--bold)"
};
export default global_FontWeight_overpass_bold;
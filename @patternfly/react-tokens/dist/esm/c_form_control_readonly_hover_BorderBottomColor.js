export const c_form_control_readonly_hover_BorderBottomColor = {
  "name": "--pf-c-form-control--readonly--hover--BorderBottomColor",
  "value": "#8a8d90",
  "var": "var(--pf-c-form-control--readonly--hover--BorderBottomColor)"
};
export default c_form_control_readonly_hover_BorderBottomColor;
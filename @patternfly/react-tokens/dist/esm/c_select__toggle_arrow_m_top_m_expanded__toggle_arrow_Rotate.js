export const c_select__toggle_arrow_m_top_m_expanded__toggle_arrow_Rotate = {
  "name": "--pf-c-select__toggle-arrow--m-top--m-expanded__toggle-arrow--Rotate",
  "value": "180deg",
  "var": "var(--pf-c-select__toggle-arrow--m-top--m-expanded__toggle-arrow--Rotate)"
};
export default c_select__toggle_arrow_m_top_m_expanded__toggle_arrow_Rotate;
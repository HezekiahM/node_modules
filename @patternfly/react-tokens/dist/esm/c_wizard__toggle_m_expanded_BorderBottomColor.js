export const c_wizard__toggle_m_expanded_BorderBottomColor = {
  "name": "--pf-c-wizard__toggle--m-expanded--BorderBottomColor",
  "value": "#d2d2d2",
  "var": "var(--pf-c-wizard__toggle--m-expanded--BorderBottomColor)"
};
export default c_wizard__toggle_m_expanded_BorderBottomColor;
export const c_popover_MaxWidth = {
  "name": "--pf-c-popover--MaxWidth",
  "value": "calc(1rem + 1rem + 18.75rem)",
  "var": "var(--pf-c-popover--MaxWidth)"
};
export default c_popover_MaxWidth;
export const c_modal_box__body_PaddingLeft = {
  "name": "--pf-c-modal-box__body--PaddingLeft",
  "value": "1.5rem",
  "var": "var(--pf-c-modal-box__body--PaddingLeft)"
};
export default c_modal_box__body_PaddingLeft;
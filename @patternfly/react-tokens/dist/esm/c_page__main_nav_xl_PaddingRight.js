export const c_page__main_nav_xl_PaddingRight = {
  "name": "--pf-c-page__main-nav--xl--PaddingRight",
  "value": "0.5rem",
  "var": "var(--pf-c-page__main-nav--xl--PaddingRight)"
};
export default c_page__main_nav_xl_PaddingRight;
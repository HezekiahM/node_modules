export const c_alert_BackgroundColor = {
  "name": "--pf-c-alert--BackgroundColor",
  "value": "#f2f9f9",
  "var": "var(--pf-c-alert--BackgroundColor)"
};
export default c_alert_BackgroundColor;
export const c_nav_m_tertiary__scroll_button_disabled_Color = {
  "name": "--pf-c-nav--m-tertiary__scroll-button--disabled--Color",
  "value": "#d2d2d2",
  "var": "var(--pf-c-nav--m-tertiary__scroll-button--disabled--Color)"
};
export default c_nav_m_tertiary__scroll_button_disabled_Color;
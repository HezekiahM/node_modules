export const c_tile__body_FontSize = {
  "name": "--pf-c-tile__body--FontSize",
  "value": "0.75rem",
  "var": "var(--pf-c-tile__body--FontSize)"
};
export default c_tile__body_FontSize;
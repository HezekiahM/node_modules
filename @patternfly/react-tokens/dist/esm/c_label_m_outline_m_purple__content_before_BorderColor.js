export const c_label_m_outline_m_purple__content_before_BorderColor = {
  "name": "--pf-c-label--m-outline--m-purple__content--before--BorderColor",
  "value": "#cbc1ff",
  "var": "var(--pf-c-label--m-outline--m-purple__content--before--BorderColor)"
};
export default c_label_m_outline_m_purple__content_before_BorderColor;
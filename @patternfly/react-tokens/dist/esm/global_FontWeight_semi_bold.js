export const global_FontWeight_semi_bold = {
  "name": "--pf-global--FontWeight--semi-bold",
  "value": "500",
  "var": "var(--pf-global--FontWeight--semi-bold)"
};
export default global_FontWeight_semi_bold;
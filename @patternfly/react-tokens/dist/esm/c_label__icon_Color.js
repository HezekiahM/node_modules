export const c_label__icon_Color = {
  "name": "--pf-c-label__icon--Color",
  "value": "#009596",
  "var": "var(--pf-c-label__icon--Color)"
};
export default c_label__icon_Color;
export const c_select__toggle_before_BorderTopColor = {
  "name": "--pf-c-select__toggle--before--BorderTopColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-select__toggle--before--BorderTopColor)"
};
export default c_select__toggle_before_BorderTopColor;
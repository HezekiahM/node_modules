export const c_description_list__group_GridTemplateColumns = {
  "name": "--pf-c-description-list__group--GridTemplateColumns",
  "value": "14ch minmax(10ch, auto)",
  "var": "var(--pf-c-description-list__group--GridTemplateColumns)"
};
export default c_description_list__group_GridTemplateColumns;
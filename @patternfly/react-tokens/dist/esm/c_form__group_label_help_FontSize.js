export const c_form__group_label_help_FontSize = {
  "name": "--pf-c-form__group-label-help--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-form__group-label-help--FontSize)"
};
export default c_form__group_label_help_FontSize;
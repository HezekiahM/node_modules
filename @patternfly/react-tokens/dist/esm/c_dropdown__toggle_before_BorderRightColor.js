export const c_dropdown__toggle_before_BorderRightColor = {
  "name": "--pf-c-dropdown__toggle--before--BorderRightColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-dropdown__toggle--before--BorderRightColor)"
};
export default c_dropdown__toggle_before_BorderRightColor;
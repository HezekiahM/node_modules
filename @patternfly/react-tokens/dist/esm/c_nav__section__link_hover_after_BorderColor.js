export const c_nav__section__link_hover_after_BorderColor = {
  "name": "--pf-c-nav__section__link--hover--after--BorderColor",
  "value": "transparent",
  "var": "var(--pf-c-nav__section__link--hover--after--BorderColor)"
};
export default c_nav__section__link_hover_after_BorderColor;
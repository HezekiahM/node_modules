export const c_table_cell_FontWeight = {
  "name": "--pf-c-table--cell--FontWeight",
  "value": "700",
  "var": "var(--pf-c-table--cell--FontWeight)"
};
export default c_table_cell_FontWeight;
export const c_drawer_m_inline__panel_PaddingLeft = {
  "name": "--pf-c-drawer--m-inline__panel--PaddingLeft",
  "value": "1px",
  "var": "var(--pf-c-drawer--m-inline__panel--PaddingLeft)"
};
export default c_drawer_m_inline__panel_PaddingLeft;
export const c_tooltip__arrow_m_top_TranslateY = {
  "name": "--pf-c-tooltip__arrow--m-top--TranslateY",
  "value": "50%",
  "var": "var(--pf-c-tooltip__arrow--m-top--TranslateY)"
};
export default c_tooltip__arrow_m_top_TranslateY;
export const global_palette_blue_50 = {
  "name": "--pf-global--palette--blue-50",
  "value": "#e7f1fa",
  "var": "var(--pf-global--palette--blue-50)"
};
export default global_palette_blue_50;
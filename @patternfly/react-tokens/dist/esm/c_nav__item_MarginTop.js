export const c_nav__item_MarginTop = {
  "name": "--pf-c-nav__item--MarginTop",
  "value": "0.5rem",
  "var": "var(--pf-c-nav__item--MarginTop)"
};
export default c_nav__item_MarginTop;
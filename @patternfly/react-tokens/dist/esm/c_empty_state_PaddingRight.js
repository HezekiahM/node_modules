export const c_empty_state_PaddingRight = {
  "name": "--pf-c-empty-state--PaddingRight",
  "value": "2rem",
  "var": "var(--pf-c-empty-state--PaddingRight)"
};
export default c_empty_state_PaddingRight;
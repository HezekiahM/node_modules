export const c_tabs__link_before_BorderRightWidth = {
  "name": "--pf-c-tabs__link--before--BorderRightWidth",
  "value": "1px",
  "var": "var(--pf-c-tabs__link--before--BorderRightWidth)"
};
export default c_tabs__link_before_BorderRightWidth;
export const global_Color_200 = {
  "name": "--pf-global--Color--200",
  "value": "#f0f0f0",
  "var": "var(--pf-global--Color--200)"
};
export default global_Color_200;
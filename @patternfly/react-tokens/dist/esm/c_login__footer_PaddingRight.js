export const c_login__footer_PaddingRight = {
  "name": "--pf-c-login__footer--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-login__footer--PaddingRight)"
};
export default c_login__footer_PaddingRight;
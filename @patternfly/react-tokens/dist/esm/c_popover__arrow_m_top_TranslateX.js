export const c_popover__arrow_m_top_TranslateX = {
  "name": "--pf-c-popover__arrow--m-top--TranslateX",
  "value": "-50%",
  "var": "var(--pf-c-popover__arrow--m-top--TranslateX)"
};
export default c_popover__arrow_m_top_TranslateX;
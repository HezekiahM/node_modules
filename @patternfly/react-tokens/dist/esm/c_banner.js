export const c_banner = {
  ".pf-c-banner.pf-m-info": {
    "global_Color_100": {
      "name": "--pf-global--Color--100",
      "value": "#151515",
      "values": [
        "--pf-global--Color--dark-100",
        "$pf-global--Color--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "global_Color_200": {
      "name": "--pf-global--Color--200",
      "value": "#6a6e73",
      "values": [
        "--pf-global--Color--dark-200",
        "$pf-global--Color--dark-200",
        "$pf-color-black-600",
        "#6a6e73"
      ]
    },
    "global_BorderColor_100": {
      "name": "--pf-global--BorderColor--100",
      "value": "#d2d2d2",
      "values": [
        "--pf-global--BorderColor--dark-100",
        "$pf-global--BorderColor--dark-100",
        "$pf-color-black-300",
        "#d2d2d2"
      ]
    },
    "global_primary_color_100": {
      "name": "--pf-global--primary-color--100",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--dark-100",
        "$pf-global--primary-color--dark-100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "global_link_Color": {
      "name": "--pf-global--link--Color",
      "value": "#06c",
      "values": [
        "--pf-global--link--Color--dark",
        "$pf-global--link--Color--dark",
        "$pf-global--primary-color--100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "global_link_Color_hover": {
      "name": "--pf-global--link--Color--hover",
      "value": "#004080",
      "values": [
        "--pf-global--link--Color--dark--hover",
        "$pf-global--link--Color--dark--hover",
        "$pf-global--primary-color--200",
        "$pf-color-blue-500",
        "#004080"
      ]
    },
    "global_BackgroundColor_100": {
      "name": "--pf-global--BackgroundColor--100",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--light-100",
        "$pf-global--BackgroundColor--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_banner_BackgroundColor": {
      "name": "--pf-c-banner--BackgroundColor",
      "value": "#73bcf7",
      "values": [
        "--pf-c-banner--m-info--BackgroundColor",
        "--pf-global--palette--blue-200",
        "$pf-color-blue-200",
        "#73bcf7"
      ]
    }
  },
  ".pf-c-banner": {
    "global_Color_100": {
      "name": "--pf-global--Color--100",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "global_Color_200": {
      "name": "--pf-global--Color--200",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--Color--light-200",
        "$pf-global--Color--light-200",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "global_BorderColor_100": {
      "name": "--pf-global--BorderColor--100",
      "value": "#b8bbbe",
      "values": [
        "--pf-global--BorderColor--light-100",
        "$pf-global--BorderColor--light-100",
        "$pf-color-black-400",
        "#b8bbbe"
      ]
    },
    "global_primary_color_100": {
      "name": "--pf-global--primary-color--100",
      "value": "#73bcf7",
      "values": [
        "--pf-global--primary-color--light-100",
        "$pf-global--primary-color--light-100",
        "$pf-color-blue-200",
        "#73bcf7"
      ]
    },
    "global_link_Color": {
      "name": "--pf-global--link--Color",
      "value": "#73bcf7",
      "values": [
        "--pf-global--link--Color--light",
        "$pf-global--link--Color--light",
        "$pf-global--active-color--300",
        "$pf-color-blue-200",
        "#73bcf7"
      ]
    },
    "global_link_Color_hover": {
      "name": "--pf-global--link--Color--hover",
      "value": "#73bcf7",
      "values": [
        "--pf-global--link--Color--light",
        "$pf-global--link--Color--light",
        "$pf-global--active-color--300",
        "$pf-color-blue-200",
        "#73bcf7"
      ]
    },
    "global_BackgroundColor_100": {
      "name": "--pf-global--BackgroundColor--100",
      "value": "#151515",
      "values": [
        "--pf-global--BackgroundColor--dark-100",
        "$pf-global--BackgroundColor--dark-100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_banner_PaddingTop": {
      "name": "--pf-c-banner--PaddingTop",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_banner_PaddingRight": {
      "name": "--pf-c-banner--PaddingRight",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_banner_md_PaddingRight": {
      "name": "--pf-c-banner--md--PaddingRight",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_banner_PaddingBottom": {
      "name": "--pf-c-banner--PaddingBottom",
      "value": "0.25rem",
      "values": [
        "--pf-global--spacer--xs",
        "$pf-global--spacer--xs",
        "pf-size-prem(4px)",
        "0.25rem"
      ]
    },
    "c_banner_PaddingLeft": {
      "name": "--pf-c-banner--PaddingLeft",
      "value": "1rem",
      "values": [
        "--pf-global--spacer--md",
        "$pf-global--spacer--md",
        "pf-size-prem(16px)",
        "1rem"
      ]
    },
    "c_banner_md_PaddingLeft": {
      "name": "--pf-c-banner--md--PaddingLeft",
      "value": "1.5rem",
      "values": [
        "--pf-global--spacer--lg",
        "$pf-global--spacer--lg",
        "pf-size-prem(24px)",
        "1.5rem"
      ]
    },
    "c_banner_FontSize": {
      "name": "--pf-c-banner--FontSize",
      "value": "0.875rem",
      "values": [
        "--pf-global--FontSize--sm",
        "$pf-global--FontSize--sm",
        "pf-font-prem(14px)",
        "0.875rem"
      ]
    },
    "c_banner_Color": {
      "name": "--pf-c-banner--Color",
      "value": "#151515",
      "values": [
        "--pf-global--Color--100",
        "$pf-global--Color--100",
        "$pf-color-black-900",
        "#151515"
      ]
    },
    "c_banner_BackgroundColor": {
      "name": "--pf-c-banner--BackgroundColor",
      "value": "#4f5255",
      "values": [
        "--pf-global--BackgroundColor--dark-400",
        "$pf-global--BackgroundColor--dark-400",
        "$pf-color-black-700",
        "#4f5255"
      ]
    },
    "c_banner_m_info_BackgroundColor": {
      "name": "--pf-c-banner--m-info--BackgroundColor",
      "value": "#73bcf7",
      "values": [
        "--pf-global--palette--blue-200",
        "$pf-color-blue-200",
        "#73bcf7"
      ]
    },
    "c_banner_m_danger_BackgroundColor": {
      "name": "--pf-c-banner--m-danger--BackgroundColor",
      "value": "#c9190b",
      "values": [
        "--pf-global--danger-color--100",
        "$pf-global--danger-color--100",
        "$pf-color-red-100",
        "#c9190b"
      ]
    },
    "c_banner_m_success_BackgroundColor": {
      "name": "--pf-c-banner--m-success--BackgroundColor",
      "value": "#3e8635",
      "values": [
        "--pf-global--success-color--100",
        "$pf-global--success-color--100",
        "$pf-color-green-500",
        "#3e8635"
      ]
    },
    "c_banner_m_warning_BackgroundColor": {
      "name": "--pf-c-banner--m-warning--BackgroundColor",
      "value": "#f0ab00",
      "values": [
        "--pf-global--warning-color--100",
        "$pf-global--warning-color--100",
        "$pf-color-gold-400",
        "#f0ab00"
      ]
    },
    "c_banner_m_sticky_ZIndex": {
      "name": "--pf-c-banner--m-sticky--ZIndex",
      "value": "300",
      "values": [
        "--pf-global--ZIndex--md",
        "$pf-global--ZIndex--md",
        "300"
      ]
    },
    "c_banner_m_sticky_BoxShadow": {
      "name": "--pf-c-banner--m-sticky--BoxShadow",
      "value": "0 0.5rem 0.5rem -0.375rem rgba(3, 3, 3, 0.18)",
      "values": [
        "--pf-global--BoxShadow--md-bottom",
        "$pf-global--BoxShadow--md-bottom",
        "0 pf-size-prem(8px) pf-size-prem(8px) pf-size-prem(-6px) rgba($pf-color-black-1000, .18)",
        "0 pf-size-prem(8px) pf-size-prem(8px) pf-size-prem(-6px) rgba(#030303, .18)",
        "0 0.5rem 0.5rem -0.375rem rgba(3, 3, 3, 0.18)"
      ]
    }
  },
  ".pf-c-banner .pf-c-card": {
    "c_card_BackgroundColor": {
      "name": "--pf-c-card--BackgroundColor",
      "value": "rgba(#030303, .32)",
      "values": [
        "--pf-global--BackgroundColor--dark-transparent-200",
        "$pf-global--BackgroundColor--dark-transparent-200",
        "rgba($pf-color-black-1000, .32)",
        "rgba(#030303, .32)"
      ]
    }
  },
  ".pf-c-banner .pf-c-button": {
    "c_button_m_primary_Color": {
      "name": "--pf-c-button--m-primary--Color",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--dark-100",
        "$pf-global--primary-color--dark-100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_primary_hover_Color": {
      "name": "--pf-c-button--m-primary--hover--Color",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--dark-100",
        "$pf-global--primary-color--dark-100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_primary_focus_Color": {
      "name": "--pf-c-button--m-primary--focus--Color",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--dark-100",
        "$pf-global--primary-color--dark-100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_primary_active_Color": {
      "name": "--pf-c-button--m-primary--active--Color",
      "value": "#06c",
      "values": [
        "--pf-global--primary-color--dark-100",
        "$pf-global--primary-color--dark-100",
        "$pf-color-blue-400",
        "#06c"
      ]
    },
    "c_button_m_primary_BackgroundColor": {
      "name": "--pf-c-button--m-primary--BackgroundColor",
      "value": "#fff",
      "values": [
        "--pf-global--BackgroundColor--light-100",
        "$pf-global--BackgroundColor--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_primary_hover_BackgroundColor": {
      "name": "--pf-c-button--m-primary--hover--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_button_m_primary_focus_BackgroundColor": {
      "name": "--pf-c-button--m-primary--focus--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_button_m_primary_active_BackgroundColor": {
      "name": "--pf-c-button--m-primary--active--BackgroundColor",
      "value": "#f0f0f0",
      "values": [
        "--pf-global--BackgroundColor--light-300",
        "$pf-global--BackgroundColor--light-300",
        "$pf-color-black-200",
        "#f0f0f0"
      ]
    },
    "c_button_m_secondary_Color": {
      "name": "--pf-c-button--m-secondary--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_hover_Color": {
      "name": "--pf-c-button--m-secondary--hover--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_focus_Color": {
      "name": "--pf-c-button--m-secondary--focus--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_active_Color": {
      "name": "--pf-c-button--m-secondary--active--Color",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_BorderColor": {
      "name": "--pf-c-button--m-secondary--BorderColor",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_hover_BorderColor": {
      "name": "--pf-c-button--m-secondary--hover--BorderColor",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_focus_BorderColor": {
      "name": "--pf-c-button--m-secondary--focus--BorderColor",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    },
    "c_button_m_secondary_active_BorderColor": {
      "name": "--pf-c-button--m-secondary--active--BorderColor",
      "value": "#fff",
      "values": [
        "--pf-global--Color--light-100",
        "$pf-global--Color--light-100",
        "$pf-color-white",
        "#fff"
      ]
    }
  },
  ".pf-c-banner.pf-m-danger": {
    "c_banner_BackgroundColor": {
      "name": "--pf-c-banner--BackgroundColor",
      "value": "#c9190b",
      "values": [
        "--pf-c-banner--m-danger--BackgroundColor",
        "--pf-global--danger-color--100",
        "$pf-global--danger-color--100",
        "$pf-color-red-100",
        "#c9190b"
      ]
    }
  },
  ".pf-c-banner.pf-m-success": {
    "c_banner_BackgroundColor": {
      "name": "--pf-c-banner--BackgroundColor",
      "value": "#3e8635",
      "values": [
        "--pf-c-banner--m-success--BackgroundColor",
        "--pf-global--success-color--100",
        "$pf-global--success-color--100",
        "$pf-color-green-500",
        "#3e8635"
      ]
    }
  },
  ".pf-c-banner.pf-m-warning": {
    "c_banner_BackgroundColor": {
      "name": "--pf-c-banner--BackgroundColor",
      "value": "#f0ab00",
      "values": [
        "--pf-c-banner--m-warning--BackgroundColor",
        "--pf-global--warning-color--100",
        "$pf-global--warning-color--100",
        "$pf-color-gold-400",
        "#f0ab00"
      ]
    }
  }
};
export default c_banner;
export const c_tile_m_display_lg__header_m_stacked__icon_Height = {
  "name": "--pf-c-tile--m-display-lg__header--m-stacked__icon--Height",
  "value": "3.375rem",
  "var": "var(--pf-c-tile--m-display-lg__header--m-stacked__icon--Height)"
};
export default c_tile_m_display_lg__header_m_stacked__icon_Height;
export const c_notification_drawer__group_m_expanded__group_toggle_icon_Rotate = {
  "name": "--pf-c-notification-drawer__group--m-expanded__group-toggle-icon--Rotate",
  "value": "90deg",
  "var": "var(--pf-c-notification-drawer__group--m-expanded__group-toggle-icon--Rotate)"
};
export default c_notification_drawer__group_m_expanded__group_toggle_icon_Rotate;
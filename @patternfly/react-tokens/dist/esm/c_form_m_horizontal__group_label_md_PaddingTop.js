export const c_form_m_horizontal__group_label_md_PaddingTop = {
  "name": "--pf-c-form--m-horizontal__group-label--md--PaddingTop",
  "value": "0.5rem",
  "var": "var(--pf-c-form--m-horizontal__group-label--md--PaddingTop)"
};
export default c_form_m_horizontal__group_label_md_PaddingTop;
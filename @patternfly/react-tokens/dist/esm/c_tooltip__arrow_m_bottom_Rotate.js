export const c_tooltip__arrow_m_bottom_Rotate = {
  "name": "--pf-c-tooltip__arrow--m-bottom--Rotate",
  "value": "45deg",
  "var": "var(--pf-c-tooltip__arrow--m-bottom--Rotate)"
};
export default c_tooltip__arrow_m_bottom_Rotate;
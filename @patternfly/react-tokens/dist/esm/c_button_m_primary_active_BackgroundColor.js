export const c_button_m_primary_active_BackgroundColor = {
  "name": "--pf-c-button--m-primary--active--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-button--m-primary--active--BackgroundColor)"
};
export default c_button_m_primary_active_BackgroundColor;
export const c_label_m_green_BackgroundColor = {
  "name": "--pf-c-label--m-green--BackgroundColor",
  "value": "#f3faf2",
  "var": "var(--pf-c-label--m-green--BackgroundColor)"
};
export default c_label_m_green_BackgroundColor;
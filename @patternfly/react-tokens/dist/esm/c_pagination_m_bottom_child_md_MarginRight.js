export const c_pagination_m_bottom_child_md_MarginRight = {
  "name": "--pf-c-pagination--m-bottom--child--md--MarginRight",
  "value": "1.5rem",
  "var": "var(--pf-c-pagination--m-bottom--child--md--MarginRight)"
};
export default c_pagination_m_bottom_child_md_MarginRight;
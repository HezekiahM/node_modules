export const c_table_thead_cell_FontWeight = {
  "name": "--pf-c-table--thead--cell--FontWeight",
  "value": "700",
  "var": "var(--pf-c-table--thead--cell--FontWeight)"
};
export default c_table_thead_cell_FontWeight;
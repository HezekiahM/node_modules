export const c_table__compound_expansion_toggle__button_before_border_width_base = {
  "name": "--pf-c-table__compound-expansion-toggle__button--before--border-width--base",
  "value": "1px",
  "var": "var(--pf-c-table__compound-expansion-toggle__button--before--border-width--base)"
};
export default c_table__compound_expansion_toggle__button_before_border_width_base;
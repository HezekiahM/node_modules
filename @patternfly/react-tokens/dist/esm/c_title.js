export const c_title = {
  ".pf-c-title": {
    "c_title_FontFamily": {
      "name": "--pf-c-title--FontFamily",
      "value": "\"RedHatDisplay\", \"Overpass\", overpass, helvetica, arial, sans-serif",
      "values": [
        "--pf-global--FontFamily--heading--sans-serif",
        "$pf-global--FontFamily--heading--sans-serif",
        "\"RedHatDisplay\", \"Overpass\", overpass, helvetica, arial, sans-serif",
        "\"RedHatDisplay\", \"Overpass\", overpass, helvetica, arial, sans-serif"
      ]
    },
    "c_title_m_4xl_LineHeight": {
      "name": "--pf-c-title--m-4xl--LineHeight",
      "value": "1.3",
      "values": [
        "--pf-global--LineHeight--sm",
        "$pf-global--LineHeight--sm",
        "1.3"
      ]
    },
    "c_title_m_4xl_FontSize": {
      "name": "--pf-c-title--m-4xl--FontSize",
      "value": "2.25rem",
      "values": [
        "--pf-global--FontSize--4xl",
        "$pf-global--FontSize--4xl",
        "pf-font-prem(36px)",
        "2.25rem"
      ]
    },
    "c_title_m_4xl_FontWeight": {
      "name": "--pf-c-title--m-4xl--FontWeight",
      "value": "400",
      "values": [
        "--pf-global--FontWeight--normal",
        "$pf-global--FontWeight--normal",
        "400"
      ]
    },
    "c_title_m_3xl_LineHeight": {
      "name": "--pf-c-title--m-3xl--LineHeight",
      "value": "1.3",
      "values": [
        "--pf-global--LineHeight--sm",
        "$pf-global--LineHeight--sm",
        "1.3"
      ]
    },
    "c_title_m_3xl_FontSize": {
      "name": "--pf-c-title--m-3xl--FontSize",
      "value": "1.75rem",
      "values": [
        "--pf-global--FontSize--3xl",
        "$pf-global--FontSize--3xl",
        "pf-font-prem(28px)",
        "1.75rem"
      ]
    },
    "c_title_m_3xl_FontWeight": {
      "name": "--pf-c-title--m-3xl--FontWeight",
      "value": "400",
      "values": [
        "--pf-global--FontWeight--normal",
        "$pf-global--FontWeight--normal",
        "400"
      ]
    },
    "c_title_m_2xl_LineHeight": {
      "name": "--pf-c-title--m-2xl--LineHeight",
      "value": "1.3",
      "values": [
        "--pf-global--LineHeight--sm",
        "$pf-global--LineHeight--sm",
        "1.3"
      ]
    },
    "c_title_m_2xl_FontSize": {
      "name": "--pf-c-title--m-2xl--FontSize",
      "value": "1.5rem",
      "values": [
        "--pf-global--FontSize--2xl",
        "$pf-global--FontSize--2xl",
        "pf-font-prem(24px)",
        "1.5rem"
      ]
    },
    "c_title_m_2xl_FontWeight": {
      "name": "--pf-c-title--m-2xl--FontWeight",
      "value": "400",
      "values": [
        "--pf-global--FontWeight--normal",
        "$pf-global--FontWeight--normal",
        "400"
      ]
    },
    "c_title_m_xl_LineHeight": {
      "name": "--pf-c-title--m-xl--LineHeight",
      "value": "1.5",
      "values": [
        "--pf-global--LineHeight--md",
        "$pf-global--LineHeight--md",
        "1.5"
      ]
    },
    "c_title_m_xl_FontSize": {
      "name": "--pf-c-title--m-xl--FontSize",
      "value": "1.25rem",
      "values": [
        "--pf-global--FontSize--xl",
        "$pf-global--FontSize--xl",
        "pf-font-prem(20px)",
        "1.25rem"
      ]
    },
    "c_title_m_xl_FontWeight": {
      "name": "--pf-c-title--m-xl--FontWeight",
      "value": "400",
      "values": [
        "--pf-global--FontWeight--normal",
        "$pf-global--FontWeight--normal",
        "400"
      ]
    },
    "c_title_m_lg_LineHeight": {
      "name": "--pf-c-title--m-lg--LineHeight",
      "value": "1.5",
      "values": [
        "--pf-global--LineHeight--md",
        "$pf-global--LineHeight--md",
        "1.5"
      ]
    },
    "c_title_m_lg_FontSize": {
      "name": "--pf-c-title--m-lg--FontSize",
      "value": "1.125rem",
      "values": [
        "--pf-global--FontSize--lg",
        "$pf-global--FontSize--lg",
        "pf-font-prem(18px)",
        "1.125rem"
      ]
    },
    "c_title_m_lg_FontWeight": {
      "name": "--pf-c-title--m-lg--FontWeight",
      "value": "400",
      "values": [
        "--pf-global--FontWeight--normal",
        "$pf-global--FontWeight--normal",
        "400"
      ]
    },
    "c_title_m_md_LineHeight": {
      "name": "--pf-c-title--m-md--LineHeight",
      "value": "1.5",
      "values": [
        "--pf-global--LineHeight--md",
        "$pf-global--LineHeight--md",
        "1.5"
      ]
    },
    "c_title_m_md_FontSize": {
      "name": "--pf-c-title--m-md--FontSize",
      "value": "1rem",
      "values": [
        "--pf-global--FontSize--md",
        "$pf-global--FontSize--md",
        "pf-font-prem(16px)",
        "1rem"
      ]
    },
    "c_title_m_md_FontWeight": {
      "name": "--pf-c-title--m-md--FontWeight",
      "value": "400",
      "values": [
        "--pf-global--FontWeight--normal",
        "$pf-global--FontWeight--normal",
        "400"
      ]
    }
  },
  ".pf-m-overpass-font .pf-c-title": {
    "c_title_m_md_FontWeight": {
      "name": "--pf-c-title--m-md--FontWeight",
      "value": "700",
      "values": [
        "--pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    },
    "c_title_m_lg_FontWeight": {
      "name": "--pf-c-title--m-lg--FontWeight",
      "value": "700",
      "values": [
        "--pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--semi-bold",
        "$pf-global--FontWeight--bold",
        "700"
      ]
    }
  }
};
export default c_title;
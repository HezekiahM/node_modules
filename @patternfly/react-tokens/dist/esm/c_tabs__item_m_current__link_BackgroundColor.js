export const c_tabs__item_m_current__link_BackgroundColor = {
  "name": "--pf-c-tabs__item--m-current__link--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-tabs__item--m-current__link--BackgroundColor)"
};
export default c_tabs__item_m_current__link_BackgroundColor;
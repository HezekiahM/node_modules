export const c_table_m_compact__action_responsive_MarginBottom = {
  "name": "--pf-c-table--m-compact__action--responsive--MarginBottom",
  "value": "calc(0.25rem * -1)",
  "var": "var(--pf-c-table--m-compact__action--responsive--MarginBottom)"
};
export default c_table_m_compact__action_responsive_MarginBottom;
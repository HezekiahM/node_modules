export const c_table_responsive_BorderColor = {
  "name": "--pf-c-table--responsive--BorderColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-table--responsive--BorderColor)"
};
export default c_table_responsive_BorderColor;
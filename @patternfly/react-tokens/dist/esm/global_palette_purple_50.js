export const global_palette_purple_50 = {
  "name": "--pf-global--palette--purple-50",
  "value": "#f2f0fc",
  "var": "var(--pf-global--palette--purple-50)"
};
export default global_palette_purple_50;
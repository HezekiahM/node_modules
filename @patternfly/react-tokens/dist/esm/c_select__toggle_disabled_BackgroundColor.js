export const c_select__toggle_disabled_BackgroundColor = {
  "name": "--pf-c-select__toggle--disabled--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-select__toggle--disabled--BackgroundColor)"
};
export default c_select__toggle_disabled_BackgroundColor;
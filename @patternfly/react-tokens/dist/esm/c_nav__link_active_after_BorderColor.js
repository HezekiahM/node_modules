export const c_nav__link_active_after_BorderColor = {
  "name": "--pf-c-nav__link--active--after--BorderColor",
  "value": "transparent",
  "var": "var(--pf-c-nav__link--active--after--BorderColor)"
};
export default c_nav__link_active_after_BorderColor;
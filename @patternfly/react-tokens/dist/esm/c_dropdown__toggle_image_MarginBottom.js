export const c_dropdown__toggle_image_MarginBottom = {
  "name": "--pf-c-dropdown__toggle-image--MarginBottom",
  "value": "0.25rem",
  "var": "var(--pf-c-dropdown__toggle-image--MarginBottom)"
};
export default c_dropdown__toggle_image_MarginBottom;
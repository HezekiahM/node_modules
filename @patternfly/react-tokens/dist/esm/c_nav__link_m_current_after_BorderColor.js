export const c_nav__link_m_current_after_BorderColor = {
  "name": "--pf-c-nav__link--m-current--after--BorderColor",
  "value": "#2b9af3",
  "var": "var(--pf-c-nav__link--m-current--after--BorderColor)"
};
export default c_nav__link_m_current_after_BorderColor;
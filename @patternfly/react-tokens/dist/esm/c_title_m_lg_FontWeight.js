export const c_title_m_lg_FontWeight = {
  "name": "--pf-c-title--m-lg--FontWeight",
  "value": "700",
  "var": "var(--pf-c-title--m-lg--FontWeight)"
};
export default c_title_m_lg_FontWeight;
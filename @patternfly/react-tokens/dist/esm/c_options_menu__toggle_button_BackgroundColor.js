export const c_options_menu__toggle_button_BackgroundColor = {
  "name": "--pf-c-options-menu__toggle-button--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-options-menu__toggle-button--BackgroundColor)"
};
export default c_options_menu__toggle_button_BackgroundColor;
export const c_button_hover_after_BorderWidth = {
  "name": "--pf-c-button--hover--after--BorderWidth",
  "value": "2px",
  "var": "var(--pf-c-button--hover--after--BorderWidth)"
};
export default c_button_hover_after_BorderWidth;
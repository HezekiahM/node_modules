export const c_nav_m_light__link_hover_BackgroundColor = {
  "name": "--pf-c-nav--m-light__link--hover--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-nav--m-light__link--hover--BackgroundColor)"
};
export default c_nav_m_light__link_hover_BackgroundColor;
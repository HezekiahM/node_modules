export const c_description_list_m_horizontal__description_width = {
  "name": "--pf-c-description-list--m-horizontal__description--width",
  "value": "minmax(10ch, auto)",
  "var": "var(--pf-c-description-list--m-horizontal__description--width)"
};
export default c_description_list_m_horizontal__description_width;
export const c_nav__scroll_button_before_BorderWidth = {
  "name": "--pf-c-nav__scroll-button--before--BorderWidth",
  "value": "1px",
  "var": "var(--pf-c-nav__scroll-button--before--BorderWidth)"
};
export default c_nav__scroll_button_before_BorderWidth;
export const c_accordion__toggle_focus_BackgroundColor = {
  "name": "--pf-c-accordion__toggle--focus--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-accordion__toggle--focus--BackgroundColor)"
};
export default c_accordion__toggle_focus_BackgroundColor;
export const c_popover__arrow_m_bottom_TranslateX = {
  "name": "--pf-c-popover__arrow--m-bottom--TranslateX",
  "value": "-50%",
  "var": "var(--pf-c-popover__arrow--m-bottom--TranslateX)"
};
export default c_popover__arrow_m_bottom_TranslateX;
export const c_radio__input_first_child_MarginLeft = {
  "name": "--pf-c-radio__input--first-child--MarginLeft",
  "value": "0.0625rem",
  "var": "var(--pf-c-radio__input--first-child--MarginLeft)"
};
export default c_radio__input_first_child_MarginLeft;
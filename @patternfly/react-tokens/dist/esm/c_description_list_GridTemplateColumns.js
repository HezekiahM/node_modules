export const c_description_list_GridTemplateColumns = {
  "name": "--pf-c-description-list--GridTemplateColumns",
  "value": "repeat(1, 1fr)",
  "var": "var(--pf-c-description-list--GridTemplateColumns)"
};
export default c_description_list_GridTemplateColumns;
export const c_content_h4_FontWeight = {
  "name": "--pf-c-content--h4--FontWeight",
  "value": "700",
  "var": "var(--pf-c-content--h4--FontWeight)"
};
export default c_content_h4_FontWeight;
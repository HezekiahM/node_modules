export const c_nav__scroll_button_Color = {
  "name": "--pf-c-nav__scroll-button--Color",
  "value": "#151515",
  "var": "var(--pf-c-nav__scroll-button--Color)"
};
export default c_nav__scroll_button_Color;
export const c_options_menu__toggle_disabled_BackgroundColor = {
  "name": "--pf-c-options-menu__toggle--disabled--BackgroundColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-options-menu__toggle--disabled--BackgroundColor)"
};
export default c_options_menu__toggle_disabled_BackgroundColor;
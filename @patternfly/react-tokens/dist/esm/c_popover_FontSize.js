export const c_popover_FontSize = {
  "name": "--pf-c-popover--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-popover--FontSize)"
};
export default c_popover_FontSize;
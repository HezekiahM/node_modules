export const c_toolbar__expandable_content_PaddingLeft = {
  "name": "--pf-c-toolbar__expandable-content--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-toolbar__expandable-content--PaddingLeft)"
};
export default c_toolbar__expandable_content_PaddingLeft;
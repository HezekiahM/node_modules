export const c_tabs__link_BackgroundColor = {
  "name": "--pf-c-tabs__link--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-tabs__link--BackgroundColor)"
};
export default c_tabs__link_BackgroundColor;
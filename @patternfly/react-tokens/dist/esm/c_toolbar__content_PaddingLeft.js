export const c_toolbar__content_PaddingLeft = {
  "name": "--pf-c-toolbar__content--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-toolbar__content--PaddingLeft)"
};
export default c_toolbar__content_PaddingLeft;
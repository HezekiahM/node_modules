export const c_wizard__footer_xl_PaddingBottom = {
  "name": "--pf-c-wizard__footer--xl--PaddingBottom",
  "value": "1rem",
  "var": "var(--pf-c-wizard__footer--xl--PaddingBottom)"
};
export default c_wizard__footer_xl_PaddingBottom;
export const c_select__menu_item_description_FontSize = {
  "name": "--pf-c-select__menu-item-description--FontSize",
  "value": "0.75rem",
  "var": "var(--pf-c-select__menu-item-description--FontSize)"
};
export default c_select__menu_item_description_FontSize;
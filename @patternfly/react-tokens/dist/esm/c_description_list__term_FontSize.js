export const c_description_list__term_FontSize = {
  "name": "--pf-c-description-list__term--FontSize",
  "value": "0.875rem",
  "var": "var(--pf-c-description-list__term--FontSize)"
};
export default c_description_list__term_FontSize;
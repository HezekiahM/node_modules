export const c_empty_state_PaddingLeft = {
  "name": "--pf-c-empty-state--PaddingLeft",
  "value": "2rem",
  "var": "var(--pf-c-empty-state--PaddingLeft)"
};
export default c_empty_state_PaddingLeft;
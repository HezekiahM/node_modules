export const c_button_PaddingRight = {
  "name": "--pf-c-button--PaddingRight",
  "value": "0.5rem",
  "var": "var(--pf-c-button--PaddingRight)"
};
export default c_button_PaddingRight;
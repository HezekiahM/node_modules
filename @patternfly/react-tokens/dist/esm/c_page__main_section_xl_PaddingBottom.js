export const c_page__main_section_xl_PaddingBottom = {
  "name": "--pf-c-page__main-section--xl--PaddingBottom",
  "value": "1.5rem",
  "var": "var(--pf-c-page__main-section--xl--PaddingBottom)"
};
export default c_page__main_section_xl_PaddingBottom;
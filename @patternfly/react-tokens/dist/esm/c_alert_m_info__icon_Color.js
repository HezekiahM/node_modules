export const c_alert_m_info__icon_Color = {
  "name": "--pf-c-alert--m-info__icon--Color",
  "value": "#2b9af3",
  "var": "var(--pf-c-alert--m-info__icon--Color)"
};
export default c_alert_m_info__icon_Color;
export const c_empty_state_PaddingBottom = {
  "name": "--pf-c-empty-state--PaddingBottom",
  "value": "2rem",
  "var": "var(--pf-c-empty-state--PaddingBottom)"
};
export default c_empty_state_PaddingBottom;
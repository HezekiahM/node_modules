export const c_chip_group__list_MarginBottom = {
  "name": "--pf-c-chip-group__list--MarginBottom",
  "value": "calc(0.25rem * -1)",
  "var": "var(--pf-c-chip-group__list--MarginBottom)"
};
export default c_chip_group__list_MarginBottom;
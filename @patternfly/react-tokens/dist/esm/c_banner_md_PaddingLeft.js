export const c_banner_md_PaddingLeft = {
  "name": "--pf-c-banner--md--PaddingLeft",
  "value": "1.5rem",
  "var": "var(--pf-c-banner--md--PaddingLeft)"
};
export default c_banner_md_PaddingLeft;
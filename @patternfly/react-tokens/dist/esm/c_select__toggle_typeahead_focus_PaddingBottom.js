export const c_select__toggle_typeahead_focus_PaddingBottom = {
  "name": "--pf-c-select__toggle-typeahead--focus--PaddingBottom",
  "value": "calc(0.375rem - 1px)",
  "var": "var(--pf-c-select__toggle-typeahead--focus--PaddingBottom)"
};
export default c_select__toggle_typeahead_focus_PaddingBottom;
export const c_nav__link_PaddingRight = {
  "name": "--pf-c-nav__link--PaddingRight",
  "value": "1rem",
  "var": "var(--pf-c-nav__link--PaddingRight)"
};
export default c_nav__link_PaddingRight;
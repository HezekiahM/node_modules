export const c_tile__icon_FontSize = {
  "name": "--pf-c-tile__icon--FontSize",
  "value": "1.5rem",
  "var": "var(--pf-c-tile__icon--FontSize)"
};
export default c_tile__icon_FontSize;
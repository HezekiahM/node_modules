export const c_breadcrumb__item_divider_MarginRight = {
  "name": "--pf-c-breadcrumb__item-divider--MarginRight",
  "value": "0.5rem",
  "var": "var(--pf-c-breadcrumb__item-divider--MarginRight)"
};
export default c_breadcrumb__item_divider_MarginRight;
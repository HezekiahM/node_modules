export const c_modal_box__body_PaddingTop = {
  "name": "--pf-c-modal-box__body--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-modal-box__body--PaddingTop)"
};
export default c_modal_box__body_PaddingTop;
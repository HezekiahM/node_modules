export const c_nav__section__link_FontSize = {
  "name": "--pf-c-nav__section__link--FontSize",
  "value": "1rem",
  "var": "var(--pf-c-nav__section__link--FontSize)"
};
export default c_nav__section__link_FontSize;
export const c_wizard__nav_list_xl_PaddingTop = {
  "name": "--pf-c-wizard__nav-list--xl--PaddingTop",
  "value": "1.5rem",
  "var": "var(--pf-c-wizard__nav-list--xl--PaddingTop)"
};
export default c_wizard__nav_list_xl_PaddingTop;
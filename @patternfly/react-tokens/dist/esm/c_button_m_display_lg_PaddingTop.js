export const c_button_m_display_lg_PaddingTop = {
  "name": "--pf-c-button--m-display-lg--PaddingTop",
  "value": "1rem",
  "var": "var(--pf-c-button--m-display-lg--PaddingTop)"
};
export default c_button_m_display_lg_PaddingTop;
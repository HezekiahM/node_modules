export const c_label_m_red__content_link_focus_before_BorderColor = {
  "name": "--pf-c-label--m-red__content--link--focus--before--BorderColor",
  "value": "#c9190b",
  "var": "var(--pf-c-label--m-red__content--link--focus--before--BorderColor)"
};
export default c_label_m_red__content_link_focus_before_BorderColor;
export const c_button__icon_m_end_MarginLeft = {
  "name": "--pf-c-button__icon--m-end--MarginLeft",
  "value": "0.25rem",
  "var": "var(--pf-c-button__icon--m-end--MarginLeft)"
};
export default c_button__icon_m_end_MarginLeft;
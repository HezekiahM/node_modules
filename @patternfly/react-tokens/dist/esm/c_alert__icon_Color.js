export const c_alert__icon_Color = {
  "name": "--pf-c-alert__icon--Color",
  "value": "#2b9af3",
  "var": "var(--pf-c-alert__icon--Color)"
};
export default c_alert__icon_Color;
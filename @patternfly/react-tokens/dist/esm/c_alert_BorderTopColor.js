export const c_alert_BorderTopColor = {
  "name": "--pf-c-alert--BorderTopColor",
  "value": "#2b9af3",
  "var": "var(--pf-c-alert--BorderTopColor)"
};
export default c_alert_BorderTopColor;
export const c_tooltip__arrow_m_right_TranslateY = {
  "name": "--pf-c-tooltip__arrow--m-right--TranslateY",
  "value": "-50%",
  "var": "var(--pf-c-tooltip__arrow--m-right--TranslateY)"
};
export default c_tooltip__arrow_m_right_TranslateY;
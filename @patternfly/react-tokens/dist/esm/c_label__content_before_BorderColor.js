export const c_label__content_before_BorderColor = {
  "name": "--pf-c-label__content--before--BorderColor",
  "value": "#8a8d90",
  "var": "var(--pf-c-label__content--before--BorderColor)"
};
export default c_label__content_before_BorderColor;
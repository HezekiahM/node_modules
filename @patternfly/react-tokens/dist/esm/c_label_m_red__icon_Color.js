export const c_label_m_red__icon_Color = {
  "name": "--pf-c-label--m-red__icon--Color",
  "value": "#c9190b",
  "var": "var(--pf-c-label--m-red__icon--Color)"
};
export default c_label_m_red__icon_Color;
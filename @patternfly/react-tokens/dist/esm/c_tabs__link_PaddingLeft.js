export const c_tabs__link_PaddingLeft = {
  "name": "--pf-c-tabs__link--PaddingLeft",
  "value": "1rem",
  "var": "var(--pf-c-tabs__link--PaddingLeft)"
};
export default c_tabs__link_PaddingLeft;
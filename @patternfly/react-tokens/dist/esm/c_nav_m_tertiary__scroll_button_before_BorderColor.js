export const c_nav_m_tertiary__scroll_button_before_BorderColor = {
  "name": "--pf-c-nav--m-tertiary__scroll-button--before--BorderColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-nav--m-tertiary__scroll-button--before--BorderColor)"
};
export default c_nav_m_tertiary__scroll_button_before_BorderColor;
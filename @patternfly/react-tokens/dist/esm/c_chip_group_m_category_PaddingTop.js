export const c_chip_group_m_category_PaddingTop = {
  "name": "--pf-c-chip-group--m-category--PaddingTop",
  "value": "0.25rem",
  "var": "var(--pf-c-chip-group--m-category--PaddingTop)"
};
export default c_chip_group_m_category_PaddingTop;
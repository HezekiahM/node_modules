export const c_table__compound_expansion_toggle__button_after_border_width_base = {
  "name": "--pf-c-table__compound-expansion-toggle__button--after--border-width--base",
  "value": "3px",
  "var": "var(--pf-c-table__compound-expansion-toggle__button--after--border-width--base)"
};
export default c_table__compound_expansion_toggle__button_after_border_width_base;
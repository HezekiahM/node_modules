export const c_form_control_BorderBottomColor = {
  "name": "--pf-c-form-control--BorderBottomColor",
  "value": "#f0ab00",
  "var": "var(--pf-c-form-control--BorderBottomColor)"
};
export default c_form_control_BorderBottomColor;
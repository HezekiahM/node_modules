export const c_toolbar__expandable_content_BackgroundColor = {
  "name": "--pf-c-toolbar__expandable-content--BackgroundColor",
  "value": "#fff",
  "var": "var(--pf-c-toolbar__expandable-content--BackgroundColor)"
};
export default c_toolbar__expandable_content_BackgroundColor;
export const c_label_m_purple__content_Color = {
  "name": "--pf-c-label--m-purple__content--Color",
  "value": "#1f0066",
  "var": "var(--pf-c-label--m-purple__content--Color)"
};
export default c_label_m_purple__content_Color;
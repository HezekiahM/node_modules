export const c_wizard__nav_list_xl_PaddingLeft = {
  "name": "--pf-c-wizard__nav-list--xl--PaddingLeft",
  "value": "calc(1.5rem + 1.5rem + 0.5rem)",
  "var": "var(--pf-c-wizard__nav-list--xl--PaddingLeft)"
};
export default c_wizard__nav_list_xl_PaddingLeft;
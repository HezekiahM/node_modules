export const c_popover_c_button_Top = {
  "name": "--pf-c-popover--c-button--Top",
  "value": "calc(1rem - 0.375rem)",
  "var": "var(--pf-c-popover--c-button--Top)"
};
export default c_popover_c_button_Top;
export const c_nav__link_m_current_BackgroundColor = {
  "name": "--pf-c-nav__link--m-current--BackgroundColor",
  "value": "transparent",
  "var": "var(--pf-c-nav__link--m-current--BackgroundColor)"
};
export default c_nav__link_m_current_BackgroundColor;
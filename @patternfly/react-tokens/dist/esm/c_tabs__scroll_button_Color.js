export const c_tabs__scroll_button_Color = {
  "name": "--pf-c-tabs__scroll-button--Color",
  "value": "#d2d2d2",
  "var": "var(--pf-c-tabs__scroll-button--Color)"
};
export default c_tabs__scroll_button_Color;
export const c_page__main_nav_xl_PaddingLeft = {
  "name": "--pf-c-page__main-nav--xl--PaddingLeft",
  "value": "0.5rem",
  "var": "var(--pf-c-page__main-nav--xl--PaddingLeft)"
};
export default c_page__main_nav_xl_PaddingLeft;
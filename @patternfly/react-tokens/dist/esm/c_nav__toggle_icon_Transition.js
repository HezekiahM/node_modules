export const c_nav__toggle_icon_Transition = {
  "name": "--pf-c-nav__toggle-icon--Transition",
  "value": "250ms",
  "var": "var(--pf-c-nav__toggle-icon--Transition)"
};
export default c_nav__toggle_icon_Transition;
export const c_dropdown__toggle_before_BorderLeftColor = {
  "name": "--pf-c-dropdown__toggle--before--BorderLeftColor",
  "value": "#f0f0f0",
  "var": "var(--pf-c-dropdown__toggle--before--BorderLeftColor)"
};
export default c_dropdown__toggle_before_BorderLeftColor;
export const c_form_control_focus_PaddingBottom = {
  "name": "--pf-c-form-control--focus--PaddingBottom",
  "value": "calc(0.375rem - 1px)",
  "var": "var(--pf-c-form-control--focus--PaddingBottom)"
};
export default c_form_control_focus_PaddingBottom;
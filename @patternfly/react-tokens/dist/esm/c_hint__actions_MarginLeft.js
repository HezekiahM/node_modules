export const c_hint__actions_MarginLeft = {
  "name": "--pf-c-hint__actions--MarginLeft",
  "value": "3rem",
  "var": "var(--pf-c-hint__actions--MarginLeft)"
};
export default c_hint__actions_MarginLeft;
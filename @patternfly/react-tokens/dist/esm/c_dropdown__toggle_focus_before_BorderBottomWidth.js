export const c_dropdown__toggle_focus_before_BorderBottomWidth = {
  "name": "--pf-c-dropdown__toggle--focus--before--BorderBottomWidth",
  "value": "2px",
  "var": "var(--pf-c-dropdown__toggle--focus--before--BorderBottomWidth)"
};
export default c_dropdown__toggle_focus_before_BorderBottomWidth;
export const c_page__header_tools_xl_MarginRight = {
  "name": "--pf-c-page__header-tools--xl--MarginRight",
  "value": "1.5rem",
  "var": "var(--pf-c-page__header-tools--xl--MarginRight)"
};
export default c_page__header_tools_xl_MarginRight;
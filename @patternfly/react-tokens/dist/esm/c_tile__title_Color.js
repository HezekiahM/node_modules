export const c_tile__title_Color = {
  "name": "--pf-c-tile__title--Color",
  "value": "#6a6e73",
  "var": "var(--pf-c-tile__title--Color)"
};
export default c_tile__title_Color;
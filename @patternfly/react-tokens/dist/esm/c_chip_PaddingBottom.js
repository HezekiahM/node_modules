export const c_chip_PaddingBottom = {
  "name": "--pf-c-chip--PaddingBottom",
  "value": "0.25rem",
  "var": "var(--pf-c-chip--PaddingBottom)"
};
export default c_chip_PaddingBottom;
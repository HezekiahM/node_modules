export const c_label__c_button_MarginRight = {
  "name": "--pf-c-label__c-button--MarginRight",
  "value": "calc(0.375rem * -1)",
  "var": "var(--pf-c-label__c-button--MarginRight)"
};
export default c_label__c_button_MarginRight;
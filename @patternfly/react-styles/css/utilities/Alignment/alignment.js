"use strict";
exports.__esModule = true;
require('./alignment.css');
exports.default = {
  "textAlignCenter": "pf-u-text-align-center",
  "textAlignCenterOnLg": "pf-u-text-align-center-on-lg",
  "textAlignCenterOnMd": "pf-u-text-align-center-on-md",
  "textAlignCenterOnSm": "pf-u-text-align-center-on-sm",
  "textAlignCenterOnXl": "pf-u-text-align-center-on-xl",
  "textAlignCenterOn_2xl": "pf-u-text-align-center-on-2xl",
  "textAlignJustify": "pf-u-text-align-justify",
  "textAlignJustifyOnLg": "pf-u-text-align-justify-on-lg",
  "textAlignJustifyOnMd": "pf-u-text-align-justify-on-md",
  "textAlignJustifyOnSm": "pf-u-text-align-justify-on-sm",
  "textAlignJustifyOnXl": "pf-u-text-align-justify-on-xl",
  "textAlignJustifyOn_2xl": "pf-u-text-align-justify-on-2xl",
  "textAlignLeft": "pf-u-text-align-left",
  "textAlignLeftOnLg": "pf-u-text-align-left-on-lg",
  "textAlignLeftOnMd": "pf-u-text-align-left-on-md",
  "textAlignLeftOnSm": "pf-u-text-align-left-on-sm",
  "textAlignLeftOnXl": "pf-u-text-align-left-on-xl",
  "textAlignLeftOn_2xl": "pf-u-text-align-left-on-2xl",
  "textAlignRight": "pf-u-text-align-right",
  "textAlignRightOnLg": "pf-u-text-align-right-on-lg",
  "textAlignRightOnMd": "pf-u-text-align-right-on-md",
  "textAlignRightOnSm": "pf-u-text-align-right-on-sm",
  "textAlignRightOnXl": "pf-u-text-align-right-on-xl",
  "textAlignRightOn_2xl": "pf-u-text-align-right-on-2xl"
};
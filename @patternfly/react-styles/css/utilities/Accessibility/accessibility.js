"use strict";
exports.__esModule = true;
require('./accessibility.css');
exports.default = {
  "hidden": "pf-u-hidden",
  "hiddenOnLg": "pf-u-hidden-on-lg",
  "hiddenOnMd": "pf-u-hidden-on-md",
  "hiddenOnSm": "pf-u-hidden-on-sm",
  "hiddenOnXl": "pf-u-hidden-on-xl",
  "hiddenOn_2xl": "pf-u-hidden-on-2xl",
  "screenReader": "pf-u-screen-reader",
  "screenReaderOnLg": "pf-u-screen-reader-on-lg",
  "screenReaderOnMd": "pf-u-screen-reader-on-md",
  "screenReaderOnSm": "pf-u-screen-reader-on-sm",
  "screenReaderOnXl": "pf-u-screen-reader-on-xl",
  "screenReaderOn_2xl": "pf-u-screen-reader-on-2xl",
  "visible": "pf-u-visible",
  "visibleOnLg": "pf-u-visible-on-lg",
  "visibleOnMd": "pf-u-visible-on-md",
  "visibleOnSm": "pf-u-visible-on-sm",
  "visibleOnXl": "pf-u-visible-on-xl",
  "visibleOn_2xl": "pf-u-visible-on-2xl"
};
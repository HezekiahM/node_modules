"use strict";
exports.__esModule = true;
require('./box-shadow.css');
exports.default = {
  "boxShadowInset": "pf-u-box-shadow-inset",
  "boxShadowLg": "pf-u-box-shadow-lg",
  "boxShadowLgBottom": "pf-u-box-shadow-lg-bottom",
  "boxShadowLgLeft": "pf-u-box-shadow-lg-left",
  "boxShadowLgRight": "pf-u-box-shadow-lg-right",
  "boxShadowLgTop": "pf-u-box-shadow-lg-top",
  "boxShadowMd": "pf-u-box-shadow-md",
  "boxShadowMdBottom": "pf-u-box-shadow-md-bottom",
  "boxShadowMdLeft": "pf-u-box-shadow-md-left",
  "boxShadowMdRight": "pf-u-box-shadow-md-right",
  "boxShadowMdTop": "pf-u-box-shadow-md-top",
  "boxShadowSm": "pf-u-box-shadow-sm",
  "boxShadowSmBottom": "pf-u-box-shadow-sm-bottom",
  "boxShadowSmLeft": "pf-u-box-shadow-sm-left",
  "boxShadowSmRight": "pf-u-box-shadow-sm-right",
  "boxShadowSmTop": "pf-u-box-shadow-sm-top",
  "boxShadowXl": "pf-u-box-shadow-xl",
  "boxShadowXlBottom": "pf-u-box-shadow-xl-bottom",
  "boxShadowXlLeft": "pf-u-box-shadow-xl-left",
  "boxShadowXlRight": "pf-u-box-shadow-xl-right",
  "boxShadowXlTop": "pf-u-box-shadow-xl-top"
};
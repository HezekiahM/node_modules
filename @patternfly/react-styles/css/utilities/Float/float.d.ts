import './float.css';
declare const _default: {
  "floatLeft": "pf-u-float-left",
  "floatLeftOnLg": "pf-u-float-left-on-lg",
  "floatLeftOnMd": "pf-u-float-left-on-md",
  "floatLeftOnSm": "pf-u-float-left-on-sm",
  "floatLeftOnXl": "pf-u-float-left-on-xl",
  "floatLeftOn_2xl": "pf-u-float-left-on-2xl",
  "floatRight": "pf-u-float-right",
  "floatRightOnLg": "pf-u-float-right-on-lg",
  "floatRightOnMd": "pf-u-float-right-on-md",
  "floatRightOnSm": "pf-u-float-right-on-sm",
  "floatRightOnXl": "pf-u-float-right-on-xl",
  "floatRightOn_2xl": "pf-u-float-right-on-2xl"
};
export default _default;
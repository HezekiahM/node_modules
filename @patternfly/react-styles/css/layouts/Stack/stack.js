"use strict";
exports.__esModule = true;
require('./stack.css');
exports.default = {
  "modifiers": {
    "fill": "pf-m-fill",
    "gutter": "pf-m-gutter"
  },
  "stack": "pf-l-stack",
  "stackItem": "pf-l-stack__item"
};
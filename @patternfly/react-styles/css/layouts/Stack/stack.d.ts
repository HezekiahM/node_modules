import './stack.css';
declare const _default: {
  "modifiers": {
    "fill": "pf-m-fill",
    "gutter": "pf-m-gutter"
  },
  "stack": "pf-l-stack",
  "stackItem": "pf-l-stack__item"
};
export default _default;
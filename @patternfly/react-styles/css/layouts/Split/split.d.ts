import './split.css';
declare const _default: {
  "modifiers": {
    "fill": "pf-m-fill",
    "gutter": "pf-m-gutter"
  },
  "split": "pf-l-split",
  "splitItem": "pf-l-split__item"
};
export default _default;
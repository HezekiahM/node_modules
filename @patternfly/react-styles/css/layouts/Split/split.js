"use strict";
exports.__esModule = true;
require('./split.css');
exports.default = {
  "modifiers": {
    "fill": "pf-m-fill",
    "gutter": "pf-m-gutter"
  },
  "split": "pf-l-split",
  "splitItem": "pf-l-split__item"
};
import './patternfly-themes.css';
declare const _default: {
  "button": "pf-c-button",
  "card": "pf-c-card",
  "modifiers": {
    "transparent": "pf-m-transparent",
    "transparent_100": "pf-m-transparent-100",
    "transparent_200": "pf-m-transparent-200",
    "opaque_100": "pf-m-opaque-100",
    "opaque_200": "pf-m-opaque-200",
    "opaque_300": "pf-m-opaque-300"
  },
  "tDark": "pf-t-dark",
  "tLight": "pf-t-light"
};
export default _default;
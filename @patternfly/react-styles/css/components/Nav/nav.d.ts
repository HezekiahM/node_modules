import './nav.css';
declare const _default: {
  "divider": "pf-c-divider",
  "modifiers": {
    "horizontal": "pf-m-horizontal",
    "tertiary": "pf-m-tertiary",
    "light": "pf-m-light",
    "scrollable": "pf-m-scrollable",
    "expandable": "pf-m-expandable",
    "current": "pf-m-current",
    "expanded": "pf-m-expanded"
  },
  "nav": "pf-c-nav",
  "navItem": "pf-c-nav__item",
  "navLink": "pf-c-nav__link",
  "navList": "pf-c-nav__list",
  "navScrollButton": "pf-c-nav__scroll-button",
  "navSection": "pf-c-nav__section",
  "navSectionTitle": "pf-c-nav__section-title",
  "navSubnav": "pf-c-nav__subnav",
  "navToggle": "pf-c-nav__toggle",
  "navToggleIcon": "pf-c-nav__toggle-icon"
};
export default _default;
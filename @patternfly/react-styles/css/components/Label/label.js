"use strict";
exports.__esModule = true;
require('./label.css');
exports.default = {
  "button": "pf-c-button",
  "label": "pf-c-label",
  "labelContent": "pf-c-label__content",
  "labelIcon": "pf-c-label__icon",
  "modifiers": {
    "blue": "pf-m-blue",
    "green": "pf-m-green",
    "orange": "pf-m-orange",
    "red": "pf-m-red",
    "purple": "pf-m-purple",
    "cyan": "pf-m-cyan",
    "outline": "pf-m-outline"
  }
};
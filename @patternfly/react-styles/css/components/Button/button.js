"use strict";
exports.__esModule = true;
require('./button.css');
exports.default = {
  "button": "pf-c-button",
  "buttonIcon": "pf-c-button__icon",
  "modifiers": {
    "active": "pf-m-active",
    "block": "pf-m-block",
    "small": "pf-m-small",
    "primary": "pf-m-primary",
    "displayLg": "pf-m-display-lg",
    "secondary": "pf-m-secondary",
    "tertiary": "pf-m-tertiary",
    "link": "pf-m-link",
    "danger": "pf-m-danger",
    "inline": "pf-m-inline",
    "control": "pf-m-control",
    "expanded": "pf-m-expanded",
    "plain": "pf-m-plain",
    "disabled": "pf-m-disabled",
    "ariaDisabled": "pf-m-aria-disabled",
    "start": "pf-m-start",
    "end": "pf-m-end",
    "overpassFont": "pf-m-overpass-font"
  }
};
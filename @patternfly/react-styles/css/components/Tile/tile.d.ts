import './tile.css';
declare const _default: {
  "modifiers": {
    "stacked": "pf-m-stacked",
    "selected": "pf-m-selected",
    "disabled": "pf-m-disabled",
    "displayLg": "pf-m-display-lg"
  },
  "tile": "pf-c-tile",
  "tileBody": "pf-c-tile__body",
  "tileHeader": "pf-c-tile__header",
  "tileIcon": "pf-c-tile__icon",
  "tileTitle": "pf-c-tile__title"
};
export default _default;
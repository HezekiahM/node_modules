"use strict";
exports.__esModule = true;
require('./dropdown.css');
exports.default = {
  "divider": "pf-c-divider",
  "dropdown": "pf-c-dropdown",
  "dropdownGroup": "pf-c-dropdown__group",
  "dropdownGroupTitle": "pf-c-dropdown__group-title",
  "dropdownMenu": "pf-c-dropdown__menu",
  "dropdownMenuItem": "pf-c-dropdown__menu-item",
  "dropdownMenuItemDescription": "pf-c-dropdown__menu-item-description",
  "dropdownMenuItemIcon": "pf-c-dropdown__menu-item-icon",
  "dropdownMenuItemMain": "pf-c-dropdown__menu-item-main",
  "dropdownToggle": "pf-c-dropdown__toggle",
  "dropdownToggleButton": "pf-c-dropdown__toggle-button",
  "dropdownToggleCheck": "pf-c-dropdown__toggle-check",
  "dropdownToggleIcon": "pf-c-dropdown__toggle-icon",
  "dropdownToggleImage": "pf-c-dropdown__toggle-image",
  "dropdownToggleText": "pf-c-dropdown__toggle-text",
  "modifiers": {
    "action": "pf-m-action",
    "disabled": "pf-m-disabled",
    "plain": "pf-m-plain",
    "splitButton": "pf-m-split-button",
    "active": "pf-m-active",
    "expanded": "pf-m-expanded",
    "primary": "pf-m-primary",
    "top": "pf-m-top",
    "alignRight": "pf-m-align-right",
    "icon": "pf-m-icon",
    "description": "pf-m-description",
    "text": "pf-m-text"
  }
};
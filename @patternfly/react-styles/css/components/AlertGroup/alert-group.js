"use strict";
exports.__esModule = true;
require('./alert-group.css');
exports.default = {
  "alertGroup": "pf-c-alert-group",
  "modifiers": {
    "toast": "pf-m-toast"
  }
};
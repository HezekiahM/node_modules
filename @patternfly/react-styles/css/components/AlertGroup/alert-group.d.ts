import './alert-group.css';
declare const _default: {
  "alertGroup": "pf-c-alert-group",
  "modifiers": {
    "toast": "pf-m-toast"
  }
};
export default _default;
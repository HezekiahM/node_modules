"use strict";
exports.__esModule = true;
require('./overflow-menu.css');
exports.default = {
  "divider": "pf-c-divider",
  "modifiers": {
    "buttonGroup": "pf-m-button-group",
    "iconButtonGroup": "pf-m-icon-button-group",
    "vertical": "pf-m-vertical"
  },
  "overflowMenu": "pf-c-overflow-menu",
  "overflowMenuContent": "pf-c-overflow-menu__content",
  "overflowMenuControl": "pf-c-overflow-menu__control",
  "overflowMenuGroup": "pf-c-overflow-menu__group",
  "overflowMenuItem": "pf-c-overflow-menu__item"
};
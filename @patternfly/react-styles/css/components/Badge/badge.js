"use strict";
exports.__esModule = true;
require('./badge.css');
exports.default = {
  "badge": "pf-c-badge",
  "modifiers": {
    "read": "pf-m-read",
    "unread": "pf-m-unread"
  }
};
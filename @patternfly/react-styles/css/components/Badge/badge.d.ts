import './badge.css';
declare const _default: {
  "badge": "pf-c-badge",
  "modifiers": {
    "read": "pf-m-read",
    "unread": "pf-m-unread"
  }
};
export default _default;
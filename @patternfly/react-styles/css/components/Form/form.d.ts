import './form.css';
declare const _default: {
  "form": "pf-c-form",
  "formActions": "pf-c-form__actions",
  "formFieldset": "pf-c-form__fieldset",
  "formGroup": "pf-c-form__group",
  "formGroupControl": "pf-c-form__group-control",
  "formGroupLabel": "pf-c-form__group-label",
  "formGroupLabelHelp": "pf-c-form__group-label-help",
  "formHelperText": "pf-c-form__helper-text",
  "formHelperTextIcon": "pf-c-form__helper-text-icon",
  "formLabel": "pf-c-form__label",
  "formLabelRequired": "pf-c-form__label-required",
  "formLabelText": "pf-c-form__label-text",
  "modifiers": {
    "horizontal": "pf-m-horizontal",
    "alignRight": "pf-m-align-right",
    "noPaddingTop": "pf-m-no-padding-top",
    "action": "pf-m-action",
    "disabled": "pf-m-disabled",
    "inline": "pf-m-inline",
    "error": "pf-m-error",
    "success": "pf-m-success",
    "warning": "pf-m-warning",
    "inactive": "pf-m-inactive",
    "hidden": "pf-m-hidden"
  }
};
export default _default;
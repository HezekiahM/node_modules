"use strict";
exports.__esModule = true;
require('./pagination.css');
exports.default = {
  "button": "pf-c-button",
  "formControl": "pf-c-form-control",
  "modifiers": {
    "bottom": "pf-m-bottom",
    "static": "pf-m-static",
    "first": "pf-m-first",
    "last": "pf-m-last",
    "compact": "pf-m-compact"
  },
  "optionsMenu": "pf-c-options-menu",
  "optionsMenuToggle": "pf-c-options-menu__toggle",
  "pagination": "pf-c-pagination",
  "paginationNav": "pf-c-pagination__nav",
  "paginationNavControl": "pf-c-pagination__nav-control",
  "paginationNavPageSelect": "pf-c-pagination__nav-page-select",
  "paginationTotalItems": "pf-c-pagination__total-items"
};
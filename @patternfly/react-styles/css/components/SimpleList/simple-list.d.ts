import './simple-list.css';
declare const _default: {
  "modifiers": {
    "current": "pf-m-current"
  },
  "simpleList": "pf-c-simple-list",
  "simpleListItemLink": "pf-c-simple-list__item-link",
  "simpleListSection": "pf-c-simple-list__section",
  "simpleListTitle": "pf-c-simple-list__title"
};
export default _default;
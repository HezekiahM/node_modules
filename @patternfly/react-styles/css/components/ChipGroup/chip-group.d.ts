import './chip-group.css';
declare const _default: {
  "chipGroup": "pf-c-chip-group",
  "chipGroupClose": "pf-c-chip-group__close",
  "chipGroupLabel": "pf-c-chip-group__label",
  "chipGroupList": "pf-c-chip-group__list",
  "chipGroupListItem": "pf-c-chip-group__list-item",
  "modifiers": {
    "category": "pf-m-category"
  }
};
export default _default;
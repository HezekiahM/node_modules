"use strict";
exports.__esModule = true;
require('./chip-group.css');
exports.default = {
  "chipGroup": "pf-c-chip-group",
  "chipGroupClose": "pf-c-chip-group__close",
  "chipGroupLabel": "pf-c-chip-group__label",
  "chipGroupList": "pf-c-chip-group__list",
  "chipGroupListItem": "pf-c-chip-group__list-item",
  "modifiers": {
    "category": "pf-m-category"
  }
};
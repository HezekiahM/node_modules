import './about-modal-box.css';
declare const _default: {
  "aboutModalBox": "pf-c-about-modal-box",
  "aboutModalBoxBrand": "pf-c-about-modal-box__brand",
  "aboutModalBoxBrandImage": "pf-c-about-modal-box__brand-image",
  "aboutModalBoxClose": "pf-c-about-modal-box__close",
  "aboutModalBoxContent": "pf-c-about-modal-box__content",
  "aboutModalBoxHeader": "pf-c-about-modal-box__header",
  "aboutModalBoxHero": "pf-c-about-modal-box__hero",
  "aboutModalBoxStrapline": "pf-c-about-modal-box__strapline",
  "button": "pf-c-button",
  "card": "pf-c-card",
  "modifiers": {
    "plain": "pf-m-plain"
  }
};
export default _default;
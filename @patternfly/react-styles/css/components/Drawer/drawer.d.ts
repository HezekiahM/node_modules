import './drawer.css';
declare const _default: {
  "drawer": "pf-c-drawer",
  "drawerActions": "pf-c-drawer__actions",
  "drawerBody": "pf-c-drawer__body",
  "drawerClose": "pf-c-drawer__close",
  "drawerContent": "pf-c-drawer__content",
  "drawerHead": "pf-c-drawer__head",
  "drawerMain": "pf-c-drawer__main",
  "drawerPanel": "pf-c-drawer__panel",
  "drawerSection": "pf-c-drawer__section",
  "modifiers": {
    "inline": "pf-m-inline",
    "noBorder": "pf-m-no-border",
    "static": "pf-m-static",
    "noBackground": "pf-m-no-background",
    "noPadding": "pf-m-no-padding",
    "padding": "pf-m-padding",
    "expanded": "pf-m-expanded",
    "panelLeft": "pf-m-panel-left",
    "width_25": "pf-m-width-25",
    "width_33": "pf-m-width-33",
    "width_50": "pf-m-width-50",
    "width_66": "pf-m-width-66",
    "width_75": "pf-m-width-75",
    "width_100": "pf-m-width-100",
    "width_25OnLg": "pf-m-width-25-on-lg",
    "width_33OnLg": "pf-m-width-33-on-lg",
    "width_50OnLg": "pf-m-width-50-on-lg",
    "width_66OnLg": "pf-m-width-66-on-lg",
    "width_75OnLg": "pf-m-width-75-on-lg",
    "width_100OnLg": "pf-m-width-100-on-lg",
    "width_25OnXl": "pf-m-width-25-on-xl",
    "width_33OnXl": "pf-m-width-33-on-xl",
    "width_50OnXl": "pf-m-width-50-on-xl",
    "width_66OnXl": "pf-m-width-66-on-xl",
    "width_75OnXl": "pf-m-width-75-on-xl",
    "width_100OnXl": "pf-m-width-100-on-xl",
    "width_25On_2xl": "pf-m-width-25-on-2xl",
    "width_33On_2xl": "pf-m-width-33-on-2xl",
    "width_50On_2xl": "pf-m-width-50-on-2xl",
    "width_66On_2xl": "pf-m-width-66-on-2xl",
    "width_75On_2xl": "pf-m-width-75-on-2xl",
    "width_100On_2xl": "pf-m-width-100-on-2xl",
    "inlineOnLg": "pf-m-inline-on-lg",
    "staticOnLg": "pf-m-static-on-lg",
    "inlineOnXl": "pf-m-inline-on-xl",
    "staticOnXl": "pf-m-static-on-xl",
    "inlineOn_2xl": "pf-m-inline-on-2xl",
    "staticOn_2xl": "pf-m-static-on-2xl"
  },
  "pageMain": "pf-c-page__main"
};
export default _default;
import './breadcrumb.css';
declare const _default: {
  "breadcrumb": "pf-c-breadcrumb",
  "breadcrumbHeading": "pf-c-breadcrumb__heading",
  "breadcrumbItem": "pf-c-breadcrumb__item",
  "breadcrumbItemDivider": "pf-c-breadcrumb__item-divider",
  "breadcrumbLink": "pf-c-breadcrumb__link",
  "breadcrumbList": "pf-c-breadcrumb__list",
  "modifiers": {
    "current": "pf-m-current",
    "overpassFont": "pf-m-overpass-font"
  }
};
export default _default;
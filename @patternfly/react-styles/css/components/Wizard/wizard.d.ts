import './wizard.css';
declare const _default: {
  "button": "pf-c-button",
  "card": "pf-c-card",
  "modalBox": "pf-c-modal-box",
  "modifiers": {
    "finished": "pf-m-finished",
    "expanded": "pf-m-expanded",
    "current": "pf-m-current",
    "disabled": "pf-m-disabled",
    "noPadding": "pf-m-no-padding"
  },
  "wizard": "pf-c-wizard",
  "wizardClose": "pf-c-wizard__close",
  "wizardDescription": "pf-c-wizard__description",
  "wizardFooter": "pf-c-wizard__footer",
  "wizardHeader": "pf-c-wizard__header",
  "wizardInnerWrap": "pf-c-wizard__inner-wrap",
  "wizardMain": "pf-c-wizard__main",
  "wizardMainBody": "pf-c-wizard__main-body",
  "wizardNav": "pf-c-wizard__nav",
  "wizardNavItem": "pf-c-wizard__nav-item",
  "wizardNavLink": "pf-c-wizard__nav-link",
  "wizardNavList": "pf-c-wizard__nav-list",
  "wizardOuterWrap": "pf-c-wizard__outer-wrap",
  "wizardTitle": "pf-c-wizard__title",
  "wizardToggle": "pf-c-wizard__toggle",
  "wizardToggleIcon": "pf-c-wizard__toggle-icon",
  "wizardToggleList": "pf-c-wizard__toggle-list",
  "wizardToggleListItem": "pf-c-wizard__toggle-list-item",
  "wizardToggleNum": "pf-c-wizard__toggle-num",
  "wizardToggleSeparator": "pf-c-wizard__toggle-separator"
};
export default _default;
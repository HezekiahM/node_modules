"use strict";
exports.__esModule = true;
require('./check.css');
exports.default = {
  "check": "pf-c-check",
  "checkDescription": "pf-c-check__description",
  "checkInput": "pf-c-check__input",
  "checkLabel": "pf-c-check__label",
  "modifiers": {
    "disabled": "pf-m-disabled"
  }
};
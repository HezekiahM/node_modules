import './check.css';
declare const _default: {
  "check": "pf-c-check",
  "checkDescription": "pf-c-check__description",
  "checkInput": "pf-c-check__input",
  "checkLabel": "pf-c-check__label",
  "modifiers": {
    "disabled": "pf-m-disabled"
  }
};
export default _default;
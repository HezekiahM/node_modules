"use strict";
exports.__esModule = true;
require('./form-control.css');
exports.default = {
  "formControl": "pf-c-form-control",
  "modifiers": {
    "success": "pf-m-success",
    "warning": "pf-m-warning",
    "search": "pf-m-search",
    "resizeVertical": "pf-m-resize-vertical",
    "resizeHorizontal": "pf-m-resize-horizontal"
  }
};
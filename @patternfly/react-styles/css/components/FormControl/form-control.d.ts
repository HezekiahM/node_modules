import './form-control.css';
declare const _default: {
  "formControl": "pf-c-form-control",
  "modifiers": {
    "success": "pf-m-success",
    "warning": "pf-m-warning",
    "search": "pf-m-search",
    "resizeVertical": "pf-m-resize-vertical",
    "resizeHorizontal": "pf-m-resize-horizontal"
  }
};
export default _default;
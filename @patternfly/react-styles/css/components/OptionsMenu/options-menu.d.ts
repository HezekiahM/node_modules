import './options-menu.css';
declare const _default: {
  "divider": "pf-c-divider",
  "modifiers": {
    "plain": "pf-m-plain",
    "text": "pf-m-text",
    "active": "pf-m-active",
    "expanded": "pf-m-expanded",
    "disabled": "pf-m-disabled",
    "top": "pf-m-top",
    "alignRight": "pf-m-align-right"
  },
  "optionsMenu": "pf-c-options-menu",
  "optionsMenuGroup": "pf-c-options-menu__group",
  "optionsMenuGroupTitle": "pf-c-options-menu__group-title",
  "optionsMenuMenu": "pf-c-options-menu__menu",
  "optionsMenuMenuItem": "pf-c-options-menu__menu-item",
  "optionsMenuMenuItemIcon": "pf-c-options-menu__menu-item-icon",
  "optionsMenuToggle": "pf-c-options-menu__toggle",
  "optionsMenuToggleButton": "pf-c-options-menu__toggle-button",
  "optionsMenuToggleButtonIcon": "pf-c-options-menu__toggle-button-icon",
  "optionsMenuToggleIcon": "pf-c-options-menu__toggle-icon",
  "optionsMenuToggleText": "pf-c-options-menu__toggle-text"
};
export default _default;
"use strict";
exports.__esModule = true;
require('./popover.css');
exports.default = {
  "button": "pf-c-button",
  "modifiers": {
    "top": "pf-m-top",
    "bottom": "pf-m-bottom",
    "left": "pf-m-left",
    "right": "pf-m-right"
  },
  "popover": "pf-c-popover",
  "popoverArrow": "pf-c-popover__arrow",
  "popoverBody": "pf-c-popover__body",
  "popoverContent": "pf-c-popover__content",
  "popoverFooter": "pf-c-popover__footer",
  "title": "pf-c-title"
};
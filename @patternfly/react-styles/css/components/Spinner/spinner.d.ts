import './spinner.css';
declare const _default: {
  "modifiers": {
    "sm": "pf-m-sm",
    "md": "pf-m-md",
    "lg": "pf-m-lg",
    "xl": "pf-m-xl"
  },
  "spinner": "pf-c-spinner",
  "spinnerClipper": "pf-c-spinner__clipper",
  "spinnerLeadBall": "pf-c-spinner__lead-ball",
  "spinnerTailBall": "pf-c-spinner__tail-ball"
};
export default _default;
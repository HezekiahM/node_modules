import './hint.css';
declare const _default: {
  "button": "pf-c-button",
  "dropdown": "pf-c-dropdown",
  "dropdownToggle": "pf-c-dropdown__toggle",
  "hint": "pf-c-hint",
  "hintActions": "pf-c-hint__actions",
  "hintBody": "pf-c-hint__body",
  "hintFooter": "pf-c-hint__footer",
  "hintTitle": "pf-c-hint__title",
  "modifiers": {
    "link": "pf-m-link",
    "inline": "pf-m-inline",
    "plain": "pf-m-plain"
  }
};
export default _default;
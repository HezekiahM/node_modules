import './chip.css';
declare const _default: {
  "badge": "pf-c-badge",
  "button": "pf-c-button",
  "chip": "pf-c-chip",
  "chipText": "pf-c-chip__text",
  "modifiers": {
    "overflow": "pf-m-overflow"
  }
};
export default _default;
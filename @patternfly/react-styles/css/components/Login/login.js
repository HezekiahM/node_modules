"use strict";
exports.__esModule = true;
require('./login.css');
exports.default = {
  "brand": "pf-c-brand",
  "button": "pf-c-button",
  "card": "pf-c-card",
  "dropdown": "pf-c-dropdown",
  "list": "pf-c-list",
  "login": "pf-c-login",
  "loginContainer": "pf-c-login__container",
  "loginFooter": "pf-c-login__footer",
  "loginHeader": "pf-c-login__header",
  "loginMain": "pf-c-login__main",
  "loginMainBody": "pf-c-login__main-body",
  "loginMainFooter": "pf-c-login__main-footer",
  "loginMainFooterBand": "pf-c-login__main-footer-band",
  "loginMainFooterLinks": "pf-c-login__main-footer-links",
  "loginMainFooterLinksItem": "pf-c-login__main-footer-links-item",
  "loginMainFooterLinksItemLink": "pf-c-login__main-footer-links-item-link",
  "loginMainHeader": "pf-c-login__main-header",
  "loginMainHeaderDesc": "pf-c-login__main-header-desc",
  "title": "pf-c-title"
};
import './input-group.css';
declare const _default: {
  "formControl": "pf-c-form-control",
  "inputGroup": "pf-c-input-group",
  "inputGroupText": "pf-c-input-group__text"
};
export default _default;
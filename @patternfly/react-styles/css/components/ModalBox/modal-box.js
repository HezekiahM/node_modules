"use strict";
exports.__esModule = true;
require('./modal-box.css');
exports.default = {
  "button": "pf-c-button",
  "modalBox": "pf-c-modal-box",
  "modalBoxBody": "pf-c-modal-box__body",
  "modalBoxDescription": "pf-c-modal-box__description",
  "modalBoxFooter": "pf-c-modal-box__footer",
  "modalBoxHeader": "pf-c-modal-box__header",
  "modalBoxTitle": "pf-c-modal-box__title",
  "modifiers": {
    "sm": "pf-m-sm",
    "md": "pf-m-md",
    "lg": "pf-m-lg"
  }
};
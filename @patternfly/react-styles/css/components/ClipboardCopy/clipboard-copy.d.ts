import './clipboard-copy.css';
declare const _default: {
  "clipboardCopy": "pf-c-clipboard-copy",
  "clipboardCopyExpandableContent": "pf-c-clipboard-copy__expandable-content",
  "clipboardCopyGroup": "pf-c-clipboard-copy__group",
  "clipboardCopyToggleIcon": "pf-c-clipboard-copy__toggle-icon",
  "modifiers": {
    "expanded": "pf-m-expanded"
  }
};
export default _default;
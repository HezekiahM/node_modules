"use strict";
exports.__esModule = true;
require('./clipboard-copy.css');
exports.default = {
  "clipboardCopy": "pf-c-clipboard-copy",
  "clipboardCopyExpandableContent": "pf-c-clipboard-copy__expandable-content",
  "clipboardCopyGroup": "pf-c-clipboard-copy__group",
  "clipboardCopyToggleIcon": "pf-c-clipboard-copy__toggle-icon",
  "modifiers": {
    "expanded": "pf-m-expanded"
  }
};
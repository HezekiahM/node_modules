import './inline-edit.css';
declare const _default: {
  "modifiers": {
    "editing": "pf-m-editing",
    "tableEditingFirstRow": "pf-m-table-editing-first-row",
    "tableEditingLastRow": "pf-m-table-editing-last-row",
    "top": "pf-m-top",
    "bottom": "pf-m-bottom",
    "bold": "pf-m-bold"
  },
  "tableEditableRow": "pf-c-table__editable-row",
  "tableInlineEditButtons": "pf-c-table__inline-edit-buttons"
};
export default _default;
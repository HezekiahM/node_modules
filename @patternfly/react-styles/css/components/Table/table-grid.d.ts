import './table-grid.css';
declare const _default: {
  "button": "pf-c-button",
  "modifiers": {
    "grid": "pf-m-grid",
    "compact": "pf-m-compact",
    "expanded": "pf-m-expanded",
    "noPadding": "pf-m-no-padding",
    "nowrap": "pf-m-nowrap",
    "fitContent": "pf-m-fit-content",
    "truncate": "pf-m-truncate",
    "gridMd": "pf-m-grid-md",
    "gridLg": "pf-m-grid-lg",
    "gridXl": "pf-m-grid-xl",
    "grid_2xl": "pf-m-grid-2xl"
  },
  "table": "pf-c-table",
  "tableAction": "pf-c-table__action",
  "tableButton": "pf-c-table__button",
  "tableCheck": "pf-c-table__check",
  "tableCompoundExpansionToggle": "pf-c-table__compound-expansion-toggle",
  "tableExpandableRow": "pf-c-table__expandable-row",
  "tableExpandableRowContent": "pf-c-table__expandable-row-content",
  "tableIcon": "pf-c-table__icon",
  "tableInlineEditAction": "pf-c-table__inline-edit-action",
  "tableText": "pf-c-table__text",
  "tableToggle": "pf-c-table__toggle",
  "tableToggleIcon": "pf-c-table__toggle-icon"
};
export default _default;
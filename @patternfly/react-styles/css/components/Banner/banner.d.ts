import './banner.css';
declare const _default: {
  "banner": "pf-c-banner",
  "button": "pf-c-button",
  "card": "pf-c-card",
  "modifiers": {
    "info": "pf-m-info",
    "warning": "pf-m-warning",
    "danger": "pf-m-danger",
    "success": "pf-m-success",
    "sticky": "pf-m-sticky"
  }
};
export default _default;
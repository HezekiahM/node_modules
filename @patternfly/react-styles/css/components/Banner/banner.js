"use strict";
exports.__esModule = true;
require('./banner.css');
exports.default = {
  "banner": "pf-c-banner",
  "button": "pf-c-button",
  "card": "pf-c-card",
  "modifiers": {
    "info": "pf-m-info",
    "warning": "pf-m-warning",
    "danger": "pf-m-danger",
    "success": "pf-m-success",
    "sticky": "pf-m-sticky"
  }
};
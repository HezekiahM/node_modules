"use strict";
exports.__esModule = true;
require('./description-list.css');
exports.default = {
  "descriptionList": "pf-c-description-list",
  "descriptionListDescription": "pf-c-description-list__description",
  "descriptionListGroup": "pf-c-description-list__group",
  "descriptionListTerm": "pf-c-description-list__term",
  "modifiers": {
    "horizontal": "pf-m-horizontal",
    "inlineGrid": "pf-m-inline-grid",
    "autoColumnWidths": "pf-m-auto-column-widths",
    "1Col": "pf-m-1-col",
    "2Col": "pf-m-2-col",
    "3Col": "pf-m-3-col",
    "1ColOnMd": "pf-m-1-col-on-md",
    "2ColOnMd": "pf-m-2-col-on-md",
    "3ColOnMd": "pf-m-3-col-on-md",
    "1ColOnLg": "pf-m-1-col-on-lg",
    "2ColOnLg": "pf-m-2-col-on-lg",
    "3ColOnLg": "pf-m-3-col-on-lg",
    "1ColOnXl": "pf-m-1-col-on-xl",
    "2ColOnXl": "pf-m-2-col-on-xl",
    "3ColOnXl": "pf-m-3-col-on-xl",
    "1ColOn_2xl": "pf-m-1-col-on-2xl",
    "2ColOn_2xl": "pf-m-2-col-on-2xl",
    "3ColOn_2xl": "pf-m-3-col-on-2xl"
  }
};
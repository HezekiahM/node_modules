import './notification-badge.css';
declare const _default: {
  "modifiers": {
    "unread": "pf-m-unread",
    "read": "pf-m-read"
  },
  "notificationBadge": "pf-c-notification-badge"
};
export default _default;
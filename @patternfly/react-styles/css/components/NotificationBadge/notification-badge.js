"use strict";
exports.__esModule = true;
require('./notification-badge.css');
exports.default = {
  "modifiers": {
    "unread": "pf-m-unread",
    "read": "pf-m-read"
  },
  "notificationBadge": "pf-c-notification-badge"
};
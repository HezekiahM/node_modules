import './card.css';
declare const _default: {
  "card": "pf-c-card",
  "cardActions": "pf-c-card__actions",
  "cardBody": "pf-c-card__body",
  "cardFooter": "pf-c-card__footer",
  "cardHeader": "pf-c-card__header",
  "cardTitle": "pf-c-card__title",
  "modifiers": {
    "hoverable": "pf-m-hoverable",
    "selectable": "pf-m-selectable",
    "selected": "pf-m-selected",
    "compact": "pf-m-compact",
    "flat": "pf-m-flat",
    "noFill": "pf-m-no-fill",
    "overpassFont": "pf-m-overpass-font"
  }
};
export default _default;
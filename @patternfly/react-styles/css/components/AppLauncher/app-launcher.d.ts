import './app-launcher.css';
declare const _default: {
  "appLauncher": "pf-c-app-launcher",
  "appLauncherGroup": "pf-c-app-launcher__group",
  "appLauncherGroupTitle": "pf-c-app-launcher__group-title",
  "appLauncherMenu": "pf-c-app-launcher__menu",
  "appLauncherMenuItem": "pf-c-app-launcher__menu-item",
  "appLauncherMenuItemExternalIcon": "pf-c-app-launcher__menu-item-external-icon",
  "appLauncherMenuItemIcon": "pf-c-app-launcher__menu-item-icon",
  "appLauncherMenuSearch": "pf-c-app-launcher__menu-search",
  "appLauncherMenuWrapper": "pf-c-app-launcher__menu-wrapper",
  "appLauncherToggle": "pf-c-app-launcher__toggle",
  "divider": "pf-c-divider",
  "modifiers": {
    "expanded": "pf-m-expanded",
    "active": "pf-m-active",
    "alignRight": "pf-m-align-right",
    "top": "pf-m-top",
    "favorite": "pf-m-favorite",
    "disabled": "pf-m-disabled",
    "external": "pf-m-external",
    "link": "pf-m-link",
    "action": "pf-m-action"
  }
};
export default _default;
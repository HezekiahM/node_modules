import './switch.css';
declare const _default: {
  "modifiers": {
    "off": "pf-m-off",
    "on": "pf-m-on"
  },
  "switch": "pf-c-switch",
  "switchInput": "pf-c-switch__input",
  "switchLabel": "pf-c-switch__label",
  "switchToggle": "pf-c-switch__toggle",
  "switchToggleIcon": "pf-c-switch__toggle-icon"
};
export default _default;
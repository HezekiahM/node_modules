"use strict";
exports.__esModule = true;
require('./accordion.css');
exports.default = {
  "accordion": "pf-c-accordion",
  "accordionExpandedContent": "pf-c-accordion__expanded-content",
  "accordionExpandedContentBody": "pf-c-accordion__expanded-content-body",
  "accordionToggle": "pf-c-accordion__toggle",
  "accordionToggleIcon": "pf-c-accordion__toggle-icon",
  "accordionToggleText": "pf-c-accordion__toggle-text",
  "modifiers": {
    "expanded": "pf-m-expanded",
    "fixed": "pf-m-fixed"
  }
};
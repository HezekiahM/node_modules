import './progress.css';
declare const _default: {
  "modifiers": {
    "sm": "pf-m-sm",
    "lg": "pf-m-lg",
    "inside": "pf-m-inside",
    "outside": "pf-m-outside",
    "singleline": "pf-m-singleline",
    "success": "pf-m-success",
    "danger": "pf-m-danger"
  },
  "progress": "pf-c-progress",
  "progressBar": "pf-c-progress__bar",
  "progressDescription": "pf-c-progress__description",
  "progressIndicator": "pf-c-progress__indicator",
  "progressMeasure": "pf-c-progress__measure",
  "progressStatus": "pf-c-progress__status",
  "progressStatusIcon": "pf-c-progress__status-icon"
};
export default _default;
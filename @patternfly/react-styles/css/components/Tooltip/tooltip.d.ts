import './tooltip.css';
declare const _default: {
  "modifiers": {
    "top": "pf-m-top",
    "bottom": "pf-m-bottom",
    "left": "pf-m-left",
    "right": "pf-m-right",
    "textAlignLeft": "pf-m-text-align-left"
  },
  "tooltip": "pf-c-tooltip",
  "tooltipArrow": "pf-c-tooltip__arrow",
  "tooltipContent": "pf-c-tooltip__content"
};
export default _default;
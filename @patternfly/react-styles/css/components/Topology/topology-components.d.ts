import './topology-components.css';
declare const _default: {
  "topologyConnectorArrow": "pf-topology-connector-arrow",
  "topologyContextMenuCDropdownMenu": "pf-topology-context-menu__c-dropdown__menu",
  "topologyContextSubMenu": "pf-topology-context-sub-menu",
  "topologyContextSubMenuArrow": "pf-topology-context-sub-menu__arrow",
  "topologyDefaultCreateConnectorArrow": "pf-topology-default-create-connector__arrow",
  "topologyDefaultCreateConnectorCreateBg": "pf-topology-default-create-connector__create__bg",
  "topologyDefaultCreateConnectorCreateCursor": "pf-topology-default-create-connector__create__cursor",
  "topologyDefaultCreateConnectorLine": "pf-topology-default-create-connector__line",
  "topologyVisualizationSurface": "pf-topology-visualization-surface",
  "topologyVisualizationSurfaceSvg": "pf-topology-visualization-surface__svg"
};
export default _default;
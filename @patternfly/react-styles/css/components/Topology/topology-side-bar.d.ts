import './topology-side-bar.css';
declare const _default: {
  "button": "pf-c-button",
  "in": "in",
  "shown": "shown",
  "topologyContainerWithSidebar": "pf-topology-container__with-sidebar",
  "topologyContainerWithSidebarOpen": "pf-topology-container__with-sidebar--open",
  "topologyContent": "pf-topology-content",
  "topologySideBar": "pf-topology-side-bar",
  "topologySideBarDismiss": "pf-topology-side-bar__dismiss",
  "topologySideBarHeader": "pf-topology-side-bar__header"
};
export default _default;
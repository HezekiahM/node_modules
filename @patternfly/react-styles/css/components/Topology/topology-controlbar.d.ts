import './topology-controlbar.css';
declare const _default: {
  "button": "pf-c-button",
  "modifiers": {
    "tertiary": "pf-m-tertiary"
  },
  "topologyControlBar": "pf-topology-control-bar",
  "topologyControlBarButton": "pf-topology-control-bar__button"
};
export default _default;
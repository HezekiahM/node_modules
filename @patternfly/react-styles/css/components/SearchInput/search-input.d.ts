import './search-input.css';
declare const _default: {
  "button": "pf-c-button",
  "searchInput": "pf-c-search-input",
  "searchInputCount": "pf-c-search-input__count",
  "searchInputIcon": "pf-c-search-input__icon",
  "searchInputNav": "pf-c-search-input__nav",
  "searchInputText": "pf-c-search-input__text",
  "searchInputTextInput": "pf-c-search-input__text-input",
  "searchInputUtilities": "pf-c-search-input__utilities"
};
export default _default;
import './file-upload.css';
declare const _default: {
  "button": "pf-c-button",
  "fileUpload": "pf-c-file-upload",
  "fileUploadFileDetails": "pf-c-file-upload__file-details",
  "fileUploadFileDetailsSpinner": "pf-c-file-upload__file-details-spinner",
  "fileUploadFileSelect": "pf-c-file-upload__file-select",
  "formControl": "pf-c-form-control",
  "modifiers": {
    "dragHover": "pf-m-drag-hover",
    "loading": "pf-m-loading",
    "control": "pf-m-control"
  }
};
export default _default;
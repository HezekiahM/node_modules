"use strict";
exports.__esModule = true;
require('./file-upload.css');
exports.default = {
  "button": "pf-c-button",
  "fileUpload": "pf-c-file-upload",
  "fileUploadFileDetails": "pf-c-file-upload__file-details",
  "fileUploadFileDetailsSpinner": "pf-c-file-upload__file-details-spinner",
  "fileUploadFileSelect": "pf-c-file-upload__file-select",
  "formControl": "pf-c-form-control",
  "modifiers": {
    "dragHover": "pf-m-drag-hover",
    "loading": "pf-m-loading",
    "control": "pf-m-control"
  }
};
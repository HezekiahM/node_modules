"use strict";
exports.__esModule = true;
require('./radio.css');
exports.default = {
  "modifiers": {
    "disabled": "pf-m-disabled"
  },
  "radio": "pf-c-radio",
  "radioDescription": "pf-c-radio__description",
  "radioInput": "pf-c-radio__input",
  "radioLabel": "pf-c-radio__label"
};
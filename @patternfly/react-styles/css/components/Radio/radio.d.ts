import './radio.css';
declare const _default: {
  "modifiers": {
    "disabled": "pf-m-disabled"
  },
  "radio": "pf-c-radio",
  "radioDescription": "pf-c-radio__description",
  "radioInput": "pf-c-radio__input",
  "radioLabel": "pf-c-radio__label"
};
export default _default;
import './backdrop.css';
declare const _default: {
  "backdrop": "pf-c-backdrop",
  "backdropOpen": "pf-c-backdrop__open"
};
export default _default;
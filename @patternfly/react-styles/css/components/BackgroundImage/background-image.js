"use strict";
exports.__esModule = true;
require('./background-image.css');
exports.default = {
  "backgroundImage": "pf-c-background-image",
  "backgroundImageFilter": "pf-c-background-image__filter"
};
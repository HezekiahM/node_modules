import './inline-edit.css';
declare const _default: {
  "button": "pf-c-button",
  "inlineEdit": "pf-c-inline-edit",
  "inlineEditAction": "pf-c-inline-edit__action",
  "inlineEditGroup": "pf-c-inline-edit__group",
  "inlineEditInput": "pf-c-inline-edit__input",
  "inlineEditLabel": "pf-c-inline-edit__label",
  "inlineEditValue": "pf-c-inline-edit__value",
  "modifiers": {
    "iconGroup": "pf-m-icon-group",
    "footer": "pf-m-footer",
    "column": "pf-m-column",
    "valid": "pf-m-valid",
    "plain": "pf-m-plain",
    "actionGroup": "pf-m-action-group",
    "enableEditable": "pf-m-enable-editable",
    "inlineEditable": "pf-m-inline-editable",
    "enable": "pf-m-enable",
    "bold": "pf-m-bold"
  }
};
export default _default;
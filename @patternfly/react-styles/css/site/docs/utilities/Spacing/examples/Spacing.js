"use strict";
exports.__esModule = true;
require('./Spacing.css');
exports.default = {
  "displayFlex": "pf-u-display-flex",
  "wsCoreFlexItem": "ws-core-flex-item",
  "wsCoreUSpacing": "ws-core-u-spacing"
};
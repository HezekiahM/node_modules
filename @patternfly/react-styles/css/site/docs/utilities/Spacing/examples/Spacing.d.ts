import './Spacing.css';
declare const _default: {
  "displayFlex": "pf-u-display-flex",
  "wsCoreFlexItem": "ws-core-flex-item",
  "wsCoreUSpacing": "ws-core-u-spacing"
};
export default _default;
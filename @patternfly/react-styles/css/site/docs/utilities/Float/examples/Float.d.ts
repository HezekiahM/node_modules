import './Float.css';
declare const _default: {
  "floatLeft": "pf-u-float-left",
  "floatRight": "pf-u-float-right",
  "wsCoreUFloat": "ws-core-u-float"
};
export default _default;
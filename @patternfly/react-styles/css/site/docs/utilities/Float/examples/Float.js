"use strict";
exports.__esModule = true;
require('./Float.css');
exports.default = {
  "floatLeft": "pf-u-float-left",
  "floatRight": "pf-u-float-right",
  "wsCoreUFloat": "ws-core-u-float"
};
"use strict";
exports.__esModule = true;
require('./Flex.css');
exports.default = {
  "displayFlex": "pf-u-display-flex",
  "displayInlineFlex": "pf-u-display-inline-flex",
  "wsCoreFlexItem": "ws-core-flex-item",
  "wsCoreUFlex": "ws-core-u-flex",
  "wsCoreUFlexLg": "ws-core-u-flex-lg",
  "wsCoreUFlexMd": "ws-core-u-flex-md"
};
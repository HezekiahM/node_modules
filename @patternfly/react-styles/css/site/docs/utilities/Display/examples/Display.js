"use strict";
exports.__esModule = true;
require('./Display.css');
exports.default = {
  "displayBlock": "pf-u-display-block",
  "displayFlex": "pf-u-display-flex",
  "displayGrid": "pf-u-display-grid",
  "displayInline": "pf-u-display-inline",
  "displayInlineBlock": "pf-u-display-inline-block",
  "displayInlineFlex": "pf-u-display-inline-flex",
  "displayTable": "pf-u-display-table",
  "displayTableCell": "pf-u-display-table-cell",
  "wsCoreUDisplay": "ws-core-u-display"
};
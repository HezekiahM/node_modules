import './Display.css';
declare const _default: {
  "displayBlock": "pf-u-display-block",
  "displayFlex": "pf-u-display-flex",
  "displayGrid": "pf-u-display-grid",
  "displayInline": "pf-u-display-inline",
  "displayInlineBlock": "pf-u-display-inline-block",
  "displayInlineFlex": "pf-u-display-inline-flex",
  "displayTable": "pf-u-display-table",
  "displayTableCell": "pf-u-display-table-cell",
  "wsCoreUDisplay": "ws-core-u-display"
};
export default _default;
"use strict";
exports.__esModule = true;
require('./Alignment.css');
exports.default = {
  "textAlignCenter": "pf-u-text-align-center",
  "textAlignJustify": "pf-u-text-align-justify",
  "textAlignLeft": "pf-u-text-align-left",
  "textAlignRight": "pf-u-text-align-right",
  "wsCoreUAlignment": "ws-core-u-alignment"
};
import './Toolbar.css';
declare const _default: {
  "toolbar": "pf-c-toolbar",
  "toolbarGroup": "pf-c-toolbar__group",
  "toolbarItem": "pf-c-toolbar__item"
};
export default _default;
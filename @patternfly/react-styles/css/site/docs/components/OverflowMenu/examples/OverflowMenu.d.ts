import './OverflowMenu.css';
declare const _default: {
  "overflowMenuGroup": "pf-c-overflow-menu__group",
  "overflowMenuItem": "pf-c-overflow-menu__item"
};
export default _default;
import './Stack.css';
declare const _default: {
  "stack": "pf-l-stack",
  "stackItem": "pf-l-stack__item",
  "wsCoreLStack": "ws-core-l-stack",
  "wsPreviewHtml": "ws-preview-html"
};
export default _default;
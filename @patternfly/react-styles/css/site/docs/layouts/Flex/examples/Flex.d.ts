import './Flex.css';
declare const _default: {
  "flex": "pf-l-flex",
  "flexItem": "pf-l-flex__item",
  "wsCoreLFlex": "ws-core-l-flex",
  "wsCoreLFlexBorder": "ws-core-l-flex-border"
};
export default _default;
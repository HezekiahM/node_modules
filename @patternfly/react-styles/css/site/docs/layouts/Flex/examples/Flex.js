"use strict";
exports.__esModule = true;
require('./Flex.css');
exports.default = {
  "flex": "pf-l-flex",
  "flexItem": "pf-l-flex__item",
  "wsCoreLFlex": "ws-core-l-flex",
  "wsCoreLFlexBorder": "ws-core-l-flex-border"
};
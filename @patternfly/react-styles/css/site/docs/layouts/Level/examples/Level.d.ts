import './Level.css';
declare const _default: {
  "level": "pf-l-level",
  "levelItem": "pf-l-level__item",
  "wsCoreLLevel": "ws-core-l-level",
  "wsPreviewHtml": "ws-preview-html"
};
export default _default;
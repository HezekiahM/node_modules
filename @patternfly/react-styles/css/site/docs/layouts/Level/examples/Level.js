"use strict";
exports.__esModule = true;
require('./Level.css');
exports.default = {
  "level": "pf-l-level",
  "levelItem": "pf-l-level__item",
  "wsCoreLLevel": "ws-core-l-level",
  "wsPreviewHtml": "ws-preview-html"
};
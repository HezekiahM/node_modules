"use strict";
exports.__esModule = true;
require('./Bullseye.css');
exports.default = {
  "bullseye": "pf-l-bullseye",
  "bullseyeItem": "pf-l-bullseye__item",
  "wsCoreLBullseye": "ws-core-l-bullseye"
};
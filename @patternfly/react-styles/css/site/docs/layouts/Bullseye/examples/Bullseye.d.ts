import './Bullseye.css';
declare const _default: {
  "bullseye": "pf-l-bullseye",
  "bullseyeItem": "pf-l-bullseye__item",
  "wsCoreLBullseye": "ws-core-l-bullseye"
};
export default _default;
"use strict";
exports.__esModule = true;
require('./Gallery.css');
exports.default = {
  "gallery": "pf-l-gallery",
  "galleryItem": "pf-l-gallery__item",
  "wsCoreLGallery": "ws-core-l-gallery"
};
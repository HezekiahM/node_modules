import './Gallery.css';
declare const _default: {
  "gallery": "pf-l-gallery",
  "galleryItem": "pf-l-gallery__item",
  "wsCoreLGallery": "ws-core-l-gallery"
};
export default _default;
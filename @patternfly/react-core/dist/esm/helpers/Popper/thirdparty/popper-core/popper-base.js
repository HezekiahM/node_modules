// @ts-nocheck
import { createPopper, popperGenerator, detectOverflow } from '.';
// eslint-disable-next-line import/no-unused-modules
export { createPopper, popperGenerator, detectOverflow };
//# sourceMappingURL=popper-base.js.map
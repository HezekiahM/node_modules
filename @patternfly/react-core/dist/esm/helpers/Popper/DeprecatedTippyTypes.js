/* eslint-disable @typescript-eslint/prefer-function-type */
/**
 * Added types from tippy.js and popper.js to preserve backwards compatibility
 * Can remove in next breaking change release
 */
export default tippy;
export { hideAll, createTippyWithPlugins, delegate, createSingleton, animateFill, followCursor, inlinePositioning, sticky, roundArrow };
//# sourceMappingURL=DeprecatedTippyTypes.js.map
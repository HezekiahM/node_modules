import { __rest } from "tslib";
import * as React from 'react';
import { css } from '@patternfly/react-styles';
import styles from '@patternfly/react-styles/css/components/SimpleList/simple-list';
import { SimpleListContext } from './SimpleList';
export class SimpleListItem extends React.Component {
    constructor() {
        super(...arguments);
        this.ref = React.createRef();
    }
    render() {
        const _a = this.props, { children, isCurrent, className, component: Component, componentClassName, componentProps, onClick, type, href } = _a, props = __rest(_a, ["children", "isCurrent", "className", "component", "componentClassName", "componentProps", "onClick", "type", "href"]);
        return (React.createElement(SimpleListContext.Consumer, null, ({ currentRef, updateCurrentRef }) => {
            const isButton = Component === 'button';
            const isCurrentItem = this.ref && currentRef ? currentRef.current === this.ref.current : isCurrent;
            const additionalComponentProps = isButton
                ? {
                    type
                }
                : {
                    tabIndex: 0,
                    href
                };
            return (React.createElement("li", Object.assign({ className: css(className) }, props),
                React.createElement(Component, Object.assign({ className: css(styles.simpleListItemLink, isCurrentItem && styles.modifiers.current, componentClassName), onClick: (evt) => {
                        onClick(evt);
                        updateCurrentRef(this.ref, this.props);
                    }, ref: this.ref }, componentProps, additionalComponentProps), children)));
        }));
    }
}
SimpleListItem.displayName = 'SimpleListItem';
SimpleListItem.defaultProps = {
    children: null,
    className: '',
    isCurrent: false,
    component: 'button',
    componentClassName: '',
    type: 'button',
    href: '',
    onClick: () => { }
};
//# sourceMappingURL=SimpleListItem.js.map
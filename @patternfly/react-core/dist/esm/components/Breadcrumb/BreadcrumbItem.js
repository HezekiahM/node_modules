import { __rest } from "tslib";
import * as React from 'react';
import AngleRightIcon from '@patternfly/react-icons/dist/js/icons/angle-right-icon';
import styles from '@patternfly/react-styles/css/components/Breadcrumb/breadcrumb';
import { css } from '@patternfly/react-styles';
export const BreadcrumbItem = (_a) => {
    var { children = null, className = '', to = null, isActive = false, showDivider, target = null, component = 'a' } = _a, props = __rest(_a, ["children", "className", "to", "isActive", "showDivider", "target", "component"]);
    const Component = component;
    return (React.createElement("li", Object.assign({}, props, { className: css(styles.breadcrumbItem, className) }),
        showDivider && (React.createElement("span", { className: styles.breadcrumbItemDivider },
            React.createElement(AngleRightIcon, null))),
        to && (React.createElement(Component, { href: to, target: target, className: css(styles.breadcrumbLink, isActive && styles.modifiers.current), "aria-current": isActive ? 'page' : undefined }, children)),
        !to && React.createElement(React.Fragment, null, children)));
};
BreadcrumbItem.displayName = 'BreadcrumbItem';
//# sourceMappingURL=BreadcrumbItem.js.map
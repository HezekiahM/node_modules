import { __rest } from "tslib";
import * as React from 'react';
import { css } from '@patternfly/react-styles';
import styles from '@patternfly/react-styles/css/components/Card/card';
export const CardHeader = (_a) => {
    var { children = null, className = '' } = _a, props = __rest(_a, ["children", "className"]);
    return (React.createElement("div", Object.assign({ className: css(styles.cardHeader, className) }, props), children));
};
CardHeader.displayName = 'CardHeader';
//# sourceMappingURL=CardHeader.js.map
import { __rest } from "tslib";
import * as React from 'react';
import { css } from '@patternfly/react-styles';
export const CardHeadMain = (_a) => {
    var { children = null, className = '' } = _a, props = __rest(_a, ["children", "className"]);
    return (React.createElement("div", Object.assign({ className: css('pf-c-card__head-main', className) }, props), children));
};
CardHeadMain.displayName = 'CardHeadMain';
//# sourceMappingURL=CardHeadMain.js.map
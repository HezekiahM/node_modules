import { __rest } from "tslib";
import * as React from 'react';
import styles from '@patternfly/react-styles/css/components/Card/card';
import { css } from '@patternfly/react-styles';
import { getOUIAProps } from '../../helpers';
export const Card = (_a) => {
    var { children = null, className = '', component = 'article', isHoverable = false, isCompact = false, isSelectable = false, isSelected = false, isFlat = false, ouiaId, ouiaSafe = true } = _a, props = __rest(_a, ["children", "className", "component", "isHoverable", "isCompact", "isSelectable", "isSelected", "isFlat", "ouiaId", "ouiaSafe"]);
    const Component = component;
    return (React.createElement(Component, Object.assign({ className: css(styles.card, isHoverable && styles.modifiers.hoverable, isCompact && styles.modifiers.compact, isSelectable && styles.modifiers.selectable, isSelected && isSelectable && styles.modifiers.selected, isFlat && styles.modifiers.flat, className), tabIndex: isSelectable ? '0' : undefined }, props, getOUIAProps(Card.displayName, ouiaId, ouiaSafe)), children));
};
Card.displayName = 'Card';
//# sourceMappingURL=Card.js.map
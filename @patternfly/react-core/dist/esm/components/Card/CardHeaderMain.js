import { __rest } from "tslib";
import * as React from 'react';
export const CardHeaderMain = (_a) => {
    var { children = null, className = '' } = _a, props = __rest(_a, ["children", "className"]);
    return (React.createElement("div", Object.assign({ className: className }, props), children));
};
CardHeaderMain.displayName = 'CardHeaderMain';
//# sourceMappingURL=CardHeaderMain.js.map
import { __rest } from "tslib";
import * as React from 'react';
import { css } from '@patternfly/react-styles';
import styles from '@patternfly/react-styles/css/components/Card/card';
export const CardTitle = (_a) => {
    var { children = null, className = '' } = _a, props = __rest(_a, ["children", "className"]);
    return (React.createElement("div", Object.assign({ className: css(styles.cardTitle, className) }, props), children));
};
CardTitle.displayName = 'CardTitle';
//# sourceMappingURL=CardTitle.js.map
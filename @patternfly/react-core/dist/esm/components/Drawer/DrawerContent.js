import { __rest } from "tslib";
import * as React from 'react';
import styles from '@patternfly/react-styles/css/components/Drawer/drawer';
import { css } from '@patternfly/react-styles';
import { DrawerMain } from './DrawerMain';
export const DrawerContent = (_a) => {
    var { 
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    className = '', children, panelContent } = _a, props = __rest(_a, ["className", "children", "panelContent"]);
    return (React.createElement(DrawerMain, null,
        React.createElement("div", Object.assign({ className: css(styles.drawerContent, className) }, props), children),
        panelContent));
};
DrawerContent.displayName = 'DrawerContent';
//# sourceMappingURL=DrawerContent.js.map
import { __rest } from "tslib";
import * as React from 'react';
import styles from '@patternfly/react-styles/css/components/Drawer/drawer';
import { css } from '@patternfly/react-styles';
import { DrawerContext } from './Drawer';
import { formatBreakpointMods } from '../../helpers/util';
export const DrawerPanelContent = (_a) => {
    var { className = '', children, hasNoBorder = false, widths } = _a, props = __rest(_a, ["className", "children", "hasNoBorder", "widths"]);
    return (React.createElement(DrawerContext.Consumer, null, ({ isExpanded, isStatic, onExpand }) => {
        const hidden = isStatic ? false : !isExpanded;
        return (React.createElement("div", Object.assign({ className: css(styles.drawerPanel, hasNoBorder && styles.modifiers.noBorder, formatBreakpointMods(widths, styles), className), onTransitionEnd: ev => {
                if (!hidden && ev.nativeEvent.propertyName === 'transform') {
                    onExpand();
                }
            }, hidden: hidden }, props), !hidden && children));
    }));
};
DrawerPanelContent.displayName = 'DrawerPanelContent';
//# sourceMappingURL=DrawerPanelContent.js.map
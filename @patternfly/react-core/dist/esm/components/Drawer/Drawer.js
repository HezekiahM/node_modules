import { __rest } from "tslib";
import * as React from 'react';
import styles from '@patternfly/react-styles/css/components/Drawer/drawer';
import { css } from '@patternfly/react-styles';
export const DrawerContext = React.createContext({
    isExpanded: false,
    isStatic: false,
    onExpand: () => { }
});
export const Drawer = (_a) => {
    var { className = '', children, isExpanded = false, isInline = false, isStatic = false, position = 'right', onExpand = () => { } } = _a, props = __rest(_a, ["className", "children", "isExpanded", "isInline", "isStatic", "position", "onExpand"]);
    return (React.createElement(DrawerContext.Provider, { value: { isExpanded, isStatic, onExpand } },
        React.createElement("div", Object.assign({ className: css(styles.drawer, isExpanded && styles.modifiers.expanded, isInline && styles.modifiers.inline, isStatic && styles.modifiers.static, position === 'left' && styles.modifiers.panelLeft, className) }, props), children)));
};
Drawer.displayName = 'Drawer';
//# sourceMappingURL=Drawer.js.map
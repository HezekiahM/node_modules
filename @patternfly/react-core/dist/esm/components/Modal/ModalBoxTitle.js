import { __rest } from "tslib";
import * as React from 'react';
import modalStyles from '@patternfly/react-styles/css/components/ModalBox/modal-box';
import { css } from '@patternfly/react-styles';
import { Tooltip } from '../Tooltip';
export const ModalBoxTitle = (_a) => {
    var { className = '', id, title } = _a, props = __rest(_a, ["className", "id", "title"]);
    const [isTooltipVisible, setIsTooltipVisible] = React.useState(false);
    const h1 = React.useRef();
    React.useLayoutEffect(() => {
        setIsTooltipVisible(h1.current && h1.current.offsetWidth < h1.current.scrollWidth);
    }, []);
    return isTooltipVisible ? (React.createElement(Tooltip, { content: title },
        React.createElement("h1", Object.assign({ id: id, ref: h1, className: css(modalStyles.modalBoxTitle, className) }, props), title))) : (React.createElement("h1", Object.assign({ id: id, ref: h1, className: css(modalStyles.modalBoxTitle, className) }, props), title));
};
ModalBoxTitle.displayName = 'ModalBoxTitle';
//# sourceMappingURL=ModalBoxTitle.js.map
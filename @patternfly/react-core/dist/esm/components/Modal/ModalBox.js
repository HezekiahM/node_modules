import { __rest } from "tslib";
import * as React from 'react';
import { css } from '@patternfly/react-styles';
import styles from '@patternfly/react-styles/css/components/ModalBox/modal-box';
export const ModalBox = (_a) => {
    var { children, className = '', variant = 'default', 'aria-labelledby': ariaLabelledby, 'aria-label': ariaLabel = '', 'aria-describedby': ariaDescribedby } = _a, props = __rest(_a, ["children", "className", "variant", 'aria-labelledby', 'aria-label', 'aria-describedby']);
    return (React.createElement("div", Object.assign({}, props, { role: "dialog", "aria-label": ariaLabel || null, "aria-labelledby": ariaLabelledby || null, "aria-describedby": ariaDescribedby, "aria-modal": "true", className: css(styles.modalBox, className, variant === 'large' && styles.modifiers.lg, variant === 'small' && styles.modifiers.sm) }), children));
};
ModalBox.displayName = 'ModalBox';
//# sourceMappingURL=ModalBox.js.map
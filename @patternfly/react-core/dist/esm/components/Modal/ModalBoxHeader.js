import { __rest } from "tslib";
import * as React from 'react';
import { css } from '@patternfly/react-styles';
export const ModalBoxHeader = (_a) => {
    var { children = null, className = '' } = _a, props = __rest(_a, ["children", "className"]);
    return (React.createElement("header", Object.assign({ className: css('pf-c-modal-box__header', className) }, props), children));
};
ModalBoxHeader.displayName = 'ModalBoxHeader';
//# sourceMappingURL=ModalBoxHeader.js.map
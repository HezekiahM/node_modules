import { __rest } from "tslib";
import * as React from 'react';
import styles from '@patternfly/react-styles/css/components/Toolbar/toolbar';
import { css } from '@patternfly/react-styles';
import { formatBreakpointMods, toCamel } from '../../helpers/util';
import { Divider } from '../Divider';
export var ToolbarItemVariant;
(function (ToolbarItemVariant) {
    ToolbarItemVariant["separator"] = "separator";
    ToolbarItemVariant["bulk-select"] = "bulk-select";
    ToolbarItemVariant["overflow-menu"] = "overflow-menu";
    ToolbarItemVariant["pagination"] = "pagination";
    ToolbarItemVariant["search-filter"] = "search-filter";
    ToolbarItemVariant["label"] = "label";
    ToolbarItemVariant["chip-group"] = "chip-group";
})(ToolbarItemVariant || (ToolbarItemVariant = {}));
export const ToolbarItem = (_a) => {
    var { className, variant, visiblity, alignment, spacer, id, children } = _a, props = __rest(_a, ["className", "variant", "visiblity", "alignment", "spacer", "id", "children"]);
    if (variant === ToolbarItemVariant.separator) {
        return React.createElement(Divider, Object.assign({ className: css(styles.modifiers.vertical, className) }, props));
    }
    return (React.createElement("div", Object.assign({ className: css(styles.toolbarItem, variant &&
            styles.modifiers[toCamel(variant)], formatBreakpointMods(visiblity, styles), formatBreakpointMods(alignment, styles), formatBreakpointMods(spacer, styles), className) }, (variant === 'label' && { 'aria-hidden': true }), { id: id }, props), children));
};
ToolbarItem.displayName = 'ToolbarItem';
//# sourceMappingURL=ToolbarItem.js.map
import { __rest } from "tslib";
import * as React from 'react';
import styles from '@patternfly/react-styles/css/components/Toolbar/toolbar';
import { css } from '@patternfly/react-styles';
import { formatBreakpointMods, toCamel } from '../../helpers/util';
export var ToolbarGroupVariant;
(function (ToolbarGroupVariant) {
    ToolbarGroupVariant["filter-group"] = "filter-group";
    ToolbarGroupVariant["icon-button-group"] = "icon-button-group";
    ToolbarGroupVariant["button-group"] = "button-group";
})(ToolbarGroupVariant || (ToolbarGroupVariant = {}));
class ToolbarGroupWithRef extends React.Component {
    render() {
        const _a = this.props, { visiblity, alignment, spacer, spaceItems, className, variant, children, innerRef } = _a, props = __rest(_a, ["visiblity", "alignment", "spacer", "spaceItems", "className", "variant", "children", "innerRef"]);
        return (React.createElement("div", Object.assign({ className: css(styles.toolbarGroup, variant && styles.modifiers[toCamel(variant)], formatBreakpointMods(visiblity, styles), formatBreakpointMods(alignment, styles), formatBreakpointMods(spacer, styles), formatBreakpointMods(spaceItems, styles), className) }, props, { ref: innerRef }), children));
    }
}
export const ToolbarGroup = React.forwardRef((props, ref) => (React.createElement(ToolbarGroupWithRef, Object.assign({}, props, { innerRef: ref }))));
//# sourceMappingURL=ToolbarGroup.js.map
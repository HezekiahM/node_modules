import * as React from 'react';
import progressStyle from '@patternfly/react-styles/css/components/Progress/progress';
import { css } from '@patternfly/react-styles';
import CheckCircleIcon from '@patternfly/react-icons/dist/js/icons/check-circle-icon';
import TimesCircleIcon from '@patternfly/react-icons/dist/js/icons/times-circle-icon';
import { ProgressBar } from './ProgressBar';
export var ProgressMeasureLocation;
(function (ProgressMeasureLocation) {
    ProgressMeasureLocation["outside"] = "outside";
    ProgressMeasureLocation["inside"] = "inside";
    ProgressMeasureLocation["top"] = "top";
    ProgressMeasureLocation["none"] = "none";
})(ProgressMeasureLocation || (ProgressMeasureLocation = {}));
export var ProgressVariant;
(function (ProgressVariant) {
    ProgressVariant["danger"] = "danger";
    ProgressVariant["success"] = "success";
})(ProgressVariant || (ProgressVariant = {}));
const variantToIcon = {
    danger: TimesCircleIcon,
    success: CheckCircleIcon
};
export const ProgressContainer = ({ progressBarAriaProps, value, title = '', parentId, label = null, variant = null, measureLocation = ProgressMeasureLocation.top }) => {
    const StatusIcon = variantToIcon.hasOwnProperty(variant) && variantToIcon[variant];
    return (React.createElement(React.Fragment, null,
        React.createElement("div", { className: css(progressStyle.progressDescription), id: `${parentId}-description`, "aria-hidden": "true" }, title),
        React.createElement("div", { className: css(progressStyle.progressStatus), "aria-hidden": "true" },
            (measureLocation === ProgressMeasureLocation.top || measureLocation === ProgressMeasureLocation.outside) && (React.createElement("span", { className: css(progressStyle.progressMeasure) }, label || `${value}%`)),
            variantToIcon.hasOwnProperty(variant) && (React.createElement("span", { className: css(progressStyle.progressStatusIcon) },
                React.createElement(StatusIcon, null)))),
        React.createElement(ProgressBar, { role: "progressbar", progressBarAriaProps: progressBarAriaProps, value: value }, measureLocation === ProgressMeasureLocation.inside && `${value}%`)));
};
ProgressContainer.displayName = 'ProgressContainer';
//# sourceMappingURL=ProgressContainer.js.map
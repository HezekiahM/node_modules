import { __rest } from "tslib";
import * as React from 'react';
import styles from '@patternfly/react-styles/css/components/FormControl/form-control';
import { css } from '@patternfly/react-styles';
import { capitalize, ValidatedOptions } from '../../helpers';
export var TextAreResizeOrientation;
(function (TextAreResizeOrientation) {
    TextAreResizeOrientation["horizontal"] = "horizontal";
    TextAreResizeOrientation["vertical"] = "vertical";
    TextAreResizeOrientation["both"] = "both";
})(TextAreResizeOrientation || (TextAreResizeOrientation = {}));
export class TextArea extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = (event) => {
            if (this.props.onChange) {
                this.props.onChange(event.currentTarget.value, event);
            }
        };
        if (!props.id && !props['aria-label']) {
            // eslint-disable-next-line no-console
            console.error('TextArea: TextArea requires either an id or aria-label to be specified');
        }
    }
    render() {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const _a = this.props, { className, value, onChange, validated, isRequired, resizeOrientation } = _a, props = __rest(_a, ["className", "value", "onChange", "validated", "isRequired", "resizeOrientation"]);
        const orientation = `resize${capitalize(resizeOrientation)}`;
        return (React.createElement("textarea", Object.assign({ className: css(styles.formControl, className, resizeOrientation !== TextAreResizeOrientation.both && styles.modifiers[orientation], validated === ValidatedOptions.success && styles.modifiers.success, validated === ValidatedOptions.warning && styles.modifiers.warning), onChange: this.handleChange }, (typeof this.props.defaultValue !== 'string' && { value }), { "aria-invalid": validated === ValidatedOptions.error, required: isRequired }, props)));
    }
}
TextArea.displayName = 'TextArea';
TextArea.defaultProps = {
    className: '',
    isRequired: false,
    validated: 'default',
    resizeOrientation: 'both',
    'aria-label': null
};
//# sourceMappingURL=TextArea.js.map
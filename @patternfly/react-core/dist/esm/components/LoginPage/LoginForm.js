import { __rest } from "tslib";
import * as React from 'react';
import { Form, FormGroup, ActionGroup, FormHelperText } from '../Form';
import { TextInput } from '../TextInput';
import { Button } from '../Button';
import { Checkbox } from '../Checkbox';
import { ValidatedOptions } from '../../helpers/constants';
export const LoginForm = (_a) => {
    var { noAutoFocus = false, className = '', showHelperText = false, helperText = null, helperTextIcon = null, usernameLabel = 'Username', usernameValue = '', onChangeUsername = () => undefined, isValidUsername = true, passwordLabel = 'Password', passwordValue = '', onChangePassword = () => undefined, isValidPassword = true, loginButtonLabel = 'Log In', isLoginButtonDisabled = false, onLoginButtonClick = () => undefined, rememberMeLabel = '', isRememberMeChecked = false, onChangeRememberMe = () => undefined } = _a, props = __rest(_a, ["noAutoFocus", "className", "showHelperText", "helperText", "helperTextIcon", "usernameLabel", "usernameValue", "onChangeUsername", "isValidUsername", "passwordLabel", "passwordValue", "onChangePassword", "isValidPassword", "loginButtonLabel", "isLoginButtonDisabled", "onLoginButtonClick", "rememberMeLabel", "isRememberMeChecked", "onChangeRememberMe"]);
    return (React.createElement(Form, Object.assign({ className: className }, props),
        React.createElement(FormHelperText, { isError: !isValidUsername || !isValidPassword, isHidden: !showHelperText, icon: helperTextIcon }, helperText),
        React.createElement(FormGroup, { label: usernameLabel, isRequired: true, validated: isValidUsername ? ValidatedOptions.default : ValidatedOptions.error, fieldId: "pf-login-username-id" },
            React.createElement(TextInput, { autoFocus: !noAutoFocus, id: "pf-login-username-id", isRequired: true, validated: isValidUsername ? ValidatedOptions.default : ValidatedOptions.error, type: "text", name: "pf-login-username-id", value: usernameValue, onChange: onChangeUsername })),
        React.createElement(FormGroup, { label: passwordLabel, isRequired: true, validated: isValidPassword ? ValidatedOptions.default : ValidatedOptions.error, fieldId: "pf-login-password-id" },
            React.createElement(TextInput, { isRequired: true, type: "password", id: "pf-login-password-id", name: "pf-login-password-id", validated: isValidPassword ? ValidatedOptions.default : ValidatedOptions.error, value: passwordValue, onChange: onChangePassword })),
        rememberMeLabel.length > 0 && (React.createElement(FormGroup, { fieldId: "pf-login-remember-me-id" },
            React.createElement(Checkbox, { id: "pf-login-remember-me-id", label: rememberMeLabel, isChecked: isRememberMeChecked, onChange: onChangeRememberMe }))),
        React.createElement(ActionGroup, null,
            React.createElement(Button, { variant: "primary", type: "submit", onClick: onLoginButtonClick, isBlock: true, isDisabled: isLoginButtonDisabled }, loginButtonLabel))));
};
LoginForm.displayName = 'LoginForm';
//# sourceMappingURL=LoginForm.js.map
import { __rest } from "tslib";
import * as React from 'react';
export const ListItem = (_a) => {
    var { children = null } = _a, props = __rest(_a, ["children"]);
    return (React.createElement("li", Object.assign({}, props), children));
};
ListItem.displayName = 'ListItem';
//# sourceMappingURL=ListItem.js.map
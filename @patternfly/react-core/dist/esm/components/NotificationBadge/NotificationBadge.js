import { __rest } from "tslib";
import * as React from 'react';
import { Button, ButtonVariant } from '../Button';
import { css } from '@patternfly/react-styles';
import styles from '@patternfly/react-styles/css/components/NotificationBadge/notification-badge';
export const NotificationBadge = (_a) => {
    var { isRead = false, className, children } = _a, props = __rest(_a, ["isRead", "className", "children"]);
    return (React.createElement(Button, Object.assign({ variant: ButtonVariant.plain, className: className }, props),
        React.createElement("span", { className: css(styles.notificationBadge, isRead ? styles.modifiers.read : styles.modifiers.unread) }, children)));
};
NotificationBadge.displayName = 'NotificationBadge';
//# sourceMappingURL=NotificationBadge.js.map
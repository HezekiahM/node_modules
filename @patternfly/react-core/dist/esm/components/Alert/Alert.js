import { __rest } from "tslib";
import * as React from 'react';
import { useState } from 'react';
import { css } from '@patternfly/react-styles';
import styles from '@patternfly/react-styles/css/components/Alert/alert';
import accessibleStyles from '@patternfly/react-styles/css/utilities/Accessibility/accessibility';
import { AlertIcon } from './AlertIcon';
import { capitalize, getOUIAProps } from '../../helpers';
import { AlertContext } from './AlertContext';
export var AlertVariant;
(function (AlertVariant) {
    AlertVariant["success"] = "success";
    AlertVariant["danger"] = "danger";
    AlertVariant["warning"] = "warning";
    AlertVariant["info"] = "info";
    AlertVariant["default"] = "default";
})(AlertVariant || (AlertVariant = {}));
export const Alert = (_a) => {
    var { variant = AlertVariant.default, isInline = false, isLiveRegion = false, variantLabel = `${capitalize(variant)} alert:`, 'aria-label': ariaLabel = `${capitalize(variant)} Alert`, actionClose, actionLinks, title, children = '', className = '', ouiaId, ouiaSafe = true, timeout = false } = _a, props = __rest(_a, ["variant", "isInline", "isLiveRegion", "variantLabel", 'aria-label', "actionClose", "actionLinks", "title", "children", "className", "ouiaId", "ouiaSafe", "timeout"]);
    const getHeadingContent = (React.createElement(React.Fragment, null,
        React.createElement("span", { className: css(accessibleStyles.screenReader) }, variantLabel),
        title));
    const [disableAlert, setDisableAlert] = useState(false);
    const customClassName = css(styles.alert, isInline && styles.modifiers.inline, variant !== AlertVariant.default && styles.modifiers[variant], className);
    if (disableAlert === false && timeout && timeout !== 0) {
        setTimeout(() => {
            setDisableAlert(true);
        }, timeout === true ? 8000 : timeout);
    }
    if (disableAlert === false) {
        return (React.createElement("div", Object.assign({}, props, { className: customClassName, "aria-label": ariaLabel }, getOUIAProps(Alert.displayName, ouiaId, ouiaSafe), (isLiveRegion && {
            'aria-live': 'polite',
            'aria-atomic': 'false'
        })),
            React.createElement(AlertIcon, { variant: variant }),
            React.createElement("h4", { className: css(styles.alertTitle) }, getHeadingContent),
            actionClose && (React.createElement(AlertContext.Provider, { value: { title, variantLabel } },
                React.createElement("div", { className: css(styles.alertAction) }, actionClose))),
            children && React.createElement("div", { className: css(styles.alertDescription) }, children),
            actionLinks && React.createElement("div", { className: css(styles.alertActionGroup) }, actionLinks)));
    }
    else {
        return null;
    }
};
Alert.displayName = 'Alert';
//# sourceMappingURL=Alert.js.map
import { __rest } from "tslib";
import * as React from 'react';
import { css } from '@patternfly/react-styles';
import styles from '@patternfly/react-styles/css/components/Alert/alert';
import CheckCircleIcon from '@patternfly/react-icons/dist/js/icons/check-circle-icon';
import ExclamationCircleIcon from '@patternfly/react-icons/dist/js/icons/exclamation-circle-icon';
import ExclamationTriangleIcon from '@patternfly/react-icons/dist/js/icons/exclamation-triangle-icon';
import InfoCircleIcon from '@patternfly/react-icons/dist/js/icons/info-circle-icon';
import BellIcon from '@patternfly/react-icons/dist/js/icons/bell-icon';
export const variantIcons = {
    success: CheckCircleIcon,
    danger: ExclamationCircleIcon,
    warning: ExclamationTriangleIcon,
    info: InfoCircleIcon,
    default: BellIcon
};
export const AlertIcon = (_a) => {
    var { variant, className = '' } = _a, props = __rest(_a, ["variant", "className"]);
    const Icon = variantIcons[variant];
    return (React.createElement("div", Object.assign({}, props, { className: css(styles.alertIcon, className) }),
        React.createElement(Icon, null)));
};
//# sourceMappingURL=AlertIcon.js.map
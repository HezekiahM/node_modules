import { __rest } from "tslib";
import * as React from 'react';
import styles from '@patternfly/react-styles/css/components/Label/label';
import { Button } from '../Button';
import { css } from '@patternfly/react-styles';
import TimesIcon from '@patternfly/react-icons/dist/js/icons/times-icon';
const colorStyles = {
    blue: styles.modifiers.blue,
    cyan: styles.modifiers.cyan,
    green: styles.modifiers.green,
    orange: styles.modifiers.orange,
    purple: styles.modifiers.purple,
    red: styles.modifiers.red,
    grey: ''
};
export const Label = (_a) => {
    var { children, className = '', color = 'grey', variant = 'filled', icon, onClose, closeBtn, closeBtnProps, href } = _a, props = __rest(_a, ["children", "className", "color", "variant", "icon", "onClose", "closeBtn", "closeBtnProps", "href"]);
    const Component = href ? 'a' : 'span';
    const button = closeBtn ? (closeBtn) : (React.createElement(Button, Object.assign({ type: "button", variant: "plain", onClick: onClose }, Object.assign({ 'aria-label': 'label-close-button' }, closeBtnProps)),
        React.createElement(TimesIcon, null)));
    return (React.createElement("span", Object.assign({}, props, { className: css(styles.label, colorStyles[color], variant === 'outline' && styles.modifiers.outline, className) }),
        React.createElement(Component, Object.assign({ className: css(styles.labelContent) }, (href && { href })),
            icon && React.createElement("span", { className: css(styles.labelIcon) }, icon),
            children),
        onClose && button));
};
Label.displayName = 'Label';
//# sourceMappingURL=Label.js.map
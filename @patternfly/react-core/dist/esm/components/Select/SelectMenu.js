import { __rest } from "tslib";
import * as React from 'react';
import styles from '@patternfly/react-styles/css/components/Select/select';
import formStyles from '@patternfly/react-styles/css/components/Form/form';
import { css } from '@patternfly/react-styles';
import { SelectOption } from './SelectOption';
import { SelectConsumer, SelectVariant } from './selectConstants';
import { FocusTrap } from '../../helpers';
class SelectMenuWithRef extends React.Component {
    extendChildren(randomId) {
        const { children, isGrouped } = this.props;
        const childrenArray = children;
        if (isGrouped) {
            return React.Children.map(childrenArray, (group, index) => React.cloneElement(group, {
                titleId: group.props.label && group.props.label.replace(/\W/g, '-'),
                children: group.props.children.map((option) => this.cloneOption(option, index++, randomId))
            }));
        }
        return React.Children.map(childrenArray, (child, index) => this.cloneOption(child, index, randomId));
    }
    cloneOption(child, index, randomId) {
        const { selected, sendRef, keyHandler } = this.props;
        const isSelected = this.checkForValue(child.props.value, selected);
        return React.cloneElement(child, {
            inputId: `${randomId}-${index}`,
            id: `${child.props.id ? child.props.id : randomId}-${index}`,
            isSelected,
            sendRef,
            keyHandler,
            index
        });
    }
    checkForValue(valueToCheck, options) {
        if (!options) {
            return false;
        }
        const isSelectOptionObject = typeof valueToCheck !== 'string' &&
            valueToCheck.toString &&
            valueToCheck.compareTo;
        if (Array.isArray(options)) {
            if (isSelectOptionObject) {
                return options.some(option => option.compareTo(valueToCheck));
            }
            else {
                return options.includes(valueToCheck);
            }
        }
        else {
            if (isSelectOptionObject) {
                return options.compareTo(valueToCheck);
            }
            else {
                return options === valueToCheck;
            }
        }
    }
    extendCheckboxChildren(children) {
        const { isGrouped, checked, sendRef, keyHandler, hasInlineFilter } = this.props;
        let index = hasInlineFilter ? 1 : 0;
        if (isGrouped) {
            return React.Children.map(children, (group) => {
                if (group.type === SelectOption) {
                    return group;
                }
                return React.cloneElement(group, {
                    titleId: group.props.label && group.props.label.replace(/\W/g, '-'),
                    children: (React.createElement("fieldset", { "aria-labelledby": group.props.label && group.props.label.replace(/\W/g, '-'), className: css(styles.selectMenuFieldset) }, React.Children.map(group.props.children, (option) => React.cloneElement(option, {
                        isChecked: this.checkForValue(option.props.value, checked),
                        sendRef,
                        keyHandler,
                        index: index++
                    }))))
                });
            });
        }
        return React.Children.map(children, (child) => React.cloneElement(child, {
            isChecked: this.checkForValue(child.props.value, checked),
            sendRef,
            keyHandler,
            index: index++
        }));
    }
    render() {
        /* eslint-disable @typescript-eslint/no-unused-vars */
        const _a = this.props, { children, isCustomContent, className, isExpanded, openedOnEnter, selected, checked, isGrouped, sendRef, keyHandler, maxHeight, noResultsFoundText, createText, 'aria-label': ariaLabel, 'aria-labelledby': ariaLabelledBy, hasInlineFilter, innerRef } = _a, props = __rest(_a, ["children", "isCustomContent", "className", "isExpanded", "openedOnEnter", "selected", "checked", "isGrouped", "sendRef", "keyHandler", "maxHeight", "noResultsFoundText", "createText", 'aria-label', 'aria-labelledby', "hasInlineFilter", "innerRef"]);
        /* eslint-enable @typescript-eslint/no-unused-vars */
        return (React.createElement(SelectConsumer, null, ({ variant, inputIdPrefix }) => (React.createElement(React.Fragment, null,
            isCustomContent && (React.createElement("div", Object.assign({ ref: innerRef, className: css(styles.selectMenu, className) }, (maxHeight && { style: { maxHeight, overflow: 'auto' } }), props), children)),
            variant !== SelectVariant.checkbox && !isCustomContent && (React.createElement("ul", Object.assign({ ref: innerRef, className: css(styles.selectMenu, className), role: "listbox" }, (maxHeight && { style: { maxHeight, overflow: 'auto' } }), props), this.extendChildren(inputIdPrefix))),
            variant === SelectVariant.checkbox && !isCustomContent && React.Children.count(children) > 0 && (React.createElement(FocusTrap, { focusTrapOptions: { clickOutsideDeactivates: true } },
                React.createElement("div", Object.assign({ ref: innerRef, className: css(styles.selectMenu, className) }, (maxHeight && { style: { maxHeight, overflow: 'auto' } })),
                    React.createElement("fieldset", Object.assign({}, props, { "aria-label": ariaLabel, "aria-labelledby": (!ariaLabel && ariaLabelledBy) || null, className: css(formStyles.formFieldset) }),
                        hasInlineFilter && [
                            children.shift(),
                            ...this.extendCheckboxChildren(children)
                        ],
                        !hasInlineFilter && this.extendCheckboxChildren(children))))),
            variant === SelectVariant.checkbox && !isCustomContent && React.Children.count(children) === 0 && (React.createElement("div", Object.assign({ ref: innerRef, className: css(styles.selectMenu, className) }, (maxHeight && { style: { maxHeight, overflow: 'auto' } })),
                React.createElement("fieldset", { className: css(styles.selectMenuFieldset) })))))));
    }
}
SelectMenuWithRef.displayName = 'SelectMenu';
SelectMenuWithRef.defaultProps = {
    className: '',
    isExpanded: false,
    isGrouped: false,
    openedOnEnter: false,
    selected: '',
    maxHeight: '',
    sendRef: () => { },
    keyHandler: () => { },
    isCustomContent: false,
    hasInlineFilter: false
};
export const SelectMenu = React.forwardRef((props, ref) => (React.createElement(SelectMenuWithRef, Object.assign({ innerRef: ref }, props), props.children)));
//# sourceMappingURL=SelectMenu.js.map
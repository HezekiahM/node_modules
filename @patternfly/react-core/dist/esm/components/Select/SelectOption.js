import { __rest } from "tslib";
import * as React from 'react';
import styles from '@patternfly/react-styles/css/components/Select/select';
import checkStyles from '@patternfly/react-styles/css/components/Check/check';
import { css } from '@patternfly/react-styles';
import CheckIcon from '@patternfly/react-icons/dist/js/icons/check-icon';
import { SelectConsumer, SelectVariant, KeyTypes } from './selectConstants';
export class SelectOption extends React.Component {
    constructor() {
        super(...arguments);
        this.ref = React.createRef();
        this.onKeyDown = (event) => {
            if (event.key === KeyTypes.Tab) {
                return;
            }
            event.preventDefault();
            if (event.key === KeyTypes.ArrowUp) {
                this.props.keyHandler(this.props.index, 'up');
            }
            else if (event.key === KeyTypes.ArrowDown) {
                this.props.keyHandler(this.props.index, 'down');
            }
            else if (event.key === KeyTypes.Enter) {
                this.ref.current.click();
                if (this.context.variant === SelectVariant.checkbox) {
                    this.ref.current.focus();
                }
            }
        };
    }
    componentDidMount() {
        this.props.sendRef(this.props.isDisabled ? null : this.ref.current, this.props.index);
    }
    componentDidUpdate() {
        this.props.sendRef(this.props.isDisabled ? null : this.ref.current, this.props.index);
    }
    render() {
        /* eslint-disable @typescript-eslint/no-unused-vars */
        const _a = this.props, { children, className, description, value, onClick, isDisabled, isPlaceholder, isNoResultsOption, isSelected, isChecked, isFocused, sendRef, keyHandler, index, component, inputId } = _a, props = __rest(_a, ["children", "className", "description", "value", "onClick", "isDisabled", "isPlaceholder", "isNoResultsOption", "isSelected", "isChecked", "isFocused", "sendRef", "keyHandler", "index", "component", "inputId"]);
        /* eslint-enable @typescript-eslint/no-unused-vars */
        const Component = component;
        return (React.createElement(SelectConsumer, null, ({ onSelect, onClose, variant, inputIdPrefix }) => (React.createElement(React.Fragment, null,
            variant !== SelectVariant.checkbox && (React.createElement("li", { role: "presentation" },
                React.createElement(Component, Object.assign({}, props, { className: css(styles.selectMenuItem, isSelected && styles.modifiers.selected, isDisabled && styles.modifiers.disabled, isFocused && styles.modifiers.focus, description && styles.modifiers.description, className), onClick: (event) => {
                        if (!isDisabled) {
                            onClick(event);
                            onSelect(event, value, isPlaceholder);
                            onClose();
                        }
                    }, role: "option", "aria-selected": isSelected || null, ref: this.ref, onKeyDown: this.onKeyDown, type: "button" }),
                    description && (React.createElement(React.Fragment, null,
                        React.createElement("div", { className: css(styles.selectMenuItemMain) },
                            children || value.toString(),
                            isSelected && (React.createElement("span", { className: css(styles.selectMenuItemIcon) },
                                React.createElement(CheckIcon, { "aria-hidden": true })))),
                        React.createElement("div", { className: css(styles.selectMenuItemDescription) }, description))),
                    !description && (React.createElement(React.Fragment, null,
                        children || value.toString(),
                        isSelected && (React.createElement("span", { className: css(styles.selectMenuItemIcon) },
                            React.createElement(CheckIcon, { "aria-hidden": true })))))))),
            variant === SelectVariant.checkbox && !isNoResultsOption && (React.createElement("label", Object.assign({}, props, { className: css(checkStyles.check, styles.selectMenuItem, isDisabled && styles.modifiers.disabled, isFocused && styles.modifiers.focus, description && styles.modifiers.description, className), onKeyDown: this.onKeyDown }),
                React.createElement("input", { id: inputId || `${inputIdPrefix}-${value.toString()}`, className: css(checkStyles.checkInput), type: "checkbox", onChange: event => {
                        if (!isDisabled) {
                            onClick(event);
                            onSelect(event, value);
                        }
                    }, ref: this.ref, checked: isChecked || false, disabled: isDisabled }),
                React.createElement("span", { className: css(checkStyles.checkLabel, isDisabled && styles.modifiers.disabled) }, children || value.toString()),
                description && React.createElement("div", { className: css(checkStyles.checkDescription) }, description))),
            variant === SelectVariant.checkbox && isNoResultsOption && (React.createElement("div", null,
                React.createElement(Component, Object.assign({}, props, { className: css(styles.selectMenuItem, isSelected && styles.modifiers.selected, isDisabled && styles.modifiers.disabled, className), role: "option", "aria-selected": isSelected || null, ref: this.ref, onKeyDown: this.onKeyDown, type: "button" }), children || value.toString())))))));
    }
}
SelectOption.displayName = 'SelectOption';
SelectOption.defaultProps = {
    className: '',
    value: '',
    index: 0,
    isDisabled: false,
    isPlaceholder: false,
    isSelected: false,
    isChecked: false,
    isNoResultsOption: false,
    component: 'button',
    onClick: () => { },
    sendRef: () => { },
    keyHandler: () => { },
    inputId: ''
};
//# sourceMappingURL=SelectOption.js.map
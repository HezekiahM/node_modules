import { __rest } from "tslib";
import * as React from 'react';
export const TabButton = (_a) => {
    var { children, 
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    tabContentRef } = _a, props = __rest(_a, ["children", "tabContentRef"]);
    const Component = (props.href ? 'a' : 'button');
    return React.createElement(Component, Object.assign({}, props), children);
};
TabButton.displayName = 'TabButton';
//# sourceMappingURL=TabButton.js.map
import { __rest } from "tslib";
import * as React from 'react';
import styles from '@patternfly/react-styles/css/components/Switch/switch';
import { css } from '@patternfly/react-styles';
import CheckIcon from '@patternfly/react-icons/dist/js/icons/check-icon';
import { getUniqueId } from '../../helpers/util';
import { getOUIAProps } from '../../helpers';
export class Switch extends React.Component {
    constructor(props) {
        super(props);
        if (!props.id && !props['aria-label']) {
            // eslint-disable-next-line no-console
            console.error('Switch: Switch requires either an id or aria-label to be specified');
        }
        this.id = props.id || getUniqueId();
    }
    render() {
        const _a = this.props, { 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        id, className, label, labelOff, isChecked, isDisabled, onChange, ouiaId, ouiaSafe } = _a, props = __rest(_a, ["id", "className", "label", "labelOff", "isChecked", "isDisabled", "onChange", "ouiaId", "ouiaSafe"]);
        const isAriaLabelledBy = props['aria-label'] === '';
        return (React.createElement("label", Object.assign({ className: css(styles.switch, className), htmlFor: this.id }, getOUIAProps(Switch.displayName, ouiaId, ouiaSafe)),
            React.createElement("input", Object.assign({ id: this.id, className: css(styles.switchInput), type: "checkbox", onChange: event => onChange(event.target.checked, event), checked: isChecked, disabled: isDisabled, "aria-labelledby": isAriaLabelledBy ? `${this.id}-on` : null }, props)),
            label !== undefined ? (React.createElement(React.Fragment, null,
                React.createElement("span", { className: css(styles.switchToggle) }),
                React.createElement("span", { className: css(styles.switchLabel, styles.modifiers.on), id: isAriaLabelledBy ? `${this.id}-on` : null, "aria-hidden": "true" }, label),
                React.createElement("span", { className: css(styles.switchLabel, styles.modifiers.off), id: isAriaLabelledBy ? `${this.id}-off` : null, "aria-hidden": "true" }, labelOff !== undefined ? labelOff : label))) : (React.createElement("span", { className: css(styles.switchToggle) },
                React.createElement("div", { className: css(styles.switchToggleIcon), "aria-hidden": "true" },
                    React.createElement(CheckIcon, { noVerticalAlign: true }))))));
    }
}
Switch.displayName = 'Switch';
Switch.defaultProps = {
    isChecked: true,
    isDisabled: false,
    'aria-label': '',
    onChange: () => undefined
};
//# sourceMappingURL=Switch.js.map
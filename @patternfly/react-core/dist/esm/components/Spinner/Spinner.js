import { __rest } from "tslib";
import * as React from 'react';
import styles from '@patternfly/react-styles/css/components/Spinner/spinner';
import { css } from '@patternfly/react-styles';
export var spinnerSize;
(function (spinnerSize) {
    spinnerSize["sm"] = "sm";
    spinnerSize["md"] = "md";
    spinnerSize["lg"] = "lg";
    spinnerSize["xl"] = "xl";
})(spinnerSize || (spinnerSize = {}));
export const Spinner = (_a) => {
    var { 
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    className = '', size = 'xl', 'aria-valuetext': ariaValueText = 'Loading...' } = _a, props = __rest(_a, ["className", "size", 'aria-valuetext']);
    return (React.createElement("span", Object.assign({ className: css(styles.spinner, styles.modifiers[size], className), role: "progressbar", "aria-valuetext": ariaValueText }, props),
        React.createElement("span", { className: css(styles.spinnerClipper) }),
        React.createElement("span", { className: css(styles.spinnerLeadBall) }),
        React.createElement("span", { className: css(styles.spinnerTailBall) })));
};
Spinner.displayName = 'Spinner';
//# sourceMappingURL=Spinner.js.map
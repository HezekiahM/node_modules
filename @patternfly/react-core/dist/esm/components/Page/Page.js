import { __rest } from "tslib";
import * as React from 'react';
import styles from '@patternfly/react-styles/css/components/Page/page';
import { css } from '@patternfly/react-styles';
import globalBreakpointXl from '@patternfly/react-tokens/dist/js/global_breakpoint_xl';
import { debounce } from '../../helpers/util';
export var PageLayouts;
(function (PageLayouts) {
    PageLayouts["vertical"] = "vertical";
    PageLayouts["horizontal"] = "horizontal";
})(PageLayouts || (PageLayouts = {}));
const PageContext = React.createContext({});
export const PageContextProvider = PageContext.Provider;
export const PageContextConsumer = PageContext.Consumer;
export class Page extends React.Component {
    constructor(props) {
        super(props);
        this.handleResize = () => {
            const { onPageResize } = this.props;
            const windowSize = window.innerWidth;
            // eslint-disable-next-line radix
            const mobileView = windowSize < Number.parseInt(globalBreakpointXl.value, 10);
            if (onPageResize) {
                onPageResize({ mobileView, windowSize });
            }
            this.setState({ mobileView });
        };
        this.onNavToggleMobile = () => {
            this.setState(prevState => ({
                mobileIsNavOpen: !prevState.mobileIsNavOpen
            }));
        };
        this.onNavToggleDesktop = () => {
            this.setState(prevState => ({
                desktopIsNavOpen: !prevState.desktopIsNavOpen
            }));
        };
        const { isManagedSidebar, defaultManagedSidebarIsOpen } = props;
        const managedSidebarOpen = !isManagedSidebar ? true : defaultManagedSidebarIsOpen;
        this.state = {
            desktopIsNavOpen: managedSidebarOpen,
            mobileIsNavOpen: false,
            mobileView: false
        };
    }
    componentDidMount() {
        const { isManagedSidebar, onPageResize } = this.props;
        if (isManagedSidebar || onPageResize) {
            window.addEventListener('resize', debounce(this.handleResize, 250));
            // Initial check if should be shown
            this.handleResize();
        }
    }
    componentWillUnmount() {
        const { isManagedSidebar, onPageResize } = this.props;
        if (isManagedSidebar || onPageResize) {
            window.removeEventListener('resize', debounce(this.handleResize, 250));
        }
    }
    render() {
        const _a = this.props, { breadcrumb, className, children, header, sidebar, skipToContent, role, mainContainerId, isManagedSidebar, 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        defaultManagedSidebarIsOpen, 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        onPageResize, mainAriaLabel, mainTabIndex } = _a, rest = __rest(_a, ["breadcrumb", "className", "children", "header", "sidebar", "skipToContent", "role", "mainContainerId", "isManagedSidebar", "defaultManagedSidebarIsOpen", "onPageResize", "mainAriaLabel", "mainTabIndex"]);
        const { mobileView, mobileIsNavOpen, desktopIsNavOpen } = this.state;
        const context = {
            isManagedSidebar,
            onNavToggle: mobileView ? this.onNavToggleMobile : this.onNavToggleDesktop,
            isNavOpen: mobileView ? mobileIsNavOpen : desktopIsNavOpen
        };
        return (React.createElement(PageContextProvider, { value: context },
            React.createElement("div", Object.assign({}, rest, { className: css(styles.page, className) }),
                skipToContent,
                header,
                sidebar,
                React.createElement("main", { role: role, id: mainContainerId, className: css(styles.pageMain), tabIndex: mainTabIndex, "aria-label": mainAriaLabel },
                    breadcrumb && React.createElement("section", { className: css(styles.pageMainBreadcrumb) }, breadcrumb),
                    children))));
    }
}
Page.displayName = 'Page';
Page.defaultProps = {
    isManagedSidebar: false,
    defaultManagedSidebarIsOpen: true,
    onPageResize: () => null,
    mainTabIndex: -1
};
//# sourceMappingURL=Page.js.map
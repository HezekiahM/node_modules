import { __rest } from "tslib";
import * as React from 'react';
import { css } from '@patternfly/react-styles';
import styles from '@patternfly/react-styles/css/components/NotificationDrawer/notification-drawer';
import { Text, TextVariants } from '../Text';
export const NotificationDrawerHeader = (_a) => {
    var { children, className = '', count, title = 'Notifications', unreadText = 'unread' } = _a, props = __rest(_a, ["children", "className", "count", "title", "unreadText"]);
    return (React.createElement("div", Object.assign({}, props, { className: css(styles.notificationDrawerHeader, className) }),
        React.createElement(Text, { component: TextVariants.h1, className: css(styles.notificationDrawerHeaderTitle) }, title),
        count && React.createElement("span", { className: css(styles.notificationDrawerHeaderStatus) }, `${count} ${unreadText}`),
        children && React.createElement("div", { className: css(styles.notificationDrawerHeaderAction) }, children)));
};
NotificationDrawerHeader.displayName = 'NotificationDrawerHeader';
//# sourceMappingURL=NotificationDrawerHeader.js.map
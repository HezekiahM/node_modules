import { __rest } from "tslib";
import * as React from 'react';
import AngleRightIcon from '@patternfly/react-icons/dist/js/icons/angle-right-icon';
import { css } from '@patternfly/react-styles';
import styles from '@patternfly/react-styles/css/components/NotificationDrawer/notification-drawer';
import { Badge } from '../Badge';
export const NotificationDrawerGroup = (_a) => {
    var { children, className = '', count, isExpanded, isRead = false, 
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    onExpand = (event, expanded) => undefined, title } = _a, props = __rest(_a, ["children", "className", "count", "isExpanded", "isRead", "onExpand", "title"]);
    return (React.createElement("section", Object.assign({}, props, { className: css(styles.notificationDrawerGroup, isExpanded && styles.modifiers.expanded, className) }),
        React.createElement("h1", null,
            React.createElement("button", { className: css(styles.notificationDrawerGroupToggle), "aria-expanded": isExpanded, onClick: e => onExpand(e, !isExpanded) },
                React.createElement("div", null, title),
                React.createElement("div", { className: css(styles.notificationDrawerGroupToggleCount) },
                    React.createElement(Badge, { isRead: isRead }, count)),
                React.createElement("span", { className: "pf-c-notification-drawer__group-toggle-icon" },
                    React.createElement(AngleRightIcon, null)))),
        children));
};
NotificationDrawerGroup.displayName = 'NotificationDrawerGroup';
//# sourceMappingURL=NotificationDrawerGroup.js.map
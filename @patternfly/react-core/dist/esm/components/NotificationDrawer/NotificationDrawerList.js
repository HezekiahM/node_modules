import { __rest } from "tslib";
import * as React from 'react';
import { css } from '@patternfly/react-styles';
import styles from '@patternfly/react-styles/css/components/NotificationDrawer/notification-drawer';
export const NotificationDrawerList = (_a) => {
    var { children, className = '', isHidden = false } = _a, props = __rest(_a, ["children", "className", "isHidden"]);
    return (React.createElement("ul", Object.assign({}, props, { className: css(styles.notificationDrawerList, className), hidden: isHidden }), children));
};
NotificationDrawerList.displayName = 'NotificationDrawerList';
//# sourceMappingURL=NotificationDrawerList.js.map
import { __rest } from "tslib";
import * as React from 'react';
import BellIcon from '@patternfly/react-icons/dist/js/icons/bell-icon';
import CheckCircleIcon from '@patternfly/react-icons/dist/js/icons/check-circle-icon';
import ExclamationCircleIcon from '@patternfly/react-icons/dist/js/icons/exclamation-circle-icon';
import ExclamationTriangleIcon from '@patternfly/react-icons/dist/js/icons/exclamation-triangle-icon';
import InfoCircleIcon from '@patternfly/react-icons/dist/js/icons/info-circle-icon';
import { css } from '@patternfly/react-styles';
import styles from '@patternfly/react-styles/css/components/NotificationDrawer/notification-drawer';
import a11yStyles from '@patternfly/react-styles/css/utilities/Accessibility/accessibility';
export const variantIcons = {
    success: CheckCircleIcon,
    danger: ExclamationCircleIcon,
    warning: ExclamationTriangleIcon,
    info: InfoCircleIcon,
    default: BellIcon
};
export const NotificationDrawerListItemHeader = (_a) => {
    var { children, className = '', icon = null, srTitle, title, variant = 'default' } = _a, props = __rest(_a, ["children", "className", "icon", "srTitle", "title", "variant"]);
    const Icon = variantIcons[variant];
    return (React.createElement(React.Fragment, null,
        React.createElement("div", Object.assign({}, props, { className: css(styles.notificationDrawerListItemHeader, className) }),
            React.createElement("span", { className: css(styles.notificationDrawerListItemHeaderIcon) }, icon ? icon : React.createElement(Icon, null)),
            React.createElement("h2", { className: css(styles.notificationDrawerListItemHeaderTitle) },
                srTitle && React.createElement("span", { className: css(a11yStyles.screenReader) }, srTitle),
                title)),
        children && React.createElement("div", { className: css(styles.notificationDrawerListItemAction) }, children)));
};
NotificationDrawerListItemHeader.displayName = 'NotificationDrawerListItemHeader';
//# sourceMappingURL=NotificationDrawerListItemHeader.js.map
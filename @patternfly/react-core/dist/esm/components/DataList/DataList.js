import { __rest } from "tslib";
import * as React from 'react';
import { css } from '@patternfly/react-styles';
import styles from '@patternfly/react-styles/css/components/DataList/data-list';
export const DataListContext = React.createContext({
    isSelectable: false
});
export const DataList = (_a) => {
    var { children = null, className = '', 'aria-label': ariaLabel, selectedDataListItemId = '', onSelectDataListItem, isCompact = false } = _a, props = __rest(_a, ["children", "className", 'aria-label', "selectedDataListItemId", "onSelectDataListItem", "isCompact"]);
    const isSelectable = onSelectDataListItem !== undefined;
    const updateSelectedDataListItem = (id) => {
        onSelectDataListItem(id);
    };
    return (React.createElement(DataListContext.Provider, { value: {
            isSelectable,
            selectedDataListItemId,
            updateSelectedDataListItem
        } },
        React.createElement("ul", Object.assign({ className: css(styles.dataList, isCompact && styles.modifiers.compact, className), "aria-label": ariaLabel }, props), children)));
};
DataList.displayName = 'DataList';
//# sourceMappingURL=DataList.js.map
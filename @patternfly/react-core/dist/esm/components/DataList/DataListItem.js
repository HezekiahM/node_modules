import { __rest } from "tslib";
import * as React from 'react';
import { css } from '@patternfly/react-styles';
import styles from '@patternfly/react-styles/css/components/DataList/data-list';
import { DataListContext } from './DataList';
import { KeyTypes } from '../Select';
export const DataListItem = (_a) => {
    var { isExpanded = false, className = '', id = '', 'aria-labelledby': ariaLabelledBy, children } = _a, props = __rest(_a, ["isExpanded", "className", "id", 'aria-labelledby', "children"]);
    return (React.createElement(DataListContext.Consumer, null, ({ isSelectable, selectedDataListItemId, updateSelectedDataListItem }) => {
        const selectDataListItem = (event) => {
            let target = event.target;
            while (event.currentTarget !== target) {
                if (('onclick' in target && target.onclick) ||
                    target.parentNode.classList.contains(styles.dataListItemAction) ||
                    target.parentNode.classList.contains(styles.dataListItemControl)) {
                    // check other event handlers are not present.
                    return;
                }
                else {
                    target = target.parentNode;
                }
            }
            updateSelectedDataListItem(id);
        };
        const onKeyDown = (event) => {
            if (event.key === KeyTypes.Enter) {
                updateSelectedDataListItem(id);
            }
        };
        return (React.createElement("li", Object.assign({ id: id, className: css(styles.dataListItem, isExpanded && styles.modifiers.expanded, isSelectable && styles.modifiers.selectable, selectedDataListItemId && selectedDataListItemId === id && styles.modifiers.selected, className), "aria-labelledby": ariaLabelledBy }, (isSelectable && { tabIndex: 0, onClick: selectDataListItem, onKeyDown }), (isSelectable && selectedDataListItemId === id && { 'aria-selected': true }), props), React.Children.map(children, child => React.isValidElement(child) &&
            React.cloneElement(child, {
                rowid: ariaLabelledBy
            }))));
    }));
};
DataListItem.displayName = 'DataListItem';
//# sourceMappingURL=DataListItem.js.map
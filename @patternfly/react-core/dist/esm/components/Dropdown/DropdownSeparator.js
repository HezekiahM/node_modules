import { __rest } from "tslib";
import * as React from 'react';
import { DropdownArrowContext } from './dropdownConstants';
import { InternalDropdownItem } from './InternalDropdownItem';
import { Divider, DividerVariant } from '../Divider';
export const DropdownSeparator = (_a) => {
    var { className = '', 
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    ref } = _a, // Types of Ref are different for React.FC vs React.Component
    props = __rest(_a, ["className", "ref"]);
    return (React.createElement(DropdownArrowContext.Consumer, null, context => (React.createElement(InternalDropdownItem, Object.assign({}, props, { context: context, component: React.createElement(Divider, { component: DividerVariant.div }), className: className, role: "separator" })))));
};
DropdownSeparator.displayName = 'DropdownSeparator';
//# sourceMappingURL=DropdownSeparator.js.map
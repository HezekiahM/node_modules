import { __rest } from "tslib";
import * as React from 'react';
import styles from '@patternfly/react-styles/css/components/Nav/nav';
import { css } from '@patternfly/react-styles';
import { NavContext } from './Nav';
export const NavItem = (_a) => {
    var { children, styleChildren = true, className, to, isActive = false, groupId = null, itemId = null, preventDefault = false, onClick = null, component = 'a' } = _a, props = __rest(_a, ["children", "styleChildren", "className", "to", "isActive", "groupId", "itemId", "preventDefault", "onClick", "component"]);
    const Component = component;
    const renderDefaultLink = (context) => {
        const preventLinkDefault = preventDefault || !to;
        return (React.createElement(Component, Object.assign({ href: to, onClick: (e) => context.onSelect(e, groupId, itemId, to, preventLinkDefault, onClick), className: css(styles.navLink, isActive && styles.modifiers.current, className), "aria-current": isActive ? 'page' : null }, props), children));
    };
    const renderClonedChild = (context, child) => React.cloneElement(child, Object.assign({ onClick: (e) => context.onSelect(e, groupId, itemId, to, preventDefault, onClick), 'aria-current': isActive ? 'page' : null }, (styleChildren && {
        className: css(styles.navLink, isActive && styles.modifiers.current, child.props && child.props.className)
    })));
    return (React.createElement("li", { className: css(styles.navItem, className) },
        React.createElement(NavContext.Consumer, null, context => React.isValidElement(children)
            ? renderClonedChild(context, children)
            : renderDefaultLink(context))));
};
NavItem.displayName = 'NavItem';
//# sourceMappingURL=NavItem.js.map
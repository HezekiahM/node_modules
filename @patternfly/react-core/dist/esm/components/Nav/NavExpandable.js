import { __rest } from "tslib";
import * as React from 'react';
import styles from '@patternfly/react-styles/css/components/Nav/nav';
import a11yStyles from '@patternfly/react-styles/css/utilities/Accessibility/accessibility';
import { css } from '@patternfly/react-styles';
import AngleRightIcon from '@patternfly/react-icons/dist/js/icons/angle-right-icon';
import { getUniqueId } from '../../helpers/util';
import { NavContext } from './Nav';
export class NavExpandable extends React.Component {
    constructor() {
        super(...arguments);
        this.expandableRef = React.createRef();
        this.id = this.props.id || getUniqueId();
        this.state = {
            expandedState: this.props.isExpanded
        };
        this.onExpand = (e, val) => {
            if (this.props.onExpand) {
                this.props.onExpand(e, val);
            }
            else {
                this.setState({ expandedState: val });
            }
        };
        this.handleToggle = (e, onToggle) => {
            // Item events can bubble up, ignore those
            if (!this.expandableRef.current || !this.expandableRef.current.contains(e.target)) {
                return;
            }
            const { groupId } = this.props;
            const { expandedState } = this.state;
            onToggle(e, groupId, !expandedState);
            this.onExpand(e, !expandedState);
        };
    }
    componentDidMount() {
        this.setState({ expandedState: this.props.isExpanded });
    }
    componentDidUpdate(prevProps) {
        if (this.props.isExpanded !== prevProps.isExpanded) {
            this.setState({ expandedState: this.props.isExpanded });
        }
    }
    render() {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const _a = this.props, { id, title, srText, children, className, isActive, groupId, isExpanded, onExpand } = _a, props = __rest(_a, ["id", "title", "srText", "children", "className", "isActive", "groupId", "isExpanded", "onExpand"]);
        const { expandedState } = this.state;
        return (React.createElement(NavContext.Consumer, null, (context) => (React.createElement("li", Object.assign({ className: css(styles.navItem, styles.modifiers.expandable, expandedState && styles.modifiers.expanded, isActive && styles.modifiers.current, className), onClick: (e) => this.handleToggle(e, context.onToggle) }, props),
            React.createElement("a", { ref: this.expandableRef, className: styles.navLink, id: srText ? null : this.id, href: "#", onClick: e => e.preventDefault(), onMouseDown: e => e.preventDefault(), "aria-expanded": expandedState },
                title,
                React.createElement("span", { className: css(styles.navToggle) },
                    React.createElement("span", { className: css(styles.navToggleIcon) },
                        React.createElement(AngleRightIcon, { "aria-hidden": "true" })))),
            React.createElement("section", { className: css(styles.navSubnav), "aria-labelledby": this.id, hidden: expandedState ? null : true },
                srText && (React.createElement("h2", { className: css(a11yStyles.screenReader), id: this.id }, srText)),
                React.createElement("ul", { className: css(styles.navList) }, children))))));
    }
}
NavExpandable.displayName = 'NavExpandable';
NavExpandable.defaultProps = {
    srText: '',
    isExpanded: false,
    children: '',
    className: '',
    groupId: null,
    isActive: false,
    id: ''
};
//# sourceMappingURL=NavExpandable.js.map
import { __rest } from "tslib";
import * as React from 'react';
export const GalleryItem = (_a) => {
    var { children = null } = _a, props = __rest(_a, ["children"]);
    return React.createElement("div", Object.assign({}, props), children);
};
GalleryItem.displayName = 'GalleryItem';
//# sourceMappingURL=GalleryItem.js.map
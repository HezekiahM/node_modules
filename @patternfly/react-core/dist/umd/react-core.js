(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('react'), require('react-dom')) :
    typeof define === 'function' && define.amd ? define(['exports', 'react', 'react-dom'], factory) :
    (global = global || self, factory(global.PatternFlyReact = {}, global.React, global.ReactDOM));
}(this, (function (exports, React, ReactDOM) { 'use strict';

    var React__default = 'default' in React ? React['default'] : React;

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __exportStar(m, exports) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m) return m.call(o);
        if (o && typeof o.length === "number") return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    }
    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    }
    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to get private field on non-instance");
        }
        return privateMap.get(receiver);
    }

    function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to set private field on non-instance");
        }
        privateMap.set(receiver, value);
        return value;
    }

    var tslib_es6 = /*#__PURE__*/Object.freeze({
        __proto__: null,
        __extends: __extends,
        get __assign () { return __assign; },
        __rest: __rest,
        __decorate: __decorate,
        __param: __param,
        __metadata: __metadata,
        __awaiter: __awaiter,
        __generator: __generator,
        __exportStar: __exportStar,
        __values: __values,
        __read: __read,
        __spread: __spread,
        __spreadArrays: __spreadArrays,
        __await: __await,
        __asyncGenerator: __asyncGenerator,
        __asyncDelegator: __asyncDelegator,
        __asyncValues: __asyncValues,
        __makeTemplateObject: __makeTemplateObject,
        __importStar: __importStar,
        __importDefault: __importDefault,
        __classPrivateFieldGet: __classPrivateFieldGet,
        __classPrivateFieldSet: __classPrivateFieldSet
    });

    /** Joins args into a className string
     *
     * @param {any} args list of objects, string, or arrays to reduce
     */
    function css(...args) {
        // Adapted from https://github.com/JedWatson/classnames/blob/master/index.js
        const classes = [];
        const hasOwn = {}.hasOwnProperty;
        args.filter(Boolean).forEach((arg) => {
            const argType = typeof arg;
            if (argType === 'string' || argType === 'number') {
                classes.push(arg);
            }
            else if (Array.isArray(arg) && arg.length) {
                const inner = css(...arg);
                if (inner) {
                    classes.push(inner);
                }
            }
            else if (argType === 'object') {
                for (const key in arg) {
                    if (hasOwn.call(arg, key) && arg[key]) {
                        classes.push(key);
                    }
                }
            }
        });
        return classes.join(' ');
    }

    function unwrapExports (x) {
    	return x && x.__esModule && Object.prototype.hasOwnProperty.call(x, 'default') ? x['default'] : x;
    }

    function createCommonjsModule(fn, module) {
    	return module = { exports: {} }, fn(module, module.exports), module.exports;
    }

    var backdrop = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "backdrop": "pf-c-backdrop",
      "backdropOpen": "pf-c-backdrop__open"
    };
    });

    var styles = unwrapExports(backdrop);

    const KEY_CODES = { ARROW_UP: 38, ARROW_DOWN: 40, ESCAPE_KEY: 27, TAB: 9, ENTER: 13, SPACE: 32 };
    const SIDE = { RIGHT: 'right', LEFT: 'left', BOTH: 'both', NONE: 'none' };
    const KEYHANDLER_DIRECTION = { UP: 'up', DOWN: 'down', RIGHT: 'right', LEFT: 'left' };
    (function (ValidatedOptions) {
        ValidatedOptions["success"] = "success";
        ValidatedOptions["error"] = "error";
        ValidatedOptions["warning"] = "warning";
        ValidatedOptions["default"] = "default";
    })(exports.ValidatedOptions || (exports.ValidatedOptions = {}));

    var candidateSelectors = [
      'input',
      'select',
      'textarea',
      'a[href]',
      'button',
      '[tabindex]',
      'audio[controls]',
      'video[controls]',
      '[contenteditable]:not([contenteditable="false"])',
    ];
    var candidateSelector = candidateSelectors.join(',');

    var matches = typeof Element === 'undefined'
      ? function () {}
      : Element.prototype.matches || Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector;

    function tabbable(el, options) {
      options = options || {};

      var elementDocument = el.ownerDocument || el;
      var regularTabbables = [];
      var orderedTabbables = [];

      var untouchabilityChecker = new UntouchabilityChecker(elementDocument);
      var candidates = el.querySelectorAll(candidateSelector);

      if (options.includeContainer) {
        if (matches.call(el, candidateSelector)) {
          candidates = Array.prototype.slice.apply(candidates);
          candidates.unshift(el);
        }
      }

      var i, candidate, candidateTabindex;
      for (i = 0; i < candidates.length; i++) {
        candidate = candidates[i];

        if (!isNodeMatchingSelectorTabbable(candidate, untouchabilityChecker)) continue;

        candidateTabindex = getTabindex(candidate);
        if (candidateTabindex === 0) {
          regularTabbables.push(candidate);
        } else {
          orderedTabbables.push({
            documentOrder: i,
            tabIndex: candidateTabindex,
            node: candidate,
          });
        }
      }

      var tabbableNodes = orderedTabbables
        .sort(sortOrderedTabbables)
        .map(function(a) { return a.node })
        .concat(regularTabbables);

      return tabbableNodes;
    }

    tabbable.isTabbable = isTabbable;
    tabbable.isFocusable = isFocusable;

    function isNodeMatchingSelectorTabbable(node, untouchabilityChecker) {
      if (
        !isNodeMatchingSelectorFocusable(node, untouchabilityChecker)
        || isNonTabbableRadio(node)
        || getTabindex(node) < 0
      ) {
        return false;
      }
      return true;
    }

    function isTabbable(node, untouchabilityChecker) {
      if (!node) throw new Error('No node provided');
      if (matches.call(node, candidateSelector) === false) return false;
      return isNodeMatchingSelectorTabbable(node, untouchabilityChecker);
    }

    function isNodeMatchingSelectorFocusable(node, untouchabilityChecker) {
      untouchabilityChecker = untouchabilityChecker || new UntouchabilityChecker(node.ownerDocument || node);
      if (
        node.disabled
        || isHiddenInput(node)
        || untouchabilityChecker.isUntouchable(node)
      ) {
        return false;
      }
      return true;
    }

    var focusableCandidateSelector = candidateSelectors.concat('iframe').join(',');
    function isFocusable(node, untouchabilityChecker) {
      if (!node) throw new Error('No node provided');
      if (matches.call(node, focusableCandidateSelector) === false) return false;
      return isNodeMatchingSelectorFocusable(node, untouchabilityChecker);
    }

    function getTabindex(node) {
      var tabindexAttr = parseInt(node.getAttribute('tabindex'), 10);
      if (!isNaN(tabindexAttr)) return tabindexAttr;
      // Browsers do not return `tabIndex` correctly for contentEditable nodes;
      // so if they don't have a tabindex attribute specifically set, assume it's 0.
      if (isContentEditable(node)) return 0;
      return node.tabIndex;
    }

    function sortOrderedTabbables(a, b) {
      return a.tabIndex === b.tabIndex ? a.documentOrder - b.documentOrder : a.tabIndex - b.tabIndex;
    }

    // Array.prototype.find not available in IE.
    function find(list, predicate) {
      for (var i = 0, length = list.length; i < length; i++) {
        if (predicate(list[i])) return list[i];
      }
    }

    function isContentEditable(node) {
      return node.contentEditable === 'true';
    }

    function isInput(node) {
      return node.tagName === 'INPUT';
    }

    function isHiddenInput(node) {
      return isInput(node) && node.type === 'hidden';
    }

    function isRadio(node) {
      return isInput(node) && node.type === 'radio';
    }

    function isNonTabbableRadio(node) {
      return isRadio(node) && !isTabbableRadio(node);
    }

    function getCheckedRadio(nodes) {
      for (var i = 0; i < nodes.length; i++) {
        if (nodes[i].checked) {
          return nodes[i];
        }
      }
    }

    function isTabbableRadio(node) {
      if (!node.name) return true;
      // This won't account for the edge case where you have radio groups with the same
      // in separate forms on the same page.
      var radioSet = node.ownerDocument.querySelectorAll('input[type="radio"][name="' + node.name + '"]');
      var checked = getCheckedRadio(radioSet);
      return !checked || checked === node;
    }

    // An element is "untouchable" if *it or one of its ancestors* has
    // `visibility: hidden` or `display: none`.
    function UntouchabilityChecker(elementDocument) {
      this.doc = elementDocument;
      // Node cache must be refreshed on every check, in case
      // the content of the element has changed. The cache contains tuples
      // mapping nodes to their boolean result.
      this.cache = [];
    }

    // getComputedStyle accurately reflects `visibility: hidden` of ancestors
    // but not `display: none`, so we need to recursively check parents.
    UntouchabilityChecker.prototype.hasDisplayNone = function hasDisplayNone(node, nodeComputedStyle) {
      if (node.nodeType !== Node.ELEMENT_NODE) return false;

        // Search for a cached result.
        var cached = find(this.cache, function(item) {
          return item === node;
        });
        if (cached) return cached[1];

        nodeComputedStyle = nodeComputedStyle || this.doc.defaultView.getComputedStyle(node);

        var result = false;

        if (nodeComputedStyle.display === 'none') {
          result = true;
        } else if (node.parentNode) {
          result = this.hasDisplayNone(node.parentNode);
        }

        this.cache.push([node, result]);

        return result;
    };

    UntouchabilityChecker.prototype.isUntouchable = function isUntouchable(node) {
      if (node === this.doc.documentElement) return false;
      var computedStyle = this.doc.defaultView.getComputedStyle(node);
      if (this.hasDisplayNone(node, computedStyle)) return true;
      return computedStyle.visibility === 'hidden';
    };

    var tabbable_1 = tabbable;

    var immutable = extend;

    var hasOwnProperty = Object.prototype.hasOwnProperty;

    function extend() {
        var target = {};

        for (var i = 0; i < arguments.length; i++) {
            var source = arguments[i];

            for (var key in source) {
                if (hasOwnProperty.call(source, key)) {
                    target[key] = source[key];
                }
            }
        }

        return target
    }

    var activeFocusTraps = (function() {
      var trapQueue = [];
      return {
        activateTrap: function(trap) {
          if (trapQueue.length > 0) {
            var activeTrap = trapQueue[trapQueue.length - 1];
            if (activeTrap !== trap) {
              activeTrap.pause();
            }
          }

          var trapIndex = trapQueue.indexOf(trap);
          if (trapIndex === -1) {
            trapQueue.push(trap);
          } else {
            // move this existing trap to the front of the queue
            trapQueue.splice(trapIndex, 1);
            trapQueue.push(trap);
          }
        },

        deactivateTrap: function(trap) {
          var trapIndex = trapQueue.indexOf(trap);
          if (trapIndex !== -1) {
            trapQueue.splice(trapIndex, 1);
          }

          if (trapQueue.length > 0) {
            trapQueue[trapQueue.length - 1].unpause();
          }
        }
      };
    })();

    function focusTrap(element, userOptions) {
      var doc = document;
      var container =
        typeof element === 'string' ? doc.querySelector(element) : element;

      var config = immutable(
        {
          returnFocusOnDeactivate: true,
          escapeDeactivates: true
        },
        userOptions
      );

      var state = {
        firstTabbableNode: null,
        lastTabbableNode: null,
        nodeFocusedBeforeActivation: null,
        mostRecentlyFocusedNode: null,
        active: false,
        paused: false
      };

      var trap = {
        activate: activate,
        deactivate: deactivate,
        pause: pause,
        unpause: unpause
      };

      return trap;

      function activate(activateOptions) {
        if (state.active) return;

        updateTabbableNodes();

        state.active = true;
        state.paused = false;
        state.nodeFocusedBeforeActivation = doc.activeElement;

        var onActivate =
          activateOptions && activateOptions.onActivate
            ? activateOptions.onActivate
            : config.onActivate;
        if (onActivate) {
          onActivate();
        }

        addListeners();
        return trap;
      }

      function deactivate(deactivateOptions) {
        if (!state.active) return;

        removeListeners();
        state.active = false;
        state.paused = false;

        activeFocusTraps.deactivateTrap(trap);

        var onDeactivate =
          deactivateOptions && deactivateOptions.onDeactivate !== undefined
            ? deactivateOptions.onDeactivate
            : config.onDeactivate;
        if (onDeactivate) {
          onDeactivate();
        }

        var returnFocus =
          deactivateOptions && deactivateOptions.returnFocus !== undefined
            ? deactivateOptions.returnFocus
            : config.returnFocusOnDeactivate;
        if (returnFocus) {
          delay(function() {
            tryFocus(state.nodeFocusedBeforeActivation);
          });
        }

        return trap;
      }

      function pause() {
        if (state.paused || !state.active) return;
        state.paused = true;
        removeListeners();
      }

      function unpause() {
        if (!state.paused || !state.active) return;
        state.paused = false;
        addListeners();
      }

      function addListeners() {
        if (!state.active) return;

        // There can be only one listening focus trap at a time
        activeFocusTraps.activateTrap(trap);

        updateTabbableNodes();

        // Delay ensures that the focused element doesn't capture the event
        // that caused the focus trap activation.
        delay(function() {
          tryFocus(getInitialFocusNode());
        });
        doc.addEventListener('focusin', checkFocusIn, true);
        doc.addEventListener('mousedown', checkPointerDown, true);
        doc.addEventListener('touchstart', checkPointerDown, true);
        doc.addEventListener('click', checkClick, true);
        doc.addEventListener('keydown', checkKey, true);

        return trap;
      }

      function removeListeners() {
        if (!state.active) return;

        doc.removeEventListener('focusin', checkFocusIn, true);
        doc.removeEventListener('mousedown', checkPointerDown, true);
        doc.removeEventListener('touchstart', checkPointerDown, true);
        doc.removeEventListener('click', checkClick, true);
        doc.removeEventListener('keydown', checkKey, true);

        return trap;
      }

      function getNodeForOption(optionName) {
        var optionValue = config[optionName];
        var node = optionValue;
        if (!optionValue) {
          return null;
        }
        if (typeof optionValue === 'string') {
          node = doc.querySelector(optionValue);
          if (!node) {
            throw new Error('`' + optionName + '` refers to no known node');
          }
        }
        if (typeof optionValue === 'function') {
          node = optionValue();
          if (!node) {
            throw new Error('`' + optionName + '` did not return a node');
          }
        }
        return node;
      }

      function getInitialFocusNode() {
        var node;
        if (getNodeForOption('initialFocus') !== null) {
          node = getNodeForOption('initialFocus');
        } else if (container.contains(doc.activeElement)) {
          node = doc.activeElement;
        } else {
          node = state.firstTabbableNode || getNodeForOption('fallbackFocus');
        }

        if (!node) {
          throw new Error(
            "You can't have a focus-trap without at least one focusable element"
          );
        }

        return node;
      }

      // This needs to be done on mousedown and touchstart instead of click
      // so that it precedes the focus event.
      function checkPointerDown(e) {
        if (container.contains(e.target)) return;
        if (config.clickOutsideDeactivates) {
          deactivate({
            returnFocus: !tabbable_1.isFocusable(e.target)
          });
        } else {
          e.preventDefault();
        }
      }

      // In case focus escapes the trap for some strange reason, pull it back in.
      function checkFocusIn(e) {
        // In Firefox when you Tab out of an iframe the Document is briefly focused.
        if (container.contains(e.target) || e.target instanceof Document) {
          return;
        }
        e.stopImmediatePropagation();
        tryFocus(state.mostRecentlyFocusedNode || getInitialFocusNode());
      }

      function checkKey(e) {
        if (config.escapeDeactivates !== false && isEscapeEvent(e)) {
          e.preventDefault();
          deactivate();
          return;
        }
        if (isTabEvent(e)) {
          checkTab(e);
          return;
        }
      }

      // Hijack Tab events on the first and last focusable nodes of the trap,
      // in order to prevent focus from escaping. If it escapes for even a
      // moment it can end up scrolling the page and causing confusion so we
      // kind of need to capture the action at the keydown phase.
      function checkTab(e) {
        updateTabbableNodes();
        if (e.shiftKey && e.target === state.firstTabbableNode) {
          e.preventDefault();
          tryFocus(state.lastTabbableNode);
          return;
        }
        if (!e.shiftKey && e.target === state.lastTabbableNode) {
          e.preventDefault();
          tryFocus(state.firstTabbableNode);
          return;
        }
      }

      function checkClick(e) {
        if (config.clickOutsideDeactivates) return;
        if (container.contains(e.target)) return;
        e.preventDefault();
        e.stopImmediatePropagation();
      }

      function updateTabbableNodes() {
        var tabbableNodes = tabbable_1(container);
        state.firstTabbableNode = tabbableNodes[0] || getInitialFocusNode();
        state.lastTabbableNode =
          tabbableNodes[tabbableNodes.length - 1] || getInitialFocusNode();
      }

      function tryFocus(node) {
        if (node === doc.activeElement) return;
        if (!node || !node.focus) {
          tryFocus(getInitialFocusNode());
          return;
        }

        node.focus();
        state.mostRecentlyFocusedNode = node;
        if (isSelectableInput(node)) {
          node.select();
        }
      }
    }

    function isSelectableInput(node) {
      return (
        node.tagName &&
        node.tagName.toLowerCase() === 'input' &&
        typeof node.select === 'function'
      );
    }

    function isEscapeEvent(e) {
      return e.key === 'Escape' || e.key === 'Esc' || e.keyCode === 27;
    }

    function isTabEvent(e) {
      return e.key === 'Tab' || e.keyCode === 9;
    }

    function delay(fn) {
      return setTimeout(fn, 0);
    }

    var focusTrap_1 = focusTrap;

    class FocusTrap extends React.Component {
        constructor(props) {
            super(props);
            this.divRef = React.createRef();
            if (typeof document !== 'undefined') {
                this.previouslyFocusedElement = document.activeElement;
            }
        }
        componentDidMount() {
            // We need to hijack the returnFocusOnDeactivate option,
            // because React can move focus into the element before we arrived at
            // this lifecycle hook (e.g. with autoFocus inputs). So the component
            // captures the previouslyFocusedElement in componentWillMount,
            // then (optionally) returns focus to it in componentWillUnmount.
            this.focusTrap = focusTrap_1(this.divRef.current, Object.assign(Object.assign({}, this.props.focusTrapOptions), { returnFocusOnDeactivate: false }));
            if (this.props.active) {
                this.focusTrap.activate();
            }
            if (this.props.paused) {
                this.focusTrap.pause();
            }
        }
        componentDidUpdate(prevProps) {
            if (prevProps.active && !this.props.active) {
                const { returnFocusOnDeactivate } = this.props.focusTrapOptions;
                const returnFocus = returnFocusOnDeactivate || false;
                const config = { returnFocus };
                this.focusTrap.deactivate(config);
            }
            else if (!prevProps.active && this.props.active) {
                this.focusTrap.activate();
            }
            if (prevProps.paused && !this.props.paused) {
                this.focusTrap.unpause();
            }
            else if (!prevProps.paused && this.props.paused) {
                this.focusTrap.pause();
            }
        }
        componentWillUnmount() {
            this.focusTrap.deactivate();
            if (this.props.focusTrapOptions.returnFocusOnDeactivate !== false &&
                this.previouslyFocusedElement &&
                this.previouslyFocusedElement.focus) {
                this.previouslyFocusedElement.focus();
            }
        }
        render() {
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            const _a = this.props, { children, className, focusTrapOptions, active, paused } = _a, rest = __rest(_a, ["children", "className", "focusTrapOptions", "active", "paused"]);
            return (React.createElement("div", Object.assign({ ref: this.divRef, className: className }, rest), children));
        }
    }
    FocusTrap.displayName = 'FocusTrap';
    FocusTrap.defaultProps = {
        active: true,
        paused: false,
        focusTrapOptions: {}
    };

    /** This Component can be used to wrap a functional component in order to generate a random ID
     * Example of how to use this component
     *
     * const Component = ({id}: {id: string}) => (
     *  <GenerateId>{randomId => (
     *     <div id={id || randomId}>
     *       div with random ID
     *     </div>
     *   )}
     *  </GenerateId>
     *  );
     */
    let currentId = 0;
    class GenerateId extends React.Component {
        constructor() {
            super(...arguments);
            this.id = `${this.props.prefix}${currentId++}`;
        }
        render() {
            return this.props.children(this.id);
        }
    }
    GenerateId.displayName = 'GenerateId';
    GenerateId.defaultProps = {
        prefix: 'pf-random-id-'
    };

    const ASTERISK = '*';

    let uid = 0;
    /** Get props to conform to OUIA spec
     *
     * @param {string} componentType OUIA component type
     * @param {number|string} id OUIA component id
     * @param {boolean} ouiaSafe false if in animation
     */
    function getOUIAProps(componentType, id, ouiaSafe = true) {
        return {
            'data-ouia-component-type': `PF4/${componentType}`,
            'data-ouia-safe': ouiaSafe,
            'data-ouia-component-id': id === undefined ? uid++ : id
        };
    }

    /**
     * @param {string} input - String to capitalize first letter
     */
    function capitalize(input) {
        return input[0].toUpperCase() + input.substring(1);
    }
    /**
     * @param {string} prefix - String to prefix ID with
     */
    function getUniqueId(prefix = 'pf') {
        const uid = new Date().getTime() +
            Math.random()
                .toString(36)
                .slice(2);
        return `${prefix}-${uid}`;
    }
    /**
     * @param { any } this - "This" reference
     * @param { Function } func - Function to debounce
     * @param { number } wait - Debounce amount
     */
    function debounce(func, wait) {
        let timeout;
        return (...args) => {
            clearTimeout(timeout);
            timeout = setTimeout(() => func.apply(this, args), wait);
        };
    }
    /** This function returns whether or not an element is within the viewable area of a container. If partial is true,
     * then this function will return true even if only part of the element is in view.
     *
     * @param {HTMLElement} container  The container to check if the element is in view of.
     * @param {HTMLElement} element    The element to check if it is view
     * @param {boolean} partial   true if partial view is allowed
     *
     * @returns { boolean } True if the component is in View.
     */
    function isElementInView(container, element, partial) {
        if (!container || !element) {
            return false;
        }
        const containerBounds = container.getBoundingClientRect();
        const elementBounds = element.getBoundingClientRect();
        const containerBoundsLeft = Math.floor(containerBounds.left);
        const containerBoundsRight = Math.floor(containerBounds.right);
        const elementBoundsLeft = Math.floor(elementBounds.left);
        const elementBoundsRight = Math.floor(elementBounds.right);
        // Check if in view
        const isTotallyInView = elementBoundsLeft >= containerBoundsLeft && elementBoundsRight <= containerBoundsRight;
        const isPartiallyInView = partial &&
            ((elementBoundsLeft < containerBoundsLeft && elementBoundsRight > containerBoundsLeft) ||
                (elementBoundsRight > containerBoundsRight && elementBoundsLeft < containerBoundsRight));
        // Return outcome
        return isTotallyInView || isPartiallyInView;
    }
    /** This function returns the side the element is out of view on (right, left or both)
     *
     * @param {HTMLElement} container    The container to check if the element is in view of.
     * @param {HTMLElement} element      The element to check if it is view
     *
     * @returns {string} right if the element is of the right, left if element is off the left or both if it is off on both sides.
     */
    function sideElementIsOutOfView(container, element) {
        const containerBounds = container.getBoundingClientRect();
        const elementBounds = element.getBoundingClientRect();
        const containerBoundsLeft = Math.floor(containerBounds.left);
        const containerBoundsRight = Math.floor(containerBounds.right);
        const elementBoundsLeft = Math.floor(elementBounds.left);
        const elementBoundsRight = Math.floor(elementBounds.right);
        // Check if in view
        const isOffLeft = elementBoundsLeft < containerBoundsLeft;
        const isOffRight = elementBoundsRight > containerBoundsRight;
        let side = SIDE.NONE;
        if (isOffRight && isOffLeft) {
            side = SIDE.BOTH;
        }
        else if (isOffRight) {
            side = SIDE.RIGHT;
        }
        else if (isOffLeft) {
            side = SIDE.LEFT;
        }
        // Return outcome
        return side;
    }
    /** Interpolates a parameterized templateString using values from a templateVars object.
     * The templateVars object should have keys and values which match the templateString's parameters.
     * Example:
     *    const templateString: 'My name is ${firstName} ${lastName}';
     *    const templateVars: {
     *      firstName: 'Jon'
     *      lastName: 'Dough'
     *    };
     *    const result = fillTemplate(templateString, templateVars);
     *    // "My name is Jon Dough"
     *
     * @param {string} templateString  The string passed by the consumer
     * @param {object} templateVars The variables passed to the string
     *
     * @returns {string} The template string literal result
     */
    function fillTemplate(templateString, templateVars) {
        return templateString.replace(/\${(.*?)}/g, (_, match) => templateVars[match] || '');
    }
    /**
     * This function allows for keyboard navigation through dropdowns. The custom argument is optional.
     *
     * @param {number} index The index of the element you're on
     * @param {number} innerIndex Inner index number
     * @param {string} position The orientation of the dropdown
     * @param {string[]} refsCollection Array of refs to the items in the dropdown
     * @param {object[]} kids Array of items in the dropdown
     * @param {boolean} [custom] Allows for handling of flexible content
     */
    function keyHandler(index, innerIndex, position, refsCollection, kids, custom = false) {
        if (!Array.isArray(kids)) {
            return;
        }
        const isMultiDimensional = refsCollection.filter(ref => ref)[0].constructor === Array;
        let nextIndex = index;
        let nextInnerIndex = innerIndex;
        if (position === 'up') {
            if (index === 0) {
                // loop back to end
                nextIndex = kids.length - 1;
            }
            else {
                nextIndex = index - 1;
            }
        }
        else if (position === 'down') {
            if (index === kids.length - 1) {
                // loop back to beginning
                nextIndex = 0;
            }
            else {
                nextIndex = index + 1;
            }
        }
        else if (position === 'left') {
            if (innerIndex === 0) {
                nextInnerIndex = refsCollection[index].length - 1;
            }
            else {
                nextInnerIndex = innerIndex - 1;
            }
        }
        else if (position === 'right') {
            if (innerIndex === refsCollection[index].length - 1) {
                nextInnerIndex = 0;
            }
            else {
                nextInnerIndex = innerIndex + 1;
            }
        }
        if (refsCollection[nextIndex] === null ||
            refsCollection[nextIndex] === undefined ||
            (isMultiDimensional &&
                (refsCollection[nextIndex][nextInnerIndex] === null || refsCollection[nextIndex][nextInnerIndex] === undefined))) {
            keyHandler(nextIndex, nextInnerIndex, position, refsCollection, kids, custom);
        }
        else if (custom) {
            if (refsCollection[nextIndex].focus) {
                refsCollection[nextIndex].focus();
            }
            // eslint-disable-next-line react/no-find-dom-node
            const element = ReactDOM.findDOMNode(refsCollection[nextIndex]);
            element.focus();
        }
        else {
            if (isMultiDimensional) {
                refsCollection[nextIndex][nextInnerIndex].focus();
            }
            else {
                refsCollection[nextIndex].focus();
            }
        }
    }
    /** This function is a helper for keyboard navigation through dropdowns.
     *
     * @param {number} index The index of the element you're on
     * @param {string} position The orientation of the dropdown
     * @param {string[]} collection Array of refs to the items in the dropdown
     */
    function getNextIndex(index, position, collection) {
        let nextIndex;
        if (position === 'up') {
            if (index === 0) {
                // loop back to end
                nextIndex = collection.length - 1;
            }
            else {
                nextIndex = index - 1;
            }
        }
        else if (index === collection.length - 1) {
            // loop back to beginning
            nextIndex = 0;
        }
        else {
            nextIndex = index + 1;
        }
        if (collection[nextIndex] === null) {
            getNextIndex(nextIndex, position, collection);
        }
        else {
            return nextIndex;
        }
    }
    /** This function is a helper for pluralizing strings.
     *
     * @param {number} i The quantity of the string you want to pluralize
     * @param {string} singular The singular version of the string
     * @param {string} plural The change to the string that should occur if the quantity is not equal to 1.
     *                 Defaults to adding an 's'.
     */
    function pluralize(i, singular, plural) {
        if (!plural) {
            plural = `${singular}s`;
        }
        return `${i || 0} ${i === 1 ? singular : plural}`;
    }
    /**
     * This function is a helper for turning arrays of breakpointMod objects for data toolbar and flex into classes
     *
     * @param {object} mods The modifiers object
     * @param {any} styles The appropriate styles object for the component
     */
    const formatBreakpointMods = (mods, styles) => Object.entries(mods || {})
        .map(([breakpoint, mod]) => `${mod}${breakpoint !== 'default' ? `-on-${breakpoint}` : ''}`)
        .map(toCamel)
        .map(mod => mod.replace(/-?(\dxl)/gi, (_res, group) => `_${group}`))
        .map(modifierKey => styles.modifiers[modifierKey])
        .filter(Boolean)
        .join(' ');
    const camelize = (s) => s
        .toUpperCase()
        .replace('-', '')
        .replace('_', '');
    /**
     *
     * @param {string} s string to make camelCased
     */
    const toCamel = (s) => s.replace(/([-_][a-z])/gi, camelize);
    /**
     * Copied from exenv
     */
    const canUseDOM = !!(typeof window !== 'undefined' && window.document && window.document.createElement);

    var bullseye = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "bullseye": "pf-l-bullseye"
    };
    });

    var styles$1 = unwrapExports(bullseye);

    var aboutModalBox = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "aboutModalBox": "pf-c-about-modal-box",
      "aboutModalBoxBrand": "pf-c-about-modal-box__brand",
      "aboutModalBoxBrandImage": "pf-c-about-modal-box__brand-image",
      "aboutModalBoxClose": "pf-c-about-modal-box__close",
      "aboutModalBoxContent": "pf-c-about-modal-box__content",
      "aboutModalBoxHeader": "pf-c-about-modal-box__header",
      "aboutModalBoxHero": "pf-c-about-modal-box__hero",
      "aboutModalBoxStrapline": "pf-c-about-modal-box__strapline",
      "button": "pf-c-button",
      "card": "pf-c-card",
      "modifiers": {
        "plain": "pf-m-plain"
      }
    };
    });

    var styles$2 = unwrapExports(aboutModalBox);

    var content = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "content": "pf-c-content",
      "modifiers": {
        "overpassFont": "pf-m-overpass-font"
      }
    };
    });

    var styles$3 = unwrapExports(content);

    const AboutModalBoxContent = (_a) => {
        var { children, className = '', trademark, id, noAboutModalBoxContentContainer = false } = _a, props = __rest(_a, ["children", "className", "trademark", "id", "noAboutModalBoxContentContainer"]);
        return (React.createElement("div", Object.assign({ className: css(styles$2.aboutModalBoxContent, className), id: id }, props),
            React.createElement("div", { className: css('pf-c-about-modal-box__body') }, noAboutModalBoxContentContainer ? children : React.createElement("div", { className: css(styles$3.content) }, children)),
            React.createElement("p", { className: css(styles$2.aboutModalBoxStrapline) }, trademark)));
    };
    AboutModalBoxContent.displayName = 'AboutModalBoxContent';

    var title = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "modifiers": {
        "4xl": "pf-m-4xl",
        "3xl": "pf-m-3xl",
        "2xl": "pf-m-2xl",
        "xl": "pf-m-xl",
        "lg": "pf-m-lg",
        "md": "pf-m-md",
        "overpassFont": "pf-m-overpass-font"
      },
      "title": "pf-c-title"
    };
    });

    var styles$4 = unwrapExports(title);

    (function (TitleSizes) {
        TitleSizes["md"] = "md";
        TitleSizes["lg"] = "lg";
        TitleSizes["xl"] = "xl";
        TitleSizes["2xl"] = "2xl";
        TitleSizes["3xl"] = "3xl";
        TitleSizes["4xl"] = "4xl";
    })(exports.TitleSizes || (exports.TitleSizes = {}));
    var headingLevelSizeMap;
    (function (headingLevelSizeMap) {
        headingLevelSizeMap["h1"] = "2xl";
        headingLevelSizeMap["h2"] = "xl";
        headingLevelSizeMap["h3"] = "lg";
        headingLevelSizeMap["h4"] = "md";
        headingLevelSizeMap["h5"] = "md";
        headingLevelSizeMap["h6"] = "md";
    })(headingLevelSizeMap || (headingLevelSizeMap = {}));
    const Title = (_a) => {
        var { className = '', children = '', headingLevel: HeadingLevel, size = headingLevelSizeMap[HeadingLevel] } = _a, props = __rest(_a, ["className", "children", "headingLevel", "size"]);
        return (React.createElement(HeadingLevel, Object.assign({}, props, { className: css(styles$4.title, size && styles$4.modifiers[size], className) }), children));
    };
    Title.displayName = 'Title';

    const AboutModalBoxHeader = (_a) => {
        var { className = '', productName = '', id } = _a, props = __rest(_a, ["className", "productName", "id"]);
        return (React.createElement("div", Object.assign({ className: css(styles$2.aboutModalBoxHeader, className) }, props),
            React.createElement(Title, { headingLevel: "h1", size: "4xl", id: id }, productName)));
    };
    AboutModalBoxHeader.displayName = 'AboutModalBoxHeader';

    var c_about_modal_box__hero_sm_BackgroundImage = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.c_about_modal_box__hero_sm_BackgroundImage = {
      "name": "--pf-c-about-modal-box__hero--sm--BackgroundImage",
      "value": "url(\"../../assets/images/pfbg_992@2x.jpg\")",
      "var": "var(--pf-c-about-modal-box__hero--sm--BackgroundImage)"
    };
    exports["default"] = exports.c_about_modal_box__hero_sm_BackgroundImage;
    });

    var c_about_modal_box__hero_sm_BackgroundImage$1 = unwrapExports(c_about_modal_box__hero_sm_BackgroundImage);
    var c_about_modal_box__hero_sm_BackgroundImage_1 = c_about_modal_box__hero_sm_BackgroundImage.c_about_modal_box__hero_sm_BackgroundImage;

    const AboutModalBoxHero = (_a) => {
        var { className, backgroundImageSrc } = _a, props = __rest(_a, ["className", "backgroundImageSrc"]);
        return (React.createElement("div", Object.assign({ style: 
            /* eslint-disable camelcase */
            backgroundImageSrc !== ''
                ? { [c_about_modal_box__hero_sm_BackgroundImage$1.name]: `url(${backgroundImageSrc})` }
                : {}, className: css(styles$2.aboutModalBoxHero, className) }, props)));
    };
    AboutModalBoxHero.displayName = 'AboutModalBoxHero';

    const AboutModalBoxBrand = (_a) => {
        var { className = '', src = '', alt } = _a, props = __rest(_a, ["className", "src", "alt"]);
        return (React.createElement("div", Object.assign({ className: css(styles$2.aboutModalBoxBrand, className) }, props),
            React.createElement("img", { className: css(styles$2.aboutModalBoxBrandImage), src: src, alt: alt })));
    };
    AboutModalBoxBrand.displayName = 'AboutModalBoxBrand';

    var button = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "button": "pf-c-button",
      "buttonIcon": "pf-c-button__icon",
      "modifiers": {
        "active": "pf-m-active",
        "block": "pf-m-block",
        "small": "pf-m-small",
        "primary": "pf-m-primary",
        "displayLg": "pf-m-display-lg",
        "secondary": "pf-m-secondary",
        "tertiary": "pf-m-tertiary",
        "link": "pf-m-link",
        "danger": "pf-m-danger",
        "inline": "pf-m-inline",
        "control": "pf-m-control",
        "expanded": "pf-m-expanded",
        "plain": "pf-m-plain",
        "disabled": "pf-m-disabled",
        "ariaDisabled": "pf-m-aria-disabled",
        "start": "pf-m-start",
        "end": "pf-m-end",
        "overpassFont": "pf-m-overpass-font"
      }
    };
    });

    var buttonStyles = unwrapExports(button);

    (function (ButtonVariant) {
        ButtonVariant["primary"] = "primary";
        ButtonVariant["secondary"] = "secondary";
        ButtonVariant["tertiary"] = "tertiary";
        ButtonVariant["danger"] = "danger";
        ButtonVariant["link"] = "link";
        ButtonVariant["plain"] = "plain";
        ButtonVariant["control"] = "control";
    })(exports.ButtonVariant || (exports.ButtonVariant = {}));
    (function (ButtonType) {
        ButtonType["button"] = "button";
        ButtonType["submit"] = "submit";
        ButtonType["reset"] = "reset";
    })(exports.ButtonType || (exports.ButtonType = {}));
    const Button = (_a) => {
        var { children = null, className = '', component = 'button', isActive = false, isBlock = false, isDisabled = false, isAriaDisabled = false, isSmall = false, inoperableEvents = ['onClick', 'onKeyPress'], isInline = false, type = exports.ButtonType.button, variant = exports.ButtonVariant.primary, iconPosition = 'left', 'aria-label': ariaLabel = null, icon = null, ouiaId, ouiaSafe = true, tabIndex = null } = _a, props = __rest(_a, ["children", "className", "component", "isActive", "isBlock", "isDisabled", "isAriaDisabled", "isSmall", "inoperableEvents", "isInline", "type", "variant", "iconPosition", 'aria-label', "icon", "ouiaId", "ouiaSafe", "tabIndex"]);
        const Component = component;
        const isButtonElement = Component === 'button';
        if (isAriaDisabled && 'development' !== 'production') {
            // eslint-disable-next-line no-console
            console.warn('You are using a beta component feature (isAriaDisabled). These api parts are subject to change in the future.');
        }
        const preventedEvents = inoperableEvents.reduce((handlers, eventToPrevent) => (Object.assign(Object.assign({}, handlers), { [eventToPrevent]: (event) => {
                event.preventDefault();
            } })), {});
        const getDefaultTabIdx = () => {
            if (isDisabled) {
                return isButtonElement ? null : -1;
            }
            else if (isAriaDisabled) {
                return null;
            }
        };
        return (React.createElement(Component, Object.assign({}, props, (isAriaDisabled ? preventedEvents : null), { "aria-disabled": isDisabled || isAriaDisabled, "aria-label": ariaLabel, className: css(buttonStyles.button, buttonStyles.modifiers[variant], isBlock && buttonStyles.modifiers.block, isDisabled && buttonStyles.modifiers.disabled, isAriaDisabled && buttonStyles.modifiers.ariaDisabled, isActive && buttonStyles.modifiers.active, isInline && variant === exports.ButtonVariant.link && buttonStyles.modifiers.inline, isSmall && buttonStyles.modifiers.small, className), disabled: isButtonElement ? isDisabled : null, tabIndex: tabIndex !== null ? tabIndex : getDefaultTabIdx(), type: isButtonElement ? type : null }, getOUIAProps(Button.displayName, ouiaId, ouiaSafe)),
            variant !== exports.ButtonVariant.plain && icon && iconPosition === 'left' && (React.createElement("span", { className: css(buttonStyles.buttonIcon, buttonStyles.modifiers.start) }, icon)),
            children,
            variant !== exports.ButtonVariant.plain && icon && iconPosition === 'right' && (React.createElement("span", { className: css(buttonStyles.buttonIcon, buttonStyles.modifiers.end) }, icon))));
    };
    Button.displayName = 'Button';

    var createIcon_1 = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", { value: true });

    const React = tslib_es6.__importStar(React__default);
    var IconSize;
    (function (IconSize) {
        IconSize["sm"] = "sm";
        IconSize["md"] = "md";
        IconSize["lg"] = "lg";
        IconSize["xl"] = "xl";
    })(IconSize = exports.IconSize || (exports.IconSize = {}));
    exports.getSize = (size) => {
        switch (size) {
            case IconSize.sm:
                return '1em';
            case IconSize.md:
                return '1.5em';
            case IconSize.lg:
                return '2em';
            case IconSize.xl:
                return '3em';
            default:
                return '1em';
        }
    };
    let currentId = 0;
    /**
     * Factory to create Icon class components for consumers
     */
    function createIcon({ name, xOffset = 0, yOffset = 0, width, height, svgPath, transform = '' }) {
        var _a;
        return _a = class SVGIcon extends React.Component {
                constructor() {
                    super(...arguments);
                    this.id = `icon-title-${currentId++}`;
                }
                render() {
                    const _a = this.props, { size, color, title, noVerticalAlign } = _a, props = tslib_es6.__rest(_a, ["size", "color", "title", "noVerticalAlign"]);
                    const hasTitle = Boolean(title);
                    const heightWidth = exports.getSize(size);
                    const baseAlign = -0.125 * Number.parseFloat(heightWidth);
                    const style = noVerticalAlign ? null : { verticalAlign: `${baseAlign}em` };
                    const viewBox = [xOffset, yOffset, width, height].join(' ');
                    return (React.createElement("svg", Object.assign({ style: style, fill: color, height: heightWidth, width: heightWidth, viewBox: viewBox, "aria-labelledby": hasTitle ? this.id : null, "aria-hidden": hasTitle ? null : true, role: "img" }, props),
                        hasTitle && React.createElement("title", { id: this.id }, title),
                        React.createElement("path", { d: svgPath, transform: transform })));
                }
            },
            _a.displayName = name,
            _a.defaultProps = {
                color: 'currentColor',
                size: IconSize.sm,
                noVerticalAlign: false
            },
            _a;
    }
    exports.createIcon = createIcon;

    });

    unwrapExports(createIcon_1);
    var createIcon_2 = createIcon_1.IconSize;
    var createIcon_3 = createIcon_1.getSize;
    var createIcon_4 = createIcon_1.createIcon;

    var timesIcon = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.TimesIconConfig = {
      name: 'TimesIcon',
      height: 512,
      width: 352,
      svgPath: 'M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z',
      yOffset: 0,
      xOffset: 0,
      transform: ''
    };
    exports.TimesIcon = createIcon_1.createIcon(exports.TimesIconConfig);
    exports["default"] = exports.TimesIcon;
    });

    var TimesIcon = unwrapExports(timesIcon);
    var timesIcon_1 = timesIcon.TimesIconConfig;
    var timesIcon_2 = timesIcon.TimesIcon;

    const AboutModalBoxCloseButton = (_a) => {
        var { className = '', onClose = () => undefined, 'aria-label': ariaLabel = 'Close Dialog' } = _a, props = __rest(_a, ["className", "onClose", 'aria-label']);
        return (React.createElement("div", Object.assign({ className: css(styles$2.aboutModalBoxClose, className) }, props),
            React.createElement(Button, { variant: "plain", onClick: onClose, "aria-label": ariaLabel },
                React.createElement(TimesIcon, null))));
    };
    AboutModalBoxCloseButton.displayName = 'AboutModalBoxCloseButton';

    const AboutModalBox = (_a) => {
        var { children, className = '' } = _a, props = __rest(_a, ["children", "className"]);
        return (React.createElement("div", Object.assign({ role: "dialog", "aria-modal": "true", className: css(styles$2.aboutModalBox, className) }, props), children));
    };
    AboutModalBox.displayName = 'AboutModalBox';

    const Backdrop = (_a) => {
        var { children = null, className = '' } = _a, props = __rest(_a, ["children", "className"]);
        return (React.createElement("div", Object.assign({}, props, { className: css(styles.backdrop, className) }), children));
    };
    Backdrop.displayName = 'Backdrop';

    const AboutModalContainer = (_a) => {
        var { children, className = '', isOpen = false, onClose = () => undefined, productName = '', trademark, brandImageSrc, brandImageAlt, backgroundImageSrc, closeButtonAriaLabel, aboutModalBoxHeaderId, aboutModalBoxContentId } = _a, props = __rest(_a, ["children", "className", "isOpen", "onClose", "productName", "trademark", "brandImageSrc", "brandImageAlt", "backgroundImageSrc", "closeButtonAriaLabel", "aboutModalBoxHeaderId", "aboutModalBoxContentId"]);
        if (!isOpen) {
            return null;
        }
        return (React.createElement(Backdrop, null,
            React.createElement(FocusTrap, { focusTrapOptions: { clickOutsideDeactivates: true }, className: css(styles$1.bullseye) },
                React.createElement(AboutModalBox, { className: className, "aria-labelledby": aboutModalBoxHeaderId, "aria-describedby": aboutModalBoxContentId },
                    React.createElement(AboutModalBoxBrand, { src: brandImageSrc, alt: brandImageAlt }),
                    React.createElement(AboutModalBoxCloseButton, { "aria-label": closeButtonAriaLabel, onClose: onClose }),
                    productName && React.createElement(AboutModalBoxHeader, { id: aboutModalBoxHeaderId, productName: productName }),
                    React.createElement(AboutModalBoxContent, Object.assign({ trademark: trademark, id: aboutModalBoxContentId, noAboutModalBoxContentContainer: false }, props), children),
                    React.createElement(AboutModalBoxHero, { backgroundImageSrc: backgroundImageSrc })))));
    };
    AboutModalContainer.displayName = 'AboutModalContainer';

    class AboutModal extends React.Component {
        constructor(props) {
            super(props);
            this.id = AboutModal.currentId++;
            this.ariaLabelledBy = `pf-about-modal-title-${this.id}`;
            this.ariaDescribedBy = `pf-about-modal-content-${this.id}`;
            this.handleEscKeyClick = (event) => {
                if (event.keyCode === KEY_CODES.ESCAPE_KEY && this.props.isOpen) {
                    this.props.onClose();
                }
            };
            this.toggleSiblingsFromScreenReaders = (hide) => {
                const { appendTo } = this.props;
                const target = this.getElement(appendTo);
                const bodyChildren = target.children;
                for (const child of Array.from(bodyChildren)) {
                    if (child !== this.state.container) {
                        hide ? child.setAttribute('aria-hidden', '' + hide) : child.removeAttribute('aria-hidden');
                    }
                }
            };
            this.getElement = (appendTo) => {
                if (typeof appendTo === 'function') {
                    return appendTo();
                }
                return appendTo || document.body;
            };
            this.state = {
                container: undefined
            };
            if (props.brandImageSrc && !props.brandImageAlt) {
                // eslint-disable-next-line no-console
                console.error('AboutModal:', 'brandImageAlt is required when a brandImageSrc is specified');
            }
        }
        componentDidMount() {
            const container = document.createElement('div');
            const target = this.getElement(this.props.appendTo);
            this.setState({ container });
            target.appendChild(container);
            target.addEventListener('keydown', this.handleEscKeyClick, false);
            if (this.props.isOpen) {
                target.classList.add(css(styles.backdropOpen));
            }
            else {
                target.classList.remove(css(styles.backdropOpen));
            }
        }
        componentDidUpdate() {
            const target = this.getElement(this.props.appendTo);
            if (this.props.isOpen) {
                target.classList.add(css(styles.backdropOpen));
                this.toggleSiblingsFromScreenReaders(true);
            }
            else {
                target.classList.remove(css(styles.backdropOpen));
                this.toggleSiblingsFromScreenReaders(false);
            }
        }
        componentWillUnmount() {
            const target = this.getElement(this.props.appendTo);
            if (this.state.container) {
                target.removeChild(this.state.container);
            }
            target.removeEventListener('keydown', this.handleEscKeyClick, false);
            target.classList.remove(css(styles.backdropOpen));
        }
        render() {
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            const _a = this.props, props = __rest(_a, ["appendTo"]);
            const { container } = this.state;
            if (!canUseDOM || !container) {
                return null;
            }
            return ReactDOM.createPortal(React.createElement(AboutModalContainer, Object.assign({ aboutModalBoxHeaderId: this.ariaLabelledBy, aboutModalBoxContentId: this.ariaDescribedBy }, props)), container);
        }
    }
    AboutModal.displayName = 'AboutModal';
    AboutModal.currentId = 0;
    AboutModal.defaultProps = {
        className: '',
        isOpen: false,
        onClose: () => undefined,
        productName: '',
        trademark: '',
        backgroundImageSrc: '',
        noAboutModalBoxContentContainer: false,
        appendTo: null
    };

    var accordion = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "accordion": "pf-c-accordion",
      "accordionExpandedContent": "pf-c-accordion__expanded-content",
      "accordionExpandedContentBody": "pf-c-accordion__expanded-content-body",
      "accordionToggle": "pf-c-accordion__toggle",
      "accordionToggleIcon": "pf-c-accordion__toggle-icon",
      "accordionToggleText": "pf-c-accordion__toggle-text",
      "modifiers": {
        "expanded": "pf-m-expanded",
        "fixed": "pf-m-fixed"
      }
    };
    });

    var styles$5 = unwrapExports(accordion);

    const AccordionContext = React.createContext({});

    const Accordion = (_a) => {
        var { children = null, className = '', 'aria-label': ariaLabel = '', headingLevel = 'h3', asDefinitionList = true } = _a, props = __rest(_a, ["children", "className", 'aria-label', "headingLevel", "asDefinitionList"]);
        const AccordionList = asDefinitionList ? 'dl' : 'div';
        return (React.createElement(AccordionList, Object.assign({ className: css(styles$5.accordion, className), "aria-label": ariaLabel }, props),
            React.createElement(AccordionContext.Provider, { value: {
                    ContentContainer: asDefinitionList ? 'dd' : 'div',
                    ToggleContainer: asDefinitionList ? 'dt' : headingLevel
                } }, children)));
    };
    Accordion.displayName = 'Accordion';

    const AccordionItem = ({ children = null }) => (React.createElement(React.Fragment, null, children));
    AccordionItem.displayName = 'AccordionItem';

    const AccordionContent = (_a) => {
        var { className = '', children = null, id = '', isHidden = false, isFixed = false, 'aria-label': ariaLabel = '', component } = _a, props = __rest(_a, ["className", "children", "id", "isHidden", "isFixed", 'aria-label', "component"]);
        return (React.createElement(AccordionContext.Consumer, null, ({ ContentContainer }) => {
            const Container = component || ContentContainer;
            return (React.createElement(Container, Object.assign({ id: id, className: css(styles$5.accordionExpandedContent, isFixed && styles$5.modifiers.fixed, !isHidden && styles$5.modifiers.expanded, className), hidden: isHidden, "aria-label": ariaLabel }, props),
                React.createElement("div", { className: css(styles$5.accordionExpandedContentBody) }, children)));
        }));
    };
    AccordionContent.displayName = 'AccordionContent';

    var angleRightIcon = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.AngleRightIconConfig = {
      name: 'AngleRightIcon',
      height: 512,
      width: 256,
      svgPath: 'M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z',
      yOffset: 0,
      xOffset: 0,
      transform: ''
    };
    exports.AngleRightIcon = createIcon_1.createIcon(exports.AngleRightIconConfig);
    exports["default"] = exports.AngleRightIcon;
    });

    var AngleRightIcon = unwrapExports(angleRightIcon);
    var angleRightIcon_1 = angleRightIcon.AngleRightIconConfig;
    var angleRightIcon_2 = angleRightIcon.AngleRightIcon;

    const AccordionToggle = (_a) => {
        var { className = '', id, isExpanded = false, children = null, component } = _a, props = __rest(_a, ["className", "id", "isExpanded", "children", "component"]);
        return (React.createElement(AccordionContext.Consumer, null, ({ ToggleContainer }) => {
            const Container = component || ToggleContainer;
            return (React.createElement(Container, null,
                React.createElement("button", Object.assign({ id: id, className: css(styles$5.accordionToggle, isExpanded && styles$5.modifiers.expanded, className) }, props, { "aria-expanded": isExpanded }),
                    React.createElement("span", { className: css(styles$5.accordionToggleText) }, children),
                    React.createElement("span", { className: css(styles$5.accordionToggleIcon) },
                        React.createElement(AngleRightIcon, null)))));
        }));
    };
    AccordionToggle.displayName = 'AccordionToggle';

    var alert = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "alert": "pf-c-alert",
      "alertAction": "pf-c-alert__action",
      "alertActionGroup": "pf-c-alert__action-group",
      "alertDescription": "pf-c-alert__description",
      "alertIcon": "pf-c-alert__icon",
      "alertTitle": "pf-c-alert__title",
      "button": "pf-c-button",
      "modifiers": {
        "success": "pf-m-success",
        "danger": "pf-m-danger",
        "warning": "pf-m-warning",
        "info": "pf-m-info",
        "inline": "pf-m-inline",
        "truncate": "pf-m-truncate",
        "overpassFont": "pf-m-overpass-font"
      }
    };
    });

    var styles$6 = unwrapExports(alert);

    var accessibility = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "hidden": "pf-u-hidden",
      "hiddenOnLg": "pf-u-hidden-on-lg",
      "hiddenOnMd": "pf-u-hidden-on-md",
      "hiddenOnSm": "pf-u-hidden-on-sm",
      "hiddenOnXl": "pf-u-hidden-on-xl",
      "hiddenOn_2xl": "pf-u-hidden-on-2xl",
      "screenReader": "pf-u-screen-reader",
      "screenReaderOnLg": "pf-u-screen-reader-on-lg",
      "screenReaderOnMd": "pf-u-screen-reader-on-md",
      "screenReaderOnSm": "pf-u-screen-reader-on-sm",
      "screenReaderOnXl": "pf-u-screen-reader-on-xl",
      "screenReaderOn_2xl": "pf-u-screen-reader-on-2xl",
      "visible": "pf-u-visible",
      "visibleOnLg": "pf-u-visible-on-lg",
      "visibleOnMd": "pf-u-visible-on-md",
      "visibleOnSm": "pf-u-visible-on-sm",
      "visibleOnXl": "pf-u-visible-on-xl",
      "visibleOn_2xl": "pf-u-visible-on-2xl"
    };
    });

    var a11yStyles = unwrapExports(accessibility);

    var checkCircleIcon = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.CheckCircleIconConfig = {
      name: 'CheckCircleIcon',
      height: 512,
      width: 512,
      svgPath: 'M504 256c0 136.967-111.033 248-248 248S8 392.967 8 256 119.033 8 256 8s248 111.033 248 248zM227.314 387.314l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.249-16.379-6.249-22.628 0L216 308.118l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.249 16.379 6.249 22.628.001z',
      yOffset: 0,
      xOffset: 0,
      transform: ''
    };
    exports.CheckCircleIcon = createIcon_1.createIcon(exports.CheckCircleIconConfig);
    exports["default"] = exports.CheckCircleIcon;
    });

    var CheckCircleIcon = unwrapExports(checkCircleIcon);
    var checkCircleIcon_1 = checkCircleIcon.CheckCircleIconConfig;
    var checkCircleIcon_2 = checkCircleIcon.CheckCircleIcon;

    var exclamationCircleIcon = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.ExclamationCircleIconConfig = {
      name: 'ExclamationCircleIcon',
      height: 512,
      width: 512,
      svgPath: 'M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z',
      yOffset: 0,
      xOffset: 0,
      transform: ''
    };
    exports.ExclamationCircleIcon = createIcon_1.createIcon(exports.ExclamationCircleIconConfig);
    exports["default"] = exports.ExclamationCircleIcon;
    });

    var ExclamationCircleIcon = unwrapExports(exclamationCircleIcon);
    var exclamationCircleIcon_1 = exclamationCircleIcon.ExclamationCircleIconConfig;
    var exclamationCircleIcon_2 = exclamationCircleIcon.ExclamationCircleIcon;

    var exclamationTriangleIcon = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.ExclamationTriangleIconConfig = {
      name: 'ExclamationTriangleIcon',
      height: 512,
      width: 576,
      svgPath: 'M569.517 440.013C587.975 472.007 564.806 512 527.94 512H48.054c-36.937 0-59.999-40.055-41.577-71.987L246.423 23.985c18.467-32.009 64.72-31.951 83.154 0l239.94 416.028zM288 354c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z',
      yOffset: 0,
      xOffset: 0,
      transform: ''
    };
    exports.ExclamationTriangleIcon = createIcon_1.createIcon(exports.ExclamationTriangleIconConfig);
    exports["default"] = exports.ExclamationTriangleIcon;
    });

    var ExclamationTriangleIcon = unwrapExports(exclamationTriangleIcon);
    var exclamationTriangleIcon_1 = exclamationTriangleIcon.ExclamationTriangleIconConfig;
    var exclamationTriangleIcon_2 = exclamationTriangleIcon.ExclamationTriangleIcon;

    var infoCircleIcon = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.InfoCircleIconConfig = {
      name: 'InfoCircleIcon',
      height: 512,
      width: 512,
      svgPath: 'M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 110c23.196 0 42 18.804 42 42s-18.804 42-42 42-42-18.804-42-42 18.804-42 42-42zm56 254c0 6.627-5.373 12-12 12h-88c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h12v-64h-12c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h64c6.627 0 12 5.373 12 12v100h12c6.627 0 12 5.373 12 12v24z',
      yOffset: 0,
      xOffset: 0,
      transform: ''
    };
    exports.InfoCircleIcon = createIcon_1.createIcon(exports.InfoCircleIconConfig);
    exports["default"] = exports.InfoCircleIcon;
    });

    var InfoCircleIcon = unwrapExports(infoCircleIcon);
    var infoCircleIcon_1 = infoCircleIcon.InfoCircleIconConfig;
    var infoCircleIcon_2 = infoCircleIcon.InfoCircleIcon;

    var bellIcon = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.BellIconConfig = {
      name: 'BellIcon',
      height: 512,
      width: 448,
      svgPath: 'M224 512c35.32 0 63.97-28.65 63.97-64H160.03c0 35.35 28.65 64 63.97 64zm215.39-149.71c-19.32-20.76-55.47-51.99-55.47-154.29 0-77.7-54.48-139.9-127.94-155.16V32c0-17.67-14.32-32-31.98-32s-31.98 14.33-31.98 32v20.84C118.56 68.1 64.08 130.3 64.08 208c0 102.3-36.15 133.53-55.47 154.29-6 6.45-8.66 14.16-8.61 21.71.11 16.4 12.98 32 32.1 32h383.8c19.12 0 32-15.6 32.1-32 .05-7.55-2.61-15.27-8.61-21.71z',
      yOffset: 0,
      xOffset: 0,
      transform: ''
    };
    exports.BellIcon = createIcon_1.createIcon(exports.BellIconConfig);
    exports["default"] = exports.BellIcon;
    });

    var BellIcon = unwrapExports(bellIcon);
    var bellIcon_1 = bellIcon.BellIconConfig;
    var bellIcon_2 = bellIcon.BellIcon;

    const variantIcons = {
        success: CheckCircleIcon,
        danger: ExclamationCircleIcon,
        warning: ExclamationTriangleIcon,
        info: InfoCircleIcon,
        default: BellIcon
    };
    const AlertIcon = (_a) => {
        var { variant, className = '' } = _a, props = __rest(_a, ["variant", "className"]);
        const Icon = variantIcons[variant];
        return (React.createElement("div", Object.assign({}, props, { className: css(styles$6.alertIcon, className) }),
            React.createElement(Icon, null)));
    };

    const AlertContext = React.createContext(null);

    (function (AlertVariant) {
        AlertVariant["success"] = "success";
        AlertVariant["danger"] = "danger";
        AlertVariant["warning"] = "warning";
        AlertVariant["info"] = "info";
        AlertVariant["default"] = "default";
    })(exports.AlertVariant || (exports.AlertVariant = {}));
    const Alert = (_a) => {
        var { variant = exports.AlertVariant.default, isInline = false, isLiveRegion = false, variantLabel = `${capitalize(variant)} alert:`, 'aria-label': ariaLabel = `${capitalize(variant)} Alert`, actionClose, actionLinks, title, children = '', className = '', ouiaId, ouiaSafe = true, timeout = false } = _a, props = __rest(_a, ["variant", "isInline", "isLiveRegion", "variantLabel", 'aria-label', "actionClose", "actionLinks", "title", "children", "className", "ouiaId", "ouiaSafe", "timeout"]);
        const getHeadingContent = (React.createElement(React.Fragment, null,
            React.createElement("span", { className: css(a11yStyles.screenReader) }, variantLabel),
            title));
        const [disableAlert, setDisableAlert] = React.useState(false);
        const customClassName = css(styles$6.alert, isInline && styles$6.modifiers.inline, variant !== exports.AlertVariant.default && styles$6.modifiers[variant], className);
        if (disableAlert === false && timeout && timeout !== 0) {
            setTimeout(() => {
                setDisableAlert(true);
            }, timeout === true ? 8000 : timeout);
        }
        if (disableAlert === false) {
            return (React.createElement("div", Object.assign({}, props, { className: customClassName, "aria-label": ariaLabel }, getOUIAProps(Alert.displayName, ouiaId, ouiaSafe), (isLiveRegion && {
                'aria-live': 'polite',
                'aria-atomic': 'false'
            })),
                React.createElement(AlertIcon, { variant: variant }),
                React.createElement("h4", { className: css(styles$6.alertTitle) }, getHeadingContent),
                actionClose && (React.createElement(AlertContext.Provider, { value: { title, variantLabel } },
                    React.createElement("div", { className: css(styles$6.alertAction) }, actionClose))),
                children && React.createElement("div", { className: css(styles$6.alertDescription) }, children),
                actionLinks && React.createElement("div", { className: css(styles$6.alertActionGroup) }, actionLinks)));
        }
        else {
            return null;
        }
    };
    Alert.displayName = 'Alert';

    const AlertActionCloseButton = (_a) => {
        var { 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        className = '', onClose = () => undefined, 'aria-label': ariaLabel = '', variantLabel } = _a, props = __rest(_a, ["className", "onClose", 'aria-label', "variantLabel"]);
        return (React.createElement(AlertContext.Consumer, null, ({ title, variantLabel: alertVariantLabel }) => (React.createElement(Button, Object.assign({ variant: exports.ButtonVariant.plain, onClick: onClose, "aria-label": ariaLabel === '' ? `Close ${variantLabel || alertVariantLabel} alert: ${title}` : ariaLabel }, props),
            React.createElement(TimesIcon, null)))));
    };
    AlertActionCloseButton.displayName = 'AlertActionCloseButton';

    const AlertActionLink = (_a) => {
        var { className = '', children } = _a, props = __rest(_a, ["className", "children"]);
        return (React.createElement(Button, Object.assign({ variant: exports.ButtonVariant.link, isInline: true, className: className }, props), children));
    };
    AlertActionLink.displayName = 'AlertActionLink';

    var alertGroup = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "alertGroup": "pf-c-alert-group",
      "modifiers": {
        "toast": "pf-m-toast"
      }
    };
    });

    var styles$7 = unwrapExports(alertGroup);

    const AlertGroupInline = (_a) => {
        var { className, children, isToast } = _a, rest = __rest(_a, ["className", "children", "isToast"]);
        return (React.createElement("ul", Object.assign({ className: css(styles$7.alertGroup, className, isToast ? styles$7.modifiers.toast : '') }, rest), React.Children.toArray(children).map((Alert, index) => (React.createElement("li", { key: index }, Alert)))));
    };
    AlertGroupInline.displayName = 'AlertGroupInline';

    class AlertGroup extends React.Component {
        constructor() {
            super(...arguments);
            this.state = {
                container: undefined
            };
        }
        componentDidMount() {
            const container = document.createElement('div');
            const target = this.getTargetElement();
            this.setState({ container });
            target.appendChild(container);
        }
        componentWillUnmount() {
            const target = this.getTargetElement();
            if (this.state.container) {
                target.removeChild(this.state.container);
            }
        }
        getTargetElement() {
            const appendTo = this.props.appendTo;
            if (typeof appendTo === 'function') {
                return appendTo();
            }
            return appendTo || document.body;
        }
        render() {
            const { className, children, isToast } = this.props;
            const alertGroup = (React.createElement(AlertGroupInline, { className: className, isToast: isToast }, children));
            if (!this.props.isToast) {
                return alertGroup;
            }
            const container = this.state.container;
            if (!canUseDOM || !container) {
                return null;
            }
            return ReactDOM.createPortal(alertGroup, container);
        }
    }
    AlertGroup.displayName = 'AlertGroup';

    var appLauncher = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "appLauncher": "pf-c-app-launcher",
      "appLauncherGroup": "pf-c-app-launcher__group",
      "appLauncherGroupTitle": "pf-c-app-launcher__group-title",
      "appLauncherMenu": "pf-c-app-launcher__menu",
      "appLauncherMenuItem": "pf-c-app-launcher__menu-item",
      "appLauncherMenuItemExternalIcon": "pf-c-app-launcher__menu-item-external-icon",
      "appLauncherMenuItemIcon": "pf-c-app-launcher__menu-item-icon",
      "appLauncherMenuSearch": "pf-c-app-launcher__menu-search",
      "appLauncherMenuWrapper": "pf-c-app-launcher__menu-wrapper",
      "appLauncherToggle": "pf-c-app-launcher__toggle",
      "divider": "pf-c-divider",
      "modifiers": {
        "expanded": "pf-m-expanded",
        "active": "pf-m-active",
        "alignRight": "pf-m-align-right",
        "top": "pf-m-top",
        "favorite": "pf-m-favorite",
        "disabled": "pf-m-disabled",
        "external": "pf-m-external",
        "link": "pf-m-link",
        "action": "pf-m-action"
      }
    };
    });

    var styles$8 = unwrapExports(appLauncher);

    var formControl = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "formControl": "pf-c-form-control",
      "modifiers": {
        "success": "pf-m-success",
        "warning": "pf-m-warning",
        "search": "pf-m-search",
        "resizeVertical": "pf-m-resize-vertical",
        "resizeHorizontal": "pf-m-resize-horizontal"
      }
    };
    });

    var formStyles = unwrapExports(formControl);

    var thIcon = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.ThIconConfig = {
      name: 'ThIcon',
      height: 512,
      width: 512,
      svgPath: 'M149.333 56v80c0 13.255-10.745 24-24 24H24c-13.255 0-24-10.745-24-24V56c0-13.255 10.745-24 24-24h101.333c13.255 0 24 10.745 24 24zm181.334 240v-80c0-13.255-10.745-24-24-24H205.333c-13.255 0-24 10.745-24 24v80c0 13.255 10.745 24 24 24h101.333c13.256 0 24.001-10.745 24.001-24zm32-240v80c0 13.255 10.745 24 24 24H488c13.255 0 24-10.745 24-24V56c0-13.255-10.745-24-24-24H386.667c-13.255 0-24 10.745-24 24zm-32 80V56c0-13.255-10.745-24-24-24H205.333c-13.255 0-24 10.745-24 24v80c0 13.255 10.745 24 24 24h101.333c13.256 0 24.001-10.745 24.001-24zm-205.334 56H24c-13.255 0-24 10.745-24 24v80c0 13.255 10.745 24 24 24h101.333c13.255 0 24-10.745 24-24v-80c0-13.255-10.745-24-24-24zM0 376v80c0 13.255 10.745 24 24 24h101.333c13.255 0 24-10.745 24-24v-80c0-13.255-10.745-24-24-24H24c-13.255 0-24 10.745-24 24zm386.667-56H488c13.255 0 24-10.745 24-24v-80c0-13.255-10.745-24-24-24H386.667c-13.255 0-24 10.745-24 24v80c0 13.255 10.745 24 24 24zm0 160H488c13.255 0 24-10.745 24-24v-80c0-13.255-10.745-24-24-24H386.667c-13.255 0-24 10.745-24 24v80c0 13.255 10.745 24 24 24zM181.333 376v80c0 13.255 10.745 24 24 24h101.333c13.255 0 24-10.745 24-24v-80c0-13.255-10.745-24-24-24H205.333c-13.255 0-24 10.745-24 24z',
      yOffset: 0,
      xOffset: 0,
      transform: ''
    };
    exports.ThIcon = createIcon_1.createIcon(exports.ThIconConfig);
    exports["default"] = exports.ThIcon;
    });

    var ThIcon = unwrapExports(thIcon);
    var thIcon_1 = thIcon.ThIconConfig;
    var thIcon_2 = thIcon.ThIcon;

    var dropdown = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "divider": "pf-c-divider",
      "dropdown": "pf-c-dropdown",
      "dropdownGroup": "pf-c-dropdown__group",
      "dropdownGroupTitle": "pf-c-dropdown__group-title",
      "dropdownMenu": "pf-c-dropdown__menu",
      "dropdownMenuItem": "pf-c-dropdown__menu-item",
      "dropdownMenuItemDescription": "pf-c-dropdown__menu-item-description",
      "dropdownMenuItemIcon": "pf-c-dropdown__menu-item-icon",
      "dropdownMenuItemMain": "pf-c-dropdown__menu-item-main",
      "dropdownToggle": "pf-c-dropdown__toggle",
      "dropdownToggleButton": "pf-c-dropdown__toggle-button",
      "dropdownToggleCheck": "pf-c-dropdown__toggle-check",
      "dropdownToggleIcon": "pf-c-dropdown__toggle-icon",
      "dropdownToggleImage": "pf-c-dropdown__toggle-image",
      "dropdownToggleText": "pf-c-dropdown__toggle-text",
      "modifiers": {
        "action": "pf-m-action",
        "disabled": "pf-m-disabled",
        "plain": "pf-m-plain",
        "splitButton": "pf-m-split-button",
        "active": "pf-m-active",
        "expanded": "pf-m-expanded",
        "primary": "pf-m-primary",
        "top": "pf-m-top",
        "alignRight": "pf-m-align-right",
        "icon": "pf-m-icon",
        "description": "pf-m-description",
        "text": "pf-m-text"
      }
    };
    });

    var styles$9 = unwrapExports(dropdown);

    (function (DropdownPosition) {
        DropdownPosition["right"] = "right";
        DropdownPosition["left"] = "left";
    })(exports.DropdownPosition || (exports.DropdownPosition = {}));
    (function (DropdownDirection) {
        DropdownDirection["up"] = "up";
        DropdownDirection["down"] = "down";
    })(exports.DropdownDirection || (exports.DropdownDirection = {}));
    const DropdownContext = React.createContext({
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        onSelect: (event) => undefined,
        id: '',
        toggleIndicatorClass: '',
        toggleIconClass: '',
        toggleTextClass: '',
        menuClass: '',
        itemClass: '',
        toggleClass: '',
        baseClass: '',
        baseComponent: 'div',
        sectionClass: '',
        sectionTitleClass: '',
        sectionComponent: 'section',
        disabledClass: '',
        plainTextClass: '',
        menuComponent: 'ul'
    });
    const DropdownArrowContext = React.createContext({
        keyHandler: null,
        sendRef: null
    });

    class DropdownMenu extends React.Component {
        constructor() {
            super(...arguments);
            this.refsCollection = [];
            this.childKeyHandler = (index, innerIndex, position, custom = false) => {
                keyHandler(index, innerIndex, position, this.refsCollection, this.props.isGrouped ? this.refsCollection : React.Children.toArray(this.props.children), custom);
            };
            this.sendRef = (index, nodes, isDisabled, isSeparator) => {
                this.refsCollection[index] = [];
                nodes.map((node, innerIndex) => {
                    if (!node) {
                        this.refsCollection[index][innerIndex] = null;
                    }
                    else if (!node.getAttribute) {
                        // eslint-disable-next-line react/no-find-dom-node
                        this.refsCollection[index][innerIndex] = ReactDOM.findDOMNode(node);
                    }
                    else if (isDisabled || isSeparator) {
                        this.refsCollection[index][innerIndex] = null;
                    }
                    else {
                        this.refsCollection[index][innerIndex] = node;
                    }
                });
            };
        }
        componentDidMount() {
            const { autoFocus } = this.props;
            if (autoFocus) {
                // Focus first non-disabled element
                const focusTargetCollection = this.refsCollection.find(ref => ref && ref[0] && !ref[0].hasAttribute('disabled'));
                const focusTarget = focusTargetCollection && focusTargetCollection[0];
                if (focusTarget && focusTarget.focus) {
                    setTimeout(() => focusTarget.focus());
                }
            }
        }
        shouldComponentUpdate() {
            // reset refsCollection before updating to account for child removal between mounts
            this.refsCollection = [];
            return true;
        }
        extendChildren() {
            const { children, isGrouped } = this.props;
            if (isGrouped) {
                let index = 0;
                return React.Children.map(children, groupedChildren => {
                    const group = groupedChildren;
                    return React.cloneElement(group, Object.assign({}, (group.props &&
                        group.props.children && {
                        children: (group.props.children.constructor === Array &&
                            React.Children.map(group.props.children, (option) => React.cloneElement(option, {
                                index: index++
                            }))) ||
                            React.cloneElement(group.props.children, {
                                index: index++
                            })
                    })));
                });
            }
            return React.Children.map(children, (child, index) => React.cloneElement(child, {
                index
            }));
        }
        render() {
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            const _a = this.props, { className, isOpen, position, children, component, isGrouped, openedOnEnter, setMenuComponentRef } = _a, props = __rest(_a, ["className", "isOpen", "position", "children", "component", "isGrouped", "openedOnEnter", "setMenuComponentRef"]);
            return (React.createElement(DropdownArrowContext.Provider, { value: {
                    keyHandler: this.childKeyHandler,
                    sendRef: this.sendRef
                } }, component === 'div' ? (React.createElement(DropdownContext.Consumer, null, ({ onSelect, menuClass }) => (React.createElement("div", { className: css(menuClass, position === exports.DropdownPosition.right && styles$9.modifiers.alignRight, className), hidden: !isOpen, onClick: event => onSelect && onSelect(event), ref: setMenuComponentRef }, children)))) : ((isGrouped && (React.createElement(DropdownContext.Consumer, null, ({ menuClass, menuComponent }) => {
                const MenuComponent = (menuComponent || 'div');
                return (React.createElement(MenuComponent, Object.assign({}, props, { className: css(menuClass, position === exports.DropdownPosition.right && styles$9.modifiers.alignRight, className), hidden: !isOpen, role: "menu", ref: setMenuComponentRef }), this.extendChildren()));
            }))) || (React.createElement(DropdownContext.Consumer, null, ({ menuClass, menuComponent }) => {
                const MenuComponent = (menuComponent || component);
                return (React.createElement(MenuComponent, Object.assign({}, props, { className: css(menuClass, position === exports.DropdownPosition.right && styles$9.modifiers.alignRight, className), hidden: !isOpen, role: "menu", ref: setMenuComponentRef }), this.extendChildren()));
            })))));
        }
    }
    DropdownMenu.displayName = 'DropdownMenu';
    DropdownMenu.defaultProps = {
        className: '',
        isOpen: true,
        openedOnEnter: false,
        autoFocus: true,
        position: exports.DropdownPosition.left,
        component: 'ul',
        isGrouped: false,
        setMenuComponentRef: null
    };

    /**
     * This component wraps any ReactNode and finds its ref
     * It has to be a class for findDOMNode to work
     * Ideally, all components used as triggers/toggles are either:
     * - class based components we can assign our own ref to
     * - functional components that have forwardRef implemented
     * However, there is no guarantee that is what will get passed in as trigger/toggle in the case of tooltips and popovers
     */
    class FindRefWrapper extends React.Component {
        componentDidMount() {
            // eslint-disable-next-line react/no-find-dom-node
            const root = ReactDOM.findDOMNode(this);
            this.props.onFoundRef(root);
        }
        render() {
            return this.props.children;
        }
    }
    FindRefWrapper.displayName = 'FindRefWrapper';

    /**
     * @param element
     */
    function getBoundingClientRect(element) {
        const rect = element.getBoundingClientRect();
        return {
            width: rect.width,
            height: rect.height,
            top: rect.top,
            right: rect.right,
            bottom: rect.bottom,
            left: rect.left,
            x: rect.left,
            y: rect.top
        };
    }

    // @ts-nocheck
    /* :: import type { Window } from '../types'; */
    /* :: declare function getWindow(node: Node | Window): Window; */
    /**
     * @param node
     */
    function getWindow(node) {
        if (node.toString() !== '[object Window]') {
            const ownerDocument = node.ownerDocument;
            return ownerDocument ? ownerDocument.defaultView : window;
        }
        return node;
    }

    // @ts-nocheck
    /**
     * @param node
     */
    function getWindowScroll(node) {
        const win = getWindow(node);
        const scrollLeft = win.pageXOffset;
        const scrollTop = win.pageYOffset;
        return {
            scrollLeft,
            scrollTop
        };
    }

    // @ts-nocheck
    /* :: declare function isElement(node: mixed): boolean %checks(node instanceof
      Element); */
    /**
     * @param node
     */
    function isElement(node) {
        const OwnElement = getWindow(node).Element;
        return node instanceof OwnElement || node instanceof Element;
    }
    /* :: declare function isHTMLElement(node: mixed): boolean %checks(node instanceof
      HTMLElement); */
    /**
     * @param node
     */
    function isHTMLElement(node) {
        const OwnElement = getWindow(node).HTMLElement;
        return node instanceof OwnElement || node instanceof HTMLElement;
    }

    // @ts-nocheck
    /**
     * @param element
     */
    function getHTMLElementScroll(element) {
        return {
            scrollLeft: element.scrollLeft,
            scrollTop: element.scrollTop
        };
    }

    // @ts-nocheck
    /**
     * @param node
     */
    function getNodeScroll(node) {
        if (node === getWindow(node) || !isHTMLElement(node)) {
            return getWindowScroll(node);
        }
        else {
            return getHTMLElementScroll(node);
        }
    }

    /**
     * @param element
     */
    function getNodeName(element) {
        return element ? (element.nodeName || '').toLowerCase() : null;
    }

    // @ts-nocheck
    /**
     * @param element
     */
    function getDocumentElement(element) {
        // $FlowFixMe: assume body is always available
        return (isElement(element) ? element.ownerDocument : element.document).documentElement;
    }

    // @ts-nocheck
    /**
     * @param element
     */
    function getWindowScrollBarX(element) {
        // If <html> has a CSS width greater than the viewport, then this will be
        // incorrect for RTL.
        // Popper 1 is broken in this case and never had a bug report so let's assume
        // it's not an issue. I don't think anyone ever specifies width on <html>
        // anyway.
        // Browsers where the left scrollbar doesn't cause an issue report `0` for
        // this (e.g. Edge 2019, IE11, Safari)
        return getBoundingClientRect(getDocumentElement(element)).left + getWindowScroll(element).scrollLeft;
    }

    // @ts-nocheck
    /**
     * @param element
     */
    function getComputedStyle(element) {
        return getWindow(element).getComputedStyle(element);
    }

    // @ts-nocheck
    /**
     * @param element
     */
    function isScrollParent(element) {
        // Firefox wants us to check `-x` and `-y` variations as well
        const { overflow, overflowX, overflowY } = getComputedStyle(element);
        return /auto|scroll|overlay|hidden/.test(overflow + overflowY + overflowX);
    }

    // Returns the composite rect of an element relative to its offsetParent.
    // Composite means it takes into account transforms as well as layout.
    /**
     * @param elementOrVirtualElement
     * @param offsetParent
     * @param isFixed
     */
    function getCompositeRect(elementOrVirtualElement, offsetParent, isFixed = false) {
        const documentElement = getDocumentElement(offsetParent);
        const rect = getBoundingClientRect(elementOrVirtualElement);
        const isOffsetParentAnElement = isHTMLElement(offsetParent);
        let scroll = { scrollLeft: 0, scrollTop: 0 };
        let offsets = { x: 0, y: 0 };
        if (isOffsetParentAnElement || (!isOffsetParentAnElement && !isFixed)) {
            if (getNodeName(offsetParent) !== 'body' || // https://github.com/popperjs/popper-core/issues/1078
                isScrollParent(documentElement)) {
                scroll = getNodeScroll(offsetParent);
            }
            if (isHTMLElement(offsetParent)) {
                offsets = getBoundingClientRect(offsetParent);
                offsets.x += offsetParent.clientLeft;
                offsets.y += offsetParent.clientTop;
            }
            else if (documentElement) {
                offsets.x = getWindowScrollBarX(documentElement);
            }
        }
        return {
            x: rect.left + scroll.scrollLeft - offsets.x,
            y: rect.top + scroll.scrollTop - offsets.y,
            width: rect.width,
            height: rect.height
        };
    }

    // Returns the layout rect of an element relative to its offsetParent. Layout
    // means it doesn't take into account transforms.
    /**
     * @param element
     */
    function getLayoutRect(element) {
        return {
            x: element.offsetLeft,
            y: element.offsetTop,
            width: element.offsetWidth,
            height: element.offsetHeight
        };
    }

    // @ts-nocheck
    /**
     * @param element
     */
    function getParentNode(element) {
        if (getNodeName(element) === 'html') {
            return element;
        }
        return (
        // $FlowFixMe: this is a quicker (but less type safe) way to save quite some bytes from the bundle
        element.assignedSlot || // step into the shadow DOM of the parent of a slotted node
            element.parentNode || // DOM Element detected
            // $FlowFixMe: need a better way to handle this...
            element.host || // ShadowRoot detected
            // $FlowFixMe: HTMLElement is a Node
            getDocumentElement(element) // fallback
        );
    }

    // @ts-nocheck
    /**
     * @param node
     */
    function getScrollParent(node) {
        if (['html', 'body', '#document'].indexOf(getNodeName(node)) >= 0) {
            // $FlowFixMe: assume body is always available
            return node.ownerDocument.body;
        }
        if (isHTMLElement(node) && isScrollParent(node)) {
            return node;
        }
        return getScrollParent(getParentNode(node));
    }

    // @ts-nocheck
    /*
    given a DOM element, return the list of all scroll parents, up the list of ancesors
    until we get to the top window object. This list is what we attach scroll listeners
    to, because if any of these parent elements scroll, we'll need to re-calculate the
    reference element's position.
    */
    /**
     * @param element
     * @param list
     */
    function listScrollParents(element, list = []) {
        const scrollParent = getScrollParent(element);
        const isBody = getNodeName(scrollParent) === 'body';
        const win = getWindow(scrollParent);
        const target = isBody
            ? [win].concat(win.visualViewport || [], isScrollParent(scrollParent) ? scrollParent : [])
            : scrollParent;
        const updatedList = list.concat(target);
        return isBody
            ? updatedList // $FlowFixMe: isBody tells us target will be an HTMLElement here
            : updatedList.concat(listScrollParents(getParentNode(target)));
    }

    // @ts-nocheck
    /**
     * @param element
     */
    function isTableElement(element) {
        return ['table', 'td', 'th'].indexOf(getNodeName(element)) >= 0;
    }

    // @ts-nocheck
    /**
     * @param element
     */
    function getTrueOffsetParent(element) {
        if (!isHTMLElement(element) || // https://github.com/popperjs/popper-core/issues/837
            getComputedStyle(element).position === 'fixed') {
            return null;
        }
        const offsetParent = element.offsetParent;
        if (offsetParent) {
            const html = getDocumentElement(offsetParent);
            if (getNodeName(offsetParent) === 'body' &&
                getComputedStyle(offsetParent).position === 'static' &&
                getComputedStyle(html).position !== 'static') {
                return html;
            }
        }
        return offsetParent;
    }
    // `.offsetParent` reports `null` for fixed elements, while absolute elements
    // return the containing block
    /**
     * @param element
     */
    function getContainingBlock(element) {
        let currentNode = getParentNode(element);
        while (isHTMLElement(currentNode) && ['html', 'body'].indexOf(getNodeName(currentNode)) < 0) {
            const css = getComputedStyle(currentNode);
            // This is non-exhaustive but covers the most common CSS properties that
            // create a containing block.
            if (css.transform !== 'none' || css.perspective !== 'none' || (css.willChange && css.willChange !== 'auto')) {
                return currentNode;
            }
            else {
                currentNode = currentNode.parentNode;
            }
        }
        return null;
    }
    // Gets the closest ancestor positioned element. Handles some edge cases,
    // such as table ancestors and cross browser bugs.
    /**
     * @param element
     */
    function getOffsetParent(element) {
        const window = getWindow(element);
        let offsetParent = getTrueOffsetParent(element);
        while (offsetParent && isTableElement(offsetParent) && getComputedStyle(offsetParent).position === 'static') {
            offsetParent = getTrueOffsetParent(offsetParent);
        }
        if (offsetParent && getNodeName(offsetParent) === 'body' && getComputedStyle(offsetParent).position === 'static') {
            return window;
        }
        return offsetParent || getContainingBlock(element) || window;
    }

    // @ts-nocheck
    const top = 'top';
    const bottom = 'bottom';
    const right = 'right';
    const left = 'left';
    const auto = 'auto';
    const basePlacements = [top, bottom, right, left];
    const start = 'start';
    const end = 'end';
    const clippingParents = 'clippingParents';
    const viewport = 'viewport';
    const popper = 'popper';
    const reference = 'reference';
    const variationPlacements = basePlacements.reduce((acc, placement) => acc.concat([`${placement}-${start}`, `${placement}-${end}`]), []);
    const placements = [...basePlacements, auto].reduce((acc, placement) => acc.concat([placement, `${placement}-${start}`, `${placement}-${end}`]), []);
    // modifiers that need to read the DOM
    const beforeRead = 'beforeRead';
    const read = 'read';
    const afterRead = 'afterRead';
    // pure-logic modifiers
    const beforeMain = 'beforeMain';
    const main = 'main';
    const afterMain = 'afterMain';
    // modifier with the purpose to write to the DOM (or write into a framework state)
    const beforeWrite = 'beforeWrite';
    const write = 'write';
    const afterWrite = 'afterWrite';
    const modifierPhases = [
        beforeRead,
        read,
        afterRead,
        beforeMain,
        main,
        afterMain,
        beforeWrite,
        write,
        afterWrite
    ];

    // source: https://stackoverflow.com/questions/49875255
    /**
     * @param modifiers
     */
    function order(modifiers) {
        const map = new Map();
        const visited = new Set();
        const result = [];
        modifiers.forEach(modifier => {
            map.set(modifier.name, modifier);
        });
        // On visiting object, check for its dependencies and visit them recursively
        /**
         * @param modifier
         */
        function sort(modifier) {
            visited.add(modifier.name);
            const requires = [...(modifier.requires || []), ...(modifier.requiresIfExists || [])];
            requires.forEach(dep => {
                if (!visited.has(dep)) {
                    const depModifier = map.get(dep);
                    if (depModifier) {
                        sort(depModifier);
                    }
                }
            });
            result.push(modifier);
        }
        modifiers.forEach(modifier => {
            if (!visited.has(modifier.name)) {
                // check for visited object
                sort(modifier);
            }
        });
        return result;
    }
    /**
     * @param modifiers
     */
    function orderModifiers(modifiers) {
        // order based on dependencies
        const orderedModifiers = order(modifiers);
        // order based on phase
        return modifierPhases.reduce((acc, phase) => acc.concat(orderedModifiers.filter(modifier => modifier.phase === phase)), []);
    }

    // @ts-nocheck
    /**
     * @param fn
     */
    function debounce$1(fn) {
        let pending;
        return () => {
            if (!pending) {
                pending = new Promise(resolve => {
                    Promise.resolve().then(() => {
                        pending = undefined;
                        resolve(fn());
                    });
                });
            }
            return pending;
        };
    }

    /**
     * @param placement
     */
    function getBasePlacement(placement) {
        return placement.split('-')[0];
    }

    /**
     * @param modifiers
     */
    function mergeByName(modifiers) {
        const merged = modifiers.reduce((merged, current) => {
            const existing = merged[current.name];
            merged[current.name] = existing
                ? Object.assign(Object.assign(Object.assign({}, existing), current), { options: Object.assign(Object.assign({}, existing.options), current.options), data: Object.assign(Object.assign({}, existing.data), current.data) }) : current;
            return merged;
        }, {});
        // IE11 does not support Object.values
        return Object.keys(merged).map(key => merged[key]);
    }

    // @ts-nocheck
    /**
     * @param element
     */
    function getViewportRect(element) {
        const win = getWindow(element);
        const html = getDocumentElement(element);
        const visualViewport = win.visualViewport;
        let width = html.clientWidth;
        let height = html.clientHeight;
        let x = 0;
        let y = 0;
        // NB: This isn't supported on iOS <= 12. If the keyboard is open, the popper
        // can be obscured underneath it.
        // Also, `html.clientHeight` adds the bottom bar height in Safari iOS, even
        // if it isn't open, so if this isn't available, the popper will be detected
        // to overflow the bottom of the screen too early.
        if (visualViewport) {
            width = visualViewport.width;
            height = visualViewport.height;
            // Uses Layout Viewport (like Chrome; Safari does not currently)
            // In Chrome, it returns a value very close to 0 (+/-) but contains rounding
            // errors due to floating point numbers, so we need to check precision.
            // Safari returns a number <= 0, usually < -1 when pinch-zoomed
            // Feature detection fails in mobile emulation mode in Chrome.
            // Math.abs(win.innerWidth / visualViewport.scale - visualViewport.width) <
            // 0.001
            // Fallback here: "Not Safari" userAgent
            if (!/^((?!chrome|android).)*safari/i.test(navigator.userAgent)) {
                x = visualViewport.offsetLeft;
                y = visualViewport.offsetTop;
            }
        }
        return {
            width,
            height,
            x: x + getWindowScrollBarX(element),
            y
        };
    }

    // Gets the entire size of the scrollable document area, even extending outside
    // of the `<html>` and `<body>` rect bounds if horizontally scrollable
    /**
     * @param element
     */
    function getDocumentRect(element) {
        const html = getDocumentElement(element);
        const winScroll = getWindowScroll(element);
        const body = element.ownerDocument.body;
        const width = Math.max(html.scrollWidth, html.clientWidth, body ? body.scrollWidth : 0, body ? body.clientWidth : 0);
        const height = Math.max(html.scrollHeight, html.clientHeight, body ? body.scrollHeight : 0, body ? body.clientHeight : 0);
        let x = -winScroll.scrollLeft + getWindowScrollBarX(element);
        const y = -winScroll.scrollTop;
        if (getComputedStyle(body || html).direction === 'rtl') {
            x += Math.max(html.clientWidth, body ? body.clientWidth : 0) - width;
        }
        return { width, height, x, y };
    }

    // @ts-nocheck
    /**
     * @param parent
     * @param child
     */
    function contains(parent, child) {
        // $FlowFixMe: hasOwnProperty doesn't seem to work in tests
        const isShadow = Boolean(child.getRootNode && child.getRootNode().host);
        // First, attempt with faster native method
        if (parent.contains(child)) {
            return true;
        } // then fallback to custom implementation with Shadow DOM support
        else if (isShadow) {
            let next = child;
            do {
                if (next && parent.isSameNode(next)) {
                    return true;
                }
                // $FlowFixMe: need a better way to handle this...
                next = next.parentNode || next.host;
            } while (next);
        }
        // Give up, the result is false
        return false;
    }

    /**
     * @param rect
     */
    function rectToClientRect(rect) {
        return Object.assign(Object.assign({}, rect), { left: rect.x, top: rect.y, right: rect.x + rect.width, bottom: rect.y + rect.height });
    }

    /**
     * @param element
     */
    function getInnerBoundingClientRect(element) {
        const rect = getBoundingClientRect(element);
        rect.top = rect.top + element.clientTop;
        rect.left = rect.left + element.clientLeft;
        rect.bottom = rect.top + element.clientHeight;
        rect.right = rect.left + element.clientWidth;
        rect.width = element.clientWidth;
        rect.height = element.clientHeight;
        rect.x = rect.left;
        rect.y = rect.top;
        return rect;
    }
    /**
     * @param element
     * @param clippingParent
     */
    function getClientRectFromMixedType(element, clippingParent) {
        return clippingParent === viewport
            ? rectToClientRect(getViewportRect(element))
            : isHTMLElement(clippingParent)
                ? getInnerBoundingClientRect(clippingParent)
                : rectToClientRect(getDocumentRect(getDocumentElement(element)));
    }
    // A "clipping parent" is an overflowable container with the characteristic of
    // clipping (or hiding) overflowing elements with a position different from
    // `initial`
    /**
     * @param element
     */
    function getClippingParents(element) {
        const clippingParents = listScrollParents(getParentNode(element));
        const canEscapeClipping = ['absolute', 'fixed'].indexOf(getComputedStyle(element).position) >= 0;
        const clipperElement = canEscapeClipping && isHTMLElement(element) ? getOffsetParent(element) : element;
        if (!isElement(clipperElement)) {
            return [];
        }
        // $FlowFixMe: https://github.com/facebook/flow/issues/1414
        return clippingParents.filter(clippingParent => isElement(clippingParent) && contains(clippingParent, clipperElement) && getNodeName(clippingParent) !== 'body');
    }
    // Gets the maximum area that the element is visible in due to any number of
    // clipping parents
    /**
     * @param element
     * @param boundary
     * @param rootBoundary
     */
    function getClippingRect(element, boundary, rootBoundary) {
        const mainClippingParents = boundary === 'clippingParents' ? getClippingParents(element) : [].concat(boundary);
        const clippingParents = [...mainClippingParents, rootBoundary];
        const firstClippingParent = clippingParents[0];
        const clippingRect = clippingParents.reduce((accRect, clippingParent) => {
            const rect = getClientRectFromMixedType(element, clippingParent);
            accRect.top = Math.max(rect.top, accRect.top);
            accRect.right = Math.min(rect.right, accRect.right);
            accRect.bottom = Math.min(rect.bottom, accRect.bottom);
            accRect.left = Math.max(rect.left, accRect.left);
            return accRect;
        }, getClientRectFromMixedType(element, firstClippingParent));
        clippingRect.width = clippingRect.right - clippingRect.left;
        clippingRect.height = clippingRect.bottom - clippingRect.top;
        clippingRect.x = clippingRect.left;
        clippingRect.y = clippingRect.top;
        return clippingRect;
    }

    /**
     * @param placement
     */
    function getVariation(placement) {
        return placement.split('-')[1];
    }

    /**
     * @param placement
     */
    function getMainAxisFromPlacement(placement) {
        return ['top', 'bottom'].indexOf(placement) >= 0 ? 'x' : 'y';
    }

    // @ts-nocheck
    /**
     *
     */
    function computeOffsets({ reference, element, placement }) {
        const basePlacement = placement ? getBasePlacement(placement) : null;
        const variation = placement ? getVariation(placement) : null;
        const commonX = reference.x + reference.width / 2 - element.width / 2;
        const commonY = reference.y + reference.height / 2 - element.height / 2;
        let offsets;
        switch (basePlacement) {
            case top:
                offsets = {
                    x: commonX,
                    y: reference.y - element.height
                };
                break;
            case bottom:
                offsets = {
                    x: commonX,
                    y: reference.y + reference.height
                };
                break;
            case right:
                offsets = {
                    x: reference.x + reference.width,
                    y: commonY
                };
                break;
            case left:
                offsets = {
                    x: reference.x - element.width,
                    y: commonY
                };
                break;
            default:
                offsets = {
                    x: reference.x,
                    y: reference.y
                };
        }
        const mainAxis = basePlacement ? getMainAxisFromPlacement(basePlacement) : null;
        if (mainAxis != null) {
            const len = mainAxis === 'y' ? 'height' : 'width';
            switch (variation) {
                case start:
                    offsets[mainAxis] = Math.floor(offsets[mainAxis]) - Math.floor(reference[len] / 2 - element[len] / 2);
                    break;
                case end:
                    offsets[mainAxis] = Math.floor(offsets[mainAxis]) + Math.ceil(reference[len] / 2 - element[len] / 2);
                    break;
            }
        }
        return offsets;
    }

    /**
     *
     */
    function getFreshSideObject() {
        return {
            top: 0,
            right: 0,
            bottom: 0,
            left: 0
        };
    }

    /**
     * @param paddingObject
     */
    function mergePaddingObject(paddingObject) {
        return Object.assign(Object.assign({}, getFreshSideObject()), paddingObject);
    }

    // @ts-nocheck
    /**
     * @param value
     * @param keys
     */
    function expandToHashMap(value, keys) {
        return keys.reduce((hashMap, key) => {
            hashMap[key] = value;
            return hashMap;
        }, {});
    }

    /**
     * @param state
     * @param options
     */
    function detectOverflow(state, options = {}) {
        const { placement = state.placement, boundary = clippingParents, rootBoundary = viewport, elementContext = popper, altBoundary = false, padding = 0 } = options;
        const paddingObject = mergePaddingObject(typeof padding !== 'number' ? padding : expandToHashMap(padding, basePlacements));
        const altContext = elementContext === popper ? reference : popper;
        const referenceElement = state.elements.reference;
        const popperRect = state.rects.popper;
        const element = state.elements[altBoundary ? altContext : elementContext];
        const clippingClientRect = getClippingRect(isElement(element) ? element : element.contextElement || getDocumentElement(state.elements.popper), boundary, rootBoundary);
        const referenceClientRect = getBoundingClientRect(referenceElement);
        const popperOffsets = computeOffsets({
            reference: referenceClientRect,
            element: popperRect,
            strategy: 'absolute',
            placement
        });
        const popperClientRect = rectToClientRect(Object.assign(Object.assign({}, popperRect), popperOffsets));
        const elementClientRect = elementContext === popper ? popperClientRect : referenceClientRect;
        // positive = overflowing the clipping rect
        // 0 or negative = within the clipping rect
        const overflowOffsets = {
            top: clippingClientRect.top - elementClientRect.top + paddingObject.top,
            bottom: elementClientRect.bottom - clippingClientRect.bottom + paddingObject.bottom,
            left: clippingClientRect.left - elementClientRect.left + paddingObject.left,
            right: elementClientRect.right - clippingClientRect.right + paddingObject.right
        };
        const offsetData = state.modifiersData.offset;
        // Offsets can be applied only to the popper element
        if (elementContext === popper && offsetData) {
            const offset = offsetData[placement];
            Object.keys(overflowOffsets).forEach(key => {
                const multiply = [right, bottom].indexOf(key) >= 0 ? 1 : -1;
                const axis = [top, bottom].indexOf(key) >= 0 ? 'y' : 'x';
                overflowOffsets[key] += offset[axis] * multiply;
            });
        }
        return overflowOffsets;
    }

    const DEFAULT_OPTIONS = {
        placement: 'bottom',
        modifiers: [],
        strategy: 'absolute'
    };
    /**
     * @param args
     */
    function areValidElements(...args) {
        return !args.some(element => !(element && typeof element.getBoundingClientRect === 'function'));
    }
    /**
     * @param generatorOptions
     */
    function popperGenerator(generatorOptions = {}) {
        const { defaultModifiers = [], defaultOptions = DEFAULT_OPTIONS } = generatorOptions;
        return function createPopper(reference, popper, options = defaultOptions) {
            let state = {
                placement: 'bottom',
                orderedModifiers: [],
                options: Object.assign(Object.assign({}, DEFAULT_OPTIONS), defaultOptions),
                modifiersData: {},
                elements: {
                    reference,
                    popper
                },
                attributes: {},
                styles: {}
            };
            let effectCleanupFns = [];
            let isDestroyed = false;
            const instance = {
                state,
                setOptions(options) {
                    cleanupModifierEffects();
                    state.options = Object.assign(Object.assign(Object.assign({}, defaultOptions), state.options), options);
                    state.scrollParents = {
                        reference: isElement(reference)
                            ? listScrollParents(reference)
                            : reference.contextElement
                                ? listScrollParents(reference.contextElement)
                                : [],
                        popper: listScrollParents(popper)
                    };
                    // Orders the modifiers based on their dependencies and `phase`
                    // properties
                    const orderedModifiers = orderModifiers(mergeByName([...defaultModifiers, ...state.options.modifiers]));
                    // Strip out disabled modifiers
                    state.orderedModifiers = orderedModifiers.filter(m => m.enabled);
                    runModifierEffects();
                    return instance.update();
                },
                // Sync update – it will always be executed, even if not necessary. This
                // is useful for low frequency updates where sync behavior simplifies the
                // logic.
                // For high frequency updates (e.g. `resize` and `scroll` events), always
                // prefer the async Popper#update method
                forceUpdate() {
                    if (isDestroyed) {
                        return;
                    }
                    const { reference, popper } = state.elements;
                    // Don't proceed if `reference` or `popper` are not valid elements
                    // anymore
                    if (!areValidElements(reference, popper)) {
                        return;
                    }
                    // Store the reference and popper rects to be read by modifiers
                    state.rects = {
                        reference: getCompositeRect(reference, getOffsetParent(popper), state.options.strategy === 'fixed'),
                        popper: getLayoutRect(popper)
                    };
                    // Modifiers have the ability to reset the current update cycle. The
                    // most common use case for this is the `flip` modifier changing the
                    // placement, which then needs to re-run all the modifiers, because the
                    // logic was previously ran for the previous placement and is therefore
                    // stale/incorrect
                    state.reset = false;
                    state.placement = state.options.placement;
                    // On each update cycle, the `modifiersData` property for each modifier
                    // is filled with the initial data specified by the modifier. This means
                    // it doesn't persist and is fresh on each update.
                    // To ensure persistent data, use `${name}#persistent`
                    state.orderedModifiers.forEach(modifier => (state.modifiersData[modifier.name] = Object.assign({}, modifier.data)));
                    for (let index = 0; index < state.orderedModifiers.length; index++) {
                        if (state.reset === true) {
                            state.reset = false;
                            index = -1;
                            continue;
                        }
                        const { fn, options = {}, name } = state.orderedModifiers[index];
                        if (typeof fn === 'function') {
                            state = fn({ state, options, name, instance }) || state;
                        }
                    }
                },
                // Async and optimistically optimized update – it will not be executed if
                // not necessary (debounced to run at most once-per-tick)
                update: debounce$1(() => new Promise(resolve => {
                    instance.forceUpdate();
                    resolve(state);
                })),
                destroy() {
                    cleanupModifierEffects();
                    isDestroyed = true;
                }
            };
            if (!areValidElements(reference, popper)) {
                return instance;
            }
            instance.setOptions(options).then(state => {
                if (!isDestroyed && options.onFirstUpdate) {
                    options.onFirstUpdate(state);
                }
            });
            // Modifiers have the ability to execute arbitrary code before the first
            // update cycle runs. They will be executed in the same order as the update
            // cycle. This is useful when a modifier adds some persistent data that
            // other modifiers need to use, but the modifier is run after the dependent
            // one.
            /**
             *
             */
            function runModifierEffects() {
                state.orderedModifiers.forEach(({ name, options = {}, effect }) => {
                    if (typeof effect === 'function') {
                        const cleanupFn = effect({ state, name, instance, options });
                        const noopFn = () => { };
                        effectCleanupFns.push(cleanupFn || noopFn);
                    }
                });
            }
            /**
             *
             */
            function cleanupModifierEffects() {
                effectCleanupFns.forEach(fn => fn());
                effectCleanupFns = [];
            }
            return instance;
        };
    }

    const passive = { passive: true };
    /**
     *
     */
    function effect({ state, instance, options }) {
        const { scroll = true, resize = true } = options;
        const window = getWindow(state.elements.popper);
        const scrollParents = [...state.scrollParents.reference, ...state.scrollParents.popper];
        if (scroll) {
            scrollParents.forEach(scrollParent => {
                scrollParent.addEventListener('scroll', instance.update, passive);
            });
        }
        if (resize) {
            window.addEventListener('resize', instance.update, passive);
        }
        return () => {
            if (scroll) {
                scrollParents.forEach(scrollParent => {
                    scrollParent.removeEventListener('scroll', instance.update, passive);
                });
            }
            if (resize) {
                window.removeEventListener('resize', instance.update, passive);
            }
        };
    }
    var eventListeners = {
        name: 'eventListeners',
        enabled: true,
        phase: 'write',
        fn: () => { },
        effect,
        data: {}
    };

    /**
     *
     */
    function popperOffsets({ state, name }) {
        // Offsets are the actual position the popper needs to have to be
        // properly positioned near its reference element
        // This is the most basic placement, and will be adjusted by
        // the modifiers in the next step
        state.modifiersData[name] = computeOffsets({
            reference: state.rects.reference,
            element: state.rects.popper,
            strategy: 'absolute',
            placement: state.placement
        });
    }
    var popperOffsets$1 = {
        name: 'popperOffsets',
        enabled: true,
        phase: 'read',
        fn: popperOffsets,
        data: {}
    };

    const unsetSides = {
        top: 'auto',
        right: 'auto',
        bottom: 'auto',
        left: 'auto'
    };
    // Round the offsets to the nearest suitable subpixel based on the DPR.
    // Zooming can change the DPR, but it seems to report a value that will
    // cleanly divide the values into the appropriate subpixels.
    /**
     *
     */
    function roundOffsets({ x, y }) {
        const win = window;
        const dpr = win.devicePixelRatio || 1;
        return {
            x: Math.round(x * dpr) / dpr || 0,
            y: Math.round(y * dpr) / dpr || 0
        };
    }
    /**
     *
     */
    function mapToStyles({ popper, popperRect, placement, offsets, position, gpuAcceleration, adaptive }) {
        let { x, y } = roundOffsets(offsets);
        const hasX = offsets.hasOwnProperty('x');
        const hasY = offsets.hasOwnProperty('y');
        let sideX = left;
        let sideY = top;
        const win = window;
        if (adaptive) {
            let offsetParent = getOffsetParent(popper);
            if (offsetParent === getWindow(popper)) {
                offsetParent = getDocumentElement(popper);
            }
            // $FlowFixMe: force type refinement, we compare offsetParent with window above, but Flow doesn't detect it
            /* :: offsetParent = (offsetParent: Element); */
            if (placement === top) {
                sideY = bottom;
                y -= offsetParent.clientHeight - popperRect.height;
                y *= gpuAcceleration ? 1 : -1;
            }
            if (placement === left) {
                sideX = right;
                x -= offsetParent.clientWidth - popperRect.width;
                x *= gpuAcceleration ? 1 : -1;
            }
        }
        const commonStyles = Object.assign({ position }, (adaptive && unsetSides));
        if (gpuAcceleration) {
            return Object.assign(Object.assign({}, commonStyles), { [sideY]: hasY ? '0' : '', [sideX]: hasX ? '0' : '', 
                // Layer acceleration can disable subpixel rendering which causes slightly
                // blurry text on low PPI displays, so we want to use 2D transforms
                // instead
                transform: (win.devicePixelRatio || 1) < 2 ? `translate(${x}px, ${y}px)` : `translate3d(${x}px, ${y}px, 0)` });
        }
        return Object.assign(Object.assign({}, commonStyles), { [sideY]: hasY ? `${y}px` : '', [sideX]: hasX ? `${x}px` : '', transform: '' });
    }
    /**
     *
     */
    function computeStyles({ state, options }) {
        const { gpuAcceleration = true, adaptive = true } = options;
        const commonStyles = {
            placement: getBasePlacement(state.placement),
            popper: state.elements.popper,
            popperRect: state.rects.popper,
            gpuAcceleration
        };
        if (state.modifiersData.popperOffsets != null) {
            state.styles.popper = Object.assign(Object.assign({}, state.styles.popper), mapToStyles(Object.assign(Object.assign({}, commonStyles), { offsets: state.modifiersData.popperOffsets, position: state.options.strategy, adaptive })));
        }
        if (state.modifiersData.arrow != null) {
            state.styles.arrow = Object.assign(Object.assign({}, state.styles.arrow), mapToStyles(Object.assign(Object.assign({}, commonStyles), { offsets: state.modifiersData.arrow, position: 'absolute', adaptive: false })));
        }
        state.attributes.popper = Object.assign(Object.assign({}, state.attributes.popper), { 'data-popper-placement': state.placement });
    }
    var computeStyles$1 = {
        name: 'computeStyles',
        enabled: true,
        phase: 'beforeWrite',
        fn: computeStyles,
        data: {}
    };

    // This modifier takes the styles prepared by the `computeStyles` modifier
    // and applies them to the HTMLElements such as popper and arrow
    /**
     *
     */
    function applyStyles({ state }) {
        Object.keys(state.elements).forEach(name => {
            const style = state.styles[name] || {};
            const attributes = state.attributes[name] || {};
            const element = state.elements[name];
            // arrow is optional + virtual elements
            if (!isHTMLElement(element) || !getNodeName(element)) {
                return;
            }
            // Flow doesn't support to extend this property, but it's the most
            // effective way to apply styles to an HTMLElement
            // $FlowFixMe
            Object.assign(element.style, style);
            Object.keys(attributes).forEach(name => {
                const value = attributes[name];
                if (value === false) {
                    element.removeAttribute(name);
                }
                else {
                    element.setAttribute(name, value === true ? '' : value);
                }
            });
        });
    }
    /**
     *
     */
    function effect$1({ state }) {
        const initialStyles = {
            popper: {
                position: state.options.strategy,
                left: '0',
                top: '0',
                margin: '0'
            },
            arrow: {
                position: 'absolute'
            },
            reference: {}
        };
        Object.assign(state.elements.popper.style, initialStyles.popper);
        if (state.elements.arrow) {
            Object.assign(state.elements.arrow.style, initialStyles.arrow);
        }
        return () => {
            Object.keys(state.elements).forEach(name => {
                const element = state.elements[name];
                const attributes = state.attributes[name] || {};
                const styleProperties = Object.keys(state.styles.hasOwnProperty(name) ? state.styles[name] : initialStyles[name]);
                // Set all values to an empty string to unset them
                const style = styleProperties.reduce((style, property) => {
                    style[property] = '';
                    return style;
                }, {});
                // arrow is optional + virtual elements
                if (!isHTMLElement(element) || !getNodeName(element)) {
                    return;
                }
                // Flow doesn't support to extend this property, but it's the most
                // effective way to apply styles to an HTMLElement
                // $FlowFixMe
                Object.assign(element.style, style);
                Object.keys(attributes).forEach(attribute => {
                    element.removeAttribute(attribute);
                });
            });
        };
    }
    var applyStyles$1 = {
        name: 'applyStyles',
        enabled: true,
        phase: 'write',
        fn: applyStyles,
        effect: effect$1,
        requires: ['computeStyles']
    };

    /**
     * @param placement
     * @param rects
     * @param offset
     */
    function distanceAndSkiddingToXY(placement, rects, offset) {
        const basePlacement = getBasePlacement(placement);
        const invertDistance = [left, top].indexOf(basePlacement) >= 0 ? -1 : 1;
        let [skidding, distance] = typeof offset === 'function'
            ? offset(Object.assign(Object.assign({}, rects), { placement }))
            : offset;
        skidding = skidding || 0;
        distance = (distance || 0) * invertDistance;
        return [left, right].indexOf(basePlacement) >= 0 ? { x: distance, y: skidding } : { x: skidding, y: distance };
    }
    /**
     *
     */
    function offset({ state, options, name }) {
        const { offset = [0, 0] } = options;
        const data = placements.reduce((acc, placement) => {
            acc[placement] = distanceAndSkiddingToXY(placement, state.rects, offset);
            return acc;
        }, {});
        const { x, y } = data[state.placement];
        if (state.modifiersData.popperOffsets != null) {
            state.modifiersData.popperOffsets.x += x;
            state.modifiersData.popperOffsets.y += y;
        }
        state.modifiersData[name] = data;
    }
    var offset$1 = {
        name: 'offset',
        enabled: true,
        phase: 'main',
        requires: ['popperOffsets'],
        fn: offset
    };

    const hash = { left: 'right', right: 'left', bottom: 'top', top: 'bottom' };
    /**
     * @param placement
     */
    function getOppositePlacement(placement) {
        return placement.replace(/left|right|bottom|top/g, matched => hash[matched]);
    }

    const hash$1 = { start: 'end', end: 'start' };
    /**
     * @param placement
     */
    function getOppositeVariationPlacement(placement) {
        return placement.replace(/start|end/g, matched => hash$1[matched]);
    }

    /* :: type OverflowsMap = { [ComputedPlacement]: number }; */
    /* ;; type OverflowsMap = { [key in ComputedPlacement]: number }; */
    /**
     * @param state
     * @param options
     */
    function computeAutoPlacement(state, options = {}) {
        const { placement, boundary, rootBoundary, padding, flipVariations, allowedAutoPlacements = placements } = options;
        const variation = getVariation(placement);
        const placements$1 = variation
            ? flipVariations
                ? variationPlacements
                : variationPlacements.filter(placement => getVariation(placement) === variation)
            : basePlacements;
        // $FlowFixMe
        let allowedPlacements = placements$1.filter(placement => allowedAutoPlacements.indexOf(placement) >= 0);
        if (allowedPlacements.length === 0) {
            allowedPlacements = placements$1;
        }
        // $FlowFixMe: Flow seems to have problems with two array unions...
        const overflows = allowedPlacements.reduce((acc, placement) => {
            acc[placement] = detectOverflow(state, {
                placement,
                boundary,
                rootBoundary,
                padding
            })[getBasePlacement(placement)];
            return acc;
        }, {});
        return Object.keys(overflows).sort((a, b) => overflows[a] - overflows[b]);
    }

    /**
     * @param placement
     */
    function getExpandedFallbackPlacements(placement) {
        if (getBasePlacement(placement) === auto) {
            return [];
        }
        const oppositePlacement = getOppositePlacement(placement);
        return [
            getOppositeVariationPlacement(placement),
            oppositePlacement,
            getOppositeVariationPlacement(oppositePlacement)
        ];
    }
    /**
     *
     */
    function flip({ state, options, name }) {
        if (state.modifiersData[name]._skip) {
            return;
        }
        const { mainAxis: checkMainAxis = true, altAxis: checkAltAxis = true, fallbackPlacements: specifiedFallbackPlacements, padding, boundary, rootBoundary, altBoundary, flipVariations = true, allowedAutoPlacements } = options;
        const preferredPlacement = state.options.placement;
        const basePlacement = getBasePlacement(preferredPlacement);
        const isBasePlacement = basePlacement === preferredPlacement;
        const fallbackPlacements = specifiedFallbackPlacements ||
            (isBasePlacement || !flipVariations
                ? [getOppositePlacement(preferredPlacement)]
                : getExpandedFallbackPlacements(preferredPlacement));
        const placements = [preferredPlacement, ...fallbackPlacements].reduce((acc, placement) => acc.concat(getBasePlacement(placement) === auto
            ? computeAutoPlacement(state, {
                placement,
                boundary,
                rootBoundary,
                padding,
                flipVariations,
                allowedAutoPlacements
            })
            : placement), []);
        const referenceRect = state.rects.reference;
        const popperRect = state.rects.popper;
        const checksMap = new Map();
        let makeFallbackChecks = true;
        let firstFittingPlacement = placements[0];
        for (let i = 0; i < placements.length; i++) {
            const placement = placements[i];
            const basePlacement = getBasePlacement(placement);
            const isStartVariation = getVariation(placement) === start;
            const isVertical = [top, bottom].indexOf(basePlacement) >= 0;
            const len = isVertical ? 'width' : 'height';
            const overflow = detectOverflow(state, {
                placement,
                boundary,
                rootBoundary,
                altBoundary,
                padding
            });
            let mainVariationSide = isVertical ? (isStartVariation ? right : left) : isStartVariation ? bottom : top;
            if (referenceRect[len] > popperRect[len]) {
                mainVariationSide = getOppositePlacement(mainVariationSide);
            }
            const altVariationSide = getOppositePlacement(mainVariationSide);
            const checks = [];
            if (checkMainAxis) {
                checks.push(overflow[basePlacement] <= 0);
            }
            if (checkAltAxis) {
                checks.push(overflow[mainVariationSide] <= 0, overflow[altVariationSide] <= 0);
            }
            if (checks.every(check => check)) {
                firstFittingPlacement = placement;
                makeFallbackChecks = false;
                break;
            }
            checksMap.set(placement, checks);
        }
        if (makeFallbackChecks) {
            // `2` may be desired in some cases – research later
            const numberOfChecks = flipVariations ? 3 : 1;
            for (let i = numberOfChecks; i > 0; i--) {
                const fittingPlacement = placements.find(placement => {
                    const checks = checksMap.get(placement);
                    if (checks) {
                        return checks.slice(0, i).every(check => check);
                    }
                });
                if (fittingPlacement) {
                    firstFittingPlacement = fittingPlacement;
                    break;
                }
            }
        }
        if (state.placement !== firstFittingPlacement) {
            state.modifiersData[name]._skip = true;
            state.placement = firstFittingPlacement;
            state.reset = true;
        }
    }
    var flip$1 = {
        name: 'flip',
        enabled: true,
        phase: 'main',
        fn: flip,
        requiresIfExists: ['offset'],
        data: { _skip: false }
    };

    // @ts-nocheck
    /**
     * @param axis
     */
    function getAltAxis(axis) {
        return axis === 'x' ? 'y' : 'x';
    }

    // @ts-nocheck
    /**
     * @param min
     * @param value
     * @param max
     */
    function within(min, value, max) {
        return Math.max(min, Math.min(value, max));
    }

    // @ts-nocheck
    /**
     *
     */
    function preventOverflow({ state, options, name }) {
        const { mainAxis: checkMainAxis = true, altAxis: checkAltAxis = false, boundary, rootBoundary, altBoundary, padding, tether = true, tetherOffset = 0 } = options;
        const overflow = detectOverflow(state, {
            boundary,
            rootBoundary,
            padding,
            altBoundary
        });
        const basePlacement = getBasePlacement(state.placement);
        const variation = getVariation(state.placement);
        const isBasePlacement = !variation;
        const mainAxis = getMainAxisFromPlacement(basePlacement);
        const altAxis = getAltAxis(mainAxis);
        const popperOffsets = state.modifiersData.popperOffsets;
        const referenceRect = state.rects.reference;
        const popperRect = state.rects.popper;
        const tetherOffsetValue = typeof tetherOffset === 'function'
            ? tetherOffset(Object.assign(Object.assign({}, state.rects), { placement: state.placement }))
            : tetherOffset;
        const data = { x: 0, y: 0 };
        if (!popperOffsets) {
            return;
        }
        if (checkMainAxis) {
            const mainSide = mainAxis === 'y' ? top : left;
            const altSide = mainAxis === 'y' ? bottom : right;
            const len = mainAxis === 'y' ? 'height' : 'width';
            const offset = popperOffsets[mainAxis];
            const min = popperOffsets[mainAxis] + overflow[mainSide];
            const max = popperOffsets[mainAxis] - overflow[altSide];
            const additive = tether ? -popperRect[len] / 2 : 0;
            const minLen = variation === start ? referenceRect[len] : popperRect[len];
            const maxLen = variation === start ? -popperRect[len] : -referenceRect[len];
            // We need to include the arrow in the calculation so the arrow doesn't go
            // outside the reference bounds
            const arrowElement = state.elements.arrow;
            const arrowRect = tether && arrowElement ? getLayoutRect(arrowElement) : { width: 0, height: 0 };
            const arrowPaddingObject = state.modifiersData['arrow#persistent']
                ? state.modifiersData['arrow#persistent'].padding
                : getFreshSideObject();
            const arrowPaddingMin = arrowPaddingObject[mainSide];
            const arrowPaddingMax = arrowPaddingObject[altSide];
            // If the reference length is smaller than the arrow length, we don't want
            // to include its full size in the calculation. If the reference is small
            // and near the edge of a boundary, the popper can overflow even if the
            // reference is not overflowing as well (e.g. virtual elements with no
            // width or height)
            const arrowLen = within(0, referenceRect[len], arrowRect[len]);
            const minOffset = isBasePlacement
                ? referenceRect[len] / 2 - additive - arrowLen - arrowPaddingMin - tetherOffsetValue
                : minLen - arrowLen - arrowPaddingMin - tetherOffsetValue;
            const maxOffset = isBasePlacement
                ? -referenceRect[len] / 2 + additive + arrowLen + arrowPaddingMax + tetherOffsetValue
                : maxLen + arrowLen + arrowPaddingMax + tetherOffsetValue;
            const arrowOffsetParent = state.elements.arrow && getOffsetParent(state.elements.arrow);
            const clientOffset = arrowOffsetParent
                ? mainAxis === 'y'
                    ? arrowOffsetParent.clientTop || 0
                    : arrowOffsetParent.clientLeft || 0
                : 0;
            const offsetModifierValue = state.modifiersData.offset ? state.modifiersData.offset[state.placement][mainAxis] : 0;
            const tetherMin = popperOffsets[mainAxis] + minOffset - offsetModifierValue - clientOffset;
            const tetherMax = popperOffsets[mainAxis] + maxOffset - offsetModifierValue;
            const preventedOffset = within(tether ? Math.min(min, tetherMin) : min, offset, tether ? Math.max(max, tetherMax) : max);
            popperOffsets[mainAxis] = preventedOffset;
            data[mainAxis] = preventedOffset - offset;
        }
        if (checkAltAxis) {
            const mainSide = mainAxis === 'x' ? top : left;
            const altSide = mainAxis === 'x' ? bottom : right;
            const offset = popperOffsets[altAxis];
            const min = offset + overflow[mainSide];
            const max = offset - overflow[altSide];
            const preventedOffset = within(min, offset, max);
            popperOffsets[altAxis] = preventedOffset;
            data[altAxis] = preventedOffset - offset;
        }
        state.modifiersData[name] = data;
    }
    var preventOverflow$1 = {
        name: 'preventOverflow',
        enabled: true,
        phase: 'main',
        fn: preventOverflow,
        requiresIfExists: ['offset']
    };

    /**
     *
     */
    function arrow({ state, name }) {
        const arrowElement = state.elements.arrow;
        const popperOffsets = state.modifiersData.popperOffsets;
        const basePlacement = getBasePlacement(state.placement);
        const axis = getMainAxisFromPlacement(basePlacement);
        const isVertical = [left, right].indexOf(basePlacement) >= 0;
        const len = isVertical ? 'height' : 'width';
        if (!arrowElement || !popperOffsets) {
            return;
        }
        const paddingObject = state.modifiersData[`${name}#persistent`].padding;
        const arrowRect = getLayoutRect(arrowElement);
        const minProp = axis === 'y' ? top : left;
        const maxProp = axis === 'y' ? bottom : right;
        const endDiff = state.rects.reference[len] + state.rects.reference[axis] - popperOffsets[axis] - state.rects.popper[len];
        const startDiff = popperOffsets[axis] - state.rects.reference[axis];
        const arrowOffsetParent = getOffsetParent(arrowElement);
        const clientSize = arrowOffsetParent
            ? axis === 'y'
                ? arrowOffsetParent.clientHeight || 0
                : arrowOffsetParent.clientWidth || 0
            : 0;
        const centerToReference = endDiff / 2 - startDiff / 2;
        // Make sure the arrow doesn't overflow the popper if the center point is
        // outside of the popper bounds
        const min = paddingObject[minProp];
        const max = clientSize - arrowRect[len] - paddingObject[maxProp];
        const center = clientSize / 2 - arrowRect[len] / 2 + centerToReference;
        const offset = within(min, center, max);
        // Prevents breaking syntax highlighting...
        const axisProp = axis;
        state.modifiersData[name] = {
            [axisProp]: offset,
            centerOffset: offset - center
        };
    }
    /**
     *
     */
    function effect$2({ state, options, name }) {
        let { element: arrowElement = '[data-popper-arrow]', padding = 0 } = options;
        if (arrowElement == null) {
            return;
        }
        // CSS selector
        if (typeof arrowElement === 'string') {
            arrowElement = state.elements.popper.querySelector(arrowElement);
            if (!arrowElement) {
                return;
            }
        }
        if (!contains(state.elements.popper, arrowElement)) {
            return;
        }
        state.elements.arrow = arrowElement;
        state.modifiersData[`${name}#persistent`] = {
            padding: mergePaddingObject(typeof padding !== 'number' ? padding : expandToHashMap(padding, basePlacements))
        };
    }
    var arrow$1 = {
        name: 'arrow',
        enabled: true,
        phase: 'main',
        fn: arrow,
        effect: effect$2,
        requires: ['popperOffsets'],
        requiresIfExists: ['preventOverflow']
    };

    /**
     * @param overflow
     * @param rect
     * @param preventedOffsets
     */
    function getSideOffsets(overflow, rect, preventedOffsets = { x: 0, y: 0 }) {
        return {
            top: overflow.top - rect.height - preventedOffsets.y,
            right: overflow.right - rect.width + preventedOffsets.x,
            bottom: overflow.bottom - rect.height + preventedOffsets.y,
            left: overflow.left - rect.width - preventedOffsets.x
        };
    }
    /**
     * @param overflow
     */
    function isAnySideFullyClipped(overflow) {
        return [top, right, bottom, left].some(side => overflow[side] >= 0);
    }
    /**
     *
     */
    function hide({ state, name }) {
        const referenceRect = state.rects.reference;
        const popperRect = state.rects.popper;
        const preventedOffsets = state.modifiersData.preventOverflow;
        const referenceOverflow = detectOverflow(state, {
            elementContext: 'reference'
        });
        const popperAltOverflow = detectOverflow(state, {
            altBoundary: true
        });
        const referenceClippingOffsets = getSideOffsets(referenceOverflow, referenceRect);
        const popperEscapeOffsets = getSideOffsets(popperAltOverflow, popperRect, preventedOffsets);
        const isReferenceHidden = isAnySideFullyClipped(referenceClippingOffsets);
        const hasPopperEscaped = isAnySideFullyClipped(popperEscapeOffsets);
        state.modifiersData[name] = {
            referenceClippingOffsets,
            popperEscapeOffsets,
            isReferenceHidden,
            hasPopperEscaped
        };
        state.attributes.popper = Object.assign(Object.assign({}, state.attributes.popper), { 'data-popper-reference-hidden': isReferenceHidden, 'data-popper-escaped': hasPopperEscaped });
    }
    var hide$1 = {
        name: 'hide',
        enabled: true,
        phase: 'main',
        requiresIfExists: ['preventOverflow'],
        fn: hide
    };

    // @ts-nocheck
    const defaultModifiers = [
        eventListeners,
        popperOffsets$1,
        computeStyles$1,
        applyStyles$1,
        offset$1,
        flip$1,
        preventOverflow$1,
        arrow$1,
        hide$1
    ];
    const createPopper = popperGenerator({ defaultModifiers });

    /* eslint-disable @typescript-eslint/consistent-type-definitions */
    const isEqual = (a, b) => JSON.stringify(a) === JSON.stringify(b);
    /**
     * Simple ponyfill for Object.fromEntries
     */
    const fromEntries = (entries) => entries.reduce((acc, [key, value]) => {
        acc[key] = value;
        return acc;
    }, {});
    /**
     * Small wrapper around `useLayoutEffect` to get rid of the warning on SSR envs
     */
    const useIsomorphicLayoutEffect = typeof window !== 'undefined' && window.document && window.document.createElement
        ? React.useLayoutEffect
        : React.useEffect;
    const EMPTY_MODIFIERS = [];
    const usePopper = (referenceElement, popperElement, options = {}) => {
        const prevOptions = React.useRef(null);
        const optionsWithDefaults = {
            onFirstUpdate: options.onFirstUpdate,
            placement: options.placement || 'bottom',
            strategy: options.strategy || 'absolute',
            modifiers: options.modifiers || EMPTY_MODIFIERS
        };
        const [state, setState] = React.useState({
            styles: {
                popper: {
                    position: optionsWithDefaults.strategy,
                    left: '0',
                    top: '0'
                }
            },
            attributes: {}
        });
        const updateStateModifier = React.useMemo(() => ({
            name: 'updateState',
            enabled: true,
            phase: 'write',
            // eslint-disable-next-line no-shadow
            fn: ({ state }) => {
                const elements = Object.keys(state.elements);
                setState({
                    styles: fromEntries(elements.map(element => [element, state.styles[element] || {}])),
                    attributes: fromEntries(elements.map(element => [element, state.attributes[element]]))
                });
            },
            requires: ['computeStyles']
        }), []);
        const popperOptions = React.useMemo(() => {
            const newOptions = {
                onFirstUpdate: optionsWithDefaults.onFirstUpdate,
                placement: optionsWithDefaults.placement,
                strategy: optionsWithDefaults.strategy,
                modifiers: [...optionsWithDefaults.modifiers, updateStateModifier, { name: 'applyStyles', enabled: false }]
            };
            if (isEqual(prevOptions.current, newOptions)) {
                return prevOptions.current || newOptions;
            }
            else {
                prevOptions.current = newOptions;
                return newOptions;
            }
        }, [
            optionsWithDefaults.onFirstUpdate,
            optionsWithDefaults.placement,
            optionsWithDefaults.strategy,
            optionsWithDefaults.modifiers,
            updateStateModifier
        ]);
        const popperInstanceRef = React.useRef();
        useIsomorphicLayoutEffect(() => {
            if (popperInstanceRef && popperInstanceRef.current) {
                popperInstanceRef.current.setOptions(popperOptions);
            }
        }, [popperOptions]);
        useIsomorphicLayoutEffect(() => {
            if (referenceElement == null || popperElement == null) {
                return;
            }
            const createPopper$1 = options.createPopper || createPopper;
            const popperInstance = createPopper$1(referenceElement, popperElement, popperOptions);
            popperInstanceRef.current = popperInstance;
            return () => {
                popperInstance.destroy();
                popperInstanceRef.current = null;
            };
        }, [referenceElement, popperElement, options.createPopper]);
        return {
            state: popperInstanceRef.current ? popperInstanceRef.current.state : null,
            styles: state.styles,
            attributes: state.attributes,
            update: popperInstanceRef.current ? popperInstanceRef.current.update : null,
            forceUpdate: popperInstanceRef.current ? popperInstanceRef.current.forceUpdate : null
        };
    };

    const hash$2 = { left: 'right', right: 'left', bottom: 'top', top: 'bottom' };
    const getOppositePlacement$1 = (placement) => placement.replace(/left|right|bottom|top/g, (matched) => hash$2[matched]);
    const getOpacityTransition = (animationDuration) => `opacity ${animationDuration}ms cubic-bezier(.54, 1.5, .38, 1.11)`;
    const Popper = ({ trigger, popper, popperMatchesTriggerWidth = true, direction = 'down', position = 'left', placement, appendTo = () => document.body, zIndex = 9999, isVisible = true, positionModifiers, distance = 0, onMouseEnter, onMouseLeave, onFocus, onBlur, onDocumentClick, onTriggerClick, onTriggerEnter, onPopperClick, onDocumentKeyDown, enableFlip = true, flipBehavior = 'flip' }) => {
        const [triggerElement, setTriggerElement] = React.useState(null);
        const [popperElement, setPopperElement] = React.useState(null);
        const [ready, setReady] = React.useState(false);
        const onDocumentClickCallback = React.useCallback(event => onDocumentClick(event, triggerElement), [
            isVisible,
            triggerElement,
            onDocumentClick
        ]);
        React.useEffect(() => {
            setReady(true);
        }, []);
        const addEventListener = (listener, element, event) => {
            if (listener && element) {
                element.addEventListener(event, listener);
            }
        };
        const removeEventListener = (listener, element, event) => {
            if (listener && element) {
                element.removeEventListener(event, listener);
            }
        };
        React.useEffect(() => {
            addEventListener(onMouseEnter, triggerElement, 'mouseenter');
            addEventListener(onMouseLeave, triggerElement, 'mouseleave');
            addEventListener(onFocus, triggerElement, 'focus');
            addEventListener(onBlur, triggerElement, 'blur');
            addEventListener(onTriggerClick, triggerElement, 'click');
            addEventListener(onTriggerEnter, triggerElement, 'keydown');
            addEventListener(onPopperClick, popperElement, 'click');
            onDocumentClick && addEventListener(onDocumentClickCallback, document, 'click');
            addEventListener(onDocumentKeyDown, document, 'keydown');
            return () => {
                removeEventListener(onMouseEnter, triggerElement, 'mouseenter');
                removeEventListener(onMouseLeave, triggerElement, 'mouseleave');
                removeEventListener(onFocus, triggerElement, 'focus');
                removeEventListener(onBlur, triggerElement, 'blur');
                removeEventListener(onTriggerClick, triggerElement, 'click');
                removeEventListener(onTriggerEnter, triggerElement, 'keydown');
                removeEventListener(onPopperClick, popperElement, 'click');
                onDocumentClick && removeEventListener(onDocumentClickCallback, document, 'click');
                removeEventListener(onDocumentKeyDown, document, 'keydown');
            };
        }, [
            triggerElement,
            popperElement,
            onMouseEnter,
            onMouseLeave,
            onFocus,
            onBlur,
            onTriggerClick,
            onTriggerEnter,
            onPopperClick,
            onDocumentClick,
            onDocumentKeyDown
        ]);
        const getPlacement = () => {
            if (placement) {
                return placement;
            }
            let convertedPlacement = direction === 'up' ? 'top' : 'bottom';
            if (position !== 'center') {
                convertedPlacement = `${convertedPlacement}-${position === 'right' ? 'end' : 'start'}`;
            }
            return convertedPlacement;
        };
        const getPlacementMemo = React.useMemo(getPlacement, [direction, position, placement]);
        const getOppositePlacementMemo = React.useMemo(() => getOppositePlacement$1(getPlacement()), [
            direction,
            position,
            placement
        ]);
        const sameWidthMod = React.useMemo(() => ({
            name: 'sameWidth',
            enabled: popperMatchesTriggerWidth,
            phase: 'beforeWrite',
            requires: ['computeStyles'],
            fn: ({ state }) => {
                state.styles.popper.width = `${state.rects.reference.width}px`;
            },
            effect: ({ state }) => {
                state.elements.popper.style.width = `${state.elements.reference.offsetWidth}px`;
                return () => { };
            }
        }), [popperMatchesTriggerWidth]);
        const { styles: popperStyles, attributes } = usePopper(triggerElement, popperElement, {
            placement: getPlacementMemo,
            modifiers: [
                {
                    name: 'offset',
                    options: {
                        offset: [0, distance]
                    }
                },
                {
                    name: 'preventOverflow',
                    enabled: false
                },
                {
                    name: 'hide',
                    enabled: false
                },
                {
                    name: 'flip',
                    enabled: getPlacementMemo.startsWith('auto') || enableFlip,
                    options: {
                        fallbackPlacements: flipBehavior === 'flip' ? [getOppositePlacementMemo] : flipBehavior
                    }
                },
                sameWidthMod
            ]
        });
        const modifierFromPopperPosition = () => {
            if (attributes && attributes.popper && attributes.popper['data-popper-placement']) {
                const popperPlacement = attributes.popper['data-popper-placement'];
                if (popperPlacement.startsWith('top')) {
                    return positionModifiers.top || '';
                }
                else if (popperPlacement.startsWith('bottom')) {
                    return positionModifiers.bottom || '';
                }
                else if (popperPlacement.startsWith('left')) {
                    return positionModifiers.left || '';
                }
                else if (popperPlacement.startsWith('right')) {
                    return positionModifiers.right || '';
                }
            }
            return positionModifiers.top;
        };
        const menuWithPopper = React.cloneElement(popper, Object.assign({ className: css(popper.props && popper.props.className, positionModifiers && modifierFromPopperPosition()), style: Object.assign(Object.assign(Object.assign({}, ((popper.props && popper.props.style) || {})), popperStyles.popper), { zIndex }) }, attributes.popper));
        const getTarget = () => {
            if (typeof appendTo === 'function') {
                return appendTo();
            }
            return appendTo;
        };
        return (React.createElement(React.Fragment, null,
            React.createElement(FindRefWrapper, { onFoundRef: (foundRef) => setTriggerElement(foundRef) }, trigger),
            ready &&
                isVisible &&
                ReactDOM.createPortal(React.createElement(FindRefWrapper, { onFoundRef: (foundRef) => setPopperElement(foundRef) }, menuWithPopper), getTarget())));
    };
    Popper.displayName = 'Popper';

    class DropdownWithContext extends React.Component {
        constructor(props) {
            super(props);
            this.openedOnEnter = false;
            this.baseComponentRef = React.createRef();
            this.menuComponentRef = React.createRef();
            this.onEnter = () => {
                this.openedOnEnter = true;
            };
            this.setMenuComponentRef = (element) => {
                this.menuComponentRef = element;
            };
            this.getMenuComponentRef = () => this.menuComponentRef;
            if (props.dropdownItems && props.dropdownItems.length > 0 && props.children) {
                // eslint-disable-next-line no-console
                console.error('Children and dropdownItems props have been provided. Only the dropdownItems prop items will be rendered');
            }
        }
        componentDidUpdate() {
            if (!this.props.isOpen) {
                this.openedOnEnter = false;
            }
        }
        render() {
            const _a = this.props, { children, className, direction, dropdownItems, isOpen, isPlain, isGrouped, 
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            onSelect, position, toggle, autoFocus, menuAppendTo } = _a, props = __rest(_a, ["children", "className", "direction", "dropdownItems", "isOpen", "isPlain", "isGrouped", "onSelect", "position", "toggle", "autoFocus", "menuAppendTo"]);
            const id = toggle.props.id || `pf-dropdown-toggle-id-${DropdownWithContext.currentId++}`;
            let component;
            let renderedContent;
            let ariaHasPopup = false;
            if (dropdownItems && dropdownItems.length > 0) {
                component = 'ul';
                renderedContent = dropdownItems;
                ariaHasPopup = true;
            }
            else {
                component = 'div';
                renderedContent = React.Children.toArray(children);
            }
            const openedOnEnter = this.openedOnEnter;
            return (React.createElement(DropdownContext.Consumer, null, ({ baseClass, baseComponent, id: contextId, ouiaId, ouiaComponentType, ouiaSafe }) => {
                const BaseComponent = baseComponent;
                const menuContainer = (React.createElement(DropdownMenu, { setMenuComponentRef: this.setMenuComponentRef, component: component, isOpen: isOpen, position: position, "aria-labelledby": contextId ? `${contextId}-toggle` : id, openedOnEnter: openedOnEnter, isGrouped: isGrouped, autoFocus: openedOnEnter && autoFocus }, renderedContent));
                const popperContainer = (React.createElement("div", { className: css(baseClass, direction === exports.DropdownDirection.up && styles$9.modifiers.top, position === exports.DropdownPosition.right && styles$9.modifiers.alignRight, isOpen && styles$9.modifiers.expanded, className) }, isOpen && menuContainer));
                const mainContainer = (React.createElement(BaseComponent, Object.assign({}, props, { className: css(baseClass, direction === exports.DropdownDirection.up && styles$9.modifiers.top, position === exports.DropdownPosition.right && styles$9.modifiers.alignRight, isOpen && styles$9.modifiers.expanded, className), ref: this.baseComponentRef }, getOUIAProps(ouiaComponentType, ouiaId, ouiaSafe)),
                    React.Children.map(toggle, oneToggle => React.cloneElement(oneToggle, {
                        parentRef: this.baseComponentRef,
                        getMenuRef: this.getMenuComponentRef,
                        isOpen,
                        id,
                        isPlain,
                        'aria-haspopup': ariaHasPopup,
                        onEnter: () => this.onEnter()
                    })),
                    menuAppendTo === 'inline' && isOpen && menuContainer));
                const getParentElement = () => {
                    if (this.baseComponentRef && this.baseComponentRef.current) {
                        return this.baseComponentRef.current.parentElement;
                    }
                    return null;
                };
                return menuAppendTo === 'inline' ? (mainContainer) : (React.createElement(Popper, { trigger: mainContainer, popper: popperContainer, direction: direction, position: position, appendTo: menuAppendTo === 'parent' ? getParentElement() : menuAppendTo, isVisible: isOpen }));
            }));
        }
    }
    DropdownWithContext.displayName = 'DropdownWithContext';
    // seed for the aria-labelledby ID
    DropdownWithContext.currentId = 0;
    DropdownWithContext.defaultProps = {
        className: '',
        dropdownItems: [],
        isOpen: false,
        isPlain: false,
        isGrouped: false,
        position: exports.DropdownPosition.left,
        direction: exports.DropdownDirection.down,
        onSelect: () => undefined,
        autoFocus: true,
        menuAppendTo: 'inline'
    };

    const Dropdown = (_a) => {
        var { onSelect, 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        ref, // Types of Ref are different for React.FC vs React.Component
        ouiaId, ouiaSafe } = _a, props = __rest(_a, ["onSelect", "ref", "ouiaId", "ouiaSafe"]);
        return (React.createElement(DropdownContext.Provider, { value: {
                onSelect: event => onSelect && onSelect(event),
                toggleTextClass: styles$9.dropdownToggleText,
                toggleIconClass: styles$9.dropdownToggleImage,
                toggleIndicatorClass: styles$9.dropdownToggleIcon,
                menuClass: styles$9.dropdownMenu,
                itemClass: styles$9.dropdownMenuItem,
                toggleClass: styles$9.dropdownToggle,
                baseClass: styles$9.dropdown,
                baseComponent: 'div',
                sectionClass: styles$9.dropdownGroup,
                sectionTitleClass: styles$9.dropdownGroupTitle,
                sectionComponent: 'section',
                disabledClass: styles$9.modifiers.disabled,
                plainTextClass: styles$9.modifiers.text,
                ouiaId,
                ouiaSafe,
                ouiaComponentType: Dropdown.displayName
            } },
            React.createElement(DropdownWithContext, Object.assign({}, props))));
    };
    Dropdown.displayName = 'Dropdown';

    const DropdownGroup = ({ children = null, className = '', label = '' }) => (React.createElement(DropdownContext.Consumer, null, ({ sectionClass, sectionTitleClass, sectionComponent }) => {
        const SectionComponent = sectionComponent;
        return (React.createElement(SectionComponent, { className: css(sectionClass, className) },
            label && (React.createElement("h1", { className: css(sectionTitleClass), "aria-hidden": true }, label)),
            React.createElement("ul", { role: "none" }, children)));
    }));
    DropdownGroup.displayName = 'DropdownGroup';

    var tooltip = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "modifiers": {
        "top": "pf-m-top",
        "bottom": "pf-m-bottom",
        "left": "pf-m-left",
        "right": "pf-m-right",
        "textAlignLeft": "pf-m-text-align-left"
      },
      "tooltip": "pf-c-tooltip",
      "tooltipArrow": "pf-c-tooltip__arrow",
      "tooltipContent": "pf-c-tooltip__content"
    };
    });

    var styles$a = unwrapExports(tooltip);

    const TooltipContent = (_a) => {
        var { className, children, isLeftAligned } = _a, props = __rest(_a, ["className", "children", "isLeftAligned"]);
        return (React.createElement("div", Object.assign({ className: css(styles$a.tooltipContent, isLeftAligned && styles$a.modifiers.textAlignLeft, className) }, props), children));
    };
    TooltipContent.displayName = 'TooltipContent';

    const TooltipArrow = (_a) => {
        var { className } = _a, props = __rest(_a, ["className"]);
        return React.createElement("div", Object.assign({ className: css(styles$a.tooltipArrow, className) }, props));
    };
    TooltipArrow.displayName = 'TooltipArrow';

    var c_tooltip_MaxWidth = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.c_tooltip_MaxWidth = {
      "name": "--pf-c-tooltip--MaxWidth",
      "value": "18.75rem",
      "var": "var(--pf-c-tooltip--MaxWidth)"
    };
    exports["default"] = exports.c_tooltip_MaxWidth;
    });

    var tooltipMaxWidth = unwrapExports(c_tooltip_MaxWidth);
    var c_tooltip_MaxWidth_1 = c_tooltip_MaxWidth.c_tooltip_MaxWidth;

    (function (TooltipPosition) {
        TooltipPosition["auto"] = "auto";
        TooltipPosition["top"] = "top";
        TooltipPosition["bottom"] = "bottom";
        TooltipPosition["left"] = "left";
        TooltipPosition["right"] = "right";
    })(exports.TooltipPosition || (exports.TooltipPosition = {}));
    // id for associating trigger with the content aria-describedby or aria-labelledby
    let pfTooltipIdCounter = 1;
    const Tooltip = (_a) => {
        var { content: bodyContent, position = 'top', trigger = 'mouseenter focus', isVisible = false, isContentLeftAligned = false, enableFlip = true, className = '', entryDelay = 0, exitDelay = 0, appendTo = () => document.body, zIndex = 9999, maxWidth = tooltipMaxWidth.value, distance = 15, aria = 'describedby', 
        // For every initial starting position, there are 3 escape positions
        flipBehavior = ['top', 'right', 'bottom', 'left', 'top', 'right', 'bottom'], id = `pf-tooltip-${pfTooltipIdCounter++}`, children, animationDuration = 300, boundary, isAppLauncher, tippyProps } = _a, rest = __rest(_a, ["content", "position", "trigger", "isVisible", "isContentLeftAligned", "enableFlip", "className", "entryDelay", "exitDelay", "appendTo", "zIndex", "maxWidth", "distance", "aria", "flipBehavior", "id", "children", "animationDuration", "boundary", "isAppLauncher", "tippyProps"]);
        {
            boundary !== undefined &&
                console.warn('The Tooltip boundary prop has been deprecated. If you want to constrain the popper to a specific element use the appendTo prop instead.');
            isAppLauncher !== undefined &&
                console.warn('The Tooltip isAppLauncher prop has been deprecated and is no longer used.');
            tippyProps !== undefined && console.warn('The Tooltip tippyProps prop has been deprecated and is no longer used.');
        }
        const triggerOnMouseenter = trigger.includes('mouseenter');
        const triggerOnFocus = trigger.includes('focus');
        const triggerOnClick = trigger.includes('click');
        const triggerManually = trigger === 'manual';
        const [visible, setVisible] = React.useState(false);
        const [opacity, setOpacity] = React.useState(0);
        const transitionTimerRef = React.useRef(null);
        const showTimerRef = React.useRef(null);
        const hideTimerRef = React.useRef(null);
        const onDocumentKeyDown = (event) => {
            if (!triggerManually) {
                if (event.keyCode === KEY_CODES.ESCAPE_KEY && visible) {
                    hide();
                }
            }
        };
        const onTriggerEnter = (event) => {
            if (event.keyCode === KEY_CODES.ENTER) {
                if (!visible) {
                    show();
                }
                else {
                    hide();
                }
            }
        };
        React.useEffect(() => {
            if (triggerManually) {
                if (isVisible) {
                    show();
                }
                else {
                    hide();
                }
            }
        }, [isVisible, triggerManually]);
        const show = () => {
            if (transitionTimerRef.current) {
                clearTimeout(transitionTimerRef.current);
            }
            if (hideTimerRef.current) {
                clearTimeout(hideTimerRef.current);
            }
            showTimerRef.current = setTimeout(() => {
                setVisible(true);
                setOpacity(1);
            }, entryDelay);
        };
        const hide = () => {
            if (showTimerRef.current) {
                clearTimeout(showTimerRef.current);
            }
            hideTimerRef.current = setTimeout(() => {
                setOpacity(0);
                transitionTimerRef.current = setTimeout(() => setVisible(false), animationDuration);
            }, exitDelay);
        };
        const positionModifiers = {
            top: styles$a.modifiers.top,
            bottom: styles$a.modifiers.bottom,
            left: styles$a.modifiers.left,
            right: styles$a.modifiers.right
        };
        const hasCustomMaxWidth = maxWidth !== tooltipMaxWidth.value;
        const content = (React.createElement("div", Object.assign({ className: css(styles$a.tooltip, className), role: "tooltip", id: id, style: {
                maxWidth: hasCustomMaxWidth ? maxWidth : null,
                opacity,
                transition: getOpacityTransition(animationDuration)
            } }, rest),
            React.createElement(TooltipArrow, null),
            React.createElement(TooltipContent, { isLeftAligned: isContentLeftAligned }, bodyContent)));
        const onDocumentClick = (event, triggerElement) => {
            // event.currentTarget = document
            // event.target could be triggerElement or something else
            {
                // hide on inside the toggle as well as on outside clicks
                if (visible) {
                    hide();
                }
                else if (event.target === triggerElement) {
                    show();
                }
            }
        };
        const addAriaToTrigger = () => {
            if (aria === 'describedby' && children.props && !children.props['aria-describedby']) {
                return React.cloneElement(children, { 'aria-describedby': id });
            }
            else if (aria === 'labelledby' && children.props && !children.props['aria-labelledby']) {
                return React.cloneElement(children, { 'aria-labelledby': id });
            }
            return children;
        };
        return (React.createElement(Popper, { trigger: aria !== 'none' ? addAriaToTrigger() : children, popper: content, popperMatchesTriggerWidth: false, appendTo: appendTo, isVisible: visible, positionModifiers: positionModifiers, distance: distance, placement: position, onMouseEnter: triggerOnMouseenter && show, onMouseLeave: triggerOnMouseenter && hide, onFocus: triggerOnFocus && show, onBlur: triggerOnFocus && hide, onDocumentClick: triggerOnClick && onDocumentClick, onDocumentKeyDown: triggerManually ? null : onDocumentKeyDown, onTriggerEnter: triggerManually ? null : onTriggerEnter, enableFlip: enableFlip, zIndex: zIndex, flipBehavior: flipBehavior }));
    };
    Tooltip.displayName = 'Tooltip';

    class InternalDropdownItem extends React.Component {
        constructor() {
            super(...arguments);
            this.ref = React.createRef();
            this.additionalRef = React.createRef();
            this.getInnerNode = (node) => (node && node.childNodes && node.childNodes.length ? node.childNodes[0] : node);
            this.onKeyDown = (event) => {
                // Detected key press on this item, notify the menu parent so that the appropriate item can be focused
                const innerIndex = event.target === this.ref.current ? 0 : 1;
                if (!this.props.customChild) {
                    event.preventDefault();
                }
                if (event.key === 'ArrowUp') {
                    this.props.context.keyHandler(this.props.index, innerIndex, KEYHANDLER_DIRECTION.UP);
                }
                else if (event.key === 'ArrowDown') {
                    this.props.context.keyHandler(this.props.index, innerIndex, KEYHANDLER_DIRECTION.DOWN);
                }
                else if (event.key === 'ArrowRight') {
                    this.props.context.keyHandler(this.props.index, innerIndex, KEYHANDLER_DIRECTION.RIGHT);
                }
                else if (event.key === 'ArrowLeft') {
                    this.props.context.keyHandler(this.props.index, innerIndex, KEYHANDLER_DIRECTION.LEFT);
                }
                else if (event.key === 'Enter' || event.key === ' ') {
                    event.target.click();
                    this.props.enterTriggersArrowDown &&
                        this.props.context.keyHandler(this.props.index, innerIndex, KEYHANDLER_DIRECTION.DOWN);
                }
            };
        }
        componentDidMount() {
            const { context, index, isDisabled, role, customChild, autoFocus } = this.props;
            const customRef = customChild ? this.getInnerNode(this.ref.current) : this.ref.current;
            context.sendRef(index, [customRef, customChild ? customRef : this.additionalRef.current], isDisabled, role === 'separator');
            autoFocus && setTimeout(() => customRef.focus());
        }
        componentDidUpdate() {
            const { context, index, isDisabled, role, customChild } = this.props;
            const customRef = customChild ? this.getInnerNode(this.ref.current) : this.ref.current;
            context.sendRef(index, [customRef, customChild ? customRef : this.additionalRef.current], isDisabled, role === 'separator');
        }
        extendAdditionalChildRef() {
            const { additionalChild } = this.props;
            return React.cloneElement(additionalChild, {
                ref: this.additionalRef
            });
        }
        render() {
            /* eslint-disable @typescript-eslint/no-unused-vars */
            const _a = this.props, { className, children, isHovered, context, onClick, component, role, isDisabled, isPlainText, index, href, tooltip, tooltipProps, id, componentID, listItemClassName, additionalChild, customChild, enterTriggersArrowDown, icon, autoFocus, styleChildren, description } = _a, additionalProps = __rest(_a, ["className", "children", "isHovered", "context", "onClick", "component", "role", "isDisabled", "isPlainText", "index", "href", "tooltip", "tooltipProps", "id", "componentID", "listItemClassName", "additionalChild", "customChild", "enterTriggersArrowDown", "icon", "autoFocus", "styleChildren", "description"]);
            /* eslint-enable @typescript-eslint/no-unused-vars */
            let classes = css(icon && styles$9.modifiers.icon, className);
            if (component === 'a') {
                additionalProps['aria-disabled'] = isDisabled;
                additionalProps.tabIndex = isDisabled ? -1 : additionalProps.tabIndex;
            }
            else if (component === 'button') {
                additionalProps.disabled = isDisabled;
                additionalProps.type = additionalProps.type || 'button';
            }
            const renderWithTooltip = (childNode) => tooltip ? (React.createElement(Tooltip, Object.assign({ content: tooltip }, tooltipProps), childNode)) : (childNode);
            const renderClonedComponent = (element) => React.cloneElement(element, Object.assign({}, (styleChildren && {
                className: css(element.props.className, classes)
            })));
            const renderDefaultComponent = (tag) => {
                const Component = tag;
                const componentContent = description ? (React.createElement(React.Fragment, null,
                    React.createElement("div", { className: styles$9.dropdownMenuItemMain },
                        icon && React.createElement("span", { className: css(styles$9.dropdownMenuItemIcon) }, icon),
                        children),
                    React.createElement("div", { className: styles$9.dropdownMenuItemDescription }, description))) : (React.createElement(React.Fragment, null,
                    icon && React.createElement("span", { className: css(styles$9.dropdownMenuItemIcon) }, icon),
                    children));
                return (React.createElement(Component, Object.assign({}, additionalProps, { href: href, ref: this.ref, className: classes, id: componentID }), componentContent));
            };
            return (React.createElement(DropdownContext.Consumer, null, ({ onSelect, itemClass, disabledClass, plainTextClass }) => {
                if (this.props.role !== 'separator') {
                    classes = css(classes, isDisabled && disabledClass, isPlainText && plainTextClass, itemClass, description && styles$9.modifiers.description);
                }
                if (customChild) {
                    return React.cloneElement(customChild, {
                        ref: this.ref,
                        onKeyDown: this.onKeyDown
                    });
                }
                const componentContent = description ? (React.createElement(React.Fragment, null,
                    React.createElement("div", { className: styles$9.dropdownMenuItemMain },
                        icon && React.createElement("span", { className: css(styles$9.dropdownMenuItemIcon) }, icon),
                        children),
                    React.createElement("div", { className: styles$9.dropdownMenuItemDescription }, description))) : (React.createElement(React.Fragment, null,
                    icon && React.createElement("span", { className: css(styles$9.dropdownMenuItemIcon) }, icon),
                    children));
                return (React.createElement("li", { className: listItemClassName || null, role: role, onKeyDown: this.onKeyDown, onClick: (event) => {
                        if (!isDisabled) {
                            onClick(event);
                            onSelect(event);
                        }
                    }, id: id },
                    renderWithTooltip(React.isValidElement(component)
                        ? renderClonedComponent(component)
                        : renderDefaultComponent(component)),
                    additionalChild && this.extendAdditionalChildRef()));
            }));
        }
    }
    InternalDropdownItem.displayName = 'InternalDropdownItem';
    InternalDropdownItem.defaultProps = {
        className: '',
        isHovered: false,
        component: 'a',
        role: 'none',
        isDisabled: false,
        isPlainText: false,
        tooltipProps: {},
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        onClick: (event) => undefined,
        index: -1,
        context: {
            keyHandler: () => { },
            sendRef: () => { }
        },
        enterTriggersArrowDown: false,
        icon: null,
        styleChildren: true,
        description: null
    };

    const DropdownItem = (_a) => {
        var { children, className, component = 'a', isDisabled = false, isPlainText = false, isHovered = false, href, tooltip, tooltipProps = {}, listItemClassName, onClick, 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        ref, // Types of Ref are different for React.FC vs React.Component
        additionalChild, customChild, tabIndex = -1, icon = null, autoFocus, description = null, styleChildren } = _a, props = __rest(_a, ["children", "className", "component", "isDisabled", "isPlainText", "isHovered", "href", "tooltip", "tooltipProps", "listItemClassName", "onClick", "ref", "additionalChild", "customChild", "tabIndex", "icon", "autoFocus", "description", "styleChildren"]);
        return (React.createElement(DropdownArrowContext.Consumer, null, context => (React.createElement(InternalDropdownItem, Object.assign({ context: context, role: "menuitem", tabIndex: tabIndex, className: className, component: component, isDisabled: isDisabled, isPlainText: isPlainText, isHovered: isHovered, href: href, tooltip: tooltip, tooltipProps: tooltipProps, listItemClassName: listItemClassName, onClick: onClick, additionalChild: additionalChild, customChild: customChild, icon: icon, autoFocus: autoFocus, styleChildren: styleChildren, description: description }, props), children))));
    };
    DropdownItem.displayName = 'DropdownItem';

    var divider = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "divider": "pf-c-divider",
      "modifiers": {
        "vertical": "pf-m-vertical",
        "insetNone": "pf-m-inset-none",
        "insetXs": "pf-m-inset-xs",
        "insetSm": "pf-m-inset-sm",
        "insetMd": "pf-m-inset-md",
        "insetLg": "pf-m-inset-lg",
        "insetXl": "pf-m-inset-xl",
        "inset_2xl": "pf-m-inset-2xl",
        "inset_3xl": "pf-m-inset-3xl",
        "insetNoneOnSm": "pf-m-inset-none-on-sm",
        "insetXsOnSm": "pf-m-inset-xs-on-sm",
        "insetSmOnSm": "pf-m-inset-sm-on-sm",
        "insetMdOnSm": "pf-m-inset-md-on-sm",
        "insetLgOnSm": "pf-m-inset-lg-on-sm",
        "insetXlOnSm": "pf-m-inset-xl-on-sm",
        "inset_2xlOnSm": "pf-m-inset-2xl-on-sm",
        "inset_3xlOnSm": "pf-m-inset-3xl-on-sm",
        "insetNoneOnMd": "pf-m-inset-none-on-md",
        "insetXsOnMd": "pf-m-inset-xs-on-md",
        "insetSmOnMd": "pf-m-inset-sm-on-md",
        "insetMdOnMd": "pf-m-inset-md-on-md",
        "insetLgOnMd": "pf-m-inset-lg-on-md",
        "insetXlOnMd": "pf-m-inset-xl-on-md",
        "inset_2xlOnMd": "pf-m-inset-2xl-on-md",
        "inset_3xlOnMd": "pf-m-inset-3xl-on-md",
        "insetNoneOnLg": "pf-m-inset-none-on-lg",
        "insetXsOnLg": "pf-m-inset-xs-on-lg",
        "insetSmOnLg": "pf-m-inset-sm-on-lg",
        "insetMdOnLg": "pf-m-inset-md-on-lg",
        "insetLgOnLg": "pf-m-inset-lg-on-lg",
        "insetXlOnLg": "pf-m-inset-xl-on-lg",
        "inset_2xlOnLg": "pf-m-inset-2xl-on-lg",
        "inset_3xlOnLg": "pf-m-inset-3xl-on-lg",
        "insetNoneOnXl": "pf-m-inset-none-on-xl",
        "insetXsOnXl": "pf-m-inset-xs-on-xl",
        "insetSmOnXl": "pf-m-inset-sm-on-xl",
        "insetMdOnXl": "pf-m-inset-md-on-xl",
        "insetLgOnXl": "pf-m-inset-lg-on-xl",
        "insetXlOnXl": "pf-m-inset-xl-on-xl",
        "inset_2xlOnXl": "pf-m-inset-2xl-on-xl",
        "inset_3xlOnXl": "pf-m-inset-3xl-on-xl",
        "insetNoneOn_2xl": "pf-m-inset-none-on-2xl",
        "insetXsOn_2xl": "pf-m-inset-xs-on-2xl",
        "insetSmOn_2xl": "pf-m-inset-sm-on-2xl",
        "insetMdOn_2xl": "pf-m-inset-md-on-2xl",
        "insetLgOn_2xl": "pf-m-inset-lg-on-2xl",
        "insetXlOn_2xl": "pf-m-inset-xl-on-2xl",
        "inset_2xlOn_2xl": "pf-m-inset-2xl-on-2xl",
        "inset_3xlOn_2xl": "pf-m-inset-3xl-on-2xl"
      }
    };
    });

    var styles$b = unwrapExports(divider);

    (function (DividerVariant) {
        DividerVariant["hr"] = "hr";
        DividerVariant["li"] = "li";
        DividerVariant["div"] = "div";
    })(exports.DividerVariant || (exports.DividerVariant = {}));
    const Divider = (_a) => {
        var { className, component = exports.DividerVariant.hr, isVertical = false, inset } = _a, props = __rest(_a, ["className", "component", "isVertical", "inset"]);
        const Component = component;
        return (React.createElement(Component, Object.assign({ className: css(styles$b.divider, isVertical && styles$b.modifiers.vertical, formatBreakpointMods(inset, styles$b), className) }, (component !== 'hr' && { role: 'separator' }), props)));
    };
    Divider.displayName = 'Divider';

    const DropdownSeparator = (_a) => {
        var { className = '', 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        ref } = _a, // Types of Ref are different for React.FC vs React.Component
        props = __rest(_a, ["className", "ref"]);
        return (React.createElement(DropdownArrowContext.Consumer, null, context => (React.createElement(InternalDropdownItem, Object.assign({}, props, { context: context, component: React.createElement(Divider, { component: exports.DividerVariant.div }), className: className, role: "separator" })))));
    };
    DropdownSeparator.displayName = 'DropdownSeparator';

    var ellipsisVIcon = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.EllipsisVIconConfig = {
      name: 'EllipsisVIcon',
      height: 512,
      width: 192,
      svgPath: 'M96 184c39.8 0 72 32.2 72 72s-32.2 72-72 72-72-32.2-72-72 32.2-72 72-72zM24 80c0 39.8 32.2 72 72 72s72-32.2 72-72S135.8 8 96 8 24 40.2 24 80zm0 352c0 39.8 32.2 72 72 72s72-32.2 72-72-32.2-72-72-72-72 32.2-72 72z',
      yOffset: 0,
      xOffset: 0,
      transform: ''
    };
    exports.EllipsisVIcon = createIcon_1.createIcon(exports.EllipsisVIconConfig);
    exports["default"] = exports.EllipsisVIcon;
    });

    var EllipsisVIcon = unwrapExports(ellipsisVIcon);
    var ellipsisVIcon_1 = ellipsisVIcon.EllipsisVIconConfig;
    var ellipsisVIcon_2 = ellipsisVIcon.EllipsisVIcon;

    class Toggle extends React.Component {
        constructor() {
            super(...arguments);
            this.buttonRef = React.createRef();
            this.componentDidMount = () => {
                document.addEventListener('mousedown', this.onDocClick);
                document.addEventListener('touchstart', this.onDocClick);
                document.addEventListener('keydown', this.onEscPress);
            };
            this.componentWillUnmount = () => {
                document.removeEventListener('mousedown', this.onDocClick);
                document.removeEventListener('touchstart', this.onDocClick);
                document.removeEventListener('keydown', this.onEscPress);
            };
            this.onDocClick = (event) => {
                const { isOpen, parentRef, onToggle, getMenuRef } = this.props;
                const menuRef = getMenuRef && getMenuRef();
                const clickedOnToggle = parentRef && parentRef.current && parentRef.current.contains(event.target);
                const clickedWithinMenu = menuRef && menuRef.contains && menuRef.contains(event.target);
                if (isOpen && !(clickedOnToggle || clickedWithinMenu)) {
                    onToggle(false, event);
                    this.buttonRef.current.focus();
                }
            };
            this.onEscPress = (event) => {
                const { parentRef, getMenuRef } = this.props;
                const keyCode = event.keyCode || event.which;
                const menuRef = getMenuRef && getMenuRef();
                const escFromToggle = parentRef && parentRef.current && parentRef.current.contains(event.target);
                const escFromWithinMenu = menuRef && menuRef.contains && menuRef.contains(event.target);
                if (this.props.isOpen &&
                    (keyCode === KEY_CODES.ESCAPE_KEY || event.key === 'Tab') &&
                    (escFromToggle || escFromWithinMenu)) {
                    this.props.onToggle(false, event);
                    this.buttonRef.current.focus();
                }
            };
            this.onKeyDown = (event) => {
                if (event.key === 'Tab' && !this.props.isOpen) {
                    return;
                }
                if (!this.props.bubbleEvent) {
                    event.stopPropagation();
                }
                event.preventDefault();
                if ((event.key === 'Tab' || event.key === 'Enter' || event.key === ' ') && this.props.isOpen) {
                    this.props.onToggle(!this.props.isOpen, event);
                }
                else if ((event.key === 'Enter' || event.key === ' ') && !this.props.isOpen) {
                    this.props.onToggle(!this.props.isOpen, event);
                    this.props.onEnter();
                }
            };
        }
        render() {
            const _a = this.props, { className, children, isOpen, isDisabled, isPlain, isPrimary, isSplitButton, onToggle, 'aria-haspopup': ariaHasPopup, 
            /* eslint-disable @typescript-eslint/no-unused-vars */
            isActive, bubbleEvent, onEnter, parentRef, getMenuRef, 
            /* eslint-enable @typescript-eslint/no-unused-vars */
            id, type } = _a, props = __rest(_a, ["className", "children", "isOpen", "isDisabled", "isPlain", "isPrimary", "isSplitButton", "onToggle", 'aria-haspopup', "isActive", "bubbleEvent", "onEnter", "parentRef", "getMenuRef", "id", "type"]);
            return (React.createElement(DropdownContext.Consumer, null, ({ toggleClass }) => (React.createElement("button", Object.assign({}, props, { id: id, ref: this.buttonRef, className: css(isSplitButton ? styles$9.dropdownToggleButton : toggleClass || styles$9.dropdownToggle, isActive && styles$9.modifiers.active, isPlain && styles$9.modifiers.plain, isPrimary && styles$9.modifiers.primary, className), type: type || 'button', onClick: event => onToggle(!isOpen, event), "aria-expanded": isOpen, "aria-haspopup": ariaHasPopup, onKeyDown: event => this.onKeyDown(event), disabled: isDisabled }), children))));
        }
    }
    Toggle.displayName = 'Toggle';
    Toggle.defaultProps = {
        className: '',
        isOpen: false,
        isActive: false,
        isDisabled: false,
        isPlain: false,
        isPrimary: false,
        isSplitButton: false,
        onToggle: () => { },
        onEnter: () => { },
        bubbleEvent: false
    };

    const KebabToggle = (_a) => {
        var { id = '', 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        children = null, className = '', isOpen = false, 'aria-label': ariaLabel = 'Actions', parentRef = null, getMenuRef = null, isActive = false, isPlain = false, isDisabled = false, bubbleEvent = false, onToggle = () => undefined, 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        ref } = _a, // Types of Ref are different for React.FC vs React.Component
        props = __rest(_a, ["id", "children", "className", "isOpen", 'aria-label', "parentRef", "getMenuRef", "isActive", "isPlain", "isDisabled", "bubbleEvent", "onToggle", "ref"]);
        return (React.createElement(Toggle, Object.assign({ id: id, className: className, isOpen: isOpen, "aria-label": ariaLabel, parentRef: parentRef, getMenuRef: getMenuRef, isActive: isActive, isPlain: isPlain, isDisabled: isDisabled, onToggle: onToggle, bubbleEvent: bubbleEvent }, props),
            React.createElement(EllipsisVIcon, null)));
    };
    KebabToggle.displayName = 'KebabToggle';

    var caretDownIcon = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.CaretDownIconConfig = {
      name: 'CaretDownIcon',
      height: 512,
      width: 320,
      svgPath: 'M31.3 192h257.3c17.8 0 26.7 21.5 14.1 34.1L174.1 354.8c-7.8 7.8-20.5 7.8-28.3 0L17.2 226.1C4.6 213.5 13.5 192 31.3 192z',
      yOffset: 0,
      xOffset: 0,
      transform: ''
    };
    exports.CaretDownIcon = createIcon_1.createIcon(exports.CaretDownIconConfig);
    exports["default"] = exports.CaretDownIcon;
    });

    var CaretDownIcon = unwrapExports(caretDownIcon);
    var caretDownIcon_1 = caretDownIcon.CaretDownIconConfig;
    var caretDownIcon_2 = caretDownIcon.CaretDownIcon;

    const DropdownToggle = (_a) => {
        var { id = '', children = null, className = '', isOpen = false, parentRef = null, getMenuRef = null, isDisabled = false, isPlain = false, isPrimary = false, 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        isActive = false, 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        onToggle = (_isOpen) => undefined, icon = null, toggleIndicator: ToggleIndicator = CaretDownIcon, splitButtonItems, splitButtonVariant = 'checkbox', 'aria-haspopup': ariaHasPopup, 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        ref } = _a, // Types of Ref are different for React.FC vs React.Component
        props = __rest(_a, ["id", "children", "className", "isOpen", "parentRef", "getMenuRef", "isDisabled", "isPlain", "isPrimary", "isActive", "onToggle", "icon", "toggleIndicator", "splitButtonItems", "splitButtonVariant", 'aria-haspopup', "ref"]);
        const toggle = (React.createElement(DropdownContext.Consumer, null, ({ toggleTextClass, toggleIndicatorClass, toggleIconClass }) => (React.createElement(Toggle, Object.assign({}, props, { id: id, className: className, isOpen: isOpen, parentRef: parentRef, getMenuRef: getMenuRef, isActive: isActive, isDisabled: isDisabled, isPlain: isPlain, isPrimary: isPrimary, onToggle: onToggle, "aria-haspopup": ariaHasPopup }, (splitButtonItems && { isSplitButton: true, 'aria-label': props['aria-label'] || 'Select' })),
            icon && React.createElement("span", { className: css(toggleIconClass) }, icon),
            children && React.createElement("span", { className: ToggleIndicator && css(toggleTextClass) }, children),
            ToggleIndicator && (React.createElement("span", { className: css(!splitButtonItems && toggleIndicatorClass) },
                React.createElement(ToggleIndicator, null)))))));
        if (splitButtonItems) {
            return (React.createElement("div", { className: css(styles$9.dropdownToggle, styles$9.modifiers.splitButton, splitButtonVariant === 'action' && styles$9.modifiers.action, isDisabled && styles$9.modifiers.disabled) },
                splitButtonItems,
                toggle));
        }
        return toggle;
    };
    DropdownToggle.displayName = 'DropdownToggle';

    class DropdownToggleCheckbox extends React.Component {
        constructor() {
            super(...arguments);
            this.handleChange = (event) => {
                this.props.onChange(event.target.checked, event);
            };
            this.calculateChecked = () => {
                const { isChecked, checked } = this.props;
                return isChecked !== undefined ? isChecked : checked;
            };
        }
        render() {
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            const _a = this.props, { className, onChange, isValid, isDisabled, isChecked, checked, children } = _a, props = __rest(_a, ["className", "onChange", "isValid", "isDisabled", "isChecked", "checked", "children"]);
            const text = children && (React.createElement("span", { className: css(styles$9.dropdownToggleText, className), "aria-hidden": "true", id: `${props.id}-text` }, children));
            return (React.createElement("label", { className: css(styles$9.dropdownToggleCheck, className), htmlFor: props.id },
                React.createElement("input", Object.assign({}, props, (this.calculateChecked() !== undefined && { onChange: this.handleChange }), { type: "checkbox", ref: elem => elem && (elem.indeterminate = isChecked === null), "aria-invalid": !isValid, disabled: isDisabled, checked: this.calculateChecked() })),
                text));
        }
    }
    DropdownToggleCheckbox.displayName = 'DropdownToggleCheckbox';
    DropdownToggleCheckbox.defaultProps = {
        className: '',
        isValid: true,
        isDisabled: false,
        onChange: () => undefined
    };

    class DropdownToggleAction extends React.Component {
        render() {
            const _a = this.props, { id, className, onClick, isDisabled, children } = _a, props = __rest(_a, ["id", "className", "onClick", "isDisabled", "children"]);
            return (React.createElement("button", Object.assign({ id: id, className: css(styles$9.dropdownToggleButton, className), onClick: onClick }, (isDisabled && { disabled: true, 'aria-disabled': true }), props), children));
        }
    }
    DropdownToggleAction.displayName = 'DropdownToggleAction';
    DropdownToggleAction.defaultProps = {
        className: '',
        isDisabled: false,
        onClick: () => { }
    };

    const ApplicationLauncherGroup = (_a) => {
        var { children } = _a, props = __rest(_a, ["children"]);
        return React.createElement(DropdownGroup, Object.assign({}, props), children);
    };
    ApplicationLauncherGroup.displayName = 'ApplicationLauncherGroup';

    const ApplicationLauncherSeparator = (_a) => {
        var props = __rest(_a, ["children"]);
        return React.createElement(DropdownSeparator, Object.assign({}, props));
    };
    ApplicationLauncherSeparator.displayName = 'ApplicationLauncherSeparator';

    const ApplicationLauncherIcon = (_a) => {
        var { 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        className = '', children } = _a, props = __rest(_a, ["className", "children"]);
        return (React.createElement("span", Object.assign({ className: css(styles$8.appLauncherMenuItemIcon) }, props), children));
    };
    ApplicationLauncherIcon.displayName = 'ApplicationLauncherIcon';

    const ApplicationLauncherText = (_a) => {
        var { className = '', children } = _a, props = __rest(_a, ["className", "children"]);
        return (React.createElement("span", Object.assign({ className: css('pf-c-app-launcher__menu-item-text', className) }, props), children));
    };
    ApplicationLauncherText.displayName = 'ApplicationLauncherText';

    var externalLinkAltIcon = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.ExternalLinkAltIconConfig = {
      name: 'ExternalLinkAltIcon',
      height: 512,
      width: 512,
      svgPath: 'M432,320H400a16,16,0,0,0-16,16V448H64V128H208a16,16,0,0,0,16-16V80a16,16,0,0,0-16-16H48A48,48,0,0,0,0,112V464a48,48,0,0,0,48,48H400a48,48,0,0,0,48-48V336A16,16,0,0,0,432,320ZM488,0h-128c-21.37,0-32.05,25.91-17,41l35.73,35.73L135,320.37a24,24,0,0,0,0,34L157.67,377a24,24,0,0,0,34,0L435.28,133.32,471,169c15,15,41,4.5,41-17V24A24,24,0,0,0,488,0Z',
      yOffset: 0,
      xOffset: 0,
      transform: ''
    };
    exports.ExternalLinkAltIcon = createIcon_1.createIcon(exports.ExternalLinkAltIconConfig);
    exports["default"] = exports.ExternalLinkAltIcon;
    });

    var ExternalLinkAltIcon = unwrapExports(externalLinkAltIcon);
    var externalLinkAltIcon_1 = externalLinkAltIcon.ExternalLinkAltIconConfig;
    var externalLinkAltIcon_2 = externalLinkAltIcon.ExternalLinkAltIcon;

    const ApplicationLauncherItemContext = React.createContext({ isExternal: false, icon: null });

    const ApplicationLauncherContent = ({ children }) => (React.createElement(ApplicationLauncherItemContext.Consumer, null, ({ isExternal, icon }) => (React.createElement(React.Fragment, null,
        icon && React.createElement(ApplicationLauncherIcon, null, icon),
        icon ? React.createElement(ApplicationLauncherText, null, children) : children,
        isExternal && (React.createElement(React.Fragment, null,
            React.createElement("span", { className: css(styles$8.appLauncherMenuItemExternalIcon) },
                React.createElement(ExternalLinkAltIcon, null)),
            React.createElement("span", { className: css(a11yStyles.screenReader) }, "(opens new window)")))))));
    ApplicationLauncherContent.displayName = 'ApplicationLauncherContent';

    const ApplicationLauncherContext = React.createContext({
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        onFavorite: (itemId, isFavorite) => { }
    });

    var starIcon = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.StarIconConfig = {
      name: 'StarIcon',
      height: 512,
      width: 576,
      svgPath: 'M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z',
      yOffset: 0,
      xOffset: 0,
      transform: ''
    };
    exports.StarIcon = createIcon_1.createIcon(exports.StarIconConfig);
    exports["default"] = exports.StarIcon;
    });

    var StarIcon = unwrapExports(starIcon);
    var starIcon_1 = starIcon.StarIconConfig;
    var starIcon_2 = starIcon.StarIcon;

    const ApplicationLauncherItem = (_a) => {
        var { className = '', id, children, icon = null, isExternal = false, href, tooltip = null, tooltipProps = null, component = 'a', isFavorite = null, ariaIsFavoriteLabel = 'starred', ariaIsNotFavoriteLabel = 'not starred', customChild, enterTriggersArrowDown = false } = _a, props = __rest(_a, ["className", "id", "children", "icon", "isExternal", "href", "tooltip", "tooltipProps", "component", "isFavorite", "ariaIsFavoriteLabel", "ariaIsNotFavoriteLabel", "customChild", "enterTriggersArrowDown"]);
        return (React.createElement(ApplicationLauncherItemContext.Provider, { value: { isExternal, icon } },
            React.createElement(ApplicationLauncherContext.Consumer, null, ({ onFavorite }) => (React.createElement(DropdownItem, Object.assign({ id: id, component: component, href: href || null, className: css(isExternal && styles$8.modifiers.external, isFavorite !== null && styles$8.modifiers.link, className), listItemClassName: css(onFavorite && styles$8.appLauncherMenuWrapper, isFavorite && styles$8.modifiers.favorite), tooltip: tooltip, tooltipProps: tooltipProps }, (enterTriggersArrowDown === true && { enterTriggersArrowDown }), (customChild && { customChild }), (isFavorite !== null && {
                additionalChild: (React.createElement("button", { className: css(styles$8.appLauncherMenuItem, styles$8.modifiers.action), "aria-label": isFavorite ? ariaIsFavoriteLabel : ariaIsNotFavoriteLabel, onClick: () => {
                        onFavorite(id, isFavorite);
                    } },
                    React.createElement(StarIcon, null)))
            }), props), children && React.createElement(ApplicationLauncherContent, null, children))))));
    };
    ApplicationLauncherItem.displayName = 'ApplicationLauncherItem';

    class ApplicationLauncher extends React.Component {
        constructor() {
            super(...arguments);
            this.createSearchBox = () => {
                const { onSearch, searchPlaceholderText, searchProps } = this.props;
                return (React.createElement("div", { key: "search", className: css(styles$8.appLauncherMenuSearch) },
                    React.createElement(ApplicationLauncherItem, { customChild: React.createElement("input", Object.assign({ type: "search", className: css(formStyles.formControl), placeholder: searchPlaceholderText, onChange: e => onSearch(e.target.value) }, searchProps)) })));
            };
            this.createRenderableFavorites = () => {
                const { items, isGrouped, favorites } = this.props;
                if (isGrouped) {
                    const favoriteItems = [];
                    items.forEach(group => group.props.children
                        .filter(item => favorites.includes(item.props.id))
                        .map(item => favoriteItems.push(React.cloneElement(item, { isFavorite: true, enterTriggersArrowDown: true }))));
                    return favoriteItems;
                }
                return items
                    .filter(item => favorites.includes(item.props.id))
                    .map(item => React.cloneElement(item, { isFavorite: true, enterTriggersArrowDown: true }));
            };
            this.extendItemsWithFavorite = () => {
                const { items, isGrouped, favorites } = this.props;
                if (isGrouped) {
                    return items.map(group => React.cloneElement(group, {
                        children: React.Children.map(group.props.children, item => {
                            if (item.type === ApplicationLauncherSeparator) {
                                return item;
                            }
                            return React.cloneElement(item, {
                                isFavorite: favorites.some(favoriteId => favoriteId === item.props.id)
                            });
                        })
                    }));
                }
                return items.map(item => React.cloneElement(item, {
                    isFavorite: favorites.some(favoriteId => favoriteId === item.props.id)
                }));
            };
        }
        render() {
            const _a = this.props, { 'aria-label': ariaLabel, isOpen, onToggle, toggleIcon, toggleId, onSelect, isDisabled, className, isGrouped, favorites, onFavorite, onSearch, items, 
            /* eslint-disable @typescript-eslint/no-unused-vars */
            searchPlaceholderText, searchProps, ref, 
            /* eslint-enable @typescript-eslint/no-unused-vars */
            favoritesLabel, searchNoResultsText, menuAppendTo } = _a, props = __rest(_a, ['aria-label', "isOpen", "onToggle", "toggleIcon", "toggleId", "onSelect", "isDisabled", "className", "isGrouped", "favorites", "onFavorite", "onSearch", "items", "searchPlaceholderText", "searchProps", "ref", "favoritesLabel", "searchNoResultsText", "menuAppendTo"]);
            let renderableItems = [];
            if (onFavorite) {
                let favoritesGroup = [];
                let renderableFavorites = [];
                if (favorites.length > 0) {
                    renderableFavorites = this.createRenderableFavorites();
                    favoritesGroup = [
                        React.createElement(ApplicationLauncherGroup, { key: "favorites", label: favoritesLabel },
                            renderableFavorites,
                            React.createElement(ApplicationLauncherSeparator, { key: "separator" }))
                    ];
                }
                if (renderableFavorites.length > 0) {
                    renderableItems = favoritesGroup.concat(this.extendItemsWithFavorite());
                }
                else {
                    renderableItems = this.extendItemsWithFavorite();
                }
            }
            else {
                renderableItems = items;
            }
            if (items.length === 0) {
                renderableItems = [
                    React.createElement(ApplicationLauncherGroup, { key: "no-results-group" },
                        React.createElement(ApplicationLauncherItem, { key: "no-results" }, searchNoResultsText))
                ];
            }
            if (onSearch) {
                renderableItems = [this.createSearchBox(), ...renderableItems];
            }
            return (React.createElement(ApplicationLauncherContext.Provider, { value: { onFavorite } },
                React.createElement(DropdownContext.Provider, { value: {
                        onSelect,
                        menuClass: styles$8.appLauncherMenu,
                        itemClass: styles$8.appLauncherMenuItem,
                        toggleClass: styles$8.appLauncherToggle,
                        baseClass: styles$8.appLauncher,
                        baseComponent: 'nav',
                        sectionClass: styles$8.appLauncherGroup,
                        sectionTitleClass: styles$8.appLauncherGroupTitle,
                        sectionComponent: 'section',
                        disabledClass: styles$8.modifiers.disabled,
                        ouiaComponentType: ApplicationLauncher.displayName
                    } },
                    React.createElement(DropdownWithContext, Object.assign({}, props, { dropdownItems: renderableItems, isOpen: isOpen, className: className, "aria-label": ariaLabel, menuAppendTo: menuAppendTo, toggle: React.createElement(DropdownToggle, { id: toggleId, toggleIndicator: null, isOpen: isOpen, onToggle: onToggle, isDisabled: isDisabled, "aria-label": ariaLabel }, toggleIcon), isGrouped: isGrouped })))));
        }
    }
    ApplicationLauncher.displayName = 'ApplicationLauncher';
    ApplicationLauncher.defaultProps = {
        className: '',
        isDisabled: false,
        direction: exports.DropdownDirection.down,
        favorites: [],
        isOpen: false,
        position: exports.DropdownPosition.left,
        /* eslint-disable @typescript-eslint/no-unused-vars */
        onSelect: (_event) => undefined,
        onToggle: (_value) => undefined,
        /* eslint-enable @typescript-eslint/no-unused-vars */
        'aria-label': 'Application launcher',
        isGrouped: false,
        toggleIcon: React.createElement(ThIcon, null),
        searchPlaceholderText: 'Filter by name...',
        searchNoResultsText: 'No results found',
        favoritesLabel: 'Favorites',
        menuAppendTo: 'inline'
    };

    var avatar = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "avatar": "pf-c-avatar"
    };
    });

    var styles$c = unwrapExports(avatar);

    const Avatar = (_a) => {
        var { className = '', src = '', alt } = _a, props = __rest(_a, ["className", "src", "alt"]);
        return React.createElement("img", Object.assign({}, props, { src: src, alt: alt, className: css(styles$c.avatar, className) }));
    };
    Avatar.displayName = 'Avatar';

    var c_background_image_BackgroundImage = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.c_background_image_BackgroundImage = {
      "name": "--pf-c-background-image--BackgroundImage",
      "value": "url(\"../../assets/images/pfbg_576.jpg\")",
      "var": "var(--pf-c-background-image--BackgroundImage)"
    };
    exports["default"] = exports.c_background_image_BackgroundImage;
    });

    var cssVar = unwrapExports(c_background_image_BackgroundImage);
    var c_background_image_BackgroundImage_1 = c_background_image_BackgroundImage.c_background_image_BackgroundImage;

    var c_background_image_BackgroundImage_2x = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.c_background_image_BackgroundImage_2x = {
      "name": "--pf-c-background-image--BackgroundImage-2x",
      "value": "url(\"../../assets/images/pfbg_576@2x.jpg\")",
      "var": "var(--pf-c-background-image--BackgroundImage-2x)"
    };
    exports["default"] = exports.c_background_image_BackgroundImage_2x;
    });

    var cssVarName2x = unwrapExports(c_background_image_BackgroundImage_2x);
    var c_background_image_BackgroundImage_2x_1 = c_background_image_BackgroundImage_2x.c_background_image_BackgroundImage_2x;

    var c_background_image_BackgroundImage_sm = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.c_background_image_BackgroundImage_sm = {
      "name": "--pf-c-background-image--BackgroundImage--sm",
      "value": "url(\"../../assets/images/pfbg_768.jpg\")",
      "var": "var(--pf-c-background-image--BackgroundImage--sm)"
    };
    exports["default"] = exports.c_background_image_BackgroundImage_sm;
    });

    var cssVarNameSm = unwrapExports(c_background_image_BackgroundImage_sm);
    var c_background_image_BackgroundImage_sm_1 = c_background_image_BackgroundImage_sm.c_background_image_BackgroundImage_sm;

    var c_background_image_BackgroundImage_sm_2x = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.c_background_image_BackgroundImage_sm_2x = {
      "name": "--pf-c-background-image--BackgroundImage--sm-2x",
      "value": "url(\"../../assets/images/pfbg_768@2x.jpg\")",
      "var": "var(--pf-c-background-image--BackgroundImage--sm-2x)"
    };
    exports["default"] = exports.c_background_image_BackgroundImage_sm_2x;
    });

    var cssVarNameSm2x = unwrapExports(c_background_image_BackgroundImage_sm_2x);
    var c_background_image_BackgroundImage_sm_2x_1 = c_background_image_BackgroundImage_sm_2x.c_background_image_BackgroundImage_sm_2x;

    var c_background_image_BackgroundImage_lg = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.c_background_image_BackgroundImage_lg = {
      "name": "--pf-c-background-image--BackgroundImage--lg",
      "value": "url(\"../../assets/images/pfbg_2000.jpg\")",
      "var": "var(--pf-c-background-image--BackgroundImage--lg)"
    };
    exports["default"] = exports.c_background_image_BackgroundImage_lg;
    });

    var cssVarNameLg = unwrapExports(c_background_image_BackgroundImage_lg);
    var c_background_image_BackgroundImage_lg_1 = c_background_image_BackgroundImage_lg.c_background_image_BackgroundImage_lg;

    var c_background_image_Filter = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.c_background_image_Filter = {
      "name": "--pf-c-background-image--Filter",
      "value": "url(\"#image_overlay\")",
      "var": "var(--pf-c-background-image--Filter)"
    };
    exports["default"] = exports.c_background_image_Filter;
    });

    var cssVarNameFilter = unwrapExports(c_background_image_Filter);
    var c_background_image_Filter_1 = c_background_image_Filter.c_background_image_Filter;

    var backgroundImage = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "backgroundImage": "pf-c-background-image",
      "backgroundImageFilter": "pf-c-background-image__filter"
    };
    });

    var styles$d = unwrapExports(backgroundImage);

    const defaultFilter = (React.createElement("filter", null,
        React.createElement("feColorMatrix", { type: "matrix", values: "1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 0 0 0 1 0" }),
        React.createElement("feComponentTransfer", { colorInterpolationFilters: "sRGB", result: "duotone" },
            React.createElement("feFuncR", { type: "table", tableValues: "0.086274509803922 0.43921568627451" }),
            React.createElement("feFuncG", { type: "table", tableValues: "0.086274509803922 0.43921568627451" }),
            React.createElement("feFuncB", { type: "table", tableValues: "0.086274509803922 0.43921568627451" }),
            React.createElement("feFuncA", { type: "table", tableValues: "0 1" }))));
    let filterCounter = 0;
    const BackgroundImage = (_a) => {
        var { className, src, filter = defaultFilter } = _a, props = __rest(_a, ["className", "src", "filter"]);
        const getUrlValue = (size) => {
            if (typeof src === 'string') {
                return `url(${src})`;
            }
            else if (typeof src === 'object') {
                return `url(${src[size]})`;
            }
            return '';
        };
        const filterId = `patternfly-background-image-filter-overlay${filterCounter++}`;
        const style = {
            [cssVar.name]: getUrlValue('xs'),
            [cssVarName2x.name]: getUrlValue('xs2x'),
            [cssVarNameSm.name]: getUrlValue('sm'),
            [cssVarNameSm2x.name]: getUrlValue('sm2x'),
            [cssVarNameLg.name]: getUrlValue('lg'),
            [cssVarNameFilter.name]: `url(#${filterId})`
        };
        return (React.createElement("div", Object.assign({ className: css(styles$d.backgroundImage, className), style: style }, props),
            React.createElement("svg", { xmlns: "http://www.w3.org/2000/svg", className: "pf-c-background-image__filter", width: "0", height: "0" }, React.cloneElement(filter, { id: filterId }))));
    };
    BackgroundImage.displayName = 'BackgroundImage';

    var badge = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "badge": "pf-c-badge",
      "modifiers": {
        "read": "pf-m-read",
        "unread": "pf-m-unread"
      }
    };
    });

    var badgeStyles = unwrapExports(badge);

    const Badge = (_a) => {
        var { isRead = false, className = '', children = '' } = _a, props = __rest(_a, ["isRead", "className", "children"]);
        return (React.createElement("span", Object.assign({}, props, { className: css(badgeStyles.badge, (isRead ? badgeStyles.modifiers.read : badgeStyles.modifiers.unread), className) }), children));
    };
    Badge.displayName = 'Badge';

    var banner = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "banner": "pf-c-banner",
      "button": "pf-c-button",
      "card": "pf-c-card",
      "modifiers": {
        "info": "pf-m-info",
        "warning": "pf-m-warning",
        "danger": "pf-m-danger",
        "success": "pf-m-success",
        "sticky": "pf-m-sticky"
      }
    };
    });

    var styles$e = unwrapExports(banner);

    const Banner = ({ children, className, variant = 'default', isSticky = false }) => (React.createElement("div", { className: css(styles$e.banner, styles$e.modifiers[variant], isSticky && styles$e.modifiers.sticky, className) }, children));
    Banner.displayName = 'Banner';

    const Brand = (_a) => {
        var { className = '', src = '', alt } = _a, props = __rest(_a, ["className", "src", "alt"]);
        return (
        /** the brand component currently contains no styling the 'pf-c-brand' string will be used for the className */
        React.createElement("img", Object.assign({}, props, { className: css('pf-c-brand', className), src: src, alt: alt })));
    };
    Brand.displayName = 'Brand';

    var breadcrumb = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "breadcrumb": "pf-c-breadcrumb",
      "breadcrumbHeading": "pf-c-breadcrumb__heading",
      "breadcrumbItem": "pf-c-breadcrumb__item",
      "breadcrumbItemDivider": "pf-c-breadcrumb__item-divider",
      "breadcrumbLink": "pf-c-breadcrumb__link",
      "breadcrumbList": "pf-c-breadcrumb__list",
      "modifiers": {
        "current": "pf-m-current",
        "overpassFont": "pf-m-overpass-font"
      }
    };
    });

    var styles$f = unwrapExports(breadcrumb);

    const Breadcrumb = (_a) => {
        var { children = null, className = '', 'aria-label': ariaLabel = 'Breadcrumb', ouiaId, ouiaSafe = true } = _a, props = __rest(_a, ["children", "className", 'aria-label', "ouiaId", "ouiaSafe"]);
        return (React.createElement("nav", Object.assign({}, props, { "aria-label": ariaLabel, className: css(styles$f.breadcrumb, className) }, getOUIAProps(Breadcrumb.displayName, ouiaId, ouiaSafe)),
            React.createElement("ol", { className: styles$f.breadcrumbList }, React.Children.map(children, (child, index) => {
                const showDivider = index > 0;
                if (React.isValidElement(child)) {
                    return React.cloneElement(child, { showDivider });
                }
                return child;
            }))));
    };
    Breadcrumb.displayName = 'Breadcrumb';

    const BreadcrumbItem = (_a) => {
        var { children = null, className = '', to = null, isActive = false, showDivider, target = null, component = 'a' } = _a, props = __rest(_a, ["children", "className", "to", "isActive", "showDivider", "target", "component"]);
        const Component = component;
        return (React.createElement("li", Object.assign({}, props, { className: css(styles$f.breadcrumbItem, className) }),
            showDivider && (React.createElement("span", { className: styles$f.breadcrumbItemDivider },
                React.createElement(AngleRightIcon, null))),
            to && (React.createElement(Component, { href: to, target: target, className: css(styles$f.breadcrumbLink, isActive && styles$f.modifiers.current), "aria-current": isActive ? 'page' : undefined }, children)),
            !to && React.createElement(React.Fragment, null, children)));
    };
    BreadcrumbItem.displayName = 'BreadcrumbItem';

    const BreadcrumbHeading = (_a) => {
        var { children = null, className = '', to = null, target = null, component = 'a', showDivider } = _a, props = __rest(_a, ["children", "className", "to", "target", "component", "showDivider"]);
        const Component = component;
        return (React.createElement("li", Object.assign({}, props, { className: css(styles$f.breadcrumbItem, className) }),
            React.createElement("h1", { className: styles$f.breadcrumbHeading },
                showDivider && (React.createElement("span", { className: styles$f.breadcrumbItemDivider },
                    React.createElement(AngleRightIcon, null))),
                to && (React.createElement(Component, { href: to, target: target, className: css(styles$f.breadcrumbLink, styles$f.modifiers.current), "aria-current": "page" }, children)),
                !to && React.createElement(React.Fragment, null, children))));
    };
    BreadcrumbHeading.displayName = 'BreadcrumbHeading';

    var card = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "card": "pf-c-card",
      "cardActions": "pf-c-card__actions",
      "cardBody": "pf-c-card__body",
      "cardFooter": "pf-c-card__footer",
      "cardHeader": "pf-c-card__header",
      "cardTitle": "pf-c-card__title",
      "modifiers": {
        "hoverable": "pf-m-hoverable",
        "selectable": "pf-m-selectable",
        "selected": "pf-m-selected",
        "compact": "pf-m-compact",
        "flat": "pf-m-flat",
        "noFill": "pf-m-no-fill",
        "overpassFont": "pf-m-overpass-font"
      }
    };
    });

    var styles$g = unwrapExports(card);

    const Card = (_a) => {
        var { children = null, className = '', component = 'article', isHoverable = false, isCompact = false, isSelectable = false, isSelected = false, isFlat = false, ouiaId, ouiaSafe = true } = _a, props = __rest(_a, ["children", "className", "component", "isHoverable", "isCompact", "isSelectable", "isSelected", "isFlat", "ouiaId", "ouiaSafe"]);
        const Component = component;
        return (React.createElement(Component, Object.assign({ className: css(styles$g.card, isHoverable && styles$g.modifiers.hoverable, isCompact && styles$g.modifiers.compact, isSelectable && styles$g.modifiers.selectable, isSelected && isSelectable && styles$g.modifiers.selected, isFlat && styles$g.modifiers.flat, className), tabIndex: isSelectable ? '0' : undefined }, props, getOUIAProps(Card.displayName, ouiaId, ouiaSafe)), children));
    };
    Card.displayName = 'Card';

    const CardActions = (_a) => {
        var { children = null, className = '' } = _a, props = __rest(_a, ["children", "className"]);
        return (React.createElement("div", Object.assign({ className: css(styles$g.cardActions, className) }, props), children));
    };
    CardActions.displayName = 'CardActions';

    const CardBody = (_a) => {
        var { children = null, className = '', component = 'div', isFilled = true } = _a, props = __rest(_a, ["children", "className", "component", "isFilled"]);
        const Component = component;
        return (React.createElement(Component, Object.assign({ className: css(styles$g.cardBody, !isFilled && styles$g.modifiers.noFill, className) }, props), children));
    };
    CardBody.displayName = 'CardBody';

    const CardFooter = (_a) => {
        var { children = null, className = '', component = 'div' } = _a, props = __rest(_a, ["children", "className", "component"]);
        const Component = component;
        return (React.createElement(Component, Object.assign({ className: css(styles$g.cardFooter, className) }, props), children));
    };
    CardFooter.displayName = 'CardFooter';

    const CardTitle = (_a) => {
        var { children = null, className = '' } = _a, props = __rest(_a, ["children", "className"]);
        return (React.createElement("div", Object.assign({ className: css(styles$g.cardTitle, className) }, props), children));
    };
    CardTitle.displayName = 'CardTitle';

    const CardHeader = (_a) => {
        var { children = null, className = '' } = _a, props = __rest(_a, ["children", "className"]);
        return (React.createElement("div", Object.assign({ className: css(styles$g.cardHeader, className) }, props), children));
    };
    CardHeader.displayName = 'CardHeader';

    const CardHeaderMain = (_a) => {
        var { children = null, className = '' } = _a, props = __rest(_a, ["children", "className"]);
        return (React.createElement("div", Object.assign({ className: className }, props), children));
    };
    CardHeaderMain.displayName = 'CardHeaderMain';

    var check = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "check": "pf-c-check",
      "checkDescription": "pf-c-check__description",
      "checkInput": "pf-c-check__input",
      "checkLabel": "pf-c-check__label",
      "modifiers": {
        "disabled": "pf-m-disabled"
      }
    };
    });

    var checkStyles = unwrapExports(check);

    // tslint:disable-next-line:no-empty
    const defaultOnChange = () => { };
    class Checkbox extends React.Component {
        constructor(props) {
            super(props);
            this.handleChange = (event) => {
                this.props.onChange(event.currentTarget.checked, event);
            };
        }
        render() {
            const _a = this.props, { 'aria-label': ariaLabel, className, onChange, isValid, isDisabled, isChecked, label, checked, defaultChecked, description } = _a, props = __rest(_a, ['aria-label', "className", "onChange", "isValid", "isDisabled", "isChecked", "label", "checked", "defaultChecked", "description"]);
            const checkedProps = {};
            if ([true, false].includes(checked) || isChecked === true) {
                checkedProps.checked = checked || isChecked;
            }
            if (onChange !== defaultOnChange) {
                checkedProps.checked = isChecked;
            }
            if ([false, true].includes(defaultChecked)) {
                checkedProps.defaultChecked = defaultChecked;
            }
            checkedProps.checked = checkedProps.checked === null ? false : checkedProps.checked;
            return (React.createElement("div", { className: css(checkStyles.check, className) },
                React.createElement("input", Object.assign({}, props, { className: css(checkStyles.checkInput), type: "checkbox", onChange: this.handleChange, "aria-invalid": !isValid, "aria-label": ariaLabel, disabled: isDisabled, ref: elem => elem && (elem.indeterminate = isChecked === null) }, checkedProps)),
                label && (React.createElement("label", { className: css(checkStyles.checkLabel, isDisabled && checkStyles.modifiers.disabled), htmlFor: props.id }, label)),
                description && React.createElement("div", { className: css(checkStyles.checkDescription) }, description)));
        }
    }
    Checkbox.displayName = 'Checkbox';
    Checkbox.defaultProps = {
        className: '',
        isValid: true,
        isDisabled: false,
        isChecked: false,
        onChange: defaultOnChange
    };

    var chipGroup = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "chipGroup": "pf-c-chip-group",
      "chipGroupClose": "pf-c-chip-group__close",
      "chipGroupLabel": "pf-c-chip-group__label",
      "chipGroupList": "pf-c-chip-group__list",
      "chipGroupListItem": "pf-c-chip-group__list-item",
      "modifiers": {
        "category": "pf-m-category"
      }
    };
    });

    var styles$h = unwrapExports(chipGroup);

    var chip = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "badge": "pf-c-badge",
      "button": "pf-c-button",
      "chip": "pf-c-chip",
      "chipText": "pf-c-chip__text",
      "modifiers": {
        "overflow": "pf-m-overflow"
      }
    };
    });

    var styles$i = unwrapExports(chip);

    class Chip extends React.Component {
        constructor(props) {
            super(props);
            this.span = React.createRef();
            this.renderOverflowChip = () => {
                const { children, className, onClick, ouiaId } = this.props;
                const Component = this.props.component;
                return (React.createElement(Component, Object.assign({ onClick: onClick, className: css(styles$i.chip, styles$i.modifiers.overflow, className) }, getOUIAProps('OverflowChip', ouiaId)),
                    React.createElement("span", { className: css(styles$i.chipText) }, children)));
            };
            this.renderChip = (randomId) => {
                const { children, tooltipPosition } = this.props;
                if (this.state.isTooltipVisible) {
                    return (React.createElement(Tooltip, { position: tooltipPosition, content: children }, this.renderInnerChip(randomId)));
                }
                return this.renderInnerChip(randomId);
            };
            this.state = {
                isTooltipVisible: false
            };
        }
        componentDidMount() {
            this.setState({
                isTooltipVisible: Boolean(this.span.current && this.span.current.offsetWidth < this.span.current.scrollWidth)
            });
        }
        renderInnerChip(id) {
            const { children, className, onClick, closeBtnAriaLabel, isReadOnly, component, ouiaId } = this.props;
            const Component = component;
            return (React.createElement(Component, Object.assign({ className: css(styles$i.chip, className) }, (this.state.isTooltipVisible && { tabIndex: 0 }), getOUIAProps(Chip.displayName, ouiaId)),
                React.createElement("span", { ref: this.span, className: css(styles$i.chipText), id: id }, children),
                !isReadOnly && (React.createElement(Button, { onClick: onClick, variant: "plain", "aria-label": closeBtnAriaLabel, id: `remove_${id}`, "aria-labelledby": `remove_${id} ${id}` },
                    React.createElement(TimesIcon, { "aria-hidden": "true" })))));
        }
        render() {
            const { isOverflowChip } = this.props;
            return (React.createElement(GenerateId, null, randomId => (isOverflowChip ? this.renderOverflowChip() : this.renderChip(this.props.id || randomId))));
        }
    }
    Chip.displayName = 'Chip';
    Chip.defaultProps = {
        closeBtnAriaLabel: 'close',
        className: '',
        isOverflowChip: false,
        isReadOnly: false,
        tooltipPosition: 'top',
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        onClick: (_e) => undefined,
        component: 'div'
    };

    class ChipGroup extends React.Component {
        constructor(props) {
            super(props);
            this.headingRef = React.createRef();
            this.toggleCollapse = () => {
                this.setState(prevState => ({
                    isOpen: !prevState.isOpen,
                    isTooltipVisible: Boolean(this.headingRef.current && this.headingRef.current.offsetWidth < this.headingRef.current.scrollWidth)
                }));
            };
            this.state = {
                isOpen: this.props.defaultIsOpen,
                isTooltipVisible: false
            };
        }
        componentDidMount() {
            this.setState({
                isTooltipVisible: Boolean(this.headingRef.current && this.headingRef.current.offsetWidth < this.headingRef.current.scrollWidth)
            });
        }
        renderLabel(id) {
            const { categoryName, tooltipPosition } = this.props;
            const { isTooltipVisible } = this.state;
            return isTooltipVisible ? (React.createElement(Tooltip, { position: tooltipPosition, content: categoryName },
                React.createElement("span", { tabIndex: 0, ref: this.headingRef, className: css(styles$h.chipGroupLabel), "aria-hidden": "true", id: id }, categoryName))) : (React.createElement("span", { ref: this.headingRef, className: css(styles$h.chipGroupLabel), "aria-hidden": "true", id: id }, categoryName));
        }
        render() {
            const _a = this.props, { categoryName, children, className, isClosable, closeBtnAriaLabel, 'aria-label': ariaLabel, onClick, numChips, expandedText, collapsedText, 
            /* eslint-disable @typescript-eslint/no-unused-vars */
            defaultIsOpen, tooltipPosition } = _a, 
            /* eslint-enable @typescript-eslint/no-unused-vars */
            rest = __rest(_a, ["categoryName", "children", "className", "isClosable", "closeBtnAriaLabel", 'aria-label', "onClick", "numChips", "expandedText", "collapsedText", "defaultIsOpen", "tooltipPosition"]);
            const { isOpen } = this.state;
            const numChildren = React.Children.count(children);
            const collapsedTextResult = fillTemplate(collapsedText, {
                remaining: React.Children.count(children) - numChips
            });
            const renderChipGroup = (id) => {
                const chipArray = !isOpen
                    ? React.Children.toArray(children).slice(0, numChips)
                    : React.Children.toArray(children);
                return (React.createElement("div", { className: css(styles$h.chipGroup, className, categoryName && styles$h.modifiers.category) },
                    categoryName && this.renderLabel(id),
                    React.createElement("ul", Object.assign({ className: css(styles$h.chipGroupList) }, (categoryName && { 'aria-labelledby': id }), (!categoryName && { 'aria-label': ariaLabel }), { role: "list" }, rest),
                        chipArray.map((child, i) => (React.createElement("li", { className: css(styles$h.chipGroupListItem), key: i }, child))),
                        numChildren > numChips && (React.createElement("li", { className: css(styles$h.chipGroupListItem) },
                            React.createElement(Chip, { isOverflowChip: true, onClick: this.toggleCollapse, component: "button" }, isOpen ? expandedText : collapsedTextResult)))),
                    isClosable && (React.createElement("div", { className: css(styles$h.chipGroupClose) },
                        React.createElement(Button, { variant: "plain", "aria-label": closeBtnAriaLabel, onClick: onClick, id: `remove_group_${id}`, "aria-labelledby": `remove_group_${id} ${id}` },
                            React.createElement(TimesIcon, { "aria-hidden": "true" }))))));
            };
            return numChildren === 0 ? null : React.createElement(GenerateId, null, randomId => renderChipGroup(this.props.id || randomId));
        }
    }
    ChipGroup.displayName = 'ChipGroup';
    ChipGroup.defaultProps = {
        expandedText: 'Show Less',
        collapsedText: '${remaining} more',
        categoryName: '',
        defaultIsOpen: false,
        numChips: 3,
        isClosable: false,
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        onClick: (_e) => undefined,
        closeBtnAriaLabel: 'Close chip group',
        tooltipPosition: 'top',
        'aria-label': 'Chip group category'
    };

    var clipboardCopy = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "clipboardCopy": "pf-c-clipboard-copy",
      "clipboardCopyExpandableContent": "pf-c-clipboard-copy__expandable-content",
      "clipboardCopyGroup": "pf-c-clipboard-copy__group",
      "clipboardCopyToggleIcon": "pf-c-clipboard-copy__toggle-icon",
      "modifiers": {
        "expanded": "pf-m-expanded"
      }
    };
    });

    var styles$j = unwrapExports(clipboardCopy);

    (function (TextInputTypes) {
        TextInputTypes["text"] = "text";
        TextInputTypes["date"] = "date";
        TextInputTypes["datetimeLocal"] = "datetime-local";
        TextInputTypes["email"] = "email";
        TextInputTypes["month"] = "month";
        TextInputTypes["number"] = "number";
        TextInputTypes["password"] = "password";
        TextInputTypes["search"] = "search";
        TextInputTypes["tel"] = "tel";
        TextInputTypes["time"] = "time";
        TextInputTypes["url"] = "url";
    })(exports.TextInputTypes || (exports.TextInputTypes = {}));
    class TextInputBase extends React.Component {
        constructor(props) {
            super(props);
            this.handleChange = (event) => {
                if (this.props.onChange) {
                    this.props.onChange(event.currentTarget.value, event);
                }
            };
            if (!props.id && !props['aria-label'] && !props['aria-labelledby']) {
                // eslint-disable-next-line no-console
                console.error('Text input:', 'Text input requires either an id or aria-label to be specified');
            }
        }
        render() {
            const _a = this.props, { innerRef, className, type, value, 
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            onChange, validated, isReadOnly, isRequired, isDisabled } = _a, props = __rest(_a, ["innerRef", "className", "type", "value", "onChange", "validated", "isReadOnly", "isRequired", "isDisabled"]);
            return (React.createElement("input", Object.assign({}, props, { className: css(formStyles.formControl, validated === exports.ValidatedOptions.success && formStyles.modifiers.success, validated === exports.ValidatedOptions.warning && formStyles.modifiers.warning, className), onChange: this.handleChange, type: type, value: value, "aria-invalid": validated === exports.ValidatedOptions.error, required: isRequired, disabled: isDisabled, readOnly: isReadOnly, ref: innerRef })));
        }
    }
    TextInputBase.displayName = 'TextInputBase';
    TextInputBase.defaultProps = {
        'aria-label': null,
        className: '',
        isRequired: false,
        validated: 'default',
        isDisabled: false,
        isReadOnly: false,
        type: exports.TextInputTypes.text,
        onChange: () => undefined
    };
    const TextInput = React.forwardRef((props, ref) => (React.createElement(TextInputBase, Object.assign({}, props, { innerRef: ref }))));

    var copyIcon = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.CopyIconConfig = {
      name: 'CopyIcon',
      height: 512,
      width: 448,
      svgPath: 'M320 448v40c0 13.255-10.745 24-24 24H24c-13.255 0-24-10.745-24-24V120c0-13.255 10.745-24 24-24h72v296c0 30.879 25.121 56 56 56h168zm0-344V0H152c-13.255 0-24 10.745-24 24v368c0 13.255 10.745 24 24 24h272c13.255 0 24-10.745 24-24V128H344c-13.2 0-24-10.8-24-24zm120.971-31.029L375.029 7.029A24 24 0 0 0 358.059 0H352v96h96v-6.059a24 24 0 0 0-7.029-16.97z',
      yOffset: 0,
      xOffset: 0,
      transform: ''
    };
    exports.CopyIcon = createIcon_1.createIcon(exports.CopyIconConfig);
    exports["default"] = exports.CopyIcon;
    });

    var CopyIcon = unwrapExports(copyIcon);
    var copyIcon_1 = copyIcon.CopyIconConfig;
    var copyIcon_2 = copyIcon.CopyIcon;

    const ClipboardCopyButton = (_a) => {
        var { onClick, exitDelay = 100, entryDelay = 100, maxWidth = '100px', position = 'top', 'aria-label': ariaLabel = 'Copyable input', id, textId, children } = _a, props = __rest(_a, ["onClick", "exitDelay", "entryDelay", "maxWidth", "position", 'aria-label', "id", "textId", "children"]);
        return (React.createElement(Tooltip, { trigger: "mouseenter focus click", exitDelay: exitDelay, entryDelay: entryDelay, maxWidth: maxWidth, position: position, content: React.createElement("div", null, children) },
            React.createElement(Button, Object.assign({ type: "button", variant: "control", onClick: onClick, "aria-label": ariaLabel, id: id, "aria-labelledby": `${id} ${textId}` }, props),
                React.createElement(CopyIcon, null))));
    };
    ClipboardCopyButton.displayName = 'ClipboardCopyButton';

    const ClipboardCopyToggle = (_a) => {
        var { onClick, id, textId, contentId, isExpanded = false } = _a, props = __rest(_a, ["onClick", "id", "textId", "contentId", "isExpanded"]);
        return (React.createElement(Button, Object.assign({ type: "button", variant: "control", onClick: onClick, id: id, "aria-labelledby": `${id} ${textId}`, "aria-controls": `${id} ${contentId}`, "aria-expanded": isExpanded }, props),
            React.createElement(AngleRightIcon, { "aria-hidden": "true" })));
    };
    ClipboardCopyToggle.displayName = 'ClipboardCopyToggle';

    class ClipboardCopyExpanded extends React.Component {
        constructor(props) {
            super(props);
        }
        render() {
            const _a = this.props, { className, children, onChange, isReadOnly, isCode } = _a, props = __rest(_a, ["className", "children", "onChange", "isReadOnly", "isCode"]);
            return (React.createElement("div", Object.assign({ suppressContentEditableWarning: true, className: css(styles$j.clipboardCopyExpandableContent, className), onInput: (e) => onChange(e.target.innerText, e), contentEditable: !isReadOnly }, props), isCode ? React.createElement("pre", null, children) : children));
        }
    }
    ClipboardCopyExpanded.displayName = 'ClipboardCopyExpanded';
    ClipboardCopyExpanded.defaultProps = {
        onChange: () => undefined,
        className: '',
        isReadOnly: false,
        isCode: false
    };

    const clipboardCopyFunc = (event, text) => {
        const clipboard = event.currentTarget.parentElement;
        const el = document.createElement('input');
        el.value = text.toString();
        clipboard.appendChild(el);
        el.select();
        document.execCommand('copy');
        clipboard.removeChild(el);
    };
    (function (ClipboardCopyVariant) {
        ClipboardCopyVariant["inline"] = "inline";
        ClipboardCopyVariant["expansion"] = "expansion";
    })(exports.ClipboardCopyVariant || (exports.ClipboardCopyVariant = {}));
    class ClipboardCopy extends React.Component {
        constructor(props) {
            super(props);
            this.timer = null;
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            this.componentDidUpdate = (prevProps, prevState) => {
                if (prevProps.children !== this.props.children) {
                    this.updateText(this.props.children);
                }
            };
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            this.expandContent = (_event) => {
                this.setState(prevState => ({
                    expanded: !prevState.expanded
                }));
            };
            this.updateText = (text) => {
                this.setState({ text });
                this.props.onChange(text);
            };
            this.render = () => {
                const _a = this.props, { 
                /* eslint-disable @typescript-eslint/no-unused-vars */
                isExpanded, onChange, // Don't pass to <div>
                /* eslint-enable @typescript-eslint/no-unused-vars */
                isReadOnly, isCode, exitDelay, maxWidth, entryDelay, switchDelay, onCopy, hoverTip, clickTip, textAriaLabel, toggleAriaLabel, variant, position, className } = _a, divProps = __rest(_a, ["isExpanded", "onChange", "isReadOnly", "isCode", "exitDelay", "maxWidth", "entryDelay", "switchDelay", "onCopy", "hoverTip", "clickTip", "textAriaLabel", "toggleAriaLabel", "variant", "position", "className"]);
                const textIdPrefix = 'text-input-';
                const toggleIdPrefix = 'toggle-';
                const contentIdPrefix = 'content-';
                return (React.createElement("div", Object.assign({ className: css(styles$j.clipboardCopy, this.state.expanded && styles$j.modifiers.expanded, className) }, divProps),
                    React.createElement(GenerateId, { prefix: "" }, id => (React.createElement(React.Fragment, null,
                        React.createElement("div", { className: css(styles$j.clipboardCopyGroup) },
                            variant === 'expansion' && (React.createElement(ClipboardCopyToggle, { isExpanded: this.state.expanded, onClick: this.expandContent, id: `${toggleIdPrefix}-${id}`, textId: `${textIdPrefix}-${id}`, contentId: `${contentIdPrefix}-${id}`, "aria-label": toggleAriaLabel })),
                            React.createElement(TextInput, { isReadOnly: isReadOnly || this.state.expanded, onChange: this.updateText, value: this.state.text, id: `text-input-${id}`, "aria-label": textAriaLabel }),
                            React.createElement(ClipboardCopyButton, { exitDelay: exitDelay, entryDelay: entryDelay, maxWidth: maxWidth, position: position, id: `copy-button-${id}`, textId: `text-input-${id}`, "aria-label": hoverTip, onClick: (event) => {
                                    if (this.timer) {
                                        window.clearTimeout(this.timer);
                                        this.setState({ copied: false });
                                    }
                                    onCopy(event, this.state.text);
                                    this.setState({ copied: true }, () => {
                                        this.timer = window.setTimeout(() => {
                                            this.setState({ copied: false });
                                            this.timer = null;
                                        }, switchDelay);
                                    });
                                } }, this.state.copied ? clickTip : hoverTip)),
                        this.state.expanded && (React.createElement(ClipboardCopyExpanded, { isReadOnly: isReadOnly, isCode: isCode, id: `content-${id}`, onChange: this.updateText }, this.state.text)))))));
            };
            this.state = {
                text: this.props.children,
                expanded: this.props.isExpanded,
                copied: false
            };
        }
    }
    ClipboardCopy.displayName = 'ClipboardCopy';
    ClipboardCopy.defaultProps = {
        hoverTip: 'Copy to clipboard',
        clickTip: 'Successfully copied to clipboard!',
        isReadOnly: false,
        isExpanded: false,
        isCode: false,
        variant: 'inline',
        position: exports.TooltipPosition.top,
        maxWidth: '150px',
        exitDelay: 1600,
        entryDelay: 100,
        switchDelay: 2000,
        onCopy: clipboardCopyFunc,
        onChange: () => undefined,
        textAriaLabel: 'Copyable input',
        toggleAriaLabel: 'Show content'
    };

    var contextSelector = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "contextSelector": "pf-c-context-selector",
      "contextSelectorMenu": "pf-c-context-selector__menu",
      "contextSelectorMenuList": "pf-c-context-selector__menu-list",
      "contextSelectorMenuListItem": "pf-c-context-selector__menu-list-item",
      "contextSelectorMenuSearch": "pf-c-context-selector__menu-search",
      "contextSelectorToggle": "pf-c-context-selector__toggle",
      "contextSelectorToggleIcon": "pf-c-context-selector__toggle-icon",
      "contextSelectorToggleText": "pf-c-context-selector__toggle-text",
      "modifiers": {
        "active": "pf-m-active",
        "expanded": "pf-m-expanded"
      }
    };
    });

    var styles$k = unwrapExports(contextSelector);

    var searchIcon = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.SearchIconConfig = {
      name: 'SearchIcon',
      height: 512,
      width: 512,
      svgPath: 'M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z',
      yOffset: 0,
      xOffset: 0,
      transform: ''
    };
    exports.SearchIcon = createIcon_1.createIcon(exports.SearchIconConfig);
    exports["default"] = exports.SearchIcon;
    });

    var SearchIcon = unwrapExports(searchIcon);
    var searchIcon_1 = searchIcon.SearchIconConfig;
    var searchIcon_2 = searchIcon.SearchIcon;

    class ContextSelectorToggle extends React.Component {
        constructor() {
            super(...arguments);
            this.toggle = React.createRef();
            this.componentDidMount = () => {
                document.addEventListener('mousedown', this.onDocClick);
                document.addEventListener('touchstart', this.onDocClick);
                document.addEventListener('keydown', this.onEscPress);
            };
            this.componentWillUnmount = () => {
                document.removeEventListener('mousedown', this.onDocClick);
                document.removeEventListener('touchstart', this.onDocClick);
                document.removeEventListener('keydown', this.onEscPress);
            };
            this.onDocClick = (event) => {
                const { isOpen, parentRef, onToggle } = this.props;
                if (isOpen && parentRef && !parentRef.contains(event.target)) {
                    onToggle(null, false);
                    this.toggle.current.focus();
                }
            };
            this.onEscPress = (event) => {
                const { isOpen, parentRef, onToggle } = this.props;
                const keyCode = event.keyCode || event.which;
                if (isOpen && keyCode === KEY_CODES.ESCAPE_KEY && parentRef && parentRef.contains(event.target)) {
                    onToggle(null, false);
                    this.toggle.current.focus();
                }
            };
            this.onKeyDown = (event) => {
                const { isOpen, onToggle, onEnter } = this.props;
                if ((event.keyCode === KEY_CODES.TAB && !isOpen) || event.key !== KEY_CODES.ENTER) {
                    return;
                }
                event.preventDefault();
                if ((event.keyCode === KEY_CODES.TAB || event.keyCode === KEY_CODES.ENTER || event.key !== KEY_CODES.SPACE) &&
                    isOpen) {
                    onToggle(null, !isOpen);
                }
                else if ((event.keyCode === KEY_CODES.ENTER || event.key === ' ') && !isOpen) {
                    onToggle(null, !isOpen);
                    onEnter();
                }
            };
        }
        render() {
            const _a = this.props, { className, toggleText, isOpen, onToggle, id, 
            /* eslint-disable @typescript-eslint/no-unused-vars */
            isActive, onEnter, parentRef } = _a, 
            /* eslint-enable @typescript-eslint/no-unused-vars */
            props = __rest(_a, ["className", "toggleText", "isOpen", "onToggle", "id", "isActive", "onEnter", "parentRef"]);
            return (React.createElement("button", Object.assign({}, props, { id: id, ref: this.toggle, className: css(styles$k.contextSelectorToggle, isActive && styles$k.modifiers.active, className), type: "button", onClick: event => onToggle(event, !isOpen), "aria-expanded": isOpen, onKeyDown: this.onKeyDown }),
                React.createElement("span", { className: css(styles$k.contextSelectorToggleText) }, toggleText),
                React.createElement("span", { className: css(styles$k.contextSelectorToggleIcon) },
                    React.createElement(CaretDownIcon, { "aria-hidden": true }))));
        }
    }
    ContextSelectorToggle.displayName = 'ContextSelectorToggle';
    ContextSelectorToggle.defaultProps = {
        className: '',
        toggleText: '',
        isOpen: false,
        onEnter: () => undefined,
        parentRef: null,
        isActive: false,
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        onToggle: (event, value) => undefined
    };

    class ContextSelectorMenuList extends React.Component {
        constructor() {
            super(...arguments);
            this.refsCollection = [];
            this.sendRef = (index, ref) => {
                this.refsCollection[index] = ref;
            };
            this.render = () => {
                // eslint-disable-next-line @typescript-eslint/no-unused-vars
                const _a = this.props, { className, isOpen, children } = _a, props = __rest(_a, ["className", "isOpen", "children"]);
                return (React.createElement("ul", Object.assign({ className: css(styles$k.contextSelectorMenuList, className), hidden: !isOpen, role: "menu" }, props), this.extendChildren()));
            };
        }
        extendChildren() {
            return React.Children.map(this.props.children, (child, index) => React.cloneElement(child, {
                sendRef: this.sendRef,
                index
            }));
        }
    }
    ContextSelectorMenuList.displayName = 'ContextSelectorMenuList';
    ContextSelectorMenuList.defaultProps = {
        children: null,
        className: '',
        isOpen: true
    };

    const ContextSelectorContext = React.createContext({
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        onSelect: (event, value) => undefined
    });

    var inputGroup = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "formControl": "pf-c-form-control",
      "inputGroup": "pf-c-input-group",
      "inputGroupText": "pf-c-input-group__text"
    };
    });

    var styles$l = unwrapExports(inputGroup);

    class FormSelect extends React.Component {
        constructor(props) {
            super(props);
            this.handleChange = (event) => {
                this.props.onChange(event.currentTarget.value, event);
            };
            if (!props.id && !props['aria-label']) {
                // eslint-disable-next-line no-console
                console.error('FormSelect requires either an id or aria-label to be specified');
            }
        }
        render() {
            const _a = this.props, { children, className, value, validated, isDisabled, isRequired, ouiaId, ouiaSafe } = _a, props = __rest(_a, ["children", "className", "value", "validated", "isDisabled", "isRequired", "ouiaId", "ouiaSafe"]);
            return (React.createElement("select", Object.assign({}, props, { className: css(formStyles.formControl, className, validated === exports.ValidatedOptions.success && formStyles.modifiers.success, validated === exports.ValidatedOptions.warning && formStyles.modifiers.warning), "aria-invalid": validated === exports.ValidatedOptions.error }, getOUIAProps(FormSelect.displayName, ouiaId, ouiaSafe), { onChange: this.handleChange, disabled: isDisabled, required: isRequired, value: value }), children));
        }
    }
    FormSelect.displayName = 'FormSelect';
    FormSelect.defaultProps = {
        className: '',
        value: '',
        validated: 'default',
        isDisabled: false,
        isRequired: false,
        onBlur: () => undefined,
        onFocus: () => undefined,
        onChange: () => undefined,
        ouiaSafe: true
    };

    const FormSelectOption = (_a) => {
        var { className = '', value = '', isDisabled = false, label } = _a, props = __rest(_a, ["className", "value", "isDisabled", "label"]);
        return (React.createElement("option", Object.assign({}, props, { className: className, value: value, disabled: isDisabled }), label));
    };
    FormSelectOption.displayName = 'FormSelectOption';

    const FormSelectOptionGroup = (_a) => {
        var { children = null, className = '', isDisabled = false, label } = _a, props = __rest(_a, ["children", "className", "isDisabled", "label"]);
        return (React.createElement("optgroup", Object.assign({}, props, { disabled: !!isDisabled, className: className, label: label }), children));
    };
    FormSelectOptionGroup.displayName = 'FormSelectOptionGroup';

    (function (TextAreResizeOrientation) {
        TextAreResizeOrientation["horizontal"] = "horizontal";
        TextAreResizeOrientation["vertical"] = "vertical";
        TextAreResizeOrientation["both"] = "both";
    })(exports.TextAreResizeOrientation || (exports.TextAreResizeOrientation = {}));
    class TextArea extends React.Component {
        constructor(props) {
            super(props);
            this.handleChange = (event) => {
                if (this.props.onChange) {
                    this.props.onChange(event.currentTarget.value, event);
                }
            };
            if (!props.id && !props['aria-label']) {
                // eslint-disable-next-line no-console
                console.error('TextArea: TextArea requires either an id or aria-label to be specified');
            }
        }
        render() {
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            const _a = this.props, { className, value, onChange, validated, isRequired, resizeOrientation } = _a, props = __rest(_a, ["className", "value", "onChange", "validated", "isRequired", "resizeOrientation"]);
            const orientation = `resize${capitalize(resizeOrientation)}`;
            return (React.createElement("textarea", Object.assign({ className: css(formStyles.formControl, className, resizeOrientation !== exports.TextAreResizeOrientation.both && formStyles.modifiers[orientation], validated === exports.ValidatedOptions.success && formStyles.modifiers.success, validated === exports.ValidatedOptions.warning && formStyles.modifiers.warning), onChange: this.handleChange }, (typeof this.props.defaultValue !== 'string' && { value }), { "aria-invalid": validated === exports.ValidatedOptions.error, required: isRequired }, props)));
        }
    }
    TextArea.displayName = 'TextArea';
    TextArea.defaultProps = {
        className: '',
        isRequired: false,
        validated: 'default',
        resizeOrientation: 'both',
        'aria-label': null
    };

    const InputGroup = (_a) => {
        var { className = '', children } = _a, props = __rest(_a, ["className", "children"]);
        const formCtrls = [FormSelect, TextArea, TextInput].map(comp => comp.toString());
        const idItem = React.Children.toArray(children).find((child) => !formCtrls.includes(child.type.toString()) && child.props.id);
        return (React.createElement("div", Object.assign({ className: css(styles$l.inputGroup, className) }, props), idItem
            ? React.Children.map(children, (child) => formCtrls.includes(child.type.toString())
                ? React.cloneElement(child, { 'aria-describedby': idItem.props.id })
                : child)
            : children));
    };
    InputGroup.displayName = 'InputGroup';

    const InputGroupText = (_a) => {
        var { className = '', component = 'span', children } = _a, props = __rest(_a, ["className", "component", "children"]);
        const Component = component;
        return (React.createElement(Component, Object.assign({ className: css(styles$l.inputGroupText, className) }, props), children));
    };
    InputGroupText.displayName = 'InputGroupText';

    // seed for the aria-labelledby ID
    let currentId$1 = 0;
    const newId = currentId$1++;
    class ContextSelector extends React.Component {
        constructor() {
            super(...arguments);
            this.parentRef = React.createRef();
            this.onEnterPressed = (event) => {
                if (event.charCode === KEY_CODES.ENTER) {
                    this.props.onSearchButtonClick();
                }
            };
        }
        render() {
            const toggleId = `pf-context-selector-toggle-id-${newId}`;
            const screenReaderLabelId = `pf-context-selector-label-id-${newId}`;
            const searchButtonId = `pf-context-selector-search-button-id-${newId}`;
            const _a = this.props, { children, className, isOpen, onToggle, onSelect, screenReaderLabel, toggleText, searchButtonAriaLabel, searchInputValue, onSearchInputChange, searchInputPlaceholder, onSearchButtonClick, menuAppendTo, ouiaId, ouiaSafe } = _a, props = __rest(_a, ["children", "className", "isOpen", "onToggle", "onSelect", "screenReaderLabel", "toggleText", "searchButtonAriaLabel", "searchInputValue", "onSearchInputChange", "searchInputPlaceholder", "onSearchButtonClick", "menuAppendTo", "ouiaId", "ouiaSafe"]);
            const menuContainer = (React.createElement("div", { className: css(styles$k.contextSelectorMenu) }, isOpen && (React.createElement(FocusTrap, { focusTrapOptions: { clickOutsideDeactivates: true } },
                React.createElement("div", { className: css(styles$k.contextSelectorMenuSearch) },
                    React.createElement(InputGroup, null,
                        React.createElement(TextInput, { value: searchInputValue, type: "search", placeholder: searchInputPlaceholder, onChange: onSearchInputChange, onKeyPress: this.onEnterPressed, "aria-labelledby": searchButtonId }),
                        React.createElement(Button, { variant: exports.ButtonVariant.control, "aria-label": searchButtonAriaLabel, id: searchButtonId, onClick: onSearchButtonClick },
                            React.createElement(SearchIcon, { "aria-hidden": "true" })))),
                React.createElement(ContextSelectorContext.Provider, { value: { onSelect } },
                    React.createElement(ContextSelectorMenuList, { isOpen: isOpen }, children))))));
            const popperContainer = (React.createElement("div", Object.assign({ className: css(styles$k.contextSelector, isOpen && styles$k.modifiers.expanded, className), ref: this.parentRef }, props), isOpen && menuContainer));
            const mainContainer = (React.createElement("div", Object.assign({ className: css(styles$k.contextSelector, isOpen && styles$k.modifiers.expanded, className), ref: this.parentRef }, getOUIAProps(ContextSelector.displayName, ouiaId, ouiaSafe), props),
                screenReaderLabel && (React.createElement("span", { id: screenReaderLabelId, hidden: true }, screenReaderLabel)),
                React.createElement(ContextSelectorToggle, { onToggle: onToggle, isOpen: isOpen, toggleText: toggleText, id: toggleId, parentRef: this.parentRef.current, "aria-labelledby": `${screenReaderLabelId} ${toggleId}` }),
                isOpen && menuAppendTo === 'inline' && menuContainer));
            const getParentElement = () => {
                if (this.parentRef && this.parentRef.current) {
                    return this.parentRef.current.parentElement;
                }
                return null;
            };
            return menuAppendTo === 'inline' ? (mainContainer) : (React.createElement(Popper, { trigger: mainContainer, popper: popperContainer, appendTo: menuAppendTo === 'parent' ? getParentElement() : menuAppendTo, isVisible: isOpen }));
        }
    }
    ContextSelector.displayName = 'ContextSelector';
    ContextSelector.defaultProps = {
        children: null,
        className: '',
        isOpen: false,
        onToggle: () => undefined,
        onSelect: () => undefined,
        screenReaderLabel: '',
        toggleText: '',
        searchButtonAriaLabel: 'Search menu items',
        searchInputValue: '',
        onSearchInputChange: () => undefined,
        searchInputPlaceholder: 'Search',
        onSearchButtonClick: () => undefined,
        menuAppendTo: 'inline',
        ouiaSafe: true
    };

    class ContextSelectorItem extends React.Component {
        constructor() {
            super(...arguments);
            this.ref = React.createRef();
        }
        componentDidMount() {
            /* eslint-disable-next-line */
            this.props.sendRef(this.props.index, this.ref.current);
        }
        render() {
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            const _a = this.props, { className, children, onClick, isDisabled, index, sendRef } = _a, props = __rest(_a, ["className", "children", "onClick", "isDisabled", "index", "sendRef"]);
            return (React.createElement(ContextSelectorContext.Consumer, null, ({ onSelect }) => (React.createElement("li", { role: "none" },
                React.createElement("button", Object.assign({ className: css(styles$k.contextSelectorMenuListItem, className), ref: this.ref, onClick: event => {
                        if (!isDisabled) {
                            onClick(event);
                            onSelect(event, children);
                        }
                    }, disabled: isDisabled }, props), children)))));
        }
    }
    ContextSelectorItem.displayName = 'ContextSelectorItem';
    ContextSelectorItem.defaultProps = {
        children: null,
        className: '',
        isDisabled: false,
        onClick: () => undefined,
        index: undefined,
        sendRef: () => { }
    };

    var dataList = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "dataList": "pf-c-data-list",
      "dataListAction": "pf-c-data-list__action",
      "dataListCell": "pf-c-data-list__cell",
      "dataListCheck": "pf-c-data-list__check",
      "dataListExpandableContent": "pf-c-data-list__expandable-content",
      "dataListExpandableContentBody": "pf-c-data-list__expandable-content-body",
      "dataListItem": "pf-c-data-list__item",
      "dataListItemAction": "pf-c-data-list__item-action",
      "dataListItemContent": "pf-c-data-list__item-content",
      "dataListItemControl": "pf-c-data-list__item-control",
      "dataListItemRow": "pf-c-data-list__item-row",
      "dataListToggle": "pf-c-data-list__toggle",
      "dataListToggleIcon": "pf-c-data-list__toggle-icon",
      "modifiers": {
        "hidden": "pf-m-hidden",
        "hiddenOnSm": "pf-m-hidden-on-sm",
        "visibleOnSm": "pf-m-visible-on-sm",
        "hiddenOnMd": "pf-m-hidden-on-md",
        "visibleOnMd": "pf-m-visible-on-md",
        "hiddenOnLg": "pf-m-hidden-on-lg",
        "visibleOnLg": "pf-m-visible-on-lg",
        "hiddenOnXl": "pf-m-hidden-on-xl",
        "visibleOnXl": "pf-m-visible-on-xl",
        "hiddenOn_2xl": "pf-m-hidden-on-2xl",
        "visibleOn_2xl": "pf-m-visible-on-2xl",
        "compact": "pf-m-compact",
        "selectable": "pf-m-selectable",
        "selected": "pf-m-selected",
        "expanded": "pf-m-expanded",
        "icon": "pf-m-icon",
        "noFill": "pf-m-no-fill",
        "alignRight": "pf-m-align-right",
        "flex_2": "pf-m-flex-2",
        "flex_3": "pf-m-flex-3",
        "flex_4": "pf-m-flex-4",
        "flex_5": "pf-m-flex-5",
        "noPadding": "pf-m-no-padding"
      }
    };
    });

    var styles$m = unwrapExports(dataList);

    const DataListContext = React.createContext({
        isSelectable: false
    });
    const DataList = (_a) => {
        var { children = null, className = '', 'aria-label': ariaLabel, selectedDataListItemId = '', onSelectDataListItem, isCompact = false } = _a, props = __rest(_a, ["children", "className", 'aria-label', "selectedDataListItemId", "onSelectDataListItem", "isCompact"]);
        const isSelectable = onSelectDataListItem !== undefined;
        const updateSelectedDataListItem = (id) => {
            onSelectDataListItem(id);
        };
        return (React.createElement(DataListContext.Provider, { value: {
                isSelectable,
                selectedDataListItemId,
                updateSelectedDataListItem
            } },
            React.createElement("ul", Object.assign({ className: css(styles$m.dataList, isCompact && styles$m.modifiers.compact, className), "aria-label": ariaLabel }, props), children)));
    };
    DataList.displayName = 'DataList';

    class DataListAction extends React.Component {
        constructor() {
            super(...arguments);
            this.state = {
                isOpen: false
            };
            this.onToggle = (isOpen) => {
                this.setState({ isOpen });
            };
            this.onSelect = () => {
                this.setState(prevState => ({
                    isOpen: !prevState.isOpen
                }));
            };
        }
        render() {
            const _a = this.props, { children, className, visibility, 
            /* eslint-disable @typescript-eslint/no-unused-vars */
            id, 'aria-label': ariaLabel, 'aria-labelledby': ariaLabelledBy, isPlainButtonAction } = _a, 
            /* eslint-enable @typescript-eslint/no-unused-vars */
            props = __rest(_a, ["children", "className", "visibility", "id", 'aria-label', 'aria-labelledby', "isPlainButtonAction"]);
            return (React.createElement("div", Object.assign({ className: css(styles$m.dataListItemAction, formatBreakpointMods(visibility, styles$m), className) }, props), isPlainButtonAction ? React.createElement("div", { className: css(styles$m.dataListAction) }, children) : children));
        }
    }
    DataListAction.displayName = 'DataListAction';

    const DataListCell = (_a) => {
        var { children = null, className = '', width = 1, isFilled = true, alignRight = false, isIcon = false } = _a, props = __rest(_a, ["children", "className", "width", "isFilled", "alignRight", "isIcon"]);
        return (React.createElement("div", Object.assign({ className: css(styles$m.dataListCell, width > 1 && styles$m.modifiers[`flex_${width}`], !isFilled && styles$m.modifiers.noFill, alignRight && styles$m.modifiers.alignRight, isIcon && styles$m.modifiers.icon, className) }, props), children));
    };
    DataListCell.displayName = 'DataListCell';

    const DataListCheck = (_a) => {
        var { className = '', 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        onChange = (checked, event) => { }, isValid = true, isDisabled = false, isChecked = null, checked = null } = _a, props = __rest(_a, ["className", "onChange", "isValid", "isDisabled", "isChecked", "checked"]);
        return (React.createElement("div", { className: css(styles$m.dataListItemControl, className) },
            React.createElement("div", { className: css('pf-c-data-list__check') },
                React.createElement("input", Object.assign({}, props, { type: "checkbox", onChange: event => onChange(event.currentTarget.checked, event), "aria-invalid": !isValid, disabled: isDisabled, checked: isChecked || checked })))));
    };
    DataListCheck.displayName = 'DataListCheck';

    var select = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "check": "pf-c-check",
      "checkLabel": "pf-c-check__label",
      "chipGroup": "pf-c-chip-group",
      "divider": "pf-c-divider",
      "formControl": "pf-c-form-control",
      "modifiers": {
        "disabled": "pf-m-disabled",
        "active": "pf-m-active",
        "expanded": "pf-m-expanded",
        "plain": "pf-m-plain",
        "typeahead": "pf-m-typeahead",
        "top": "pf-m-top",
        "alignRight": "pf-m-align-right",
        "favorite": "pf-m-favorite",
        "favoriteAction": "pf-m-favorite-action",
        "focus": "pf-m-focus",
        "link": "pf-m-link",
        "action": "pf-m-action",
        "selected": "pf-m-selected",
        "description": "pf-m-description"
      },
      "select": "pf-c-select",
      "selectMenu": "pf-c-select__menu",
      "selectMenuFieldset": "pf-c-select__menu-fieldset",
      "selectMenuGroup": "pf-c-select__menu-group",
      "selectMenuGroupTitle": "pf-c-select__menu-group-title",
      "selectMenuItem": "pf-c-select__menu-item",
      "selectMenuItemActionIcon": "pf-c-select__menu-item-action-icon",
      "selectMenuItemDescription": "pf-c-select__menu-item-description",
      "selectMenuItemIcon": "pf-c-select__menu-item-icon",
      "selectMenuItemMain": "pf-c-select__menu-item-main",
      "selectMenuItemMatch": "pf-c-select__menu-item--match",
      "selectMenuSearch": "pf-c-select__menu-search",
      "selectMenuWrapper": "pf-c-select__menu-wrapper",
      "selectToggle": "pf-c-select__toggle",
      "selectToggleArrow": "pf-c-select__toggle-arrow",
      "selectToggleBadge": "pf-c-select__toggle-badge",
      "selectToggleButton": "pf-c-select__toggle-button",
      "selectToggleClear": "pf-c-select__toggle-clear",
      "selectToggleIcon": "pf-c-select__toggle-icon",
      "selectToggleText": "pf-c-select__toggle-text",
      "selectToggleTypeahead": "pf-c-select__toggle-typeahead",
      "selectToggleWrapper": "pf-c-select__toggle-wrapper"
    };
    });

    var styles$n = unwrapExports(select);

    var timesCircleIcon = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.TimesCircleIconConfig = {
      name: 'TimesCircleIcon',
      height: 512,
      width: 512,
      svgPath: 'M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z',
      yOffset: 0,
      xOffset: 0,
      transform: ''
    };
    exports.TimesCircleIcon = createIcon_1.createIcon(exports.TimesCircleIconConfig);
    exports["default"] = exports.TimesCircleIcon;
    });

    var TimesCircleIcon = unwrapExports(timesCircleIcon);
    var timesCircleIcon_1 = timesCircleIcon.TimesCircleIconConfig;
    var timesCircleIcon_2 = timesCircleIcon.TimesCircleIcon;

    var form = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "form": "pf-c-form",
      "formActions": "pf-c-form__actions",
      "formFieldset": "pf-c-form__fieldset",
      "formGroup": "pf-c-form__group",
      "formGroupControl": "pf-c-form__group-control",
      "formGroupLabel": "pf-c-form__group-label",
      "formGroupLabelHelp": "pf-c-form__group-label-help",
      "formHelperText": "pf-c-form__helper-text",
      "formHelperTextIcon": "pf-c-form__helper-text-icon",
      "formLabel": "pf-c-form__label",
      "formLabelRequired": "pf-c-form__label-required",
      "formLabelText": "pf-c-form__label-text",
      "modifiers": {
        "horizontal": "pf-m-horizontal",
        "alignRight": "pf-m-align-right",
        "noPaddingTop": "pf-m-no-padding-top",
        "action": "pf-m-action",
        "disabled": "pf-m-disabled",
        "inline": "pf-m-inline",
        "error": "pf-m-error",
        "success": "pf-m-success",
        "warning": "pf-m-warning",
        "inactive": "pf-m-inactive",
        "hidden": "pf-m-hidden"
      }
    };
    });

    var styles$o = unwrapExports(form);

    var checkIcon = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.CheckIconConfig = {
      name: 'CheckIcon',
      height: 512,
      width: 512,
      svgPath: 'M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z',
      yOffset: 0,
      xOffset: 0,
      transform: ''
    };
    exports.CheckIcon = createIcon_1.createIcon(exports.CheckIconConfig);
    exports["default"] = exports.CheckIcon;
    });

    var CheckIcon = unwrapExports(checkIcon);
    var checkIcon_1 = checkIcon.CheckIconConfig;
    var checkIcon_2 = checkIcon.CheckIcon;

    const SelectContext = React.createContext(null);
    const SelectProvider = SelectContext.Provider;
    const SelectConsumer = SelectContext.Consumer;
    (function (SelectVariant) {
        SelectVariant["single"] = "single";
        SelectVariant["checkbox"] = "checkbox";
        SelectVariant["typeahead"] = "typeahead";
        SelectVariant["typeaheadMulti"] = "typeaheadmulti";
        SelectVariant["panel"] = "panel";
    })(exports.SelectVariant || (exports.SelectVariant = {}));
    (function (SelectDirection) {
        SelectDirection["up"] = "up";
        SelectDirection["down"] = "down";
    })(exports.SelectDirection || (exports.SelectDirection = {}));
    const KeyTypes = {
        Tab: 'Tab',
        Space: ' ',
        Escape: 'Escape',
        Enter: 'Enter',
        ArrowUp: 'ArrowUp',
        ArrowDown: 'ArrowDown'
    };

    class SelectOption extends React.Component {
        constructor() {
            super(...arguments);
            this.ref = React.createRef();
            this.onKeyDown = (event) => {
                if (event.key === KeyTypes.Tab) {
                    return;
                }
                event.preventDefault();
                if (event.key === KeyTypes.ArrowUp) {
                    this.props.keyHandler(this.props.index, 'up');
                }
                else if (event.key === KeyTypes.ArrowDown) {
                    this.props.keyHandler(this.props.index, 'down');
                }
                else if (event.key === KeyTypes.Enter) {
                    this.ref.current.click();
                    if (this.context.variant === exports.SelectVariant.checkbox) {
                        this.ref.current.focus();
                    }
                }
            };
        }
        componentDidMount() {
            this.props.sendRef(this.props.isDisabled ? null : this.ref.current, this.props.index);
        }
        componentDidUpdate() {
            this.props.sendRef(this.props.isDisabled ? null : this.ref.current, this.props.index);
        }
        render() {
            /* eslint-disable @typescript-eslint/no-unused-vars */
            const _a = this.props, { children, className, description, value, onClick, isDisabled, isPlaceholder, isNoResultsOption, isSelected, isChecked, isFocused, sendRef, keyHandler, index, component, inputId } = _a, props = __rest(_a, ["children", "className", "description", "value", "onClick", "isDisabled", "isPlaceholder", "isNoResultsOption", "isSelected", "isChecked", "isFocused", "sendRef", "keyHandler", "index", "component", "inputId"]);
            /* eslint-enable @typescript-eslint/no-unused-vars */
            const Component = component;
            return (React.createElement(SelectConsumer, null, ({ onSelect, onClose, variant, inputIdPrefix }) => (React.createElement(React.Fragment, null,
                variant !== exports.SelectVariant.checkbox && (React.createElement("li", { role: "presentation" },
                    React.createElement(Component, Object.assign({}, props, { className: css(styles$n.selectMenuItem, isSelected && styles$n.modifiers.selected, isDisabled && styles$n.modifiers.disabled, isFocused && styles$n.modifiers.focus, description && styles$n.modifiers.description, className), onClick: (event) => {
                            if (!isDisabled) {
                                onClick(event);
                                onSelect(event, value, isPlaceholder);
                                onClose();
                            }
                        }, role: "option", "aria-selected": isSelected || null, ref: this.ref, onKeyDown: this.onKeyDown, type: "button" }),
                        description && (React.createElement(React.Fragment, null,
                            React.createElement("div", { className: css(styles$n.selectMenuItemMain) },
                                children || value.toString(),
                                isSelected && (React.createElement("span", { className: css(styles$n.selectMenuItemIcon) },
                                    React.createElement(CheckIcon, { "aria-hidden": true })))),
                            React.createElement("div", { className: css(styles$n.selectMenuItemDescription) }, description))),
                        !description && (React.createElement(React.Fragment, null,
                            children || value.toString(),
                            isSelected && (React.createElement("span", { className: css(styles$n.selectMenuItemIcon) },
                                React.createElement(CheckIcon, { "aria-hidden": true })))))))),
                variant === exports.SelectVariant.checkbox && !isNoResultsOption && (React.createElement("label", Object.assign({}, props, { className: css(checkStyles.check, styles$n.selectMenuItem, isDisabled && styles$n.modifiers.disabled, isFocused && styles$n.modifiers.focus, description && styles$n.modifiers.description, className), onKeyDown: this.onKeyDown }),
                    React.createElement("input", { id: inputId || `${inputIdPrefix}-${value.toString()}`, className: css(checkStyles.checkInput), type: "checkbox", onChange: event => {
                            if (!isDisabled) {
                                onClick(event);
                                onSelect(event, value);
                            }
                        }, ref: this.ref, checked: isChecked || false, disabled: isDisabled }),
                    React.createElement("span", { className: css(checkStyles.checkLabel, isDisabled && styles$n.modifiers.disabled) }, children || value.toString()),
                    description && React.createElement("div", { className: css(checkStyles.checkDescription) }, description))),
                variant === exports.SelectVariant.checkbox && isNoResultsOption && (React.createElement("div", null,
                    React.createElement(Component, Object.assign({}, props, { className: css(styles$n.selectMenuItem, isSelected && styles$n.modifiers.selected, isDisabled && styles$n.modifiers.disabled, className), role: "option", "aria-selected": isSelected || null, ref: this.ref, onKeyDown: this.onKeyDown, type: "button" }), children || value.toString())))))));
        }
    }
    SelectOption.displayName = 'SelectOption';
    SelectOption.defaultProps = {
        className: '',
        value: '',
        index: 0,
        isDisabled: false,
        isPlaceholder: false,
        isSelected: false,
        isChecked: false,
        isNoResultsOption: false,
        component: 'button',
        onClick: () => { },
        sendRef: () => { },
        keyHandler: () => { },
        inputId: ''
    };

    class SelectMenuWithRef extends React.Component {
        extendChildren(randomId) {
            const { children, isGrouped } = this.props;
            const childrenArray = children;
            if (isGrouped) {
                return React.Children.map(childrenArray, (group, index) => React.cloneElement(group, {
                    titleId: group.props.label && group.props.label.replace(/\W/g, '-'),
                    children: group.props.children.map((option) => this.cloneOption(option, index++, randomId))
                }));
            }
            return React.Children.map(childrenArray, (child, index) => this.cloneOption(child, index, randomId));
        }
        cloneOption(child, index, randomId) {
            const { selected, sendRef, keyHandler } = this.props;
            const isSelected = this.checkForValue(child.props.value, selected);
            return React.cloneElement(child, {
                inputId: `${randomId}-${index}`,
                id: `${child.props.id ? child.props.id : randomId}-${index}`,
                isSelected,
                sendRef,
                keyHandler,
                index
            });
        }
        checkForValue(valueToCheck, options) {
            if (!options) {
                return false;
            }
            const isSelectOptionObject = typeof valueToCheck !== 'string' &&
                valueToCheck.toString &&
                valueToCheck.compareTo;
            if (Array.isArray(options)) {
                if (isSelectOptionObject) {
                    return options.some(option => option.compareTo(valueToCheck));
                }
                else {
                    return options.includes(valueToCheck);
                }
            }
            else {
                if (isSelectOptionObject) {
                    return options.compareTo(valueToCheck);
                }
                else {
                    return options === valueToCheck;
                }
            }
        }
        extendCheckboxChildren(children) {
            const { isGrouped, checked, sendRef, keyHandler, hasInlineFilter } = this.props;
            let index = hasInlineFilter ? 1 : 0;
            if (isGrouped) {
                return React.Children.map(children, (group) => {
                    if (group.type === SelectOption) {
                        return group;
                    }
                    return React.cloneElement(group, {
                        titleId: group.props.label && group.props.label.replace(/\W/g, '-'),
                        children: (React.createElement("fieldset", { "aria-labelledby": group.props.label && group.props.label.replace(/\W/g, '-'), className: css(styles$n.selectMenuFieldset) }, React.Children.map(group.props.children, (option) => React.cloneElement(option, {
                            isChecked: this.checkForValue(option.props.value, checked),
                            sendRef,
                            keyHandler,
                            index: index++
                        }))))
                    });
                });
            }
            return React.Children.map(children, (child) => React.cloneElement(child, {
                isChecked: this.checkForValue(child.props.value, checked),
                sendRef,
                keyHandler,
                index: index++
            }));
        }
        render() {
            /* eslint-disable @typescript-eslint/no-unused-vars */
            const _a = this.props, { children, isCustomContent, className, isExpanded, openedOnEnter, selected, checked, isGrouped, sendRef, keyHandler, maxHeight, noResultsFoundText, createText, 'aria-label': ariaLabel, 'aria-labelledby': ariaLabelledBy, hasInlineFilter, innerRef } = _a, props = __rest(_a, ["children", "isCustomContent", "className", "isExpanded", "openedOnEnter", "selected", "checked", "isGrouped", "sendRef", "keyHandler", "maxHeight", "noResultsFoundText", "createText", 'aria-label', 'aria-labelledby', "hasInlineFilter", "innerRef"]);
            /* eslint-enable @typescript-eslint/no-unused-vars */
            return (React.createElement(SelectConsumer, null, ({ variant, inputIdPrefix }) => (React.createElement(React.Fragment, null,
                isCustomContent && (React.createElement("div", Object.assign({ ref: innerRef, className: css(styles$n.selectMenu, className) }, (maxHeight && { style: { maxHeight, overflow: 'auto' } }), props), children)),
                variant !== exports.SelectVariant.checkbox && !isCustomContent && (React.createElement("ul", Object.assign({ ref: innerRef, className: css(styles$n.selectMenu, className), role: "listbox" }, (maxHeight && { style: { maxHeight, overflow: 'auto' } }), props), this.extendChildren(inputIdPrefix))),
                variant === exports.SelectVariant.checkbox && !isCustomContent && React.Children.count(children) > 0 && (React.createElement(FocusTrap, { focusTrapOptions: { clickOutsideDeactivates: true } },
                    React.createElement("div", Object.assign({ ref: innerRef, className: css(styles$n.selectMenu, className) }, (maxHeight && { style: { maxHeight, overflow: 'auto' } })),
                        React.createElement("fieldset", Object.assign({}, props, { "aria-label": ariaLabel, "aria-labelledby": (!ariaLabel && ariaLabelledBy) || null, className: css(styles$o.formFieldset) }),
                            hasInlineFilter && [
                                children.shift(),
                                ...this.extendCheckboxChildren(children)
                            ],
                            !hasInlineFilter && this.extendCheckboxChildren(children))))),
                variant === exports.SelectVariant.checkbox && !isCustomContent && React.Children.count(children) === 0 && (React.createElement("div", Object.assign({ ref: innerRef, className: css(styles$n.selectMenu, className) }, (maxHeight && { style: { maxHeight, overflow: 'auto' } })),
                    React.createElement("fieldset", { className: css(styles$n.selectMenuFieldset) })))))));
        }
    }
    SelectMenuWithRef.displayName = 'SelectMenu';
    SelectMenuWithRef.defaultProps = {
        className: '',
        isExpanded: false,
        isGrouped: false,
        openedOnEnter: false,
        selected: '',
        maxHeight: '',
        sendRef: () => { },
        keyHandler: () => { },
        isCustomContent: false,
        hasInlineFilter: false
    };
    const SelectMenu = React.forwardRef((props, ref) => (React.createElement(SelectMenuWithRef, Object.assign({ innerRef: ref }, props), props.children)));

    class SelectToggle extends React.Component {
        constructor(props) {
            super(props);
            this.onDocClick = (event) => {
                const { parentRef, menuRef, isOpen, onToggle, onClose } = this.props;
                const clickedOnToggle = parentRef && parentRef.current && parentRef.current.contains(event.target);
                const clickedWithinMenu = menuRef && menuRef.current && menuRef.current.contains && menuRef.current.contains(event.target);
                if (isOpen && !(clickedOnToggle || clickedWithinMenu)) {
                    onToggle(false);
                    onClose();
                    this.toggle.current.focus();
                }
            };
            this.onEscPress = (event) => {
                const { parentRef, menuRef, isOpen, variant, onToggle, onClose } = this.props;
                if (event.key === KeyTypes.Tab && variant === exports.SelectVariant.checkbox) {
                    return;
                }
                const escFromToggle = parentRef && parentRef.current && parentRef.current.contains(event.target);
                const escFromWithinMenu = menuRef && menuRef.current && menuRef.current.contains && menuRef.current.contains(event.target);
                if (isOpen &&
                    (event.key === KeyTypes.Escape || event.key === KeyTypes.Tab) &&
                    (escFromToggle || escFromWithinMenu)) {
                    onToggle(false);
                    onClose();
                    this.toggle.current.focus();
                }
            };
            this.onKeyDown = (event) => {
                const { isOpen, onToggle, variant, onClose, onEnter, handleTypeaheadKeys } = this.props;
                if ((event.key === KeyTypes.ArrowDown || event.key === KeyTypes.ArrowUp) &&
                    (variant === exports.SelectVariant.typeahead || variant === exports.SelectVariant.typeaheadMulti)) {
                    handleTypeaheadKeys((event.key === KeyTypes.ArrowDown && 'down') || (event.key === KeyTypes.ArrowUp && 'up'));
                }
                if (event.key === KeyTypes.Enter &&
                    (variant === exports.SelectVariant.typeahead || variant === exports.SelectVariant.typeaheadMulti)) {
                    if (isOpen) {
                        handleTypeaheadKeys('enter');
                    }
                    else {
                        onToggle(!isOpen);
                    }
                }
                if ((event.key === KeyTypes.Tab && variant === exports.SelectVariant.checkbox) ||
                    (event.key === KeyTypes.Tab && !isOpen) ||
                    (event.key !== KeyTypes.Enter && event.key !== KeyTypes.Space) ||
                    ((event.key === KeyTypes.Space || event.key === KeyTypes.Enter) &&
                        (variant === exports.SelectVariant.typeahead || variant === exports.SelectVariant.typeaheadMulti))) {
                    return;
                }
                event.preventDefault();
                if ((event.key === KeyTypes.Tab || event.key === KeyTypes.Enter || event.key === KeyTypes.Space) && isOpen) {
                    onToggle(!isOpen);
                    onClose();
                    this.toggle.current.focus();
                }
                else if ((event.key === KeyTypes.Enter || event.key === KeyTypes.Space) && !isOpen) {
                    onToggle(!isOpen);
                    onEnter();
                }
            };
            const { variant } = props;
            const isTypeahead = variant === exports.SelectVariant.typeahead || variant === exports.SelectVariant.typeaheadMulti;
            this.toggle = isTypeahead ? React.createRef() : React.createRef();
        }
        componentDidMount() {
            document.addEventListener('mousedown', this.onDocClick);
            document.addEventListener('touchstart', this.onDocClick);
            document.addEventListener('keydown', this.onEscPress);
        }
        componentWillUnmount() {
            document.removeEventListener('mousedown', this.onDocClick);
            document.removeEventListener('touchstart', this.onDocClick);
            document.removeEventListener('keydown', this.onEscPress);
        }
        render() {
            /* eslint-disable @typescript-eslint/no-unused-vars */
            const _a = this.props, { className, children, isOpen, isActive, isPlain, isDisabled, variant, onToggle, onEnter, onClose, handleTypeaheadKeys, parentRef, menuRef, id, type, hasClearButton, 'aria-labelledby': ariaLabelledBy, 'aria-label': ariaLabel } = _a, props = __rest(_a, ["className", "children", "isOpen", "isActive", "isPlain", "isDisabled", "variant", "onToggle", "onEnter", "onClose", "handleTypeaheadKeys", "parentRef", "menuRef", "id", "type", "hasClearButton", 'aria-labelledby', 'aria-label']);
            /* eslint-enable @typescript-eslint/no-unused-vars */
            const isTypeahead = variant === exports.SelectVariant.typeahead || variant === exports.SelectVariant.typeaheadMulti || hasClearButton;
            const toggleProps = {
                id,
                'aria-labelledby': ariaLabelledBy,
                'aria-expanded': isOpen,
                'aria-haspopup': (variant !== exports.SelectVariant.checkbox && 'listbox') || null
            };
            return (React.createElement(React.Fragment, null,
                !isTypeahead && (React.createElement("button", Object.assign({}, props, toggleProps, { ref: this.toggle, type: type, className: css(styles$n.selectToggle, isDisabled && styles$n.modifiers.disabled, isPlain && styles$n.modifiers.plain, isActive && styles$n.modifiers.active, className), 
                    // eslint-disable-next-line @typescript-eslint/no-unused-vars
                    onClick: _event => {
                        onToggle(!isOpen);
                        if (isOpen) {
                            onClose();
                        }
                    }, onKeyDown: this.onKeyDown, disabled: isDisabled }),
                    children,
                    React.createElement("span", { className: css(styles$n.selectToggleArrow) },
                        React.createElement(CaretDownIcon, null)))),
                isTypeahead && (React.createElement("div", Object.assign({}, props, { ref: this.toggle, className: css(styles$n.selectToggle, isDisabled && styles$n.modifiers.disabled, isPlain && styles$n.modifiers.plain, isTypeahead && styles$n.modifiers.typeahead, className), 
                    // eslint-disable-next-line @typescript-eslint/no-unused-vars
                    onClick: _event => {
                        if (!isDisabled) {
                            onToggle(true);
                        }
                    }, onKeyDown: this.onKeyDown }),
                    children,
                    React.createElement("button", Object.assign({}, toggleProps, { type: type, className: css(buttonStyles.button, styles$n.selectToggleButton, styles$n.modifiers.plain), "aria-label": ariaLabel, onClick: _event => {
                            _event.stopPropagation();
                            onToggle(!isOpen);
                            if (isOpen) {
                                onClose();
                            }
                        } }, ((variant === exports.SelectVariant.typeahead || variant === exports.SelectVariant.typeaheadMulti) && {
                        tabIndex: -1
                    }), { disabled: isDisabled }),
                        React.createElement(CaretDownIcon, { className: css(styles$n.selectToggleArrow) }))))));
        }
    }
    SelectToggle.displayName = 'SelectToggle';
    SelectToggle.defaultProps = {
        className: '',
        isOpen: false,
        isActive: false,
        isPlain: false,
        isDisabled: false,
        hasClearButton: false,
        variant: 'single',
        'aria-labelledby': '',
        'aria-label': '',
        type: 'button',
        onToggle: () => { },
        onEnter: () => { },
        onClose: () => { }
    };

    // seed for the aria-labelledby ID
    let currentId$2 = 0;
    class Select extends React.Component {
        constructor() {
            super(...arguments);
            this.parentRef = React.createRef();
            this.menuComponentRef = React.createRef();
            this.filterRef = React.createRef();
            this.clearRef = React.createRef();
            this.refCollection = [];
            this.state = {
                openedOnEnter: false,
                typeaheadInputValue: null,
                typeaheadActiveChild: null,
                typeaheadFilteredChildren: React.Children.toArray(this.props.children),
                typeaheadCurrIndex: -1,
                creatableValue: ''
            };
            this.componentDidUpdate = (prevProps, prevState) => {
                if (this.props.hasInlineFilter) {
                    this.refCollection[0] = this.filterRef.current;
                }
                if (!prevState.openedOnEnter && this.state.openedOnEnter && !this.props.customContent) {
                    this.refCollection[0].focus();
                }
                if (prevProps.children !== this.props.children) {
                    this.setState({
                        typeaheadFilteredChildren: React.Children.toArray(this.props.children)
                    });
                }
                if (prevProps.selections !== this.props.selections && this.props.variant === exports.SelectVariant.typeahead) {
                    this.setState({
                        typeaheadInputValue: this.props.selections
                    });
                }
            };
            this.onEnter = () => {
                this.setState({ openedOnEnter: true });
            };
            this.onClose = () => {
                this.setState({
                    openedOnEnter: false,
                    typeaheadInputValue: null,
                    typeaheadActiveChild: null,
                    typeaheadFilteredChildren: React.Children.toArray(this.props.children),
                    typeaheadCurrIndex: -1
                });
            };
            this.onChange = (e) => {
                const { onFilter, isCreatable, onCreateOption, createText, noResultsFoundText, children } = this.props;
                let typeaheadFilteredChildren;
                if (onFilter) {
                    typeaheadFilteredChildren = onFilter(e) || children;
                }
                else {
                    let input;
                    try {
                        input = new RegExp(e.target.value.toString(), 'i');
                    }
                    catch (err) {
                        input = new RegExp(e.target.value.toString().replace(/[.*+?^${}()|[\]\\]/g, '\\$&'), 'i');
                    }
                    typeaheadFilteredChildren =
                        e.target.value.toString() !== ''
                            ? React.Children.toArray(this.props.children).filter(child => this.getDisplay(child.props.value.toString(), 'text').search(input) === 0)
                            : React.Children.toArray(this.props.children);
                }
                if (!typeaheadFilteredChildren) {
                    typeaheadFilteredChildren = [];
                }
                if (typeaheadFilteredChildren.length === 0) {
                    !isCreatable &&
                        typeaheadFilteredChildren.push(React.createElement(SelectOption, { isDisabled: true, key: 0, value: noResultsFoundText, isNoResultsOption: true }));
                }
                if (isCreatable && e.target.value !== '') {
                    const newValue = e.target.value;
                    typeaheadFilteredChildren.push(React.createElement(SelectOption, { key: 0, value: newValue, onClick: () => onCreateOption && onCreateOption(newValue) },
                        createText,
                        " \"",
                        newValue,
                        "\""));
                }
                this.setState({
                    typeaheadInputValue: e.target.value,
                    typeaheadCurrIndex: -1,
                    typeaheadFilteredChildren,
                    typeaheadActiveChild: null,
                    creatableValue: e.target.value
                });
                this.refCollection = [];
            };
            this.onClick = (e) => {
                e.stopPropagation();
            };
            this.clearSelection = (e) => {
                e.stopPropagation();
                this.setState({
                    typeaheadInputValue: null,
                    typeaheadActiveChild: null,
                    typeaheadFilteredChildren: React.Children.toArray(this.props.children),
                    typeaheadCurrIndex: -1
                });
            };
            this.sendRef = (ref, index) => {
                this.refCollection[index] = ref;
            };
            this.handleArrowKeys = (index, position) => {
                keyHandler(index, 0, position, this.refCollection, this.refCollection);
            };
            this.handleFocus = () => {
                if (!this.props.isOpen) {
                    this.props.onToggle(true);
                }
            };
            this.handleTypeaheadKeys = (position) => {
                const { isOpen, isCreatable, createText } = this.props;
                const { typeaheadActiveChild, typeaheadCurrIndex } = this.state;
                if (isOpen) {
                    if (position === 'enter' && (typeaheadActiveChild || this.refCollection[0])) {
                        this.setState({
                            typeaheadInputValue: (typeaheadActiveChild && typeaheadActiveChild.innerText) || this.refCollection[0].innerText
                        });
                        if (typeaheadActiveChild) {
                            typeaheadActiveChild.click();
                        }
                        else {
                            this.refCollection[0].click();
                        }
                    }
                    else {
                        let nextIndex;
                        if (typeaheadCurrIndex === -1 && position === 'down') {
                            nextIndex = 0;
                        }
                        else if (typeaheadCurrIndex === -1 && position === 'up') {
                            nextIndex = this.refCollection.length - 1;
                        }
                        else {
                            nextIndex = getNextIndex(typeaheadCurrIndex, position, this.refCollection);
                        }
                        const hasDescriptionElm = Boolean(this.refCollection[nextIndex].classList.contains('pf-m-description'));
                        const optionTextElm = hasDescriptionElm
                            ? this.refCollection[nextIndex].firstElementChild
                            : this.refCollection[nextIndex];
                        this.setState({
                            typeaheadCurrIndex: nextIndex,
                            typeaheadActiveChild: this.refCollection[nextIndex],
                            typeaheadInputValue: isCreatable && optionTextElm.innerText.includes(createText)
                                ? this.state.creatableValue
                                : optionTextElm.innerText
                        });
                    }
                }
            };
            this.getDisplay = (value, type = 'node') => {
                if (!value) {
                    return;
                }
                const item = this.props.isGrouped
                    ? React.Children.toArray(this.props.children)
                        .reduce((acc, curr) => [...acc, ...React.Children.toArray(curr.props.children)], [])
                        .find(child => child.props.value.toString() === value.toString())
                    : React.Children.toArray(this.props.children).find(child => child.props.value.toString() === value.toString());
                if (item) {
                    if (item && item.props.children) {
                        if (type === 'node') {
                            return item.props.children;
                        }
                        return this.findText(item);
                    }
                    return item.props.value.toString();
                }
                return value;
            };
            this.findText = (item) => {
                if (!item.props || !item.props.children) {
                    if (typeof item !== 'string') {
                        return '';
                    }
                    return item;
                }
                if (typeof item.props.children === 'string') {
                    return item.props.children;
                }
                const multi = [];
                React.Children.toArray(item.props.children).forEach((child) => multi.push(this.findText(child)));
                return multi.join('');
            };
            this.generateSelectedBadge = () => {
                const { customBadgeText, selections } = this.props;
                if (customBadgeText !== null) {
                    return customBadgeText;
                }
                if (Array.isArray(selections) && selections.length > 0) {
                    return selections.length;
                }
                return null;
            };
        }
        extendTypeaheadChildren(typeaheadActiveChild) {
            let activeElement = null;
            if (Boolean(typeaheadActiveChild) && typeaheadActiveChild.classList.contains('pf-m-description')) {
                activeElement = typeaheadActiveChild.firstElementChild;
            }
            else if (typeaheadActiveChild) {
                activeElement = typeaheadActiveChild;
            }
            return this.state.typeaheadFilteredChildren.map((child) => React.cloneElement(child, {
                isFocused: activeElement &&
                    (activeElement.innerText === this.getDisplay(child.props.value.toString(), 'text') ||
                        (this.props.isCreatable &&
                            typeaheadActiveChild.innerText === `{createText} "${child.props.value}"`))
            }));
        }
        render() {
            const _a = this.props, { children, className, customContent, variant, direction, onToggle, onSelect, onClear, toggleId, isOpen, isGrouped, isPlain, isDisabled, selections: selectionsProp, typeAheadAriaLabel, clearSelectionsAriaLabel, toggleAriaLabel, removeSelectionAriaLabel, 'aria-label': ariaLabel, 'aria-labelledby': ariaLabelledBy, placeholderText, width, maxHeight, toggleIcon, ouiaId, ouiaSafe, hasInlineFilter, isCheckboxSelectionBadgeHidden, inlineFilterPlaceholderText, 
            /* eslint-disable @typescript-eslint/no-unused-vars */
            onFilter, onCreateOption, isCreatable, createText, noResultsFoundText, customBadgeText, inputIdPrefix, 
            /* eslint-enable @typescript-eslint/no-unused-vars */
            menuAppendTo } = _a, props = __rest(_a, ["children", "className", "customContent", "variant", "direction", "onToggle", "onSelect", "onClear", "toggleId", "isOpen", "isGrouped", "isPlain", "isDisabled", "selections", "typeAheadAriaLabel", "clearSelectionsAriaLabel", "toggleAriaLabel", "removeSelectionAriaLabel", 'aria-label', 'aria-labelledby', "placeholderText", "width", "maxHeight", "toggleIcon", "ouiaId", "ouiaSafe", "hasInlineFilter", "isCheckboxSelectionBadgeHidden", "inlineFilterPlaceholderText", "onFilter", "onCreateOption", "isCreatable", "createText", "noResultsFoundText", "customBadgeText", "inputIdPrefix", "menuAppendTo"]);
            const { openedOnEnter, typeaheadInputValue, typeaheadActiveChild, typeaheadFilteredChildren } = this.state;
            const selectToggleId = toggleId || `pf-select-toggle-id-${currentId$2++}`;
            const selections = Array.isArray(selectionsProp) ? selectionsProp : [selectionsProp];
            const hasAnySelections = Boolean(selections[0] && selections[0] !== '');
            let childPlaceholderText = null;
            if (!customContent) {
                if (!hasAnySelections && !placeholderText) {
                    const childPlaceholder = React.Children.toArray(children).filter((child) => child.props.isPlaceholder === true);
                    childPlaceholderText =
                        (childPlaceholder[0] && this.getDisplay(childPlaceholder[0].props.value, 'node')) ||
                            (children[0] && this.getDisplay(children[0].props.value, 'node'));
                }
            }
            const hasOnClear = onClear !== Select.defaultProps.onClear;
            const clearBtn = (React.createElement("button", { className: css(buttonStyles.button, buttonStyles.modifiers.plain, styles$n.selectToggleClear), onClick: e => {
                    this.clearSelection(e);
                    onClear(e);
                }, "aria-label": clearSelectionsAriaLabel, type: "button", disabled: isDisabled, ref: this.clearRef, onKeyDown: event => {
                    if (event.key === KeyTypes.Enter) {
                        this.clearRef.current.click();
                    }
                } },
                React.createElement(TimesCircleIcon, { "aria-hidden": true })));
            let selectedChips = null;
            if (variant === exports.SelectVariant.typeaheadMulti) {
                selectedChips = (React.createElement(ChipGroup, null, selections &&
                    selections.map(item => (React.createElement(Chip, { key: item, onClick: (e) => onSelect(e, item), closeBtnAriaLabel: removeSelectionAriaLabel }, this.getDisplay(item, 'node'))))));
            }
            let filterWithChildren = children;
            if (hasInlineFilter) {
                const filterBox = (React.createElement(React.Fragment, null,
                    React.createElement("div", { key: "inline-filter", className: css(styles$n.selectMenuSearch) },
                        React.createElement("input", { key: "inline-filter-input", type: "search", className: css(formStyles.formControl, formStyles.modifiers.search), onChange: this.onChange, placeholder: inlineFilterPlaceholderText, onKeyDown: event => {
                                if (event.key === KeyTypes.ArrowUp) {
                                    this.handleArrowKeys(0, 'up');
                                }
                                else if (event.key === KeyTypes.ArrowDown) {
                                    this.handleArrowKeys(0, 'down');
                                }
                            }, ref: this.filterRef, autoComplete: "off" })),
                    React.createElement(Divider, { key: "inline-filter-divider" })));
                this.refCollection[0] = this.filterRef.current;
                filterWithChildren = [filterBox, ...typeaheadFilteredChildren].map((option, index) => React.cloneElement(option, { key: index }));
            }
            let variantProps;
            let variantChildren;
            if (customContent) {
                variantProps = {
                    selected: selections,
                    openedOnEnter,
                    isCustomContent: true
                };
                variantChildren = customContent;
            }
            else {
                switch (variant) {
                    case 'single':
                        variantProps = {
                            selected: selections[0],
                            openedOnEnter
                        };
                        variantChildren = children;
                        break;
                    case 'checkbox':
                        variantProps = {
                            checked: selections,
                            isGrouped,
                            hasInlineFilter
                        };
                        variantChildren = filterWithChildren;
                        break;
                    case 'typeahead':
                        variantProps = {
                            selected: selections[0],
                            openedOnEnter
                        };
                        variantChildren = this.extendTypeaheadChildren(typeaheadActiveChild);
                        break;
                    case 'typeaheadmulti':
                        variantProps = {
                            selected: selections,
                            openedOnEnter
                        };
                        variantChildren = this.extendTypeaheadChildren(typeaheadActiveChild);
                        break;
                }
            }
            const menuContainer = (React.createElement(SelectMenu, Object.assign({}, props, { isGrouped: isGrouped, selected: selections }, variantProps, { openedOnEnter: openedOnEnter, "aria-label": ariaLabel, "aria-labelledby": ariaLabelledBy, sendRef: this.sendRef, keyHandler: this.handleArrowKeys, maxHeight: maxHeight, ref: this.menuComponentRef }), variantChildren));
            const popperContainer = (React.createElement("div", { className: css(styles$n.select, isOpen && styles$n.modifiers.expanded, direction === exports.SelectDirection.up && styles$n.modifiers.top, className) }, isOpen && menuContainer));
            const mainContainer = (React.createElement("div", Object.assign({ className: css(styles$n.select, isOpen && styles$n.modifiers.expanded, direction === exports.SelectDirection.up && styles$n.modifiers.top, className), ref: this.parentRef }, getOUIAProps(Select.displayName, ouiaId, ouiaSafe)),
                React.createElement(SelectToggle, { id: selectToggleId, parentRef: this.parentRef, menuRef: this.menuComponentRef, isOpen: isOpen, isPlain: isPlain, onToggle: onToggle, onEnter: this.onEnter, onClose: this.onClose, variant: variant, "aria-labelledby": `${ariaLabelledBy || ''} ${selectToggleId}`, "aria-label": toggleAriaLabel, handleTypeaheadKeys: this.handleTypeaheadKeys, isDisabled: isDisabled, hasClearButton: hasOnClear },
                    customContent && (React.createElement("div", { className: css(styles$n.selectToggleWrapper) },
                        toggleIcon && React.createElement("span", { className: css(styles$n.selectToggleIcon) }, toggleIcon),
                        React.createElement("span", { className: css(styles$n.selectToggleText) }, placeholderText))),
                    variant === exports.SelectVariant.single && !customContent && (React.createElement(React.Fragment, null,
                        React.createElement("div", { className: css(styles$n.selectToggleWrapper) },
                            toggleIcon && React.createElement("span", { className: css(styles$n.selectToggleIcon) }, toggleIcon),
                            React.createElement("span", { className: css(styles$n.selectToggleText) }, this.getDisplay(selections[0], 'node') || placeholderText || childPlaceholderText)),
                        hasOnClear && hasAnySelections && clearBtn)),
                    variant === exports.SelectVariant.checkbox && !customContent && (React.createElement(React.Fragment, null,
                        React.createElement("div", { className: css(styles$n.selectToggleWrapper) },
                            toggleIcon && React.createElement("span", { className: css(styles$n.selectToggleIcon) }, toggleIcon),
                            React.createElement("span", { className: css(styles$n.selectToggleText) }, placeholderText),
                            !isCheckboxSelectionBadgeHidden && hasAnySelections && (React.createElement("div", { className: css(styles$n.selectToggleBadge) },
                                React.createElement("span", { className: css(badgeStyles.badge, badgeStyles.modifiers.read) }, this.generateSelectedBadge())))),
                        hasOnClear && hasAnySelections && clearBtn)),
                    variant === exports.SelectVariant.typeahead && !customContent && (React.createElement(React.Fragment, null,
                        React.createElement("div", { className: css(styles$n.selectToggleWrapper) },
                            toggleIcon && React.createElement("span", { className: css(styles$n.selectToggleIcon) }, toggleIcon),
                            React.createElement("input", { className: css(formStyles.formControl, styles$n.selectToggleTypeahead), "aria-activedescendant": typeaheadActiveChild && typeaheadActiveChild.id, id: `${selectToggleId}-select-typeahead`, "aria-label": typeAheadAriaLabel, placeholder: placeholderText, value: typeaheadInputValue !== null
                                    ? typeaheadInputValue
                                    : this.getDisplay(selections[0], 'text') || '', type: "text", onClick: this.onClick, onChange: this.onChange, onFocus: this.handleFocus, autoComplete: "off", disabled: isDisabled })),
                        (selections[0] || typeaheadInputValue) && clearBtn)),
                    variant === exports.SelectVariant.typeaheadMulti && !customContent && (React.createElement(React.Fragment, null,
                        React.createElement("div", { className: css(styles$n.selectToggleWrapper) },
                            toggleIcon && React.createElement("span", { className: css(styles$n.selectToggleIcon) }, toggleIcon),
                            selections && Array.isArray(selections) && selections.length > 0 && selectedChips,
                            React.createElement("input", { className: css(formStyles.formControl, styles$n.selectToggleTypeahead), "aria-activedescendant": typeaheadActiveChild && typeaheadActiveChild.id, id: `${selectToggleId}-select-multi-typeahead-typeahead`, "aria-label": typeAheadAriaLabel, placeholder: placeholderText, value: typeaheadInputValue !== null ? typeaheadInputValue : '', type: "text", onChange: this.onChange, onClick: this.onClick, onFocus: this.handleFocus, autoComplete: "off", disabled: isDisabled })),
                        ((selections && selections.length > 0) || typeaheadInputValue) && clearBtn))),
                isOpen && menuAppendTo === 'inline' && menuContainer));
            const getParentElement = () => {
                if (this.parentRef && this.parentRef.current) {
                    return this.parentRef.current.parentElement;
                }
                return null;
            };
            return (React.createElement(GenerateId, null, randomId => (React.createElement(SelectContext.Provider, { value: { onSelect, onClose: this.onClose, variant, inputIdPrefix: inputIdPrefix || randomId } }, menuAppendTo === 'inline' ? (mainContainer) : (React.createElement(Popper, { trigger: mainContainer, popper: popperContainer, direction: direction, appendTo: menuAppendTo === 'parent' ? getParentElement() : menuAppendTo, isVisible: isOpen }))))));
        }
    }
    Select.displayName = 'Select';
    Select.defaultProps = {
        children: [],
        className: '',
        direction: exports.SelectDirection.down,
        toggleId: null,
        isOpen: false,
        isGrouped: false,
        isPlain: false,
        isDisabled: false,
        isCreatable: false,
        'aria-label': '',
        'aria-labelledby': '',
        typeAheadAriaLabel: '',
        clearSelectionsAriaLabel: 'Clear all',
        toggleAriaLabel: 'Options menu',
        removeSelectionAriaLabel: 'Remove',
        selections: [],
        createText: 'Create',
        placeholderText: '',
        noResultsFoundText: 'No results found',
        variant: exports.SelectVariant.single,
        width: '',
        onClear: () => undefined,
        onCreateOption: () => undefined,
        toggleIcon: null,
        onFilter: null,
        customContent: null,
        hasInlineFilter: false,
        inlineFilterPlaceholderText: null,
        customBadgeText: null,
        inputIdPrefix: '',
        menuAppendTo: 'inline',
        ouiaSafe: true
    };

    const SelectGroup = (_a) => {
        var { children = [], className = '', label = '', titleId = '' } = _a, props = __rest(_a, ["children", "className", "label", "titleId"]);
        return (React.createElement("div", Object.assign({}, props, { className: css(styles$n.selectMenuGroup, className) }),
            React.createElement("div", { className: css(styles$n.selectMenuGroupTitle), id: titleId, "aria-hidden": true }, label),
            children));
    };
    SelectGroup.displayName = 'SelectGroup';

    const DataListItem = (_a) => {
        var { isExpanded = false, className = '', id = '', 'aria-labelledby': ariaLabelledBy, children } = _a, props = __rest(_a, ["isExpanded", "className", "id", 'aria-labelledby', "children"]);
        return (React.createElement(DataListContext.Consumer, null, ({ isSelectable, selectedDataListItemId, updateSelectedDataListItem }) => {
            const selectDataListItem = (event) => {
                let target = event.target;
                while (event.currentTarget !== target) {
                    if (('onclick' in target && target.onclick) ||
                        target.parentNode.classList.contains(styles$m.dataListItemAction) ||
                        target.parentNode.classList.contains(styles$m.dataListItemControl)) {
                        // check other event handlers are not present.
                        return;
                    }
                    else {
                        target = target.parentNode;
                    }
                }
                updateSelectedDataListItem(id);
            };
            const onKeyDown = (event) => {
                if (event.key === KeyTypes.Enter) {
                    updateSelectedDataListItem(id);
                }
            };
            return (React.createElement("li", Object.assign({ id: id, className: css(styles$m.dataListItem, isExpanded && styles$m.modifiers.expanded, isSelectable && styles$m.modifiers.selectable, selectedDataListItemId && selectedDataListItemId === id && styles$m.modifiers.selected, className), "aria-labelledby": ariaLabelledBy }, (isSelectable && { tabIndex: 0, onClick: selectDataListItem, onKeyDown }), (isSelectable && selectedDataListItemId === id && { 'aria-selected': true }), props), React.Children.map(children, child => React.isValidElement(child) &&
                React.cloneElement(child, {
                    rowid: ariaLabelledBy
                }))));
        }));
    };
    DataListItem.displayName = 'DataListItem';

    const DataListItemCells = (_a) => {
        var { className = '', dataListCells, 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        rowid = '' } = _a, props = __rest(_a, ["className", "dataListCells", "rowid"]);
        return (React.createElement("div", Object.assign({ className: css(styles$m.dataListItemContent, className) }, props), dataListCells));
    };
    DataListItemCells.displayName = 'DataListItemCells';

    const DataListItemRow = (_a) => {
        var { children, className = '', rowid = '' } = _a, props = __rest(_a, ["children", "className", "rowid"]);
        return (React.createElement("div", Object.assign({ className: css(styles$m.dataListItemRow, className) }, props), React.Children.map(children, child => React.isValidElement(child) &&
            React.cloneElement(child, {
                rowid
            }))));
    };
    DataListItemRow.displayName = 'DataListItemRow';

    const DataListToggle = (_a) => {
        var { className = '', isExpanded = false, 'aria-controls': ariaControls = '', 'aria-label': ariaLabel = 'Details', rowid = '', id } = _a, props = __rest(_a, ["className", "isExpanded", 'aria-controls', 'aria-label', "rowid", "id"]);
        return (React.createElement("div", Object.assign({ className: css(styles$m.dataListItemControl, className) }, props),
            React.createElement("div", { className: css(styles$m.dataListToggle) },
                React.createElement(Button, { id: id, variant: exports.ButtonVariant.plain, "aria-controls": ariaControls !== '' && ariaControls, "aria-label": ariaLabel, "aria-labelledby": ariaLabel !== 'Details' ? null : `${rowid} ${id}`, "aria-expanded": isExpanded },
                    React.createElement("div", { className: css(styles$m.dataListToggleIcon) },
                        React.createElement(AngleRightIcon, null))))));
    };
    DataListToggle.displayName = 'DataListToggle';

    const DataListContent = (_a) => {
        var { className = '', children = null, id = '', isHidden = false, 'aria-label': ariaLabel, hasNoPadding = false, 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        rowid = '' } = _a, props = __rest(_a, ["className", "children", "id", "isHidden", 'aria-label', "hasNoPadding", "rowid"]);
        return (React.createElement("section", Object.assign({ id: id, className: css(styles$m.dataListExpandableContent, className), hidden: isHidden, "aria-label": ariaLabel }, props),
            React.createElement("div", { className: css(styles$m.dataListExpandableContentBody, hasNoPadding && styles$m.modifiers.noPadding) }, children)));
    };
    DataListContent.displayName = 'DataListContent';

    var drawer = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "drawer": "pf-c-drawer",
      "drawerActions": "pf-c-drawer__actions",
      "drawerBody": "pf-c-drawer__body",
      "drawerClose": "pf-c-drawer__close",
      "drawerContent": "pf-c-drawer__content",
      "drawerHead": "pf-c-drawer__head",
      "drawerMain": "pf-c-drawer__main",
      "drawerPanel": "pf-c-drawer__panel",
      "drawerSection": "pf-c-drawer__section",
      "modifiers": {
        "inline": "pf-m-inline",
        "noBorder": "pf-m-no-border",
        "static": "pf-m-static",
        "noBackground": "pf-m-no-background",
        "noPadding": "pf-m-no-padding",
        "padding": "pf-m-padding",
        "expanded": "pf-m-expanded",
        "panelLeft": "pf-m-panel-left",
        "width_25": "pf-m-width-25",
        "width_33": "pf-m-width-33",
        "width_50": "pf-m-width-50",
        "width_66": "pf-m-width-66",
        "width_75": "pf-m-width-75",
        "width_100": "pf-m-width-100",
        "width_25OnLg": "pf-m-width-25-on-lg",
        "width_33OnLg": "pf-m-width-33-on-lg",
        "width_50OnLg": "pf-m-width-50-on-lg",
        "width_66OnLg": "pf-m-width-66-on-lg",
        "width_75OnLg": "pf-m-width-75-on-lg",
        "width_100OnLg": "pf-m-width-100-on-lg",
        "width_25OnXl": "pf-m-width-25-on-xl",
        "width_33OnXl": "pf-m-width-33-on-xl",
        "width_50OnXl": "pf-m-width-50-on-xl",
        "width_66OnXl": "pf-m-width-66-on-xl",
        "width_75OnXl": "pf-m-width-75-on-xl",
        "width_100OnXl": "pf-m-width-100-on-xl",
        "width_25On_2xl": "pf-m-width-25-on-2xl",
        "width_33On_2xl": "pf-m-width-33-on-2xl",
        "width_50On_2xl": "pf-m-width-50-on-2xl",
        "width_66On_2xl": "pf-m-width-66-on-2xl",
        "width_75On_2xl": "pf-m-width-75-on-2xl",
        "width_100On_2xl": "pf-m-width-100-on-2xl",
        "inlineOnLg": "pf-m-inline-on-lg",
        "staticOnLg": "pf-m-static-on-lg",
        "inlineOnXl": "pf-m-inline-on-xl",
        "staticOnXl": "pf-m-static-on-xl",
        "inlineOn_2xl": "pf-m-inline-on-2xl",
        "staticOn_2xl": "pf-m-static-on-2xl"
      },
      "pageMain": "pf-c-page__main"
    };
    });

    var styles$p = unwrapExports(drawer);

    const DrawerContext = React.createContext({
        isExpanded: false,
        isStatic: false,
        onExpand: () => { }
    });
    const Drawer = (_a) => {
        var { className = '', children, isExpanded = false, isInline = false, isStatic = false, position = 'right', onExpand = () => { } } = _a, props = __rest(_a, ["className", "children", "isExpanded", "isInline", "isStatic", "position", "onExpand"]);
        return (React.createElement(DrawerContext.Provider, { value: { isExpanded, isStatic, onExpand } },
            React.createElement("div", Object.assign({ className: css(styles$p.drawer, isExpanded && styles$p.modifiers.expanded, isInline && styles$p.modifiers.inline, isStatic && styles$p.modifiers.static, position === 'left' && styles$p.modifiers.panelLeft, className) }, props), children)));
    };
    Drawer.displayName = 'Drawer';

    const DrawerActions = (_a) => {
        var { 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        className = '', children } = _a, props = __rest(_a, ["className", "children"]);
        return (React.createElement("div", Object.assign({ className: css(styles$p.drawerActions, className) }, props), children));
    };
    DrawerActions.displayName = 'DrawerActions';

    const DrawerCloseButton = (_a) => {
        var { 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        className = '', onClose = () => undefined, 'aria-label': ariaLabel = 'Close drawer panel' } = _a, props = __rest(_a, ["className", "onClose", 'aria-label']);
        return (React.createElement("div", Object.assign({ className: css(styles$p.drawerClose, className) }, props),
            React.createElement(Button, { variant: "plain", onClick: onClose, "aria-label": ariaLabel },
                React.createElement(TimesIcon, null))));
    };
    DrawerCloseButton.displayName = 'DrawerCloseButton';

    const DrawerMain = (_a) => {
        var { 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        className = '', children } = _a, props = __rest(_a, ["className", "children"]);
        return (React.createElement("div", Object.assign({ className: css(styles$p.drawerMain, className) }, props), children));
    };
    DrawerMain.displayName = 'DrawerMain';

    const DrawerContent = (_a) => {
        var { 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        className = '', children, panelContent } = _a, props = __rest(_a, ["className", "children", "panelContent"]);
        return (React.createElement(DrawerMain, null,
            React.createElement("div", Object.assign({ className: css(styles$p.drawerContent, className) }, props), children),
            panelContent));
    };
    DrawerContent.displayName = 'DrawerContent';

    const DrawerContentBody = (_a) => {
        var { 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        className = '', children, hasPadding = false } = _a, props = __rest(_a, ["className", "children", "hasPadding"]);
        return (React.createElement("div", Object.assign({ className: css(styles$p.drawerBody, hasPadding && styles$p.modifiers.padding, className) }, props), children));
    };
    DrawerContentBody.displayName = 'DrawerContentBody';

    const DrawerPanelBody = (_a) => {
        var { 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        className = '', children, hasNoPadding = false } = _a, props = __rest(_a, ["className", "children", "hasNoPadding"]);
        return (React.createElement("div", Object.assign({ className: css(styles$p.drawerBody, hasNoPadding && styles$p.modifiers.noPadding, className) }, props), children));
    };
    DrawerPanelBody.displayName = 'DrawerPanelBody';

    const DrawerHead = (_a) => {
        var { 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        className = '', children, hasNoPadding = false } = _a, props = __rest(_a, ["className", "children", "hasNoPadding"]);
        return (React.createElement(DrawerPanelBody, { hasNoPadding: hasNoPadding },
            React.createElement("div", Object.assign({ className: css(styles$p.drawerHead, className) }, props), children)));
    };
    DrawerHead.displayName = 'DrawerHead';

    const DrawerPanelContent = (_a) => {
        var { className = '', children, hasNoBorder = false, widths } = _a, props = __rest(_a, ["className", "children", "hasNoBorder", "widths"]);
        return (React.createElement(DrawerContext.Consumer, null, ({ isExpanded, isStatic, onExpand }) => {
            const hidden = isStatic ? false : !isExpanded;
            return (React.createElement("div", Object.assign({ className: css(styles$p.drawerPanel, hasNoBorder && styles$p.modifiers.noBorder, formatBreakpointMods(widths, styles$p), className), onTransitionEnd: ev => {
                    if (!hidden && ev.nativeEvent.propertyName === 'transform') {
                        onExpand();
                    }
                }, hidden: hidden }, props), !hidden && children));
        }));
    };
    DrawerPanelContent.displayName = 'DrawerPanelContent';

    const DrawerSection = (_a) => {
        var { 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        className = '', children } = _a, props = __rest(_a, ["className", "children"]);
        return (React.createElement("div", Object.assign({ className: css(styles$p.drawerSection, className) }, props), children));
    };
    DrawerSection.displayName = 'DrawerSection';

    var emptyState = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "button": "pf-c-button",
      "emptyState": "pf-c-empty-state",
      "emptyStateBody": "pf-c-empty-state__body",
      "emptyStateContent": "pf-c-empty-state__content",
      "emptyStateIcon": "pf-c-empty-state__icon",
      "emptyStatePrimary": "pf-c-empty-state__primary",
      "emptyStateSecondary": "pf-c-empty-state__secondary",
      "modifiers": {
        "sm": "pf-m-sm",
        "lg": "pf-m-lg",
        "xl": "pf-m-xl",
        "fullHeight": "pf-m-full-height",
        "primary": "pf-m-primary",
        "overpassFont": "pf-m-overpass-font"
      },
      "title": "pf-c-title"
    };
    });

    var styles$q = unwrapExports(emptyState);

    (function (EmptyStateVariant) {
        EmptyStateVariant["small"] = "small";
        EmptyStateVariant["large"] = "large";
        EmptyStateVariant["xl"] = "xl";
        EmptyStateVariant["full"] = "full";
    })(exports.EmptyStateVariant || (exports.EmptyStateVariant = {}));
    const EmptyState = (_a) => {
        var { children, className = '', variant = exports.EmptyStateVariant.full, isFullHeight } = _a, props = __rest(_a, ["children", "className", "variant", "isFullHeight"]);
        return (React.createElement("div", Object.assign({ className: css(styles$q.emptyState, variant === 'small' && styles$q.modifiers.sm, variant === 'large' && styles$q.modifiers.lg, variant === 'xl' && styles$q.modifiers.xl, isFullHeight && styles$q.modifiers.fullHeight, className) }, props),
            React.createElement("div", { className: css(styles$q.emptyStateContent) }, children)));
    };
    EmptyState.displayName = 'EmptyState';

    const EmptyStateBody = (_a) => {
        var { children, className = '' } = _a, props = __rest(_a, ["children", "className"]);
        return (React.createElement("div", Object.assign({ className: css(styles$q.emptyStateBody, className) }, props), children));
    };
    EmptyStateBody.displayName = 'EmptyStateBody';

    const EmptyStateIcon = (_a) => {
        var { className = '', icon: IconComponent, component: AnyComponent, variant = 'icon' } = _a, props = __rest(_a, ["className", "icon", "component", "variant"]);
        const classNames = css(styles$q.emptyStateIcon, className);
        return variant === 'icon' ? (React.createElement(IconComponent, Object.assign({ className: classNames }, props, { "aria-hidden": "true" }))) : (React.createElement("div", { className: classNames },
            React.createElement(AnyComponent, null)));
    };
    EmptyStateIcon.displayName = 'EmptyStateIcon';

    const EmptyStateSecondaryActions = (_a) => {
        var { children = null, className = '' } = _a, props = __rest(_a, ["children", "className"]);
        return (React.createElement("div", Object.assign({ className: css(styles$q.emptyStateSecondary, className) }, props), children));
    };
    EmptyStateSecondaryActions.displayName = 'EmptyStateSecondaryActions';

    const EmptyStatePrimary = (_a) => {
        var { children, className = '' } = _a, props = __rest(_a, ["children", "className"]);
        return (React.createElement("div", Object.assign({ className: css(styles$q.emptyStatePrimary, className) }, props), children));
    };
    EmptyStatePrimary.displayName = 'EmptyStatePrimary';

    var expandableSection = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "expandableSection": "pf-c-expandable-section",
      "expandableSectionContent": "pf-c-expandable-section__content",
      "expandableSectionToggle": "pf-c-expandable-section__toggle",
      "expandableSectionToggleIcon": "pf-c-expandable-section__toggle-icon",
      "expandableSectionToggleText": "pf-c-expandable-section__toggle-text",
      "modifiers": {
        "expanded": "pf-m-expanded",
        "active": "pf-m-active",
        "overpassFont": "pf-m-overpass-font"
      }
    };
    });

    var styles$r = unwrapExports(expandableSection);

    class ExpandableSection extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                isExpanded: props.isExpanded
            };
        }
        calculateToggleText(toggleText, toggleTextExpanded, toggleTextCollapsed, propOrStateIsExpanded) {
            if (propOrStateIsExpanded && toggleTextExpanded !== '') {
                return toggleTextExpanded;
            }
            if (!propOrStateIsExpanded && toggleTextCollapsed !== '') {
                return toggleTextCollapsed;
            }
            return toggleText;
        }
        render() {
            const _a = this.props, { onToggle: onToggleProp, 
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            isActive, className, toggleText, toggleTextExpanded, toggleTextCollapsed, children, isExpanded } = _a, props = __rest(_a, ["onToggle", "isActive", "className", "toggleText", "toggleTextExpanded", "toggleTextCollapsed", "children", "isExpanded"]);
            let onToggle = onToggleProp;
            let propOrStateIsExpanded = isExpanded;
            // uncontrolled
            if (isExpanded === undefined) {
                propOrStateIsExpanded = this.state.isExpanded;
                onToggle = isOpen => {
                    this.setState({ isExpanded: isOpen }, () => onToggleProp(this.state.isExpanded));
                };
            }
            const computedToggleText = this.calculateToggleText(toggleText, toggleTextExpanded, toggleTextCollapsed, propOrStateIsExpanded);
            return (React.createElement("div", Object.assign({}, props, { className: css(styles$r.expandableSection, propOrStateIsExpanded && styles$r.modifiers.expanded, isActive && styles$r.modifiers.active, className) }),
                React.createElement("button", { className: css(styles$r.expandableSectionToggle), type: "button", "aria-expanded": propOrStateIsExpanded, onClick: () => onToggle(!propOrStateIsExpanded) },
                    React.createElement("span", { className: css(styles$r.expandableSectionToggleIcon) },
                        React.createElement(AngleRightIcon, { "aria-hidden": true })),
                    React.createElement("span", { className: css(styles$r.expandableSectionToggleText) }, computedToggleText)),
                React.createElement("div", { className: css(styles$r.expandableSectionContent), hidden: !propOrStateIsExpanded }, children)));
        }
    }
    ExpandableSection.displayName = 'ExpandableSection';
    ExpandableSection.defaultProps = {
        className: '',
        toggleText: '',
        toggleTextExpanded: '',
        toggleTextCollapsed: '',
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        onToggle: (isExpanded) => undefined,
        isActive: false
    };

    var fileUpload = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "button": "pf-c-button",
      "fileUpload": "pf-c-file-upload",
      "fileUploadFileDetails": "pf-c-file-upload__file-details",
      "fileUploadFileDetailsSpinner": "pf-c-file-upload__file-details-spinner",
      "fileUploadFileSelect": "pf-c-file-upload__file-select",
      "formControl": "pf-c-form-control",
      "modifiers": {
        "dragHover": "pf-m-drag-hover",
        "loading": "pf-m-loading",
        "control": "pf-m-control"
      }
    };
    });

    var styles$s = unwrapExports(fileUpload);

    var spinner = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "modifiers": {
        "sm": "pf-m-sm",
        "md": "pf-m-md",
        "lg": "pf-m-lg",
        "xl": "pf-m-xl"
      },
      "spinner": "pf-c-spinner",
      "spinnerClipper": "pf-c-spinner__clipper",
      "spinnerLeadBall": "pf-c-spinner__lead-ball",
      "spinnerTailBall": "pf-c-spinner__tail-ball"
    };
    });

    var styles$t = unwrapExports(spinner);

    (function (spinnerSize) {
        spinnerSize["sm"] = "sm";
        spinnerSize["md"] = "md";
        spinnerSize["lg"] = "lg";
        spinnerSize["xl"] = "xl";
    })(exports.spinnerSize || (exports.spinnerSize = {}));
    const Spinner = (_a) => {
        var { 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        className = '', size = 'xl', 'aria-valuetext': ariaValueText = 'Loading...' } = _a, props = __rest(_a, ["className", "size", 'aria-valuetext']);
        return (React.createElement("span", Object.assign({ className: css(styles$t.spinner, styles$t.modifiers[size], className), role: "progressbar", "aria-valuetext": ariaValueText }, props),
            React.createElement("span", { className: css(styles$t.spinnerClipper) }),
            React.createElement("span", { className: css(styles$t.spinnerLeadBall) }),
            React.createElement("span", { className: css(styles$t.spinnerTailBall) })));
    };
    Spinner.displayName = 'Spinner';

    var fileReaderType;
    (function (fileReaderType) {
        fileReaderType["text"] = "text";
        fileReaderType["dataURL"] = "dataURL";
    })(fileReaderType || (fileReaderType = {}));
    /**
     * Read a file using the FileReader API, either as a plain text string or as a DataURL string.
     * Returns a promise which will resolve with the file contents as a string or reject with a DOMException.
     *
     * @param {File} fileHandle - File object to read
     * @param {fileReaderType} type - How to read it
     */
    function readFile(fileHandle, type) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.onload = () => resolve(reader.result);
            reader.onerror = () => reject(reader.error);
            if (type === fileReaderType.text) {
                reader.readAsText(fileHandle);
            }
            else if (type === fileReaderType.dataURL) {
                reader.readAsDataURL(fileHandle);
            }
            else {
                reject('unknown type');
            }
        });
    }

    const FileUploadField = (_a) => {
        var { id, type, value = '', filename = '', onChange = () => { }, onBrowseButtonClick = () => { }, onClearButtonClick = () => { }, className = '', isDisabled = false, isReadOnly = false, isLoading = false, spinnerAriaValueText, isRequired = false, isDragActive = false, validated = 'default', 'aria-label': ariaLabel = 'File upload', filenamePlaceholder = 'Drag a file here or browse to upload', filenameAriaLabel = filename ? 'Read only filename' : filenamePlaceholder, browseButtonText = 'Browse...', clearButtonText = 'Clear', isClearButtonDisabled = !filename && !value, containerRef = null, allowEditingUploadedText = false, hideDefaultPreview = false, children = null } = _a, props = __rest(_a, ["id", "type", "value", "filename", "onChange", "onBrowseButtonClick", "onClearButtonClick", "className", "isDisabled", "isReadOnly", "isLoading", "spinnerAriaValueText", "isRequired", "isDragActive", "validated", 'aria-label', "filenamePlaceholder", "filenameAriaLabel", "browseButtonText", "clearButtonText", "isClearButtonDisabled", "containerRef", "allowEditingUploadedText", "hideDefaultPreview", "children"]);
        const onTextAreaChange = (newValue, event) => {
            onChange(newValue, filename, event);
        };
        return (React.createElement("div", Object.assign({ className: css(styles$s.fileUpload, isDragActive && styles$s.modifiers.dragHover, isLoading && styles$s.modifiers.loading, className), ref: containerRef }, props),
            React.createElement("div", { className: styles$s.fileUploadFileSelect },
                React.createElement(InputGroup, null,
                    React.createElement(TextInput, { isReadOnly // Always read-only regardless of isReadOnly prop (which is just for the TextArea)
                        : true, isDisabled: isDisabled, id: `${id}-filename`, name: `${id}-filename`, "aria-label": filenameAriaLabel, placeholder: filenamePlaceholder, "aria-describedby": `${id}-browse-button`, value: filename }),
                    React.createElement(Button, { id: `${id}-browse-button`, variant: exports.ButtonVariant.control, onClick: onBrowseButtonClick, isDisabled: isDisabled }, browseButtonText),
                    React.createElement(Button, { variant: exports.ButtonVariant.control, isDisabled: isDisabled || isClearButtonDisabled, onClick: onClearButtonClick }, clearButtonText))),
            React.createElement("div", { className: styles$s.fileUploadFileDetails },
                !hideDefaultPreview && type === fileReaderType.text && (React.createElement(TextArea, { readOnly: isReadOnly || (!!filename && !allowEditingUploadedText), disabled: isDisabled, isRequired: isRequired, resizeOrientation: exports.TextAreResizeOrientation.vertical, validated: validated, id: id, name: id, "aria-label": ariaLabel, value: value, onChange: onTextAreaChange })),
                isLoading && (React.createElement("div", { className: styles$s.fileUploadFileDetailsSpinner },
                    React.createElement(Spinner, { size: exports.spinnerSize.lg, "aria-valuetext": spinnerAriaValueText })))),
            children));
    };
    FileUploadField.displayName = 'FileUploadField';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */

    function __awaiter$1(thisArg, _arguments, P, generator) {
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator$1(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __read$1(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread$1() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read$1(arguments[i]));
        return ar;
    }

    var COMMON_MIME_TYPES = new Map([
        ['avi', 'video/avi'],
        ['gif', 'image/gif'],
        ['ico', 'image/x-icon'],
        ['jpeg', 'image/jpeg'],
        ['jpg', 'image/jpeg'],
        ['mkv', 'video/x-matroska'],
        ['mov', 'video/quicktime'],
        ['mp4', 'video/mp4'],
        ['pdf', 'application/pdf'],
        ['png', 'image/png'],
        ['zip', 'application/zip'],
        ['doc', 'application/msword'],
        ['docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document']
    ]);
    function toFileWithPath(file, path) {
        var f = withMimeType(file);
        if (typeof f.path !== 'string') { // on electron, path is already set to the absolute path
            var webkitRelativePath = file.webkitRelativePath;
            Object.defineProperty(f, 'path', {
                value: typeof path === 'string'
                    ? path
                    // If <input webkitdirectory> is set,
                    // the File will have a {webkitRelativePath} property
                    // https://developer.mozilla.org/en-US/docs/Web/API/HTMLInputElement/webkitdirectory
                    : typeof webkitRelativePath === 'string' && webkitRelativePath.length > 0
                        ? webkitRelativePath
                        : file.name,
                writable: false,
                configurable: false,
                enumerable: true
            });
        }
        return f;
    }
    function withMimeType(file) {
        var name = file.name;
        var hasExtension = name && name.lastIndexOf('.') !== -1;
        if (hasExtension && !file.type) {
            var ext = name.split('.')
                .pop().toLowerCase();
            var type = COMMON_MIME_TYPES.get(ext);
            if (type) {
                Object.defineProperty(file, 'type', {
                    value: type,
                    writable: false,
                    configurable: false,
                    enumerable: true
                });
            }
        }
        return file;
    }

    var FILES_TO_IGNORE = [
        // Thumbnail cache files for macOS and Windows
        '.DS_Store',
        'Thumbs.db' // Windows
    ];
    /**
     * Convert a DragEvent's DataTrasfer object to a list of File objects
     * NOTE: If some of the items are folders,
     * everything will be flattened and placed in the same list but the paths will be kept as a {path} property.
     * @param evt
     */
    function fromEvent(evt) {
        return __awaiter$1(this, void 0, void 0, function () {
            return __generator$1(this, function (_a) {
                return [2 /*return*/, isDragEvt(evt) && evt.dataTransfer
                        ? getDataTransferFiles(evt.dataTransfer, evt.type)
                        : getInputFiles(evt)];
            });
        });
    }
    function isDragEvt(value) {
        return !!value.dataTransfer;
    }
    function getInputFiles(evt) {
        var files = isInput$1(evt.target)
            ? evt.target.files
                ? fromList(evt.target.files)
                : []
            : [];
        return files.map(function (file) { return toFileWithPath(file); });
    }
    function isInput$1(value) {
        return value !== null;
    }
    function getDataTransferFiles(dt, type) {
        return __awaiter$1(this, void 0, void 0, function () {
            var items, files;
            return __generator$1(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!dt.items) return [3 /*break*/, 2];
                        items = fromList(dt.items)
                            .filter(function (item) { return item.kind === 'file'; });
                        // According to https://html.spec.whatwg.org/multipage/dnd.html#dndevents,
                        // only 'dragstart' and 'drop' has access to the data (source node)
                        if (type !== 'drop') {
                            return [2 /*return*/, items];
                        }
                        return [4 /*yield*/, Promise.all(items.map(toFilePromises))];
                    case 1:
                        files = _a.sent();
                        return [2 /*return*/, noIgnoredFiles(flatten(files))];
                    case 2: return [2 /*return*/, noIgnoredFiles(fromList(dt.files)
                            .map(function (file) { return toFileWithPath(file); }))];
                }
            });
        });
    }
    function noIgnoredFiles(files) {
        return files.filter(function (file) { return FILES_TO_IGNORE.indexOf(file.name) === -1; });
    }
    // IE11 does not support Array.from()
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/from#Browser_compatibility
    // https://developer.mozilla.org/en-US/docs/Web/API/FileList
    // https://developer.mozilla.org/en-US/docs/Web/API/DataTransferItemList
    function fromList(items) {
        var files = [];
        // tslint:disable: prefer-for-of
        for (var i = 0; i < items.length; i++) {
            var file = items[i];
            files.push(file);
        }
        return files;
    }
    // https://developer.mozilla.org/en-US/docs/Web/API/DataTransferItem
    function toFilePromises(item) {
        if (typeof item.webkitGetAsEntry !== 'function') {
            return fromDataTransferItem(item);
        }
        var entry = item.webkitGetAsEntry();
        // Safari supports dropping an image node from a different window and can be retrieved using
        // the DataTransferItem.getAsFile() API
        // NOTE: FileSystemEntry.file() throws if trying to get the file
        if (entry && entry.isDirectory) {
            return fromDirEntry(entry);
        }
        return fromDataTransferItem(item);
    }
    function flatten(items) {
        return items.reduce(function (acc, files) { return __spread$1(acc, (Array.isArray(files) ? flatten(files) : [files])); }, []);
    }
    function fromDataTransferItem(item) {
        var file = item.getAsFile();
        if (!file) {
            return Promise.reject(item + " is not a File");
        }
        var fwp = toFileWithPath(file);
        return Promise.resolve(fwp);
    }
    // https://developer.mozilla.org/en-US/docs/Web/API/FileSystemEntry
    function fromEntry(entry) {
        return __awaiter$1(this, void 0, void 0, function () {
            return __generator$1(this, function (_a) {
                return [2 /*return*/, entry.isDirectory ? fromDirEntry(entry) : fromFileEntry(entry)];
            });
        });
    }
    // https://developer.mozilla.org/en-US/docs/Web/API/FileSystemDirectoryEntry
    function fromDirEntry(entry) {
        var reader = entry.createReader();
        return new Promise(function (resolve, reject) {
            var entries = [];
            function readEntries() {
                var _this = this;
                // https://developer.mozilla.org/en-US/docs/Web/API/FileSystemDirectoryEntry/createReader
                // https://developer.mozilla.org/en-US/docs/Web/API/FileSystemDirectoryReader/readEntries
                reader.readEntries(function (batch) { return __awaiter$1(_this, void 0, void 0, function () {
                    var files, err_1, items;
                    return __generator$1(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (!!batch.length) return [3 /*break*/, 5];
                                _a.label = 1;
                            case 1:
                                _a.trys.push([1, 3, , 4]);
                                return [4 /*yield*/, Promise.all(entries)];
                            case 2:
                                files = _a.sent();
                                resolve(files);
                                return [3 /*break*/, 4];
                            case 3:
                                err_1 = _a.sent();
                                reject(err_1);
                                return [3 /*break*/, 4];
                            case 4: return [3 /*break*/, 6];
                            case 5:
                                items = Promise.all(batch.map(fromEntry));
                                entries.push(items);
                                // Continue reading
                                readEntries();
                                _a.label = 6;
                            case 6: return [2 /*return*/];
                        }
                    });
                }); }, function (err) {
                    reject(err);
                });
            }
            readEntries();
        });
    }
    // https://developer.mozilla.org/en-US/docs/Web/API/FileSystemFileEntry
    function fromFileEntry(entry) {
        return __awaiter$1(this, void 0, void 0, function () {
            return __generator$1(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        entry.file(function (file) {
                            var fwp = toFileWithPath(file, entry.fullPath);
                            resolve(fwp);
                        }, function (err) {
                            reject(err);
                        });
                    })];
            });
        });
    }

    var reactIs_development = createCommonjsModule(function (module, exports) {



    {
      (function() {

    Object.defineProperty(exports, '__esModule', { value: true });

    // The Symbol used to tag the ReactElement-like types. If there is no native Symbol
    // nor polyfill, then a plain number is used for performance.
    var hasSymbol = typeof Symbol === 'function' && Symbol.for;
    var REACT_ELEMENT_TYPE = hasSymbol ? Symbol.for('react.element') : 0xeac7;
    var REACT_PORTAL_TYPE = hasSymbol ? Symbol.for('react.portal') : 0xeaca;
    var REACT_FRAGMENT_TYPE = hasSymbol ? Symbol.for('react.fragment') : 0xeacb;
    var REACT_STRICT_MODE_TYPE = hasSymbol ? Symbol.for('react.strict_mode') : 0xeacc;
    var REACT_PROFILER_TYPE = hasSymbol ? Symbol.for('react.profiler') : 0xead2;
    var REACT_PROVIDER_TYPE = hasSymbol ? Symbol.for('react.provider') : 0xeacd;
    var REACT_CONTEXT_TYPE = hasSymbol ? Symbol.for('react.context') : 0xeace; // TODO: We don't use AsyncMode or ConcurrentMode anymore. They were temporary
    // (unstable) APIs that have been removed. Can we remove the symbols?

    var REACT_ASYNC_MODE_TYPE = hasSymbol ? Symbol.for('react.async_mode') : 0xeacf;
    var REACT_CONCURRENT_MODE_TYPE = hasSymbol ? Symbol.for('react.concurrent_mode') : 0xeacf;
    var REACT_FORWARD_REF_TYPE = hasSymbol ? Symbol.for('react.forward_ref') : 0xead0;
    var REACT_SUSPENSE_TYPE = hasSymbol ? Symbol.for('react.suspense') : 0xead1;
    var REACT_SUSPENSE_LIST_TYPE = hasSymbol ? Symbol.for('react.suspense_list') : 0xead8;
    var REACT_MEMO_TYPE = hasSymbol ? Symbol.for('react.memo') : 0xead3;
    var REACT_LAZY_TYPE = hasSymbol ? Symbol.for('react.lazy') : 0xead4;
    var REACT_FUNDAMENTAL_TYPE = hasSymbol ? Symbol.for('react.fundamental') : 0xead5;
    var REACT_RESPONDER_TYPE = hasSymbol ? Symbol.for('react.responder') : 0xead6;
    var REACT_SCOPE_TYPE = hasSymbol ? Symbol.for('react.scope') : 0xead7;

    function isValidElementType(type) {
      return typeof type === 'string' || typeof type === 'function' || // Note: its typeof might be other than 'symbol' or 'number' if it's a polyfill.
      type === REACT_FRAGMENT_TYPE || type === REACT_CONCURRENT_MODE_TYPE || type === REACT_PROFILER_TYPE || type === REACT_STRICT_MODE_TYPE || type === REACT_SUSPENSE_TYPE || type === REACT_SUSPENSE_LIST_TYPE || typeof type === 'object' && type !== null && (type.$$typeof === REACT_LAZY_TYPE || type.$$typeof === REACT_MEMO_TYPE || type.$$typeof === REACT_PROVIDER_TYPE || type.$$typeof === REACT_CONTEXT_TYPE || type.$$typeof === REACT_FORWARD_REF_TYPE || type.$$typeof === REACT_FUNDAMENTAL_TYPE || type.$$typeof === REACT_RESPONDER_TYPE || type.$$typeof === REACT_SCOPE_TYPE);
    }

    /**
     * Forked from fbjs/warning:
     * https://github.com/facebook/fbjs/blob/e66ba20ad5be433eb54423f2b097d829324d9de6/packages/fbjs/src/__forks__/warning.js
     *
     * Only change is we use console.warn instead of console.error,
     * and do nothing when 'console' is not supported.
     * This really simplifies the code.
     * ---
     * Similar to invariant but only logs a warning if the condition is not met.
     * This can be used to log issues in development environments in critical
     * paths. Removing the logging code for production environments will keep the
     * same logic and follow the same code paths.
     */
    var lowPriorityWarningWithoutStack = function () {};

    {
      var printWarning = function (format) {
        for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
          args[_key - 1] = arguments[_key];
        }

        var argIndex = 0;
        var message = 'Warning: ' + format.replace(/%s/g, function () {
          return args[argIndex++];
        });

        if (typeof console !== 'undefined') {
          console.warn(message);
        }

        try {
          // --- Welcome to debugging React ---
          // This error was thrown as a convenience so that you can use this stack
          // to find the callsite that caused this warning to fire.
          throw new Error(message);
        } catch (x) {}
      };

      lowPriorityWarningWithoutStack = function (condition, format) {
        if (format === undefined) {
          throw new Error('`lowPriorityWarningWithoutStack(condition, format, ...args)` requires a warning ' + 'message argument');
        }

        if (!condition) {
          for (var _len2 = arguments.length, args = new Array(_len2 > 2 ? _len2 - 2 : 0), _key2 = 2; _key2 < _len2; _key2++) {
            args[_key2 - 2] = arguments[_key2];
          }

          printWarning.apply(void 0, [format].concat(args));
        }
      };
    }

    var lowPriorityWarningWithoutStack$1 = lowPriorityWarningWithoutStack;

    function typeOf(object) {
      if (typeof object === 'object' && object !== null) {
        var $$typeof = object.$$typeof;

        switch ($$typeof) {
          case REACT_ELEMENT_TYPE:
            var type = object.type;

            switch (type) {
              case REACT_ASYNC_MODE_TYPE:
              case REACT_CONCURRENT_MODE_TYPE:
              case REACT_FRAGMENT_TYPE:
              case REACT_PROFILER_TYPE:
              case REACT_STRICT_MODE_TYPE:
              case REACT_SUSPENSE_TYPE:
                return type;

              default:
                var $$typeofType = type && type.$$typeof;

                switch ($$typeofType) {
                  case REACT_CONTEXT_TYPE:
                  case REACT_FORWARD_REF_TYPE:
                  case REACT_PROVIDER_TYPE:
                    return $$typeofType;

                  default:
                    return $$typeof;
                }

            }

          case REACT_LAZY_TYPE:
          case REACT_MEMO_TYPE:
          case REACT_PORTAL_TYPE:
            return $$typeof;
        }
      }

      return undefined;
    } // AsyncMode is deprecated along with isAsyncMode

    var AsyncMode = REACT_ASYNC_MODE_TYPE;
    var ConcurrentMode = REACT_CONCURRENT_MODE_TYPE;
    var ContextConsumer = REACT_CONTEXT_TYPE;
    var ContextProvider = REACT_PROVIDER_TYPE;
    var Element = REACT_ELEMENT_TYPE;
    var ForwardRef = REACT_FORWARD_REF_TYPE;
    var Fragment = REACT_FRAGMENT_TYPE;
    var Lazy = REACT_LAZY_TYPE;
    var Memo = REACT_MEMO_TYPE;
    var Portal = REACT_PORTAL_TYPE;
    var Profiler = REACT_PROFILER_TYPE;
    var StrictMode = REACT_STRICT_MODE_TYPE;
    var Suspense = REACT_SUSPENSE_TYPE;
    var hasWarnedAboutDeprecatedIsAsyncMode = false; // AsyncMode should be deprecated

    function isAsyncMode(object) {
      {
        if (!hasWarnedAboutDeprecatedIsAsyncMode) {
          hasWarnedAboutDeprecatedIsAsyncMode = true;
          lowPriorityWarningWithoutStack$1(false, 'The ReactIs.isAsyncMode() alias has been deprecated, ' + 'and will be removed in React 17+. Update your code to use ' + 'ReactIs.isConcurrentMode() instead. It has the exact same API.');
        }
      }

      return isConcurrentMode(object) || typeOf(object) === REACT_ASYNC_MODE_TYPE;
    }
    function isConcurrentMode(object) {
      return typeOf(object) === REACT_CONCURRENT_MODE_TYPE;
    }
    function isContextConsumer(object) {
      return typeOf(object) === REACT_CONTEXT_TYPE;
    }
    function isContextProvider(object) {
      return typeOf(object) === REACT_PROVIDER_TYPE;
    }
    function isElement(object) {
      return typeof object === 'object' && object !== null && object.$$typeof === REACT_ELEMENT_TYPE;
    }
    function isForwardRef(object) {
      return typeOf(object) === REACT_FORWARD_REF_TYPE;
    }
    function isFragment(object) {
      return typeOf(object) === REACT_FRAGMENT_TYPE;
    }
    function isLazy(object) {
      return typeOf(object) === REACT_LAZY_TYPE;
    }
    function isMemo(object) {
      return typeOf(object) === REACT_MEMO_TYPE;
    }
    function isPortal(object) {
      return typeOf(object) === REACT_PORTAL_TYPE;
    }
    function isProfiler(object) {
      return typeOf(object) === REACT_PROFILER_TYPE;
    }
    function isStrictMode(object) {
      return typeOf(object) === REACT_STRICT_MODE_TYPE;
    }
    function isSuspense(object) {
      return typeOf(object) === REACT_SUSPENSE_TYPE;
    }

    exports.typeOf = typeOf;
    exports.AsyncMode = AsyncMode;
    exports.ConcurrentMode = ConcurrentMode;
    exports.ContextConsumer = ContextConsumer;
    exports.ContextProvider = ContextProvider;
    exports.Element = Element;
    exports.ForwardRef = ForwardRef;
    exports.Fragment = Fragment;
    exports.Lazy = Lazy;
    exports.Memo = Memo;
    exports.Portal = Portal;
    exports.Profiler = Profiler;
    exports.StrictMode = StrictMode;
    exports.Suspense = Suspense;
    exports.isValidElementType = isValidElementType;
    exports.isAsyncMode = isAsyncMode;
    exports.isConcurrentMode = isConcurrentMode;
    exports.isContextConsumer = isContextConsumer;
    exports.isContextProvider = isContextProvider;
    exports.isElement = isElement;
    exports.isForwardRef = isForwardRef;
    exports.isFragment = isFragment;
    exports.isLazy = isLazy;
    exports.isMemo = isMemo;
    exports.isPortal = isPortal;
    exports.isProfiler = isProfiler;
    exports.isStrictMode = isStrictMode;
    exports.isSuspense = isSuspense;
      })();
    }
    });

    unwrapExports(reactIs_development);
    var reactIs_development_1 = reactIs_development.typeOf;
    var reactIs_development_2 = reactIs_development.AsyncMode;
    var reactIs_development_3 = reactIs_development.ConcurrentMode;
    var reactIs_development_4 = reactIs_development.ContextConsumer;
    var reactIs_development_5 = reactIs_development.ContextProvider;
    var reactIs_development_6 = reactIs_development.Element;
    var reactIs_development_7 = reactIs_development.ForwardRef;
    var reactIs_development_8 = reactIs_development.Fragment;
    var reactIs_development_9 = reactIs_development.Lazy;
    var reactIs_development_10 = reactIs_development.Memo;
    var reactIs_development_11 = reactIs_development.Portal;
    var reactIs_development_12 = reactIs_development.Profiler;
    var reactIs_development_13 = reactIs_development.StrictMode;
    var reactIs_development_14 = reactIs_development.Suspense;
    var reactIs_development_15 = reactIs_development.isValidElementType;
    var reactIs_development_16 = reactIs_development.isAsyncMode;
    var reactIs_development_17 = reactIs_development.isConcurrentMode;
    var reactIs_development_18 = reactIs_development.isContextConsumer;
    var reactIs_development_19 = reactIs_development.isContextProvider;
    var reactIs_development_20 = reactIs_development.isElement;
    var reactIs_development_21 = reactIs_development.isForwardRef;
    var reactIs_development_22 = reactIs_development.isFragment;
    var reactIs_development_23 = reactIs_development.isLazy;
    var reactIs_development_24 = reactIs_development.isMemo;
    var reactIs_development_25 = reactIs_development.isPortal;
    var reactIs_development_26 = reactIs_development.isProfiler;
    var reactIs_development_27 = reactIs_development.isStrictMode;
    var reactIs_development_28 = reactIs_development.isSuspense;

    var reactIs = createCommonjsModule(function (module) {

    {
      module.exports = reactIs_development;
    }
    });

    /*
    object-assign
    (c) Sindre Sorhus
    @license MIT
    */
    /* eslint-disable no-unused-vars */
    var getOwnPropertySymbols = Object.getOwnPropertySymbols;
    var hasOwnProperty$1 = Object.prototype.hasOwnProperty;
    var propIsEnumerable = Object.prototype.propertyIsEnumerable;

    function toObject(val) {
    	if (val === null || val === undefined) {
    		throw new TypeError('Object.assign cannot be called with null or undefined');
    	}

    	return Object(val);
    }

    function shouldUseNative() {
    	try {
    		if (!Object.assign) {
    			return false;
    		}

    		// Detect buggy property enumeration order in older V8 versions.

    		// https://bugs.chromium.org/p/v8/issues/detail?id=4118
    		var test1 = new String('abc');  // eslint-disable-line no-new-wrappers
    		test1[5] = 'de';
    		if (Object.getOwnPropertyNames(test1)[0] === '5') {
    			return false;
    		}

    		// https://bugs.chromium.org/p/v8/issues/detail?id=3056
    		var test2 = {};
    		for (var i = 0; i < 10; i++) {
    			test2['_' + String.fromCharCode(i)] = i;
    		}
    		var order2 = Object.getOwnPropertyNames(test2).map(function (n) {
    			return test2[n];
    		});
    		if (order2.join('') !== '0123456789') {
    			return false;
    		}

    		// https://bugs.chromium.org/p/v8/issues/detail?id=3056
    		var test3 = {};
    		'abcdefghijklmnopqrst'.split('').forEach(function (letter) {
    			test3[letter] = letter;
    		});
    		if (Object.keys(Object.assign({}, test3)).join('') !==
    				'abcdefghijklmnopqrst') {
    			return false;
    		}

    		return true;
    	} catch (err) {
    		// We don't expect any of the above to throw, but better to be safe.
    		return false;
    	}
    }

    var objectAssign = shouldUseNative() ? Object.assign : function (target, source) {
    	var from;
    	var to = toObject(target);
    	var symbols;

    	for (var s = 1; s < arguments.length; s++) {
    		from = Object(arguments[s]);

    		for (var key in from) {
    			if (hasOwnProperty$1.call(from, key)) {
    				to[key] = from[key];
    			}
    		}

    		if (getOwnPropertySymbols) {
    			symbols = getOwnPropertySymbols(from);
    			for (var i = 0; i < symbols.length; i++) {
    				if (propIsEnumerable.call(from, symbols[i])) {
    					to[symbols[i]] = from[symbols[i]];
    				}
    			}
    		}
    	}

    	return to;
    };

    /**
     * Copyright (c) 2013-present, Facebook, Inc.
     *
     * This source code is licensed under the MIT license found in the
     * LICENSE file in the root directory of this source tree.
     */

    var ReactPropTypesSecret = 'SECRET_DO_NOT_PASS_THIS_OR_YOU_WILL_BE_FIRED';

    var ReactPropTypesSecret_1 = ReactPropTypesSecret;

    var printWarning = function() {};

    {
      var ReactPropTypesSecret$1 = ReactPropTypesSecret_1;
      var loggedTypeFailures = {};
      var has = Function.call.bind(Object.prototype.hasOwnProperty);

      printWarning = function(text) {
        var message = 'Warning: ' + text;
        if (typeof console !== 'undefined') {
          console.error(message);
        }
        try {
          // --- Welcome to debugging React ---
          // This error was thrown as a convenience so that you can use this stack
          // to find the callsite that caused this warning to fire.
          throw new Error(message);
        } catch (x) {}
      };
    }

    /**
     * Assert that the values match with the type specs.
     * Error messages are memorized and will only be shown once.
     *
     * @param {object} typeSpecs Map of name to a ReactPropType
     * @param {object} values Runtime values that need to be type-checked
     * @param {string} location e.g. "prop", "context", "child context"
     * @param {string} componentName Name of the component for error messages.
     * @param {?Function} getStack Returns the component stack.
     * @private
     */
    function checkPropTypes(typeSpecs, values, location, componentName, getStack) {
      {
        for (var typeSpecName in typeSpecs) {
          if (has(typeSpecs, typeSpecName)) {
            var error;
            // Prop type validation may throw. In case they do, we don't want to
            // fail the render phase where it didn't fail before. So we log it.
            // After these have been cleaned up, we'll let them throw.
            try {
              // This is intentionally an invariant that gets caught. It's the same
              // behavior as without this statement except with a better message.
              if (typeof typeSpecs[typeSpecName] !== 'function') {
                var err = Error(
                  (componentName || 'React class') + ': ' + location + ' type `' + typeSpecName + '` is invalid; ' +
                  'it must be a function, usually from the `prop-types` package, but received `' + typeof typeSpecs[typeSpecName] + '`.'
                );
                err.name = 'Invariant Violation';
                throw err;
              }
              error = typeSpecs[typeSpecName](values, typeSpecName, componentName, location, null, ReactPropTypesSecret$1);
            } catch (ex) {
              error = ex;
            }
            if (error && !(error instanceof Error)) {
              printWarning(
                (componentName || 'React class') + ': type specification of ' +
                location + ' `' + typeSpecName + '` is invalid; the type checker ' +
                'function must return `null` or an `Error` but returned a ' + typeof error + '. ' +
                'You may have forgotten to pass an argument to the type checker ' +
                'creator (arrayOf, instanceOf, objectOf, oneOf, oneOfType, and ' +
                'shape all require an argument).'
              );
            }
            if (error instanceof Error && !(error.message in loggedTypeFailures)) {
              // Only monitor this failure once because there tends to be a lot of the
              // same error.
              loggedTypeFailures[error.message] = true;

              var stack = getStack ? getStack() : '';

              printWarning(
                'Failed ' + location + ' type: ' + error.message + (stack != null ? stack : '')
              );
            }
          }
        }
      }
    }

    /**
     * Resets warning cache when testing.
     *
     * @private
     */
    checkPropTypes.resetWarningCache = function() {
      {
        loggedTypeFailures = {};
      }
    };

    var checkPropTypes_1 = checkPropTypes;

    var has$1 = Function.call.bind(Object.prototype.hasOwnProperty);
    var printWarning$1 = function() {};

    {
      printWarning$1 = function(text) {
        var message = 'Warning: ' + text;
        if (typeof console !== 'undefined') {
          console.error(message);
        }
        try {
          // --- Welcome to debugging React ---
          // This error was thrown as a convenience so that you can use this stack
          // to find the callsite that caused this warning to fire.
          throw new Error(message);
        } catch (x) {}
      };
    }

    function emptyFunctionThatReturnsNull() {
      return null;
    }

    var factoryWithTypeCheckers = function(isValidElement, throwOnDirectAccess) {
      /* global Symbol */
      var ITERATOR_SYMBOL = typeof Symbol === 'function' && Symbol.iterator;
      var FAUX_ITERATOR_SYMBOL = '@@iterator'; // Before Symbol spec.

      /**
       * Returns the iterator method function contained on the iterable object.
       *
       * Be sure to invoke the function with the iterable as context:
       *
       *     var iteratorFn = getIteratorFn(myIterable);
       *     if (iteratorFn) {
       *       var iterator = iteratorFn.call(myIterable);
       *       ...
       *     }
       *
       * @param {?object} maybeIterable
       * @return {?function}
       */
      function getIteratorFn(maybeIterable) {
        var iteratorFn = maybeIterable && (ITERATOR_SYMBOL && maybeIterable[ITERATOR_SYMBOL] || maybeIterable[FAUX_ITERATOR_SYMBOL]);
        if (typeof iteratorFn === 'function') {
          return iteratorFn;
        }
      }

      /**
       * Collection of methods that allow declaration and validation of props that are
       * supplied to React components. Example usage:
       *
       *   var Props = require('ReactPropTypes');
       *   var MyArticle = React.createClass({
       *     propTypes: {
       *       // An optional string prop named "description".
       *       description: Props.string,
       *
       *       // A required enum prop named "category".
       *       category: Props.oneOf(['News','Photos']).isRequired,
       *
       *       // A prop named "dialog" that requires an instance of Dialog.
       *       dialog: Props.instanceOf(Dialog).isRequired
       *     },
       *     render: function() { ... }
       *   });
       *
       * A more formal specification of how these methods are used:
       *
       *   type := array|bool|func|object|number|string|oneOf([...])|instanceOf(...)
       *   decl := ReactPropTypes.{type}(.isRequired)?
       *
       * Each and every declaration produces a function with the same signature. This
       * allows the creation of custom validation functions. For example:
       *
       *  var MyLink = React.createClass({
       *    propTypes: {
       *      // An optional string or URI prop named "href".
       *      href: function(props, propName, componentName) {
       *        var propValue = props[propName];
       *        if (propValue != null && typeof propValue !== 'string' &&
       *            !(propValue instanceof URI)) {
       *          return new Error(
       *            'Expected a string or an URI for ' + propName + ' in ' +
       *            componentName
       *          );
       *        }
       *      }
       *    },
       *    render: function() {...}
       *  });
       *
       * @internal
       */

      var ANONYMOUS = '<<anonymous>>';

      // Important!
      // Keep this list in sync with production version in `./factoryWithThrowingShims.js`.
      var ReactPropTypes = {
        array: createPrimitiveTypeChecker('array'),
        bool: createPrimitiveTypeChecker('boolean'),
        func: createPrimitiveTypeChecker('function'),
        number: createPrimitiveTypeChecker('number'),
        object: createPrimitiveTypeChecker('object'),
        string: createPrimitiveTypeChecker('string'),
        symbol: createPrimitiveTypeChecker('symbol'),

        any: createAnyTypeChecker(),
        arrayOf: createArrayOfTypeChecker,
        element: createElementTypeChecker(),
        elementType: createElementTypeTypeChecker(),
        instanceOf: createInstanceTypeChecker,
        node: createNodeChecker(),
        objectOf: createObjectOfTypeChecker,
        oneOf: createEnumTypeChecker,
        oneOfType: createUnionTypeChecker,
        shape: createShapeTypeChecker,
        exact: createStrictShapeTypeChecker,
      };

      /**
       * inlined Object.is polyfill to avoid requiring consumers ship their own
       * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/is
       */
      /*eslint-disable no-self-compare*/
      function is(x, y) {
        // SameValue algorithm
        if (x === y) {
          // Steps 1-5, 7-10
          // Steps 6.b-6.e: +0 != -0
          return x !== 0 || 1 / x === 1 / y;
        } else {
          // Step 6.a: NaN == NaN
          return x !== x && y !== y;
        }
      }
      /*eslint-enable no-self-compare*/

      /**
       * We use an Error-like object for backward compatibility as people may call
       * PropTypes directly and inspect their output. However, we don't use real
       * Errors anymore. We don't inspect their stack anyway, and creating them
       * is prohibitively expensive if they are created too often, such as what
       * happens in oneOfType() for any type before the one that matched.
       */
      function PropTypeError(message) {
        this.message = message;
        this.stack = '';
      }
      // Make `instanceof Error` still work for returned errors.
      PropTypeError.prototype = Error.prototype;

      function createChainableTypeChecker(validate) {
        {
          var manualPropTypeCallCache = {};
          var manualPropTypeWarningCount = 0;
        }
        function checkType(isRequired, props, propName, componentName, location, propFullName, secret) {
          componentName = componentName || ANONYMOUS;
          propFullName = propFullName || propName;

          if (secret !== ReactPropTypesSecret_1) {
            if (throwOnDirectAccess) {
              // New behavior only for users of `prop-types` package
              var err = new Error(
                'Calling PropTypes validators directly is not supported by the `prop-types` package. ' +
                'Use `PropTypes.checkPropTypes()` to call them. ' +
                'Read more at http://fb.me/use-check-prop-types'
              );
              err.name = 'Invariant Violation';
              throw err;
            } else if ( typeof console !== 'undefined') {
              // Old behavior for people using React.PropTypes
              var cacheKey = componentName + ':' + propName;
              if (
                !manualPropTypeCallCache[cacheKey] &&
                // Avoid spamming the console because they are often not actionable except for lib authors
                manualPropTypeWarningCount < 3
              ) {
                printWarning$1(
                  'You are manually calling a React.PropTypes validation ' +
                  'function for the `' + propFullName + '` prop on `' + componentName  + '`. This is deprecated ' +
                  'and will throw in the standalone `prop-types` package. ' +
                  'You may be seeing this warning due to a third-party PropTypes ' +
                  'library. See https://fb.me/react-warning-dont-call-proptypes ' + 'for details.'
                );
                manualPropTypeCallCache[cacheKey] = true;
                manualPropTypeWarningCount++;
              }
            }
          }
          if (props[propName] == null) {
            if (isRequired) {
              if (props[propName] === null) {
                return new PropTypeError('The ' + location + ' `' + propFullName + '` is marked as required ' + ('in `' + componentName + '`, but its value is `null`.'));
              }
              return new PropTypeError('The ' + location + ' `' + propFullName + '` is marked as required in ' + ('`' + componentName + '`, but its value is `undefined`.'));
            }
            return null;
          } else {
            return validate(props, propName, componentName, location, propFullName);
          }
        }

        var chainedCheckType = checkType.bind(null, false);
        chainedCheckType.isRequired = checkType.bind(null, true);

        return chainedCheckType;
      }

      function createPrimitiveTypeChecker(expectedType) {
        function validate(props, propName, componentName, location, propFullName, secret) {
          var propValue = props[propName];
          var propType = getPropType(propValue);
          if (propType !== expectedType) {
            // `propValue` being instance of, say, date/regexp, pass the 'object'
            // check, but we can offer a more precise error message here rather than
            // 'of type `object`'.
            var preciseType = getPreciseType(propValue);

            return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + preciseType + '` supplied to `' + componentName + '`, expected ') + ('`' + expectedType + '`.'));
          }
          return null;
        }
        return createChainableTypeChecker(validate);
      }

      function createAnyTypeChecker() {
        return createChainableTypeChecker(emptyFunctionThatReturnsNull);
      }

      function createArrayOfTypeChecker(typeChecker) {
        function validate(props, propName, componentName, location, propFullName) {
          if (typeof typeChecker !== 'function') {
            return new PropTypeError('Property `' + propFullName + '` of component `' + componentName + '` has invalid PropType notation inside arrayOf.');
          }
          var propValue = props[propName];
          if (!Array.isArray(propValue)) {
            var propType = getPropType(propValue);
            return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected an array.'));
          }
          for (var i = 0; i < propValue.length; i++) {
            var error = typeChecker(propValue, i, componentName, location, propFullName + '[' + i + ']', ReactPropTypesSecret_1);
            if (error instanceof Error) {
              return error;
            }
          }
          return null;
        }
        return createChainableTypeChecker(validate);
      }

      function createElementTypeChecker() {
        function validate(props, propName, componentName, location, propFullName) {
          var propValue = props[propName];
          if (!isValidElement(propValue)) {
            var propType = getPropType(propValue);
            return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected a single ReactElement.'));
          }
          return null;
        }
        return createChainableTypeChecker(validate);
      }

      function createElementTypeTypeChecker() {
        function validate(props, propName, componentName, location, propFullName) {
          var propValue = props[propName];
          if (!reactIs.isValidElementType(propValue)) {
            var propType = getPropType(propValue);
            return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected a single ReactElement type.'));
          }
          return null;
        }
        return createChainableTypeChecker(validate);
      }

      function createInstanceTypeChecker(expectedClass) {
        function validate(props, propName, componentName, location, propFullName) {
          if (!(props[propName] instanceof expectedClass)) {
            var expectedClassName = expectedClass.name || ANONYMOUS;
            var actualClassName = getClassName(props[propName]);
            return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + actualClassName + '` supplied to `' + componentName + '`, expected ') + ('instance of `' + expectedClassName + '`.'));
          }
          return null;
        }
        return createChainableTypeChecker(validate);
      }

      function createEnumTypeChecker(expectedValues) {
        if (!Array.isArray(expectedValues)) {
          {
            if (arguments.length > 1) {
              printWarning$1(
                'Invalid arguments supplied to oneOf, expected an array, got ' + arguments.length + ' arguments. ' +
                'A common mistake is to write oneOf(x, y, z) instead of oneOf([x, y, z]).'
              );
            } else {
              printWarning$1('Invalid argument supplied to oneOf, expected an array.');
            }
          }
          return emptyFunctionThatReturnsNull;
        }

        function validate(props, propName, componentName, location, propFullName) {
          var propValue = props[propName];
          for (var i = 0; i < expectedValues.length; i++) {
            if (is(propValue, expectedValues[i])) {
              return null;
            }
          }

          var valuesString = JSON.stringify(expectedValues, function replacer(key, value) {
            var type = getPreciseType(value);
            if (type === 'symbol') {
              return String(value);
            }
            return value;
          });
          return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of value `' + String(propValue) + '` ' + ('supplied to `' + componentName + '`, expected one of ' + valuesString + '.'));
        }
        return createChainableTypeChecker(validate);
      }

      function createObjectOfTypeChecker(typeChecker) {
        function validate(props, propName, componentName, location, propFullName) {
          if (typeof typeChecker !== 'function') {
            return new PropTypeError('Property `' + propFullName + '` of component `' + componentName + '` has invalid PropType notation inside objectOf.');
          }
          var propValue = props[propName];
          var propType = getPropType(propValue);
          if (propType !== 'object') {
            return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected an object.'));
          }
          for (var key in propValue) {
            if (has$1(propValue, key)) {
              var error = typeChecker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret_1);
              if (error instanceof Error) {
                return error;
              }
            }
          }
          return null;
        }
        return createChainableTypeChecker(validate);
      }

      function createUnionTypeChecker(arrayOfTypeCheckers) {
        if (!Array.isArray(arrayOfTypeCheckers)) {
           printWarning$1('Invalid argument supplied to oneOfType, expected an instance of array.') ;
          return emptyFunctionThatReturnsNull;
        }

        for (var i = 0; i < arrayOfTypeCheckers.length; i++) {
          var checker = arrayOfTypeCheckers[i];
          if (typeof checker !== 'function') {
            printWarning$1(
              'Invalid argument supplied to oneOfType. Expected an array of check functions, but ' +
              'received ' + getPostfixForTypeWarning(checker) + ' at index ' + i + '.'
            );
            return emptyFunctionThatReturnsNull;
          }
        }

        function validate(props, propName, componentName, location, propFullName) {
          for (var i = 0; i < arrayOfTypeCheckers.length; i++) {
            var checker = arrayOfTypeCheckers[i];
            if (checker(props, propName, componentName, location, propFullName, ReactPropTypesSecret_1) == null) {
              return null;
            }
          }

          return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` supplied to ' + ('`' + componentName + '`.'));
        }
        return createChainableTypeChecker(validate);
      }

      function createNodeChecker() {
        function validate(props, propName, componentName, location, propFullName) {
          if (!isNode(props[propName])) {
            return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` supplied to ' + ('`' + componentName + '`, expected a ReactNode.'));
          }
          return null;
        }
        return createChainableTypeChecker(validate);
      }

      function createShapeTypeChecker(shapeTypes) {
        function validate(props, propName, componentName, location, propFullName) {
          var propValue = props[propName];
          var propType = getPropType(propValue);
          if (propType !== 'object') {
            return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type `' + propType + '` ' + ('supplied to `' + componentName + '`, expected `object`.'));
          }
          for (var key in shapeTypes) {
            var checker = shapeTypes[key];
            if (!checker) {
              continue;
            }
            var error = checker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret_1);
            if (error) {
              return error;
            }
          }
          return null;
        }
        return createChainableTypeChecker(validate);
      }

      function createStrictShapeTypeChecker(shapeTypes) {
        function validate(props, propName, componentName, location, propFullName) {
          var propValue = props[propName];
          var propType = getPropType(propValue);
          if (propType !== 'object') {
            return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type `' + propType + '` ' + ('supplied to `' + componentName + '`, expected `object`.'));
          }
          // We need to check all keys in case some are required but missing from
          // props.
          var allKeys = objectAssign({}, props[propName], shapeTypes);
          for (var key in allKeys) {
            var checker = shapeTypes[key];
            if (!checker) {
              return new PropTypeError(
                'Invalid ' + location + ' `' + propFullName + '` key `' + key + '` supplied to `' + componentName + '`.' +
                '\nBad object: ' + JSON.stringify(props[propName], null, '  ') +
                '\nValid keys: ' +  JSON.stringify(Object.keys(shapeTypes), null, '  ')
              );
            }
            var error = checker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret_1);
            if (error) {
              return error;
            }
          }
          return null;
        }

        return createChainableTypeChecker(validate);
      }

      function isNode(propValue) {
        switch (typeof propValue) {
          case 'number':
          case 'string':
          case 'undefined':
            return true;
          case 'boolean':
            return !propValue;
          case 'object':
            if (Array.isArray(propValue)) {
              return propValue.every(isNode);
            }
            if (propValue === null || isValidElement(propValue)) {
              return true;
            }

            var iteratorFn = getIteratorFn(propValue);
            if (iteratorFn) {
              var iterator = iteratorFn.call(propValue);
              var step;
              if (iteratorFn !== propValue.entries) {
                while (!(step = iterator.next()).done) {
                  if (!isNode(step.value)) {
                    return false;
                  }
                }
              } else {
                // Iterator will provide entry [k,v] tuples rather than values.
                while (!(step = iterator.next()).done) {
                  var entry = step.value;
                  if (entry) {
                    if (!isNode(entry[1])) {
                      return false;
                    }
                  }
                }
              }
            } else {
              return false;
            }

            return true;
          default:
            return false;
        }
      }

      function isSymbol(propType, propValue) {
        // Native Symbol.
        if (propType === 'symbol') {
          return true;
        }

        // falsy value can't be a Symbol
        if (!propValue) {
          return false;
        }

        // 19.4.3.5 Symbol.prototype[@@toStringTag] === 'Symbol'
        if (propValue['@@toStringTag'] === 'Symbol') {
          return true;
        }

        // Fallback for non-spec compliant Symbols which are polyfilled.
        if (typeof Symbol === 'function' && propValue instanceof Symbol) {
          return true;
        }

        return false;
      }

      // Equivalent of `typeof` but with special handling for array and regexp.
      function getPropType(propValue) {
        var propType = typeof propValue;
        if (Array.isArray(propValue)) {
          return 'array';
        }
        if (propValue instanceof RegExp) {
          // Old webkits (at least until Android 4.0) return 'function' rather than
          // 'object' for typeof a RegExp. We'll normalize this here so that /bla/
          // passes PropTypes.object.
          return 'object';
        }
        if (isSymbol(propType, propValue)) {
          return 'symbol';
        }
        return propType;
      }

      // This handles more types than `getPropType`. Only used for error messages.
      // See `createPrimitiveTypeChecker`.
      function getPreciseType(propValue) {
        if (typeof propValue === 'undefined' || propValue === null) {
          return '' + propValue;
        }
        var propType = getPropType(propValue);
        if (propType === 'object') {
          if (propValue instanceof Date) {
            return 'date';
          } else if (propValue instanceof RegExp) {
            return 'regexp';
          }
        }
        return propType;
      }

      // Returns a string that is postfixed to a warning about an invalid type.
      // For example, "undefined" or "of type array"
      function getPostfixForTypeWarning(value) {
        var type = getPreciseType(value);
        switch (type) {
          case 'array':
          case 'object':
            return 'an ' + type;
          case 'boolean':
          case 'date':
          case 'regexp':
            return 'a ' + type;
          default:
            return type;
        }
      }

      // Returns class name of the object, if any.
      function getClassName(propValue) {
        if (!propValue.constructor || !propValue.constructor.name) {
          return ANONYMOUS;
        }
        return propValue.constructor.name;
      }

      ReactPropTypes.checkPropTypes = checkPropTypes_1;
      ReactPropTypes.resetWarningCache = checkPropTypes_1.resetWarningCache;
      ReactPropTypes.PropTypes = ReactPropTypes;

      return ReactPropTypes;
    };

    var propTypes = createCommonjsModule(function (module) {
    /**
     * Copyright (c) 2013-present, Facebook, Inc.
     *
     * This source code is licensed under the MIT license found in the
     * LICENSE file in the root directory of this source tree.
     */

    {
      var ReactIs = reactIs;

      // By explicitly using `prop-types` you are opting into new development behavior.
      // http://fb.me/prop-types-in-prod
      var throwOnDirectAccess = true;
      module.exports = factoryWithTypeCheckers(ReactIs.isElement, throwOnDirectAccess);
    }
    });

    var dist = createCommonjsModule(function (module) {
    module.exports=function(t){function n(e){if(r[e])return r[e].exports;var o=r[e]={i:e,l:!1,exports:{}};return t[e].call(o.exports,o,o.exports,n),o.l=!0,o.exports}var r={};return n.m=t,n.c=r,n.d=function(t,r,e){n.o(t,r)||Object.defineProperty(t,r,{configurable:!1,enumerable:!0,get:e});},n.n=function(t){var r=t&&t.__esModule?function(){return t.default}:function(){return t};return n.d(r,"a",r),r},n.o=function(t,n){return Object.prototype.hasOwnProperty.call(t,n)},n.p="",n(n.s=13)}([function(t,n){var r=t.exports="undefined"!=typeof window&&window.Math==Math?window:"undefined"!=typeof self&&self.Math==Math?self:Function("return this")();"number"==typeof __g&&(__g=r);},function(t,n){t.exports=function(t){return "object"==typeof t?null!==t:"function"==typeof t};},function(t,n){var r=t.exports={version:"2.5.0"};"number"==typeof __e&&(__e=r);},function(t,n,r){t.exports=!r(4)(function(){return 7!=Object.defineProperty({},"a",{get:function(){return 7}}).a});},function(t,n){t.exports=function(t){try{return !!t()}catch(t){return !0}};},function(t,n){var r={}.toString;t.exports=function(t){return r.call(t).slice(8,-1)};},function(t,n,r){var e=r(32)("wks"),o=r(9),i=r(0).Symbol,u="function"==typeof i;(t.exports=function(t){return e[t]||(e[t]=u&&i[t]||(u?i:o)("Symbol."+t))}).store=e;},function(t,n,r){var e=r(0),o=r(2),i=r(8),u=r(22),c=r(10),f=function(t,n,r){var a,s,p,l,v=t&f.F,y=t&f.G,h=t&f.S,d=t&f.P,x=t&f.B,g=y?e:h?e[n]||(e[n]={}):(e[n]||{}).prototype,m=y?o:o[n]||(o[n]={}),b=m.prototype||(m.prototype={});y&&(r=n);for(a in r)s=!v&&g&&void 0!==g[a],p=(s?g:r)[a],l=x&&s?c(p,e):d&&"function"==typeof p?c(Function.call,p):p,g&&u(g,a,p,t&f.U),m[a]!=p&&i(m,a,l),d&&b[a]!=p&&(b[a]=p);};e.core=o,f.F=1,f.G=2,f.S=4,f.P=8,f.B=16,f.W=32,f.U=64,f.R=128,t.exports=f;},function(t,n,r){var e=r(16),o=r(21);t.exports=r(3)?function(t,n,r){return e.f(t,n,o(1,r))}:function(t,n,r){return t[n]=r,t};},function(t,n){var r=0,e=Math.random();t.exports=function(t){return "Symbol(".concat(void 0===t?"":t,")_",(++r+e).toString(36))};},function(t,n,r){var e=r(24);t.exports=function(t,n,r){if(e(t),void 0===n)return t;switch(r){case 1:return function(r){return t.call(n,r)};case 2:return function(r,e){return t.call(n,r,e)};case 3:return function(r,e,o){return t.call(n,r,e,o)}}return function(){return t.apply(n,arguments)}};},function(t,n){t.exports=function(t){if(void 0==t)throw TypeError("Can't call method on  "+t);return t};},function(t,n,r){var e=r(28),o=Math.min;t.exports=function(t){return t>0?o(e(t),9007199254740991):0};},function(t,n,r){n.__esModule=!0,n.default=function(t,n){if(t&&n){var r=Array.isArray(n)?n:n.split(","),e=t.name||"",o=t.type||"",i=o.replace(/\/.*$/,"");return r.some(function(t){var n=t.trim();return "."===n.charAt(0)?e.toLowerCase().endsWith(n.toLowerCase()):n.endsWith("/*")?i===n.replace(/\/.*$/,""):o===n})}return !0},r(14),r(34);},function(t,n,r){r(15),t.exports=r(2).Array.some;},function(t,n,r){var e=r(7),o=r(25)(3);e(e.P+e.F*!r(33)([].some,!0),"Array",{some:function(t){return o(this,t,arguments[1])}});},function(t,n,r){var e=r(17),o=r(18),i=r(20),u=Object.defineProperty;n.f=r(3)?Object.defineProperty:function(t,n,r){if(e(t),n=i(n,!0),e(r),o)try{return u(t,n,r)}catch(t){}if("get"in r||"set"in r)throw TypeError("Accessors not supported!");return "value"in r&&(t[n]=r.value),t};},function(t,n,r){var e=r(1);t.exports=function(t){if(!e(t))throw TypeError(t+" is not an object!");return t};},function(t,n,r){t.exports=!r(3)&&!r(4)(function(){return 7!=Object.defineProperty(r(19)("div"),"a",{get:function(){return 7}}).a});},function(t,n,r){var e=r(1),o=r(0).document,i=e(o)&&e(o.createElement);t.exports=function(t){return i?o.createElement(t):{}};},function(t,n,r){var e=r(1);t.exports=function(t,n){if(!e(t))return t;var r,o;if(n&&"function"==typeof(r=t.toString)&&!e(o=r.call(t)))return o;if("function"==typeof(r=t.valueOf)&&!e(o=r.call(t)))return o;if(!n&&"function"==typeof(r=t.toString)&&!e(o=r.call(t)))return o;throw TypeError("Can't convert object to primitive value")};},function(t,n){t.exports=function(t,n){return {enumerable:!(1&t),configurable:!(2&t),writable:!(4&t),value:n}};},function(t,n,r){var e=r(0),o=r(8),i=r(23),u=r(9)("src"),c=Function.toString,f=(""+c).split("toString");r(2).inspectSource=function(t){return c.call(t)},(t.exports=function(t,n,r,c){var a="function"==typeof r;a&&(i(r,"name")||o(r,"name",n)),t[n]!==r&&(a&&(i(r,u)||o(r,u,t[n]?""+t[n]:f.join(String(n)))),t===e?t[n]=r:c?t[n]?t[n]=r:o(t,n,r):(delete t[n],o(t,n,r)));})(Function.prototype,"toString",function(){return "function"==typeof this&&this[u]||c.call(this)});},function(t,n){var r={}.hasOwnProperty;t.exports=function(t,n){return r.call(t,n)};},function(t,n){t.exports=function(t){if("function"!=typeof t)throw TypeError(t+" is not a function!");return t};},function(t,n,r){var e=r(10),o=r(26),i=r(27),u=r(12),c=r(29);t.exports=function(t,n){var r=1==t,f=2==t,a=3==t,s=4==t,p=6==t,l=5==t||p,v=n||c;return function(n,c,y){for(var h,d,x=i(n),g=o(x),m=e(c,y,3),b=u(g.length),_=0,w=r?v(n,b):f?v(n,0):void 0;b>_;_++)if((l||_ in g)&&(h=g[_],d=m(h,_,x),t))if(r)w[_]=d;else if(d)switch(t){case 3:return !0;case 5:return h;case 6:return _;case 2:w.push(h);}else if(s)return !1;return p?-1:a||s?s:w}};},function(t,n,r){var e=r(5);t.exports=Object("z").propertyIsEnumerable(0)?Object:function(t){return "String"==e(t)?t.split(""):Object(t)};},function(t,n,r){var e=r(11);t.exports=function(t){return Object(e(t))};},function(t,n){var r=Math.ceil,e=Math.floor;t.exports=function(t){return isNaN(t=+t)?0:(t>0?e:r)(t)};},function(t,n,r){var e=r(30);t.exports=function(t,n){return new(e(t))(n)};},function(t,n,r){var e=r(1),o=r(31),i=r(6)("species");t.exports=function(t){var n;return o(t)&&(n=t.constructor,"function"!=typeof n||n!==Array&&!o(n.prototype)||(n=void 0),e(n)&&null===(n=n[i])&&(n=void 0)),void 0===n?Array:n};},function(t,n,r){var e=r(5);t.exports=Array.isArray||function(t){return "Array"==e(t)};},function(t,n,r){var e=r(0),o=e["__core-js_shared__"]||(e["__core-js_shared__"]={});t.exports=function(t){return o[t]||(o[t]={})};},function(t,n,r){var e=r(4);t.exports=function(t,n){return !!t&&e(function(){n?t.call(null,function(){},1):t.call(null);})};},function(t,n,r){r(35),t.exports=r(2).String.endsWith;},function(t,n,r){var e=r(7),o=r(12),i=r(36),u="".endsWith;e(e.P+e.F*r(38)("endsWith"),"String",{endsWith:function(t){var n=i(this,t,"endsWith"),r=arguments.length>1?arguments[1]:void 0,e=o(n.length),c=void 0===r?e:Math.min(o(r),e),f=String(t);return u?u.call(n,f,c):n.slice(c-f.length,c)===f}});},function(t,n,r){var e=r(37),o=r(11);t.exports=function(t,n,r){if(e(n))throw TypeError("String#"+r+" doesn't accept regex!");return String(o(t))};},function(t,n,r){var e=r(1),o=r(5),i=r(6)("match");t.exports=function(t){var n;return e(t)&&(void 0!==(n=t[i])?!!n:"RegExp"==o(t))};},function(t,n,r){var e=r(6)("match");t.exports=function(t){var n=/./;try{"/./"[t](n);}catch(r){try{return n[e]=!1,!"/./"[t](n)}catch(t){}}return !0};}]);
    });

    var accepts = unwrapExports(dist);

    var supportMultiple = typeof document !== 'undefined' && document && document.createElement ? 'multiple' in document.createElement('input') : true;

    // Firefox versions prior to 53 return a bogus MIME type for every file drag, so dragovers with
    // that MIME type will always be accepted
    function fileAccepted(file, accept) {
      return file.type === 'application/x-moz-file' || accepts(file, accept);
    }

    function fileMatchSize(file, maxSize, minSize) {
      return file.size <= maxSize && file.size >= minSize;
    }

    function allFilesAccepted(files, accept) {
      return files.every(function (file) {
        return fileAccepted(file, accept);
      });
    }

    // React's synthetic events has evt.isPropagationStopped,
    // but to remain compatibility with other libs (Preact) fall back
    // to check evt.cancelBubble
    function isPropagationStopped(evt) {
      if (typeof evt.isPropagationStopped === 'function') {
        return evt.isPropagationStopped();
      } else if (typeof evt.cancelBubble !== 'undefined') {
        return evt.cancelBubble;
      }
      return false;
    }

    // React's synthetic events has evt.isDefaultPrevented,
    // but to remain compatibility with other libs (Preact) first
    // check evt.defaultPrevented
    function isDefaultPrevented(evt) {
      if (typeof evt.defaultPrevented !== 'undefined') {
        return evt.defaultPrevented;
      } else if (typeof evt.isDefaultPrevented === 'function') {
        return evt.isDefaultPrevented();
      }
      return false;
    }

    function isDragDataWithFiles(evt) {
      if (!evt.dataTransfer) {
        return true;
      }
      // https://developer.mozilla.org/en-US/docs/Web/API/DataTransfer/types
      // https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API/Recommended_drag_types#file
      return Array.prototype.some.call(evt.dataTransfer.types, function (type) {
        return type === 'Files' || type === 'application/x-moz-file';
      });
    }

    // allow the entire document to be a drag target
    function onDocumentDragOver(evt) {
      evt.preventDefault();
    }

    function isIe(userAgent) {
      return userAgent.indexOf('MSIE') !== -1 || userAgent.indexOf('Trident/') !== -1;
    }

    function isEdge(userAgent) {
      return userAgent.indexOf('Edge/') !== -1;
    }

    function isIeOrEdge() {
      var userAgent = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : window.navigator.userAgent;

      return isIe(userAgent) || isEdge(userAgent);
    }

    /**
     * This is intended to be used to compose event handlers
     * They are executed in order until one of them calls `event.preventDefault()`.
     * Not sure this is the best way to do this, but it seems legit.
     * @param {Function} fns the event hanlder functions
     * @return {Function} the event handler to add to an element
     */
    function composeEventHandlers() {
      for (var _len = arguments.length, fns = Array(_len), _key = 0; _key < _len; _key++) {
        fns[_key] = arguments[_key];
      }

      return function (event) {
        for (var _len2 = arguments.length, args = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
          args[_key2 - 1] = arguments[_key2];
        }

        return fns.some(function (fn) {
          fn && fn.apply(undefined, [event].concat(args));
          return event.defaultPrevented;
        });
      };
    }

    var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

    var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

    function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

    function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

    function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

    function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

    function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

    function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

    var Dropzone = function (_React$Component) {
      _inherits(Dropzone, _React$Component);

      function Dropzone() {
        var _ref;

        var _temp, _this, _ret;

        _classCallCheck(this, Dropzone);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
          args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Dropzone.__proto__ || Object.getPrototypeOf(Dropzone)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
          draggedFiles: [],
          acceptedFiles: [],
          rejectedFiles: []
        }, _this.isFileDialogActive = false, _this.onDocumentDrop = function (evt) {
          if (_this.node && _this.node.contains(evt.target)) {
            // if we intercepted an event for our instance, let it propagate down to the instance's onDrop handler
            return;
          }
          evt.preventDefault();
          _this.dragTargets = [];
        }, _this.onDragStart = function (evt) {
          evt.persist();
          if (_this.props.onDragStart && isDragDataWithFiles(evt)) {
            _this.props.onDragStart.call(_this, evt);
          }
        }, _this.onDragEnter = function (evt) {
          evt.preventDefault();

          // Count the dropzone and any children that are entered.
          if (_this.dragTargets.indexOf(evt.target) === -1) {
            _this.dragTargets.push(evt.target);
          }

          evt.persist();

          if (isDragDataWithFiles(evt)) {
            Promise.resolve(_this.props.getDataTransferItems(evt)).then(function (draggedFiles) {
              if (isPropagationStopped(evt)) {
                return;
              }

              _this.setState({
                draggedFiles: draggedFiles,
                // Do not rely on files for the drag state. It doesn't work in Safari.
                isDragActive: true
              });
            });

            if (_this.props.onDragEnter) {
              _this.props.onDragEnter.call(_this, evt);
            }
          }
        }, _this.onDragOver = function (evt) {
          // eslint-disable-line class-methods-use-this
          evt.preventDefault();
          evt.persist();

          if (evt.dataTransfer) {
            evt.dataTransfer.dropEffect = 'copy';
          }

          if (_this.props.onDragOver && isDragDataWithFiles(evt)) {
            _this.props.onDragOver.call(_this, evt);
          }

          return false;
        }, _this.onDragLeave = function (evt) {
          evt.preventDefault();
          evt.persist();

          // Only deactivate once the dropzone and all children have been left.
          _this.dragTargets = _this.dragTargets.filter(function (el) {
            return el !== evt.target && _this.node.contains(el);
          });
          if (_this.dragTargets.length > 0) {
            return;
          }

          // Clear dragging files state
          _this.setState({
            isDragActive: false,
            draggedFiles: []
          });

          if (_this.props.onDragLeave && isDragDataWithFiles(evt)) {
            _this.props.onDragLeave.call(_this, evt);
          }
        }, _this.onDrop = function (evt) {
          var _this$props = _this.props,
              onDrop = _this$props.onDrop,
              onDropAccepted = _this$props.onDropAccepted,
              onDropRejected = _this$props.onDropRejected,
              multiple = _this$props.multiple,
              accept = _this$props.accept,
              getDataTransferItems = _this$props.getDataTransferItems;

          // Stop default browser behavior

          evt.preventDefault();

          // Persist event for later usage
          evt.persist();

          // Reset the counter along with the drag on a drop.
          _this.dragTargets = [];
          _this.isFileDialogActive = false;

          // Clear files value
          _this.draggedFiles = null;

          // Reset drag state
          _this.setState({
            isDragActive: false,
            draggedFiles: []
          });

          if (isDragDataWithFiles(evt)) {
            Promise.resolve(getDataTransferItems(evt)).then(function (fileList) {
              var acceptedFiles = [];
              var rejectedFiles = [];

              if (isPropagationStopped(evt)) {
                return;
              }

              fileList.forEach(function (file) {
                if (fileAccepted(file, accept) && fileMatchSize(file, _this.props.maxSize, _this.props.minSize)) {
                  acceptedFiles.push(file);
                } else {
                  rejectedFiles.push(file);
                }
              });

              if (!multiple && acceptedFiles.length > 1) {
                // if not in multi mode add any extra accepted files to rejected.
                // This will allow end users to easily ignore a multi file drop in "single" mode.
                rejectedFiles.push.apply(rejectedFiles, _toConsumableArray(acceptedFiles.splice(0)));
              }

              // Update `acceptedFiles` and `rejectedFiles` state
              // This will make children render functions receive the appropriate
              // values
              _this.setState({ acceptedFiles: acceptedFiles, rejectedFiles: rejectedFiles }, function () {
                if (onDrop) {
                  onDrop.call(_this, acceptedFiles, rejectedFiles, evt);
                }

                if (rejectedFiles.length > 0 && onDropRejected) {
                  onDropRejected.call(_this, rejectedFiles, evt);
                }

                if (acceptedFiles.length > 0 && onDropAccepted) {
                  onDropAccepted.call(_this, acceptedFiles, evt);
                }
              });
            });
          }
        }, _this.onClick = function (evt) {
          var onClick = _this.props.onClick;

          // if onClick prop is given, run it first

          if (onClick) {
            onClick.call(_this, evt);
          }

          // If the event hasn't been default prevented from within
          // the onClick listener, open the file dialog
          if (!isDefaultPrevented(evt)) {
            evt.stopPropagation();

            // in IE11/Edge the file-browser dialog is blocking, ensure this is behind setTimeout
            // this is so react can handle state changes in the onClick prop above above
            // see: https://github.com/react-dropzone/react-dropzone/issues/450
            if (isIeOrEdge()) {
              setTimeout(_this.open, 0);
            } else {
              _this.open();
            }
          }
        }, _this.onInputElementClick = function (evt) {
          evt.stopPropagation();
        }, _this.onFileDialogCancel = function () {
          // timeout will not recognize context of this method
          var onFileDialogCancel = _this.props.onFileDialogCancel;
          // execute the timeout only if the FileDialog is opened in the browser

          if (_this.isFileDialogActive) {
            setTimeout(function () {
              if (_this.input != null) {
                // Returns an object as FileList
                var files = _this.input.files;


                if (!files.length) {
                  _this.isFileDialogActive = false;

                  if (typeof onFileDialogCancel === 'function') {
                    onFileDialogCancel();
                  }
                }
              }
            }, 300);
          }
        }, _this.onFocus = function (evt) {
          var onFocus = _this.props.onFocus;

          if (onFocus) {
            onFocus.call(_this, evt);
          }
          if (!isDefaultPrevented(evt)) {
            _this.setState({ isFocused: true });
          }
        }, _this.onBlur = function (evt) {
          var onBlur = _this.props.onBlur;

          if (onBlur) {
            onBlur.call(_this, evt);
          }
          if (!isDefaultPrevented(evt)) {
            _this.setState({ isFocused: false });
          }
        }, _this.onKeyDown = function (evt) {
          var onKeyDown = _this.props.onKeyDown;

          if (!_this.node.isEqualNode(evt.target)) {
            return;
          }

          if (onKeyDown) {
            onKeyDown.call(_this, evt);
          }

          if (!isDefaultPrevented(evt) && (evt.keyCode === 32 || evt.keyCode === 13)) {
            evt.preventDefault();
            _this.open();
          }
        }, _this.composeHandler = function (handler) {
          if (_this.props.disabled) {
            return null;
          }
          return handler;
        }, _this.getRootProps = function () {
          var _extends2;

          var _ref2 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

          var _ref2$refKey = _ref2.refKey,
              refKey = _ref2$refKey === undefined ? 'ref' : _ref2$refKey,
              onKeyDown = _ref2.onKeyDown,
              onFocus = _ref2.onFocus,
              onBlur = _ref2.onBlur,
              onClick = _ref2.onClick,
              onDragStart = _ref2.onDragStart,
              onDragEnter = _ref2.onDragEnter,
              onDragOver = _ref2.onDragOver,
              onDragLeave = _ref2.onDragLeave,
              onDrop = _ref2.onDrop,
              rest = _objectWithoutProperties(_ref2, ['refKey', 'onKeyDown', 'onFocus', 'onBlur', 'onClick', 'onDragStart', 'onDragEnter', 'onDragOver', 'onDragLeave', 'onDrop']);

          return _extends((_extends2 = {
            onKeyDown: _this.composeHandler(onKeyDown ? composeEventHandlers(onKeyDown, _this.onKeyDown) : _this.onKeyDown),
            onFocus: _this.composeHandler(onFocus ? composeEventHandlers(onFocus, _this.onFocus) : _this.onFocus),
            onBlur: _this.composeHandler(onBlur ? composeEventHandlers(onBlur, _this.onBlur) : _this.onBlur),
            onClick: _this.composeHandler(onClick ? composeEventHandlers(onClick, _this.onClick) : _this.onClick),
            onDragStart: _this.composeHandler(onDragStart ? composeEventHandlers(onDragStart, _this.onDragStart) : _this.onDragStart),
            onDragEnter: _this.composeHandler(onDragEnter ? composeEventHandlers(onDragEnter, _this.onDragEnter) : _this.onDragEnter),
            onDragOver: _this.composeHandler(onDragOver ? composeEventHandlers(onDragOver, _this.onDragOver) : _this.onDragOver),
            onDragLeave: _this.composeHandler(onDragLeave ? composeEventHandlers(onDragLeave, _this.onDragLeave) : _this.onDragLeave),
            onDrop: _this.composeHandler(onDrop ? composeEventHandlers(onDrop, _this.onDrop) : _this.onDrop)
          }, _defineProperty(_extends2, refKey, _this.setNodeRef), _defineProperty(_extends2, 'tabIndex', _this.props.disabled ? -1 : 0), _extends2), rest);
        }, _this.getInputProps = function () {
          var _ref3 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

          var _ref3$refKey = _ref3.refKey,
              refKey = _ref3$refKey === undefined ? 'ref' : _ref3$refKey,
              onChange = _ref3.onChange,
              onClick = _ref3.onClick,
              rest = _objectWithoutProperties(_ref3, ['refKey', 'onChange', 'onClick']);

          var _this$props2 = _this.props,
              accept = _this$props2.accept,
              multiple = _this$props2.multiple,
              name = _this$props2.name;

          var inputProps = _defineProperty({
            accept: accept,
            type: 'file',
            style: { display: 'none' },
            multiple: supportMultiple && multiple,
            onChange: composeEventHandlers(onChange, _this.onDrop),
            onClick: composeEventHandlers(onClick, _this.onInputElementClick),
            autoComplete: 'off',
            tabIndex: -1
          }, refKey, _this.setInputRef);
          if (name && name.length) {
            inputProps.name = name;
          }
          return _extends({}, inputProps, rest);
        }, _this.setNodeRef = function (node) {
          _this.node = node;
        }, _this.setInputRef = function (input) {
          _this.input = input;
        }, _this.open = function () {
          _this.isFileDialogActive = true;
          if (_this.input) {
            _this.input.value = null;
            _this.input.click();
          }
        }, _temp), _possibleConstructorReturn(_this, _ret);
      }

      _createClass(Dropzone, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
          var preventDropOnDocument = this.props.preventDropOnDocument;

          this.dragTargets = [];

          if (preventDropOnDocument) {
            document.addEventListener('dragover', onDocumentDragOver, false);
            document.addEventListener('drop', this.onDocumentDrop, false);
          }

          window.addEventListener('focus', this.onFileDialogCancel, false);
        }
      }, {
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
          var preventDropOnDocument = this.props.preventDropOnDocument;

          if (preventDropOnDocument) {
            document.removeEventListener('dragover', onDocumentDragOver);
            document.removeEventListener('drop', this.onDocumentDrop);
          }

          window.removeEventListener('focus', this.onFileDialogCancel, false);
        }

        /**
         * Open system file upload dialog.
         *
         * @public
         */

      }, {
        key: 'render',
        value: function render() {
          var _props = this.props,
              children = _props.children,
              multiple = _props.multiple,
              disabled = _props.disabled;
          var _state = this.state,
              isDragActive = _state.isDragActive,
              isFocused = _state.isFocused,
              draggedFiles = _state.draggedFiles,
              acceptedFiles = _state.acceptedFiles,
              rejectedFiles = _state.rejectedFiles;


          var filesCount = draggedFiles.length;
          var isMultipleAllowed = multiple || filesCount <= 1;
          var isDragAccept = filesCount > 0 && allFilesAccepted(draggedFiles, this.props.accept);
          var isDragReject = filesCount > 0 && (!isDragAccept || !isMultipleAllowed);

          return children({
            isDragActive: isDragActive,
            isDragAccept: isDragAccept,
            isDragReject: isDragReject,
            draggedFiles: draggedFiles,
            acceptedFiles: acceptedFiles,
            rejectedFiles: rejectedFiles,
            isFocused: isFocused && !disabled,
            getRootProps: this.getRootProps,
            getInputProps: this.getInputProps,
            open: this.open
          });
        }
      }]);

      return Dropzone;
    }(React__default.Component);

    Dropzone.propTypes = {
      /**
       * Allow specific types of files. See https://github.com/okonet/attr-accept for more information.
       * Keep in mind that mime type determination is not reliable across platforms. CSV files,
       * for example, are reported as text/plain under macOS but as application/vnd.ms-excel under
       * Windows. In some cases there might not be a mime type set at all.
       * See: https://github.com/react-dropzone/react-dropzone/issues/276
       */
      accept: propTypes.oneOfType([propTypes.string, propTypes.arrayOf(propTypes.string)]),

      /**
       * Render function that renders the actual component
       *
       * @param {Object} props
       * @param {Function} props.getRootProps Returns the props you should apply to the root drop container you render
       * @param {Function} props.getInputProps Returns the props you should apply to hidden file input you render
       * @param {Function} props.open Open the native file selection dialog
       * @param {Boolean} props.isFocused Dropzone area is in focus
       * @param {Boolean} props.isDragActive Active drag is in progress
       * @param {Boolean} props.isDragAccept Dragged files are accepted
       * @param {Boolean} props.isDragReject Some dragged files are rejected
       * @param {Array} props.draggedFiles Files in active drag
       * @param {Array} props.acceptedFiles Accepted files
       * @param {Array} props.rejectedFiles Rejected files
       */
      children: propTypes.func,

      /**
       * Enable/disable the dropzone entirely
       */
      disabled: propTypes.bool,

      /**
       * If false, allow dropped items to take over the current browser window
       */
      preventDropOnDocument: propTypes.bool,

      /**
       * Allow dropping multiple files
       */
      multiple: propTypes.bool,

      /**
       * `name` attribute for the input tag
       */
      name: propTypes.string,

      /**
       * Maximum file size (in bytes)
       */
      maxSize: propTypes.number,

      /**
       * Minimum file size (in bytes)
       */
      minSize: propTypes.number,

      /**
       * getDataTransferItems handler
       * @param {Event} event
       * @returns {Array} array of File objects
       */
      getDataTransferItems: propTypes.func,

      /**
       * onClick callback
       * @param {Event} event
       */
      onClick: propTypes.func,

      /**
       * onFocus callback
       */
      onFocus: propTypes.func,

      /**
       * onBlur callback
       */
      onBlur: propTypes.func,

      /**
       * onKeyDown callback
       */
      onKeyDown: propTypes.func,

      /**
       * The `onDrop` method that accepts two arguments.
       * The first argument represents the accepted files and the second argument the rejected files.
       *
       * ```javascript
       * function onDrop(acceptedFiles, rejectedFiles) {
       *   // do stuff with files...
       * }
       * ```
       *
       * Files are accepted or rejected based on the `accept` prop.
       * This must be a valid [MIME type](http://www.iana.org/assignments/media-types/media-types.xhtml) according to [input element specification](https://www.w3.org/wiki/HTML/Elements/input/file) or a valid file extension.
       *
       * Note that the `onDrop` callback will always be called regardless if the dropped files were accepted or rejected.
       * You can use the `onDropAccepted`/`onDropRejected` props if you'd like to react to a specific event instead of the `onDrop` prop.
       *
       * The `onDrop` callback will provide you with an array of [Files](https://developer.mozilla.org/en-US/docs/Web/API/File) which you can then process and send to a server.
       * For example, with [SuperAgent](https://github.com/visionmedia/superagent) as a http/ajax library:
       *
       * ```javascript
       * function onDrop(acceptedFiles) {
       *   const req = request.post('/upload')
       *   acceptedFiles.forEach(file => {
       *     req.attach(file.name, file)
       *   })
       *   req.end(callback)
       * }
       * ```
       */
      onDrop: propTypes.func,

      /**
       * onDropAccepted callback
       */
      onDropAccepted: propTypes.func,

      /**
       * onDropRejected callback
       */
      onDropRejected: propTypes.func,

      /**
       * onDragStart callback
       */
      onDragStart: propTypes.func,

      /**
       * onDragEnter callback
       */
      onDragEnter: propTypes.func,

      /**
       * onDragOver callback
       */
      onDragOver: propTypes.func,

      /**
       * onDragLeave callback
       */
      onDragLeave: propTypes.func,

      /**
       * Provide a callback on clicking the cancel button of the file dialog
       */
      onFileDialogCancel: propTypes.func
    };

    Dropzone.defaultProps = {
      preventDropOnDocument: true,
      disabled: false,
      multiple: true,
      maxSize: Infinity,
      minSize: 0,
      getDataTransferItems: fromEvent
    };

    const FileUpload = (_a) => {
        var { id, type, value = type === fileReaderType.text || type === fileReaderType.dataURL ? '' : null, filename = '', children = null, onChange = () => { }, onReadStarted = () => { }, onReadFinished = () => { }, onReadFailed = () => { }, dropzoneProps = {} } = _a, props = __rest(_a, ["id", "type", "value", "filename", "children", "onChange", "onReadStarted", "onReadFinished", "onReadFailed", "dropzoneProps"]);
        const onDropAccepted = (acceptedFiles, event) => {
            if (acceptedFiles.length > 0) {
                const fileHandle = acceptedFiles[0];
                if (type === fileReaderType.text || type === fileReaderType.dataURL) {
                    onChange('', fileHandle.name, event); // Show the filename while reading
                    onReadStarted(fileHandle);
                    readFile(fileHandle, type)
                        .then(data => {
                        onReadFinished(fileHandle);
                        onChange(data, fileHandle.name, event);
                    })
                        .catch((error) => {
                        onReadFailed(error, fileHandle);
                        onReadFinished(fileHandle);
                        onChange('', '', event); // Clear the filename field on a failure
                    });
                }
                else {
                    onChange(fileHandle, fileHandle.name, event);
                }
            }
            dropzoneProps.onDropAccepted && dropzoneProps.onDropAccepted(acceptedFiles, event);
        };
        const onDropRejected = (rejectedFiles, event) => {
            if (rejectedFiles.length > 0) {
                onChange('', rejectedFiles[0].name, event);
            }
            dropzoneProps.onDropRejected && dropzoneProps.onDropRejected(rejectedFiles, event);
        };
        const onClearButtonClick = (event) => {
            onChange('', '', event);
        };
        return (React.createElement(Dropzone, Object.assign({ multiple: false }, dropzoneProps, { onDropAccepted: onDropAccepted, onDropRejected: onDropRejected }), ({ getRootProps, getInputProps, isDragActive, open }) => (React.createElement(FileUploadField, Object.assign({}, getRootProps(Object.assign(Object.assign({}, props), { refKey: 'containerRef', onClick: event => event.preventDefault() // Prevents clicking TextArea from opening file dialog
         })), { tabIndex: null, id: id, type: type, filename: filename, value: value, onChange: onChange, isDragActive: isDragActive, onBrowseButtonClick: open, onClearButtonClick: onClearButtonClick }),
            React.createElement("input", Object.assign({}, getInputProps())),
            children))));
    };
    FileUpload.displayName = 'FileUpload';

    const ActionGroup = (_a) => {
        var { children = null, className = '' } = _a, props = __rest(_a, ["children", "className"]);
        const customClassName = css(styles$o.formGroup, styles$o.modifiers.action, className);
        const formActionsComponent = React.createElement("div", { className: css(styles$o.formActions) }, children);
        return (React.createElement("div", Object.assign({}, props, { className: customClassName }),
            React.createElement("div", { className: css(styles$o.formGroupControl) }, formActionsComponent)));
    };
    ActionGroup.displayName = 'ActionGroup';

    const Form = (_a) => {
        var { children = null, className = '', isHorizontal = false } = _a, props = __rest(_a, ["children", "className", "isHorizontal"]);
        return (React.createElement("form", Object.assign({ noValidate: true }, props, { className: css(styles$o.form, isHorizontal && styles$o.modifiers.horizontal, className) }), children));
    };
    Form.displayName = 'Form';

    const FormGroup = (_a) => {
        var { children = null, className = '', label, labelIcon, isRequired = false, validated = 'default', isInline = false, hasNoPaddingTop = false, helperText, helperTextInvalid, helperTextIcon, helperTextInvalidIcon, fieldId } = _a, props = __rest(_a, ["children", "className", "label", "labelIcon", "isRequired", "validated", "isInline", "hasNoPaddingTop", "helperText", "helperTextInvalid", "helperTextIcon", "helperTextInvalidIcon", "fieldId"]);
        const validHelperText = typeof helperText !== 'string' ? (helperText) : (React.createElement("div", { className: css(styles$o.formHelperText, validated === exports.ValidatedOptions.success && styles$o.modifiers.success, validated === exports.ValidatedOptions.warning && styles$o.modifiers.warning), id: `${fieldId}-helper`, "aria-live": "polite" },
            helperTextIcon && React.createElement("span", { className: css(styles$o.formHelperTextIcon) }, helperTextIcon),
            helperText));
        const inValidHelperText = typeof helperTextInvalid !== 'string' ? (helperTextInvalid) : (React.createElement("div", { className: css(styles$o.formHelperText, styles$o.modifiers.error), id: `${fieldId}-helper`, "aria-live": "polite" },
            helperTextInvalidIcon && React.createElement("span", { className: css(styles$o.formHelperTextIcon) }, helperTextInvalidIcon),
            helperTextInvalid));
        const showValidHelperTxt = (validationType) => validationType !== exports.ValidatedOptions.error && helperText ? validHelperText : '';
        return (React.createElement("div", Object.assign({}, props, { className: css(styles$o.formGroup, className) }),
            label && (React.createElement("div", { className: css(styles$o.formGroupLabel, hasNoPaddingTop && styles$o.modifiers.noPaddingTop) },
                React.createElement("label", { className: css(styles$o.formLabel), htmlFor: fieldId },
                    React.createElement("span", { className: css(styles$o.formLabelText) }, label),
                    isRequired && (React.createElement("span", { className: css(styles$o.formLabelRequired), "aria-hidden": "true" },
                        ' ',
                        ASTERISK))),
                ' ',
                React.isValidElement(labelIcon) && labelIcon)),
            React.createElement("div", { className: css(styles$o.formGroupControl, isInline && styles$o.modifiers.inline) },
                children,
                validated === exports.ValidatedOptions.error && helperTextInvalid ? inValidHelperText : showValidHelperTxt(validated))));
    };
    FormGroup.displayName = 'FormGroup';

    const FormHelperText = (_a) => {
        var { children = null, isError = false, isHidden = true, className = '', icon = null } = _a, props = __rest(_a, ["children", "isError", "isHidden", "className", "icon"]);
        return (React.createElement("p", Object.assign({ className: css(styles$o.formHelperText, isError && styles$o.modifiers.error, isHidden && styles$o.modifiers.hidden, className) }, props),
            icon && React.createElement("span", { className: css(styles$o.formHelperTextIcon) }, icon),
            children));
    };
    FormHelperText.displayName = 'FormHelperText';

    var label = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "button": "pf-c-button",
      "label": "pf-c-label",
      "labelContent": "pf-c-label__content",
      "labelIcon": "pf-c-label__icon",
      "modifiers": {
        "blue": "pf-m-blue",
        "green": "pf-m-green",
        "orange": "pf-m-orange",
        "red": "pf-m-red",
        "purple": "pf-m-purple",
        "cyan": "pf-m-cyan",
        "outline": "pf-m-outline"
      }
    };
    });

    var styles$u = unwrapExports(label);

    const colorStyles = {
        blue: styles$u.modifiers.blue,
        cyan: styles$u.modifiers.cyan,
        green: styles$u.modifiers.green,
        orange: styles$u.modifiers.orange,
        purple: styles$u.modifiers.purple,
        red: styles$u.modifiers.red,
        grey: ''
    };
    const Label = (_a) => {
        var { children, className = '', color = 'grey', variant = 'filled', icon, onClose, closeBtn, closeBtnProps, href } = _a, props = __rest(_a, ["children", "className", "color", "variant", "icon", "onClose", "closeBtn", "closeBtnProps", "href"]);
        const Component = href ? 'a' : 'span';
        const button = closeBtn ? (closeBtn) : (React.createElement(Button, Object.assign({ type: "button", variant: "plain", onClick: onClose }, Object.assign({ 'aria-label': 'label-close-button' }, closeBtnProps)),
            React.createElement(TimesIcon, null)));
        return (React.createElement("span", Object.assign({}, props, { className: css(styles$u.label, colorStyles[color], variant === 'outline' && styles$u.modifiers.outline, className) }),
            React.createElement(Component, Object.assign({ className: css(styles$u.labelContent) }, (href && { href })),
                icon && React.createElement("span", { className: css(styles$u.labelIcon) }, icon),
                children),
            onClose && button));
    };
    Label.displayName = 'Label';

    var list = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "list": "pf-c-list",
      "modifiers": {
        "inline": "pf-m-inline"
      }
    };
    });

    var styles$v = unwrapExports(list);

    (function (OrderType) {
        OrderType["number"] = "1";
        OrderType["lowercaseLetter"] = "a";
        OrderType["uppercaseLetter"] = "A";
        OrderType["lowercaseRomanNumber"] = "i";
        OrderType["uppercaseRomanNumber"] = "I";
    })(exports.OrderType || (exports.OrderType = {}));
    (function (ListVariant) {
        ListVariant["inline"] = "inline";
    })(exports.ListVariant || (exports.ListVariant = {}));
    (function (ListComponent) {
        ListComponent["ol"] = "ol";
        ListComponent["ul"] = "ul";
    })(exports.ListComponent || (exports.ListComponent = {}));
    const List = (_a) => {
        var { className = '', children = null, variant = null, type = exports.OrderType.number, ref = null, component = exports.ListComponent.ul } = _a, props = __rest(_a, ["className", "children", "variant", "type", "ref", "component"]);
        return component === exports.ListComponent.ol ? (React.createElement("ol", Object.assign({ ref: ref, type: type }, props, { className: css(styles$v.list, variant && styles$v.modifiers[variant], className) }), children)) : (React.createElement("ul", Object.assign({ ref: ref }, props, { className: css(styles$v.list, variant && styles$v.modifiers[variant], className) }), children));
    };
    List.displayName = 'List';

    const ListItem = (_a) => {
        var { children = null } = _a, props = __rest(_a, ["children"]);
        return (React.createElement("li", Object.assign({}, props), children));
    };
    ListItem.displayName = 'ListItem';

    var login = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "brand": "pf-c-brand",
      "button": "pf-c-button",
      "card": "pf-c-card",
      "dropdown": "pf-c-dropdown",
      "list": "pf-c-list",
      "login": "pf-c-login",
      "loginContainer": "pf-c-login__container",
      "loginFooter": "pf-c-login__footer",
      "loginHeader": "pf-c-login__header",
      "loginMain": "pf-c-login__main",
      "loginMainBody": "pf-c-login__main-body",
      "loginMainFooter": "pf-c-login__main-footer",
      "loginMainFooterBand": "pf-c-login__main-footer-band",
      "loginMainFooterLinks": "pf-c-login__main-footer-links",
      "loginMainFooterLinksItem": "pf-c-login__main-footer-links-item",
      "loginMainFooterLinksItemLink": "pf-c-login__main-footer-links-item-link",
      "loginMainHeader": "pf-c-login__main-header",
      "loginMainHeaderDesc": "pf-c-login__main-header-desc",
      "title": "pf-c-title"
    };
    });

    var styles$w = unwrapExports(login);

    const Login = (_a) => {
        var { className = '', children = null, footer = null, header = null } = _a, props = __rest(_a, ["className", "children", "footer", "header"]);
        return (React.createElement("div", Object.assign({}, props, { className: css(styles$w.login, className) }),
            React.createElement("div", { className: css(styles$w.loginContainer) },
                header,
                React.createElement("main", { className: css(styles$w.loginMain) }, children),
                footer)));
    };
    Login.displayName = 'Login';

    const LoginHeader = (_a) => {
        var { className = '', children = null, headerBrand = null } = _a, props = __rest(_a, ["className", "children", "headerBrand"]);
        return (React.createElement("header", Object.assign({ className: css(styles$w.loginHeader, className) }, props),
            headerBrand,
            children));
    };
    LoginHeader.displayName = 'LoginHeader';

    const LoginFooter = (_a) => {
        var { className = '', children = null } = _a, props = __rest(_a, ["className", "children"]);
        return (React.createElement("footer", Object.assign({ className: css(styles$w.loginFooter, className) }, props), children));
    };
    LoginFooter.displayName = 'LoginFooter';

    const LoginMainHeader = (_a) => {
        var { children = null, className = '', title = '', subtitle = '' } = _a, props = __rest(_a, ["children", "className", "title", "subtitle"]);
        return (React.createElement("header", Object.assign({ className: css(styles$w.loginMainHeader, className) }, props),
            title && (React.createElement(Title, { headingLevel: "h2", size: exports.TitleSizes['3xl'] }, title)),
            subtitle && React.createElement("p", { className: css(styles$w.loginMainHeaderDesc) }, subtitle),
            children));
    };
    LoginMainHeader.displayName = 'LoginMainHeader';

    const LoginMainBody = (_a) => {
        var { children = null, className = '' } = _a, props = __rest(_a, ["children", "className"]);
        return (React.createElement("div", Object.assign({ className: css(styles$w.loginMainBody, className) }, props), children));
    };
    LoginMainBody.displayName = 'LoginMainBody';

    const LoginMainFooter = (_a) => {
        var { children = null, socialMediaLoginContent = null, signUpForAccountMessage = null, forgotCredentials = null, className = '' } = _a, props = __rest(_a, ["children", "socialMediaLoginContent", "signUpForAccountMessage", "forgotCredentials", "className"]);
        return (React.createElement("div", Object.assign({ className: css(styles$w.loginMainFooter, className) }, props),
            children,
            socialMediaLoginContent && React.createElement("ul", { className: css(styles$w.loginMainFooterLinks) }, socialMediaLoginContent),
            (signUpForAccountMessage || forgotCredentials) && (React.createElement("div", { className: css(styles$w.loginMainFooterBand) },
                signUpForAccountMessage,
                forgotCredentials))));
    };
    LoginMainFooter.displayName = 'LoginMainFooter';

    const LoginPage = (_a) => {
        var { children = null, className = '', brandImgSrc = '', brandImgAlt = '', backgroundImgSrc = '', backgroundImgAlt = '', footerListItems = null, textContent = '', footerListVariants, loginTitle, loginSubtitle, signUpForAccountMessage = null, forgotCredentials = null, socialMediaLoginContent = null } = _a, props = __rest(_a, ["children", "className", "brandImgSrc", "brandImgAlt", "backgroundImgSrc", "backgroundImgAlt", "footerListItems", "textContent", "footerListVariants", "loginTitle", "loginSubtitle", "signUpForAccountMessage", "forgotCredentials", "socialMediaLoginContent"]);
        const HeaderBrand = (React.createElement(React.Fragment, null,
            React.createElement(Brand, { src: brandImgSrc, alt: brandImgAlt })));
        const Header = React.createElement(LoginHeader, { headerBrand: HeaderBrand });
        const Footer = (React.createElement(LoginFooter, null,
            React.createElement("p", null, textContent),
            React.createElement(List, { variant: footerListVariants }, footerListItems)));
        return (React.createElement(React.Fragment, null,
            backgroundImgSrc && React.createElement(BackgroundImage, { src: backgroundImgSrc, alt: backgroundImgAlt }),
            React.createElement(Login, Object.assign({ header: Header, footer: Footer, className: css(className) }, props),
                React.createElement(LoginMainHeader, { title: loginTitle, subtitle: loginSubtitle }),
                React.createElement(LoginMainBody, null, children),
                (socialMediaLoginContent || forgotCredentials || signUpForAccountMessage) && (React.createElement(LoginMainFooter, { socialMediaLoginContent: socialMediaLoginContent, forgotCredentials: forgotCredentials, signUpForAccountMessage: signUpForAccountMessage })))));
    };
    LoginPage.displayName = 'LoginPage';

    const LoginForm = (_a) => {
        var { noAutoFocus = false, className = '', showHelperText = false, helperText = null, helperTextIcon = null, usernameLabel = 'Username', usernameValue = '', onChangeUsername = () => undefined, isValidUsername = true, passwordLabel = 'Password', passwordValue = '', onChangePassword = () => undefined, isValidPassword = true, loginButtonLabel = 'Log In', isLoginButtonDisabled = false, onLoginButtonClick = () => undefined, rememberMeLabel = '', isRememberMeChecked = false, onChangeRememberMe = () => undefined } = _a, props = __rest(_a, ["noAutoFocus", "className", "showHelperText", "helperText", "helperTextIcon", "usernameLabel", "usernameValue", "onChangeUsername", "isValidUsername", "passwordLabel", "passwordValue", "onChangePassword", "isValidPassword", "loginButtonLabel", "isLoginButtonDisabled", "onLoginButtonClick", "rememberMeLabel", "isRememberMeChecked", "onChangeRememberMe"]);
        return (React.createElement(Form, Object.assign({ className: className }, props),
            React.createElement(FormHelperText, { isError: !isValidUsername || !isValidPassword, isHidden: !showHelperText, icon: helperTextIcon }, helperText),
            React.createElement(FormGroup, { label: usernameLabel, isRequired: true, validated: isValidUsername ? exports.ValidatedOptions.default : exports.ValidatedOptions.error, fieldId: "pf-login-username-id" },
                React.createElement(TextInput, { autoFocus: !noAutoFocus, id: "pf-login-username-id", isRequired: true, validated: isValidUsername ? exports.ValidatedOptions.default : exports.ValidatedOptions.error, type: "text", name: "pf-login-username-id", value: usernameValue, onChange: onChangeUsername })),
            React.createElement(FormGroup, { label: passwordLabel, isRequired: true, validated: isValidPassword ? exports.ValidatedOptions.default : exports.ValidatedOptions.error, fieldId: "pf-login-password-id" },
                React.createElement(TextInput, { isRequired: true, type: "password", id: "pf-login-password-id", name: "pf-login-password-id", validated: isValidPassword ? exports.ValidatedOptions.default : exports.ValidatedOptions.error, value: passwordValue, onChange: onChangePassword })),
            rememberMeLabel.length > 0 && (React.createElement(FormGroup, { fieldId: "pf-login-remember-me-id" },
                React.createElement(Checkbox, { id: "pf-login-remember-me-id", label: rememberMeLabel, isChecked: isRememberMeChecked, onChange: onChangeRememberMe }))),
            React.createElement(ActionGroup, null,
                React.createElement(Button, { variant: "primary", type: "submit", onClick: onLoginButtonClick, isBlock: true, isDisabled: isLoginButtonDisabled }, loginButtonLabel))));
    };
    LoginForm.displayName = 'LoginForm';

    const LoginFooterItem = (_a) => {
        var { 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        className = '', children = null, href = '#', target = '_blank' } = _a, props = __rest(_a, ["className", "children", "href", "target"]);
        return React.isValidElement(children) ? (children) : (React.createElement("a", Object.assign({ target: target, href: href }, props), children));
    };
    LoginFooterItem.displayName = 'LoginFooterItem';

    const LoginMainFooterBandItem = (_a) => {
        var { children = null, className = '' } = _a, props = __rest(_a, ["children", "className"]);
        return (React.createElement("p", Object.assign({ className: css(`${styles$w.loginMainFooterBand}-item`, className) }, props), children));
    };
    LoginMainFooterBandItem.displayName = 'LoginMainFooterBandItem';

    const LoginMainFooterLinksItem = (_a) => {
        var { children = null, href = '', target = '', className = '', linkComponent = 'a', linkComponentProps } = _a, props = __rest(_a, ["children", "href", "target", "className", "linkComponent", "linkComponentProps"]);
        const LinkComponent = linkComponent;
        return (React.createElement("li", Object.assign({ className: css(styles$w.loginMainFooterLinksItem, className) }, props),
            React.createElement(LinkComponent, Object.assign({ className: css(styles$w.loginMainFooterLinksItemLink), href: href, target: target }, linkComponentProps), children)));
    };
    LoginMainFooterLinksItem.displayName = 'LoginMainFooterLinksItem';

    var modalBox = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "button": "pf-c-button",
      "modalBox": "pf-c-modal-box",
      "modalBoxBody": "pf-c-modal-box__body",
      "modalBoxDescription": "pf-c-modal-box__description",
      "modalBoxFooter": "pf-c-modal-box__footer",
      "modalBoxHeader": "pf-c-modal-box__header",
      "modalBoxTitle": "pf-c-modal-box__title",
      "modifiers": {
        "sm": "pf-m-sm",
        "md": "pf-m-md",
        "lg": "pf-m-lg"
      }
    };
    });

    var modalStyles = unwrapExports(modalBox);

    const ModalBoxBody = (_a) => {
        var { children = null, className = '' } = _a, props = __rest(_a, ["children", "className"]);
        return (React.createElement("div", Object.assign({}, props, { className: css(modalStyles.modalBoxBody, className) }), children));
    };
    ModalBoxBody.displayName = 'ModalBoxBody';

    const ModalBoxCloseButton = (_a) => {
        var { className = '', onClose = () => undefined } = _a, props = __rest(_a, ["className", "onClose"]);
        return (React.createElement(Button, Object.assign({ className: className, variant: "plain", onClick: onClose, "aria-label": "Close" }, props),
            React.createElement(TimesIcon, null)));
    };
    ModalBoxCloseButton.displayName = 'ModalBoxCloseButton';

    const ModalBox = (_a) => {
        var { children, className = '', variant = 'default', 'aria-labelledby': ariaLabelledby, 'aria-label': ariaLabel = '', 'aria-describedby': ariaDescribedby } = _a, props = __rest(_a, ["children", "className", "variant", 'aria-labelledby', 'aria-label', 'aria-describedby']);
        return (React.createElement("div", Object.assign({}, props, { role: "dialog", "aria-label": ariaLabel || null, "aria-labelledby": ariaLabelledby || null, "aria-describedby": ariaDescribedby, "aria-modal": "true", className: css(modalStyles.modalBox, className, variant === 'large' && modalStyles.modifiers.lg, variant === 'small' && modalStyles.modifiers.sm) }), children));
    };
    ModalBox.displayName = 'ModalBox';

    const ModalBoxFooter = (_a) => {
        var { children = null, className = '' } = _a, props = __rest(_a, ["children", "className"]);
        return (React.createElement("footer", Object.assign({}, props, { className: css(modalStyles.modalBoxFooter, className) }), children));
    };
    ModalBoxFooter.displayName = 'ModalBoxFooter';

    const ModalBoxDescription = (_a) => {
        var { children = null, className = '', id = '' } = _a, props = __rest(_a, ["children", "className", "id"]);
        return (React.createElement("div", Object.assign({}, props, { id: id, className: css(modalStyles.modalBoxDescription, className) }), children));
    };
    ModalBoxDescription.displayName = 'ModalBoxDescription';

    const ModalBoxHeader = (_a) => {
        var { children = null, className = '' } = _a, props = __rest(_a, ["children", "className"]);
        return (React.createElement("header", Object.assign({ className: css('pf-c-modal-box__header', className) }, props), children));
    };
    ModalBoxHeader.displayName = 'ModalBoxHeader';

    const ModalBoxTitle = (_a) => {
        var { className = '', id, title } = _a, props = __rest(_a, ["className", "id", "title"]);
        const [isTooltipVisible, setIsTooltipVisible] = React.useState(false);
        const h1 = React.useRef();
        React.useLayoutEffect(() => {
            setIsTooltipVisible(h1.current && h1.current.offsetWidth < h1.current.scrollWidth);
        }, []);
        return isTooltipVisible ? (React.createElement(Tooltip, { content: title },
            React.createElement("h1", Object.assign({ id: id, ref: h1, className: css(modalStyles.modalBoxTitle, className) }, props), title))) : (React.createElement("h1", Object.assign({ id: id, ref: h1, className: css(modalStyles.modalBoxTitle, className) }, props), title));
    };
    ModalBoxTitle.displayName = 'ModalBoxTitle';

    const ModalContent = (_a) => {
        var { children, className = '', isOpen = false, header = null, description = null, title = '', 'aria-label': ariaLabel = '', 'aria-describedby': ariaDescribedby, 'aria-labelledby': ariaLabelledby, showClose = true, footer = null, actions = [], onClose = () => undefined, variant = 'default', width = -1, boxId, labelId, descriptorId, disableFocusTrap = false, hasNoBodyWrapper = false, ouiaId, ouiaSafe = true } = _a, props = __rest(_a, ["children", "className", "isOpen", "header", "description", "title", 'aria-label', 'aria-describedby', 'aria-labelledby', "showClose", "footer", "actions", "onClose", "variant", "width", "boxId", "labelId", "descriptorId", "disableFocusTrap", "hasNoBodyWrapper", "ouiaId", "ouiaSafe"]);
        if (!isOpen) {
            return null;
        }
        const modalBoxHeader = header ? (React.createElement(ModalBoxHeader, null, header)) : (title && (React.createElement(ModalBoxHeader, null,
            React.createElement(ModalBoxTitle, { title: title, id: labelId, className: css(modalStyles.modalBoxTitle) }),
            description && React.createElement(ModalBoxDescription, { id: descriptorId }, description))));
        const modalBoxFooter = footer ? (React.createElement(ModalBoxFooter, null, footer)) : (actions.length > 0 && React.createElement(ModalBoxFooter, null, actions));
        const modalBody = hasNoBodyWrapper ? (children) : (React.createElement(ModalBoxBody, Object.assign({}, props, (!description && !ariaDescribedby && { id: descriptorId })), children));
        const boxStyle = width === -1 ? {} : { width };
        const ariaLabelledbyFormatted = () => {
            if (ariaLabelledby === null) {
                return null;
            }
            const idRefList = [];
            if ((ariaLabel && boxId) !== '') {
                idRefList.push(ariaLabel && boxId);
            }
            if (ariaLabelledby) {
                idRefList.push(ariaLabelledby);
            }
            if (title) {
                idRefList.push(labelId);
            }
            return idRefList.join(' ');
        };
        const modalBox = (React.createElement(ModalBox, Object.assign({ id: boxId, style: boxStyle, className: className, variant: variant, "aria-label": ariaLabel, "aria-labelledby": ariaLabelledbyFormatted(), "aria-describedby": ariaDescribedby || (hasNoBodyWrapper ? null : descriptorId) }, getOUIAProps(ModalContent.displayName, ouiaId, ouiaSafe)),
            showClose && React.createElement(ModalBoxCloseButton, { onClose: onClose }),
            modalBoxHeader,
            modalBody,
            modalBoxFooter));
        return (React.createElement(Backdrop, null,
            React.createElement(FocusTrap, { active: !disableFocusTrap, focusTrapOptions: { clickOutsideDeactivates: true }, className: css(styles$1.bullseye) }, modalBox)));
    };
    ModalContent.displayName = 'ModalContent';

    (function (ModalVariant) {
        ModalVariant["small"] = "small";
        ModalVariant["large"] = "large";
        ModalVariant["default"] = "default";
    })(exports.ModalVariant || (exports.ModalVariant = {}));
    class Modal extends React.Component {
        constructor(props) {
            super(props);
            this.boxId = '';
            this.labelId = '';
            this.descriptorId = '';
            this.handleEscKeyClick = (event) => {
                const { onEscapePress } = this.props;
                if (event.keyCode === KEY_CODES.ESCAPE_KEY && this.props.isOpen) {
                    onEscapePress ? onEscapePress(event) : this.props.onClose();
                }
            };
            this.getElement = (appendTo) => {
                let target;
                if (typeof appendTo === 'function') {
                    target = appendTo();
                }
                else {
                    target = appendTo;
                }
                return target;
            };
            this.toggleSiblingsFromScreenReaders = (hide) => {
                const { appendTo } = this.props;
                const target = this.getElement(appendTo);
                const bodyChildren = target.children;
                for (const child of Array.from(bodyChildren)) {
                    if (child !== this.state.container) {
                        hide ? child.setAttribute('aria-hidden', '' + hide) : child.removeAttribute('aria-hidden');
                    }
                }
            };
            this.isEmpty = (value) => value === null || value === undefined || value === '';
            const boxIdNum = Modal.currentId++;
            const labelIdNum = boxIdNum + 1;
            const descriptorIdNum = boxIdNum + 2;
            this.boxId = props.id || `pf-modal-part-${boxIdNum}`;
            this.labelId = `pf-modal-part-${labelIdNum}`;
            this.descriptorId = `pf-modal-part-${descriptorIdNum}`;
            this.state = {
                container: undefined
            };
        }
        componentDidMount() {
            const { appendTo, title, 'aria-label': ariaLabel, 'aria-labelledby': ariaLabelledby, hasNoBodyWrapper, header } = this.props;
            const target = this.getElement(appendTo);
            const container = document.createElement('div');
            this.setState({ container });
            target.appendChild(container);
            target.addEventListener('keydown', this.handleEscKeyClick, false);
            if (this.props.isOpen) {
                target.classList.add(css(styles.backdropOpen));
            }
            else {
                target.classList.remove(css(styles.backdropOpen));
            }
            if (this.isEmpty(title) && this.isEmpty(ariaLabel) && this.isEmpty(ariaLabelledby)) {
                // eslint-disable-next-line no-console
                console.error('Modal: Specify at least one of: title, aria-label, aria-labelledby.');
            }
            if (this.isEmpty(ariaLabel) && this.isEmpty(ariaLabelledby) && (hasNoBodyWrapper || header)) {
                // eslint-disable-next-line no-console
                console.error('Modal: When using hasNoBodyWrapper or setting a custom header, ensure you assign an accessible name to the the modal container with aria-label or aria-labelledby.');
            }
        }
        componentDidUpdate() {
            const { appendTo } = this.props;
            const target = this.getElement(appendTo);
            if (this.props.isOpen) {
                target.classList.add(css(styles.backdropOpen));
                this.toggleSiblingsFromScreenReaders(true);
            }
            else {
                target.classList.remove(css(styles.backdropOpen));
                this.toggleSiblingsFromScreenReaders(false);
            }
        }
        componentWillUnmount() {
            const { appendTo } = this.props;
            const target = this.getElement(appendTo);
            if (this.state.container) {
                target.removeChild(this.state.container);
            }
            target.removeEventListener('keydown', this.handleEscKeyClick, false);
            target.classList.remove(css(styles.backdropOpen));
        }
        render() {
            const _a = this.props, { 
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            appendTo, 
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            onEscapePress, 'aria-labelledby': ariaLabelledby, 'aria-label': ariaLabel, 'aria-describedby': ariaDescribedby, title, ouiaId, ouiaSafe } = _a, props = __rest(_a, ["appendTo", "onEscapePress", 'aria-labelledby', 'aria-label', 'aria-describedby', "title", "ouiaId", "ouiaSafe"]);
            const { container } = this.state;
            if (!canUseDOM || !container) {
                return null;
            }
            return ReactDOM.createPortal(React.createElement(ModalContent, Object.assign({}, props, { boxId: this.boxId, labelId: this.labelId, descriptorId: this.descriptorId, title: title, "aria-label": ariaLabel, "aria-describedby": ariaDescribedby, "aria-labelledby": ariaLabelledby, ouiaId: ouiaId, ouiaSafe: ouiaSafe })), container);
        }
    }
    Modal.displayName = 'Modal';
    Modal.currentId = 0;
    Modal.defaultProps = {
        className: '',
        isOpen: false,
        title: '',
        'aria-label': '',
        showClose: true,
        'aria-describedby': '',
        'aria-labelledby': '',
        id: undefined,
        actions: [],
        onClose: () => undefined,
        variant: 'default',
        hasNoBodyWrapper: false,
        appendTo: (typeof document !== 'undefined' && document.body) || null,
        ouiaSafe: true
    };

    var nav = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "divider": "pf-c-divider",
      "modifiers": {
        "horizontal": "pf-m-horizontal",
        "tertiary": "pf-m-tertiary",
        "light": "pf-m-light",
        "scrollable": "pf-m-scrollable",
        "expandable": "pf-m-expandable",
        "current": "pf-m-current",
        "expanded": "pf-m-expanded"
      },
      "nav": "pf-c-nav",
      "navItem": "pf-c-nav__item",
      "navLink": "pf-c-nav__link",
      "navList": "pf-c-nav__list",
      "navScrollButton": "pf-c-nav__scroll-button",
      "navSection": "pf-c-nav__section",
      "navSectionTitle": "pf-c-nav__section-title",
      "navSubnav": "pf-c-nav__subnav",
      "navToggle": "pf-c-nav__toggle",
      "navToggleIcon": "pf-c-nav__toggle-icon"
    };
    });

    var styles$x = unwrapExports(nav);

    const NavContext = React.createContext({});
    class Nav extends React.Component {
        constructor() {
            super(...arguments);
            this.state = {
                isScrollable: false
            };
        }
        // Callback from NavItem
        onSelect(event, groupId, itemId, to, preventDefault, onClick) {
            if (preventDefault) {
                event.preventDefault();
            }
            this.props.onSelect({ groupId, itemId, event, to });
            if (onClick) {
                onClick(event, itemId, groupId, to);
            }
        }
        // Callback from NavExpandable
        onToggle(event, groupId, toggleValue) {
            this.props.onToggle({
                event,
                groupId,
                isExpanded: toggleValue
            });
        }
        render() {
            const _a = this.props, { 'aria-label': ariaLabel, children, className, 
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            onSelect, 
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            onToggle, theme, ouiaId, ouiaSafe, variant } = _a, props = __rest(_a, ['aria-label', "children", "className", "onSelect", "onToggle", "theme", "ouiaId", "ouiaSafe", "variant"]);
            const isHorizontal = ['horizontal', 'tertiary'].includes(variant);
            return (React.createElement(NavContext.Provider, { value: {
                    onSelect: (event, groupId, itemId, to, preventDefault, onClick) => this.onSelect(event, groupId, itemId, to, preventDefault, onClick),
                    onToggle: (event, groupId, expanded) => this.onToggle(event, groupId, expanded),
                    updateIsScrollable: (isScrollable) => this.setState({ isScrollable }),
                    isHorizontal
                } },
                React.createElement("nav", Object.assign({ className: css(styles$x.nav, theme === 'light' && styles$x.modifiers.light, isHorizontal && styles$x.modifiers.horizontal, variant === 'tertiary' && styles$x.modifiers.tertiary, this.state.isScrollable && styles$x.modifiers.scrollable, className), "aria-label": ariaLabel || variant === 'tertiary' ? 'Local' : 'Global' }, getOUIAProps(Nav.displayName, ouiaId, ouiaSafe), props), children)));
        }
    }
    Nav.displayName = 'Nav';
    Nav.defaultProps = {
        onSelect: () => undefined,
        onToggle: () => undefined,
        theme: 'dark',
        ouiaSafe: true
    };

    var angleLeftIcon = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.AngleLeftIconConfig = {
      name: 'AngleLeftIcon',
      height: 512,
      width: 256,
      svgPath: 'M31.7 239l136-136c9.4-9.4 24.6-9.4 33.9 0l22.6 22.6c9.4 9.4 9.4 24.6 0 33.9L127.9 256l96.4 96.4c9.4 9.4 9.4 24.6 0 33.9L201.7 409c-9.4 9.4-24.6 9.4-33.9 0l-136-136c-9.5-9.4-9.5-24.6-.1-34z',
      yOffset: 0,
      xOffset: 0,
      transform: ''
    };
    exports.AngleLeftIcon = createIcon_1.createIcon(exports.AngleLeftIconConfig);
    exports["default"] = exports.AngleLeftIcon;
    });

    var AngleLeftIcon = unwrapExports(angleLeftIcon);
    var angleLeftIcon_1 = angleLeftIcon.AngleLeftIconConfig;
    var angleLeftIcon_2 = angleLeftIcon.AngleLeftIcon;

    class NavList extends React.Component {
        constructor() {
            super(...arguments);
            this.state = {
                scrollViewAtStart: false,
                scrollViewAtEnd: false
            };
            this.navList = React.createRef();
            this.handleScrollButtons = () => {
                const container = this.navList.current;
                if (container) {
                    // check if it elements are in view
                    const scrollViewAtStart = isElementInView(container, container.firstChild, false);
                    const scrollViewAtEnd = isElementInView(container, container.lastChild, false);
                    this.setState({
                        scrollViewAtStart,
                        scrollViewAtEnd
                    });
                    this.context.updateIsScrollable(!scrollViewAtStart || !scrollViewAtEnd);
                }
            };
            this.scrollLeft = () => {
                // find first Element that is fully in view on the left, then scroll to the element before it
                const container = this.navList.current;
                if (container) {
                    const childrenArr = Array.from(container.children);
                    let firstElementInView;
                    let lastElementOutOfView;
                    for (let i = 0; i < childrenArr.length && !firstElementInView; i++) {
                        if (isElementInView(container, childrenArr[i], false)) {
                            firstElementInView = childrenArr[i];
                            lastElementOutOfView = childrenArr[i - 1];
                        }
                    }
                    if (lastElementOutOfView) {
                        container.scrollLeft -= lastElementOutOfView.scrollWidth;
                    }
                    this.handleScrollButtons();
                }
            };
            this.scrollRight = () => {
                // find last Element that is fully in view on the right, then scroll to the element after it
                const container = this.navList.current;
                if (container) {
                    const childrenArr = Array.from(container.children);
                    let lastElementInView;
                    let firstElementOutOfView;
                    for (let i = childrenArr.length - 1; i >= 0 && !lastElementInView; i--) {
                        if (isElementInView(container, childrenArr[i], false)) {
                            lastElementInView = childrenArr[i];
                            firstElementOutOfView = childrenArr[i + 1];
                        }
                    }
                    if (firstElementOutOfView) {
                        container.scrollLeft += firstElementOutOfView.scrollWidth;
                    }
                    this.handleScrollButtons();
                }
            };
        }
        componentDidMount() {
            window.addEventListener('resize', this.handleScrollButtons, false);
            this.handleScrollButtons();
        }
        componentWillUnmount() {
            window.removeEventListener('resize', this.handleScrollButtons, false);
        }
        render() {
            const _a = this.props, { children, className, ariaLeftScroll, ariaRightScroll } = _a, props = __rest(_a, ["children", "className", "ariaLeftScroll", "ariaRightScroll"]);
            const { scrollViewAtStart, scrollViewAtEnd } = this.state;
            return (React.createElement(NavContext.Consumer, null, ({ isHorizontal }) => (React.createElement(React.Fragment, null,
                isHorizontal && (React.createElement("button", { className: css(styles$x.navScrollButton), "aria-label": ariaLeftScroll, onClick: this.scrollLeft, disabled: scrollViewAtStart },
                    React.createElement(AngleLeftIcon, null))),
                React.createElement("ul", Object.assign({ ref: this.navList, className: css(styles$x.navList, className), onScroll: this.handleScrollButtons }, props), children),
                isHorizontal && (React.createElement("button", { className: css(styles$x.navScrollButton), "aria-label": ariaRightScroll, onClick: this.scrollRight, disabled: scrollViewAtEnd },
                    React.createElement(AngleRightIcon, null)))))));
        }
    }
    NavList.displayName = 'NavList';
    NavList.contextType = NavContext;
    NavList.defaultProps = {
        ariaLeftScroll: 'Scroll left',
        ariaRightScroll: 'Scroll right'
    };

    const NavGroup = (_a) => {
        var { title, children = null, className = '', id = getUniqueId() } = _a, props = __rest(_a, ["title", "children", "className", "id"]);
        return (React.createElement("section", Object.assign({ className: css(styles$x.navSection, className), "aria-labelledby": id }, props),
            React.createElement("h2", { className: css(styles$x.navSectionTitle), id: id }, title),
            React.createElement("ul", null, children)));
    };
    NavGroup.displayName = 'NavGroup';

    const NavItem = (_a) => {
        var { children, styleChildren = true, className, to, isActive = false, groupId = null, itemId = null, preventDefault = false, onClick = null, component = 'a' } = _a, props = __rest(_a, ["children", "styleChildren", "className", "to", "isActive", "groupId", "itemId", "preventDefault", "onClick", "component"]);
        const Component = component;
        const renderDefaultLink = (context) => {
            const preventLinkDefault = preventDefault || !to;
            return (React.createElement(Component, Object.assign({ href: to, onClick: (e) => context.onSelect(e, groupId, itemId, to, preventLinkDefault, onClick), className: css(styles$x.navLink, isActive && styles$x.modifiers.current, className), "aria-current": isActive ? 'page' : null }, props), children));
        };
        const renderClonedChild = (context, child) => React.cloneElement(child, Object.assign({ onClick: (e) => context.onSelect(e, groupId, itemId, to, preventDefault, onClick), 'aria-current': isActive ? 'page' : null }, (styleChildren && {
            className: css(styles$x.navLink, isActive && styles$x.modifiers.current, child.props && child.props.className)
        })));
        return (React.createElement("li", { className: css(styles$x.navItem, className) },
            React.createElement(NavContext.Consumer, null, context => React.isValidElement(children)
                ? renderClonedChild(context, children)
                : renderDefaultLink(context))));
    };
    NavItem.displayName = 'NavItem';

    const NavItemSeparator = (_a) => {
        var { component = 'li' } = _a, props = __rest(_a, ["component"]);
        return React.createElement(Divider, Object.assign({ component: component }, props));
    };
    NavItemSeparator.displayName = 'NavItemSeparator';

    class NavExpandable extends React.Component {
        constructor() {
            super(...arguments);
            this.expandableRef = React.createRef();
            this.id = this.props.id || getUniqueId();
            this.state = {
                expandedState: this.props.isExpanded
            };
            this.onExpand = (e, val) => {
                if (this.props.onExpand) {
                    this.props.onExpand(e, val);
                }
                else {
                    this.setState({ expandedState: val });
                }
            };
            this.handleToggle = (e, onToggle) => {
                // Item events can bubble up, ignore those
                if (!this.expandableRef.current || !this.expandableRef.current.contains(e.target)) {
                    return;
                }
                const { groupId } = this.props;
                const { expandedState } = this.state;
                onToggle(e, groupId, !expandedState);
                this.onExpand(e, !expandedState);
            };
        }
        componentDidMount() {
            this.setState({ expandedState: this.props.isExpanded });
        }
        componentDidUpdate(prevProps) {
            if (this.props.isExpanded !== prevProps.isExpanded) {
                this.setState({ expandedState: this.props.isExpanded });
            }
        }
        render() {
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            const _a = this.props, { id, title, srText, children, className, isActive, groupId, isExpanded, onExpand } = _a, props = __rest(_a, ["id", "title", "srText", "children", "className", "isActive", "groupId", "isExpanded", "onExpand"]);
            const { expandedState } = this.state;
            return (React.createElement(NavContext.Consumer, null, (context) => (React.createElement("li", Object.assign({ className: css(styles$x.navItem, styles$x.modifiers.expandable, expandedState && styles$x.modifiers.expanded, isActive && styles$x.modifiers.current, className), onClick: (e) => this.handleToggle(e, context.onToggle) }, props),
                React.createElement("a", { ref: this.expandableRef, className: styles$x.navLink, id: srText ? null : this.id, href: "#", onClick: e => e.preventDefault(), onMouseDown: e => e.preventDefault(), "aria-expanded": expandedState },
                    title,
                    React.createElement("span", { className: css(styles$x.navToggle) },
                        React.createElement("span", { className: css(styles$x.navToggleIcon) },
                            React.createElement(AngleRightIcon, { "aria-hidden": "true" })))),
                React.createElement("section", { className: css(styles$x.navSubnav), "aria-labelledby": this.id, hidden: expandedState ? null : true },
                    srText && (React.createElement("h2", { className: css(a11yStyles.screenReader), id: this.id }, srText)),
                    React.createElement("ul", { className: css(styles$x.navList) }, children))))));
        }
    }
    NavExpandable.displayName = 'NavExpandable';
    NavExpandable.defaultProps = {
        srText: '',
        isExpanded: false,
        children: '',
        className: '',
        groupId: null,
        isActive: false,
        id: ''
    };

    var notificationBadge = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "modifiers": {
        "unread": "pf-m-unread",
        "read": "pf-m-read"
      },
      "notificationBadge": "pf-c-notification-badge"
    };
    });

    var styles$y = unwrapExports(notificationBadge);

    const NotificationBadge = (_a) => {
        var { isRead = false, className, children } = _a, props = __rest(_a, ["isRead", "className", "children"]);
        return (React.createElement(Button, Object.assign({ variant: exports.ButtonVariant.plain, className: className }, props),
            React.createElement("span", { className: css(styles$y.notificationBadge, isRead ? styles$y.modifiers.read : styles$y.modifiers.unread) }, children)));
    };
    NotificationBadge.displayName = 'NotificationBadge';

    var notificationDrawer = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "modifiers": {
        "read": "pf-m-read",
        "info": "pf-m-info",
        "warning": "pf-m-warning",
        "danger": "pf-m-danger",
        "success": "pf-m-success",
        "hoverable": "pf-m-hoverable",
        "truncate": "pf-m-truncate",
        "expanded": "pf-m-expanded"
      },
      "notificationDrawer": "pf-c-notification-drawer",
      "notificationDrawerBody": "pf-c-notification-drawer__body",
      "notificationDrawerGroup": "pf-c-notification-drawer__group",
      "notificationDrawerGroupList": "pf-c-notification-drawer__group-list",
      "notificationDrawerGroupToggle": "pf-c-notification-drawer__group-toggle",
      "notificationDrawerGroupToggleCount": "pf-c-notification-drawer__group-toggle-count",
      "notificationDrawerGroupToggleIcon": "pf-c-notification-drawer__group-toggle-icon",
      "notificationDrawerGroupToggleTitle": "pf-c-notification-drawer__group-toggle-title",
      "notificationDrawerHeader": "pf-c-notification-drawer__header",
      "notificationDrawerHeaderAction": "pf-c-notification-drawer__header-action",
      "notificationDrawerHeaderStatus": "pf-c-notification-drawer__header-status",
      "notificationDrawerHeaderTitle": "pf-c-notification-drawer__header-title",
      "notificationDrawerList": "pf-c-notification-drawer__list",
      "notificationDrawerListItem": "pf-c-notification-drawer__list-item",
      "notificationDrawerListItemAction": "pf-c-notification-drawer__list-item-action",
      "notificationDrawerListItemDescription": "pf-c-notification-drawer__list-item-description",
      "notificationDrawerListItemHeader": "pf-c-notification-drawer__list-item-header",
      "notificationDrawerListItemHeaderIcon": "pf-c-notification-drawer__list-item-header-icon",
      "notificationDrawerListItemHeaderTitle": "pf-c-notification-drawer__list-item-header-title",
      "notificationDrawerListItemTimestamp": "pf-c-notification-drawer__list-item-timestamp"
    };
    });

    var styles$z = unwrapExports(notificationDrawer);

    const NotificationDrawer = (_a) => {
        var { children, className = '' } = _a, props = __rest(_a, ["children", "className"]);
        return (React.createElement("div", Object.assign({}, props, { className: css(styles$z.notificationDrawer, className) }), children));
    };
    NotificationDrawer.displayName = 'NotificationDrawer';

    const NotificationDrawerBody = (_a) => {
        var { children, className = '' } = _a, props = __rest(_a, ["children", "className"]);
        return (React.createElement("div", Object.assign({}, props, { className: css(styles$z.notificationDrawerBody, className) }), children));
    };
    NotificationDrawerBody.displayName = 'NotificationDrawerBody';

    const NotificationDrawerGroup = (_a) => {
        var { children, className = '', count, isExpanded, isRead = false, 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        onExpand = (event, expanded) => undefined, title } = _a, props = __rest(_a, ["children", "className", "count", "isExpanded", "isRead", "onExpand", "title"]);
        return (React.createElement("section", Object.assign({}, props, { className: css(styles$z.notificationDrawerGroup, isExpanded && styles$z.modifiers.expanded, className) }),
            React.createElement("h1", null,
                React.createElement("button", { className: css(styles$z.notificationDrawerGroupToggle), "aria-expanded": isExpanded, onClick: e => onExpand(e, !isExpanded) },
                    React.createElement("div", null, title),
                    React.createElement("div", { className: css(styles$z.notificationDrawerGroupToggleCount) },
                        React.createElement(Badge, { isRead: isRead }, count)),
                    React.createElement("span", { className: "pf-c-notification-drawer__group-toggle-icon" },
                        React.createElement(AngleRightIcon, null)))),
            children));
    };
    NotificationDrawerGroup.displayName = 'NotificationDrawerGroup';

    const NotificationDrawerGroupList = (_a) => {
        var { children, className = '' } = _a, props = __rest(_a, ["children", "className"]);
        return (React.createElement("div", Object.assign({}, props, { className: css(styles$z.notificationDrawerGroupList, className) }), children));
    };
    NotificationDrawerGroupList.displayName = 'NotificationDrawerGroupList';

    const TextContent = (_a) => {
        var { children = null, className = '' } = _a, props = __rest(_a, ["children", "className"]);
        return (React.createElement("div", Object.assign({}, props, { className: css(styles$3.content, className) }), children));
    };
    TextContent.displayName = 'TextContent';

    (function (TextVariants) {
        TextVariants["h1"] = "h1";
        TextVariants["h2"] = "h2";
        TextVariants["h3"] = "h3";
        TextVariants["h4"] = "h4";
        TextVariants["h5"] = "h5";
        TextVariants["h6"] = "h6";
        TextVariants["p"] = "p";
        TextVariants["a"] = "a";
        TextVariants["small"] = "small";
        TextVariants["blockquote"] = "blockquote";
        TextVariants["pre"] = "pre";
    })(exports.TextVariants || (exports.TextVariants = {}));
    const Text = (_a) => {
        var { children = null, className = '', component = exports.TextVariants.p } = _a, props = __rest(_a, ["children", "className", "component"]);
        const Component = component;
        return (React.createElement(Component, Object.assign({}, props, { "data-pf-content": true, className: css(className) }), children));
    };
    Text.displayName = 'Text';

    (function (TextListVariants) {
        TextListVariants["ul"] = "ul";
        TextListVariants["ol"] = "ol";
        TextListVariants["dl"] = "dl";
    })(exports.TextListVariants || (exports.TextListVariants = {}));
    const TextList = (_a) => {
        var { children = null, className = '', component = exports.TextListVariants.ul } = _a, props = __rest(_a, ["children", "className", "component"]);
        const Component = component;
        return (React.createElement(Component, Object.assign({}, props, { "data-pf-content": true, className: css(className) }), children));
    };
    TextList.displayName = 'TextList';

    (function (TextListItemVariants) {
        TextListItemVariants["li"] = "li";
        TextListItemVariants["dt"] = "dt";
        TextListItemVariants["dd"] = "dd";
    })(exports.TextListItemVariants || (exports.TextListItemVariants = {}));
    const TextListItem = (_a) => {
        var { children = null, className = '', component = exports.TextListItemVariants.li } = _a, props = __rest(_a, ["children", "className", "component"]);
        const Component = component;
        return (React.createElement(Component, Object.assign({}, props, { "data-pf-content": true, className: css(className) }), children));
    };
    TextListItem.displayName = 'TextListItem';

    const NotificationDrawerHeader = (_a) => {
        var { children, className = '', count, title = 'Notifications', unreadText = 'unread' } = _a, props = __rest(_a, ["children", "className", "count", "title", "unreadText"]);
        return (React.createElement("div", Object.assign({}, props, { className: css(styles$z.notificationDrawerHeader, className) }),
            React.createElement(Text, { component: exports.TextVariants.h1, className: css(styles$z.notificationDrawerHeaderTitle) }, title),
            count && React.createElement("span", { className: css(styles$z.notificationDrawerHeaderStatus) }, `${count} ${unreadText}`),
            children && React.createElement("div", { className: css(styles$z.notificationDrawerHeaderAction) }, children)));
    };
    NotificationDrawerHeader.displayName = 'NotificationDrawerHeader';

    const NotificationDrawerList = (_a) => {
        var { children, className = '', isHidden = false } = _a, props = __rest(_a, ["children", "className", "isHidden"]);
        return (React.createElement("ul", Object.assign({}, props, { className: css(styles$z.notificationDrawerList, className), hidden: isHidden }), children));
    };
    NotificationDrawerList.displayName = 'NotificationDrawerList';

    const NotificationDrawerListItem = (_a) => {
        var { children = null, className = '', isHoverable = true, isRead = false, 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        onClick = (event) => undefined, tabIndex = 0, variant } = _a, props = __rest(_a, ["children", "className", "isHoverable", "isRead", "onClick", "tabIndex", "variant"]);
        return (React.createElement("li", Object.assign({}, props, { className: css(styles$z.notificationDrawerListItem, isHoverable && styles$z.modifiers.hoverable, styles$z.modifiers[variant], isRead && styles$z.modifiers.read, className), tabIndex: tabIndex, onClick: e => onClick(e) }), children));
    };
    NotificationDrawerListItem.displayName = 'NotificationDrawerListItem';

    const NotificationDrawerListItemBody = (_a) => {
        var { children, className = '', timestamp } = _a, props = __rest(_a, ["children", "className", "timestamp"]);
        return (React.createElement(React.Fragment, null,
            React.createElement("div", Object.assign({}, props, { className: css(styles$z.notificationDrawerListItemDescription, className) }), children),
            timestamp && React.createElement("div", { className: css(styles$z.notificationDrawerListItemTimestamp, className) }, timestamp)));
    };
    NotificationDrawerListItemBody.displayName = 'NotificationDrawerListItemBody';

    const variantIcons$1 = {
        success: CheckCircleIcon,
        danger: ExclamationCircleIcon,
        warning: ExclamationTriangleIcon,
        info: InfoCircleIcon,
        default: BellIcon
    };
    const NotificationDrawerListItemHeader = (_a) => {
        var { children, className = '', icon = null, srTitle, title, variant = 'default' } = _a, props = __rest(_a, ["children", "className", "icon", "srTitle", "title", "variant"]);
        const Icon = variantIcons$1[variant];
        return (React.createElement(React.Fragment, null,
            React.createElement("div", Object.assign({}, props, { className: css(styles$z.notificationDrawerListItemHeader, className) }),
                React.createElement("span", { className: css(styles$z.notificationDrawerListItemHeaderIcon) }, icon ? icon : React.createElement(Icon, null)),
                React.createElement("h2", { className: css(styles$z.notificationDrawerListItemHeaderTitle) },
                    srTitle && React.createElement("span", { className: css(a11yStyles.screenReader) }, srTitle),
                    title)),
            children && React.createElement("div", { className: css(styles$z.notificationDrawerListItemAction) }, children)));
    };
    NotificationDrawerListItemHeader.displayName = 'NotificationDrawerListItemHeader';

    var optionsMenu = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "divider": "pf-c-divider",
      "modifiers": {
        "plain": "pf-m-plain",
        "text": "pf-m-text",
        "active": "pf-m-active",
        "expanded": "pf-m-expanded",
        "disabled": "pf-m-disabled",
        "top": "pf-m-top",
        "alignRight": "pf-m-align-right"
      },
      "optionsMenu": "pf-c-options-menu",
      "optionsMenuGroup": "pf-c-options-menu__group",
      "optionsMenuGroupTitle": "pf-c-options-menu__group-title",
      "optionsMenuMenu": "pf-c-options-menu__menu",
      "optionsMenuMenuItem": "pf-c-options-menu__menu-item",
      "optionsMenuMenuItemIcon": "pf-c-options-menu__menu-item-icon",
      "optionsMenuToggle": "pf-c-options-menu__toggle",
      "optionsMenuToggleButton": "pf-c-options-menu__toggle-button",
      "optionsMenuToggleButtonIcon": "pf-c-options-menu__toggle-button-icon",
      "optionsMenuToggleIcon": "pf-c-options-menu__toggle-icon",
      "optionsMenuToggleText": "pf-c-options-menu__toggle-text"
    };
    });

    var styles$A = unwrapExports(optionsMenu);

    (function (OptionsMenuPosition) {
        OptionsMenuPosition["right"] = "right";
        OptionsMenuPosition["left"] = "left";
    })(exports.OptionsMenuPosition || (exports.OptionsMenuPosition = {}));
    (function (OptionsMenuDirection) {
        OptionsMenuDirection["up"] = "up";
        OptionsMenuDirection["down"] = "down";
    })(exports.OptionsMenuDirection || (exports.OptionsMenuDirection = {}));
    const OptionsMenu = (_a) => {
        var { className = '', menuItems, toggle, isText = false, isGrouped = false, id, 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        ref, menuAppendTo = 'inline', ouiaId, ouiaSafe = true } = _a, props = __rest(_a, ["className", "menuItems", "toggle", "isText", "isGrouped", "id", "ref", "menuAppendTo", "ouiaId", "ouiaSafe"]);
        return (React.createElement(DropdownContext.Provider, { value: {
                id,
                onSelect: () => undefined,
                toggleIndicatorClass: styles$A.optionsMenuToggleIcon,
                toggleTextClass: styles$A.optionsMenuToggleText,
                menuClass: styles$A.optionsMenuMenu,
                itemClass: styles$A.optionsMenuMenuItem,
                toggleClass: isText ? styles$A.optionsMenuToggleButton : styles$A.optionsMenuToggle,
                baseClass: styles$A.optionsMenu,
                disabledClass: styles$A.modifiers.disabled,
                menuComponent: isGrouped ? 'div' : 'ul',
                baseComponent: 'div',
                ouiaId,
                ouiaSafe,
                ouiaComponentType: OptionsMenu.displayName
            } },
            React.createElement(DropdownWithContext, Object.assign({}, props, { id: id, dropdownItems: menuItems, className: className, isGrouped: isGrouped, toggle: toggle, menuAppendTo: menuAppendTo }))));
    };
    OptionsMenu.displayName = 'OptionsMenu';

    const OptionsMenuToggle = (_a) => {
        var { isPlain = false, isDisabled = false, isOpen = false, parentId = '', toggleTemplate = React.createElement(React.Fragment, null), hideCaret = false, 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        isActive = false, 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        isSplitButton = false, 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        type, 'aria-label': ariaLabel = 'Options menu' } = _a, props = __rest(_a, ["isPlain", "isDisabled", "isOpen", "parentId", "toggleTemplate", "hideCaret", "isActive", "isSplitButton", "type", 'aria-label']);
        return (React.createElement(DropdownContext.Consumer, null, ({ id: contextId }) => (React.createElement(DropdownToggle, Object.assign({}, ((isPlain || hideCaret) && { toggleIndicator: null }), props, { isPlain: isPlain, isOpen: isOpen, isDisabled: isDisabled, isActive: isActive, id: parentId ? `${parentId}-toggle` : `${contextId}-toggle`, "aria-haspopup": "listbox", "aria-label": ariaLabel, "aria-expanded": isOpen }, (toggleTemplate ? { children: toggleTemplate } : {}))))));
    };
    OptionsMenuToggle.displayName = 'OptionsMenuToggle';

    const OptionsMenuItemGroup = (_a) => {
        var { className = '', 'aria-label': ariaLabel = '', groupTitle = '', children = null, hasSeparator = false } = _a, props = __rest(_a, ["className", 'aria-label', "groupTitle", "children", "hasSeparator"]);
        return (React.createElement("section", Object.assign({}, props, { className: css(styles$A.optionsMenuGroup) }),
            groupTitle && React.createElement("h1", { className: css(styles$A.optionsMenuGroupTitle) }, groupTitle),
            React.createElement("ul", { className: className, "aria-label": ariaLabel },
                children,
                hasSeparator && React.createElement(Divider, { component: "li", role: "separator" }))));
    };
    OptionsMenuItemGroup.displayName = 'OptionsMenuItemGroup';

    const OptionsMenuItem = (_a) => {
        var { children = null, isSelected = false, onSelect = () => null, id = '', isDisabled } = _a, props = __rest(_a, ["children", "isSelected", "onSelect", "id", "isDisabled"]);
        return (React.createElement(DropdownItem, Object.assign({ id: id, component: "button", isDisabled: isDisabled, onClick: (event) => onSelect(event) }, (isDisabled && { 'aria-disabled': true }), props),
            children,
            isSelected && (React.createElement("span", { className: css(styles$A.optionsMenuMenuItemIcon) },
                React.createElement(CheckIcon, { "aria-hidden": isSelected })))));
    };
    OptionsMenuItem.displayName = 'OptionsMenuItem';

    const OptionsMenuSeparator = (_a) => {
        var { component = 'li' } = _a, props = __rest(_a, ["component"]);
        return React.createElement(Divider, Object.assign({ component: component }, props));
    };
    OptionsMenuSeparator.displayName = 'OptionsMenuSeparator';

    const OptionsMenuToggleWithText = (_a) => {
        var { parentId = '', toggleText, toggleTextClassName = '', toggleButtonContents, toggleButtonContentsClassName = '', onToggle = () => null, isOpen = false, isPlain = false, isDisabled = false, 
        /* eslint-disable @typescript-eslint/no-unused-vars */
        isActive = false, 'aria-haspopup': ariaHasPopup, parentRef, onEnter, 
        /* eslint-enable @typescript-eslint/no-unused-vars */
        'aria-label': ariaLabel = 'Options menu' } = _a, props = __rest(_a, ["parentId", "toggleText", "toggleTextClassName", "toggleButtonContents", "toggleButtonContentsClassName", "onToggle", "isOpen", "isPlain", "isDisabled", "isActive", 'aria-haspopup', "parentRef", "onEnter", 'aria-label']);
        const buttonRef = React.useRef();
        React.useEffect(() => {
            document.addEventListener('mousedown', onDocClick);
            document.addEventListener('touchstart', onDocClick);
            document.addEventListener('keydown', onEscPress);
            return () => {
                document.removeEventListener('mousedown', onDocClick);
                document.removeEventListener('touchstart', onDocClick);
                document.removeEventListener('keydown', onEscPress);
            };
        });
        const onDocClick = (event) => {
            if (isOpen && parentRef && parentRef.current && !parentRef.current.contains(event.target)) {
                onToggle(false);
                buttonRef.current.focus();
            }
        };
        const onKeyDown = (event) => {
            if (event.key === 'Tab' && !isOpen) {
                return;
            }
            event.preventDefault();
            if ((event.key === 'Enter' || event.key === ' ') && isOpen) {
                onToggle(!isOpen);
            }
            else if ((event.key === 'Enter' || event.key === ' ') && !isOpen) {
                onToggle(!isOpen);
                onEnter(event);
            }
        };
        const onEscPress = (event) => {
            const keyCode = event.keyCode || event.which;
            if (isOpen &&
                (keyCode === KEY_CODES.ESCAPE_KEY || event.key === 'Tab') &&
                parentRef &&
                parentRef.current &&
                parentRef.current.contains(event.target)) {
                onToggle(false);
                buttonRef.current.focus();
            }
        };
        return (React.createElement("div", Object.assign({ className: css(styles$A.optionsMenuToggle, styles$A.modifiers.text, isPlain && styles$A.modifiers.plain, isDisabled && styles$A.modifiers.disabled, isActive && styles$A.modifiers.active) }, props),
            React.createElement("span", { className: css(styles$A.optionsMenuToggleText, toggleTextClassName) }, toggleText),
            React.createElement("button", { className: css(styles$A.optionsMenuToggleButton, toggleButtonContentsClassName), id: `${parentId}-toggle`, "aria-haspopup": "listbox", "aria-label": ariaLabel, "aria-expanded": isOpen, ref: buttonRef, disabled: isDisabled, onClick: () => onToggle(!isOpen), onKeyDown: onKeyDown },
                React.createElement("span", { className: css(styles$A.optionsMenuToggleButtonIcon) }, toggleButtonContents))));
    };
    OptionsMenuToggleWithText.displayName = 'OptionsMenuToggleWithText';

    var overflowMenu = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "divider": "pf-c-divider",
      "modifiers": {
        "buttonGroup": "pf-m-button-group",
        "iconButtonGroup": "pf-m-icon-button-group",
        "vertical": "pf-m-vertical"
      },
      "overflowMenu": "pf-c-overflow-menu",
      "overflowMenuContent": "pf-c-overflow-menu__content",
      "overflowMenuControl": "pf-c-overflow-menu__control",
      "overflowMenuGroup": "pf-c-overflow-menu__group",
      "overflowMenuItem": "pf-c-overflow-menu__item"
    };
    });

    var styles$B = unwrapExports(overflowMenu);

    const OverflowMenuContext = React.createContext({
        isBelowBreakpoint: false
    });

    var global_breakpoint_md = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.global_breakpoint_md = {
      "name": "--pf-global--breakpoint--md",
      "value": "768px",
      "var": "var(--pf-global--breakpoint--md)"
    };
    exports["default"] = exports.global_breakpoint_md;
    });

    var globalBreakpointMd = unwrapExports(global_breakpoint_md);
    var global_breakpoint_md_1 = global_breakpoint_md.global_breakpoint_md;

    var global_breakpoint_lg = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.global_breakpoint_lg = {
      "name": "--pf-global--breakpoint--lg",
      "value": "992px",
      "var": "var(--pf-global--breakpoint--lg)"
    };
    exports["default"] = exports.global_breakpoint_lg;
    });

    var globalBreakpointLg = unwrapExports(global_breakpoint_lg);
    var global_breakpoint_lg_1 = global_breakpoint_lg.global_breakpoint_lg;

    var global_breakpoint_xl = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.global_breakpoint_xl = {
      "name": "--pf-global--breakpoint--xl",
      "value": "1200px",
      "var": "var(--pf-global--breakpoint--xl)"
    };
    exports["default"] = exports.global_breakpoint_xl;
    });

    var globalBreakpointXl = unwrapExports(global_breakpoint_xl);
    var global_breakpoint_xl_1 = global_breakpoint_xl.global_breakpoint_xl;

    var global_breakpoint_2xl = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.global_breakpoint_2xl = {
      "name": "--pf-global--breakpoint--2xl",
      "value": "1450px",
      "var": "var(--pf-global--breakpoint--2xl)"
    };
    exports["default"] = exports.global_breakpoint_2xl;
    });

    var globalBreakpoint2xl = unwrapExports(global_breakpoint_2xl);
    var global_breakpoint_2xl_1 = global_breakpoint_2xl.global_breakpoint_2xl;

    const breakpoints = {
        md: globalBreakpointMd,
        lg: globalBreakpointLg,
        xl: globalBreakpointXl,
        '2xl': globalBreakpoint2xl
    };
    class OverflowMenu extends React.Component {
        constructor(props) {
            super(props);
            this.handleResize = () => {
                const breakpointPx = breakpoints[this.props.breakpoint];
                if (!breakpointPx) {
                    // eslint-disable-next-line no-console
                    console.error('OverflowMenu will not be visible without a valid breakpoint.');
                    return;
                }
                const breakpointWidth = Number(breakpointPx.value.replace('px', ''));
                const isBelowBreakpoint = window.innerWidth < breakpointWidth;
                this.setState({ isBelowBreakpoint });
            };
            this.state = {
                isBelowBreakpoint: false
            };
        }
        componentDidMount() {
            this.handleResize();
            window.addEventListener('resize', debounce(this.handleResize, 250));
        }
        componentWillUnmount() {
            window.removeEventListener('resize', debounce(this.handleResize, 250));
        }
        render() {
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            const _a = this.props, { className, breakpoint, children } = _a, props = __rest(_a, ["className", "breakpoint", "children"]);
            return (React.createElement("div", Object.assign({}, props, { className: css(styles$B.overflowMenu, className) }),
                React.createElement(OverflowMenuContext.Provider, { value: { isBelowBreakpoint: this.state.isBelowBreakpoint } }, children)));
        }
    }
    OverflowMenu.displayName = 'OverflowMenu';
    OverflowMenu.contextType = OverflowMenuContext;

    const OverflowMenuControl = ({ className, children, hasAdditionalOptions }) => (React.createElement(OverflowMenuContext.Consumer, null, value => (value.isBelowBreakpoint || hasAdditionalOptions) && (React.createElement("div", { className: css(styles$B.overflowMenuControl, className) },
        " ",
        children,
        " "))));
    OverflowMenuControl.displayName = 'OverflowMenuControl';

    const OverflowMenuContent = ({ className, children, isPersistent }) => (React.createElement(OverflowMenuContext.Consumer, null, value => (!value.isBelowBreakpoint || isPersistent) && (React.createElement("div", { className: css(styles$B.overflowMenuContent, className) }, children))));
    OverflowMenuContent.displayName = 'OverflowMenuContent';

    const OverflowMenuGroup = ({ className, children, isPersistent = false, groupType }) => (React.createElement(OverflowMenuContext.Consumer, null, value => (isPersistent || !value.isBelowBreakpoint) && (React.createElement("div", { className: css(styles$B.overflowMenuGroup, groupType === 'button' && styles$B.modifiers.buttonGroup, groupType === 'icon' && styles$B.modifiers.iconButtonGroup, className) }, children))));
    OverflowMenuGroup.displayName = 'OverflowMenuGroup';

    const OverflowMenuItem = ({ className, children, isPersistent = false }) => (React.createElement(OverflowMenuContext.Consumer, null, value => (isPersistent || !value.isBelowBreakpoint) && (React.createElement("div", { className: css(styles$B.overflowMenuItem, className) },
        " ",
        children,
        " "))));
    OverflowMenuItem.displayName = 'OverflowMenuItem';

    const OverflowMenuDropdownItem = ({ children, isShared = false, index }) => (React.createElement(OverflowMenuContext.Consumer, null, value => (!isShared || value.isBelowBreakpoint) && (React.createElement(DropdownItem, { component: "button", index: index }, children))));
    OverflowMenuDropdownItem.displayName = 'OverflowMenuDropdownItem';

    var page = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "avatar": "pf-c-avatar",
      "brand": "pf-c-brand",
      "button": "pf-c-button",
      "card": "pf-c-card",
      "drawer": "pf-c-drawer",
      "modifiers": {
        "light": "pf-m-light",
        "hidden": "pf-m-hidden",
        "hiddenOnSm": "pf-m-hidden-on-sm",
        "visibleOnSm": "pf-m-visible-on-sm",
        "hiddenOnMd": "pf-m-hidden-on-md",
        "visibleOnMd": "pf-m-visible-on-md",
        "hiddenOnLg": "pf-m-hidden-on-lg",
        "visibleOnLg": "pf-m-visible-on-lg",
        "hiddenOnXl": "pf-m-hidden-on-xl",
        "visibleOnXl": "pf-m-visible-on-xl",
        "hiddenOn_2xl": "pf-m-hidden-on-2xl",
        "visibleOn_2xl": "pf-m-visible-on-2xl",
        "selected": "pf-m-selected",
        "unread": "pf-m-unread",
        "expanded": "pf-m-expanded",
        "collapsed": "pf-m-collapsed",
        "fill": "pf-m-fill",
        "noFill": "pf-m-no-fill",
        "dark_100": "pf-m-dark-100",
        "dark_200": "pf-m-dark-200",
        "padding": "pf-m-padding",
        "noPadding": "pf-m-no-padding",
        "paddingOnSm": "pf-m-padding-on-sm",
        "noPaddingOnSm": "pf-m-no-padding-on-sm",
        "paddingOnMd": "pf-m-padding-on-md",
        "noPaddingOnMd": "pf-m-no-padding-on-md",
        "paddingOnLg": "pf-m-padding-on-lg",
        "noPaddingOnLg": "pf-m-no-padding-on-lg",
        "paddingOnXl": "pf-m-padding-on-xl",
        "noPaddingOnXl": "pf-m-no-padding-on-xl",
        "paddingOn_2xl": "pf-m-padding-on-2xl",
        "noPaddingOn_2xl": "pf-m-no-padding-on-2xl"
      },
      "nav": "pf-c-nav",
      "notificationBadge": "pf-c-notification-badge",
      "page": "pf-c-page",
      "pageDrawer": "pf-c-page__drawer",
      "pageHeader": "pf-c-page__header",
      "pageHeaderBrand": "pf-c-page__header-brand",
      "pageHeaderBrandLink": "pf-c-page__header-brand-link",
      "pageHeaderBrandToggle": "pf-c-page__header-brand-toggle",
      "pageHeaderNav": "pf-c-page__header-nav",
      "pageHeaderTools": "pf-c-page__header-tools",
      "pageHeaderToolsGroup": "pf-c-page__header-tools-group",
      "pageHeaderToolsItem": "pf-c-page__header-tools-item",
      "pageMain": "pf-c-page__main",
      "pageMainBreadcrumb": "pf-c-page__main-breadcrumb",
      "pageMainNav": "pf-c-page__main-nav",
      "pageMainSection": "pf-c-page__main-section",
      "pageMainWizard": "pf-c-page__main-wizard",
      "pageSidebar": "pf-c-page__sidebar",
      "pageSidebarBody": "pf-c-page__sidebar-body"
    };
    });

    var styles$C = unwrapExports(page);

    (function (PageLayouts) {
        PageLayouts["vertical"] = "vertical";
        PageLayouts["horizontal"] = "horizontal";
    })(exports.PageLayouts || (exports.PageLayouts = {}));
    const PageContext = React.createContext({});
    const PageContextProvider = PageContext.Provider;
    const PageContextConsumer = PageContext.Consumer;
    class Page extends React.Component {
        constructor(props) {
            super(props);
            this.handleResize = () => {
                const { onPageResize } = this.props;
                const windowSize = window.innerWidth;
                // eslint-disable-next-line radix
                const mobileView = windowSize < Number.parseInt(globalBreakpointXl.value, 10);
                if (onPageResize) {
                    onPageResize({ mobileView, windowSize });
                }
                this.setState({ mobileView });
            };
            this.onNavToggleMobile = () => {
                this.setState(prevState => ({
                    mobileIsNavOpen: !prevState.mobileIsNavOpen
                }));
            };
            this.onNavToggleDesktop = () => {
                this.setState(prevState => ({
                    desktopIsNavOpen: !prevState.desktopIsNavOpen
                }));
            };
            const { isManagedSidebar, defaultManagedSidebarIsOpen } = props;
            const managedSidebarOpen = !isManagedSidebar ? true : defaultManagedSidebarIsOpen;
            this.state = {
                desktopIsNavOpen: managedSidebarOpen,
                mobileIsNavOpen: false,
                mobileView: false
            };
        }
        componentDidMount() {
            const { isManagedSidebar, onPageResize } = this.props;
            if (isManagedSidebar || onPageResize) {
                window.addEventListener('resize', debounce(this.handleResize, 250));
                // Initial check if should be shown
                this.handleResize();
            }
        }
        componentWillUnmount() {
            const { isManagedSidebar, onPageResize } = this.props;
            if (isManagedSidebar || onPageResize) {
                window.removeEventListener('resize', debounce(this.handleResize, 250));
            }
        }
        render() {
            const _a = this.props, { breadcrumb, className, children, header, sidebar, skipToContent, role, mainContainerId, isManagedSidebar, 
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            defaultManagedSidebarIsOpen, 
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            onPageResize, mainAriaLabel, mainTabIndex } = _a, rest = __rest(_a, ["breadcrumb", "className", "children", "header", "sidebar", "skipToContent", "role", "mainContainerId", "isManagedSidebar", "defaultManagedSidebarIsOpen", "onPageResize", "mainAriaLabel", "mainTabIndex"]);
            const { mobileView, mobileIsNavOpen, desktopIsNavOpen } = this.state;
            const context = {
                isManagedSidebar,
                onNavToggle: mobileView ? this.onNavToggleMobile : this.onNavToggleDesktop,
                isNavOpen: mobileView ? mobileIsNavOpen : desktopIsNavOpen
            };
            return (React.createElement(PageContextProvider, { value: context },
                React.createElement("div", Object.assign({}, rest, { className: css(styles$C.page, className) }),
                    skipToContent,
                    header,
                    sidebar,
                    React.createElement("main", { role: role, id: mainContainerId, className: css(styles$C.pageMain), tabIndex: mainTabIndex, "aria-label": mainAriaLabel },
                        breadcrumb && React.createElement("section", { className: css(styles$C.pageMainBreadcrumb) }, breadcrumb),
                        children))));
        }
    }
    Page.displayName = 'Page';
    Page.defaultProps = {
        isManagedSidebar: false,
        defaultManagedSidebarIsOpen: true,
        onPageResize: () => null,
        mainTabIndex: -1
    };

    var barsIcon = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.BarsIconConfig = {
      name: 'BarsIcon',
      height: 512,
      width: 448,
      svgPath: 'M16 132h416c8.837 0 16-7.163 16-16V76c0-8.837-7.163-16-16-16H16C7.163 60 0 67.163 0 76v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16z',
      yOffset: 0,
      xOffset: 0,
      transform: ''
    };
    exports.BarsIcon = createIcon_1.createIcon(exports.BarsIconConfig);
    exports["default"] = exports.BarsIcon;
    });

    var BarsIcon = unwrapExports(barsIcon);
    var barsIcon_1 = barsIcon.BarsIconConfig;
    var barsIcon_2 = barsIcon.BarsIcon;

    const PageHeader = (_a) => {
        var { className = '', logo = null, logoProps = null, logoComponent = 'a', headerTools = null, topNav = null, isNavOpen = true, role = undefined, showNavToggle = false, onNavToggle = () => undefined, 'aria-label': ariaLabel = 'Global navigation' } = _a, props = __rest(_a, ["className", "logo", "logoProps", "logoComponent", "headerTools", "topNav", "isNavOpen", "role", "showNavToggle", "onNavToggle", 'aria-label']);
        const LogoComponent = logoComponent;
        return (React.createElement(PageContextConsumer, null, ({ isManagedSidebar, onNavToggle: managedOnNavToggle, isNavOpen: managedIsNavOpen }) => {
            const navToggle = isManagedSidebar ? managedOnNavToggle : onNavToggle;
            const navOpen = isManagedSidebar ? managedIsNavOpen : isNavOpen;
            return (React.createElement("header", Object.assign({ role: role, className: css(styles$C.pageHeader, className) }, props),
                (showNavToggle || logo) && (React.createElement("div", { className: css(styles$C.pageHeaderBrand) },
                    showNavToggle && (React.createElement("div", { className: css(styles$C.pageHeaderBrandToggle) },
                        React.createElement(Button, { id: "nav-toggle", onClick: navToggle, "aria-label": ariaLabel, "aria-controls": "page-sidebar", "aria-expanded": navOpen ? 'true' : 'false', variant: exports.ButtonVariant.plain },
                            React.createElement(BarsIcon, null)))),
                    logo && (React.createElement(LogoComponent, Object.assign({ className: css(styles$C.pageHeaderBrandLink) }, logoProps), logo)))),
                topNav && React.createElement("div", { className: css(styles$C.pageHeaderNav) }, topNav),
                headerTools));
        }));
    };
    PageHeader.displayName = 'PageHeader';

    const PageSidebar = (_a) => {
        var { className = '', nav, isNavOpen = true, theme = 'dark' } = _a, props = __rest(_a, ["className", "nav", "isNavOpen", "theme"]);
        return (React.createElement(PageContextConsumer, null, ({ isManagedSidebar, isNavOpen: managedIsNavOpen }) => {
            const navOpen = isManagedSidebar ? managedIsNavOpen : isNavOpen;
            return (React.createElement("div", Object.assign({ id: "page-sidebar", className: css(styles$C.pageSidebar, theme === 'light' && styles$C.modifiers.light, navOpen && styles$C.modifiers.expanded, !navOpen && styles$C.modifiers.collapsed, className) }, props),
                React.createElement("div", { className: css(styles$C.pageSidebarBody) }, nav)));
        }));
    };
    PageSidebar.displayName = 'PageSidebar';

    (function (PageSectionVariants) {
        PageSectionVariants["default"] = "default";
        PageSectionVariants["light"] = "light";
        PageSectionVariants["dark"] = "dark";
        PageSectionVariants["darker"] = "darker";
    })(exports.PageSectionVariants || (exports.PageSectionVariants = {}));
    (function (PageSectionTypes) {
        PageSectionTypes["default"] = "default";
        PageSectionTypes["nav"] = "nav";
    })(exports.PageSectionTypes || (exports.PageSectionTypes = {}));
    const variantType = {
        [exports.PageSectionTypes.default]: styles$C.pageMainSection,
        [exports.PageSectionTypes.nav]: styles$C.pageMainNav
    };
    const variantStyle = {
        [exports.PageSectionVariants.default]: '',
        [exports.PageSectionVariants.light]: styles$C.modifiers.light,
        [exports.PageSectionVariants.dark]: styles$C.modifiers.dark_200,
        [exports.PageSectionVariants.darker]: styles$C.modifiers.dark_100
    };
    const PageSection = (_a) => {
        var { className = '', children, variant = 'default', type = 'default', padding, isFilled } = _a, props = __rest(_a, ["className", "children", "variant", "type", "padding", "isFilled"]);
        return (React.createElement("section", Object.assign({}, props, { className: css(variantType[type], formatBreakpointMods(padding, styles$C), variantStyle[variant], isFilled === false && styles$C.modifiers.noFill, isFilled === true && styles$C.modifiers.fill, className) }), children));
    };
    PageSection.displayName = 'PageSection';

    const PageHeaderTools = (_a) => {
        var { children, className } = _a, props = __rest(_a, ["children", "className"]);
        return (React.createElement("div", Object.assign({ className: css(styles$C.pageHeaderTools, className) }, props), children));
    };
    PageHeaderTools.displayName = 'PageHeaderTools';

    const PageHeaderToolsGroup = (_a) => {
        var { children, className, visibility } = _a, props = __rest(_a, ["children", "className", "visibility"]);
        return (React.createElement("div", Object.assign({ className: css(styles$C.pageHeaderToolsGroup, formatBreakpointMods(visibility, styles$C), className) }, props), children));
    };
    PageHeaderToolsGroup.displayName = 'PageHeaderToolsGroup';

    const PageHeaderToolsItem = ({ children, className, visibility, isSelected }) => (React.createElement("div", { className: css(styles$C.pageHeaderToolsItem, isSelected && styles$C.modifiers.selected, formatBreakpointMods(visibility, styles$C), className) }, children));
    PageHeaderToolsItem.displayName = 'PageHeaderToolsItem';

    const ToggleTemplate = ({ firstIndex = 0, lastIndex = 0, itemCount = 0, itemsTitle = 'items' }) => (React.createElement(React.Fragment, null,
        React.createElement("b", null,
            firstIndex,
            " - ",
            lastIndex),
        ' ',
        "of ",
        React.createElement("b", null, itemCount),
        " ",
        itemsTitle));
    ToggleTemplate.displayName = 'ToggleTemplate';

    var pagination = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "button": "pf-c-button",
      "formControl": "pf-c-form-control",
      "modifiers": {
        "bottom": "pf-m-bottom",
        "static": "pf-m-static",
        "first": "pf-m-first",
        "last": "pf-m-last",
        "compact": "pf-m-compact"
      },
      "optionsMenu": "pf-c-options-menu",
      "optionsMenuToggle": "pf-c-options-menu__toggle",
      "pagination": "pf-c-pagination",
      "paginationNav": "pf-c-pagination__nav",
      "paginationNavControl": "pf-c-pagination__nav-control",
      "paginationNavPageSelect": "pf-c-pagination__nav-page-select",
      "paginationTotalItems": "pf-c-pagination__total-items"
    };
    });

    var styles$D = unwrapExports(pagination);

    var angleDoubleLeftIcon = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.AngleDoubleLeftIconConfig = {
      name: 'AngleDoubleLeftIcon',
      height: 512,
      width: 448,
      svgPath: 'M223.7 239l136-136c9.4-9.4 24.6-9.4 33.9 0l22.6 22.6c9.4 9.4 9.4 24.6 0 33.9L319.9 256l96.4 96.4c9.4 9.4 9.4 24.6 0 33.9L393.7 409c-9.4 9.4-24.6 9.4-33.9 0l-136-136c-9.5-9.4-9.5-24.6-.1-34zm-192 34l136 136c9.4 9.4 24.6 9.4 33.9 0l22.6-22.6c9.4-9.4 9.4-24.6 0-33.9L127.9 256l96.4-96.4c9.4-9.4 9.4-24.6 0-33.9L201.7 103c-9.4-9.4-24.6-9.4-33.9 0l-136 136c-9.5 9.4-9.5 24.6-.1 34z',
      yOffset: 0,
      xOffset: 0,
      transform: ''
    };
    exports.AngleDoubleLeftIcon = createIcon_1.createIcon(exports.AngleDoubleLeftIconConfig);
    exports["default"] = exports.AngleDoubleLeftIcon;
    });

    var AngleDoubleLeftIcon = unwrapExports(angleDoubleLeftIcon);
    var angleDoubleLeftIcon_1 = angleDoubleLeftIcon.AngleDoubleLeftIconConfig;
    var angleDoubleLeftIcon_2 = angleDoubleLeftIcon.AngleDoubleLeftIcon;

    var angleDoubleRightIcon = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.AngleDoubleRightIconConfig = {
      name: 'AngleDoubleRightIcon',
      height: 512,
      width: 448,
      svgPath: 'M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34zm192-34l-136-136c-9.4-9.4-24.6-9.4-33.9 0l-22.6 22.6c-9.4 9.4-9.4 24.6 0 33.9l96.4 96.4-96.4 96.4c-9.4 9.4-9.4 24.6 0 33.9l22.6 22.6c9.4 9.4 24.6 9.4 33.9 0l136-136c9.4-9.2 9.4-24.4 0-33.8z',
      yOffset: 0,
      xOffset: 0,
      transform: ''
    };
    exports.AngleDoubleRightIcon = createIcon_1.createIcon(exports.AngleDoubleRightIconConfig);
    exports["default"] = exports.AngleDoubleRightIcon;
    });

    var AngleDoubleRightIcon = unwrapExports(angleDoubleRightIcon);
    var angleDoubleRightIcon_1 = angleDoubleRightIcon.AngleDoubleRightIconConfig;
    var angleDoubleRightIcon_2 = angleDoubleRightIcon.AngleDoubleRightIcon;

    class Navigation extends React.Component {
        constructor(props) {
            super(props);
            this.handleNewPage = (_evt, newPage) => {
                const { perPage, onSetPage } = this.props;
                const startIdx = (newPage - 1) * perPage;
                const endIdx = newPage * perPage;
                return onSetPage(_evt, newPage, perPage, startIdx, endIdx);
            };
            this.state = { userInputPage: this.props.page };
        }
        static parseInteger(input, lastPage) {
            // eslint-disable-next-line radix
            let inputPage = Number.parseInt(input, 10);
            if (!Number.isNaN(inputPage)) {
                inputPage = inputPage > lastPage ? lastPage : inputPage;
                inputPage = inputPage < 1 ? 1 : inputPage;
            }
            return inputPage;
        }
        onChange(event, lastPage) {
            const inputPage = Navigation.parseInteger(event.target.value, lastPage);
            this.setState({ userInputPage: Number.isNaN(inputPage) ? event.target.value : inputPage });
        }
        onKeyDown(event, page, lastPage, onPageInput) {
            if (event.keyCode === KEY_CODES.ENTER) {
                const inputPage = Navigation.parseInteger(this.state.userInputPage, lastPage);
                onPageInput(event, Number.isNaN(inputPage) ? page : inputPage);
                this.handleNewPage(event, Number.isNaN(inputPage) ? page : inputPage);
            }
        }
        componentDidUpdate(lastState) {
            if (this.props.page !== lastState.page &&
                this.props.page <= this.props.lastPage &&
                this.state.userInputPage !== this.props.page) {
                this.setState({ userInputPage: this.props.page });
            }
        }
        render() {
            const _a = this.props, { page, 
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            perPage, 
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            onSetPage, isDisabled, lastPage, firstPage, pagesTitle, toLastPage, toNextPage, toFirstPage, toPreviousPage, currPage, paginationTitle, onNextClick, onPreviousClick, onFirstClick, onLastClick, onPageInput, className, isCompact } = _a, props = __rest(_a, ["page", "perPage", "onSetPage", "isDisabled", "lastPage", "firstPage", "pagesTitle", "toLastPage", "toNextPage", "toFirstPage", "toPreviousPage", "currPage", "paginationTitle", "onNextClick", "onPreviousClick", "onFirstClick", "onLastClick", "onPageInput", "className", "isCompact"]);
            const { userInputPage } = this.state;
            return (React.createElement("nav", Object.assign({ className: css(styles$D.paginationNav, className), "aria-label": paginationTitle }, props),
                !isCompact && (React.createElement("div", { className: css(styles$D.paginationNavControl, styles$D.modifiers.first) },
                    React.createElement(Button, { variant: exports.ButtonVariant.plain, isDisabled: isDisabled || page === firstPage || page === 0, "aria-label": toFirstPage, "data-action": "first", onClick: event => {
                            onFirstClick(event, 1);
                            this.handleNewPage(event, 1);
                            this.setState({ userInputPage: 1 });
                        } },
                        React.createElement(AngleDoubleLeftIcon, null)))),
                React.createElement("div", { className: styles$D.paginationNavControl },
                    React.createElement(Button, { variant: exports.ButtonVariant.plain, isDisabled: isDisabled || page === firstPage || page === 0, "data-action": "previous", onClick: event => {
                            const newPage = page - 1 >= 1 ? page - 1 : 1;
                            onPreviousClick(event, newPage);
                            this.handleNewPage(event, newPage);
                            this.setState({ userInputPage: newPage });
                        }, "aria-label": toPreviousPage },
                        React.createElement(AngleLeftIcon, null))),
                !isCompact && (React.createElement("div", { className: styles$D.paginationNavPageSelect },
                    React.createElement("input", { className: css(styles$D.formControl), "aria-label": currPage, type: "number", disabled: isDisabled || (page === firstPage && page === lastPage) || page === 0, min: lastPage <= 0 && firstPage <= 0 ? 0 : 1, max: lastPage, value: userInputPage, onKeyDown: event => this.onKeyDown(event, page, lastPage, onPageInput), onChange: event => this.onChange(event, lastPage) }),
                    React.createElement("span", { "aria-hidden": "true" },
                        "of ",
                        pagesTitle ? pluralize(lastPage, pagesTitle) : lastPage))),
                React.createElement("div", { className: styles$D.paginationNavControl },
                    React.createElement(Button, { variant: exports.ButtonVariant.plain, isDisabled: isDisabled || page === lastPage, "aria-label": toNextPage, "data-action": "next", onClick: event => {
                            const newPage = page + 1 <= lastPage ? page + 1 : lastPage;
                            onNextClick(event, newPage);
                            this.handleNewPage(event, newPage);
                            this.setState({ userInputPage: newPage });
                        } },
                        React.createElement(AngleRightIcon, null))),
                !isCompact && (React.createElement("div", { className: css(styles$D.paginationNavControl, styles$D.modifiers.last) },
                    React.createElement(Button, { variant: exports.ButtonVariant.plain, isDisabled: isDisabled || page === lastPage, "aria-label": toLastPage, "data-action": "last", onClick: event => {
                            onLastClick(event, lastPage);
                            this.handleNewPage(event, lastPage);
                            this.setState({ userInputPage: lastPage });
                        } },
                        React.createElement(AngleDoubleRightIcon, null))))));
        }
    }
    Navigation.displayName = 'Navigation';
    Navigation.defaultProps = {
        className: '',
        isDisabled: false,
        isCompact: false,
        lastPage: 0,
        firstPage: 0,
        pagesTitle: '',
        toLastPage: 'Go to last page',
        toNextPage: 'Go to next page',
        toFirstPage: 'Go to first page',
        toPreviousPage: 'Go to previous page',
        currPage: 'Current page',
        paginationTitle: 'Pagination',
        onNextClick: () => undefined,
        onPreviousClick: () => undefined,
        onFirstClick: () => undefined,
        onLastClick: () => undefined,
        onPageInput: () => undefined
    };

    let toggleId = 0;
    const OptionsToggle = ({ itemsTitle = 'items', optionsToggle = 'Select', 
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    itemsPerPageTitle = 'Items per page', firstIndex = 0, lastIndex = 0, itemCount = 0, widgetId = '', showToggle = true, 
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    onToggle = (_isOpen) => undefined, isOpen = false, isDisabled = false, parentRef = null, toggleTemplate: ToggleTemplate = '', onEnter = null }) => (React.createElement("div", { className: css(styles$A.optionsMenuToggle, isDisabled && styles$A.modifiers.disabled, styles$A.modifiers.plain, styles$A.modifiers.text) }, showToggle && (React.createElement(React.Fragment, null,
        React.createElement("span", { className: css(styles$A.optionsMenuToggleText) }, typeof ToggleTemplate === 'string' ? (fillTemplate(ToggleTemplate, { firstIndex, lastIndex, itemCount, itemsTitle })) : (React.createElement(ToggleTemplate, { firstIndex: firstIndex, lastIndex: lastIndex, itemCount: itemCount, itemsTitle: itemsTitle }))),
        React.createElement(DropdownToggle, { onEnter: onEnter, "aria-label": optionsToggle, onToggle: onToggle, isDisabled: isDisabled || itemCount <= 0, isOpen: isOpen, id: `${widgetId}-toggle-${toggleId++}`, className: styles$A.optionsMenuToggleButton, parentRef: parentRef })))));
    OptionsToggle.displayName = 'OptionsToggle';

    class PaginationOptionsMenu extends React.Component {
        constructor(props) {
            super(props);
            this.parentRef = React.createRef();
            this.onToggle = (isOpen) => {
                this.setState({ isOpen });
            };
            this.onSelect = () => {
                this.setState((prevState) => ({ isOpen: !prevState.isOpen }));
            };
            this.handleNewPerPage = (_evt, newPerPage) => {
                const { page, onPerPageSelect, itemCount, defaultToFullPage } = this.props;
                let newPage = page;
                while (Math.ceil(itemCount / newPerPage) < newPage) {
                    newPage--;
                }
                if (defaultToFullPage) {
                    if (itemCount / newPerPage !== newPage) {
                        while (newPage > 1 && itemCount - newPerPage * newPage < 0) {
                            newPage--;
                        }
                    }
                }
                const startIdx = (newPage - 1) * newPerPage;
                const endIdx = newPage * newPerPage;
                return onPerPageSelect(_evt, newPerPage, newPage, startIdx, endIdx);
            };
            this.renderItems = () => {
                const { perPageOptions, perPage, perPageSuffix } = this.props;
                return perPageOptions.map(({ value, title }) => (React.createElement(DropdownItem, { key: value, component: "button", "data-action": `per-page-${value}`, className: css(perPage === value && 'pf-m-selected'), onClick: event => this.handleNewPerPage(event, value) },
                    title,
                    ` ${perPageSuffix}`,
                    perPage === value && (React.createElement("div", { className: css(styles$A.optionsMenuMenuItemIcon) },
                        React.createElement(CheckIcon, null))))));
            };
            this.state = {
                isOpen: false
            };
        }
        render() {
            const { widgetId, isDisabled, itemsPerPageTitle, dropDirection, optionsToggle, perPageOptions, toggleTemplate, firstIndex, lastIndex, itemCount, itemsTitle } = this.props;
            const { isOpen } = this.state;
            return (React.createElement(DropdownContext.Provider, { value: {
                    id: widgetId,
                    onSelect: this.onSelect,
                    toggleIndicatorClass: styles$A.optionsMenuToggleButtonIcon,
                    toggleTextClass: styles$A.optionsMenuToggleText,
                    menuClass: styles$A.optionsMenuMenu,
                    itemClass: styles$A.optionsMenuMenuItem,
                    toggleClass: ' ',
                    baseClass: styles$A.optionsMenu,
                    disabledClass: styles$A.modifiers.disabled,
                    menuComponent: 'ul',
                    baseComponent: 'div',
                    ouiaComponentType: PaginationOptionsMenu.displayName
                } },
                React.createElement(DropdownWithContext, { direction: dropDirection, isOpen: isOpen, toggle: React.createElement(OptionsToggle, { optionsToggle: optionsToggle, itemsPerPageTitle: itemsPerPageTitle, showToggle: perPageOptions && perPageOptions.length > 0, onToggle: this.onToggle, isOpen: isOpen, widgetId: widgetId, firstIndex: firstIndex, lastIndex: lastIndex, itemCount: itemCount, itemsTitle: itemsTitle, toggleTemplate: toggleTemplate, parentRef: this.parentRef.current, isDisabled: isDisabled }), dropdownItems: this.renderItems(), isPlain: true })));
        }
    }
    PaginationOptionsMenu.displayName = 'PaginationOptionsMenu';
    PaginationOptionsMenu.defaultProps = {
        className: '',
        widgetId: '',
        isDisabled: false,
        dropDirection: exports.DropdownDirection.down,
        perPageOptions: [],
        itemsPerPageTitle: 'Items per page',
        perPageSuffix: 'per page',
        optionsToggle: 'Select',
        perPage: 0,
        firstIndex: 0,
        lastIndex: 0,
        defaultToFullPage: false,
        itemCount: 0,
        itemsTitle: 'items',
        toggleTemplate: ({ firstIndex, lastIndex, itemCount, itemsTitle }) => (React.createElement(React.Fragment, null,
            React.createElement("b", null,
                firstIndex,
                " - ",
                lastIndex),
            ' ',
            "of",
            React.createElement("b", null, itemCount),
            " ",
            itemsTitle)),
        onPerPageSelect: () => null
    };

    var c_pagination__nav_page_select_c_form_control_width_chars = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.c_pagination__nav_page_select_c_form_control_width_chars = {
      "name": "--pf-c-pagination__nav-page-select--c-form-control--width-chars",
      "value": "2",
      "var": "var(--pf-c-pagination__nav-page-select--c-form-control--width-chars)"
    };
    exports["default"] = exports.c_pagination__nav_page_select_c_form_control_width_chars;
    });

    var widthChars = unwrapExports(c_pagination__nav_page_select_c_form_control_width_chars);
    var c_pagination__nav_page_select_c_form_control_width_chars_1 = c_pagination__nav_page_select_c_form_control_width_chars.c_pagination__nav_page_select_c_form_control_width_chars;

    (function (PaginationVariant) {
        PaginationVariant["top"] = "top";
        PaginationVariant["bottom"] = "bottom";
    })(exports.PaginationVariant || (exports.PaginationVariant = {}));
    const defaultPerPageOptions = [
        {
            title: '10',
            value: 10
        },
        {
            title: '20',
            value: 20
        },
        {
            title: '50',
            value: 50
        },
        {
            title: '100',
            value: 100
        }
    ];
    const handleInputWidth = (lastPage, node) => {
        if (!node) {
            return;
        }
        const len = String(lastPage).length;
        if (len >= 3) {
            node.style.setProperty(widthChars.name, `${len}`);
        }
        else {
            node.style.setProperty(widthChars.name, '2');
        }
    };
    let paginationId = 0;
    class Pagination extends React.Component {
        constructor() {
            super(...arguments);
            this.paginationRef = React.createRef();
        }
        getLastPage() {
            const { itemCount, perPage } = this.props;
            return Math.ceil(itemCount / perPage) || 0;
        }
        componentDidMount() {
            const node = this.paginationRef.current;
            handleInputWidth(this.getLastPage(), node);
        }
        componentDidUpdate(prevProps) {
            const node = this.paginationRef.current;
            if (prevProps.perPage !== this.props.perPage || prevProps.itemCount !== this.props.itemCount) {
                handleInputWidth(this.getLastPage(), node);
            }
        }
        render() {
            const _a = this.props, { children, className, variant, isDisabled, isCompact, isStatic, perPage, titles, firstPage, page: propPage, offset, defaultToFullPage, itemCount, itemsStart, itemsEnd, perPageOptions, dropDirection: dropDirectionProp, widgetId, toggleTemplate, onSetPage, onPerPageSelect, onFirstClick, onPreviousClick, onNextClick, onPageInput, onLastClick, ouiaId, ouiaSafe } = _a, props = __rest(_a, ["children", "className", "variant", "isDisabled", "isCompact", "isStatic", "perPage", "titles", "firstPage", "page", "offset", "defaultToFullPage", "itemCount", "itemsStart", "itemsEnd", "perPageOptions", "dropDirection", "widgetId", "toggleTemplate", "onSetPage", "onPerPageSelect", "onFirstClick", "onPreviousClick", "onNextClick", "onPageInput", "onLastClick", "ouiaId", "ouiaSafe"]);
            const dropDirection = dropDirectionProp || (variant === 'bottom' && !isStatic ? 'up' : 'down');
            let page = propPage;
            if (!page && offset) {
                page = Math.ceil(offset / perPage);
            }
            const lastPage = this.getLastPage();
            if (page < firstPage && itemCount > 0) {
                page = firstPage;
            }
            else if (page > lastPage) {
                page = lastPage;
            }
            const firstIndex = itemCount <= 0 ? 0 : (page - 1) * perPage + 1;
            let lastIndex;
            if (itemCount <= 0) {
                lastIndex = 0;
            }
            else {
                lastIndex = page === lastPage ? itemCount : page * perPage;
            }
            return (React.createElement("div", Object.assign({ ref: this.paginationRef, className: css(styles$D.pagination, variant === exports.PaginationVariant.bottom && styles$D.modifiers.bottom, isCompact && styles$D.modifiers.compact, isStatic && styles$D.modifiers.static, className), id: `${widgetId}-${paginationId++}` }, getOUIAProps(Pagination.displayName, ouiaId, ouiaSafe), props),
                variant === exports.PaginationVariant.top && (React.createElement("div", { className: css(styles$D.paginationTotalItems) },
                    React.createElement(ToggleTemplate, { firstIndex: firstIndex, lastIndex: lastIndex, itemCount: itemCount, itemsTitle: titles.items }))),
                React.createElement(PaginationOptionsMenu, { itemsPerPageTitle: titles.itemsPerPage, perPageSuffix: titles.perPageSuffix, itemsTitle: isCompact ? '' : titles.items, optionsToggle: titles.optionsToggle, perPageOptions: perPageOptions, firstIndex: itemsStart !== null ? itemsStart : firstIndex, lastIndex: itemsEnd !== null ? itemsEnd : lastIndex, defaultToFullPage: defaultToFullPage, itemCount: itemCount, page: page, perPage: perPage, lastPage: lastPage, onPerPageSelect: onPerPageSelect, dropDirection: dropDirection, widgetId: widgetId, toggleTemplate: toggleTemplate, isDisabled: isDisabled }),
                React.createElement(Navigation, { pagesTitle: titles.page, toLastPage: titles.toLastPage, toPreviousPage: titles.toPreviousPage, toNextPage: titles.toNextPage, toFirstPage: titles.toFirstPage, currPage: titles.currPage, paginationTitle: titles.paginationTitle, page: itemCount <= 0 ? 0 : page, perPage: perPage, firstPage: itemsStart !== null ? itemsStart : 1, lastPage: lastPage, onSetPage: onSetPage, onFirstClick: onFirstClick, onPreviousClick: onPreviousClick, onNextClick: onNextClick, onLastClick: onLastClick, onPageInput: onPageInput, isDisabled: isDisabled, isCompact: isCompact }),
                children));
        }
    }
    Pagination.displayName = 'Pagination';
    Pagination.defaultProps = {
        children: null,
        className: '',
        variant: exports.PaginationVariant.top,
        isDisabled: false,
        isCompact: false,
        perPage: defaultPerPageOptions[0].value,
        titles: {
            items: '',
            page: '',
            itemsPerPage: 'Items per page',
            perPageSuffix: 'per page',
            toFirstPage: 'Go to first page',
            toPreviousPage: 'Go to previous page',
            toLastPage: 'Go to last page',
            toNextPage: 'Go to next page',
            optionsToggle: 'Items per page',
            currPage: 'Current page',
            paginationTitle: 'Pagination'
        },
        firstPage: 1,
        page: 0,
        offset: 0,
        defaultToFullPage: false,
        itemsStart: null,
        itemsEnd: null,
        perPageOptions: defaultPerPageOptions,
        widgetId: 'pagination-options-menu',
        toggleTemplate: ToggleTemplate,
        onSetPage: () => undefined,
        onPerPageSelect: () => undefined,
        onFirstClick: () => undefined,
        onPreviousClick: () => undefined,
        onNextClick: () => undefined,
        onPageInput: () => undefined,
        onLastClick: () => undefined,
        ouiaSafe: true
    };

    var popover = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "button": "pf-c-button",
      "modifiers": {
        "top": "pf-m-top",
        "bottom": "pf-m-bottom",
        "left": "pf-m-left",
        "right": "pf-m-right"
      },
      "popover": "pf-c-popover",
      "popoverArrow": "pf-c-popover__arrow",
      "popoverBody": "pf-c-popover__body",
      "popoverContent": "pf-c-popover__content",
      "popoverFooter": "pf-c-popover__footer",
      "title": "pf-c-title"
    };
    });

    var styles$E = unwrapExports(popover);

    const PopoverContent = (_a) => {
        var { className = null, children } = _a, props = __rest(_a, ["className", "children"]);
        return (React.createElement("div", Object.assign({ className: css(styles$E.popoverContent, className) }, props), children));
    };
    PopoverContent.displayName = 'PopoverContent';

    const PopoverBody = (_a) => {
        var { children, id } = _a, props = __rest(_a, ["children", "id"]);
        return (React.createElement("div", Object.assign({ className: css(styles$E.popoverBody), id: id }, props), children));
    };
    PopoverBody.displayName = 'PopoverBody';

    const PopoverHeader = (_a) => {
        var { children, id } = _a, props = __rest(_a, ["children", "id"]);
        return (React.createElement(Title, Object.assign({ headingLevel: "h6", size: exports.TitleSizes.md, id: id }, props), children));
    };
    PopoverHeader.displayName = 'PopoverHeader';

    const PopoverFooter = (_a) => {
        var { children, className = '' } = _a, props = __rest(_a, ["children", "className"]);
        return (React.createElement("footer", Object.assign({ className: css(styles$E.popoverFooter, className) }, props), children));
    };
    PopoverFooter.displayName = 'PopoverFooter';

    const PopoverCloseButton = (_a) => {
        var { onClose = () => undefined } = _a, props = __rest(_a, ["onClose"]);
        const [closeButtonElement, setCloseButtonElement] = React.useState(null);
        React.useEffect(() => {
            closeButtonElement && closeButtonElement.addEventListener('click', onClose, false);
            return () => {
                closeButtonElement && closeButtonElement.removeEventListener('click', onClose, false);
            };
        }, [closeButtonElement]);
        return (React.createElement(FindRefWrapper, { onFoundRef: (foundRef) => setCloseButtonElement(foundRef) },
            React.createElement(Button, Object.assign({ variant: "plain", "aria-label": true }, props, { style: { pointerEvents: 'auto' } }),
                React.createElement(TimesIcon, null))));
    };
    PopoverCloseButton.displayName = 'PopoverCloseButton';

    const PopoverArrow = (_a) => {
        var { className = '' } = _a, props = __rest(_a, ["className"]);
        return React.createElement("div", Object.assign({ className: css(styles$E.popoverArrow, className) }, props));
    };
    PopoverArrow.displayName = 'PopoverArrow';

    var c_popover_MaxWidth = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.c_popover_MaxWidth = {
      "name": "--pf-c-popover--MaxWidth",
      "value": "calc(1rem + 1rem + 18.75rem)",
      "var": "var(--pf-c-popover--MaxWidth)"
    };
    exports["default"] = exports.c_popover_MaxWidth;
    });

    var popoverMaxWidth = unwrapExports(c_popover_MaxWidth);
    var c_popover_MaxWidth_1 = c_popover_MaxWidth.c_popover_MaxWidth;

    var c_popover_MinWidth = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;
    exports.c_popover_MinWidth = {
      "name": "--pf-c-popover--MinWidth",
      "value": "calc(1rem + 1rem + 18.75rem)",
      "var": "var(--pf-c-popover--MinWidth)"
    };
    exports["default"] = exports.c_popover_MinWidth;
    });

    var popoverMinWidth = unwrapExports(c_popover_MinWidth);
    var c_popover_MinWidth_1 = c_popover_MinWidth.c_popover_MinWidth;

    (function (PopoverPosition) {
        PopoverPosition["auto"] = "auto";
        PopoverPosition["top"] = "top";
        PopoverPosition["bottom"] = "bottom";
        PopoverPosition["left"] = "left";
        PopoverPosition["right"] = "right";
    })(exports.PopoverPosition || (exports.PopoverPosition = {}));
    const Popover = (_a) => {
        var { children, position = 'top', enableFlip = true, className = '', isVisible = null, shouldClose = () => null, shouldOpen = () => null, 'aria-label': ariaLabel = '', bodyContent, headerContent = null, footerContent = null, appendTo = () => document.body, hideOnOutsideClick = true, onHide = () => null, onHidden = () => null, onShow = () => null, onShown = () => null, onMount = () => null, zIndex = 9999, minWidth = popoverMinWidth && popoverMinWidth.value, maxWidth = popoverMaxWidth && popoverMaxWidth.value, closeBtnAriaLabel = 'Close', distance = 25, 
        // For every initial starting position, there are 3 escape positions
        flipBehavior = ['top', 'right', 'bottom', 'left', 'top', 'right', 'bottom'], animationDuration = 300, id, boundary, tippyProps } = _a, rest = __rest(_a, ["children", "position", "enableFlip", "className", "isVisible", "shouldClose", "shouldOpen", 'aria-label', "bodyContent", "headerContent", "footerContent", "appendTo", "hideOnOutsideClick", "onHide", "onHidden", "onShow", "onShown", "onMount", "zIndex", "minWidth", "maxWidth", "closeBtnAriaLabel", "distance", "flipBehavior", "animationDuration", "id", "boundary", "tippyProps"]);
        {
            boundary !== undefined &&
                console.warn('The Popover boundary prop has been deprecated. If you want to constrain the popper to a specific element use the appendTo prop instead.');
            tippyProps !== undefined && console.warn('The Popover tippyProps prop has been deprecated and is no longer used.');
        }
        // could make this a prop in the future (true | false | 'toggle')
        // const hideOnClick = true;
        const uniqueId = id || getUniqueId();
        const triggerManually = isVisible !== null;
        const [visible, setVisible] = React.useState(false);
        const [opacity, setOpacity] = React.useState(0);
        const [focusTrapActive, setFocusTrapActive] = React.useState(false);
        const transitionTimerRef = React.useRef(null);
        const showTimerRef = React.useRef(null);
        const hideTimerRef = React.useRef(null);
        React.useEffect(() => {
            onMount();
        }, []);
        React.useEffect(() => {
            if (triggerManually) {
                if (isVisible) {
                    show();
                }
                else {
                    hide();
                }
            }
        }, [isVisible, triggerManually]);
        const show = (withFocusTrap) => {
            onShow();
            if (transitionTimerRef.current) {
                clearTimeout(transitionTimerRef.current);
            }
            if (hideTimerRef.current) {
                clearTimeout(hideTimerRef.current);
            }
            showTimerRef.current = setTimeout(() => {
                setVisible(true);
                setOpacity(1);
                withFocusTrap && setFocusTrapActive(true);
                onShown();
            }, 0);
        };
        const hide = () => {
            onHide();
            if (showTimerRef.current) {
                clearTimeout(showTimerRef.current);
            }
            hideTimerRef.current = setTimeout(() => {
                setOpacity(0);
                setFocusTrapActive(false);
                transitionTimerRef.current = setTimeout(() => {
                    setVisible(false);
                    onHidden();
                }, animationDuration);
            }, 0);
        };
        const positionModifiers = {
            top: styles$E.modifiers.top,
            bottom: styles$E.modifiers.bottom,
            left: styles$E.modifiers.left,
            right: styles$E.modifiers.right
        };
        const hasCustomMinWidth = minWidth !== popoverMinWidth.value;
        const hasCustomMaxWidth = maxWidth !== popoverMaxWidth.value;
        const onDocumentKeyDown = (event) => {
            if (event.keyCode === KEY_CODES.ESCAPE_KEY && visible) {
                if (triggerManually) {
                    shouldClose(null, hide);
                }
                else {
                    hide();
                }
            }
        };
        const onDocumentClick = () => {
            // did not click on trigger or popper (otherwise the event bubbling would have been prevented) which means we clicked outside
            if (hideOnOutsideClick && visible) {
                if (triggerManually) {
                    shouldClose(null, hide);
                }
                else {
                    hide();
                }
            }
        };
        const onTriggerEnter = (event) => {
            if (event.keyCode === KEY_CODES.ENTER) {
                if (!visible) {
                    if (triggerManually) {
                        shouldOpen(show);
                    }
                    else {
                        show(true);
                    }
                }
                else {
                    if (triggerManually) {
                        shouldClose(null, hide);
                    }
                    else {
                        hide();
                    }
                }
            }
        };
        const onTriggerClick = () => {
            if (triggerManually) {
                if (visible) {
                    shouldClose(null, hide);
                }
                else {
                    shouldOpen(show);
                }
            }
            else {
                if (visible) {
                    hide();
                }
                else {
                    show();
                }
            }
        };
        const onPopperClick = (event) => {
            event.stopPropagation();
        };
        const onContentMouseDown = () => {
            if (focusTrapActive) {
                setFocusTrapActive(false);
            }
        };
        const closePopover = (event) => {
            event.stopPropagation();
            if (triggerManually) {
                shouldClose(null, hide);
            }
            else {
                hide();
            }
        };
        const content = (React.createElement(FocusTrap, Object.assign({ active: focusTrapActive, focusTrapOptions: { returnFocusOnDeactivate: true, clickOutsideDeactivates: true }, className: css(styles$E.popover, className), role: "dialog", "aria-modal": "true", "aria-label": headerContent ? undefined : ariaLabel, "aria-labelledby": headerContent ? `popover-${uniqueId}-header` : undefined, "aria-describedby": `popover-${uniqueId}-body`, onMouseDown: onContentMouseDown, style: {
                minWidth: hasCustomMinWidth ? minWidth : null,
                maxWidth: hasCustomMaxWidth ? maxWidth : null,
                opacity,
                transition: getOpacityTransition(animationDuration)
            } }, rest),
            React.createElement(PopoverArrow, null),
            React.createElement(PopoverContent, null,
                React.createElement(PopoverCloseButton, { onClose: closePopover, "aria-label": closeBtnAriaLabel }),
                headerContent && React.createElement(PopoverHeader, { id: `popover-${uniqueId}-header` }, headerContent),
                React.createElement(PopoverBody, { id: `popover-${uniqueId}-body` }, bodyContent),
                footerContent && React.createElement(PopoverFooter, { id: `popover-${uniqueId}-footer` }, footerContent))));
        return (React.createElement(Popper, { trigger: children, popper: content, popperMatchesTriggerWidth: false, appendTo: appendTo, isVisible: visible, positionModifiers: positionModifiers, distance: distance, placement: position, onTriggerClick: onTriggerClick, onTriggerEnter: onTriggerEnter, onPopperClick: onPopperClick, onDocumentClick: onDocumentClick, onDocumentKeyDown: onDocumentKeyDown, enableFlip: enableFlip, zIndex: zIndex, flipBehavior: flipBehavior }));
    };
    Popover.displayName = 'Popover';

    var progress = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "modifiers": {
        "sm": "pf-m-sm",
        "lg": "pf-m-lg",
        "inside": "pf-m-inside",
        "outside": "pf-m-outside",
        "singleline": "pf-m-singleline",
        "success": "pf-m-success",
        "danger": "pf-m-danger"
      },
      "progress": "pf-c-progress",
      "progressBar": "pf-c-progress__bar",
      "progressDescription": "pf-c-progress__description",
      "progressIndicator": "pf-c-progress__indicator",
      "progressMeasure": "pf-c-progress__measure",
      "progressStatus": "pf-c-progress__status",
      "progressStatusIcon": "pf-c-progress__status-icon"
    };
    });

    var styles$F = unwrapExports(progress);

    const ProgressBar = (_a) => {
        var { progressBarAriaProps, className = '', children = null, value } = _a, props = __rest(_a, ["progressBarAriaProps", "className", "children", "value"]);
        return (React.createElement("div", Object.assign({}, props, { className: css(styles$F.progressBar, className) }, progressBarAriaProps),
            React.createElement("div", { className: css(styles$F.progressIndicator), style: { width: `${value}%` } },
                React.createElement("span", { className: css(styles$F.progressMeasure) }, children))));
    };
    ProgressBar.displayName = 'ProgressBar';

    (function (ProgressMeasureLocation) {
        ProgressMeasureLocation["outside"] = "outside";
        ProgressMeasureLocation["inside"] = "inside";
        ProgressMeasureLocation["top"] = "top";
        ProgressMeasureLocation["none"] = "none";
    })(exports.ProgressMeasureLocation || (exports.ProgressMeasureLocation = {}));
    (function (ProgressVariant) {
        ProgressVariant["danger"] = "danger";
        ProgressVariant["success"] = "success";
    })(exports.ProgressVariant || (exports.ProgressVariant = {}));
    const variantToIcon = {
        danger: TimesCircleIcon,
        success: CheckCircleIcon
    };
    const ProgressContainer = ({ progressBarAriaProps, value, title = '', parentId, label = null, variant = null, measureLocation = exports.ProgressMeasureLocation.top }) => {
        const StatusIcon = variantToIcon.hasOwnProperty(variant) && variantToIcon[variant];
        return (React.createElement(React.Fragment, null,
            React.createElement("div", { className: css(styles$F.progressDescription), id: `${parentId}-description`, "aria-hidden": "true" }, title),
            React.createElement("div", { className: css(styles$F.progressStatus), "aria-hidden": "true" },
                (measureLocation === exports.ProgressMeasureLocation.top || measureLocation === exports.ProgressMeasureLocation.outside) && (React.createElement("span", { className: css(styles$F.progressMeasure) }, label || `${value}%`)),
                variantToIcon.hasOwnProperty(variant) && (React.createElement("span", { className: css(styles$F.progressStatusIcon) },
                    React.createElement(StatusIcon, null)))),
            React.createElement(ProgressBar, { role: "progressbar", progressBarAriaProps: progressBarAriaProps, value: value }, measureLocation === exports.ProgressMeasureLocation.inside && `${value}%`)));
    };
    ProgressContainer.displayName = 'ProgressContainer';

    (function (ProgressSize) {
        ProgressSize["sm"] = "sm";
        ProgressSize["md"] = "md";
        ProgressSize["lg"] = "lg";
    })(exports.ProgressSize || (exports.ProgressSize = {}));
    class Progress extends React.Component {
        constructor() {
            super(...arguments);
            this.id = this.props.id || getUniqueId();
        }
        render() {
            const _a = this.props, { 
            /* eslint-disable @typescript-eslint/no-unused-vars */
            id, size, 
            /* eslint-enable @typescript-eslint/no-unused-vars */
            className, value, title, label, variant, measureLocation, min, max, valueText } = _a, props = __rest(_a, ["id", "size", "className", "value", "title", "label", "variant", "measureLocation", "min", "max", "valueText"]);
            const progressBarAriaProps = {
                'aria-labelledby': `${this.id}-description`,
                'aria-valuemin': min,
                'aria-valuenow': value,
                'aria-valuemax': max
            };
            if (valueText) {
                progressBarAriaProps['aria-valuetext'] = valueText;
            }
            const scaledValue = Math.min(100, Math.max(0, Math.floor(((value - min) / (max - min)) * 100)));
            return (React.createElement("div", Object.assign({}, props, { className: css(styles$F.progress, styles$F.modifiers[variant], ['inside', 'outside'].includes(measureLocation) && styles$F.modifiers[measureLocation], measureLocation === 'inside' ? styles$F.modifiers[exports.ProgressSize.lg] : styles$F.modifiers[size], !title && styles$F.modifiers.singleline, className), id: this.id }),
                React.createElement(ProgressContainer, { parentId: this.id, value: scaledValue, title: title, label: label, variant: variant, measureLocation: measureLocation, progressBarAriaProps: progressBarAriaProps })));
        }
    }
    Progress.displayName = 'Progress';
    Progress.defaultProps = {
        className: '',
        measureLocation: exports.ProgressMeasureLocation.top,
        variant: null,
        id: '',
        title: '',
        min: 0,
        max: 100,
        size: null,
        label: null,
        value: 0,
        valueText: null
    };

    var radio = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "modifiers": {
        "disabled": "pf-m-disabled"
      },
      "radio": "pf-c-radio",
      "radioDescription": "pf-c-radio__description",
      "radioInput": "pf-c-radio__input",
      "radioLabel": "pf-c-radio__label"
    };
    });

    var styles$G = unwrapExports(radio);

    class Radio extends React.Component {
        constructor(props) {
            super(props);
            this.handleChange = (event) => {
                this.props.onChange(event.currentTarget.checked, event);
            };
            if (!props.label && !props['aria-label']) {
                // eslint-disable-next-line no-console
                console.error('Radio:', 'Radio requires an aria-label to be specified');
            }
        }
        render() {
            const _a = this.props, { 'aria-label': ariaLabel, checked, className, defaultChecked, isLabelWrapped, isLabelBeforeButton, isChecked, isDisabled, isValid, label, 
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            onChange, description, ouiaId, ouiaSafe = true } = _a, props = __rest(_a, ['aria-label', "checked", "className", "defaultChecked", "isLabelWrapped", "isLabelBeforeButton", "isChecked", "isDisabled", "isValid", "label", "onChange", "description", "ouiaId", "ouiaSafe"]);
            const inputRendered = (React.createElement("input", Object.assign({}, props, { className: css(styles$G.radioInput), type: "radio", onChange: this.handleChange, "aria-invalid": !isValid, disabled: isDisabled, checked: checked || isChecked }, (checked === undefined && { defaultChecked }), (!label && { 'aria-label': ariaLabel }), getOUIAProps(Radio.displayName, ouiaId, ouiaSafe))));
            const labelRendered = !label ? null : isLabelWrapped ? (React.createElement("span", { className: css(styles$G.radioLabel, isDisabled && styles$G.modifiers.disabled) }, label)) : (React.createElement("label", { className: css(styles$G.radioLabel, isDisabled && styles$G.modifiers.disabled), htmlFor: props.id }, label));
            const descRender = description ? React.createElement("div", { className: css(styles$G.radioDescription) }, description) : null;
            const childrenRendered = isLabelBeforeButton ? (React.createElement(React.Fragment, null,
                labelRendered,
                inputRendered,
                descRender)) : (React.createElement(React.Fragment, null,
                inputRendered,
                labelRendered,
                descRender));
            return isLabelWrapped ? (React.createElement("label", { className: css(styles$G.radio, className), htmlFor: props.id }, childrenRendered)) : (React.createElement("div", { className: css(styles$G.radio, className) }, childrenRendered));
        }
    }
    Radio.displayName = 'Radio';
    Radio.defaultProps = {
        className: '',
        isDisabled: false,
        isValid: true,
        onChange: () => { }
    };

    var simpleList = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "modifiers": {
        "current": "pf-m-current"
      },
      "simpleList": "pf-c-simple-list",
      "simpleListItemLink": "pf-c-simple-list__item-link",
      "simpleListSection": "pf-c-simple-list__section",
      "simpleListTitle": "pf-c-simple-list__title"
    };
    });

    var styles$H = unwrapExports(simpleList);

    const SimpleListGroup = (_a) => {
        var { children = null, className = '', title = '', titleClassName = '', id = '' } = _a, props = __rest(_a, ["children", "className", "title", "titleClassName", "id"]);
        return (React.createElement("section", Object.assign({ className: css(styles$H.simpleListSection) }, props),
            React.createElement("h2", { id: id, className: css(styles$H.simpleListTitle, titleClassName), "aria-hidden": "true" }, title),
            React.createElement("ul", { className: css(className), "aria-labelledby": id }, children)));
    };
    SimpleListGroup.displayName = 'SimpleListGroup';

    const SimpleListContext = React.createContext({});
    class SimpleList extends React.Component {
        constructor() {
            super(...arguments);
            this.state = {
                currentRef: null
            };
            this.handleCurrentUpdate = (newCurrentRef, itemProps) => {
                this.setState({ currentRef: newCurrentRef });
                const { onSelect } = this.props;
                onSelect && onSelect(newCurrentRef, itemProps);
            };
        }
        componentDidMount() {
            if (!SimpleList.hasWarnBeta && 'development' !== 'production') {
                // eslint-disable-next-line no-console
                console.warn('This component is in beta and subject to change.');
                SimpleList.hasWarnBeta = true;
            }
        }
        render() {
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            const _a = this.props, { children, className, onSelect } = _a, props = __rest(_a, ["children", "className", "onSelect"]);
            let isGrouped = false;
            if (children) {
                isGrouped = React.Children.toArray(children)[0].type === SimpleListGroup;
            }
            return (React.createElement(SimpleListContext.Provider, { value: {
                    currentRef: this.state.currentRef,
                    updateCurrentRef: this.handleCurrentUpdate
                } },
                React.createElement("div", Object.assign({ className: css(styles$H.simpleList, className) }, props, (isGrouped && { role: 'list' })),
                    isGrouped && children,
                    !isGrouped && React.createElement("ul", null, children))));
        }
    }
    SimpleList.displayName = 'SimpleList';
    SimpleList.hasWarnBeta = false;
    SimpleList.defaultProps = {
        children: null,
        className: ''
    };

    class SimpleListItem extends React.Component {
        constructor() {
            super(...arguments);
            this.ref = React.createRef();
        }
        render() {
            const _a = this.props, { children, isCurrent, className, component: Component, componentClassName, componentProps, onClick, type, href } = _a, props = __rest(_a, ["children", "isCurrent", "className", "component", "componentClassName", "componentProps", "onClick", "type", "href"]);
            return (React.createElement(SimpleListContext.Consumer, null, ({ currentRef, updateCurrentRef }) => {
                const isButton = Component === 'button';
                const isCurrentItem = this.ref && currentRef ? currentRef.current === this.ref.current : isCurrent;
                const additionalComponentProps = isButton
                    ? {
                        type
                    }
                    : {
                        tabIndex: 0,
                        href
                    };
                return (React.createElement("li", Object.assign({ className: css(className) }, props),
                    React.createElement(Component, Object.assign({ className: css(styles$H.simpleListItemLink, isCurrentItem && styles$H.modifiers.current, componentClassName), onClick: (evt) => {
                            onClick(evt);
                            updateCurrentRef(this.ref, this.props);
                        }, ref: this.ref }, componentProps, additionalComponentProps), children)));
            }));
        }
    }
    SimpleListItem.displayName = 'SimpleListItem';
    SimpleListItem.defaultProps = {
        children: null,
        className: '',
        isCurrent: false,
        component: 'button',
        componentClassName: '',
        type: 'button',
        href: '',
        onClick: () => { }
    };

    var skipToContent = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "skipToContent": "pf-c-skip-to-content"
    };
    });

    var styles$I = unwrapExports(skipToContent);

    class SkipToContent extends React.Component {
        constructor() {
            super(...arguments);
            this.componentRef = React.createRef();
        }
        componentDidMount() {
            if (this.props.show && this.componentRef.current) {
                this.componentRef.current.focus();
            }
        }
        render() {
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            const _a = this.props, { children, className, href, show, type } = _a, rest = __rest(_a, ["children", "className", "href", "show", "type"]);
            return (React.createElement("a", Object.assign({}, rest, { className: css(buttonStyles.button, buttonStyles.modifiers.primary, styles$I.skipToContent, className), ref: this.componentRef, href: href }), children));
        }
    }
    SkipToContent.displayName = 'SkipToContent';
    SkipToContent.defaultProps = {
        show: false
    };

    var _switch = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "modifiers": {
        "off": "pf-m-off",
        "on": "pf-m-on"
      },
      "switch": "pf-c-switch",
      "switchInput": "pf-c-switch__input",
      "switchLabel": "pf-c-switch__label",
      "switchToggle": "pf-c-switch__toggle",
      "switchToggleIcon": "pf-c-switch__toggle-icon"
    };
    });

    var styles$J = unwrapExports(_switch);

    class Switch extends React.Component {
        constructor(props) {
            super(props);
            if (!props.id && !props['aria-label']) {
                // eslint-disable-next-line no-console
                console.error('Switch: Switch requires either an id or aria-label to be specified');
            }
            this.id = props.id || getUniqueId();
        }
        render() {
            const _a = this.props, { 
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            id, className, label, labelOff, isChecked, isDisabled, onChange, ouiaId, ouiaSafe } = _a, props = __rest(_a, ["id", "className", "label", "labelOff", "isChecked", "isDisabled", "onChange", "ouiaId", "ouiaSafe"]);
            const isAriaLabelledBy = props['aria-label'] === '';
            return (React.createElement("label", Object.assign({ className: css(styles$J.switch, className), htmlFor: this.id }, getOUIAProps(Switch.displayName, ouiaId, ouiaSafe)),
                React.createElement("input", Object.assign({ id: this.id, className: css(styles$J.switchInput), type: "checkbox", onChange: event => onChange(event.target.checked, event), checked: isChecked, disabled: isDisabled, "aria-labelledby": isAriaLabelledBy ? `${this.id}-on` : null }, props)),
                label !== undefined ? (React.createElement(React.Fragment, null,
                    React.createElement("span", { className: css(styles$J.switchToggle) }),
                    React.createElement("span", { className: css(styles$J.switchLabel, styles$J.modifiers.on), id: isAriaLabelledBy ? `${this.id}-on` : null, "aria-hidden": "true" }, label),
                    React.createElement("span", { className: css(styles$J.switchLabel, styles$J.modifiers.off), id: isAriaLabelledBy ? `${this.id}-off` : null, "aria-hidden": "true" }, labelOff !== undefined ? labelOff : label))) : (React.createElement("span", { className: css(styles$J.switchToggle) },
                    React.createElement("div", { className: css(styles$J.switchToggleIcon), "aria-hidden": "true" },
                        React.createElement(CheckIcon, { noVerticalAlign: true }))))));
        }
    }
    Switch.displayName = 'Switch';
    Switch.defaultProps = {
        isChecked: true,
        isDisabled: false,
        'aria-label': '',
        onChange: () => undefined
    };

    const Tab = () => null;
    Tab.displayName = 'Tab';

    var tabs = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "modifiers": {
        "fill": "pf-m-fill",
        "scrollable": "pf-m-scrollable",
        "secondary": "pf-m-secondary",
        "noBorderBottom": "pf-m-no-border-bottom",
        "box": "pf-m-box",
        "vertical": "pf-m-vertical",
        "current": "pf-m-current",
        "insetNone": "pf-m-inset-none",
        "insetSm": "pf-m-inset-sm",
        "insetMd": "pf-m-inset-md",
        "insetLg": "pf-m-inset-lg",
        "insetXl": "pf-m-inset-xl",
        "inset_2xl": "pf-m-inset-2xl",
        "insetNoneOnSm": "pf-m-inset-none-on-sm",
        "insetSmOnSm": "pf-m-inset-sm-on-sm",
        "insetMdOnSm": "pf-m-inset-md-on-sm",
        "insetLgOnSm": "pf-m-inset-lg-on-sm",
        "insetXlOnSm": "pf-m-inset-xl-on-sm",
        "inset_2xlOnSm": "pf-m-inset-2xl-on-sm",
        "insetNoneOnMd": "pf-m-inset-none-on-md",
        "insetSmOnMd": "pf-m-inset-sm-on-md",
        "insetMdOnMd": "pf-m-inset-md-on-md",
        "insetLgOnMd": "pf-m-inset-lg-on-md",
        "insetXlOnMd": "pf-m-inset-xl-on-md",
        "inset_2xlOnMd": "pf-m-inset-2xl-on-md",
        "insetNoneOnLg": "pf-m-inset-none-on-lg",
        "insetSmOnLg": "pf-m-inset-sm-on-lg",
        "insetMdOnLg": "pf-m-inset-md-on-lg",
        "insetLgOnLg": "pf-m-inset-lg-on-lg",
        "insetXlOnLg": "pf-m-inset-xl-on-lg",
        "inset_2xlOnLg": "pf-m-inset-2xl-on-lg",
        "insetNoneOnXl": "pf-m-inset-none-on-xl",
        "insetSmOnXl": "pf-m-inset-sm-on-xl",
        "insetMdOnXl": "pf-m-inset-md-on-xl",
        "insetLgOnXl": "pf-m-inset-lg-on-xl",
        "insetXlOnXl": "pf-m-inset-xl-on-xl",
        "inset_2xlOnXl": "pf-m-inset-2xl-on-xl",
        "insetNoneOn_2xl": "pf-m-inset-none-on-2xl",
        "insetSmOn_2xl": "pf-m-inset-sm-on-2xl",
        "insetMdOn_2xl": "pf-m-inset-md-on-2xl",
        "insetLgOn_2xl": "pf-m-inset-lg-on-2xl",
        "insetXlOn_2xl": "pf-m-inset-xl-on-2xl",
        "inset_2xlOn_2xl": "pf-m-inset-2xl-on-2xl"
      },
      "tabs": "pf-c-tabs",
      "tabsItem": "pf-c-tabs__item",
      "tabsItemIcon": "pf-c-tabs__item-icon",
      "tabsItemText": "pf-c-tabs__item-text",
      "tabsLink": "pf-c-tabs__link",
      "tabsList": "pf-c-tabs__list",
      "tabsScrollButton": "pf-c-tabs__scroll-button"
    };
    });

    var styles$K = unwrapExports(tabs);

    const TabButton = (_a) => {
        var { children, 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        tabContentRef } = _a, props = __rest(_a, ["children", "tabContentRef"]);
        const Component = (props.href ? 'a' : 'button');
        return React.createElement(Component, Object.assign({}, props), children);
    };
    TabButton.displayName = 'TabButton';

    const TabContentBase = (_a) => {
        var { id, activeKey, 'aria-label': ariaLabel, child, children, className, eventKey, innerRef } = _a, props = __rest(_a, ["id", "activeKey", 'aria-label', "child", "children", "className", "eventKey", "innerRef"]);
        if (children || child) {
            let labelledBy;
            if (ariaLabel) {
                labelledBy = null;
            }
            else {
                labelledBy = children ? `pf-tab-${eventKey}-${id}` : `pf-tab-${child.props.eventKey}-${id}`;
            }
            return (React.createElement("section", Object.assign({ ref: innerRef, hidden: children ? null : child.props.eventKey !== activeKey, className: children ? css('pf-c-tab-content', className) : css('pf-c-tab-content', child.props.className), id: children ? id : `pf-tab-section-${child.props.eventKey}-${id}`, "aria-label": ariaLabel, "aria-labelledby": labelledBy, role: "tabpanel", tabIndex: 0 }, props), children || child.props.children));
        }
        return null;
    };
    const TabContent = React.forwardRef((props, ref) => (React.createElement(TabContentBase, Object.assign({}, props, { innerRef: ref }))));

    (function (TabsComponent) {
        TabsComponent["div"] = "div";
        TabsComponent["nav"] = "nav";
    })(exports.TabsComponent || (exports.TabsComponent = {}));
    class Tabs extends React.Component {
        constructor(props) {
            super(props);
            this.tabList = React.createRef();
            this.handleScrollButtons = () => {
                if (this.tabList.current && !this.props.isVertical) {
                    const container = this.tabList.current;
                    // get first element and check if it is in view
                    const overflowOnLeft = !isElementInView(container, container.firstChild, false);
                    // get last element and check if it is in view
                    const overflowOnRight = !isElementInView(container, container.lastChild, false);
                    const showScrollButtons = overflowOnLeft || overflowOnRight;
                    const disableLeftScrollButton = !overflowOnLeft;
                    const disableRightScrollButton = !overflowOnRight;
                    this.setState({
                        showScrollButtons,
                        disableLeftScrollButton,
                        disableRightScrollButton
                    });
                }
            };
            this.scrollLeft = () => {
                // find first Element that is fully in view on the left, then scroll to the element before it
                if (this.tabList.current) {
                    const container = this.tabList.current;
                    const childrenArr = Array.from(container.children);
                    let firstElementInView;
                    let lastElementOutOfView;
                    let i;
                    for (i = 0; i < childrenArr.length && !firstElementInView; i++) {
                        if (isElementInView(container, childrenArr[i], false)) {
                            firstElementInView = childrenArr[i];
                            lastElementOutOfView = childrenArr[i - 1];
                        }
                    }
                    if (lastElementOutOfView) {
                        container.scrollLeft -= lastElementOutOfView.scrollWidth;
                    }
                }
            };
            this.scrollRight = () => {
                // find last Element that is fully in view on the right, then scroll to the element after it
                if (this.tabList.current) {
                    const container = this.tabList.current;
                    const childrenArr = Array.from(container.children);
                    let lastElementInView;
                    let firstElementOutOfView;
                    for (let i = childrenArr.length - 1; i >= 0 && !lastElementInView; i--) {
                        if (isElementInView(container, childrenArr[i], false)) {
                            lastElementInView = childrenArr[i];
                            firstElementOutOfView = childrenArr[i + 1];
                        }
                    }
                    if (firstElementOutOfView) {
                        container.scrollLeft += firstElementOutOfView.scrollWidth;
                    }
                }
            };
            this.state = {
                showScrollButtons: false,
                disableLeftScrollButton: false,
                disableRightScrollButton: false,
                shownKeys: [this.props.activeKey] // only for mountOnEnter case
            };
        }
        handleTabClick(event, eventKey, tabContentRef, mountOnEnter) {
            const { shownKeys } = this.state;
            this.props.onSelect(event, eventKey);
            // process any tab content sections outside of the component
            if (tabContentRef) {
                React.Children.toArray(this.props.children)
                    .filter(child => child.props && child.props.tabContentRef && child.props.tabContentRef.current)
                    .forEach(child => (child.props.tabContentRef.current.hidden = true));
                // most recently selected tabContent
                if (tabContentRef.current) {
                    tabContentRef.current.hidden = false;
                }
            }
            if (mountOnEnter) {
                this.setState({
                    shownKeys: shownKeys.concat(eventKey)
                });
            }
        }
        componentDidMount() {
            if (!this.props.isVertical) {
                window.addEventListener('resize', this.handleScrollButtons, false);
                // call the handle resize function to check if scroll buttons should be shown
                this.handleScrollButtons();
            }
        }
        componentWillUnmount() {
            if (!this.props.isVertical) {
                window.removeEventListener('resize', this.handleScrollButtons, false);
            }
        }
        render() {
            const _a = this.props, { className, children, activeKey, id, isFilled, isSecondary, isVertical, isBox, leftScrollAriaLabel, rightScrollAriaLabel, 'aria-label': ariaLabel, component, ouiaId, ouiaSafe, mountOnEnter, unmountOnExit, inset } = _a, props = __rest(_a, ["className", "children", "activeKey", "id", "isFilled", "isSecondary", "isVertical", "isBox", "leftScrollAriaLabel", "rightScrollAriaLabel", 'aria-label', "component", "ouiaId", "ouiaSafe", "mountOnEnter", "unmountOnExit", "inset"]);
            const { showScrollButtons, disableLeftScrollButton, disableRightScrollButton, shownKeys } = this.state;
            const uniqueId = id || getUniqueId();
            const Component = component === exports.TabsComponent.nav ? 'nav' : 'div';
            return (React.createElement(React.Fragment, null,
                React.createElement(Component, Object.assign({ "aria-label": ariaLabel, className: css(styles$K.tabs, isFilled && styles$K.modifiers.fill, isSecondary && styles$K.modifiers.secondary, isVertical && styles$K.modifiers.vertical, isBox && styles$K.modifiers.box, showScrollButtons && !isVertical && styles$K.modifiers.scrollable, formatBreakpointMods(inset, styles$K), className) }, getOUIAProps(Tabs.displayName, ouiaId, ouiaSafe), { id: id && id }, props),
                    React.createElement("button", { className: css(styles$K.tabsScrollButton, isSecondary && buttonStyles.modifiers.secondary), "aria-label": leftScrollAriaLabel, onClick: this.scrollLeft, disabled: disableLeftScrollButton, "aria-hidden": disableLeftScrollButton },
                        React.createElement(AngleLeftIcon, null)),
                    React.createElement("ul", { className: css(styles$K.tabsList), ref: this.tabList, onScroll: this.handleScrollButtons }, React.Children.toArray(children)
                        .filter(Boolean)
                        .map((child, index) => {
                        const _a = child.props, { title, eventKey, tabContentRef, id: childId, tabContentId, isHidden = false } = _a, rest = __rest(_a, ["title", "eventKey", "tabContentRef", "id", "tabContentId", "isHidden"]);
                        return isHidden ? null : (React.createElement("li", { key: index, className: css(styles$K.tabsItem, eventKey === activeKey && styles$K.modifiers.current, className) },
                            React.createElement(TabButton, Object.assign({ className: css(styles$K.tabsLink), onClick: (event) => this.handleTabClick(event, eventKey, tabContentRef, mountOnEnter), id: `pf-tab-${eventKey}-${childId || uniqueId}`, "aria-controls": tabContentId ? `${tabContentId}` : `pf-tab-section-${eventKey}-${childId || uniqueId}`, tabContentRef: tabContentRef }, rest), title)));
                    })),
                    React.createElement("button", { className: css(styles$K.tabsScrollButton, isSecondary && buttonStyles.modifiers.secondary), "aria-label": rightScrollAriaLabel, onClick: this.scrollRight, disabled: disableRightScrollButton, "aria-hidden": disableRightScrollButton },
                        React.createElement(AngleRightIcon, null))),
                React.Children.toArray(children)
                    .filter(child => child &&
                    child.props.children &&
                    !(unmountOnExit && child.props.eventKey !== activeKey) &&
                    !(mountOnEnter && shownKeys.indexOf(child.props.eventKey) === -1))
                    .map((child, index) => (React.createElement(TabContent, { key: index, activeKey: activeKey, child: child, id: child.props.id || uniqueId })))));
        }
    }
    Tabs.displayName = 'Tabs';
    Tabs.defaultProps = {
        activeKey: 0,
        onSelect: () => undefined,
        isFilled: false,
        isSecondary: false,
        isVertical: false,
        isBox: false,
        leftScrollAriaLabel: 'Scroll left',
        rightScrollAriaLabel: 'Scroll right',
        component: exports.TabsComponent.div,
        mountOnEnter: false,
        unmountOnExit: false,
        ouiaSafe: true
    };

    const TabTitleText = (_a) => {
        var { children, className = '' } = _a, props = __rest(_a, ["children", "className"]);
        return (React.createElement("span", Object.assign({ className: css(styles$K.tabsItemText, className) }, props), children));
    };
    TabTitleText.displayName = 'TabTitleText';

    const TabTitleIcon = (_a) => {
        var { children, className = '' } = _a, props = __rest(_a, ["children", "className"]);
        return (React.createElement("span", Object.assign({ className: css(styles$K.tabsItemIcon, className) }, props), children));
    };
    TabTitleIcon.displayName = 'TabTitleIcon';

    var toolbar = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "button": "pf-c-button",
      "chipGroup": "pf-c-chip-group",
      "divider": "pf-c-divider",
      "modifiers": {
        "vertical": "pf-m-vertical",
        "buttonGroup": "pf-m-button-group",
        "iconButtonGroup": "pf-m-icon-button-group",
        "filterGroup": "pf-m-filter-group",
        "toggleGroup": "pf-m-toggle-group",
        "alignRight": "pf-m-align-right",
        "overflowMenu": "pf-m-overflow-menu",
        "bulkSelect": "pf-m-bulk-select",
        "searchFilter": "pf-m-search-filter",
        "chipGroup": "pf-m-chip-group",
        "label": "pf-m-label",
        "pagination": "pf-m-pagination",
        "expanded": "pf-m-expanded",
        "chipContainer": "pf-m-chip-container",
        "plain": "pf-m-plain",
        "show": "pf-m-show",
        "showOnMd": "pf-m-show-on-md",
        "showOnLg": "pf-m-show-on-lg",
        "showOnXl": "pf-m-show-on-xl",
        "showOn_2xl": "pf-m-show-on-2xl",
        "alignLeft": "pf-m-align-left",
        "hidden": "pf-m-hidden",
        "visible": "pf-m-visible",
        "alignRightOnMd": "pf-m-align-right-on-md",
        "alignLeftOnMd": "pf-m-align-left-on-md",
        "hiddenOnMd": "pf-m-hidden-on-md",
        "visibleOnMd": "pf-m-visible-on-md",
        "alignRightOnLg": "pf-m-align-right-on-lg",
        "alignLeftOnLg": "pf-m-align-left-on-lg",
        "hiddenOnLg": "pf-m-hidden-on-lg",
        "visibleOnLg": "pf-m-visible-on-lg",
        "alignRightOnXl": "pf-m-align-right-on-xl",
        "alignLeftOnXl": "pf-m-align-left-on-xl",
        "hiddenOnXl": "pf-m-hidden-on-xl",
        "visibleOnXl": "pf-m-visible-on-xl",
        "alignRightOn_2xl": "pf-m-align-right-on-2xl",
        "alignLeftOn_2xl": "pf-m-align-left-on-2xl",
        "hiddenOn_2xl": "pf-m-hidden-on-2xl",
        "visibleOn_2xl": "pf-m-visible-on-2xl",
        "spaceItemsNone": "pf-m-space-items-none",
        "spaceItemsSm": "pf-m-space-items-sm",
        "spaceItemsMd": "pf-m-space-items-md",
        "spaceItemsLg": "pf-m-space-items-lg",
        "spaceItemsNoneOnMd": "pf-m-space-items-none-on-md",
        "spaceItemsSmOnMd": "pf-m-space-items-sm-on-md",
        "spaceItemsMdOnMd": "pf-m-space-items-md-on-md",
        "spaceItemsLgOnMd": "pf-m-space-items-lg-on-md",
        "spaceItemsNoneOnLg": "pf-m-space-items-none-on-lg",
        "spaceItemsSmOnLg": "pf-m-space-items-sm-on-lg",
        "spaceItemsMdOnLg": "pf-m-space-items-md-on-lg",
        "spaceItemsLgOnLg": "pf-m-space-items-lg-on-lg",
        "spaceItemsNoneOnXl": "pf-m-space-items-none-on-xl",
        "spaceItemsSmOnXl": "pf-m-space-items-sm-on-xl",
        "spaceItemsMdOnXl": "pf-m-space-items-md-on-xl",
        "spaceItemsLgOnXl": "pf-m-space-items-lg-on-xl",
        "spaceItemsNoneOn_2xl": "pf-m-space-items-none-on-2xl",
        "spaceItemsSmOn_2xl": "pf-m-space-items-sm-on-2xl",
        "spaceItemsMdOn_2xl": "pf-m-space-items-md-on-2xl",
        "spaceItemsLgOn_2xl": "pf-m-space-items-lg-on-2xl",
        "spacerNone": "pf-m-spacer-none",
        "spacerSm": "pf-m-spacer-sm",
        "spacerMd": "pf-m-spacer-md",
        "spacerLg": "pf-m-spacer-lg",
        "spacerNoneOnMd": "pf-m-spacer-none-on-md",
        "spacerSmOnMd": "pf-m-spacer-sm-on-md",
        "spacerMdOnMd": "pf-m-spacer-md-on-md",
        "spacerLgOnMd": "pf-m-spacer-lg-on-md",
        "spacerNoneOnLg": "pf-m-spacer-none-on-lg",
        "spacerSmOnLg": "pf-m-spacer-sm-on-lg",
        "spacerMdOnLg": "pf-m-spacer-md-on-lg",
        "spacerLgOnLg": "pf-m-spacer-lg-on-lg",
        "spacerNoneOnXl": "pf-m-spacer-none-on-xl",
        "spacerSmOnXl": "pf-m-spacer-sm-on-xl",
        "spacerMdOnXl": "pf-m-spacer-md-on-xl",
        "spacerLgOnXl": "pf-m-spacer-lg-on-xl",
        "spacerNoneOn_2xl": "pf-m-spacer-none-on-2xl",
        "spacerSmOn_2xl": "pf-m-spacer-sm-on-2xl",
        "spacerMdOn_2xl": "pf-m-spacer-md-on-2xl",
        "spacerLgOn_2xl": "pf-m-spacer-lg-on-2xl"
      },
      "pagination": "pf-c-pagination",
      "toolbar": "pf-c-toolbar",
      "toolbarContent": "pf-c-toolbar__content",
      "toolbarContentSection": "pf-c-toolbar__content-section",
      "toolbarExpandableContent": "pf-c-toolbar__expandable-content",
      "toolbarGroup": "pf-c-toolbar__group",
      "toolbarItem": "pf-c-toolbar__item",
      "toolbarToggle": "pf-c-toolbar__toggle"
    };
    });

    var styles$L = unwrapExports(toolbar);

    const ToolbarContext = React.createContext({
        isExpanded: false,
        toggleIsExpanded: () => { },
        chipGroupContentRef: null,
        updateNumberFilters: () => { },
        numberOfFilters: 0,
        clearAllFilters: () => { }
    });
    const ToolbarContentContext = React.createContext({
        expandableContentRef: null,
        expandableContentId: '',
        chipContainerRef: null
    });
    const globalBreakpoints = {
        md: parseInt(globalBreakpointMd.value),
        lg: parseInt(globalBreakpointLg.value),
        xl: parseInt(globalBreakpointXl.value),
        '2xl': parseInt(globalBreakpoint2xl.value)
    };

    (function (ToolbarItemVariant) {
        ToolbarItemVariant["separator"] = "separator";
        ToolbarItemVariant["bulk-select"] = "bulk-select";
        ToolbarItemVariant["overflow-menu"] = "overflow-menu";
        ToolbarItemVariant["pagination"] = "pagination";
        ToolbarItemVariant["search-filter"] = "search-filter";
        ToolbarItemVariant["label"] = "label";
        ToolbarItemVariant["chip-group"] = "chip-group";
    })(exports.ToolbarItemVariant || (exports.ToolbarItemVariant = {}));
    const ToolbarItem = (_a) => {
        var { className, variant, visiblity, alignment, spacer, id, children } = _a, props = __rest(_a, ["className", "variant", "visiblity", "alignment", "spacer", "id", "children"]);
        if (variant === exports.ToolbarItemVariant.separator) {
            return React.createElement(Divider, Object.assign({ className: css(styles$L.modifiers.vertical, className) }, props));
        }
        return (React.createElement("div", Object.assign({ className: css(styles$L.toolbarItem, variant &&
                styles$L.modifiers[toCamel(variant)], formatBreakpointMods(visiblity, styles$L), formatBreakpointMods(alignment, styles$L), formatBreakpointMods(spacer, styles$L), className) }, (variant === 'label' && { 'aria-hidden': true }), { id: id }, props), children));
    };
    ToolbarItem.displayName = 'ToolbarItem';

    (function (ToolbarGroupVariant) {
        ToolbarGroupVariant["filter-group"] = "filter-group";
        ToolbarGroupVariant["icon-button-group"] = "icon-button-group";
        ToolbarGroupVariant["button-group"] = "button-group";
    })(exports.ToolbarGroupVariant || (exports.ToolbarGroupVariant = {}));
    class ToolbarGroupWithRef extends React.Component {
        render() {
            const _a = this.props, { visiblity, alignment, spacer, spaceItems, className, variant, children, innerRef } = _a, props = __rest(_a, ["visiblity", "alignment", "spacer", "spaceItems", "className", "variant", "children", "innerRef"]);
            return (React.createElement("div", Object.assign({ className: css(styles$L.toolbarGroup, variant && styles$L.modifiers[toCamel(variant)], formatBreakpointMods(visiblity, styles$L), formatBreakpointMods(alignment, styles$L), formatBreakpointMods(spacer, styles$L), formatBreakpointMods(spaceItems, styles$L), className) }, props, { ref: innerRef }), children));
        }
    }
    const ToolbarGroup = React.forwardRef((props, ref) => (React.createElement(ToolbarGroupWithRef, Object.assign({}, props, { innerRef: ref }))));

    class ToolbarChipGroupContent extends React.Component {
        render() {
            const _a = this.props, { className, isExpanded, chipGroupContentRef, clearAllFilters, showClearFiltersButton, clearFiltersButtonText, collapseListedFiltersBreakpoint, numberOfFilters } = _a, props = __rest(_a, ["className", "isExpanded", "chipGroupContentRef", "clearAllFilters", "showClearFiltersButton", "clearFiltersButtonText", "collapseListedFiltersBreakpoint", "numberOfFilters"]);
            const clearChipGroups = () => {
                clearAllFilters();
            };
            const collapseListedFilters = typeof window !== 'undefined' ? window.innerWidth < globalBreakpoints[collapseListedFiltersBreakpoint] : false;
            return (React.createElement("div", Object.assign({ className: css(styles$L.toolbarContent, (numberOfFilters === 0 || isExpanded) && styles$L.modifiers.hidden, className) }, ((numberOfFilters === 0 || isExpanded) && { hidden: true }), { ref: chipGroupContentRef }, props),
                React.createElement(ToolbarGroup, Object.assign({ className: css(collapseListedFilters && styles$L.modifiers.hidden) }, (collapseListedFilters && { hidden: true }), (collapseListedFilters && { 'aria-hidden': true }))),
                collapseListedFilters && numberOfFilters > 0 && !isExpanded && (React.createElement(ToolbarGroup, null,
                    React.createElement(ToolbarItem, null,
                        numberOfFilters,
                        " filters applied"))),
                showClearFiltersButton && !isExpanded && (React.createElement(ToolbarItem, null,
                    React.createElement(Button, { variant: "link", onClick: clearChipGroups, isInline: true }, clearFiltersButtonText)))));
        }
    }
    ToolbarChipGroupContent.displayName = 'ToolbarChipGroupContent';
    ToolbarChipGroupContent.defaultProps = {
        clearFiltersButtonText: 'Clear all filters',
        collapseListedFiltersBreakpoint: 'lg'
    };

    class Toolbar extends React.Component {
        constructor() {
            super(...arguments);
            this.chipGroupContentRef = React.createRef();
            this.staticFilterInfo = {};
            this.state = {
                isManagedToggleExpanded: false,
                filterInfo: {}
            };
            this.isToggleManaged = () => !(this.props.isExpanded || !!this.props.toggleIsExpanded);
            this.toggleIsExpanded = () => {
                this.setState(prevState => ({
                    isManagedToggleExpanded: !prevState.isManagedToggleExpanded
                }));
            };
            this.closeExpandableContent = () => {
                this.setState(() => ({
                    isManagedToggleExpanded: false
                }));
            };
            this.updateNumberFilters = (categoryName, numberOfFilters) => {
                const filterInfoToUpdate = Object.assign({}, this.staticFilterInfo);
                if (!filterInfoToUpdate.hasOwnProperty(categoryName) || filterInfoToUpdate[categoryName] !== numberOfFilters) {
                    filterInfoToUpdate[categoryName] = numberOfFilters;
                    this.staticFilterInfo = filterInfoToUpdate;
                    this.setState({ filterInfo: filterInfoToUpdate });
                }
            };
            this.getNumberOfFilters = () => Object.values(this.state.filterInfo).reduce((acc, cur) => acc + cur, 0);
            this.renderToolbar = (randomId) => {
                const _a = this.props, { clearAllFilters, clearFiltersButtonText, collapseListedFiltersBreakpoint, isExpanded: isExpandedProp, toggleIsExpanded, className, children } = _a, props = __rest(_a, ["clearAllFilters", "clearFiltersButtonText", "collapseListedFiltersBreakpoint", "isExpanded", "toggleIsExpanded", "className", "children"]);
                const { isManagedToggleExpanded } = this.state;
                const isToggleManaged = this.isToggleManaged();
                const isExpanded = isToggleManaged ? isManagedToggleExpanded : isExpandedProp;
                const numberOfFilters = this.getNumberOfFilters();
                const showClearFiltersButton = numberOfFilters > 0;
                return (React.createElement("div", Object.assign({ className: css(styles$L.toolbar, className), id: randomId }, props),
                    React.createElement(ToolbarContext.Provider, { value: {
                            isExpanded,
                            toggleIsExpanded: isToggleManaged ? this.toggleIsExpanded : toggleIsExpanded,
                            chipGroupContentRef: this.chipGroupContentRef,
                            updateNumberFilters: this.updateNumberFilters,
                            numberOfFilters,
                            clearAllFilters,
                            clearFiltersButtonText,
                            showClearFiltersButton,
                            toolbarId: randomId
                        } },
                        children,
                        React.createElement(ToolbarChipGroupContent, { isExpanded: isExpanded, chipGroupContentRef: this.chipGroupContentRef, clearAllFilters: clearAllFilters, showClearFiltersButton: showClearFiltersButton, clearFiltersButtonText: clearFiltersButtonText, numberOfFilters: numberOfFilters, collapseListedFiltersBreakpoint: collapseListedFiltersBreakpoint }))));
            };
        }
        componentDidMount() {
            if (this.isToggleManaged()) {
                window.addEventListener('resize', this.closeExpandableContent);
            }
        }
        componentWillUnmount() {
            if (this.isToggleManaged()) {
                window.removeEventListener('resize', this.closeExpandableContent);
            }
        }
        render() {
            return this.props.id ? (this.renderToolbar(this.props.id)) : (React.createElement(GenerateId, null, randomId => this.renderToolbar(randomId)));
        }
    }
    Toolbar.displayName = 'Toolbar';

    class ToolbarExpandableContent extends React.Component {
        render() {
            const _a = this.props, { className, expandableContentRef, chipContainerRef, 
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            isExpanded, clearAllFilters, clearFiltersButtonText, showClearFiltersButton } = _a, props = __rest(_a, ["className", "expandableContentRef", "chipContainerRef", "isExpanded", "clearAllFilters", "clearFiltersButtonText", "showClearFiltersButton"]);
            const { numberOfFilters } = this.context;
            const clearChipGroups = () => {
                clearAllFilters();
            };
            return (React.createElement("div", Object.assign({ className: css(styles$L.toolbarExpandableContent, className), ref: expandableContentRef }, props),
                React.createElement(ToolbarGroup, null),
                numberOfFilters > 0 && (React.createElement(ToolbarGroup, { className: styles$L.modifiers.chipContainer },
                    React.createElement(ToolbarGroup, { ref: chipContainerRef }),
                    showClearFiltersButton && (React.createElement(ToolbarItem, null,
                        React.createElement(Button, { variant: "link", onClick: clearChipGroups, isInline: true }, clearFiltersButtonText)))))));
        }
    }
    ToolbarExpandableContent.displayName = 'ToolbarExpandableContent';
    ToolbarExpandableContent.contextType = ToolbarContext;
    ToolbarExpandableContent.defaultProps = {
        isExpanded: false,
        clearFiltersButtonText: 'Clear all filters'
    };

    class ToolbarContent extends React.Component {
        constructor() {
            super(...arguments);
            this.expandableContentRef = React.createRef();
            this.chipContainerRef = React.createRef();
        }
        render() {
            const _a = this.props, { className, children, isExpanded, toolbarId, visiblity, alignment, clearAllFilters, showClearFiltersButton, clearFiltersButtonText } = _a, props = __rest(_a, ["className", "children", "isExpanded", "toolbarId", "visiblity", "alignment", "clearAllFilters", "showClearFiltersButton", "clearFiltersButtonText"]);
            return (React.createElement("div", Object.assign({ className: css(styles$L.toolbarContent, formatBreakpointMods(visiblity, styles$L), formatBreakpointMods(alignment, styles$L), className) }, props),
                React.createElement(ToolbarContext.Consumer, null, ({ clearAllFilters: clearAllFiltersContext, clearFiltersButtonText: clearFiltersButtonContext, showClearFiltersButton: showClearFiltersButtonContext, toolbarId: toolbarIdContext }) => {
                    const expandableContentId = `${toolbarId ||
                    toolbarIdContext}-expandable-content-${ToolbarContent.currentId++}`;
                    return (React.createElement(ToolbarContentContext.Provider, { value: {
                            expandableContentRef: this.expandableContentRef,
                            expandableContentId,
                            chipContainerRef: this.chipContainerRef
                        } },
                        React.createElement("div", { className: css(styles$L.toolbarContentSection) }, children),
                        React.createElement(ToolbarExpandableContent, { id: expandableContentId, isExpanded: isExpanded, expandableContentRef: this.expandableContentRef, chipContainerRef: this.chipContainerRef, clearAllFilters: clearAllFilters || clearAllFiltersContext, showClearFiltersButton: showClearFiltersButton || showClearFiltersButtonContext, clearFiltersButtonText: clearFiltersButtonText || clearFiltersButtonContext })));
                })));
        }
    }
    ToolbarContent.displayName = 'ToolbarContent';
    ToolbarContent.currentId = 0;
    ToolbarContent.defaultProps = {
        isExpanded: false,
        showClearFiltersButton: false
    };

    class ToolbarFilter extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                isMounted: false
            };
        }
        componentDidMount() {
            const { categoryName, chips } = this.props;
            this.context.updateNumberFilters(typeof categoryName === 'string' ? categoryName : categoryName.name, chips.length);
            this.setState({ isMounted: true });
        }
        componentDidUpdate() {
            const { categoryName, chips } = this.props;
            this.context.updateNumberFilters(typeof categoryName === 'string' ? categoryName : categoryName.name, chips.length);
        }
        render() {
            const _a = this.props, { children, chips, deleteChipGroup, deleteChip, categoryName, showToolbarItem } = _a, props = __rest(_a, ["children", "chips", "deleteChipGroup", "deleteChip", "categoryName", "showToolbarItem"]);
            const { isExpanded, chipGroupContentRef } = this.context;
            const chipGroup = chips.length ? (React.createElement(ToolbarItem, { variant: "chip-group" },
                React.createElement(ChipGroup, { key: typeof categoryName === 'string' ? categoryName : categoryName.key, categoryName: typeof categoryName === 'string' ? categoryName : categoryName.name, isClosable: deleteChipGroup !== undefined, onClick: () => deleteChipGroup(categoryName) }, chips.map(chip => typeof chip === 'string' ? (React.createElement(Chip, { key: chip, onClick: () => deleteChip(categoryName, chip) }, chip)) : (React.createElement(Chip, { key: chip.key, onClick: () => deleteChip(categoryName, chip) }, chip.node)))))) : null;
            if (!isExpanded && this.state.isMounted) {
                return (React.createElement(React.Fragment, null,
                    showToolbarItem && React.createElement(ToolbarItem, Object.assign({}, props), children),
                    ReactDOM.createPortal(chipGroup, chipGroupContentRef.current.firstElementChild)));
            }
            return (React.createElement(ToolbarContentContext.Consumer, null, ({ chipContainerRef }) => (React.createElement(React.Fragment, null,
                showToolbarItem && React.createElement(ToolbarItem, Object.assign({}, props), children),
                chipContainerRef.current && ReactDOM.createPortal(chipGroup, chipContainerRef.current)))));
        }
    }
    ToolbarFilter.displayName = 'ToolbarFilter';
    ToolbarFilter.contextType = ToolbarContext;
    ToolbarFilter.defaultProps = {
        chips: [],
        showToolbarItem: true
    };

    class ToolbarToggleGroup extends React.Component {
        constructor() {
            super(...arguments);
            this.isContentPopup = () => {
                const viewportSize = window.innerWidth;
                const lgBreakpointValue = parseInt(globalBreakpointLg.value);
                return viewportSize < lgBreakpointValue;
            };
        }
        render() {
            const _a = this.props, { toggleIcon, variant, visiblity, breakpoint, alignment, spacer, spaceItems, className, children } = _a, props = __rest(_a, ["toggleIcon", "variant", "visiblity", "breakpoint", "alignment", "spacer", "spaceItems", "className", "children"]);
            if (!breakpoint && !toggleIcon) {
                // eslint-disable-next-line no-console
                console.error('ToolbarToggleGroup will not be visible without a breakpoint or toggleIcon.');
            }
            return (React.createElement(ToolbarContext.Consumer, null, ({ isExpanded, toggleIsExpanded }) => (React.createElement(ToolbarContentContext.Consumer, null, ({ expandableContentRef, expandableContentId }) => {
                if (expandableContentRef.current && expandableContentRef.current.classList) {
                    if (isExpanded) {
                        expandableContentRef.current.classList.add(styles$L.modifiers.expanded);
                    }
                    else {
                        expandableContentRef.current.classList.remove(styles$L.modifiers.expanded);
                    }
                }
                return (React.createElement("div", Object.assign({ className: css(styles$L.toolbarGroup, styles$L.modifiers.toggleGroup, variant && styles$L.modifiers[toCamel(variant)], breakpoint &&
                        styles$L.modifiers[`showOn${capitalize(breakpoint.replace('2xl', '_2xl'))}`], formatBreakpointMods(visiblity, styles$L), formatBreakpointMods(alignment, styles$L), formatBreakpointMods(spacer, styles$L), formatBreakpointMods(spaceItems, styles$L), className) }, props),
                    React.createElement("div", { className: css(styles$L.toolbarToggle) },
                        React.createElement(Button, Object.assign({ variant: "plain", onClick: toggleIsExpanded, "aria-label": "Show Filters" }, (isExpanded && { 'aria-expanded': true }), { "aria-haspopup": isExpanded && this.isContentPopup(), "aria-controls": expandableContentId }), toggleIcon)),
                    isExpanded
                        ? ReactDOM.createPortal(children, expandableContentRef.current.firstElementChild)
                        : children));
            }))));
        }
    }
    ToolbarToggleGroup.displayName = 'ToolbarToggleGroup';

    var wizard = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "button": "pf-c-button",
      "card": "pf-c-card",
      "modalBox": "pf-c-modal-box",
      "modifiers": {
        "finished": "pf-m-finished",
        "expanded": "pf-m-expanded",
        "current": "pf-m-current",
        "disabled": "pf-m-disabled",
        "noPadding": "pf-m-no-padding"
      },
      "wizard": "pf-c-wizard",
      "wizardClose": "pf-c-wizard__close",
      "wizardDescription": "pf-c-wizard__description",
      "wizardFooter": "pf-c-wizard__footer",
      "wizardHeader": "pf-c-wizard__header",
      "wizardInnerWrap": "pf-c-wizard__inner-wrap",
      "wizardMain": "pf-c-wizard__main",
      "wizardMainBody": "pf-c-wizard__main-body",
      "wizardNav": "pf-c-wizard__nav",
      "wizardNavItem": "pf-c-wizard__nav-item",
      "wizardNavLink": "pf-c-wizard__nav-link",
      "wizardNavList": "pf-c-wizard__nav-list",
      "wizardOuterWrap": "pf-c-wizard__outer-wrap",
      "wizardTitle": "pf-c-wizard__title",
      "wizardToggle": "pf-c-wizard__toggle",
      "wizardToggleIcon": "pf-c-wizard__toggle-icon",
      "wizardToggleList": "pf-c-wizard__toggle-list",
      "wizardToggleListItem": "pf-c-wizard__toggle-list-item",
      "wizardToggleNum": "pf-c-wizard__toggle-num",
      "wizardToggleSeparator": "pf-c-wizard__toggle-separator"
    };
    });

    var styles$M = unwrapExports(wizard);

    const WizardFooterInternal = ({ onNext, onBack, onClose, isValid, firstStep, activeStep, nextButtonText, backButtonText, cancelButtonText }) => (React.createElement("footer", { className: css(styles$M.wizardFooter) },
        React.createElement(Button, { variant: exports.ButtonVariant.primary, type: "submit", onClick: onNext, isDisabled: !isValid }, nextButtonText),
        !activeStep.hideBackButton && (React.createElement(Button, { variant: exports.ButtonVariant.secondary, onClick: onBack, className: css(firstStep && 'pf-m-disabled') }, backButtonText)),
        !activeStep.hideCancelButton && (React.createElement(Button, { variant: exports.ButtonVariant.link, onClick: onClose }, cancelButtonText))));
    WizardFooterInternal.displayName = 'WizardFooterInternal';

    const WizardBody = ({ children, hasNoBodyPadding = false }) => (React.createElement("main", { className: css(styles$M.wizardMain) },
        React.createElement("div", { className: css(styles$M.wizardMainBody, hasNoBodyPadding && styles$M.modifiers.noPadding) }, children)));
    WizardBody.displayName = 'WizardBody';

    const WizardToggle = ({ isNavOpen, onNavToggle, nav, steps, activeStep, children, hasNoBodyPadding = false, 'aria-label': ariaLabel = 'Wizard Toggle' }) => {
        let activeStepIndex;
        let activeStepName;
        let activeStepSubName;
        for (let i = 0; i < steps.length; i++) {
            if ((activeStep.id && steps[i].id === activeStep.id) || steps[i].name === activeStep.name) {
                activeStepIndex = i + 1;
                activeStepName = steps[i].name;
                break;
            }
            else if (steps[i].steps) {
                for (const step of steps[i].steps) {
                    if ((activeStep.id && step.id === activeStep.id) || step.name === activeStep.name) {
                        activeStepIndex = i + 1;
                        activeStepName = steps[i].name;
                        activeStepSubName = step.name;
                        break;
                    }
                }
            }
        }
        return (React.createElement(React.Fragment, null,
            React.createElement("button", { onClick: () => onNavToggle(!isNavOpen), className: css(styles$M.wizardToggle, isNavOpen && 'pf-m-expanded'), "aria-label": ariaLabel, "aria-expanded": isNavOpen },
                React.createElement("ol", { className: css(styles$M.wizardToggleList) },
                    React.createElement("li", { className: css(styles$M.wizardToggleListItem) },
                        React.createElement("span", { className: css(styles$M.wizardToggleNum) }, activeStepIndex),
                        " ",
                        activeStepName,
                        activeStepSubName && React.createElement(AngleRightIcon, { className: css(styles$M.wizardToggleSeparator), "aria-hidden": "true" })),
                    activeStepSubName && React.createElement("li", { className: css(styles$M.wizardToggleListItem) }, activeStepSubName)),
                React.createElement("span", { className: css(styles$M.wizardToggleIcon) },
                    React.createElement(CaretDownIcon, { "aria-hidden": "true" }))),
            React.createElement("div", { className: css(styles$M.wizardOuterWrap) },
                React.createElement("div", { className: css(styles$M.wizardInnerWrap) },
                    nav(isNavOpen),
                    React.createElement(WizardBody, { hasNoBodyPadding: hasNoBodyPadding }, activeStep.component)),
                children)));
    };
    WizardToggle.displayName = 'WizardToggle';

    const WizardNav = ({ children, 'aria-label': ariaLabel, isOpen = false, returnList = false }) => {
        const innerList = React.createElement("ol", { className: css(styles$M.wizardNavList) }, children);
        if (returnList) {
            return innerList;
        }
        return (React.createElement("nav", { className: css(styles$M.wizardNav, isOpen && 'pf-m-expanded'), "aria-label": ariaLabel },
            React.createElement("ol", { className: css(styles$M.wizardNavList) }, children)));
    };
    WizardNav.displayName = 'WizardNav';

    const WizardNavItem = ({ children = null, content = '', isCurrent = false, isDisabled = false, step, onNavItemClick = () => undefined, navItemComponent = 'a' }) => {
        const NavItemComponent = navItemComponent;
        return (React.createElement("li", { className: css(styles$M.wizardNavItem) },
            React.createElement(NavItemComponent, { "aria-current": isCurrent && !children ? 'page' : false, onClick: () => onNavItemClick(step), className: css(styles$M.wizardNavLink, isCurrent && 'pf-m-current', isDisabled && 'pf-m-disabled'), "aria-disabled": isDisabled ? true : false, tabIndex: isDisabled ? -1 : undefined }, content),
            children));
    };
    WizardNavItem.displayName = 'WizardNavItem';

    const WizardContext = React.createContext({
        goToStepById: () => null,
        goToStepByName: () => null,
        onNext: () => null,
        onBack: () => null,
        onClose: () => null,
        activeStep: { name: null }
    });
    const WizardContextProvider = WizardContext.Provider;
    const WizardContextConsumer = WizardContext.Consumer;

    const WizardHeader = ({ onClose = () => undefined, title, description, hideClose, closeButtonAriaLabel, titleId, descriptionId }) => (React.createElement("div", { className: css(styles$M.wizardHeader) },
        !hideClose && (React.createElement(Button, { variant: "plain", className: css(styles$M.wizardClose), "aria-label": closeButtonAriaLabel, onClick: onClose },
            React.createElement(TimesIcon, { "aria-hidden": "true" }))),
        React.createElement(Title, { headingLevel: "h2", size: "3xl", className: css(styles$M.wizardTitle), "aria-label": title, id: titleId }, title || React.createElement(React.Fragment, null, "\u00A0")),
        description && (React.createElement("p", { className: css(styles$M.wizardDescription), id: descriptionId }, description))));
    WizardHeader.displayName = 'WizardHeader';

    class Wizard extends React.Component {
        constructor(props) {
            super(props);
            this.handleKeyClicks = (event) => {
                if (event.keyCode === KEY_CODES.ESCAPE_KEY) {
                    if (this.state.isNavOpen) {
                        this.setState({ isNavOpen: !this.state.isNavOpen });
                    }
                    else if (this.props.isOpen) {
                        this.props.onClose();
                    }
                }
            };
            this.onNext = () => {
                const { onNext, onClose, onSave } = this.props;
                const { currentStep } = this.state;
                const flattenedSteps = this.getFlattenedSteps();
                const maxSteps = flattenedSteps.length;
                if (currentStep >= maxSteps) {
                    // Hit the save button at the end of the wizard
                    if (onSave) {
                        return onSave();
                    }
                    return onClose();
                }
                else {
                    const newStep = currentStep + 1;
                    this.setState({
                        currentStep: newStep
                    });
                    const { id: prevId, name: prevName } = flattenedSteps[currentStep - 1];
                    const { id, name } = flattenedSteps[newStep - 1];
                    return onNext && onNext({ id, name }, { prevId, prevName });
                }
            };
            this.onBack = () => {
                const { onBack } = this.props;
                const { currentStep } = this.state;
                const flattenedSteps = this.getFlattenedSteps();
                if (flattenedSteps.length < currentStep) {
                    // Previous step was removed, just update the currentStep state
                    const adjustedStep = flattenedSteps.length;
                    this.setState({
                        currentStep: adjustedStep
                    });
                }
                else {
                    const newStep = currentStep - 1 <= 0 ? 0 : currentStep - 1;
                    this.setState({
                        currentStep: newStep
                    });
                    const { id: prevId, name: prevName } = flattenedSteps[newStep];
                    const { id, name } = flattenedSteps[newStep - 1];
                    return onBack && onBack({ id, name }, { prevId, prevName });
                }
            };
            this.goToStep = (step) => {
                const { onGoToStep } = this.props;
                const { currentStep } = this.state;
                const flattenedSteps = this.getFlattenedSteps();
                const maxSteps = flattenedSteps.length;
                if (step < 1) {
                    step = 1;
                }
                else if (step > maxSteps) {
                    step = maxSteps;
                }
                this.setState({ currentStep: step, isNavOpen: false });
                const { id: prevId, name: prevName } = flattenedSteps[currentStep - 1];
                const { id, name } = flattenedSteps[step - 1];
                return onGoToStep && onGoToStep({ id, name }, { prevId, prevName });
            };
            this.goToStepById = (stepId) => {
                const flattenedSteps = this.getFlattenedSteps();
                let step;
                for (let i = 0; i < flattenedSteps.length; i++) {
                    if (flattenedSteps[i].id === stepId) {
                        step = i + 1;
                        break;
                    }
                }
                if (step) {
                    this.setState({ currentStep: step });
                }
            };
            this.goToStepByName = (stepName) => {
                const flattenedSteps = this.getFlattenedSteps();
                let step;
                for (let i = 0; i < flattenedSteps.length; i++) {
                    if (flattenedSteps[i].name === stepName) {
                        step = i + 1;
                        break;
                    }
                }
                if (step) {
                    this.setState({ currentStep: step });
                }
            };
            this.getFlattenedSteps = () => {
                const { steps } = this.props;
                const flattenedSteps = [];
                for (const step of steps) {
                    if (step.steps) {
                        for (const childStep of step.steps) {
                            flattenedSteps.push(childStep);
                        }
                    }
                    else {
                        flattenedSteps.push(step);
                    }
                }
                return flattenedSteps;
            };
            this.getFlattenedStepsIndex = (flattenedSteps, stepName) => {
                for (let i = 0; i < flattenedSteps.length; i++) {
                    if (flattenedSteps[i].name === stepName) {
                        return i + 1;
                    }
                }
                return 0;
            };
            this.initSteps = (steps) => {
                // Set default Step values
                for (let i = 0; i < steps.length; i++) {
                    if (steps[i].steps) {
                        for (let j = 0; j < steps[i].steps.length; j++) {
                            steps[i].steps[j] = Object.assign({ canJumpTo: true }, steps[i].steps[j]);
                        }
                    }
                    steps[i] = Object.assign({ canJumpTo: true }, steps[i]);
                }
                return steps;
            };
            this.getElement = (appendTo) => {
                if (typeof appendTo === 'function') {
                    return appendTo();
                }
                return appendTo || document.body;
            };
            const newId = Wizard.currentId++;
            this.titleId = props.titleId || `pf-wizard-title-${newId}`;
            this.descriptionId = props.descriptionId || `pf-wizard-description-${newId}`;
            this.state = {
                currentStep: this.props.startAtStep && Number.isInteger(this.props.startAtStep) ? this.props.startAtStep : 1,
                isNavOpen: false
            };
        }
        componentDidMount() {
            const target = typeof document !== 'undefined' ? document.body : null;
            if (target) {
                target.addEventListener('keydown', this.handleKeyClicks, false);
            }
        }
        componentWillUnmount() {
            const target = (typeof document !== 'undefined' && document.body) || null;
            if (target) {
                target.removeEventListener('keydown', this.handleKeyClicks, false);
            }
        }
        render() {
            const _a = this.props, { 
            /* eslint-disable @typescript-eslint/no-unused-vars */
            width, height, title, description, onClose, onSave, onBack, onNext, onGoToStep, className, steps, startAtStep, nextButtonText = 'Next', backButtonText = 'Back', cancelButtonText = 'Cancel', hideClose, closeButtonAriaLabel = 'Close', navAriaLabel, hasNoBodyPadding, footer, appendTo, isOpen, titleId, descriptionId } = _a, rest = __rest(_a, ["width", "height", "title", "description", "onClose", "onSave", "onBack", "onNext", "onGoToStep", "className", "steps", "startAtStep", "nextButtonText", "backButtonText", "cancelButtonText", "hideClose", "closeButtonAriaLabel", "navAriaLabel", "hasNoBodyPadding", "footer", "appendTo", "isOpen", "titleId", "descriptionId"])
            /* eslint-enable @typescript-eslint/no-unused-vars */
            ;
            const { currentStep } = this.state;
            const flattenedSteps = this.getFlattenedSteps();
            const adjustedStep = flattenedSteps.length < currentStep ? flattenedSteps.length : currentStep;
            const activeStep = flattenedSteps[adjustedStep - 1];
            const computedSteps = this.initSteps(steps);
            const firstStep = activeStep === flattenedSteps[0];
            const isValid = activeStep && activeStep.enableNext !== undefined ? activeStep.enableNext : true;
            const nav = (isWizardNavOpen) => (React.createElement(WizardNav, { isOpen: isWizardNavOpen, "aria-label": navAriaLabel }, computedSteps.map((step, index) => {
                if (step.isFinishedStep) {
                    // Don't show finished step in the side nav
                    return;
                }
                let enabled;
                let navItemStep;
                if (step.steps) {
                    let hasActiveChild = false;
                    let canJumpToParent = false;
                    for (const subStep of step.steps) {
                        if (activeStep.name === subStep.name) {
                            // one of the children matches
                            hasActiveChild = true;
                        }
                        if (subStep.canJumpTo) {
                            canJumpToParent = true;
                        }
                    }
                    navItemStep = this.getFlattenedStepsIndex(flattenedSteps, step.steps[0].name);
                    return (React.createElement(WizardNavItem, { key: index, content: step.name, isCurrent: hasActiveChild, isDisabled: !canJumpToParent, step: navItemStep, onNavItemClick: this.goToStep },
                        React.createElement(WizardNav, { returnList: true }, step.steps.map((childStep, indexChild) => {
                            if (childStep.isFinishedStep) {
                                // Don't show finished step in the side nav
                                return;
                            }
                            navItemStep = this.getFlattenedStepsIndex(flattenedSteps, childStep.name);
                            enabled = childStep.canJumpTo;
                            return (React.createElement(WizardNavItem, { key: `child_${indexChild}`, content: childStep.name, isCurrent: activeStep.name === childStep.name, isDisabled: !enabled, step: navItemStep, onNavItemClick: this.goToStep }));
                        }))));
                }
                navItemStep = this.getFlattenedStepsIndex(flattenedSteps, step.name);
                enabled = step.canJumpTo;
                return (React.createElement(WizardNavItem, { key: index, content: step.name, isCurrent: activeStep.name === step.name, isDisabled: !enabled, step: navItemStep, onNavItemClick: this.goToStep }));
            })));
            const context = {
                goToStepById: this.goToStepById,
                goToStepByName: this.goToStepByName,
                onNext: this.onNext,
                onBack: this.onBack,
                onClose,
                activeStep
            };
            const wizard = (React.createElement(WizardContextProvider, { value: context },
                React.createElement("div", Object.assign({}, rest, { className: css(styles$M.wizard, activeStep && activeStep.isFinishedStep && 'pf-m-finished', className) }, (height && { style: { height } })),
                    title && (React.createElement(WizardHeader, { titleId: this.titleId, descriptionId: this.descriptionId, onClose: onClose, title: title, description: description, closeButtonAriaLabel: closeButtonAriaLabel, hideClose: hideClose })),
                    React.createElement(WizardToggle, { isNavOpen: this.state.isNavOpen, onNavToggle: isNavOpen => this.setState({ isNavOpen }), nav: nav, steps: steps, activeStep: activeStep, hasNoBodyPadding: hasNoBodyPadding }, footer || (React.createElement(WizardFooterInternal, { onNext: this.onNext, onBack: this.onBack, onClose: onClose, isValid: isValid, firstStep: firstStep, activeStep: activeStep, nextButtonText: (activeStep && activeStep.nextButtonText) || nextButtonText, backButtonText: backButtonText, cancelButtonText: cancelButtonText }))))));
            if (isOpen !== undefined) {
                return (React.createElement(Modal, { isOpen: isOpen, variant: exports.ModalVariant.large, "aria-labelledby": this.titleId, "aria-describedby": this.descriptionId, showClose: false, hasNoBodyWrapper: true }, wizard));
            }
            return wizard;
        }
    }
    Wizard.displayName = 'Wizard';
    Wizard.currentId = 0;
    Wizard.defaultProps = {
        title: '',
        description: '',
        className: '',
        startAtStep: 1,
        nextButtonText: 'Next',
        backButtonText: 'Back',
        cancelButtonText: 'Cancel',
        hideClose: false,
        closeButtonAriaLabel: 'Close',
        navAriaLabel: 'Steps',
        hasNoBodyPadding: false,
        onBack: null,
        onNext: null,
        onGoToStep: null,
        width: null,
        height: null,
        footer: null,
        onClose: () => undefined,
        appendTo: null,
        isOpen: undefined
    };

    const WizardFooter = ({ children }) => (React.createElement("footer", { className: css(styles$M.wizardFooter) }, children));
    WizardFooter.displayName = 'WizardFooter';

    const Bullseye = (_a) => {
        var { children = null, className = '', component = 'div' } = _a, props = __rest(_a, ["children", "className", "component"]);
        const Component = component;
        return (React.createElement(Component, Object.assign({ className: css(styles$1.bullseye, className) }, props), children));
    };
    Bullseye.displayName = 'Bullseye';

    var flex = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "flex": "pf-l-flex",
      "modifiers": {
        "flex": "pf-m-flex",
        "inlineFlex": "pf-m-inline-flex",
        "column": "pf-m-column",
        "columnReverse": "pf-m-column-reverse",
        "row": "pf-m-row",
        "rowReverse": "pf-m-row-reverse",
        "wrap": "pf-m-wrap",
        "wrapReverse": "pf-m-wrap-reverse",
        "nowrap": "pf-m-nowrap",
        "justifyContentFlexStart": "pf-m-justify-content-flex-start",
        "justifyContentFlexEnd": "pf-m-justify-content-flex-end",
        "justifyContentCenter": "pf-m-justify-content-center",
        "justifyContentSpaceBetween": "pf-m-justify-content-space-between",
        "justifyContentSpaceAround": "pf-m-justify-content-space-around",
        "justifyContentSpaceEvenly": "pf-m-justify-content-space-evenly",
        "alignItemsFlexStart": "pf-m-align-items-flex-start",
        "alignItemsFlexEnd": "pf-m-align-items-flex-end",
        "alignItemsCenter": "pf-m-align-items-center",
        "alignItemsStretch": "pf-m-align-items-stretch",
        "alignItemsBaseline": "pf-m-align-items-baseline",
        "alignContentFlexStart": "pf-m-align-content-flex-start",
        "alignContentFlexEnd": "pf-m-align-content-flex-end",
        "alignContentCenter": "pf-m-align-content-center",
        "alignContentStretch": "pf-m-align-content-stretch",
        "alignContentSpaceBetween": "pf-m-align-content-space-between",
        "alignContentSpaceAround": "pf-m-align-content-space-around",
        "alignRight": "pf-m-align-right",
        "alignLeft": "pf-m-align-left",
        "grow": "pf-m-grow",
        "shrink": "pf-m-shrink",
        "fullWidth": "pf-m-full-width",
        "flex_1": "pf-m-flex-1",
        "flex_2": "pf-m-flex-2",
        "flex_3": "pf-m-flex-3",
        "flex_4": "pf-m-flex-4",
        "flexDefault": "pf-m-flex-default",
        "flexNone": "pf-m-flex-none",
        "alignSelfFlexStart": "pf-m-align-self-flex-start",
        "alignSelfFlexEnd": "pf-m-align-self-flex-end",
        "alignSelfCenter": "pf-m-align-self-center",
        "alignSelfBaseline": "pf-m-align-self-baseline",
        "alignSelfStretch": "pf-m-align-self-stretch",
        "flexOnSm": "pf-m-flex-on-sm",
        "inlineFlexOnSm": "pf-m-inline-flex-on-sm",
        "columnOnSm": "pf-m-column-on-sm",
        "columnReverseOnSm": "pf-m-column-reverse-on-sm",
        "rowOnSm": "pf-m-row-on-sm",
        "rowReverseOnSm": "pf-m-row-reverse-on-sm",
        "wrapOnSm": "pf-m-wrap-on-sm",
        "wrapReverseOnSm": "pf-m-wrap-reverse-on-sm",
        "nowrapOnSm": "pf-m-nowrap-on-sm",
        "justifyContentFlexStartOnSm": "pf-m-justify-content-flex-start-on-sm",
        "justifyContentFlexEndOnSm": "pf-m-justify-content-flex-end-on-sm",
        "justifyContentCenterOnSm": "pf-m-justify-content-center-on-sm",
        "justifyContentSpaceBetweenOnSm": "pf-m-justify-content-space-between-on-sm",
        "justifyContentSpaceAroundOnSm": "pf-m-justify-content-space-around-on-sm",
        "justifyContentSpaceEvenlyOnSm": "pf-m-justify-content-space-evenly-on-sm",
        "alignItemsFlexStartOnSm": "pf-m-align-items-flex-start-on-sm",
        "alignItemsFlexEndOnSm": "pf-m-align-items-flex-end-on-sm",
        "alignItemsCenterOnSm": "pf-m-align-items-center-on-sm",
        "alignItemsStretchOnSm": "pf-m-align-items-stretch-on-sm",
        "alignItemsBaselineOnSm": "pf-m-align-items-baseline-on-sm",
        "alignContentFlexStartOnSm": "pf-m-align-content-flex-start-on-sm",
        "alignContentFlexEndOnSm": "pf-m-align-content-flex-end-on-sm",
        "alignContentCenterOnSm": "pf-m-align-content-center-on-sm",
        "alignContentStretchOnSm": "pf-m-align-content-stretch-on-sm",
        "alignContentSpaceBetweenOnSm": "pf-m-align-content-space-between-on-sm",
        "alignContentSpaceAroundOnSm": "pf-m-align-content-space-around-on-sm",
        "alignRightOnSm": "pf-m-align-right-on-sm",
        "alignLeftOnSm": "pf-m-align-left-on-sm",
        "growOnSm": "pf-m-grow-on-sm",
        "shrinkOnSm": "pf-m-shrink-on-sm",
        "fullWidthOnSm": "pf-m-full-width-on-sm",
        "flex_1OnSm": "pf-m-flex-1-on-sm",
        "flex_2OnSm": "pf-m-flex-2-on-sm",
        "flex_3OnSm": "pf-m-flex-3-on-sm",
        "flex_4OnSm": "pf-m-flex-4-on-sm",
        "flexDefaultOnSm": "pf-m-flex-default-on-sm",
        "flexNoneOnSm": "pf-m-flex-none-on-sm",
        "alignSelfFlexStartOnSm": "pf-m-align-self-flex-start-on-sm",
        "alignSelfFlexEndOnSm": "pf-m-align-self-flex-end-on-sm",
        "alignSelfCenterOnSm": "pf-m-align-self-center-on-sm",
        "alignSelfBaselineOnSm": "pf-m-align-self-baseline-on-sm",
        "alignSelfStretchOnSm": "pf-m-align-self-stretch-on-sm",
        "flexOnMd": "pf-m-flex-on-md",
        "inlineFlexOnMd": "pf-m-inline-flex-on-md",
        "columnOnMd": "pf-m-column-on-md",
        "columnReverseOnMd": "pf-m-column-reverse-on-md",
        "rowOnMd": "pf-m-row-on-md",
        "rowReverseOnMd": "pf-m-row-reverse-on-md",
        "wrapOnMd": "pf-m-wrap-on-md",
        "wrapReverseOnMd": "pf-m-wrap-reverse-on-md",
        "nowrapOnMd": "pf-m-nowrap-on-md",
        "justifyContentFlexStartOnMd": "pf-m-justify-content-flex-start-on-md",
        "justifyContentFlexEndOnMd": "pf-m-justify-content-flex-end-on-md",
        "justifyContentCenterOnMd": "pf-m-justify-content-center-on-md",
        "justifyContentSpaceBetweenOnMd": "pf-m-justify-content-space-between-on-md",
        "justifyContentSpaceAroundOnMd": "pf-m-justify-content-space-around-on-md",
        "justifyContentSpaceEvenlyOnMd": "pf-m-justify-content-space-evenly-on-md",
        "alignItemsFlexStartOnMd": "pf-m-align-items-flex-start-on-md",
        "alignItemsFlexEndOnMd": "pf-m-align-items-flex-end-on-md",
        "alignItemsCenterOnMd": "pf-m-align-items-center-on-md",
        "alignItemsStretchOnMd": "pf-m-align-items-stretch-on-md",
        "alignItemsBaselineOnMd": "pf-m-align-items-baseline-on-md",
        "alignContentFlexStartOnMd": "pf-m-align-content-flex-start-on-md",
        "alignContentFlexEndOnMd": "pf-m-align-content-flex-end-on-md",
        "alignContentCenterOnMd": "pf-m-align-content-center-on-md",
        "alignContentStretchOnMd": "pf-m-align-content-stretch-on-md",
        "alignContentSpaceBetweenOnMd": "pf-m-align-content-space-between-on-md",
        "alignContentSpaceAroundOnMd": "pf-m-align-content-space-around-on-md",
        "alignRightOnMd": "pf-m-align-right-on-md",
        "alignLeftOnMd": "pf-m-align-left-on-md",
        "growOnMd": "pf-m-grow-on-md",
        "shrinkOnMd": "pf-m-shrink-on-md",
        "fullWidthOnMd": "pf-m-full-width-on-md",
        "flex_1OnMd": "pf-m-flex-1-on-md",
        "flex_2OnMd": "pf-m-flex-2-on-md",
        "flex_3OnMd": "pf-m-flex-3-on-md",
        "flex_4OnMd": "pf-m-flex-4-on-md",
        "flexDefaultOnMd": "pf-m-flex-default-on-md",
        "flexNoneOnMd": "pf-m-flex-none-on-md",
        "alignSelfFlexStartOnMd": "pf-m-align-self-flex-start-on-md",
        "alignSelfFlexEndOnMd": "pf-m-align-self-flex-end-on-md",
        "alignSelfCenterOnMd": "pf-m-align-self-center-on-md",
        "alignSelfBaselineOnMd": "pf-m-align-self-baseline-on-md",
        "alignSelfStretchOnMd": "pf-m-align-self-stretch-on-md",
        "flexOnLg": "pf-m-flex-on-lg",
        "inlineFlexOnLg": "pf-m-inline-flex-on-lg",
        "columnOnLg": "pf-m-column-on-lg",
        "columnReverseOnLg": "pf-m-column-reverse-on-lg",
        "rowOnLg": "pf-m-row-on-lg",
        "rowReverseOnLg": "pf-m-row-reverse-on-lg",
        "wrapOnLg": "pf-m-wrap-on-lg",
        "wrapReverseOnLg": "pf-m-wrap-reverse-on-lg",
        "nowrapOnLg": "pf-m-nowrap-on-lg",
        "justifyContentFlexStartOnLg": "pf-m-justify-content-flex-start-on-lg",
        "justifyContentFlexEndOnLg": "pf-m-justify-content-flex-end-on-lg",
        "justifyContentCenterOnLg": "pf-m-justify-content-center-on-lg",
        "justifyContentSpaceBetweenOnLg": "pf-m-justify-content-space-between-on-lg",
        "justifyContentSpaceAroundOnLg": "pf-m-justify-content-space-around-on-lg",
        "justifyContentSpaceEvenlyOnLg": "pf-m-justify-content-space-evenly-on-lg",
        "alignItemsFlexStartOnLg": "pf-m-align-items-flex-start-on-lg",
        "alignItemsFlexEndOnLg": "pf-m-align-items-flex-end-on-lg",
        "alignItemsCenterOnLg": "pf-m-align-items-center-on-lg",
        "alignItemsStretchOnLg": "pf-m-align-items-stretch-on-lg",
        "alignItemsBaselineOnLg": "pf-m-align-items-baseline-on-lg",
        "alignContentFlexStartOnLg": "pf-m-align-content-flex-start-on-lg",
        "alignContentFlexEndOnLg": "pf-m-align-content-flex-end-on-lg",
        "alignContentCenterOnLg": "pf-m-align-content-center-on-lg",
        "alignContentStretchOnLg": "pf-m-align-content-stretch-on-lg",
        "alignContentSpaceBetweenOnLg": "pf-m-align-content-space-between-on-lg",
        "alignContentSpaceAroundOnLg": "pf-m-align-content-space-around-on-lg",
        "alignRightOnLg": "pf-m-align-right-on-lg",
        "alignLeftOnLg": "pf-m-align-left-on-lg",
        "growOnLg": "pf-m-grow-on-lg",
        "shrinkOnLg": "pf-m-shrink-on-lg",
        "fullWidthOnLg": "pf-m-full-width-on-lg",
        "flex_1OnLg": "pf-m-flex-1-on-lg",
        "flex_2OnLg": "pf-m-flex-2-on-lg",
        "flex_3OnLg": "pf-m-flex-3-on-lg",
        "flex_4OnLg": "pf-m-flex-4-on-lg",
        "flexDefaultOnLg": "pf-m-flex-default-on-lg",
        "flexNoneOnLg": "pf-m-flex-none-on-lg",
        "alignSelfFlexStartOnLg": "pf-m-align-self-flex-start-on-lg",
        "alignSelfFlexEndOnLg": "pf-m-align-self-flex-end-on-lg",
        "alignSelfCenterOnLg": "pf-m-align-self-center-on-lg",
        "alignSelfBaselineOnLg": "pf-m-align-self-baseline-on-lg",
        "alignSelfStretchOnLg": "pf-m-align-self-stretch-on-lg",
        "flexOnXl": "pf-m-flex-on-xl",
        "inlineFlexOnXl": "pf-m-inline-flex-on-xl",
        "columnOnXl": "pf-m-column-on-xl",
        "columnReverseOnXl": "pf-m-column-reverse-on-xl",
        "rowOnXl": "pf-m-row-on-xl",
        "rowReverseOnXl": "pf-m-row-reverse-on-xl",
        "wrapOnXl": "pf-m-wrap-on-xl",
        "wrapReverseOnXl": "pf-m-wrap-reverse-on-xl",
        "nowrapOnXl": "pf-m-nowrap-on-xl",
        "justifyContentFlexStartOnXl": "pf-m-justify-content-flex-start-on-xl",
        "justifyContentFlexEndOnXl": "pf-m-justify-content-flex-end-on-xl",
        "justifyContentCenterOnXl": "pf-m-justify-content-center-on-xl",
        "justifyContentSpaceBetweenOnXl": "pf-m-justify-content-space-between-on-xl",
        "justifyContentSpaceAroundOnXl": "pf-m-justify-content-space-around-on-xl",
        "justifyContentSpaceEvenlyOnXl": "pf-m-justify-content-space-evenly-on-xl",
        "alignItemsFlexStartOnXl": "pf-m-align-items-flex-start-on-xl",
        "alignItemsFlexEndOnXl": "pf-m-align-items-flex-end-on-xl",
        "alignItemsCenterOnXl": "pf-m-align-items-center-on-xl",
        "alignItemsStretchOnXl": "pf-m-align-items-stretch-on-xl",
        "alignItemsBaselineOnXl": "pf-m-align-items-baseline-on-xl",
        "alignContentFlexStartOnXl": "pf-m-align-content-flex-start-on-xl",
        "alignContentFlexEndOnXl": "pf-m-align-content-flex-end-on-xl",
        "alignContentCenterOnXl": "pf-m-align-content-center-on-xl",
        "alignContentStretchOnXl": "pf-m-align-content-stretch-on-xl",
        "alignContentSpaceBetweenOnXl": "pf-m-align-content-space-between-on-xl",
        "alignContentSpaceAroundOnXl": "pf-m-align-content-space-around-on-xl",
        "alignRightOnXl": "pf-m-align-right-on-xl",
        "alignLeftOnXl": "pf-m-align-left-on-xl",
        "growOnXl": "pf-m-grow-on-xl",
        "shrinkOnXl": "pf-m-shrink-on-xl",
        "fullWidthOnXl": "pf-m-full-width-on-xl",
        "flex_1OnXl": "pf-m-flex-1-on-xl",
        "flex_2OnXl": "pf-m-flex-2-on-xl",
        "flex_3OnXl": "pf-m-flex-3-on-xl",
        "flex_4OnXl": "pf-m-flex-4-on-xl",
        "flexDefaultOnXl": "pf-m-flex-default-on-xl",
        "flexNoneOnXl": "pf-m-flex-none-on-xl",
        "alignSelfFlexStartOnXl": "pf-m-align-self-flex-start-on-xl",
        "alignSelfFlexEndOnXl": "pf-m-align-self-flex-end-on-xl",
        "alignSelfCenterOnXl": "pf-m-align-self-center-on-xl",
        "alignSelfBaselineOnXl": "pf-m-align-self-baseline-on-xl",
        "alignSelfStretchOnXl": "pf-m-align-self-stretch-on-xl",
        "flexOn_2xl": "pf-m-flex-on-2xl",
        "inlineFlexOn_2xl": "pf-m-inline-flex-on-2xl",
        "columnOn_2xl": "pf-m-column-on-2xl",
        "columnReverseOn_2xl": "pf-m-column-reverse-on-2xl",
        "rowOn_2xl": "pf-m-row-on-2xl",
        "rowReverseOn_2xl": "pf-m-row-reverse-on-2xl",
        "wrapOn_2xl": "pf-m-wrap-on-2xl",
        "wrapReverseOn_2xl": "pf-m-wrap-reverse-on-2xl",
        "nowrapOn_2xl": "pf-m-nowrap-on-2xl",
        "justifyContentFlexStartOn_2xl": "pf-m-justify-content-flex-start-on-2xl",
        "justifyContentFlexEndOn_2xl": "pf-m-justify-content-flex-end-on-2xl",
        "justifyContentCenterOn_2xl": "pf-m-justify-content-center-on-2xl",
        "justifyContentSpaceBetweenOn_2xl": "pf-m-justify-content-space-between-on-2xl",
        "justifyContentSpaceAroundOn_2xl": "pf-m-justify-content-space-around-on-2xl",
        "justifyContentSpaceEvenlyOn_2xl": "pf-m-justify-content-space-evenly-on-2xl",
        "alignItemsFlexStartOn_2xl": "pf-m-align-items-flex-start-on-2xl",
        "alignItemsFlexEndOn_2xl": "pf-m-align-items-flex-end-on-2xl",
        "alignItemsCenterOn_2xl": "pf-m-align-items-center-on-2xl",
        "alignItemsStretchOn_2xl": "pf-m-align-items-stretch-on-2xl",
        "alignItemsBaselineOn_2xl": "pf-m-align-items-baseline-on-2xl",
        "alignContentFlexStartOn_2xl": "pf-m-align-content-flex-start-on-2xl",
        "alignContentFlexEndOn_2xl": "pf-m-align-content-flex-end-on-2xl",
        "alignContentCenterOn_2xl": "pf-m-align-content-center-on-2xl",
        "alignContentStretchOn_2xl": "pf-m-align-content-stretch-on-2xl",
        "alignContentSpaceBetweenOn_2xl": "pf-m-align-content-space-between-on-2xl",
        "alignContentSpaceAroundOn_2xl": "pf-m-align-content-space-around-on-2xl",
        "alignRightOn_2xl": "pf-m-align-right-on-2xl",
        "alignLeftOn_2xl": "pf-m-align-left-on-2xl",
        "growOn_2xl": "pf-m-grow-on-2xl",
        "shrinkOn_2xl": "pf-m-shrink-on-2xl",
        "fullWidthOn_2xl": "pf-m-full-width-on-2xl",
        "flex_1On_2xl": "pf-m-flex-1-on-2xl",
        "flex_2On_2xl": "pf-m-flex-2-on-2xl",
        "flex_3On_2xl": "pf-m-flex-3-on-2xl",
        "flex_4On_2xl": "pf-m-flex-4-on-2xl",
        "flexDefaultOn_2xl": "pf-m-flex-default-on-2xl",
        "flexNoneOn_2xl": "pf-m-flex-none-on-2xl",
        "alignSelfFlexStartOn_2xl": "pf-m-align-self-flex-start-on-2xl",
        "alignSelfFlexEndOn_2xl": "pf-m-align-self-flex-end-on-2xl",
        "alignSelfCenterOn_2xl": "pf-m-align-self-center-on-2xl",
        "alignSelfBaselineOn_2xl": "pf-m-align-self-baseline-on-2xl",
        "alignSelfStretchOn_2xl": "pf-m-align-self-stretch-on-2xl",
        "spaceItemsNone": "pf-m-space-items-none",
        "spaceItemsXs": "pf-m-space-items-xs",
        "spaceItemsSm": "pf-m-space-items-sm",
        "spaceItemsMd": "pf-m-space-items-md",
        "spaceItemsLg": "pf-m-space-items-lg",
        "spaceItemsXl": "pf-m-space-items-xl",
        "spaceItems_2xl": "pf-m-space-items-2xl",
        "spaceItems_3xl": "pf-m-space-items-3xl",
        "spaceItems_4xl": "pf-m-space-items-4xl",
        "spaceItemsNoneOnSm": "pf-m-space-items-none-on-sm",
        "spaceItemsXsOnSm": "pf-m-space-items-xs-on-sm",
        "spaceItemsSmOnSm": "pf-m-space-items-sm-on-sm",
        "spaceItemsMdOnSm": "pf-m-space-items-md-on-sm",
        "spaceItemsLgOnSm": "pf-m-space-items-lg-on-sm",
        "spaceItemsXlOnSm": "pf-m-space-items-xl-on-sm",
        "spaceItems_2xlOnSm": "pf-m-space-items-2xl-on-sm",
        "spaceItems_3xlOnSm": "pf-m-space-items-3xl-on-sm",
        "spaceItems_4xlOnSm": "pf-m-space-items-4xl-on-sm",
        "spaceItemsNoneOnMd": "pf-m-space-items-none-on-md",
        "spaceItemsXsOnMd": "pf-m-space-items-xs-on-md",
        "spaceItemsSmOnMd": "pf-m-space-items-sm-on-md",
        "spaceItemsMdOnMd": "pf-m-space-items-md-on-md",
        "spaceItemsLgOnMd": "pf-m-space-items-lg-on-md",
        "spaceItemsXlOnMd": "pf-m-space-items-xl-on-md",
        "spaceItems_2xlOnMd": "pf-m-space-items-2xl-on-md",
        "spaceItems_3xlOnMd": "pf-m-space-items-3xl-on-md",
        "spaceItems_4xlOnMd": "pf-m-space-items-4xl-on-md",
        "spaceItemsNoneOnLg": "pf-m-space-items-none-on-lg",
        "spaceItemsXsOnLg": "pf-m-space-items-xs-on-lg",
        "spaceItemsSmOnLg": "pf-m-space-items-sm-on-lg",
        "spaceItemsMdOnLg": "pf-m-space-items-md-on-lg",
        "spaceItemsLgOnLg": "pf-m-space-items-lg-on-lg",
        "spaceItemsXlOnLg": "pf-m-space-items-xl-on-lg",
        "spaceItems_2xlOnLg": "pf-m-space-items-2xl-on-lg",
        "spaceItems_3xlOnLg": "pf-m-space-items-3xl-on-lg",
        "spaceItems_4xlOnLg": "pf-m-space-items-4xl-on-lg",
        "spaceItemsNoneOnXl": "pf-m-space-items-none-on-xl",
        "spaceItemsXsOnXl": "pf-m-space-items-xs-on-xl",
        "spaceItemsSmOnXl": "pf-m-space-items-sm-on-xl",
        "spaceItemsMdOnXl": "pf-m-space-items-md-on-xl",
        "spaceItemsLgOnXl": "pf-m-space-items-lg-on-xl",
        "spaceItemsXlOnXl": "pf-m-space-items-xl-on-xl",
        "spaceItems_2xlOnXl": "pf-m-space-items-2xl-on-xl",
        "spaceItems_3xlOnXl": "pf-m-space-items-3xl-on-xl",
        "spaceItems_4xlOnXl": "pf-m-space-items-4xl-on-xl",
        "spaceItemsNoneOn_2xl": "pf-m-space-items-none-on-2xl",
        "spaceItemsXsOn_2xl": "pf-m-space-items-xs-on-2xl",
        "spaceItemsSmOn_2xl": "pf-m-space-items-sm-on-2xl",
        "spaceItemsMdOn_2xl": "pf-m-space-items-md-on-2xl",
        "spaceItemsLgOn_2xl": "pf-m-space-items-lg-on-2xl",
        "spaceItemsXlOn_2xl": "pf-m-space-items-xl-on-2xl",
        "spaceItems_2xlOn_2xl": "pf-m-space-items-2xl-on-2xl",
        "spaceItems_3xlOn_2xl": "pf-m-space-items-3xl-on-2xl",
        "spaceItems_4xlOn_2xl": "pf-m-space-items-4xl-on-2xl",
        "spacerNone": "pf-m-spacer-none",
        "spacerXs": "pf-m-spacer-xs",
        "spacerSm": "pf-m-spacer-sm",
        "spacerMd": "pf-m-spacer-md",
        "spacerLg": "pf-m-spacer-lg",
        "spacerXl": "pf-m-spacer-xl",
        "spacer_2xl": "pf-m-spacer-2xl",
        "spacer_3xl": "pf-m-spacer-3xl",
        "spacer_4xl": "pf-m-spacer-4xl",
        "spacerNoneOnSm": "pf-m-spacer-none-on-sm",
        "spacerXsOnSm": "pf-m-spacer-xs-on-sm",
        "spacerSmOnSm": "pf-m-spacer-sm-on-sm",
        "spacerMdOnSm": "pf-m-spacer-md-on-sm",
        "spacerLgOnSm": "pf-m-spacer-lg-on-sm",
        "spacerXlOnSm": "pf-m-spacer-xl-on-sm",
        "spacer_2xlOnSm": "pf-m-spacer-2xl-on-sm",
        "spacer_3xlOnSm": "pf-m-spacer-3xl-on-sm",
        "spacer_4xlOnSm": "pf-m-spacer-4xl-on-sm",
        "spacerNoneOnMd": "pf-m-spacer-none-on-md",
        "spacerXsOnMd": "pf-m-spacer-xs-on-md",
        "spacerSmOnMd": "pf-m-spacer-sm-on-md",
        "spacerMdOnMd": "pf-m-spacer-md-on-md",
        "spacerLgOnMd": "pf-m-spacer-lg-on-md",
        "spacerXlOnMd": "pf-m-spacer-xl-on-md",
        "spacer_2xlOnMd": "pf-m-spacer-2xl-on-md",
        "spacer_3xlOnMd": "pf-m-spacer-3xl-on-md",
        "spacer_4xlOnMd": "pf-m-spacer-4xl-on-md",
        "spacerNoneOnLg": "pf-m-spacer-none-on-lg",
        "spacerXsOnLg": "pf-m-spacer-xs-on-lg",
        "spacerSmOnLg": "pf-m-spacer-sm-on-lg",
        "spacerMdOnLg": "pf-m-spacer-md-on-lg",
        "spacerLgOnLg": "pf-m-spacer-lg-on-lg",
        "spacerXlOnLg": "pf-m-spacer-xl-on-lg",
        "spacer_2xlOnLg": "pf-m-spacer-2xl-on-lg",
        "spacer_3xlOnLg": "pf-m-spacer-3xl-on-lg",
        "spacer_4xlOnLg": "pf-m-spacer-4xl-on-lg",
        "spacerNoneOnXl": "pf-m-spacer-none-on-xl",
        "spacerXsOnXl": "pf-m-spacer-xs-on-xl",
        "spacerSmOnXl": "pf-m-spacer-sm-on-xl",
        "spacerMdOnXl": "pf-m-spacer-md-on-xl",
        "spacerLgOnXl": "pf-m-spacer-lg-on-xl",
        "spacerXlOnXl": "pf-m-spacer-xl-on-xl",
        "spacer_2xlOnXl": "pf-m-spacer-2xl-on-xl",
        "spacer_3xlOnXl": "pf-m-spacer-3xl-on-xl",
        "spacer_4xlOnXl": "pf-m-spacer-4xl-on-xl",
        "spacerNoneOn_2xl": "pf-m-spacer-none-on-2xl",
        "spacerXsOn_2xl": "pf-m-spacer-xs-on-2xl",
        "spacerSmOn_2xl": "pf-m-spacer-sm-on-2xl",
        "spacerMdOn_2xl": "pf-m-spacer-md-on-2xl",
        "spacerLgOn_2xl": "pf-m-spacer-lg-on-2xl",
        "spacerXlOn_2xl": "pf-m-spacer-xl-on-2xl",
        "spacer_2xlOn_2xl": "pf-m-spacer-2xl-on-2xl",
        "spacer_3xlOn_2xl": "pf-m-spacer-3xl-on-2xl",
        "spacer_4xlOn_2xl": "pf-m-spacer-4xl-on-2xl"
      }
    };
    });

    var styles$N = unwrapExports(flex);

    const Flex = (_a) => {
        var { children = null, className = '', spacer, spaceItems, grow, shrink, flex, direction, alignItems, alignContent, alignSelf, align, justifyContent, display, fullWidth, flexWrap } = _a, props = __rest(_a, ["children", "className", "spacer", "spaceItems", "grow", "shrink", "flex", "direction", "alignItems", "alignContent", "alignSelf", "align", "justifyContent", "display", "fullWidth", "flexWrap"]);
        return (React.createElement("div", Object.assign({ className: css(styles$N.flex, formatBreakpointMods(spacer, styles$N), formatBreakpointMods(spaceItems, styles$N), formatBreakpointMods(grow, styles$N), formatBreakpointMods(shrink, styles$N), formatBreakpointMods(flex, styles$N), formatBreakpointMods(direction, styles$N), formatBreakpointMods(alignItems, styles$N), formatBreakpointMods(alignContent, styles$N), formatBreakpointMods(alignSelf, styles$N), formatBreakpointMods(align, styles$N), formatBreakpointMods(justifyContent, styles$N), formatBreakpointMods(display, styles$N), formatBreakpointMods(fullWidth, styles$N), formatBreakpointMods(flexWrap, styles$N), className) }, props), children));
    };
    Flex.displayName = 'Flex';

    const FlexItem = (_a) => {
        var { children = null, className = '', spacer, grow, shrink, flex, alignSelf, align, fullWidth } = _a, props = __rest(_a, ["children", "className", "spacer", "grow", "shrink", "flex", "alignSelf", "align", "fullWidth"]);
        return (React.createElement("div", Object.assign({}, props, { className: css(formatBreakpointMods(spacer, styles$N), formatBreakpointMods(grow, styles$N), formatBreakpointMods(shrink, styles$N), formatBreakpointMods(flex, styles$N), formatBreakpointMods(alignSelf, styles$N), formatBreakpointMods(align, styles$N), formatBreakpointMods(fullWidth, styles$N), className) }), children));
    };
    FlexItem.displayName = 'FlexItem';

    var gallery = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "gallery": "pf-l-gallery",
      "modifiers": {
        "gutter": "pf-m-gutter"
      }
    };
    });

    var styles$O = unwrapExports(gallery);

    const Gallery = (_a) => {
        var { children = null, className = '', hasGutter = false } = _a, props = __rest(_a, ["children", "className", "hasGutter"]);
        return (React.createElement("div", Object.assign({ className: css(styles$O.gallery, hasGutter && styles$O.modifiers.gutter, className) }, props), children));
    };
    Gallery.displayName = 'Gallery';

    const GalleryItem = (_a) => {
        var { children = null } = _a, props = __rest(_a, ["children"]);
        return React.createElement("div", Object.assign({}, props), children);
    };
    GalleryItem.displayName = 'GalleryItem';

    var grid = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "grid": "pf-l-grid",
      "gridItem": "pf-l-grid__item",
      "modifiers": {
        "all_1Col": "pf-m-all-1-col",
        "all_2Col": "pf-m-all-2-col",
        "all_3Col": "pf-m-all-3-col",
        "all_4Col": "pf-m-all-4-col",
        "all_5Col": "pf-m-all-5-col",
        "all_6Col": "pf-m-all-6-col",
        "all_7Col": "pf-m-all-7-col",
        "all_8Col": "pf-m-all-8-col",
        "all_9Col": "pf-m-all-9-col",
        "all_10Col": "pf-m-all-10-col",
        "all_11Col": "pf-m-all-11-col",
        "all_12Col": "pf-m-all-12-col",
        "all_1ColOnSm": "pf-m-all-1-col-on-sm",
        "all_2ColOnSm": "pf-m-all-2-col-on-sm",
        "all_3ColOnSm": "pf-m-all-3-col-on-sm",
        "all_4ColOnSm": "pf-m-all-4-col-on-sm",
        "all_5ColOnSm": "pf-m-all-5-col-on-sm",
        "all_6ColOnSm": "pf-m-all-6-col-on-sm",
        "all_7ColOnSm": "pf-m-all-7-col-on-sm",
        "all_8ColOnSm": "pf-m-all-8-col-on-sm",
        "all_9ColOnSm": "pf-m-all-9-col-on-sm",
        "all_10ColOnSm": "pf-m-all-10-col-on-sm",
        "all_11ColOnSm": "pf-m-all-11-col-on-sm",
        "all_12ColOnSm": "pf-m-all-12-col-on-sm",
        "all_1ColOnMd": "pf-m-all-1-col-on-md",
        "all_2ColOnMd": "pf-m-all-2-col-on-md",
        "all_3ColOnMd": "pf-m-all-3-col-on-md",
        "all_4ColOnMd": "pf-m-all-4-col-on-md",
        "all_5ColOnMd": "pf-m-all-5-col-on-md",
        "all_6ColOnMd": "pf-m-all-6-col-on-md",
        "all_7ColOnMd": "pf-m-all-7-col-on-md",
        "all_8ColOnMd": "pf-m-all-8-col-on-md",
        "all_9ColOnMd": "pf-m-all-9-col-on-md",
        "all_10ColOnMd": "pf-m-all-10-col-on-md",
        "all_11ColOnMd": "pf-m-all-11-col-on-md",
        "all_12ColOnMd": "pf-m-all-12-col-on-md",
        "all_1ColOnLg": "pf-m-all-1-col-on-lg",
        "all_2ColOnLg": "pf-m-all-2-col-on-lg",
        "all_3ColOnLg": "pf-m-all-3-col-on-lg",
        "all_4ColOnLg": "pf-m-all-4-col-on-lg",
        "all_5ColOnLg": "pf-m-all-5-col-on-lg",
        "all_6ColOnLg": "pf-m-all-6-col-on-lg",
        "all_7ColOnLg": "pf-m-all-7-col-on-lg",
        "all_8ColOnLg": "pf-m-all-8-col-on-lg",
        "all_9ColOnLg": "pf-m-all-9-col-on-lg",
        "all_10ColOnLg": "pf-m-all-10-col-on-lg",
        "all_11ColOnLg": "pf-m-all-11-col-on-lg",
        "all_12ColOnLg": "pf-m-all-12-col-on-lg",
        "all_1ColOnXl": "pf-m-all-1-col-on-xl",
        "all_2ColOnXl": "pf-m-all-2-col-on-xl",
        "all_3ColOnXl": "pf-m-all-3-col-on-xl",
        "all_4ColOnXl": "pf-m-all-4-col-on-xl",
        "all_5ColOnXl": "pf-m-all-5-col-on-xl",
        "all_6ColOnXl": "pf-m-all-6-col-on-xl",
        "all_7ColOnXl": "pf-m-all-7-col-on-xl",
        "all_8ColOnXl": "pf-m-all-8-col-on-xl",
        "all_9ColOnXl": "pf-m-all-9-col-on-xl",
        "all_10ColOnXl": "pf-m-all-10-col-on-xl",
        "all_11ColOnXl": "pf-m-all-11-col-on-xl",
        "all_12ColOnXl": "pf-m-all-12-col-on-xl",
        "all_1ColOn_2xl": "pf-m-all-1-col-on-2xl",
        "all_2ColOn_2xl": "pf-m-all-2-col-on-2xl",
        "all_3ColOn_2xl": "pf-m-all-3-col-on-2xl",
        "all_4ColOn_2xl": "pf-m-all-4-col-on-2xl",
        "all_5ColOn_2xl": "pf-m-all-5-col-on-2xl",
        "all_6ColOn_2xl": "pf-m-all-6-col-on-2xl",
        "all_7ColOn_2xl": "pf-m-all-7-col-on-2xl",
        "all_8ColOn_2xl": "pf-m-all-8-col-on-2xl",
        "all_9ColOn_2xl": "pf-m-all-9-col-on-2xl",
        "all_10ColOn_2xl": "pf-m-all-10-col-on-2xl",
        "all_11ColOn_2xl": "pf-m-all-11-col-on-2xl",
        "all_12ColOn_2xl": "pf-m-all-12-col-on-2xl",
        "1Col": "pf-m-1-col",
        "2Col": "pf-m-2-col",
        "3Col": "pf-m-3-col",
        "4Col": "pf-m-4-col",
        "5Col": "pf-m-5-col",
        "6Col": "pf-m-6-col",
        "7Col": "pf-m-7-col",
        "8Col": "pf-m-8-col",
        "9Col": "pf-m-9-col",
        "10Col": "pf-m-10-col",
        "11Col": "pf-m-11-col",
        "12Col": "pf-m-12-col",
        "offset_1Col": "pf-m-offset-1-col",
        "offset_2Col": "pf-m-offset-2-col",
        "offset_3Col": "pf-m-offset-3-col",
        "offset_4Col": "pf-m-offset-4-col",
        "offset_5Col": "pf-m-offset-5-col",
        "offset_6Col": "pf-m-offset-6-col",
        "offset_7Col": "pf-m-offset-7-col",
        "offset_8Col": "pf-m-offset-8-col",
        "offset_9Col": "pf-m-offset-9-col",
        "offset_10Col": "pf-m-offset-10-col",
        "offset_11Col": "pf-m-offset-11-col",
        "offset_12Col": "pf-m-offset-12-col",
        "1Row": "pf-m-1-row",
        "2Row": "pf-m-2-row",
        "3Row": "pf-m-3-row",
        "4Row": "pf-m-4-row",
        "5Row": "pf-m-5-row",
        "6Row": "pf-m-6-row",
        "7Row": "pf-m-7-row",
        "8Row": "pf-m-8-row",
        "9Row": "pf-m-9-row",
        "10Row": "pf-m-10-row",
        "11Row": "pf-m-11-row",
        "12Row": "pf-m-12-row",
        "1ColOnSm": "pf-m-1-col-on-sm",
        "2ColOnSm": "pf-m-2-col-on-sm",
        "3ColOnSm": "pf-m-3-col-on-sm",
        "4ColOnSm": "pf-m-4-col-on-sm",
        "5ColOnSm": "pf-m-5-col-on-sm",
        "6ColOnSm": "pf-m-6-col-on-sm",
        "7ColOnSm": "pf-m-7-col-on-sm",
        "8ColOnSm": "pf-m-8-col-on-sm",
        "9ColOnSm": "pf-m-9-col-on-sm",
        "10ColOnSm": "pf-m-10-col-on-sm",
        "11ColOnSm": "pf-m-11-col-on-sm",
        "12ColOnSm": "pf-m-12-col-on-sm",
        "offset_1ColOnSm": "pf-m-offset-1-col-on-sm",
        "offset_2ColOnSm": "pf-m-offset-2-col-on-sm",
        "offset_3ColOnSm": "pf-m-offset-3-col-on-sm",
        "offset_4ColOnSm": "pf-m-offset-4-col-on-sm",
        "offset_5ColOnSm": "pf-m-offset-5-col-on-sm",
        "offset_6ColOnSm": "pf-m-offset-6-col-on-sm",
        "offset_7ColOnSm": "pf-m-offset-7-col-on-sm",
        "offset_8ColOnSm": "pf-m-offset-8-col-on-sm",
        "offset_9ColOnSm": "pf-m-offset-9-col-on-sm",
        "offset_10ColOnSm": "pf-m-offset-10-col-on-sm",
        "offset_11ColOnSm": "pf-m-offset-11-col-on-sm",
        "offset_12ColOnSm": "pf-m-offset-12-col-on-sm",
        "1RowOnSm": "pf-m-1-row-on-sm",
        "2RowOnSm": "pf-m-2-row-on-sm",
        "3RowOnSm": "pf-m-3-row-on-sm",
        "4RowOnSm": "pf-m-4-row-on-sm",
        "5RowOnSm": "pf-m-5-row-on-sm",
        "6RowOnSm": "pf-m-6-row-on-sm",
        "7RowOnSm": "pf-m-7-row-on-sm",
        "8RowOnSm": "pf-m-8-row-on-sm",
        "9RowOnSm": "pf-m-9-row-on-sm",
        "10RowOnSm": "pf-m-10-row-on-sm",
        "11RowOnSm": "pf-m-11-row-on-sm",
        "12RowOnSm": "pf-m-12-row-on-sm",
        "1ColOnMd": "pf-m-1-col-on-md",
        "2ColOnMd": "pf-m-2-col-on-md",
        "3ColOnMd": "pf-m-3-col-on-md",
        "4ColOnMd": "pf-m-4-col-on-md",
        "5ColOnMd": "pf-m-5-col-on-md",
        "6ColOnMd": "pf-m-6-col-on-md",
        "7ColOnMd": "pf-m-7-col-on-md",
        "8ColOnMd": "pf-m-8-col-on-md",
        "9ColOnMd": "pf-m-9-col-on-md",
        "10ColOnMd": "pf-m-10-col-on-md",
        "11ColOnMd": "pf-m-11-col-on-md",
        "12ColOnMd": "pf-m-12-col-on-md",
        "offset_1ColOnMd": "pf-m-offset-1-col-on-md",
        "offset_2ColOnMd": "pf-m-offset-2-col-on-md",
        "offset_3ColOnMd": "pf-m-offset-3-col-on-md",
        "offset_4ColOnMd": "pf-m-offset-4-col-on-md",
        "offset_5ColOnMd": "pf-m-offset-5-col-on-md",
        "offset_6ColOnMd": "pf-m-offset-6-col-on-md",
        "offset_7ColOnMd": "pf-m-offset-7-col-on-md",
        "offset_8ColOnMd": "pf-m-offset-8-col-on-md",
        "offset_9ColOnMd": "pf-m-offset-9-col-on-md",
        "offset_10ColOnMd": "pf-m-offset-10-col-on-md",
        "offset_11ColOnMd": "pf-m-offset-11-col-on-md",
        "offset_12ColOnMd": "pf-m-offset-12-col-on-md",
        "1RowOnMd": "pf-m-1-row-on-md",
        "2RowOnMd": "pf-m-2-row-on-md",
        "3RowOnMd": "pf-m-3-row-on-md",
        "4RowOnMd": "pf-m-4-row-on-md",
        "5RowOnMd": "pf-m-5-row-on-md",
        "6RowOnMd": "pf-m-6-row-on-md",
        "7RowOnMd": "pf-m-7-row-on-md",
        "8RowOnMd": "pf-m-8-row-on-md",
        "9RowOnMd": "pf-m-9-row-on-md",
        "10RowOnMd": "pf-m-10-row-on-md",
        "11RowOnMd": "pf-m-11-row-on-md",
        "12RowOnMd": "pf-m-12-row-on-md",
        "1ColOnLg": "pf-m-1-col-on-lg",
        "2ColOnLg": "pf-m-2-col-on-lg",
        "3ColOnLg": "pf-m-3-col-on-lg",
        "4ColOnLg": "pf-m-4-col-on-lg",
        "5ColOnLg": "pf-m-5-col-on-lg",
        "6ColOnLg": "pf-m-6-col-on-lg",
        "7ColOnLg": "pf-m-7-col-on-lg",
        "8ColOnLg": "pf-m-8-col-on-lg",
        "9ColOnLg": "pf-m-9-col-on-lg",
        "10ColOnLg": "pf-m-10-col-on-lg",
        "11ColOnLg": "pf-m-11-col-on-lg",
        "12ColOnLg": "pf-m-12-col-on-lg",
        "offset_1ColOnLg": "pf-m-offset-1-col-on-lg",
        "offset_2ColOnLg": "pf-m-offset-2-col-on-lg",
        "offset_3ColOnLg": "pf-m-offset-3-col-on-lg",
        "offset_4ColOnLg": "pf-m-offset-4-col-on-lg",
        "offset_5ColOnLg": "pf-m-offset-5-col-on-lg",
        "offset_6ColOnLg": "pf-m-offset-6-col-on-lg",
        "offset_7ColOnLg": "pf-m-offset-7-col-on-lg",
        "offset_8ColOnLg": "pf-m-offset-8-col-on-lg",
        "offset_9ColOnLg": "pf-m-offset-9-col-on-lg",
        "offset_10ColOnLg": "pf-m-offset-10-col-on-lg",
        "offset_11ColOnLg": "pf-m-offset-11-col-on-lg",
        "offset_12ColOnLg": "pf-m-offset-12-col-on-lg",
        "1RowOnLg": "pf-m-1-row-on-lg",
        "2RowOnLg": "pf-m-2-row-on-lg",
        "3RowOnLg": "pf-m-3-row-on-lg",
        "4RowOnLg": "pf-m-4-row-on-lg",
        "5RowOnLg": "pf-m-5-row-on-lg",
        "6RowOnLg": "pf-m-6-row-on-lg",
        "7RowOnLg": "pf-m-7-row-on-lg",
        "8RowOnLg": "pf-m-8-row-on-lg",
        "9RowOnLg": "pf-m-9-row-on-lg",
        "10RowOnLg": "pf-m-10-row-on-lg",
        "11RowOnLg": "pf-m-11-row-on-lg",
        "12RowOnLg": "pf-m-12-row-on-lg",
        "1ColOnXl": "pf-m-1-col-on-xl",
        "2ColOnXl": "pf-m-2-col-on-xl",
        "3ColOnXl": "pf-m-3-col-on-xl",
        "4ColOnXl": "pf-m-4-col-on-xl",
        "5ColOnXl": "pf-m-5-col-on-xl",
        "6ColOnXl": "pf-m-6-col-on-xl",
        "7ColOnXl": "pf-m-7-col-on-xl",
        "8ColOnXl": "pf-m-8-col-on-xl",
        "9ColOnXl": "pf-m-9-col-on-xl",
        "10ColOnXl": "pf-m-10-col-on-xl",
        "11ColOnXl": "pf-m-11-col-on-xl",
        "12ColOnXl": "pf-m-12-col-on-xl",
        "offset_1ColOnXl": "pf-m-offset-1-col-on-xl",
        "offset_2ColOnXl": "pf-m-offset-2-col-on-xl",
        "offset_3ColOnXl": "pf-m-offset-3-col-on-xl",
        "offset_4ColOnXl": "pf-m-offset-4-col-on-xl",
        "offset_5ColOnXl": "pf-m-offset-5-col-on-xl",
        "offset_6ColOnXl": "pf-m-offset-6-col-on-xl",
        "offset_7ColOnXl": "pf-m-offset-7-col-on-xl",
        "offset_8ColOnXl": "pf-m-offset-8-col-on-xl",
        "offset_9ColOnXl": "pf-m-offset-9-col-on-xl",
        "offset_10ColOnXl": "pf-m-offset-10-col-on-xl",
        "offset_11ColOnXl": "pf-m-offset-11-col-on-xl",
        "offset_12ColOnXl": "pf-m-offset-12-col-on-xl",
        "1RowOnXl": "pf-m-1-row-on-xl",
        "2RowOnXl": "pf-m-2-row-on-xl",
        "3RowOnXl": "pf-m-3-row-on-xl",
        "4RowOnXl": "pf-m-4-row-on-xl",
        "5RowOnXl": "pf-m-5-row-on-xl",
        "6RowOnXl": "pf-m-6-row-on-xl",
        "7RowOnXl": "pf-m-7-row-on-xl",
        "8RowOnXl": "pf-m-8-row-on-xl",
        "9RowOnXl": "pf-m-9-row-on-xl",
        "10RowOnXl": "pf-m-10-row-on-xl",
        "11RowOnXl": "pf-m-11-row-on-xl",
        "12RowOnXl": "pf-m-12-row-on-xl",
        "1ColOn_2xl": "pf-m-1-col-on-2xl",
        "2ColOn_2xl": "pf-m-2-col-on-2xl",
        "3ColOn_2xl": "pf-m-3-col-on-2xl",
        "4ColOn_2xl": "pf-m-4-col-on-2xl",
        "5ColOn_2xl": "pf-m-5-col-on-2xl",
        "6ColOn_2xl": "pf-m-6-col-on-2xl",
        "7ColOn_2xl": "pf-m-7-col-on-2xl",
        "8ColOn_2xl": "pf-m-8-col-on-2xl",
        "9ColOn_2xl": "pf-m-9-col-on-2xl",
        "10ColOn_2xl": "pf-m-10-col-on-2xl",
        "11ColOn_2xl": "pf-m-11-col-on-2xl",
        "12ColOn_2xl": "pf-m-12-col-on-2xl",
        "offset_1ColOn_2xl": "pf-m-offset-1-col-on-2xl",
        "offset_2ColOn_2xl": "pf-m-offset-2-col-on-2xl",
        "offset_3ColOn_2xl": "pf-m-offset-3-col-on-2xl",
        "offset_4ColOn_2xl": "pf-m-offset-4-col-on-2xl",
        "offset_5ColOn_2xl": "pf-m-offset-5-col-on-2xl",
        "offset_6ColOn_2xl": "pf-m-offset-6-col-on-2xl",
        "offset_7ColOn_2xl": "pf-m-offset-7-col-on-2xl",
        "offset_8ColOn_2xl": "pf-m-offset-8-col-on-2xl",
        "offset_9ColOn_2xl": "pf-m-offset-9-col-on-2xl",
        "offset_10ColOn_2xl": "pf-m-offset-10-col-on-2xl",
        "offset_11ColOn_2xl": "pf-m-offset-11-col-on-2xl",
        "offset_12ColOn_2xl": "pf-m-offset-12-col-on-2xl",
        "1RowOn_2xl": "pf-m-1-row-on-2xl",
        "2RowOn_2xl": "pf-m-2-row-on-2xl",
        "3RowOn_2xl": "pf-m-3-row-on-2xl",
        "4RowOn_2xl": "pf-m-4-row-on-2xl",
        "5RowOn_2xl": "pf-m-5-row-on-2xl",
        "6RowOn_2xl": "pf-m-6-row-on-2xl",
        "7RowOn_2xl": "pf-m-7-row-on-2xl",
        "8RowOn_2xl": "pf-m-8-row-on-2xl",
        "9RowOn_2xl": "pf-m-9-row-on-2xl",
        "10RowOn_2xl": "pf-m-10-row-on-2xl",
        "11RowOn_2xl": "pf-m-11-row-on-2xl",
        "12RowOn_2xl": "pf-m-12-row-on-2xl",
        "gutter": "pf-m-gutter"
      }
    };
    });

    var styles$P = unwrapExports(grid);

    (function (BaseSizes) {
        BaseSizes["xs"] = "xs";
        BaseSizes["sm"] = "sm";
        BaseSizes["md"] = "md";
        BaseSizes["lg"] = "lg";
        BaseSizes["xl"] = "xl";
        BaseSizes["2xl"] = "2xl";
        BaseSizes["3xl"] = "3xl";
        BaseSizes["4xl"] = "4xl";
    })(exports.BaseSizes || (exports.BaseSizes = {}));
    (function (DeviceSizes) {
        DeviceSizes["sm"] = "Sm";
        DeviceSizes["md"] = "Md";
        DeviceSizes["lg"] = "Lg";
        DeviceSizes["xl"] = "Xl";
        DeviceSizes["xl2"] = "_2xl";
    })(exports.DeviceSizes || (exports.DeviceSizes = {}));

    const Grid = (_a) => {
        var { children = null, className = '', hasGutter, span = null } = _a, props = __rest(_a, ["children", "className", "hasGutter", "span"]);
        const classes = [styles$P.grid, span && styles$P.modifiers[`all_${span}Col`]];
        Object.entries(exports.DeviceSizes).forEach(([propKey, gridSpanModifier]) => {
            const key = propKey;
            const propValue = props[key];
            if (propValue) {
                classes.push(styles$P.modifiers[`all_${propValue}ColOn${gridSpanModifier}`]);
            }
            delete props[key];
        });
        return (React.createElement("div", Object.assign({ className: css(...classes, hasGutter && styles$P.modifiers.gutter, className) }, props), children));
    };
    Grid.displayName = 'Grid';

    const GridItem = (_a) => {
        var { children = null, className = '', span = null, rowSpan = null, offset = null } = _a, props = __rest(_a, ["children", "className", "span", "rowSpan", "offset"]);
        const classes = [
            styles$P.gridItem,
            span && styles$P.modifiers[`${span}Col`],
            rowSpan && styles$P.modifiers[`${rowSpan}Row`],
            offset && styles$P.modifiers[`offset_${offset}Col`]
        ];
        Object.entries(exports.DeviceSizes).forEach(([propKey, classModifier]) => {
            const key = propKey;
            const rowSpanKey = `${key}RowSpan`;
            const offsetKey = `${key}Offset`;
            const spanValue = props[key];
            const rowSpanValue = props[rowSpanKey];
            const offsetValue = props[offsetKey];
            if (spanValue) {
                classes.push(styles$P.modifiers[`${spanValue}ColOn${classModifier}`]);
            }
            if (rowSpanValue) {
                classes.push(styles$P.modifiers[`${rowSpanValue}RowOn${classModifier}`]);
            }
            if (offsetValue) {
                classes.push(styles$P.modifiers[`offset_${offsetValue}ColOn${classModifier}`]);
            }
            delete props[key];
            delete props[rowSpanKey];
            delete props[offsetKey];
        });
        return (React.createElement("div", Object.assign({ className: css(...classes, className) }, props), children));
    };
    GridItem.displayName = 'GridItem';

    var level = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "level": "pf-l-level",
      "modifiers": {
        "gutter": "pf-m-gutter"
      }
    };
    });

    var styles$Q = unwrapExports(level);

    const Level = (_a) => {
        var { hasGutter, className = '', children = null } = _a, props = __rest(_a, ["hasGutter", "className", "children"]);
        return (React.createElement("div", Object.assign({}, props, { className: css(styles$Q.level, hasGutter && styles$Q.modifiers.gutter, className) }), children));
    };
    Level.displayName = 'Level';

    const LevelItem = (_a) => {
        var { children = null } = _a, props = __rest(_a, ["children"]);
        return (React.createElement("div", Object.assign({}, props), children));
    };
    LevelItem.displayName = 'LevelItem';

    var split = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "modifiers": {
        "fill": "pf-m-fill",
        "gutter": "pf-m-gutter"
      },
      "split": "pf-l-split",
      "splitItem": "pf-l-split__item"
    };
    });

    var styles$R = unwrapExports(split);

    const Split = (_a) => {
        var { hasGutter = false, className = '', children = null, component = 'div' } = _a, props = __rest(_a, ["hasGutter", "className", "children", "component"]);
        const Component = component;
        return (React.createElement(Component, Object.assign({}, props, { className: css(styles$R.split, hasGutter && styles$R.modifiers.gutter, className) }), children));
    };
    Split.displayName = 'Split';

    const SplitItem = (_a) => {
        var { isFilled = false, className = '', children = null } = _a, props = __rest(_a, ["isFilled", "className", "children"]);
        return (React.createElement("div", Object.assign({}, props, { className: css(styles$R.splitItem, isFilled && styles$R.modifiers.fill, className) }), children));
    };
    SplitItem.displayName = 'SplitItem';

    var stack = createCommonjsModule(function (module, exports) {
    exports.__esModule = true;

    exports.default = {
      "modifiers": {
        "fill": "pf-m-fill",
        "gutter": "pf-m-gutter"
      },
      "stack": "pf-l-stack",
      "stackItem": "pf-l-stack__item"
    };
    });

    var styles$S = unwrapExports(stack);

    const Stack = (_a) => {
        var { hasGutter = false, className = '', children = null, component = 'div' } = _a, props = __rest(_a, ["hasGutter", "className", "children", "component"]);
        const Component = component;
        return (React.createElement(Component, Object.assign({}, props, { className: css(styles$S.stack, hasGutter && styles$S.modifiers.gutter, className) }), children));
    };
    Stack.displayName = 'Stack';

    const StackItem = (_a) => {
        var { isFilled = false, className = '', children = null } = _a, props = __rest(_a, ["isFilled", "className", "children"]);
        return (React.createElement("div", Object.assign({}, props, { className: css(styles$S.stackItem, isFilled && styles$S.modifiers.fill, className) }), children));
    };
    StackItem.displayName = 'StackItem';

    exports.ASTERISK = ASTERISK;
    exports.AboutModal = AboutModal;
    exports.Accordion = Accordion;
    exports.AccordionContent = AccordionContent;
    exports.AccordionItem = AccordionItem;
    exports.AccordionToggle = AccordionToggle;
    exports.ActionGroup = ActionGroup;
    exports.Alert = Alert;
    exports.AlertActionCloseButton = AlertActionCloseButton;
    exports.AlertActionLink = AlertActionLink;
    exports.AlertContext = AlertContext;
    exports.AlertGroup = AlertGroup;
    exports.ApplicationLauncher = ApplicationLauncher;
    exports.ApplicationLauncherContent = ApplicationLauncherContent;
    exports.ApplicationLauncherContext = ApplicationLauncherContext;
    exports.ApplicationLauncherGroup = ApplicationLauncherGroup;
    exports.ApplicationLauncherIcon = ApplicationLauncherIcon;
    exports.ApplicationLauncherItem = ApplicationLauncherItem;
    exports.ApplicationLauncherItemContext = ApplicationLauncherItemContext;
    exports.ApplicationLauncherSeparator = ApplicationLauncherSeparator;
    exports.ApplicationLauncherText = ApplicationLauncherText;
    exports.Avatar = Avatar;
    exports.Backdrop = Backdrop;
    exports.BackgroundImage = BackgroundImage;
    exports.Badge = Badge;
    exports.Banner = Banner;
    exports.Brand = Brand;
    exports.Breadcrumb = Breadcrumb;
    exports.BreadcrumbHeading = BreadcrumbHeading;
    exports.BreadcrumbItem = BreadcrumbItem;
    exports.Bullseye = Bullseye;
    exports.Button = Button;
    exports.Card = Card;
    exports.CardActions = CardActions;
    exports.CardBody = CardBody;
    exports.CardFooter = CardFooter;
    exports.CardHeader = CardHeader;
    exports.CardHeaderMain = CardHeaderMain;
    exports.CardTitle = CardTitle;
    exports.Checkbox = Checkbox;
    exports.Chip = Chip;
    exports.ChipGroup = ChipGroup;
    exports.ClipboardCopy = ClipboardCopy;
    exports.ContextSelector = ContextSelector;
    exports.ContextSelectorItem = ContextSelectorItem;
    exports.DataList = DataList;
    exports.DataListAction = DataListAction;
    exports.DataListCell = DataListCell;
    exports.DataListCheck = DataListCheck;
    exports.DataListContent = DataListContent;
    exports.DataListContext = DataListContext;
    exports.DataListItem = DataListItem;
    exports.DataListItemCells = DataListItemCells;
    exports.DataListItemRow = DataListItemRow;
    exports.DataListToggle = DataListToggle;
    exports.Divider = Divider;
    exports.Drawer = Drawer;
    exports.DrawerActions = DrawerActions;
    exports.DrawerCloseButton = DrawerCloseButton;
    exports.DrawerContent = DrawerContent;
    exports.DrawerContentBody = DrawerContentBody;
    exports.DrawerContext = DrawerContext;
    exports.DrawerHead = DrawerHead;
    exports.DrawerPanelBody = DrawerPanelBody;
    exports.DrawerPanelContent = DrawerPanelContent;
    exports.DrawerSection = DrawerSection;
    exports.Dropdown = Dropdown;
    exports.DropdownArrowContext = DropdownArrowContext;
    exports.DropdownContext = DropdownContext;
    exports.DropdownGroup = DropdownGroup;
    exports.DropdownItem = DropdownItem;
    exports.DropdownMenu = DropdownMenu;
    exports.DropdownSeparator = DropdownSeparator;
    exports.DropdownToggle = DropdownToggle;
    exports.DropdownToggleAction = DropdownToggleAction;
    exports.DropdownToggleCheckbox = DropdownToggleCheckbox;
    exports.DropdownWithContext = DropdownWithContext;
    exports.EmptyState = EmptyState;
    exports.EmptyStateBody = EmptyStateBody;
    exports.EmptyStateIcon = EmptyStateIcon;
    exports.EmptyStatePrimary = EmptyStatePrimary;
    exports.EmptyStateSecondaryActions = EmptyStateSecondaryActions;
    exports.ExpandableSection = ExpandableSection;
    exports.FileUpload = FileUpload;
    exports.FileUploadField = FileUploadField;
    exports.Flex = Flex;
    exports.FlexItem = FlexItem;
    exports.FocusTrap = FocusTrap;
    exports.Form = Form;
    exports.FormGroup = FormGroup;
    exports.FormHelperText = FormHelperText;
    exports.FormSelect = FormSelect;
    exports.FormSelectOption = FormSelectOption;
    exports.FormSelectOptionGroup = FormSelectOptionGroup;
    exports.Gallery = Gallery;
    exports.GalleryItem = GalleryItem;
    exports.GenerateId = GenerateId;
    exports.Grid = Grid;
    exports.GridItem = GridItem;
    exports.InputGroup = InputGroup;
    exports.InputGroupText = InputGroupText;
    exports.KEYHANDLER_DIRECTION = KEYHANDLER_DIRECTION;
    exports.KEY_CODES = KEY_CODES;
    exports.KebabToggle = KebabToggle;
    exports.KeyTypes = KeyTypes;
    exports.Label = Label;
    exports.Level = Level;
    exports.LevelItem = LevelItem;
    exports.List = List;
    exports.ListItem = ListItem;
    exports.Login = Login;
    exports.LoginFooter = LoginFooter;
    exports.LoginFooterItem = LoginFooterItem;
    exports.LoginForm = LoginForm;
    exports.LoginHeader = LoginHeader;
    exports.LoginMainBody = LoginMainBody;
    exports.LoginMainFooter = LoginMainFooter;
    exports.LoginMainFooterBandItem = LoginMainFooterBandItem;
    exports.LoginMainFooterLinksItem = LoginMainFooterLinksItem;
    exports.LoginMainHeader = LoginMainHeader;
    exports.LoginPage = LoginPage;
    exports.Modal = Modal;
    exports.ModalBox = ModalBox;
    exports.ModalBoxBody = ModalBoxBody;
    exports.ModalBoxCloseButton = ModalBoxCloseButton;
    exports.ModalBoxFooter = ModalBoxFooter;
    exports.ModalBoxHeader = ModalBoxHeader;
    exports.ModalContent = ModalContent;
    exports.Nav = Nav;
    exports.NavContext = NavContext;
    exports.NavExpandable = NavExpandable;
    exports.NavGroup = NavGroup;
    exports.NavItem = NavItem;
    exports.NavItemSeparator = NavItemSeparator;
    exports.NavList = NavList;
    exports.NotificationBadge = NotificationBadge;
    exports.NotificationDrawer = NotificationDrawer;
    exports.NotificationDrawerBody = NotificationDrawerBody;
    exports.NotificationDrawerGroup = NotificationDrawerGroup;
    exports.NotificationDrawerGroupList = NotificationDrawerGroupList;
    exports.NotificationDrawerHeader = NotificationDrawerHeader;
    exports.NotificationDrawerList = NotificationDrawerList;
    exports.NotificationDrawerListItem = NotificationDrawerListItem;
    exports.NotificationDrawerListItemBody = NotificationDrawerListItemBody;
    exports.NotificationDrawerListItemHeader = NotificationDrawerListItemHeader;
    exports.OptionsMenu = OptionsMenu;
    exports.OptionsMenuItem = OptionsMenuItem;
    exports.OptionsMenuItemGroup = OptionsMenuItemGroup;
    exports.OptionsMenuSeparator = OptionsMenuSeparator;
    exports.OptionsMenuToggle = OptionsMenuToggle;
    exports.OptionsMenuToggleWithText = OptionsMenuToggleWithText;
    exports.OverflowMenu = OverflowMenu;
    exports.OverflowMenuContent = OverflowMenuContent;
    exports.OverflowMenuControl = OverflowMenuControl;
    exports.OverflowMenuDropdownItem = OverflowMenuDropdownItem;
    exports.OverflowMenuGroup = OverflowMenuGroup;
    exports.OverflowMenuItem = OverflowMenuItem;
    exports.Page = Page;
    exports.PageContextConsumer = PageContextConsumer;
    exports.PageContextProvider = PageContextProvider;
    exports.PageHeader = PageHeader;
    exports.PageHeaderTools = PageHeaderTools;
    exports.PageHeaderToolsGroup = PageHeaderToolsGroup;
    exports.PageHeaderToolsItem = PageHeaderToolsItem;
    exports.PageSection = PageSection;
    exports.PageSidebar = PageSidebar;
    exports.Pagination = Pagination;
    exports.Popover = Popover;
    exports.Progress = Progress;
    exports.ProgressBar = ProgressBar;
    exports.ProgressContainer = ProgressContainer;
    exports.Radio = Radio;
    exports.SIDE = SIDE;
    exports.Select = Select;
    exports.SelectConsumer = SelectConsumer;
    exports.SelectContext = SelectContext;
    exports.SelectGroup = SelectGroup;
    exports.SelectOption = SelectOption;
    exports.SelectProvider = SelectProvider;
    exports.SimpleList = SimpleList;
    exports.SimpleListContext = SimpleListContext;
    exports.SimpleListGroup = SimpleListGroup;
    exports.SimpleListItem = SimpleListItem;
    exports.SkipToContent = SkipToContent;
    exports.Spinner = Spinner;
    exports.Split = Split;
    exports.SplitItem = SplitItem;
    exports.Stack = Stack;
    exports.StackItem = StackItem;
    exports.Switch = Switch;
    exports.Tab = Tab;
    exports.TabContent = TabContent;
    exports.TabTitleIcon = TabTitleIcon;
    exports.TabTitleText = TabTitleText;
    exports.Tabs = Tabs;
    exports.Text = Text;
    exports.TextArea = TextArea;
    exports.TextContent = TextContent;
    exports.TextInput = TextInput;
    exports.TextInputBase = TextInputBase;
    exports.TextList = TextList;
    exports.TextListItem = TextListItem;
    exports.Title = Title;
    exports.ToggleTemplate = ToggleTemplate;
    exports.Toolbar = Toolbar;
    exports.ToolbarContent = ToolbarContent;
    exports.ToolbarContentContext = ToolbarContentContext;
    exports.ToolbarContext = ToolbarContext;
    exports.ToolbarFilter = ToolbarFilter;
    exports.ToolbarGroup = ToolbarGroup;
    exports.ToolbarItem = ToolbarItem;
    exports.ToolbarToggleGroup = ToolbarToggleGroup;
    exports.Tooltip = Tooltip;
    exports.Wizard = Wizard;
    exports.WizardBody = WizardBody;
    exports.WizardContextConsumer = WizardContextConsumer;
    exports.WizardContextProvider = WizardContextProvider;
    exports.WizardFooter = WizardFooter;
    exports.WizardHeader = WizardHeader;
    exports.WizardNav = WizardNav;
    exports.WizardNavItem = WizardNavItem;
    exports.WizardToggle = WizardToggle;
    exports.canUseDOM = canUseDOM;
    exports.capitalize = capitalize;
    exports.clipboardCopyFunc = clipboardCopyFunc;
    exports.debounce = debounce;
    exports.fillTemplate = fillTemplate;
    exports.formatBreakpointMods = formatBreakpointMods;
    exports.getNextIndex = getNextIndex;
    exports.getOUIAProps = getOUIAProps;
    exports.getUniqueId = getUniqueId;
    exports.globalBreakpoints = globalBreakpoints;
    exports.isElementInView = isElementInView;
    exports.keyHandler = keyHandler;
    exports.pluralize = pluralize;
    exports.sideElementIsOutOfView = sideElementIsOutOfView;
    exports.toCamel = toCamel;
    exports.variantIcons = variantIcons$1;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const split_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/layouts/Split/split"));
const react_styles_1 = require("@patternfly/react-styles");
exports.Split = (_a) => {
    var { hasGutter = false, className = '', children = null, component = 'div' } = _a, props = tslib_1.__rest(_a, ["hasGutter", "className", "children", "component"]);
    const Component = component;
    return (React.createElement(Component, Object.assign({}, props, { className: react_styles_1.css(split_1.default.split, hasGutter && split_1.default.modifiers.gutter, className) }), children));
};
exports.Split.displayName = 'Split';
//# sourceMappingURL=Split.js.map
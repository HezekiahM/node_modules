"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const react_styles_1 = require("@patternfly/react-styles");
const simple_list_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/SimpleList/simple-list"));
const SimpleListGroup_1 = require("./SimpleListGroup");
exports.SimpleListContext = React.createContext({});
class SimpleList extends React.Component {
    constructor() {
        super(...arguments);
        this.state = {
            currentRef: null
        };
        this.handleCurrentUpdate = (newCurrentRef, itemProps) => {
            this.setState({ currentRef: newCurrentRef });
            const { onSelect } = this.props;
            onSelect && onSelect(newCurrentRef, itemProps);
        };
    }
    componentDidMount() {
        if (!SimpleList.hasWarnBeta && process.env.NODE_ENV !== 'production') {
            // eslint-disable-next-line no-console
            console.warn('This component is in beta and subject to change.');
            SimpleList.hasWarnBeta = true;
        }
    }
    render() {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const _a = this.props, { children, className, onSelect } = _a, props = tslib_1.__rest(_a, ["children", "className", "onSelect"]);
        let isGrouped = false;
        if (children) {
            isGrouped = React.Children.toArray(children)[0].type === SimpleListGroup_1.SimpleListGroup;
        }
        return (React.createElement(exports.SimpleListContext.Provider, { value: {
                currentRef: this.state.currentRef,
                updateCurrentRef: this.handleCurrentUpdate
            } },
            React.createElement("div", Object.assign({ className: react_styles_1.css(simple_list_1.default.simpleList, className) }, props, (isGrouped && { role: 'list' })),
                isGrouped && children,
                !isGrouped && React.createElement("ul", null, children))));
    }
}
exports.SimpleList = SimpleList;
SimpleList.displayName = 'SimpleList';
SimpleList.hasWarnBeta = false;
SimpleList.defaultProps = {
    children: null,
    className: ''
};
//# sourceMappingURL=SimpleList.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const react_styles_1 = require("@patternfly/react-styles");
const simple_list_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/SimpleList/simple-list"));
const SimpleList_1 = require("./SimpleList");
class SimpleListItem extends React.Component {
    constructor() {
        super(...arguments);
        this.ref = React.createRef();
    }
    render() {
        const _a = this.props, { children, isCurrent, className, component: Component, componentClassName, componentProps, onClick, type, href } = _a, props = tslib_1.__rest(_a, ["children", "isCurrent", "className", "component", "componentClassName", "componentProps", "onClick", "type", "href"]);
        return (React.createElement(SimpleList_1.SimpleListContext.Consumer, null, ({ currentRef, updateCurrentRef }) => {
            const isButton = Component === 'button';
            const isCurrentItem = this.ref && currentRef ? currentRef.current === this.ref.current : isCurrent;
            const additionalComponentProps = isButton
                ? {
                    type
                }
                : {
                    tabIndex: 0,
                    href
                };
            return (React.createElement("li", Object.assign({ className: react_styles_1.css(className) }, props),
                React.createElement(Component, Object.assign({ className: react_styles_1.css(simple_list_1.default.simpleListItemLink, isCurrentItem && simple_list_1.default.modifiers.current, componentClassName), onClick: (evt) => {
                        onClick(evt);
                        updateCurrentRef(this.ref, this.props);
                    }, ref: this.ref }, componentProps, additionalComponentProps), children)));
        }));
    }
}
exports.SimpleListItem = SimpleListItem;
SimpleListItem.displayName = 'SimpleListItem';
SimpleListItem.defaultProps = {
    children: null,
    className: '',
    isCurrent: false,
    component: 'button',
    componentClassName: '',
    type: 'button',
    href: '',
    onClick: () => { }
};
//# sourceMappingURL=SimpleListItem.js.map
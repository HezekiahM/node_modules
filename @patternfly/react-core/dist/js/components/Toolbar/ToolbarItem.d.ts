import * as React from 'react';
export declare enum ToolbarItemVariant {
    separator = "separator",
    'bulk-select' = "bulk-select",
    'overflow-menu' = "overflow-menu",
    pagination = "pagination",
    'search-filter' = "search-filter",
    label = "label",
    'chip-group' = "chip-group"
}
export interface ToolbarItemProps extends React.HTMLProps<HTMLDivElement> {
    /** Classes applied to root element of the data toolbar item */
    className?: string;
    /** A type modifier which modifies spacing specifically depending on the type of item */
    variant?: ToolbarItemVariant | 'bulk-select' | 'overflow-menu' | 'pagination' | 'search-filter' | 'label' | 'chip-group' | 'separator';
    /** Visibility at various breakpoints. */
    visiblity?: {
        default?: 'hidden' | 'visible';
        md?: 'hidden' | 'visible';
        lg?: 'hidden' | 'visible';
        xl?: 'hidden' | 'visible';
        '2xl'?: 'hidden' | 'visible';
    };
    /** Alignment at various breakpoints. */
    alignment?: {
        default?: 'alignRight' | 'alignLeft';
        md?: 'alignRight' | 'alignLeft';
        lg?: 'alignRight' | 'alignLeft';
        xl?: 'alignRight' | 'alignLeft';
        '2xl'?: 'alignRight' | 'alignLeft';
    };
    /** Spacers at various breakpoints. */
    spacer?: {
        default?: 'spacerNone' | 'spacerSm' | 'spacerMd' | 'spacerLg';
        md?: 'spacerNone' | 'spacerSm' | 'spacerMd' | 'spacerLg';
        lg?: 'spacerNone' | 'spacerSm' | 'spacerMd' | 'spacerLg';
        xl?: 'spacerNone' | 'spacerSm' | 'spacerMd' | 'spacerLg';
        '2xl'?: 'spacerNone' | 'spacerSm' | 'spacerMd' | 'spacerLg';
    };
    /** id for this data toolbar item */
    id?: string;
    /** Content to be rendered inside the data toolbar item */
    children?: React.ReactNode;
}
export declare const ToolbarItem: React.FunctionComponent<ToolbarItemProps>;
//# sourceMappingURL=ToolbarItem.d.ts.map
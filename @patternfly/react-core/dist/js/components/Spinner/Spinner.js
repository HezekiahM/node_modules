"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const spinner_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/Spinner/spinner"));
const react_styles_1 = require("@patternfly/react-styles");
var spinnerSize;
(function (spinnerSize) {
    spinnerSize["sm"] = "sm";
    spinnerSize["md"] = "md";
    spinnerSize["lg"] = "lg";
    spinnerSize["xl"] = "xl";
})(spinnerSize = exports.spinnerSize || (exports.spinnerSize = {}));
exports.Spinner = (_a) => {
    var { 
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    className = '', size = 'xl', 'aria-valuetext': ariaValueText = 'Loading...' } = _a, props = tslib_1.__rest(_a, ["className", "size", 'aria-valuetext']);
    return (React.createElement("span", Object.assign({ className: react_styles_1.css(spinner_1.default.spinner, spinner_1.default.modifiers[size], className), role: "progressbar", "aria-valuetext": ariaValueText }, props),
        React.createElement("span", { className: react_styles_1.css(spinner_1.default.spinnerClipper) }),
        React.createElement("span", { className: react_styles_1.css(spinner_1.default.spinnerLeadBall) }),
        React.createElement("span", { className: react_styles_1.css(spinner_1.default.spinnerTailBall) })));
};
exports.Spinner.displayName = 'Spinner';
//# sourceMappingURL=Spinner.js.map
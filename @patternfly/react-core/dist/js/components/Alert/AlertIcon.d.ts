import * as React from 'react';
export declare const variantIcons: {
    success: React.ComponentClass<import("@patternfly/react-icons/dist/js/createIcon").SVGIconProps, any>;
    danger: React.ComponentClass<import("@patternfly/react-icons/dist/js/createIcon").SVGIconProps, any>;
    warning: React.ComponentClass<import("@patternfly/react-icons/dist/js/createIcon").SVGIconProps, any>;
    info: React.ComponentClass<import("@patternfly/react-icons/dist/js/createIcon").SVGIconProps, any>;
    default: React.ComponentClass<import("@patternfly/react-icons/dist/js/createIcon").SVGIconProps, any>;
};
export interface AlertIconProps extends React.HTMLProps<HTMLDivElement> {
    /** variant */
    variant: 'success' | 'danger' | 'warning' | 'info' | 'default';
    /** className */
    className?: string;
}
export declare const AlertIcon: ({ variant, className, ...props }: AlertIconProps) => JSX.Element;
//# sourceMappingURL=AlertIcon.d.ts.map
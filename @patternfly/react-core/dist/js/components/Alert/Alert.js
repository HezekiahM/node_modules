"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const react_1 = require("react");
const react_styles_1 = require("@patternfly/react-styles");
const alert_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/Alert/alert"));
const accessibility_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/utilities/Accessibility/accessibility"));
const AlertIcon_1 = require("./AlertIcon");
const helpers_1 = require("../../helpers");
const AlertContext_1 = require("./AlertContext");
var AlertVariant;
(function (AlertVariant) {
    AlertVariant["success"] = "success";
    AlertVariant["danger"] = "danger";
    AlertVariant["warning"] = "warning";
    AlertVariant["info"] = "info";
    AlertVariant["default"] = "default";
})(AlertVariant = exports.AlertVariant || (exports.AlertVariant = {}));
exports.Alert = (_a) => {
    var { variant = AlertVariant.default, isInline = false, isLiveRegion = false, variantLabel = `${helpers_1.capitalize(variant)} alert:`, 'aria-label': ariaLabel = `${helpers_1.capitalize(variant)} Alert`, actionClose, actionLinks, title, children = '', className = '', ouiaId, ouiaSafe = true, timeout = false } = _a, props = tslib_1.__rest(_a, ["variant", "isInline", "isLiveRegion", "variantLabel", 'aria-label', "actionClose", "actionLinks", "title", "children", "className", "ouiaId", "ouiaSafe", "timeout"]);
    const getHeadingContent = (React.createElement(React.Fragment, null,
        React.createElement("span", { className: react_styles_1.css(accessibility_1.default.screenReader) }, variantLabel),
        title));
    const [disableAlert, setDisableAlert] = react_1.useState(false);
    const customClassName = react_styles_1.css(alert_1.default.alert, isInline && alert_1.default.modifiers.inline, variant !== AlertVariant.default && alert_1.default.modifiers[variant], className);
    if (disableAlert === false && timeout && timeout !== 0) {
        setTimeout(() => {
            setDisableAlert(true);
        }, timeout === true ? 8000 : timeout);
    }
    if (disableAlert === false) {
        return (React.createElement("div", Object.assign({}, props, { className: customClassName, "aria-label": ariaLabel }, helpers_1.getOUIAProps(exports.Alert.displayName, ouiaId, ouiaSafe), (isLiveRegion && {
            'aria-live': 'polite',
            'aria-atomic': 'false'
        })),
            React.createElement(AlertIcon_1.AlertIcon, { variant: variant }),
            React.createElement("h4", { className: react_styles_1.css(alert_1.default.alertTitle) }, getHeadingContent),
            actionClose && (React.createElement(AlertContext_1.AlertContext.Provider, { value: { title, variantLabel } },
                React.createElement("div", { className: react_styles_1.css(alert_1.default.alertAction) }, actionClose))),
            children && React.createElement("div", { className: react_styles_1.css(alert_1.default.alertDescription) }, children),
            actionLinks && React.createElement("div", { className: react_styles_1.css(alert_1.default.alertActionGroup) }, actionLinks)));
    }
    else {
        return null;
    }
};
exports.Alert.displayName = 'Alert';
//# sourceMappingURL=Alert.js.map
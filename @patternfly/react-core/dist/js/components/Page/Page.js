"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const page_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/Page/page"));
const react_styles_1 = require("@patternfly/react-styles");
const global_breakpoint_xl_1 = tslib_1.__importDefault(require("@patternfly/react-tokens/dist/js/global_breakpoint_xl"));
const util_1 = require("../../helpers/util");
var PageLayouts;
(function (PageLayouts) {
    PageLayouts["vertical"] = "vertical";
    PageLayouts["horizontal"] = "horizontal";
})(PageLayouts = exports.PageLayouts || (exports.PageLayouts = {}));
const PageContext = React.createContext({});
exports.PageContextProvider = PageContext.Provider;
exports.PageContextConsumer = PageContext.Consumer;
class Page extends React.Component {
    constructor(props) {
        super(props);
        this.handleResize = () => {
            const { onPageResize } = this.props;
            const windowSize = window.innerWidth;
            // eslint-disable-next-line radix
            const mobileView = windowSize < Number.parseInt(global_breakpoint_xl_1.default.value, 10);
            if (onPageResize) {
                onPageResize({ mobileView, windowSize });
            }
            this.setState({ mobileView });
        };
        this.onNavToggleMobile = () => {
            this.setState(prevState => ({
                mobileIsNavOpen: !prevState.mobileIsNavOpen
            }));
        };
        this.onNavToggleDesktop = () => {
            this.setState(prevState => ({
                desktopIsNavOpen: !prevState.desktopIsNavOpen
            }));
        };
        const { isManagedSidebar, defaultManagedSidebarIsOpen } = props;
        const managedSidebarOpen = !isManagedSidebar ? true : defaultManagedSidebarIsOpen;
        this.state = {
            desktopIsNavOpen: managedSidebarOpen,
            mobileIsNavOpen: false,
            mobileView: false
        };
    }
    componentDidMount() {
        const { isManagedSidebar, onPageResize } = this.props;
        if (isManagedSidebar || onPageResize) {
            window.addEventListener('resize', util_1.debounce(this.handleResize, 250));
            // Initial check if should be shown
            this.handleResize();
        }
    }
    componentWillUnmount() {
        const { isManagedSidebar, onPageResize } = this.props;
        if (isManagedSidebar || onPageResize) {
            window.removeEventListener('resize', util_1.debounce(this.handleResize, 250));
        }
    }
    render() {
        const _a = this.props, { breadcrumb, className, children, header, sidebar, skipToContent, role, mainContainerId, isManagedSidebar, 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        defaultManagedSidebarIsOpen, 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        onPageResize, mainAriaLabel, mainTabIndex } = _a, rest = tslib_1.__rest(_a, ["breadcrumb", "className", "children", "header", "sidebar", "skipToContent", "role", "mainContainerId", "isManagedSidebar", "defaultManagedSidebarIsOpen", "onPageResize", "mainAriaLabel", "mainTabIndex"]);
        const { mobileView, mobileIsNavOpen, desktopIsNavOpen } = this.state;
        const context = {
            isManagedSidebar,
            onNavToggle: mobileView ? this.onNavToggleMobile : this.onNavToggleDesktop,
            isNavOpen: mobileView ? mobileIsNavOpen : desktopIsNavOpen
        };
        return (React.createElement(exports.PageContextProvider, { value: context },
            React.createElement("div", Object.assign({}, rest, { className: react_styles_1.css(page_1.default.page, className) }),
                skipToContent,
                header,
                sidebar,
                React.createElement("main", { role: role, id: mainContainerId, className: react_styles_1.css(page_1.default.pageMain), tabIndex: mainTabIndex, "aria-label": mainAriaLabel },
                    breadcrumb && React.createElement("section", { className: react_styles_1.css(page_1.default.pageMainBreadcrumb) }, breadcrumb),
                    children))));
    }
}
exports.Page = Page;
Page.displayName = 'Page';
Page.defaultProps = {
    isManagedSidebar: false,
    defaultManagedSidebarIsOpen: true,
    onPageResize: () => null,
    mainTabIndex: -1
};
//# sourceMappingURL=Page.js.map
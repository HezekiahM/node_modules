"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const page_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/Page/page"));
const react_styles_1 = require("@patternfly/react-styles");
const util_1 = require("../../helpers/util");
var PageSectionVariants;
(function (PageSectionVariants) {
    PageSectionVariants["default"] = "default";
    PageSectionVariants["light"] = "light";
    PageSectionVariants["dark"] = "dark";
    PageSectionVariants["darker"] = "darker";
})(PageSectionVariants = exports.PageSectionVariants || (exports.PageSectionVariants = {}));
var PageSectionTypes;
(function (PageSectionTypes) {
    PageSectionTypes["default"] = "default";
    PageSectionTypes["nav"] = "nav";
})(PageSectionTypes = exports.PageSectionTypes || (exports.PageSectionTypes = {}));
const variantType = {
    [PageSectionTypes.default]: page_1.default.pageMainSection,
    [PageSectionTypes.nav]: page_1.default.pageMainNav
};
const variantStyle = {
    [PageSectionVariants.default]: '',
    [PageSectionVariants.light]: page_1.default.modifiers.light,
    [PageSectionVariants.dark]: page_1.default.modifiers.dark_200,
    [PageSectionVariants.darker]: page_1.default.modifiers.dark_100
};
exports.PageSection = (_a) => {
    var { className = '', children, variant = 'default', type = 'default', padding, isFilled } = _a, props = tslib_1.__rest(_a, ["className", "children", "variant", "type", "padding", "isFilled"]);
    return (React.createElement("section", Object.assign({}, props, { className: react_styles_1.css(variantType[type], util_1.formatBreakpointMods(padding, page_1.default), variantStyle[variant], isFilled === false && page_1.default.modifiers.noFill, isFilled === true && page_1.default.modifiers.fill, className) }), children));
};
exports.PageSection.displayName = 'PageSection';
//# sourceMappingURL=PageSection.js.map
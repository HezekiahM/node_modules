"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const react_styles_1 = require("@patternfly/react-styles");
const data_list_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/DataList/data-list"));
exports.DataListCheck = (_a) => {
    var { className = '', 
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    onChange = (checked, event) => { }, isValid = true, isDisabled = false, isChecked = null, checked = null } = _a, props = tslib_1.__rest(_a, ["className", "onChange", "isValid", "isDisabled", "isChecked", "checked"]);
    return (React.createElement("div", { className: react_styles_1.css(data_list_1.default.dataListItemControl, className) },
        React.createElement("div", { className: react_styles_1.css('pf-c-data-list__check') },
            React.createElement("input", Object.assign({}, props, { type: "checkbox", onChange: event => onChange(event.currentTarget.checked, event), "aria-invalid": !isValid, disabled: isDisabled, checked: isChecked || checked })))));
};
exports.DataListCheck.displayName = 'DataListCheck';
//# sourceMappingURL=DataListCheck.js.map
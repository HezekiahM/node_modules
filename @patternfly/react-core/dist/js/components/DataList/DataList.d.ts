import * as React from 'react';
export interface DataListProps extends React.HTMLProps<HTMLUListElement> {
    /** Content rendered inside the DataList list */
    children?: React.ReactNode;
    /** Additional classes added to the DataList list */
    className?: string;
    /** Adds accessible text to the DataList list */
    'aria-label': string;
    /** Optional callback to make DataList selectable, fired when DataListItem selected */
    onSelectDataListItem?: (id: string) => void;
    /** Id of DataList item currently selected */
    selectedDataListItemId?: string;
    /** Flag indicating if DataList should have compact styling */
    isCompact?: boolean;
}
interface DataListContextProps {
    isSelectable: boolean;
    selectedDataListItemId: string;
    updateSelectedDataListItem: (id: string) => void;
}
export declare const DataListContext: React.Context<Partial<DataListContextProps>>;
export declare const DataList: React.FunctionComponent<DataListProps>;
export {};
//# sourceMappingURL=DataList.d.ts.map
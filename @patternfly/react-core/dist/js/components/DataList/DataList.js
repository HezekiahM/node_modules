"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const react_styles_1 = require("@patternfly/react-styles");
const data_list_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/DataList/data-list"));
exports.DataListContext = React.createContext({
    isSelectable: false
});
exports.DataList = (_a) => {
    var { children = null, className = '', 'aria-label': ariaLabel, selectedDataListItemId = '', onSelectDataListItem, isCompact = false } = _a, props = tslib_1.__rest(_a, ["children", "className", 'aria-label', "selectedDataListItemId", "onSelectDataListItem", "isCompact"]);
    const isSelectable = onSelectDataListItem !== undefined;
    const updateSelectedDataListItem = (id) => {
        onSelectDataListItem(id);
    };
    return (React.createElement(exports.DataListContext.Provider, { value: {
            isSelectable,
            selectedDataListItemId,
            updateSelectedDataListItem
        } },
        React.createElement("ul", Object.assign({ className: react_styles_1.css(data_list_1.default.dataList, isCompact && data_list_1.default.modifiers.compact, className), "aria-label": ariaLabel }, props), children)));
};
exports.DataList.displayName = 'DataList';
//# sourceMappingURL=DataList.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const react_styles_1 = require("@patternfly/react-styles");
const data_list_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/DataList/data-list"));
const DataList_1 = require("./DataList");
const Select_1 = require("../Select");
exports.DataListItem = (_a) => {
    var { isExpanded = false, className = '', id = '', 'aria-labelledby': ariaLabelledBy, children } = _a, props = tslib_1.__rest(_a, ["isExpanded", "className", "id", 'aria-labelledby', "children"]);
    return (React.createElement(DataList_1.DataListContext.Consumer, null, ({ isSelectable, selectedDataListItemId, updateSelectedDataListItem }) => {
        const selectDataListItem = (event) => {
            let target = event.target;
            while (event.currentTarget !== target) {
                if (('onclick' in target && target.onclick) ||
                    target.parentNode.classList.contains(data_list_1.default.dataListItemAction) ||
                    target.parentNode.classList.contains(data_list_1.default.dataListItemControl)) {
                    // check other event handlers are not present.
                    return;
                }
                else {
                    target = target.parentNode;
                }
            }
            updateSelectedDataListItem(id);
        };
        const onKeyDown = (event) => {
            if (event.key === Select_1.KeyTypes.Enter) {
                updateSelectedDataListItem(id);
            }
        };
        return (React.createElement("li", Object.assign({ id: id, className: react_styles_1.css(data_list_1.default.dataListItem, isExpanded && data_list_1.default.modifiers.expanded, isSelectable && data_list_1.default.modifiers.selectable, selectedDataListItemId && selectedDataListItemId === id && data_list_1.default.modifiers.selected, className), "aria-labelledby": ariaLabelledBy }, (isSelectable && { tabIndex: 0, onClick: selectDataListItem, onKeyDown }), (isSelectable && selectedDataListItemId === id && { 'aria-selected': true }), props), React.Children.map(children, child => React.isValidElement(child) &&
            React.cloneElement(child, {
                rowid: ariaLabelledBy
            }))));
    }));
};
exports.DataListItem.displayName = 'DataListItem';
//# sourceMappingURL=DataListItem.js.map
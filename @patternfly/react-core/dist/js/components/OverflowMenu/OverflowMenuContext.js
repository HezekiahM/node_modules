"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
exports.OverflowMenuContext = React.createContext({
    isBelowBreakpoint: false
});
//# sourceMappingURL=OverflowMenuContext.js.map
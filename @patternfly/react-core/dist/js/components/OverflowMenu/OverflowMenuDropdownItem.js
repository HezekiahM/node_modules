"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const Dropdown_1 = require("../Dropdown");
const OverflowMenuContext_1 = require("./OverflowMenuContext");
exports.OverflowMenuDropdownItem = ({ children, isShared = false, index }) => (React.createElement(OverflowMenuContext_1.OverflowMenuContext.Consumer, null, value => (!isShared || value.isBelowBreakpoint) && (React.createElement(Dropdown_1.DropdownItem, { component: "button", index: index }, children))));
exports.OverflowMenuDropdownItem.displayName = 'OverflowMenuDropdownItem';
//# sourceMappingURL=OverflowMenuDropdownItem.js.map
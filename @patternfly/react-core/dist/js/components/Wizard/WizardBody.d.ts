import * as React from 'react';
export interface WizardBodyProps {
    /** Anything that can be rendered in the Wizard body */
    children: any;
    /** Set to true to remove the default body padding */
    hasNoBodyPadding: boolean;
}
export declare const WizardBody: React.FunctionComponent<WizardBodyProps>;
//# sourceMappingURL=WizardBody.d.ts.map
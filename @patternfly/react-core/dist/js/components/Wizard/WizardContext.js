"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const WizardContext = React.createContext({
    goToStepById: () => null,
    goToStepByName: () => null,
    onNext: () => null,
    onBack: () => null,
    onClose: () => null,
    activeStep: { name: null }
});
exports.WizardContextProvider = WizardContext.Provider;
exports.WizardContextConsumer = WizardContext.Consumer;
//# sourceMappingURL=WizardContext.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const react_styles_1 = require("@patternfly/react-styles");
const wizard_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/Wizard/wizard"));
exports.WizardNavItem = ({ children = null, content = '', isCurrent = false, isDisabled = false, step, onNavItemClick = () => undefined, navItemComponent = 'a' }) => {
    const NavItemComponent = navItemComponent;
    return (React.createElement("li", { className: react_styles_1.css(wizard_1.default.wizardNavItem) },
        React.createElement(NavItemComponent, { "aria-current": isCurrent && !children ? 'page' : false, onClick: () => onNavItemClick(step), className: react_styles_1.css(wizard_1.default.wizardNavLink, isCurrent && 'pf-m-current', isDisabled && 'pf-m-disabled'), "aria-disabled": isDisabled ? true : false, tabIndex: isDisabled ? -1 : undefined }, content),
        children));
};
exports.WizardNavItem.displayName = 'WizardNavItem';
//# sourceMappingURL=WizardNavItem.js.map
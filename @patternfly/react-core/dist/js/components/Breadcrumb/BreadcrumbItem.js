"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const angle_right_icon_1 = tslib_1.__importDefault(require("@patternfly/react-icons/dist/js/icons/angle-right-icon"));
const breadcrumb_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/Breadcrumb/breadcrumb"));
const react_styles_1 = require("@patternfly/react-styles");
exports.BreadcrumbItem = (_a) => {
    var { children = null, className = '', to = null, isActive = false, showDivider, target = null, component = 'a' } = _a, props = tslib_1.__rest(_a, ["children", "className", "to", "isActive", "showDivider", "target", "component"]);
    const Component = component;
    return (React.createElement("li", Object.assign({}, props, { className: react_styles_1.css(breadcrumb_1.default.breadcrumbItem, className) }),
        showDivider && (React.createElement("span", { className: breadcrumb_1.default.breadcrumbItemDivider },
            React.createElement(angle_right_icon_1.default, null))),
        to && (React.createElement(Component, { href: to, target: target, className: react_styles_1.css(breadcrumb_1.default.breadcrumbLink, isActive && breadcrumb_1.default.modifiers.current), "aria-current": isActive ? 'page' : undefined }, children)),
        !to && React.createElement(React.Fragment, null, children)));
};
exports.BreadcrumbItem.displayName = 'BreadcrumbItem';
//# sourceMappingURL=BreadcrumbItem.js.map
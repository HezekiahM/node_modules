import * as React from 'react';
export declare const ApplicationLauncherContext: React.Context<{
    onFavorite: (itemId: string, isFavorite: boolean) => void;
}>;
//# sourceMappingURL=ApplicationLauncherContext.d.ts.map
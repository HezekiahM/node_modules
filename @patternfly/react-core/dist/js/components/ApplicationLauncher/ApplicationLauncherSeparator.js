"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const Dropdown_1 = require("../Dropdown");
exports.ApplicationLauncherSeparator = (_a) => {
    var { 
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    children } = _a, props = tslib_1.__rest(_a, ["children"]);
    return React.createElement(Dropdown_1.DropdownSeparator, Object.assign({}, props));
};
exports.ApplicationLauncherSeparator.displayName = 'ApplicationLauncherSeparator';
//# sourceMappingURL=ApplicationLauncherSeparator.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const react_styles_1 = require("@patternfly/react-styles");
const modal_box_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/ModalBox/modal-box"));
exports.ModalBox = (_a) => {
    var { children, className = '', variant = 'default', 'aria-labelledby': ariaLabelledby, 'aria-label': ariaLabel = '', 'aria-describedby': ariaDescribedby } = _a, props = tslib_1.__rest(_a, ["children", "className", "variant", 'aria-labelledby', 'aria-label', 'aria-describedby']);
    return (React.createElement("div", Object.assign({}, props, { role: "dialog", "aria-label": ariaLabel || null, "aria-labelledby": ariaLabelledby || null, "aria-describedby": ariaDescribedby, "aria-modal": "true", className: react_styles_1.css(modal_box_1.default.modalBox, className, variant === 'large' && modal_box_1.default.modifiers.lg, variant === 'small' && modal_box_1.default.modifiers.sm) }), children));
};
exports.ModalBox.displayName = 'ModalBox';
//# sourceMappingURL=ModalBox.js.map
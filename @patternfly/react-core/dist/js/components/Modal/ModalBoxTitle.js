"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const modal_box_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/ModalBox/modal-box"));
const react_styles_1 = require("@patternfly/react-styles");
const Tooltip_1 = require("../Tooltip");
exports.ModalBoxTitle = (_a) => {
    var { className = '', id, title } = _a, props = tslib_1.__rest(_a, ["className", "id", "title"]);
    const [isTooltipVisible, setIsTooltipVisible] = React.useState(false);
    const h1 = React.useRef();
    React.useLayoutEffect(() => {
        setIsTooltipVisible(h1.current && h1.current.offsetWidth < h1.current.scrollWidth);
    }, []);
    return isTooltipVisible ? (React.createElement(Tooltip_1.Tooltip, { content: title },
        React.createElement("h1", Object.assign({ id: id, ref: h1, className: react_styles_1.css(modal_box_1.default.modalBoxTitle, className) }, props), title))) : (React.createElement("h1", Object.assign({ id: id, ref: h1, className: react_styles_1.css(modal_box_1.default.modalBoxTitle, className) }, props), title));
};
exports.ModalBoxTitle.displayName = 'ModalBoxTitle';
//# sourceMappingURL=ModalBoxTitle.js.map
import * as React from 'react';
export interface ModalBoxHeaderProps {
    /** Content rendered inside the Header */
    children?: React.ReactNode;
    /** Additional classes added to the button */
    className?: string;
}
export declare const ModalBoxHeader: React.FunctionComponent<ModalBoxHeaderProps>;
//# sourceMappingURL=ModalBoxHeader.d.ts.map
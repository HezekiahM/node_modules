import * as React from 'react';
import { PickOptional } from '../../helpers';
import { OUIAProps } from '../../helpers';
export interface ModalProps extends React.HTMLProps<HTMLDivElement>, OUIAProps {
    /** Content rendered inside the Modal. */
    children: React.ReactNode;
    /** Additional classes added to the Modal */
    className?: string;
    /** Flag to show the modal */
    isOpen?: boolean;
    /** Complex header (more than just text), supersedes title for header content */
    header?: React.ReactNode;
    /** Simple text content of the Modal Header, also used for aria-label on the body */
    title?: string;
    /** Id to use for Modal Box label */
    'aria-labelledby'?: string | null;
    /** Accessible descriptor of modal */
    'aria-label'?: string;
    /** Id to use for Modal Box descriptor */
    'aria-describedby'?: string;
    /** Flag to show the close button in the header area of the modal */
    showClose?: boolean;
    /** Custom footer */
    footer?: React.ReactNode;
    /** Action buttons to add to the standard Modal Footer, ignored if `footer` is given */
    actions?: any;
    /** A callback for when the close button is clicked */
    onClose?: () => void;
    /** Default width of the Modal. */
    width?: number | string;
    /** The parent container to append the modal to. Defaults to document.body */
    appendTo?: HTMLElement | (() => HTMLElement);
    /** Flag to disable focus trap */
    disableFocusTrap?: boolean;
    /** Description of the modal */
    description?: React.ReactNode;
    /** Variant of the modal */
    variant?: 'small' | 'large' | 'default';
    /** Flag indicating if modal content should be placed in a modal box body wrapper */
    hasNoBodyWrapper?: boolean;
    /** An ID to use for the ModalBox container */
    id?: string;
    /** Modal handles pressing of the Escape key and closes the modal. If you want to handle this yourself you can use this callback function */
    onEscapePress?: (event: KeyboardEvent) => void;
}
export declare enum ModalVariant {
    small = "small",
    large = "large",
    default = "default"
}
interface ModalState {
    container: HTMLElement;
}
export declare class Modal extends React.Component<ModalProps, ModalState> {
    static displayName: string;
    static currentId: number;
    boxId: string;
    labelId: string;
    descriptorId: string;
    static defaultProps: PickOptional<ModalProps>;
    constructor(props: ModalProps);
    handleEscKeyClick: (event: KeyboardEvent) => void;
    getElement: (appendTo: HTMLElement | (() => HTMLElement)) => HTMLElement;
    toggleSiblingsFromScreenReaders: (hide: boolean) => void;
    isEmpty: (value: string) => boolean;
    componentDidMount(): void;
    componentDidUpdate(): void;
    componentWillUnmount(): void;
    render(): React.ReactPortal;
}
export {};
//# sourceMappingURL=Modal.d.ts.map
import * as React from 'react';
export interface ModalBoxFooterProps {
    /** Content rendered inside the Footer */
    children?: React.ReactNode;
    /** Additional classes added to the Footer */
    className?: string;
}
export declare const ModalBoxFooter: React.FunctionComponent<ModalBoxFooterProps>;
//# sourceMappingURL=ModalBoxFooter.d.ts.map
import * as React from 'react';
export interface ModalBoxTitleProps {
    /** Content rendered inside the modal box header title. */
    title: React.ReactNode;
    /** Additional classes added to the modal box header title. */
    className?: string;
    /** id of the modal box header title. */
    id: string;
}
export declare const ModalBoxTitle: React.FunctionComponent<ModalBoxTitleProps>;
//# sourceMappingURL=ModalBoxTitle.d.ts.map
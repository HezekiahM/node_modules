"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const context_selector_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/ContextSelector/context-selector"));
const react_styles_1 = require("@patternfly/react-styles");
const contextSelectorConstants_1 = require("./contextSelectorConstants");
class ContextSelectorItem extends React.Component {
    constructor() {
        super(...arguments);
        this.ref = React.createRef();
    }
    componentDidMount() {
        /* eslint-disable-next-line */
        this.props.sendRef(this.props.index, this.ref.current);
    }
    render() {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const _a = this.props, { className, children, onClick, isDisabled, index, sendRef } = _a, props = tslib_1.__rest(_a, ["className", "children", "onClick", "isDisabled", "index", "sendRef"]);
        return (React.createElement(contextSelectorConstants_1.ContextSelectorContext.Consumer, null, ({ onSelect }) => (React.createElement("li", { role: "none" },
            React.createElement("button", Object.assign({ className: react_styles_1.css(context_selector_1.default.contextSelectorMenuListItem, className), ref: this.ref, onClick: event => {
                    if (!isDisabled) {
                        onClick(event);
                        onSelect(event, children);
                    }
                }, disabled: isDisabled }, props), children)))));
    }
}
exports.ContextSelectorItem = ContextSelectorItem;
ContextSelectorItem.displayName = 'ContextSelectorItem';
ContextSelectorItem.defaultProps = {
    children: null,
    className: '',
    isDisabled: false,
    onClick: () => undefined,
    index: undefined,
    sendRef: () => { }
};
//# sourceMappingURL=ContextSelectorItem.js.map
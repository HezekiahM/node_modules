"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const form_control_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/FormControl/form-control"));
const react_styles_1 = require("@patternfly/react-styles");
const helpers_1 = require("../../helpers");
var TextAreResizeOrientation;
(function (TextAreResizeOrientation) {
    TextAreResizeOrientation["horizontal"] = "horizontal";
    TextAreResizeOrientation["vertical"] = "vertical";
    TextAreResizeOrientation["both"] = "both";
})(TextAreResizeOrientation = exports.TextAreResizeOrientation || (exports.TextAreResizeOrientation = {}));
class TextArea extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = (event) => {
            if (this.props.onChange) {
                this.props.onChange(event.currentTarget.value, event);
            }
        };
        if (!props.id && !props['aria-label']) {
            // eslint-disable-next-line no-console
            console.error('TextArea: TextArea requires either an id or aria-label to be specified');
        }
    }
    render() {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const _a = this.props, { className, value, onChange, validated, isRequired, resizeOrientation } = _a, props = tslib_1.__rest(_a, ["className", "value", "onChange", "validated", "isRequired", "resizeOrientation"]);
        const orientation = `resize${helpers_1.capitalize(resizeOrientation)}`;
        return (React.createElement("textarea", Object.assign({ className: react_styles_1.css(form_control_1.default.formControl, className, resizeOrientation !== TextAreResizeOrientation.both && form_control_1.default.modifiers[orientation], validated === helpers_1.ValidatedOptions.success && form_control_1.default.modifiers.success, validated === helpers_1.ValidatedOptions.warning && form_control_1.default.modifiers.warning), onChange: this.handleChange }, (typeof this.props.defaultValue !== 'string' && { value }), { "aria-invalid": validated === helpers_1.ValidatedOptions.error, required: isRequired }, props)));
    }
}
exports.TextArea = TextArea;
TextArea.displayName = 'TextArea';
TextArea.defaultProps = {
    className: '',
    isRequired: false,
    validated: 'default',
    resizeOrientation: 'both',
    'aria-label': null
};
//# sourceMappingURL=TextArea.js.map
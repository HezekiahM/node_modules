"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
exports.TabButton = (_a) => {
    var { children, 
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    tabContentRef } = _a, props = tslib_1.__rest(_a, ["children", "tabContentRef"]);
    const Component = (props.href ? 'a' : 'button');
    return React.createElement(Component, Object.assign({}, props), children);
};
exports.TabButton.displayName = 'TabButton';
//# sourceMappingURL=TabButton.js.map
import * as React from 'react';
export interface LabelProps extends React.HTMLProps<HTMLSpanElement> {
    /** Content rendered inside the label. */
    children: React.ReactNode;
    /** Additional classes added to the label. */
    className?: string;
    /** Color of the label. */
    color?: 'blue' | 'cyan' | 'green' | 'orange' | 'purple' | 'red' | 'grey';
    /** Variant of the label. */
    variant?: 'outline' | 'filled';
    /** Icon added to the left of the label text. */
    icon?: React.ReactNode;
    /** Close click callback for removable labels. If present, label will have a close button. */
    onClose?: (event: React.MouseEvent) => void;
    /** Node for custom close button. */
    closeBtn?: React.ReactNode;
    /** Additional properties for the default close button. */
    closeBtnProps?: any;
    /** Href for a label that is a link. If present, the label will change to an anchor element. */
    href?: string;
}
export declare const Label: React.FunctionComponent<LabelProps>;
//# sourceMappingURL=Label.d.ts.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const label_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/Label/label"));
const Button_1 = require("../Button");
const react_styles_1 = require("@patternfly/react-styles");
const times_icon_1 = tslib_1.__importDefault(require("@patternfly/react-icons/dist/js/icons/times-icon"));
const colorStyles = {
    blue: label_1.default.modifiers.blue,
    cyan: label_1.default.modifiers.cyan,
    green: label_1.default.modifiers.green,
    orange: label_1.default.modifiers.orange,
    purple: label_1.default.modifiers.purple,
    red: label_1.default.modifiers.red,
    grey: ''
};
exports.Label = (_a) => {
    var { children, className = '', color = 'grey', variant = 'filled', icon, onClose, closeBtn, closeBtnProps, href } = _a, props = tslib_1.__rest(_a, ["children", "className", "color", "variant", "icon", "onClose", "closeBtn", "closeBtnProps", "href"]);
    const Component = href ? 'a' : 'span';
    const button = closeBtn ? (closeBtn) : (React.createElement(Button_1.Button, Object.assign({ type: "button", variant: "plain", onClick: onClose }, Object.assign({ 'aria-label': 'label-close-button' }, closeBtnProps)),
        React.createElement(times_icon_1.default, null)));
    return (React.createElement("span", Object.assign({}, props, { className: react_styles_1.css(label_1.default.label, colorStyles[color], variant === 'outline' && label_1.default.modifiers.outline, className) }),
        React.createElement(Component, Object.assign({ className: react_styles_1.css(label_1.default.labelContent) }, (href && { href })),
            icon && React.createElement("span", { className: react_styles_1.css(label_1.default.labelIcon) }, icon),
            children),
        onClose && button));
};
exports.Label.displayName = 'Label';
//# sourceMappingURL=Label.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
exports.CardHeaderMain = (_a) => {
    var { children = null, className = '' } = _a, props = tslib_1.__rest(_a, ["children", "className"]);
    return (React.createElement("div", Object.assign({ className: className }, props), children));
};
exports.CardHeaderMain.displayName = 'CardHeaderMain';
//# sourceMappingURL=CardHeaderMain.js.map
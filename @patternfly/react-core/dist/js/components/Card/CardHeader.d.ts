import * as React from 'react';
export interface CardHeaderProps extends React.HTMLProps<HTMLDivElement> {
    /** Content rendered inside the CardHeader */
    children?: React.ReactNode;
    /** Additional classes added to the CardHeader */
    className?: string;
}
export declare const CardHeader: React.FunctionComponent<CardHeaderProps>;
//# sourceMappingURL=CardHeader.d.ts.map
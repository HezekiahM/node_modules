"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const card_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/Card/card"));
const react_styles_1 = require("@patternfly/react-styles");
const helpers_1 = require("../../helpers");
exports.Card = (_a) => {
    var { children = null, className = '', component = 'article', isHoverable = false, isCompact = false, isSelectable = false, isSelected = false, isFlat = false, ouiaId, ouiaSafe = true } = _a, props = tslib_1.__rest(_a, ["children", "className", "component", "isHoverable", "isCompact", "isSelectable", "isSelected", "isFlat", "ouiaId", "ouiaSafe"]);
    const Component = component;
    return (React.createElement(Component, Object.assign({ className: react_styles_1.css(card_1.default.card, isHoverable && card_1.default.modifiers.hoverable, isCompact && card_1.default.modifiers.compact, isSelectable && card_1.default.modifiers.selectable, isSelected && isSelectable && card_1.default.modifiers.selected, isFlat && card_1.default.modifiers.flat, className), tabIndex: isSelectable ? '0' : undefined }, props, helpers_1.getOUIAProps(exports.Card.displayName, ouiaId, ouiaSafe)), children));
};
exports.Card.displayName = 'Card';
//# sourceMappingURL=Card.js.map
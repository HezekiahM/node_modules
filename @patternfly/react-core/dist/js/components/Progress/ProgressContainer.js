"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const progress_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/Progress/progress"));
const react_styles_1 = require("@patternfly/react-styles");
const check_circle_icon_1 = tslib_1.__importDefault(require("@patternfly/react-icons/dist/js/icons/check-circle-icon"));
const times_circle_icon_1 = tslib_1.__importDefault(require("@patternfly/react-icons/dist/js/icons/times-circle-icon"));
const ProgressBar_1 = require("./ProgressBar");
var ProgressMeasureLocation;
(function (ProgressMeasureLocation) {
    ProgressMeasureLocation["outside"] = "outside";
    ProgressMeasureLocation["inside"] = "inside";
    ProgressMeasureLocation["top"] = "top";
    ProgressMeasureLocation["none"] = "none";
})(ProgressMeasureLocation = exports.ProgressMeasureLocation || (exports.ProgressMeasureLocation = {}));
var ProgressVariant;
(function (ProgressVariant) {
    ProgressVariant["danger"] = "danger";
    ProgressVariant["success"] = "success";
})(ProgressVariant = exports.ProgressVariant || (exports.ProgressVariant = {}));
const variantToIcon = {
    danger: times_circle_icon_1.default,
    success: check_circle_icon_1.default
};
exports.ProgressContainer = ({ progressBarAriaProps, value, title = '', parentId, label = null, variant = null, measureLocation = ProgressMeasureLocation.top }) => {
    const StatusIcon = variantToIcon.hasOwnProperty(variant) && variantToIcon[variant];
    return (React.createElement(React.Fragment, null,
        React.createElement("div", { className: react_styles_1.css(progress_1.default.progressDescription), id: `${parentId}-description`, "aria-hidden": "true" }, title),
        React.createElement("div", { className: react_styles_1.css(progress_1.default.progressStatus), "aria-hidden": "true" },
            (measureLocation === ProgressMeasureLocation.top || measureLocation === ProgressMeasureLocation.outside) && (React.createElement("span", { className: react_styles_1.css(progress_1.default.progressMeasure) }, label || `${value}%`)),
            variantToIcon.hasOwnProperty(variant) && (React.createElement("span", { className: react_styles_1.css(progress_1.default.progressStatusIcon) },
                React.createElement(StatusIcon, null)))),
        React.createElement(ProgressBar_1.ProgressBar, { role: "progressbar", progressBarAriaProps: progressBarAriaProps, value: value }, measureLocation === ProgressMeasureLocation.inside && `${value}%`)));
};
exports.ProgressContainer.displayName = 'ProgressContainer';
//# sourceMappingURL=ProgressContainer.js.map
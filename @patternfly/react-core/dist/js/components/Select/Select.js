"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const select_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/Select/select"));
const badge_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/Badge/badge"));
const form_control_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/FormControl/form-control"));
const button_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/Button/button"));
const react_styles_1 = require("@patternfly/react-styles");
const times_circle_icon_1 = tslib_1.__importDefault(require("@patternfly/react-icons/dist/js/icons/times-circle-icon"));
const SelectMenu_1 = require("./SelectMenu");
const SelectOption_1 = require("./SelectOption");
const SelectToggle_1 = require("./SelectToggle");
const selectConstants_1 = require("./selectConstants");
const ChipGroup_1 = require("../ChipGroup");
const helpers_1 = require("../../helpers");
const Divider_1 = require("../Divider");
const Popper_1 = require("../../helpers/Popper/Popper");
// seed for the aria-labelledby ID
let currentId = 0;
class Select extends React.Component {
    constructor() {
        super(...arguments);
        this.parentRef = React.createRef();
        this.menuComponentRef = React.createRef();
        this.filterRef = React.createRef();
        this.clearRef = React.createRef();
        this.refCollection = [];
        this.state = {
            openedOnEnter: false,
            typeaheadInputValue: null,
            typeaheadActiveChild: null,
            typeaheadFilteredChildren: React.Children.toArray(this.props.children),
            typeaheadCurrIndex: -1,
            creatableValue: ''
        };
        this.componentDidUpdate = (prevProps, prevState) => {
            if (this.props.hasInlineFilter) {
                this.refCollection[0] = this.filterRef.current;
            }
            if (!prevState.openedOnEnter && this.state.openedOnEnter && !this.props.customContent) {
                this.refCollection[0].focus();
            }
            if (prevProps.children !== this.props.children) {
                this.setState({
                    typeaheadFilteredChildren: React.Children.toArray(this.props.children)
                });
            }
            if (prevProps.selections !== this.props.selections && this.props.variant === selectConstants_1.SelectVariant.typeahead) {
                this.setState({
                    typeaheadInputValue: this.props.selections
                });
            }
        };
        this.onEnter = () => {
            this.setState({ openedOnEnter: true });
        };
        this.onClose = () => {
            this.setState({
                openedOnEnter: false,
                typeaheadInputValue: null,
                typeaheadActiveChild: null,
                typeaheadFilteredChildren: React.Children.toArray(this.props.children),
                typeaheadCurrIndex: -1
            });
        };
        this.onChange = (e) => {
            const { onFilter, isCreatable, onCreateOption, createText, noResultsFoundText, children } = this.props;
            let typeaheadFilteredChildren;
            if (onFilter) {
                typeaheadFilteredChildren = onFilter(e) || children;
            }
            else {
                let input;
                try {
                    input = new RegExp(e.target.value.toString(), 'i');
                }
                catch (err) {
                    input = new RegExp(e.target.value.toString().replace(/[.*+?^${}()|[\]\\]/g, '\\$&'), 'i');
                }
                typeaheadFilteredChildren =
                    e.target.value.toString() !== ''
                        ? React.Children.toArray(this.props.children).filter(child => this.getDisplay(child.props.value.toString(), 'text').search(input) === 0)
                        : React.Children.toArray(this.props.children);
            }
            if (!typeaheadFilteredChildren) {
                typeaheadFilteredChildren = [];
            }
            if (typeaheadFilteredChildren.length === 0) {
                !isCreatable &&
                    typeaheadFilteredChildren.push(React.createElement(SelectOption_1.SelectOption, { isDisabled: true, key: 0, value: noResultsFoundText, isNoResultsOption: true }));
            }
            if (isCreatable && e.target.value !== '') {
                const newValue = e.target.value;
                typeaheadFilteredChildren.push(React.createElement(SelectOption_1.SelectOption, { key: 0, value: newValue, onClick: () => onCreateOption && onCreateOption(newValue) },
                    createText,
                    " \"",
                    newValue,
                    "\""));
            }
            this.setState({
                typeaheadInputValue: e.target.value,
                typeaheadCurrIndex: -1,
                typeaheadFilteredChildren,
                typeaheadActiveChild: null,
                creatableValue: e.target.value
            });
            this.refCollection = [];
        };
        this.onClick = (e) => {
            e.stopPropagation();
        };
        this.clearSelection = (e) => {
            e.stopPropagation();
            this.setState({
                typeaheadInputValue: null,
                typeaheadActiveChild: null,
                typeaheadFilteredChildren: React.Children.toArray(this.props.children),
                typeaheadCurrIndex: -1
            });
        };
        this.sendRef = (ref, index) => {
            this.refCollection[index] = ref;
        };
        this.handleArrowKeys = (index, position) => {
            helpers_1.keyHandler(index, 0, position, this.refCollection, this.refCollection);
        };
        this.handleFocus = () => {
            if (!this.props.isOpen) {
                this.props.onToggle(true);
            }
        };
        this.handleTypeaheadKeys = (position) => {
            const { isOpen, isCreatable, createText } = this.props;
            const { typeaheadActiveChild, typeaheadCurrIndex } = this.state;
            if (isOpen) {
                if (position === 'enter' && (typeaheadActiveChild || this.refCollection[0])) {
                    this.setState({
                        typeaheadInputValue: (typeaheadActiveChild && typeaheadActiveChild.innerText) || this.refCollection[0].innerText
                    });
                    if (typeaheadActiveChild) {
                        typeaheadActiveChild.click();
                    }
                    else {
                        this.refCollection[0].click();
                    }
                }
                else {
                    let nextIndex;
                    if (typeaheadCurrIndex === -1 && position === 'down') {
                        nextIndex = 0;
                    }
                    else if (typeaheadCurrIndex === -1 && position === 'up') {
                        nextIndex = this.refCollection.length - 1;
                    }
                    else {
                        nextIndex = helpers_1.getNextIndex(typeaheadCurrIndex, position, this.refCollection);
                    }
                    const hasDescriptionElm = Boolean(this.refCollection[nextIndex].classList.contains('pf-m-description'));
                    const optionTextElm = hasDescriptionElm
                        ? this.refCollection[nextIndex].firstElementChild
                        : this.refCollection[nextIndex];
                    this.setState({
                        typeaheadCurrIndex: nextIndex,
                        typeaheadActiveChild: this.refCollection[nextIndex],
                        typeaheadInputValue: isCreatable && optionTextElm.innerText.includes(createText)
                            ? this.state.creatableValue
                            : optionTextElm.innerText
                    });
                }
            }
        };
        this.getDisplay = (value, type = 'node') => {
            if (!value) {
                return;
            }
            const item = this.props.isGrouped
                ? React.Children.toArray(this.props.children)
                    .reduce((acc, curr) => [...acc, ...React.Children.toArray(curr.props.children)], [])
                    .find(child => child.props.value.toString() === value.toString())
                : React.Children.toArray(this.props.children).find(child => child.props.value.toString() === value.toString());
            if (item) {
                if (item && item.props.children) {
                    if (type === 'node') {
                        return item.props.children;
                    }
                    return this.findText(item);
                }
                return item.props.value.toString();
            }
            return value;
        };
        this.findText = (item) => {
            if (!item.props || !item.props.children) {
                if (typeof item !== 'string') {
                    return '';
                }
                return item;
            }
            if (typeof item.props.children === 'string') {
                return item.props.children;
            }
            const multi = [];
            React.Children.toArray(item.props.children).forEach((child) => multi.push(this.findText(child)));
            return multi.join('');
        };
        this.generateSelectedBadge = () => {
            const { customBadgeText, selections } = this.props;
            if (customBadgeText !== null) {
                return customBadgeText;
            }
            if (Array.isArray(selections) && selections.length > 0) {
                return selections.length;
            }
            return null;
        };
    }
    extendTypeaheadChildren(typeaheadActiveChild) {
        let activeElement = null;
        if (Boolean(typeaheadActiveChild) && typeaheadActiveChild.classList.contains('pf-m-description')) {
            activeElement = typeaheadActiveChild.firstElementChild;
        }
        else if (typeaheadActiveChild) {
            activeElement = typeaheadActiveChild;
        }
        return this.state.typeaheadFilteredChildren.map((child) => React.cloneElement(child, {
            isFocused: activeElement &&
                (activeElement.innerText === this.getDisplay(child.props.value.toString(), 'text') ||
                    (this.props.isCreatable &&
                        typeaheadActiveChild.innerText === `{createText} "${child.props.value}"`))
        }));
    }
    render() {
        const _a = this.props, { children, className, customContent, variant, direction, onToggle, onSelect, onClear, toggleId, isOpen, isGrouped, isPlain, isDisabled, selections: selectionsProp, typeAheadAriaLabel, clearSelectionsAriaLabel, toggleAriaLabel, removeSelectionAriaLabel, 'aria-label': ariaLabel, 'aria-labelledby': ariaLabelledBy, placeholderText, width, maxHeight, toggleIcon, ouiaId, ouiaSafe, hasInlineFilter, isCheckboxSelectionBadgeHidden, inlineFilterPlaceholderText, 
        /* eslint-disable @typescript-eslint/no-unused-vars */
        onFilter, onCreateOption, isCreatable, createText, noResultsFoundText, customBadgeText, inputIdPrefix, 
        /* eslint-enable @typescript-eslint/no-unused-vars */
        menuAppendTo } = _a, props = tslib_1.__rest(_a, ["children", "className", "customContent", "variant", "direction", "onToggle", "onSelect", "onClear", "toggleId", "isOpen", "isGrouped", "isPlain", "isDisabled", "selections", "typeAheadAriaLabel", "clearSelectionsAriaLabel", "toggleAriaLabel", "removeSelectionAriaLabel", 'aria-label', 'aria-labelledby', "placeholderText", "width", "maxHeight", "toggleIcon", "ouiaId", "ouiaSafe", "hasInlineFilter", "isCheckboxSelectionBadgeHidden", "inlineFilterPlaceholderText", "onFilter", "onCreateOption", "isCreatable", "createText", "noResultsFoundText", "customBadgeText", "inputIdPrefix", "menuAppendTo"]);
        const { openedOnEnter, typeaheadInputValue, typeaheadActiveChild, typeaheadFilteredChildren } = this.state;
        const selectToggleId = toggleId || `pf-select-toggle-id-${currentId++}`;
        const selections = Array.isArray(selectionsProp) ? selectionsProp : [selectionsProp];
        const hasAnySelections = Boolean(selections[0] && selections[0] !== '');
        let childPlaceholderText = null;
        if (!customContent) {
            if (!hasAnySelections && !placeholderText) {
                const childPlaceholder = React.Children.toArray(children).filter((child) => child.props.isPlaceholder === true);
                childPlaceholderText =
                    (childPlaceholder[0] && this.getDisplay(childPlaceholder[0].props.value, 'node')) ||
                        (children[0] && this.getDisplay(children[0].props.value, 'node'));
            }
        }
        const hasOnClear = onClear !== Select.defaultProps.onClear;
        const clearBtn = (React.createElement("button", { className: react_styles_1.css(button_1.default.button, button_1.default.modifiers.plain, select_1.default.selectToggleClear), onClick: e => {
                this.clearSelection(e);
                onClear(e);
            }, "aria-label": clearSelectionsAriaLabel, type: "button", disabled: isDisabled, ref: this.clearRef, onKeyDown: event => {
                if (event.key === selectConstants_1.KeyTypes.Enter) {
                    this.clearRef.current.click();
                }
            } },
            React.createElement(times_circle_icon_1.default, { "aria-hidden": true })));
        let selectedChips = null;
        if (variant === selectConstants_1.SelectVariant.typeaheadMulti) {
            selectedChips = (React.createElement(ChipGroup_1.ChipGroup, null, selections &&
                selections.map(item => (React.createElement(ChipGroup_1.Chip, { key: item, onClick: (e) => onSelect(e, item), closeBtnAriaLabel: removeSelectionAriaLabel }, this.getDisplay(item, 'node'))))));
        }
        let filterWithChildren = children;
        if (hasInlineFilter) {
            const filterBox = (React.createElement(React.Fragment, null,
                React.createElement("div", { key: "inline-filter", className: react_styles_1.css(select_1.default.selectMenuSearch) },
                    React.createElement("input", { key: "inline-filter-input", type: "search", className: react_styles_1.css(form_control_1.default.formControl, form_control_1.default.modifiers.search), onChange: this.onChange, placeholder: inlineFilterPlaceholderText, onKeyDown: event => {
                            if (event.key === selectConstants_1.KeyTypes.ArrowUp) {
                                this.handleArrowKeys(0, 'up');
                            }
                            else if (event.key === selectConstants_1.KeyTypes.ArrowDown) {
                                this.handleArrowKeys(0, 'down');
                            }
                        }, ref: this.filterRef, autoComplete: "off" })),
                React.createElement(Divider_1.Divider, { key: "inline-filter-divider" })));
            this.refCollection[0] = this.filterRef.current;
            filterWithChildren = [filterBox, ...typeaheadFilteredChildren].map((option, index) => React.cloneElement(option, { key: index }));
        }
        let variantProps;
        let variantChildren;
        if (customContent) {
            variantProps = {
                selected: selections,
                openedOnEnter,
                isCustomContent: true
            };
            variantChildren = customContent;
        }
        else {
            switch (variant) {
                case 'single':
                    variantProps = {
                        selected: selections[0],
                        openedOnEnter
                    };
                    variantChildren = children;
                    break;
                case 'checkbox':
                    variantProps = {
                        checked: selections,
                        isGrouped,
                        hasInlineFilter
                    };
                    variantChildren = filterWithChildren;
                    break;
                case 'typeahead':
                    variantProps = {
                        selected: selections[0],
                        openedOnEnter
                    };
                    variantChildren = this.extendTypeaheadChildren(typeaheadActiveChild);
                    break;
                case 'typeaheadmulti':
                    variantProps = {
                        selected: selections,
                        openedOnEnter
                    };
                    variantChildren = this.extendTypeaheadChildren(typeaheadActiveChild);
                    break;
            }
        }
        const menuContainer = (React.createElement(SelectMenu_1.SelectMenu, Object.assign({}, props, { isGrouped: isGrouped, selected: selections }, variantProps, { openedOnEnter: openedOnEnter, "aria-label": ariaLabel, "aria-labelledby": ariaLabelledBy, sendRef: this.sendRef, keyHandler: this.handleArrowKeys, maxHeight: maxHeight, ref: this.menuComponentRef }), variantChildren));
        const popperContainer = (React.createElement("div", { className: react_styles_1.css(select_1.default.select, isOpen && select_1.default.modifiers.expanded, direction === selectConstants_1.SelectDirection.up && select_1.default.modifiers.top, className) }, isOpen && menuContainer));
        const mainContainer = (React.createElement("div", Object.assign({ className: react_styles_1.css(select_1.default.select, isOpen && select_1.default.modifiers.expanded, direction === selectConstants_1.SelectDirection.up && select_1.default.modifiers.top, className), ref: this.parentRef }, helpers_1.getOUIAProps(Select.displayName, ouiaId, ouiaSafe)),
            React.createElement(SelectToggle_1.SelectToggle, { id: selectToggleId, parentRef: this.parentRef, menuRef: this.menuComponentRef, isOpen: isOpen, isPlain: isPlain, onToggle: onToggle, onEnter: this.onEnter, onClose: this.onClose, variant: variant, "aria-labelledby": `${ariaLabelledBy || ''} ${selectToggleId}`, "aria-label": toggleAriaLabel, handleTypeaheadKeys: this.handleTypeaheadKeys, isDisabled: isDisabled, hasClearButton: hasOnClear },
                customContent && (React.createElement("div", { className: react_styles_1.css(select_1.default.selectToggleWrapper) },
                    toggleIcon && React.createElement("span", { className: react_styles_1.css(select_1.default.selectToggleIcon) }, toggleIcon),
                    React.createElement("span", { className: react_styles_1.css(select_1.default.selectToggleText) }, placeholderText))),
                variant === selectConstants_1.SelectVariant.single && !customContent && (React.createElement(React.Fragment, null,
                    React.createElement("div", { className: react_styles_1.css(select_1.default.selectToggleWrapper) },
                        toggleIcon && React.createElement("span", { className: react_styles_1.css(select_1.default.selectToggleIcon) }, toggleIcon),
                        React.createElement("span", { className: react_styles_1.css(select_1.default.selectToggleText) }, this.getDisplay(selections[0], 'node') || placeholderText || childPlaceholderText)),
                    hasOnClear && hasAnySelections && clearBtn)),
                variant === selectConstants_1.SelectVariant.checkbox && !customContent && (React.createElement(React.Fragment, null,
                    React.createElement("div", { className: react_styles_1.css(select_1.default.selectToggleWrapper) },
                        toggleIcon && React.createElement("span", { className: react_styles_1.css(select_1.default.selectToggleIcon) }, toggleIcon),
                        React.createElement("span", { className: react_styles_1.css(select_1.default.selectToggleText) }, placeholderText),
                        !isCheckboxSelectionBadgeHidden && hasAnySelections && (React.createElement("div", { className: react_styles_1.css(select_1.default.selectToggleBadge) },
                            React.createElement("span", { className: react_styles_1.css(badge_1.default.badge, badge_1.default.modifiers.read) }, this.generateSelectedBadge())))),
                    hasOnClear && hasAnySelections && clearBtn)),
                variant === selectConstants_1.SelectVariant.typeahead && !customContent && (React.createElement(React.Fragment, null,
                    React.createElement("div", { className: react_styles_1.css(select_1.default.selectToggleWrapper) },
                        toggleIcon && React.createElement("span", { className: react_styles_1.css(select_1.default.selectToggleIcon) }, toggleIcon),
                        React.createElement("input", { className: react_styles_1.css(form_control_1.default.formControl, select_1.default.selectToggleTypeahead), "aria-activedescendant": typeaheadActiveChild && typeaheadActiveChild.id, id: `${selectToggleId}-select-typeahead`, "aria-label": typeAheadAriaLabel, placeholder: placeholderText, value: typeaheadInputValue !== null
                                ? typeaheadInputValue
                                : this.getDisplay(selections[0], 'text') || '', type: "text", onClick: this.onClick, onChange: this.onChange, onFocus: this.handleFocus, autoComplete: "off", disabled: isDisabled })),
                    (selections[0] || typeaheadInputValue) && clearBtn)),
                variant === selectConstants_1.SelectVariant.typeaheadMulti && !customContent && (React.createElement(React.Fragment, null,
                    React.createElement("div", { className: react_styles_1.css(select_1.default.selectToggleWrapper) },
                        toggleIcon && React.createElement("span", { className: react_styles_1.css(select_1.default.selectToggleIcon) }, toggleIcon),
                        selections && Array.isArray(selections) && selections.length > 0 && selectedChips,
                        React.createElement("input", { className: react_styles_1.css(form_control_1.default.formControl, select_1.default.selectToggleTypeahead), "aria-activedescendant": typeaheadActiveChild && typeaheadActiveChild.id, id: `${selectToggleId}-select-multi-typeahead-typeahead`, "aria-label": typeAheadAriaLabel, placeholder: placeholderText, value: typeaheadInputValue !== null ? typeaheadInputValue : '', type: "text", onChange: this.onChange, onClick: this.onClick, onFocus: this.handleFocus, autoComplete: "off", disabled: isDisabled })),
                    ((selections && selections.length > 0) || typeaheadInputValue) && clearBtn))),
            isOpen && menuAppendTo === 'inline' && menuContainer));
        const getParentElement = () => {
            if (this.parentRef && this.parentRef.current) {
                return this.parentRef.current.parentElement;
            }
            return null;
        };
        return (React.createElement(helpers_1.GenerateId, null, randomId => (React.createElement(selectConstants_1.SelectContext.Provider, { value: { onSelect, onClose: this.onClose, variant, inputIdPrefix: inputIdPrefix || randomId } }, menuAppendTo === 'inline' ? (mainContainer) : (React.createElement(Popper_1.Popper, { trigger: mainContainer, popper: popperContainer, direction: direction, appendTo: menuAppendTo === 'parent' ? getParentElement() : menuAppendTo, isVisible: isOpen }))))));
    }
}
exports.Select = Select;
Select.displayName = 'Select';
Select.defaultProps = {
    children: [],
    className: '',
    direction: selectConstants_1.SelectDirection.down,
    toggleId: null,
    isOpen: false,
    isGrouped: false,
    isPlain: false,
    isDisabled: false,
    isCreatable: false,
    'aria-label': '',
    'aria-labelledby': '',
    typeAheadAriaLabel: '',
    clearSelectionsAriaLabel: 'Clear all',
    toggleAriaLabel: 'Options menu',
    removeSelectionAriaLabel: 'Remove',
    selections: [],
    createText: 'Create',
    placeholderText: '',
    noResultsFoundText: 'No results found',
    variant: selectConstants_1.SelectVariant.single,
    width: '',
    onClear: () => undefined,
    onCreateOption: () => undefined,
    toggleIcon: null,
    onFilter: null,
    customContent: null,
    hasInlineFilter: false,
    inlineFilterPlaceholderText: null,
    customBadgeText: null,
    inputIdPrefix: '',
    menuAppendTo: 'inline',
    ouiaSafe: true
};
//# sourceMappingURL=Select.js.map
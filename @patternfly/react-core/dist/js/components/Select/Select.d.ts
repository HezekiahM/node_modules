import * as React from 'react';
import { SelectOptionObject } from './SelectOption';
import { OUIAProps, PickOptional } from '../../helpers';
import { ToggleMenuBaseProps } from '../../helpers/Popper/Popper';
export interface SelectProps extends ToggleMenuBaseProps, Omit<React.HTMLProps<HTMLDivElement>, 'onSelect' | 'ref' | 'checked' | 'selected'>, OUIAProps {
    /** Content rendered inside the Select */
    children?: React.ReactElement[];
    /** Classes applied to the root of the Select */
    className?: string;
    /** Flag specifying which direction the Select menu expands */
    direction?: 'up' | 'down';
    /** Flag to indicate if select is open */
    isOpen?: boolean;
    /** Flag to indicate if select options are grouped */
    isGrouped?: boolean;
    /** Display the toggle with no border or background */
    isPlain?: boolean;
    /** Flag to indicate if select is disabled */
    isDisabled?: boolean;
    /** Flag to indicate if the typeahead select allows new items */
    isCreatable?: boolean;
    /** Text displayed in typeahead select to prompt the user to create an item */
    createText?: string;
    /** Title text of Select */
    placeholderText?: string | React.ReactNode;
    /** Text to display in typeahead select when no results are found */
    noResultsFoundText?: string;
    /** Array of selected items for multi select variants. */
    selections?: string | SelectOptionObject | (string | SelectOptionObject)[];
    /** Flag indicating if selection badge should be hidden for checkbox variant,default false */
    isCheckboxSelectionBadgeHidden?: boolean;
    /** Id for select toggle element */
    toggleId?: string;
    /** Adds accessible text to Select */
    'aria-label'?: string;
    /** Id of label for the Select aria-labelledby */
    'aria-labelledby'?: string;
    /** Label for input field of type ahead select variants */
    typeAheadAriaLabel?: string;
    /** Label for clear selection button of type ahead select variants */
    clearSelectionsAriaLabel?: string;
    /** Label for toggle of type ahead select variants */
    toggleAriaLabel?: string;
    /** Label for remove chip button of multiple type ahead select variant */
    removeSelectionAriaLabel?: string;
    /** Callback for selection behavior */
    onSelect?: (event: React.MouseEvent | React.ChangeEvent, value: string | SelectOptionObject, isPlaceholder?: boolean) => void;
    /** Callback for toggle button behavior */
    onToggle: (isExpanded: boolean) => void;
    /** Callback for typeahead clear button */
    onClear?: (event: React.MouseEvent) => void;
    /** Optional callback for custom filtering */
    onFilter?: (e: React.ChangeEvent<HTMLInputElement>) => React.ReactElement[];
    /** Optional callback for newly created options */
    onCreateOption?: (newOptionValue: string) => void;
    /** Variant of rendered Select */
    variant?: 'single' | 'checkbox' | 'typeahead' | 'typeaheadmulti';
    /** Width of the select container as a number of px or string percentage */
    width?: string | number;
    /** Max height of the select container as a number of px or string percentage */
    maxHeight?: string | number;
    /** Icon element to render inside the select toggle */
    toggleIcon?: React.ReactElement;
    /** Custom content to render in the select menu.  If this prop is defined, the variant prop will be ignored and the select will render with a single select toggle */
    customContent?: React.ReactNode;
    /** Flag indicating if select should have an inline text input for filtering */
    hasInlineFilter?: boolean;
    /** Placeholder text for inline filter */
    inlineFilterPlaceholderText?: string;
    /** Custom text for select badge */
    customBadgeText?: string | number;
    /** Prefix for the id of the input in the checkbox select variant*/
    inputIdPrefix?: string;
}
export interface SelectState {
    openedOnEnter: boolean;
    typeaheadInputValue: string | null;
    typeaheadActiveChild?: HTMLElement;
    typeaheadFilteredChildren: React.ReactNode[];
    typeaheadCurrIndex: number;
    creatableValue: string;
}
export declare class Select extends React.Component<SelectProps & OUIAProps, SelectState> {
    static displayName: string;
    private parentRef;
    private menuComponentRef;
    private filterRef;
    private clearRef;
    private refCollection;
    static defaultProps: PickOptional<SelectProps>;
    state: SelectState;
    componentDidUpdate: (prevProps: SelectProps, prevState: SelectState) => void;
    onEnter: () => void;
    onClose: () => void;
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
    onClick: (e: React.MouseEvent<Element, MouseEvent>) => void;
    clearSelection: (e: React.MouseEvent<Element, MouseEvent>) => void;
    extendTypeaheadChildren(typeaheadActiveChild: HTMLElement): React.ReactElement<any, string | ((props: any) => React.ReactElement<any, string | any | (new (props: any) => React.Component<any, any, any>)>) | (new (props: any) => React.Component<any, any, any>)>[];
    sendRef: (ref: React.ReactNode, index: number) => void;
    handleArrowKeys: (index: number, position: string) => void;
    handleFocus: () => void;
    handleTypeaheadKeys: (position: string) => void;
    getDisplay: (value: string | SelectOptionObject, type?: "text" | "node") => any;
    findText: (item: React.ReactElement) => string;
    generateSelectedBadge: () => React.ReactText;
    render(): JSX.Element;
}
//# sourceMappingURL=Select.d.ts.map
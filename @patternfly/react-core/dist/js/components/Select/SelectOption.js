"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const select_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/Select/select"));
const check_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/Check/check"));
const react_styles_1 = require("@patternfly/react-styles");
const check_icon_1 = tslib_1.__importDefault(require("@patternfly/react-icons/dist/js/icons/check-icon"));
const selectConstants_1 = require("./selectConstants");
class SelectOption extends React.Component {
    constructor() {
        super(...arguments);
        this.ref = React.createRef();
        this.onKeyDown = (event) => {
            if (event.key === selectConstants_1.KeyTypes.Tab) {
                return;
            }
            event.preventDefault();
            if (event.key === selectConstants_1.KeyTypes.ArrowUp) {
                this.props.keyHandler(this.props.index, 'up');
            }
            else if (event.key === selectConstants_1.KeyTypes.ArrowDown) {
                this.props.keyHandler(this.props.index, 'down');
            }
            else if (event.key === selectConstants_1.KeyTypes.Enter) {
                this.ref.current.click();
                if (this.context.variant === selectConstants_1.SelectVariant.checkbox) {
                    this.ref.current.focus();
                }
            }
        };
    }
    componentDidMount() {
        this.props.sendRef(this.props.isDisabled ? null : this.ref.current, this.props.index);
    }
    componentDidUpdate() {
        this.props.sendRef(this.props.isDisabled ? null : this.ref.current, this.props.index);
    }
    render() {
        /* eslint-disable @typescript-eslint/no-unused-vars */
        const _a = this.props, { children, className, description, value, onClick, isDisabled, isPlaceholder, isNoResultsOption, isSelected, isChecked, isFocused, sendRef, keyHandler, index, component, inputId } = _a, props = tslib_1.__rest(_a, ["children", "className", "description", "value", "onClick", "isDisabled", "isPlaceholder", "isNoResultsOption", "isSelected", "isChecked", "isFocused", "sendRef", "keyHandler", "index", "component", "inputId"]);
        /* eslint-enable @typescript-eslint/no-unused-vars */
        const Component = component;
        return (React.createElement(selectConstants_1.SelectConsumer, null, ({ onSelect, onClose, variant, inputIdPrefix }) => (React.createElement(React.Fragment, null,
            variant !== selectConstants_1.SelectVariant.checkbox && (React.createElement("li", { role: "presentation" },
                React.createElement(Component, Object.assign({}, props, { className: react_styles_1.css(select_1.default.selectMenuItem, isSelected && select_1.default.modifiers.selected, isDisabled && select_1.default.modifiers.disabled, isFocused && select_1.default.modifiers.focus, description && select_1.default.modifiers.description, className), onClick: (event) => {
                        if (!isDisabled) {
                            onClick(event);
                            onSelect(event, value, isPlaceholder);
                            onClose();
                        }
                    }, role: "option", "aria-selected": isSelected || null, ref: this.ref, onKeyDown: this.onKeyDown, type: "button" }),
                    description && (React.createElement(React.Fragment, null,
                        React.createElement("div", { className: react_styles_1.css(select_1.default.selectMenuItemMain) },
                            children || value.toString(),
                            isSelected && (React.createElement("span", { className: react_styles_1.css(select_1.default.selectMenuItemIcon) },
                                React.createElement(check_icon_1.default, { "aria-hidden": true })))),
                        React.createElement("div", { className: react_styles_1.css(select_1.default.selectMenuItemDescription) }, description))),
                    !description && (React.createElement(React.Fragment, null,
                        children || value.toString(),
                        isSelected && (React.createElement("span", { className: react_styles_1.css(select_1.default.selectMenuItemIcon) },
                            React.createElement(check_icon_1.default, { "aria-hidden": true })))))))),
            variant === selectConstants_1.SelectVariant.checkbox && !isNoResultsOption && (React.createElement("label", Object.assign({}, props, { className: react_styles_1.css(check_1.default.check, select_1.default.selectMenuItem, isDisabled && select_1.default.modifiers.disabled, isFocused && select_1.default.modifiers.focus, description && select_1.default.modifiers.description, className), onKeyDown: this.onKeyDown }),
                React.createElement("input", { id: inputId || `${inputIdPrefix}-${value.toString()}`, className: react_styles_1.css(check_1.default.checkInput), type: "checkbox", onChange: event => {
                        if (!isDisabled) {
                            onClick(event);
                            onSelect(event, value);
                        }
                    }, ref: this.ref, checked: isChecked || false, disabled: isDisabled }),
                React.createElement("span", { className: react_styles_1.css(check_1.default.checkLabel, isDisabled && select_1.default.modifiers.disabled) }, children || value.toString()),
                description && React.createElement("div", { className: react_styles_1.css(check_1.default.checkDescription) }, description))),
            variant === selectConstants_1.SelectVariant.checkbox && isNoResultsOption && (React.createElement("div", null,
                React.createElement(Component, Object.assign({}, props, { className: react_styles_1.css(select_1.default.selectMenuItem, isSelected && select_1.default.modifiers.selected, isDisabled && select_1.default.modifiers.disabled, className), role: "option", "aria-selected": isSelected || null, ref: this.ref, onKeyDown: this.onKeyDown, type: "button" }), children || value.toString())))))));
    }
}
exports.SelectOption = SelectOption;
SelectOption.displayName = 'SelectOption';
SelectOption.defaultProps = {
    className: '',
    value: '',
    index: 0,
    isDisabled: false,
    isPlaceholder: false,
    isSelected: false,
    isChecked: false,
    isNoResultsOption: false,
    component: 'button',
    onClick: () => { },
    sendRef: () => { },
    keyHandler: () => { },
    inputId: ''
};
//# sourceMappingURL=SelectOption.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const select_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/Select/select"));
const button_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/Button/button"));
const react_styles_1 = require("@patternfly/react-styles");
const caret_down_icon_1 = tslib_1.__importDefault(require("@patternfly/react-icons/dist/js/icons/caret-down-icon"));
const selectConstants_1 = require("./selectConstants");
class SelectToggle extends React.Component {
    constructor(props) {
        super(props);
        this.onDocClick = (event) => {
            const { parentRef, menuRef, isOpen, onToggle, onClose } = this.props;
            const clickedOnToggle = parentRef && parentRef.current && parentRef.current.contains(event.target);
            const clickedWithinMenu = menuRef && menuRef.current && menuRef.current.contains && menuRef.current.contains(event.target);
            if (isOpen && !(clickedOnToggle || clickedWithinMenu)) {
                onToggle(false);
                onClose();
                this.toggle.current.focus();
            }
        };
        this.onEscPress = (event) => {
            const { parentRef, menuRef, isOpen, variant, onToggle, onClose } = this.props;
            if (event.key === selectConstants_1.KeyTypes.Tab && variant === selectConstants_1.SelectVariant.checkbox) {
                return;
            }
            const escFromToggle = parentRef && parentRef.current && parentRef.current.contains(event.target);
            const escFromWithinMenu = menuRef && menuRef.current && menuRef.current.contains && menuRef.current.contains(event.target);
            if (isOpen &&
                (event.key === selectConstants_1.KeyTypes.Escape || event.key === selectConstants_1.KeyTypes.Tab) &&
                (escFromToggle || escFromWithinMenu)) {
                onToggle(false);
                onClose();
                this.toggle.current.focus();
            }
        };
        this.onKeyDown = (event) => {
            const { isOpen, onToggle, variant, onClose, onEnter, handleTypeaheadKeys } = this.props;
            if ((event.key === selectConstants_1.KeyTypes.ArrowDown || event.key === selectConstants_1.KeyTypes.ArrowUp) &&
                (variant === selectConstants_1.SelectVariant.typeahead || variant === selectConstants_1.SelectVariant.typeaheadMulti)) {
                handleTypeaheadKeys((event.key === selectConstants_1.KeyTypes.ArrowDown && 'down') || (event.key === selectConstants_1.KeyTypes.ArrowUp && 'up'));
            }
            if (event.key === selectConstants_1.KeyTypes.Enter &&
                (variant === selectConstants_1.SelectVariant.typeahead || variant === selectConstants_1.SelectVariant.typeaheadMulti)) {
                if (isOpen) {
                    handleTypeaheadKeys('enter');
                }
                else {
                    onToggle(!isOpen);
                }
            }
            if ((event.key === selectConstants_1.KeyTypes.Tab && variant === selectConstants_1.SelectVariant.checkbox) ||
                (event.key === selectConstants_1.KeyTypes.Tab && !isOpen) ||
                (event.key !== selectConstants_1.KeyTypes.Enter && event.key !== selectConstants_1.KeyTypes.Space) ||
                ((event.key === selectConstants_1.KeyTypes.Space || event.key === selectConstants_1.KeyTypes.Enter) &&
                    (variant === selectConstants_1.SelectVariant.typeahead || variant === selectConstants_1.SelectVariant.typeaheadMulti))) {
                return;
            }
            event.preventDefault();
            if ((event.key === selectConstants_1.KeyTypes.Tab || event.key === selectConstants_1.KeyTypes.Enter || event.key === selectConstants_1.KeyTypes.Space) && isOpen) {
                onToggle(!isOpen);
                onClose();
                this.toggle.current.focus();
            }
            else if ((event.key === selectConstants_1.KeyTypes.Enter || event.key === selectConstants_1.KeyTypes.Space) && !isOpen) {
                onToggle(!isOpen);
                onEnter();
            }
        };
        const { variant } = props;
        const isTypeahead = variant === selectConstants_1.SelectVariant.typeahead || variant === selectConstants_1.SelectVariant.typeaheadMulti;
        this.toggle = isTypeahead ? React.createRef() : React.createRef();
    }
    componentDidMount() {
        document.addEventListener('mousedown', this.onDocClick);
        document.addEventListener('touchstart', this.onDocClick);
        document.addEventListener('keydown', this.onEscPress);
    }
    componentWillUnmount() {
        document.removeEventListener('mousedown', this.onDocClick);
        document.removeEventListener('touchstart', this.onDocClick);
        document.removeEventListener('keydown', this.onEscPress);
    }
    render() {
        /* eslint-disable @typescript-eslint/no-unused-vars */
        const _a = this.props, { className, children, isOpen, isActive, isPlain, isDisabled, variant, onToggle, onEnter, onClose, handleTypeaheadKeys, parentRef, menuRef, id, type, hasClearButton, 'aria-labelledby': ariaLabelledBy, 'aria-label': ariaLabel } = _a, props = tslib_1.__rest(_a, ["className", "children", "isOpen", "isActive", "isPlain", "isDisabled", "variant", "onToggle", "onEnter", "onClose", "handleTypeaheadKeys", "parentRef", "menuRef", "id", "type", "hasClearButton", 'aria-labelledby', 'aria-label']);
        /* eslint-enable @typescript-eslint/no-unused-vars */
        const isTypeahead = variant === selectConstants_1.SelectVariant.typeahead || variant === selectConstants_1.SelectVariant.typeaheadMulti || hasClearButton;
        const toggleProps = {
            id,
            'aria-labelledby': ariaLabelledBy,
            'aria-expanded': isOpen,
            'aria-haspopup': (variant !== selectConstants_1.SelectVariant.checkbox && 'listbox') || null
        };
        return (React.createElement(React.Fragment, null,
            !isTypeahead && (React.createElement("button", Object.assign({}, props, toggleProps, { ref: this.toggle, type: type, className: react_styles_1.css(select_1.default.selectToggle, isDisabled && select_1.default.modifiers.disabled, isPlain && select_1.default.modifiers.plain, isActive && select_1.default.modifiers.active, className), 
                // eslint-disable-next-line @typescript-eslint/no-unused-vars
                onClick: _event => {
                    onToggle(!isOpen);
                    if (isOpen) {
                        onClose();
                    }
                }, onKeyDown: this.onKeyDown, disabled: isDisabled }),
                children,
                React.createElement("span", { className: react_styles_1.css(select_1.default.selectToggleArrow) },
                    React.createElement(caret_down_icon_1.default, null)))),
            isTypeahead && (React.createElement("div", Object.assign({}, props, { ref: this.toggle, className: react_styles_1.css(select_1.default.selectToggle, isDisabled && select_1.default.modifiers.disabled, isPlain && select_1.default.modifiers.plain, isTypeahead && select_1.default.modifiers.typeahead, className), 
                // eslint-disable-next-line @typescript-eslint/no-unused-vars
                onClick: _event => {
                    if (!isDisabled) {
                        onToggle(true);
                    }
                }, onKeyDown: this.onKeyDown }),
                children,
                React.createElement("button", Object.assign({}, toggleProps, { type: type, className: react_styles_1.css(button_1.default.button, select_1.default.selectToggleButton, select_1.default.modifiers.plain), "aria-label": ariaLabel, onClick: _event => {
                        _event.stopPropagation();
                        onToggle(!isOpen);
                        if (isOpen) {
                            onClose();
                        }
                    } }, ((variant === selectConstants_1.SelectVariant.typeahead || variant === selectConstants_1.SelectVariant.typeaheadMulti) && {
                    tabIndex: -1
                }), { disabled: isDisabled }),
                    React.createElement(caret_down_icon_1.default, { className: react_styles_1.css(select_1.default.selectToggleArrow) }))))));
    }
}
exports.SelectToggle = SelectToggle;
SelectToggle.displayName = 'SelectToggle';
SelectToggle.defaultProps = {
    className: '',
    isOpen: false,
    isActive: false,
    isPlain: false,
    isDisabled: false,
    hasClearButton: false,
    variant: 'single',
    'aria-labelledby': '',
    'aria-label': '',
    type: 'button',
    onToggle: () => { },
    onEnter: () => { },
    onClose: () => { }
};
//# sourceMappingURL=SelectToggle.js.map
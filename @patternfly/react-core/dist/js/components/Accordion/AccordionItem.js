"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
exports.AccordionItem = ({ children = null }) => (React.createElement(React.Fragment, null, children));
exports.AccordionItem.displayName = 'AccordionItem';
//# sourceMappingURL=AccordionItem.js.map
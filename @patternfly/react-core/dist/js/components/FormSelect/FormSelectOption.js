"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
exports.FormSelectOption = (_a) => {
    var { className = '', value = '', isDisabled = false, label } = _a, props = tslib_1.__rest(_a, ["className", "value", "isDisabled", "label"]);
    return (React.createElement("option", Object.assign({}, props, { className: className, value: value, disabled: isDisabled }), label));
};
exports.FormSelectOption.displayName = 'FormSelectOption';
//# sourceMappingURL=FormSelectOption.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const input_group_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/InputGroup/input-group"));
const react_styles_1 = require("@patternfly/react-styles");
exports.InputGroupText = (_a) => {
    var { className = '', component = 'span', children } = _a, props = tslib_1.__rest(_a, ["className", "component", "children"]);
    const Component = component;
    return (React.createElement(Component, Object.assign({ className: react_styles_1.css(input_group_1.default.inputGroupText, className) }, props), children));
};
exports.InputGroupText.displayName = 'InputGroupText';
//# sourceMappingURL=InputGroupText.js.map
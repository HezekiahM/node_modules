"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const Divider_1 = require("../Divider");
exports.NavItemSeparator = (_a) => {
    var { component = 'li' } = _a, props = tslib_1.__rest(_a, ["component"]);
    return React.createElement(Divider_1.Divider, Object.assign({ component: component }, props));
};
exports.NavItemSeparator.displayName = 'NavItemSeparator';
//# sourceMappingURL=NavItemSeparator.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const nav_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/Nav/nav"));
const accessibility_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/utilities/Accessibility/accessibility"));
const react_styles_1 = require("@patternfly/react-styles");
const angle_right_icon_1 = tslib_1.__importDefault(require("@patternfly/react-icons/dist/js/icons/angle-right-icon"));
const util_1 = require("../../helpers/util");
const Nav_1 = require("./Nav");
class NavExpandable extends React.Component {
    constructor() {
        super(...arguments);
        this.expandableRef = React.createRef();
        this.id = this.props.id || util_1.getUniqueId();
        this.state = {
            expandedState: this.props.isExpanded
        };
        this.onExpand = (e, val) => {
            if (this.props.onExpand) {
                this.props.onExpand(e, val);
            }
            else {
                this.setState({ expandedState: val });
            }
        };
        this.handleToggle = (e, onToggle) => {
            // Item events can bubble up, ignore those
            if (!this.expandableRef.current || !this.expandableRef.current.contains(e.target)) {
                return;
            }
            const { groupId } = this.props;
            const { expandedState } = this.state;
            onToggle(e, groupId, !expandedState);
            this.onExpand(e, !expandedState);
        };
    }
    componentDidMount() {
        this.setState({ expandedState: this.props.isExpanded });
    }
    componentDidUpdate(prevProps) {
        if (this.props.isExpanded !== prevProps.isExpanded) {
            this.setState({ expandedState: this.props.isExpanded });
        }
    }
    render() {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const _a = this.props, { id, title, srText, children, className, isActive, groupId, isExpanded, onExpand } = _a, props = tslib_1.__rest(_a, ["id", "title", "srText", "children", "className", "isActive", "groupId", "isExpanded", "onExpand"]);
        const { expandedState } = this.state;
        return (React.createElement(Nav_1.NavContext.Consumer, null, (context) => (React.createElement("li", Object.assign({ className: react_styles_1.css(nav_1.default.navItem, nav_1.default.modifiers.expandable, expandedState && nav_1.default.modifiers.expanded, isActive && nav_1.default.modifiers.current, className), onClick: (e) => this.handleToggle(e, context.onToggle) }, props),
            React.createElement("a", { ref: this.expandableRef, className: nav_1.default.navLink, id: srText ? null : this.id, href: "#", onClick: e => e.preventDefault(), onMouseDown: e => e.preventDefault(), "aria-expanded": expandedState },
                title,
                React.createElement("span", { className: react_styles_1.css(nav_1.default.navToggle) },
                    React.createElement("span", { className: react_styles_1.css(nav_1.default.navToggleIcon) },
                        React.createElement(angle_right_icon_1.default, { "aria-hidden": "true" })))),
            React.createElement("section", { className: react_styles_1.css(nav_1.default.navSubnav), "aria-labelledby": this.id, hidden: expandedState ? null : true },
                srText && (React.createElement("h2", { className: react_styles_1.css(accessibility_1.default.screenReader), id: this.id }, srText)),
                React.createElement("ul", { className: react_styles_1.css(nav_1.default.navList) }, children))))));
    }
}
exports.NavExpandable = NavExpandable;
NavExpandable.displayName = 'NavExpandable';
NavExpandable.defaultProps = {
    srText: '',
    isExpanded: false,
    children: '',
    className: '',
    groupId: null,
    isActive: false,
    id: ''
};
//# sourceMappingURL=NavExpandable.js.map
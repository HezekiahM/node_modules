"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const nav_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/Nav/nav"));
const react_styles_1 = require("@patternfly/react-styles");
const Nav_1 = require("./Nav");
exports.NavItem = (_a) => {
    var { children, styleChildren = true, className, to, isActive = false, groupId = null, itemId = null, preventDefault = false, onClick = null, component = 'a' } = _a, props = tslib_1.__rest(_a, ["children", "styleChildren", "className", "to", "isActive", "groupId", "itemId", "preventDefault", "onClick", "component"]);
    const Component = component;
    const renderDefaultLink = (context) => {
        const preventLinkDefault = preventDefault || !to;
        return (React.createElement(Component, Object.assign({ href: to, onClick: (e) => context.onSelect(e, groupId, itemId, to, preventLinkDefault, onClick), className: react_styles_1.css(nav_1.default.navLink, isActive && nav_1.default.modifiers.current, className), "aria-current": isActive ? 'page' : null }, props), children));
    };
    const renderClonedChild = (context, child) => React.cloneElement(child, Object.assign({ onClick: (e) => context.onSelect(e, groupId, itemId, to, preventDefault, onClick), 'aria-current': isActive ? 'page' : null }, (styleChildren && {
        className: react_styles_1.css(nav_1.default.navLink, isActive && nav_1.default.modifiers.current, child.props && child.props.className)
    })));
    return (React.createElement("li", { className: react_styles_1.css(nav_1.default.navItem, className) },
        React.createElement(Nav_1.NavContext.Consumer, null, context => React.isValidElement(children)
            ? renderClonedChild(context, children)
            : renderDefaultLink(context))));
};
exports.NavItem.displayName = 'NavItem';
//# sourceMappingURL=NavItem.js.map
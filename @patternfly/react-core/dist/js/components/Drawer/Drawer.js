"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const drawer_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/Drawer/drawer"));
const react_styles_1 = require("@patternfly/react-styles");
exports.DrawerContext = React.createContext({
    isExpanded: false,
    isStatic: false,
    onExpand: () => { }
});
exports.Drawer = (_a) => {
    var { className = '', children, isExpanded = false, isInline = false, isStatic = false, position = 'right', onExpand = () => { } } = _a, props = tslib_1.__rest(_a, ["className", "children", "isExpanded", "isInline", "isStatic", "position", "onExpand"]);
    return (React.createElement(exports.DrawerContext.Provider, { value: { isExpanded, isStatic, onExpand } },
        React.createElement("div", Object.assign({ className: react_styles_1.css(drawer_1.default.drawer, isExpanded && drawer_1.default.modifiers.expanded, isInline && drawer_1.default.modifiers.inline, isStatic && drawer_1.default.modifiers.static, position === 'left' && drawer_1.default.modifiers.panelLeft, className) }, props), children)));
};
exports.Drawer.displayName = 'Drawer';
//# sourceMappingURL=Drawer.js.map
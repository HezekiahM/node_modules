import * as React from 'react';
export interface DrawerProps extends React.HTMLProps<HTMLDivElement> {
    /** Additional classes added to the Drawer. */
    className?: string;
    /** Content rendered in the left hand panel */
    children?: React.ReactNode;
    /** Indicates if the drawer is expanded */
    isExpanded?: boolean;
    /** Indicates if the content element and panel element are displayed side by side. */
    isInline?: boolean;
    /** Indicates if the drawer will always show both content and panel. */
    isStatic?: boolean;
    /** Position of the drawer panel */
    position?: 'left' | 'right';
    /** Callback when drawer panel is expanded after waiting 250ms for animation to complete. */
    onExpand?: () => void;
}
export interface DrawerContextProps {
    isExpanded: boolean;
    isStatic: boolean;
    onExpand?: () => void;
}
export declare const DrawerContext: React.Context<Partial<DrawerContextProps>>;
export declare const Drawer: React.SFC<DrawerProps>;
//# sourceMappingURL=Drawer.d.ts.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const drawer_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/Drawer/drawer"));
const react_styles_1 = require("@patternfly/react-styles");
const DrawerMain_1 = require("./DrawerMain");
exports.DrawerContent = (_a) => {
    var { 
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    className = '', children, panelContent } = _a, props = tslib_1.__rest(_a, ["className", "children", "panelContent"]);
    return (React.createElement(DrawerMain_1.DrawerMain, null,
        React.createElement("div", Object.assign({ className: react_styles_1.css(drawer_1.default.drawerContent, className) }, props), children),
        panelContent));
};
exports.DrawerContent.displayName = 'DrawerContent';
//# sourceMappingURL=DrawerContent.js.map
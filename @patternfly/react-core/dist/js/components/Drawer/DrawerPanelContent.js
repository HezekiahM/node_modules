"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const drawer_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/Drawer/drawer"));
const react_styles_1 = require("@patternfly/react-styles");
const Drawer_1 = require("./Drawer");
const util_1 = require("../../helpers/util");
exports.DrawerPanelContent = (_a) => {
    var { className = '', children, hasNoBorder = false, widths } = _a, props = tslib_1.__rest(_a, ["className", "children", "hasNoBorder", "widths"]);
    return (React.createElement(Drawer_1.DrawerContext.Consumer, null, ({ isExpanded, isStatic, onExpand }) => {
        const hidden = isStatic ? false : !isExpanded;
        return (React.createElement("div", Object.assign({ className: react_styles_1.css(drawer_1.default.drawerPanel, hasNoBorder && drawer_1.default.modifiers.noBorder, util_1.formatBreakpointMods(widths, drawer_1.default), className), onTransitionEnd: ev => {
                if (!hidden && ev.nativeEvent.propertyName === 'transform') {
                    onExpand();
                }
            }, hidden: hidden }, props), !hidden && children));
    }));
};
exports.DrawerPanelContent.displayName = 'DrawerPanelContent';
//# sourceMappingURL=DrawerPanelContent.js.map
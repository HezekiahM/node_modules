"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const button_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/Button/button"));
const react_styles_1 = require("@patternfly/react-styles");
const helpers_1 = require("../../helpers");
var ButtonVariant;
(function (ButtonVariant) {
    ButtonVariant["primary"] = "primary";
    ButtonVariant["secondary"] = "secondary";
    ButtonVariant["tertiary"] = "tertiary";
    ButtonVariant["danger"] = "danger";
    ButtonVariant["link"] = "link";
    ButtonVariant["plain"] = "plain";
    ButtonVariant["control"] = "control";
})(ButtonVariant = exports.ButtonVariant || (exports.ButtonVariant = {}));
var ButtonType;
(function (ButtonType) {
    ButtonType["button"] = "button";
    ButtonType["submit"] = "submit";
    ButtonType["reset"] = "reset";
})(ButtonType = exports.ButtonType || (exports.ButtonType = {}));
exports.Button = (_a) => {
    var { children = null, className = '', component = 'button', isActive = false, isBlock = false, isDisabled = false, isAriaDisabled = false, isSmall = false, inoperableEvents = ['onClick', 'onKeyPress'], isInline = false, type = ButtonType.button, variant = ButtonVariant.primary, iconPosition = 'left', 'aria-label': ariaLabel = null, icon = null, ouiaId, ouiaSafe = true, tabIndex = null } = _a, props = tslib_1.__rest(_a, ["children", "className", "component", "isActive", "isBlock", "isDisabled", "isAriaDisabled", "isSmall", "inoperableEvents", "isInline", "type", "variant", "iconPosition", 'aria-label', "icon", "ouiaId", "ouiaSafe", "tabIndex"]);
    const Component = component;
    const isButtonElement = Component === 'button';
    if (isAriaDisabled && process.env.NODE_ENV !== 'production') {
        // eslint-disable-next-line no-console
        console.warn('You are using a beta component feature (isAriaDisabled). These api parts are subject to change in the future.');
    }
    const preventedEvents = inoperableEvents.reduce((handlers, eventToPrevent) => (Object.assign(Object.assign({}, handlers), { [eventToPrevent]: (event) => {
            event.preventDefault();
        } })), {});
    const getDefaultTabIdx = () => {
        if (isDisabled) {
            return isButtonElement ? null : -1;
        }
        else if (isAriaDisabled) {
            return null;
        }
    };
    return (React.createElement(Component, Object.assign({}, props, (isAriaDisabled ? preventedEvents : null), { "aria-disabled": isDisabled || isAriaDisabled, "aria-label": ariaLabel, className: react_styles_1.css(button_1.default.button, button_1.default.modifiers[variant], isBlock && button_1.default.modifiers.block, isDisabled && button_1.default.modifiers.disabled, isAriaDisabled && button_1.default.modifiers.ariaDisabled, isActive && button_1.default.modifiers.active, isInline && variant === ButtonVariant.link && button_1.default.modifiers.inline, isSmall && button_1.default.modifiers.small, className), disabled: isButtonElement ? isDisabled : null, tabIndex: tabIndex !== null ? tabIndex : getDefaultTabIdx(), type: isButtonElement ? type : null }, helpers_1.getOUIAProps(exports.Button.displayName, ouiaId, ouiaSafe)),
        variant !== ButtonVariant.plain && icon && iconPosition === 'left' && (React.createElement("span", { className: react_styles_1.css(button_1.default.buttonIcon, button_1.default.modifiers.start) }, icon)),
        children,
        variant !== ButtonVariant.plain && icon && iconPosition === 'right' && (React.createElement("span", { className: react_styles_1.css(button_1.default.buttonIcon, button_1.default.modifiers.end) }, icon))));
};
exports.Button.displayName = 'Button';
//# sourceMappingURL=Button.js.map
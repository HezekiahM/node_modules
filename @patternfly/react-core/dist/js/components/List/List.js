"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const list_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/List/list"));
const react_styles_1 = require("@patternfly/react-styles");
var OrderType;
(function (OrderType) {
    OrderType["number"] = "1";
    OrderType["lowercaseLetter"] = "a";
    OrderType["uppercaseLetter"] = "A";
    OrderType["lowercaseRomanNumber"] = "i";
    OrderType["uppercaseRomanNumber"] = "I";
})(OrderType = exports.OrderType || (exports.OrderType = {}));
var ListVariant;
(function (ListVariant) {
    ListVariant["inline"] = "inline";
})(ListVariant = exports.ListVariant || (exports.ListVariant = {}));
var ListComponent;
(function (ListComponent) {
    ListComponent["ol"] = "ol";
    ListComponent["ul"] = "ul";
})(ListComponent = exports.ListComponent || (exports.ListComponent = {}));
exports.List = (_a) => {
    var { className = '', children = null, variant = null, type = OrderType.number, ref = null, component = ListComponent.ul } = _a, props = tslib_1.__rest(_a, ["className", "children", "variant", "type", "ref", "component"]);
    return component === ListComponent.ol ? (React.createElement("ol", Object.assign({ ref: ref, type: type }, props, { className: react_styles_1.css(list_1.default.list, variant && list_1.default.modifiers[variant], className) }), children)) : (React.createElement("ul", Object.assign({ ref: ref }, props, { className: react_styles_1.css(list_1.default.list, variant && list_1.default.modifiers[variant], className) }), children));
};
exports.List.displayName = 'List';
//# sourceMappingURL=List.js.map
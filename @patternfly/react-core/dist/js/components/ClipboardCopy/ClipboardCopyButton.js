"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const copy_icon_1 = tslib_1.__importDefault(require("@patternfly/react-icons/dist/js/icons/copy-icon"));
const Button_1 = require("../Button");
const Tooltip_1 = require("../Tooltip");
exports.ClipboardCopyButton = (_a) => {
    var { onClick, exitDelay = 100, entryDelay = 100, maxWidth = '100px', position = 'top', 'aria-label': ariaLabel = 'Copyable input', id, textId, children } = _a, props = tslib_1.__rest(_a, ["onClick", "exitDelay", "entryDelay", "maxWidth", "position", 'aria-label', "id", "textId", "children"]);
    return (React.createElement(Tooltip_1.Tooltip, { trigger: "mouseenter focus click", exitDelay: exitDelay, entryDelay: entryDelay, maxWidth: maxWidth, position: position, content: React.createElement("div", null, children) },
        React.createElement(Button_1.Button, Object.assign({ type: "button", variant: "control", onClick: onClick, "aria-label": ariaLabel, id: id, "aria-labelledby": `${id} ${textId}` }, props),
            React.createElement(copy_icon_1.default, null))));
};
exports.ClipboardCopyButton.displayName = 'ClipboardCopyButton';
//# sourceMappingURL=ClipboardCopyButton.js.map
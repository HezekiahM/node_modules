"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const Button_1 = require("../Button");
const react_styles_1 = require("@patternfly/react-styles");
const notification_badge_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/NotificationBadge/notification-badge"));
exports.NotificationBadge = (_a) => {
    var { isRead = false, className, children } = _a, props = tslib_1.__rest(_a, ["isRead", "className", "children"]);
    return (React.createElement(Button_1.Button, Object.assign({ variant: Button_1.ButtonVariant.plain, className: className }, props),
        React.createElement("span", { className: react_styles_1.css(notification_badge_1.default.notificationBadge, isRead ? notification_badge_1.default.modifiers.read : notification_badge_1.default.modifiers.unread) }, children)));
};
exports.NotificationBadge.displayName = 'NotificationBadge';
//# sourceMappingURL=NotificationBadge.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const form_control_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/FormControl/form-control"));
const react_styles_1 = require("@patternfly/react-styles");
const constants_1 = require("../../helpers/constants");
var TextInputTypes;
(function (TextInputTypes) {
    TextInputTypes["text"] = "text";
    TextInputTypes["date"] = "date";
    TextInputTypes["datetimeLocal"] = "datetime-local";
    TextInputTypes["email"] = "email";
    TextInputTypes["month"] = "month";
    TextInputTypes["number"] = "number";
    TextInputTypes["password"] = "password";
    TextInputTypes["search"] = "search";
    TextInputTypes["tel"] = "tel";
    TextInputTypes["time"] = "time";
    TextInputTypes["url"] = "url";
})(TextInputTypes = exports.TextInputTypes || (exports.TextInputTypes = {}));
class TextInputBase extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = (event) => {
            if (this.props.onChange) {
                this.props.onChange(event.currentTarget.value, event);
            }
        };
        if (!props.id && !props['aria-label'] && !props['aria-labelledby']) {
            // eslint-disable-next-line no-console
            console.error('Text input:', 'Text input requires either an id or aria-label to be specified');
        }
    }
    render() {
        const _a = this.props, { innerRef, className, type, value, 
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        onChange, validated, isReadOnly, isRequired, isDisabled } = _a, props = tslib_1.__rest(_a, ["innerRef", "className", "type", "value", "onChange", "validated", "isReadOnly", "isRequired", "isDisabled"]);
        return (React.createElement("input", Object.assign({}, props, { className: react_styles_1.css(form_control_1.default.formControl, validated === constants_1.ValidatedOptions.success && form_control_1.default.modifiers.success, validated === constants_1.ValidatedOptions.warning && form_control_1.default.modifiers.warning, className), onChange: this.handleChange, type: type, value: value, "aria-invalid": validated === constants_1.ValidatedOptions.error, required: isRequired, disabled: isDisabled, readOnly: isReadOnly, ref: innerRef })));
    }
}
exports.TextInputBase = TextInputBase;
TextInputBase.displayName = 'TextInputBase';
TextInputBase.defaultProps = {
    'aria-label': null,
    className: '',
    isRequired: false,
    validated: 'default',
    isDisabled: false,
    isReadOnly: false,
    type: TextInputTypes.text,
    onChange: () => undefined
};
exports.TextInput = React.forwardRef((props, ref) => (React.createElement(TextInputBase, Object.assign({}, props, { innerRef: ref }))));
//# sourceMappingURL=TextInput.js.map
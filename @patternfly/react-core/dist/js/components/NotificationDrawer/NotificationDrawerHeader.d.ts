import * as React from 'react';
export interface NotificationDrawerHeaderProps extends React.HTMLProps<HTMLDivElement> {
    /**  Content rendered inside the drawer */
    children?: React.ReactNode;
    /**  Additional classes for notification drawer header. */
    className?: string;
    /**  Notification drawer heading count */
    count?: number;
    /**  Notification drawer heading title */
    title?: string;
    /**  Notification drawer heading unread custom text */
    unreadText?: string;
}
export declare const NotificationDrawerHeader: React.FunctionComponent<NotificationDrawerHeaderProps>;
//# sourceMappingURL=NotificationDrawerHeader.d.ts.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const angle_right_icon_1 = tslib_1.__importDefault(require("@patternfly/react-icons/dist/js/icons/angle-right-icon"));
const react_styles_1 = require("@patternfly/react-styles");
const notification_drawer_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/NotificationDrawer/notification-drawer"));
const Badge_1 = require("../Badge");
exports.NotificationDrawerGroup = (_a) => {
    var { children, className = '', count, isExpanded, isRead = false, 
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    onExpand = (event, expanded) => undefined, title } = _a, props = tslib_1.__rest(_a, ["children", "className", "count", "isExpanded", "isRead", "onExpand", "title"]);
    return (React.createElement("section", Object.assign({}, props, { className: react_styles_1.css(notification_drawer_1.default.notificationDrawerGroup, isExpanded && notification_drawer_1.default.modifiers.expanded, className) }),
        React.createElement("h1", null,
            React.createElement("button", { className: react_styles_1.css(notification_drawer_1.default.notificationDrawerGroupToggle), "aria-expanded": isExpanded, onClick: e => onExpand(e, !isExpanded) },
                React.createElement("div", null, title),
                React.createElement("div", { className: react_styles_1.css(notification_drawer_1.default.notificationDrawerGroupToggleCount) },
                    React.createElement(Badge_1.Badge, { isRead: isRead }, count)),
                React.createElement("span", { className: "pf-c-notification-drawer__group-toggle-icon" },
                    React.createElement(angle_right_icon_1.default, null)))),
        children));
};
exports.NotificationDrawerGroup.displayName = 'NotificationDrawerGroup';
//# sourceMappingURL=NotificationDrawerGroup.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const react_styles_1 = require("@patternfly/react-styles");
const notification_drawer_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/NotificationDrawer/notification-drawer"));
exports.NotificationDrawerList = (_a) => {
    var { children, className = '', isHidden = false } = _a, props = tslib_1.__rest(_a, ["children", "className", "isHidden"]);
    return (React.createElement("ul", Object.assign({}, props, { className: react_styles_1.css(notification_drawer_1.default.notificationDrawerList, className), hidden: isHidden }), children));
};
exports.NotificationDrawerList.displayName = 'NotificationDrawerList';
//# sourceMappingURL=NotificationDrawerList.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const react_styles_1 = require("@patternfly/react-styles");
const notification_drawer_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/NotificationDrawer/notification-drawer"));
const Text_1 = require("../Text");
exports.NotificationDrawerHeader = (_a) => {
    var { children, className = '', count, title = 'Notifications', unreadText = 'unread' } = _a, props = tslib_1.__rest(_a, ["children", "className", "count", "title", "unreadText"]);
    return (React.createElement("div", Object.assign({}, props, { className: react_styles_1.css(notification_drawer_1.default.notificationDrawerHeader, className) }),
        React.createElement(Text_1.Text, { component: Text_1.TextVariants.h1, className: react_styles_1.css(notification_drawer_1.default.notificationDrawerHeaderTitle) }, title),
        count && React.createElement("span", { className: react_styles_1.css(notification_drawer_1.default.notificationDrawerHeaderStatus) }, `${count} ${unreadText}`),
        children && React.createElement("div", { className: react_styles_1.css(notification_drawer_1.default.notificationDrawerHeaderAction) }, children)));
};
exports.NotificationDrawerHeader.displayName = 'NotificationDrawerHeader';
//# sourceMappingURL=NotificationDrawerHeader.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const react_styles_1 = require("@patternfly/react-styles");
const notification_drawer_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/NotificationDrawer/notification-drawer"));
exports.NotificationDrawerListItem = (_a) => {
    var { children = null, className = '', isHoverable = true, isRead = false, 
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    onClick = (event) => undefined, tabIndex = 0, variant } = _a, props = tslib_1.__rest(_a, ["children", "className", "isHoverable", "isRead", "onClick", "tabIndex", "variant"]);
    return (React.createElement("li", Object.assign({}, props, { className: react_styles_1.css(notification_drawer_1.default.notificationDrawerListItem, isHoverable && notification_drawer_1.default.modifiers.hoverable, notification_drawer_1.default.modifiers[variant], isRead && notification_drawer_1.default.modifiers.read, className), tabIndex: tabIndex, onClick: e => onClick(e) }), children));
};
exports.NotificationDrawerListItem.displayName = 'NotificationDrawerListItem';
//# sourceMappingURL=NotificationDrawerListItem.js.map
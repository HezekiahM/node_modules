import * as React from 'react';
export interface NotificationDrawerProps extends React.HTMLProps<HTMLDivElement> {
    /**  Content rendered inside the notification drawer */
    children?: React.ReactNode;
    /**  Additional classes added to the notification drawer */
    className?: string;
}
export declare const NotificationDrawer: React.FunctionComponent<NotificationDrawerProps>;
//# sourceMappingURL=NotificationDrawer.d.ts.map
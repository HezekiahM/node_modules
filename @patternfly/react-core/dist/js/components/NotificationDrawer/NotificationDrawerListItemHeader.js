"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const bell_icon_1 = tslib_1.__importDefault(require("@patternfly/react-icons/dist/js/icons/bell-icon"));
const check_circle_icon_1 = tslib_1.__importDefault(require("@patternfly/react-icons/dist/js/icons/check-circle-icon"));
const exclamation_circle_icon_1 = tslib_1.__importDefault(require("@patternfly/react-icons/dist/js/icons/exclamation-circle-icon"));
const exclamation_triangle_icon_1 = tslib_1.__importDefault(require("@patternfly/react-icons/dist/js/icons/exclamation-triangle-icon"));
const info_circle_icon_1 = tslib_1.__importDefault(require("@patternfly/react-icons/dist/js/icons/info-circle-icon"));
const react_styles_1 = require("@patternfly/react-styles");
const notification_drawer_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/NotificationDrawer/notification-drawer"));
const accessibility_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/utilities/Accessibility/accessibility"));
exports.variantIcons = {
    success: check_circle_icon_1.default,
    danger: exclamation_circle_icon_1.default,
    warning: exclamation_triangle_icon_1.default,
    info: info_circle_icon_1.default,
    default: bell_icon_1.default
};
exports.NotificationDrawerListItemHeader = (_a) => {
    var { children, className = '', icon = null, srTitle, title, variant = 'default' } = _a, props = tslib_1.__rest(_a, ["children", "className", "icon", "srTitle", "title", "variant"]);
    const Icon = exports.variantIcons[variant];
    return (React.createElement(React.Fragment, null,
        React.createElement("div", Object.assign({}, props, { className: react_styles_1.css(notification_drawer_1.default.notificationDrawerListItemHeader, className) }),
            React.createElement("span", { className: react_styles_1.css(notification_drawer_1.default.notificationDrawerListItemHeaderIcon) }, icon ? icon : React.createElement(Icon, null)),
            React.createElement("h2", { className: react_styles_1.css(notification_drawer_1.default.notificationDrawerListItemHeaderTitle) },
                srTitle && React.createElement("span", { className: react_styles_1.css(accessibility_1.default.screenReader) }, srTitle),
                title)),
        children && React.createElement("div", { className: react_styles_1.css(notification_drawer_1.default.notificationDrawerListItemAction) }, children)));
};
exports.NotificationDrawerListItemHeader.displayName = 'NotificationDrawerListItemHeader';
//# sourceMappingURL=NotificationDrawerListItemHeader.js.map
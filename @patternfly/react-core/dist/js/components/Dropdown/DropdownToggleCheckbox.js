"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const dropdown_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/Dropdown/dropdown"));
const react_styles_1 = require("@patternfly/react-styles");
class DropdownToggleCheckbox extends React.Component {
    constructor() {
        super(...arguments);
        this.handleChange = (event) => {
            this.props.onChange(event.target.checked, event);
        };
        this.calculateChecked = () => {
            const { isChecked, checked } = this.props;
            return isChecked !== undefined ? isChecked : checked;
        };
    }
    render() {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const _a = this.props, { className, onChange, isValid, isDisabled, isChecked, checked, children } = _a, props = tslib_1.__rest(_a, ["className", "onChange", "isValid", "isDisabled", "isChecked", "checked", "children"]);
        const text = children && (React.createElement("span", { className: react_styles_1.css(dropdown_1.default.dropdownToggleText, className), "aria-hidden": "true", id: `${props.id}-text` }, children));
        return (React.createElement("label", { className: react_styles_1.css(dropdown_1.default.dropdownToggleCheck, className), htmlFor: props.id },
            React.createElement("input", Object.assign({}, props, (this.calculateChecked() !== undefined && { onChange: this.handleChange }), { type: "checkbox", ref: elem => elem && (elem.indeterminate = isChecked === null), "aria-invalid": !isValid, disabled: isDisabled, checked: this.calculateChecked() })),
            text));
    }
}
exports.DropdownToggleCheckbox = DropdownToggleCheckbox;
DropdownToggleCheckbox.displayName = 'DropdownToggleCheckbox';
DropdownToggleCheckbox.defaultProps = {
    className: '',
    isValid: true,
    isDisabled: false,
    onChange: () => undefined
};
//# sourceMappingURL=DropdownToggleCheckbox.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
const ReactDOM = tslib_1.__importStar(require("react-dom"));
const dropdown_1 = tslib_1.__importDefault(require("@patternfly/react-styles/css/components/Dropdown/dropdown"));
const react_styles_1 = require("@patternfly/react-styles");
const util_1 = require("../../helpers/util");
const dropdownConstants_1 = require("./dropdownConstants");
class DropdownMenu extends React.Component {
    constructor() {
        super(...arguments);
        this.refsCollection = [];
        this.childKeyHandler = (index, innerIndex, position, custom = false) => {
            util_1.keyHandler(index, innerIndex, position, this.refsCollection, this.props.isGrouped ? this.refsCollection : React.Children.toArray(this.props.children), custom);
        };
        this.sendRef = (index, nodes, isDisabled, isSeparator) => {
            this.refsCollection[index] = [];
            nodes.map((node, innerIndex) => {
                if (!node) {
                    this.refsCollection[index][innerIndex] = null;
                }
                else if (!node.getAttribute) {
                    // eslint-disable-next-line react/no-find-dom-node
                    this.refsCollection[index][innerIndex] = ReactDOM.findDOMNode(node);
                }
                else if (isDisabled || isSeparator) {
                    this.refsCollection[index][innerIndex] = null;
                }
                else {
                    this.refsCollection[index][innerIndex] = node;
                }
            });
        };
    }
    componentDidMount() {
        const { autoFocus } = this.props;
        if (autoFocus) {
            // Focus first non-disabled element
            const focusTargetCollection = this.refsCollection.find(ref => ref && ref[0] && !ref[0].hasAttribute('disabled'));
            const focusTarget = focusTargetCollection && focusTargetCollection[0];
            if (focusTarget && focusTarget.focus) {
                setTimeout(() => focusTarget.focus());
            }
        }
    }
    shouldComponentUpdate() {
        // reset refsCollection before updating to account for child removal between mounts
        this.refsCollection = [];
        return true;
    }
    extendChildren() {
        const { children, isGrouped } = this.props;
        if (isGrouped) {
            let index = 0;
            return React.Children.map(children, groupedChildren => {
                const group = groupedChildren;
                return React.cloneElement(group, Object.assign({}, (group.props &&
                    group.props.children && {
                    children: (group.props.children.constructor === Array &&
                        React.Children.map(group.props.children, (option) => React.cloneElement(option, {
                            index: index++
                        }))) ||
                        React.cloneElement(group.props.children, {
                            index: index++
                        })
                })));
            });
        }
        return React.Children.map(children, (child, index) => React.cloneElement(child, {
            index
        }));
    }
    render() {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const _a = this.props, { className, isOpen, position, children, component, isGrouped, openedOnEnter, setMenuComponentRef } = _a, props = tslib_1.__rest(_a, ["className", "isOpen", "position", "children", "component", "isGrouped", "openedOnEnter", "setMenuComponentRef"]);
        return (React.createElement(dropdownConstants_1.DropdownArrowContext.Provider, { value: {
                keyHandler: this.childKeyHandler,
                sendRef: this.sendRef
            } }, component === 'div' ? (React.createElement(dropdownConstants_1.DropdownContext.Consumer, null, ({ onSelect, menuClass }) => (React.createElement("div", { className: react_styles_1.css(menuClass, position === dropdownConstants_1.DropdownPosition.right && dropdown_1.default.modifiers.alignRight, className), hidden: !isOpen, onClick: event => onSelect && onSelect(event), ref: setMenuComponentRef }, children)))) : ((isGrouped && (React.createElement(dropdownConstants_1.DropdownContext.Consumer, null, ({ menuClass, menuComponent }) => {
            const MenuComponent = (menuComponent || 'div');
            return (React.createElement(MenuComponent, Object.assign({}, props, { className: react_styles_1.css(menuClass, position === dropdownConstants_1.DropdownPosition.right && dropdown_1.default.modifiers.alignRight, className), hidden: !isOpen, role: "menu", ref: setMenuComponentRef }), this.extendChildren()));
        }))) || (React.createElement(dropdownConstants_1.DropdownContext.Consumer, null, ({ menuClass, menuComponent }) => {
            const MenuComponent = (menuComponent || component);
            return (React.createElement(MenuComponent, Object.assign({}, props, { className: react_styles_1.css(menuClass, position === dropdownConstants_1.DropdownPosition.right && dropdown_1.default.modifiers.alignRight, className), hidden: !isOpen, role: "menu", ref: setMenuComponentRef }), this.extendChildren()));
        })))));
    }
}
exports.DropdownMenu = DropdownMenu;
DropdownMenu.displayName = 'DropdownMenu';
DropdownMenu.defaultProps = {
    className: '',
    isOpen: true,
    openedOnEnter: false,
    autoFocus: true,
    position: dropdownConstants_1.DropdownPosition.left,
    component: 'ul',
    isGrouped: false,
    setMenuComponentRef: null
};
//# sourceMappingURL=DropdownMenu.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const React = tslib_1.__importStar(require("react"));
exports.ToggleTemplate = ({ firstIndex = 0, lastIndex = 0, itemCount = 0, itemsTitle = 'items' }) => (React.createElement(React.Fragment, null,
    React.createElement("b", null,
        firstIndex,
        " - ",
        lastIndex),
    ' ',
    "of ",
    React.createElement("b", null, itemCount),
    " ",
    itemsTitle));
exports.ToggleTemplate.displayName = 'ToggleTemplate';
//# sourceMappingURL=ToggleTemplate.js.map
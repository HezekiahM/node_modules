/// <reference types="react" />
declare type OuiaId = number | string;
export interface OUIAProps {
    ouiaId?: OuiaId;
    ouiaSafe?: boolean;
}
/** Get props to conform to OUIA spec
 *
 * @param {string} componentType OUIA component type
 * @param {number|string} id OUIA component id
 * @param {boolean} ouiaSafe false if in animation
 */
export declare function getOUIAProps(componentType: string, id: OuiaId | undefined, ouiaSafe?: boolean): {
    'data-ouia-component-type': string;
    'data-ouia-safe': boolean;
    'data-ouia-component-id': import("react").ReactText;
};
export {};
//# sourceMappingURL=ouia.d.ts.map
"use strict";
/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable @typescript-eslint/no-namespace */
/**
 * Added types from tippy.js and popper.js to preserve backwards compatibility
 * Can remove in next breaking change release
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Popper;
//# sourceMappingURL=DeprecatedPopperTypes.js.map
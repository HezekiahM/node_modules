"use strict";
/* eslint-disable @typescript-eslint/prefer-function-type */
/**
 * Added types from tippy.js and popper.js to preserve backwards compatibility
 * Can remove in next breaking change release
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = tippy;
//# sourceMappingURL=DeprecatedTippyTypes.js.map
import { Options as PopperOptions, VirtualElement } from '../popper-core/popper';
export declare const usePopper: (referenceElement: Element | VirtualElement, popperElement: HTMLElement, options?: Partial<PopperOptions & {
    createPopper: <TModifier extends Partial<import("../popper-core/types").Modifier<any, any>>>(reference: Element | VirtualElement, popper: HTMLElement, options?: Partial<import("../popper-core/types").OptionsGeneric<TModifier>>) => import("../popper-core/types").Instance;
}>) => {
    state: any;
    styles: {
        [key: string]: Partial<CSSStyleDeclaration>;
    };
    attributes: {
        [key: string]: {
            [key: string]: string;
        };
    };
    update: any;
    forceUpdate: any;
};
//# sourceMappingURL=usePopper.d.ts.map
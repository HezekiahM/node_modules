"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
// @ts-nocheck
const _1 = require(".");
exports.popperGenerator = _1.popperGenerator;
exports.detectOverflow = _1.detectOverflow;
const eventListeners_1 = tslib_1.__importDefault(require("./modifiers/eventListeners"));
const popperOffsets_1 = tslib_1.__importDefault(require("./modifiers/popperOffsets"));
const computeStyles_1 = tslib_1.__importDefault(require("./modifiers/computeStyles"));
const applyStyles_1 = tslib_1.__importDefault(require("./modifiers/applyStyles"));
const defaultModifiers = [eventListeners_1.default, popperOffsets_1.default, computeStyles_1.default, applyStyles_1.default];
exports.defaultModifiers = defaultModifiers;
const createPopper = _1.popperGenerator({ defaultModifiers });
exports.createPopper = createPopper;
//# sourceMappingURL=popper-lite.js.map
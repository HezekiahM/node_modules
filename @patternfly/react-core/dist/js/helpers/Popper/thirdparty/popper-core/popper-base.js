"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// @ts-nocheck
const _1 = require(".");
exports.createPopper = _1.createPopper;
exports.popperGenerator = _1.popperGenerator;
exports.detectOverflow = _1.detectOverflow;
//# sourceMappingURL=popper-base.js.map
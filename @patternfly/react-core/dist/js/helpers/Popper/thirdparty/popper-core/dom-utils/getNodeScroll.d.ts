import { Window } from '../types';
/**
 * @param node
 */
export default function getNodeScroll(node: Node | Window): any;
//# sourceMappingURL=getNodeScroll.d.ts.map
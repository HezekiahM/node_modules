"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.KEY_CODES = { ARROW_UP: 38, ARROW_DOWN: 40, ESCAPE_KEY: 27, TAB: 9, ENTER: 13, SPACE: 32 };
exports.SIDE = { RIGHT: 'right', LEFT: 'left', BOTH: 'both', NONE: 'none' };
exports.KEYHANDLER_DIRECTION = { UP: 'up', DOWN: 'down', RIGHT: 'right', LEFT: 'left' };
var ValidatedOptions;
(function (ValidatedOptions) {
    ValidatedOptions["success"] = "success";
    ValidatedOptions["error"] = "error";
    ValidatedOptions["warning"] = "warning";
    ValidatedOptions["default"] = "default";
})(ValidatedOptions = exports.ValidatedOptions || (exports.ValidatedOptions = {}));
//# sourceMappingURL=constants.js.map